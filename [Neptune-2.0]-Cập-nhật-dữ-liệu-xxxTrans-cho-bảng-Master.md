
# Nguyên tắc chung

MasterTrans là bảng ghi nhận sự thay đổi các giá trị tại MasterObject có liên hệ đến phân hệ GL. Ví dụ sự thay đổi về gốc tiền gửi, gốc vay, lãi...

Do đó, bất kỳ các hành động nào (từ người dùng hoặc hệ thống) làm thay đổi các giá trị này thì hệ thống phải cập nhật dữ liệu vào bảng MasterTrans.

# Khóa chính của bảng MasterTrans

Khóa của bảng MasterTrans bao gồm:
- Số giao dịch (TransactionNumer)
- Khóa của MasterObject
- TransCode

Ví dụ thay đổi về gốc tài khoản tiền gửi thì MasterTrans DepositAccountTrans là tổ hợp khóa của bảng tiền gửi và trường TransCode

# Vai trò của TransCode

TransCode là dữ liệu chuẩn hóa cho sự thay đổi nghiệp vụ trên MasterObject. Do đó cần định danh nhất quán trong quy trình xử lý. Chẳng hạn:
- DPT_CREDIT_BALANCE: gốc tiền gửi bị tăng
- DPT_DEBIT_BALANCE: gốc tiền gửi bị giảm
- IFC_IACR: dự chi/thu lãi

TransCode cũng là giá trị để xây dựng nên dữ liệu GL.

# Tạo bút toán GL từ TransCode

Để việc xây dựng bút toán GL từ TransCode được đồng nhất ta sẽ định nghĩa mối quan hệ này trong các bảng liên quan xxxGLConfig. Trong đó, xxx là tên của MasterTrans. Chẳng hạn ta có MasterTrans là CreditAccountTrans thì bảng cấu hình này sẽ có tên là CreditAccountTransGLConfig.

Khóa của bảng xxxGLConfig là tổ hợp:
- Các yếu tố ảnh hưởng đến bút toán
- TransCode

Ví dụ bút toán dự phòng gốc cho tài khoản vay bị chi phối bởi:
- Trạng thái nhóm nợ
- TransCode liên quan đến gốc/lãi

# Cấu hình TransCode cho `o9deposit`

Dùng bảng `TransRuleConfig` để quy định các mã nghiệp vụ có liên quan đến Posting:

| Id | TransCode          | GenerateDebitEntry | GenerateCreditEntry | DebitSysAccountName | CreditSysAccountName |
|----|--------------------|--------------------|---------------------|---------------------|----------------------|
| 1  | DPT_CREDIT_BALANCE | N                  | Y                   |                     |                      |
| 2  | DPT_DEBIT_BALANCE  | Y                  | N                   |                     |                      |
| 3  | IFC_IACR           | Y                  | Y                   | INTEREST_PAID       | INTEREST             |
| 4  | IFC_IPAY           | N                  | Y                   | INTEREST            |                      |
| 5  | IFC_ADJUST         | Y                  | Y                   | INTEREST_PAID       | INTEREST             |

# Cấu hình GL cho Catalog

Bảng `DepositCatalogueGLs`

| Id | CatalogCode | SysAccountName | COAAccount | AccountAlias     |
|----|-------------|----------------|------------|------------------|
| 1  | C1          | DEPOSIT        | 4211       | ###421101?+=0001 |
| 1  | C2          | DEPOSIT        | 4212       | ###421201?+=0001 |


# Cấu hình GL cho tài khoản (khi mở tài khoản)

Bảng `DepositAccountGLs`

| Id | MasterAccountNumber | SysAccountName | CatalogCode | GLAccount        |
|----|---------------------|----------------|-------------|------------------|
| 1  | A100                | DEPOSIT        | C1          | 0024211010000001 |
| 2  | A101                | DEPOSIT        | C2          | 0024212010000001 |
| 3  | A102                | DEPOSIT        | C3          | 0024213010000001 |

# Thực hiện giao dịch `Transfer` giữa 2 tài khoản `A100` và `A101`

- Thay đổi `Balance` bảng `DepositAccount`
- Tạo dữ liệu cho bảng audit `TransactionDetails`
- Tạo dữ liệu cho bảng Statement `DepositStatement`
- Tạo dữ liệu `MasterTrans` (`mới`)

| Id | TransactionNumber | MasterAccount | BusinessCode       | TransactionStatus | Amount | Ghi chú                          |
|----|-------------------|---------------|--------------------|-------------------|--------|----------------------------------|
| 1  | 1                 | A100          | DPT_DEBIT_BALANCE  | N                 | 1000   | Giảm gốc trong giao dịch DPT_TRF |
| 2  | 1                 | A101          | DPT_CREDIT_BALANCE | N                 | 1000   | Tăng gốctrong giao dịch DPT_TRF  |


- Tạo GL cho giao dịch từ dữ liệu bảng `MasterTrans` (`mới`)

| Id | TransactionNumber | TransactionStatus | ServiceCode | MasterAccount | SysAccountName | GLAccount        | DorC | Amount | BranchCode | CurrencyCode | Posted |
|----|-------------------|-------------------|-------------|---------------|----------------|------------------|------|--------|------------|--------------|--------|
| 1  | 1                 | N                 | DPT         | A100          | DEPOSIT        | 0024211010000001 | D    | 1000   | 002        | MMK          | N      |
| 2  | 1                 | N                 | DPT         | A101          | DEPOSIT        | 0024212010000001 | C    | 1000   | 002        | MMK          | N      |
