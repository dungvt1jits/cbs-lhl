# Cấu hình điều kiện phân loại nợ 

Điều kiện chuyển nhóm nợ được định nghĩa bằng các biểu thức cấu hình. Trong đó các điều kiện định lượng được biểu đạt bằng các biểu thức cho trước.

Khi điều kiện bị thay đổi hoặc phát sinh các đại lượng mới thì chỉ cần xây dựng thêm các đại lượng này tương ứng và khai thác trong phần khai báo điều kiện và tiếp tục các bước sau.

Ví dụ với Normal Loan, Myanmar Regulation được mô tả như sau:

![image](uploads/33ee4f353812ca8c98e4886f68c65410/image.png)

# Tính toán nhóm nợ mới cho khoản vay

Ta sẽ thực hiện khai báo các biểu thức tương ứng và định nghĩa điều kiện vào trong bảng cấu hình về sự dịch chuyển nhóm nợ. Dữ liệu nhóm nợ từng tài khoản vay được ghi nhận vào bảng `CreditAccountOverDueDaysTable`. Tham khảo [Cấu hình động điều kiện chuyển nhóm nợ](https://hcm.jits.com.vn:8062/rndhcm/cbs-shwe/-/issues/490).

# Thực hiện việc chuyển nhóm nợ cho khoản vay

Sau khi xác định được trạng thái nhóm nợ đích của tài khoản vay, ta sẽ ghi nhận dữ liệu về việc chuyển nhóm này trong bảng `ClassificationTrans` thuộc service `credit`.

# Tạo GL từ bảng `ClassificationTrans`

Cấu trúc bảng

![image](uploads/019c0c4eb70762aff1cafc91b7840e6f/image.png)

Căn cứ dữ liệu chuyển nhóm nợ cho từng tài khoản vay trong bảng `ClassificationTrans`. Tiến hành sinh bút toán chuyển nhóm nợ cho tài khoản vay.

Căn cứ vào trường `FromStatus` để sinh entry `Cr`

Căn cứ vào trường `ToStatus` để sinh entry `Dr`


# Đồng bộ `GLEntries` với ACT service