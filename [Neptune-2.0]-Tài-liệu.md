# Data Dictionary

- [Neptune Core Banking - Data Dictionary](https://docs.google.com/spreadsheets/d/1Q1zuDytWO3Lvz_ybmK-oVhnMZy98Y7kx)

# Luồng xử lý giao dịch

- [Luồng xử lý giao dịch trên core]([Neptune 2.0] Luồng xử lý giao dịch trên core)

## Xử lý dữ liệu Master
- [Cập nhật Master]([Neptune 2.0] Cập nhật dữ liệu xxxTrans cho bảng Master)

## Xử lý dữ liệu IFC

- [Lưu GL lãi phí xuống cấp IFC]([Neptune 2.0] Lưu GL lãi phí xuống cấp IFC)

- [Cập nhật IFC]([Neptune 2.0] Cập nhật dữ liệu IFC)

## Xử lý dữ liệu GL

- [Tạo dữ liệu sinh bút toán IBT]([Neptune 2.0] Tạo dữ liệu sinh bút toán IBT)

- [Tạo dữ liệu sinh bút toán FX]([Neptune 2.0] Xử lý bút toán mua bán ngoại tệ)

- [Đồng bộ GL]([Neptune 2.0] Tổng hợp dữ liệu GL)

- [Xử lý bút toán tổng hợp IBT và FX]([Neptune 2.0] Xử lý bút toán tổng hợp IBT và FX)

## Xử lý Loan Classification & Provisioning
  
[Phân loại nợ tài khoản vay]([Neptune 2.0] Phân loại nợ tài khoản vay)

Tham khảo bước [B_CRD_NPL_GEN](https://hcm.jits.com.vn:8062/rndhcm/cbs-shwe/-/issues/490)

# Xử lý giao dịch Batch

- [Cấu trúc Store Procedure trong xử lý batch](https://hcm.jits.com.vn:8062/rndhcm/cbs-shwe/-/wikis/%5BNeptune%202.0%5D%20C%E1%BA%A5u%20tr%C3%BAc%20Store%20Procedure%20trong%20x%E1%BB%AD%20l%C3%BD%20batch)

- [Danh sách các bước batch (bản ShweBank)](https://hcm.jits.com.vn:8062/rndhcm/cbs-shwe/-/wikis/%5BBatch%5D%20Danh%20s%C3%A1ch%20s%C3%A1ch%20giao%20d%E1%BB%8Bch%20batch)

- Quy tắc chung:
  - Tất cả các bước xử lý batch đều được viết bằng `code native`
  - Mỗi bước batch đều xảy ra chỉ trên một service duy nhất (hoặc thành công tất cả hoặc thất bại tất cả)
  - Khi sinh dữ liệu luôn ưu tiên dùng câu lệnh `insert into ... select ...` để đảm bảo tốc độ
  - Khi cần thực thi câu lệnh mà nội dung câu lệnh do người dùng cấu hình thì cần xây dựng câu lệnh với điều kiện được đặt trong một cột nào đó của bảng cấu hình. Sau đó thực thi câu lệnh bằng lệnh `exec ...` Tham khảo bước `B_CRD_NPL_EXEC`
  - Khi thực hiện update luôn ưu tiên thực hiện theo lô. Trường hợp update một bảng từ một nguồn khác (bảng hoặc select...) thì dùng lệnh `update.... from....`
  - Luôn đảm bảo tính nhất quán dữ liệu giữa các bảng có quan hệ với nhau bằng các trường liên kết
  - Không sử dụng cột có tính chất `Identiy` để tham chiếu từ bảng khác vì giá trị cột này không có ý nghĩa
  - Nếu cần dùng một giá trị mang tính duy nhất thì có thể dùng hàm `NewID()` để tạo một chuỗi `GUID`

# Đối soát

Việc đối soát dữ liệu giữa các service là cần thiết.

Quy tắc chung:

- Mỗi yêu cầu đi qua Neptune Server được cấp một con số duy nhất là `TransactionNumber`. Số này được quản lý bởi service Admin
- Các service khác khi xử lý cho yêu cầu này được cung cấp các giá trị chung này và lưu vào bảng `Transaction` trên service của mình
- Mỗi service sẽ sinh dữ liệu trên nhiều bảng. Các bảng này sẽ cần ghi nhận dữ liệu phát sinh theo số `TransactionNumber` này
- Đối với dữ liệu GL, các service sẽ tạo các GL entries tại bảng `GLEntries`. Tại bước đồng bộ số liệu GL sang service `ACT`, dữ liệu này sẽ được chuyển sang theo hình thức file.
- Mối quan hệ giữa các `GLEntries` và các `Master Trans` liên kết với nhau bằng trường `GLEntries`.`TransId`:
  - Khi tạo dữ liệu MasterTrans, cần sinh dữ liệu `GUID` cho trường `TransId` của bảng `MasterTrans`
  - Khi tạo dữ liệu GLEntries, cập nhật các entries GL được sinh ra từ `MasterTrans` kèm với số `TransId` của `MasterTrans`

# Tham chiếu

- [Stored Procedure trên `Credit` service](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BNeptune-2.0%5D-Danh-s%C3%A1ch-c%C3%A1c-Stored-Procedure-tr%C3%AAn-server-%5BCredit%5D)