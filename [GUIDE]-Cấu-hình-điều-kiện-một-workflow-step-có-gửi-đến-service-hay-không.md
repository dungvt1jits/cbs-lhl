
# Mô tả

Một workflow có nhiều steps.

Có nhiều tình huống ta cần thiết lập điều kiện nào đó dựa vào dữ liệu đầu vào của workflow.
- Nếu thỏa thì workflow step mới được gửi đến service qua queue
- Nếu không thỏa thì workflow step sẽ không gửi đi và thực hiện step kế tiếp

# Thực hiện

Trong phần định nghĩa workflow step, có một trường SENDING_CONDITION. Trường này định nghĩa về điều kiện để đánh giá xem đúng hay sai.

## Cú pháp

```json
{
  "conditions": [
    điều kiện 1,
    điều kiện 2,...
  ]
}
```

Ví dụ:

```json
{
  "conditions": [
    {
      "func": "IsNotNull",
      "type": "boolean",
      "paras": [
        "$.execution.input.fields.CASA_ACCOUNT"
      ]
    }
  ]
}
```

## Cách đánh giá

Sau khi đánh giá từng phần tử trong danh sách (giá trị bool cho mỗi phần tử), các phần tử này sẽ được `AND` lại với nhau.

Nói đơn giản là:
- Nếu có 1 phần tử có giá trị `false` thì toàn bộ điều kiện xem như là `false`
- Ngược lại, tất cả phần tử đều phải có giá trị `true` thì điều kiện mới được xem là `true`

và điều kiện để workflow step được gửi đi là điều kiện của trường này phải là `true`

> Trong trường hợp không khai báo giá trị cho trường này, thì mặc định NeptuneServer sẽ hiểu là `true`.

# Ghi chú

Để cấu hình một bước với điều kiện `sending_condition` nếu không thỏa thì không gửi message cho service nhưng vẫn tiếp tục thực hiện các bước sau thì ta cần phải cấu hình điều kiện ghi nhận Step thành công (`SUCCESS_CONDITION`) trong `WF_STEP_DEF`.

Ví dụ:

![image](uploads/623b928b89f074ab5054532c9519aea3/image.png)
