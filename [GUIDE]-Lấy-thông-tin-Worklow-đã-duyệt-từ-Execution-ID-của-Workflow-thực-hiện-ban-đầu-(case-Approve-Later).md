
# Mô tả bài toán duyệt worklow

- 1. Thực hiện một workflow với workflow_type loại `Normal`.
- 2. Workflow được yêu cầu duyệt vì một lý do đặc biệt nào đó (không đủ hạn mức thực hiện, giao dịch đặc biệt...)
- 3. Phía người dùng nhận được 2 lựa chọn: `Approve Now` hoặc `Approve Later`. Người dùng chọn `Approve Later`
- 4. Giao dịch được lưu lại trong màn hình `Transaction Journal` với trạng thái `Pending to approve`
- 5. Tại màn hình `Transaction Journal` người dùng thấy giao dịch tại `bước 3`
- 6. Người dùng bấm nút Approve, nhập Account thực hiện duyệt
- 7. Người dùng nhấn nút View, hệ thống không hiển thị được nội dung giao dịch `đã duyệt`

# Phân tích

Theo luồng Approve giao dịch: tại thời điểm thực hiện giao dịch (Approve Later) hệ thống tạo ra một Workflow với mã (ExecutionID) A. Tại thời điểm Approve thực tế, hệ thống tạo tiếp một workflow khác thực hiện duyệt workflow A với mã B. Toàn bộ thông tin thực hiện duyệt workflow A lúc này được thực hiện và ghi nhận dưới workflow B. Tuy nhiên, khi thể hiện trên màn hình Transaction Journal, người dùng chỉ thấy được số giao dịch A và khi view, hệ thống sẽ gọi api `/api/workflow/get-workflow-info` với số giao dịch A.

# Giải pháp

Giao dịch B sau khi duyệt thành công giao dịch A có lưu lại `mối quan hệ B - A` này.

Hệ thống cung cấp thêm một api mới `api/workflow/get-approved-execution-info/{pOriginalExecutionID}` dùng để lấy thông tin workflow B từ mã (ExecutionID) A.

Trong trường hợp tham số `pOriginalExecutionID` này không tìm thấy từ mối quan hệ `Thực hiện - Duyệt` thì hệ thống sẽ cố gắng trả về thông tin của giao dịch thực hiện.

Do đó, tại màn hình `Transaction Journal`, để đơn giản trong việc sử dụng api, developer có thể sử dụng api này thay cho api ban đầu (`api/workflow/get-execution-info/pExecutionID`) thì có thể bao quát được cả 2 trường hợp lấy thông tin của giao dịch thực hiện hay giao dịch duyệt.

# Cập nhật

- NeptuneServer mới nhất (sau 9:00 am, ngày 03-Sep-2022)
