# Mô tả

Hiện tại GL cho các khoản Lãi/phí đang được đặt tại cấp Catalog. Với cấu trúc này, các loại hình có nhiều loại lãi với hạch toán GL khác nhau hệ thống không đáp ứng được hoặc phải cấu hình bội số.

Chẳng hạn, loại hình vay có 3 loại lãi với 3 loại GL khác nhau cho nghiệp vụ dự thu. Khi đó phía Catalog phải khai báo:

- INTEREST1/INTEREST_PAID1
- INTEREST2/INTEREST_PAID2
- INTEREST3/INTEREST_PAID3

và với 3 loại phí khác nhau, catalog phải bổ sung thêm 3 loại GL khác nhau

- IFCC1
- IFCC2
- IFCC3

Để đơn giản việc này ta cần đổi lại cấu trúc GL theo IFC

# Giải pháp

- GL theo catalog chỉ lưu các GL có liên quan đến các yếu tố thuộc master như _gốc_ hoặc _dự phòng_
- Đối với các yếu tố liên quan đến IFC như tài khoản _dự thu_, _dự chi_, _chi phí_, _thu nhập_ sẽ được đưa xuống cấp IFC với tài khoản không đổi: `INTEREST`, `INTEREST_PAID`, `IFCC`...
- Đối với nghiệp vụ chuyển nhóm nợ: tài khoản chạy theo nhóm nợ nên việc cấu hình tài khoản gốc cũng cần được cấu hình lại theo nhóm nợ
