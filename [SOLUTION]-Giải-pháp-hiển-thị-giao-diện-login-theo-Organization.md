# Mô tả

Hiện tại thì CMS đang mặc định hiển thị màn hình login với các background, logo, title mặc định của hệ thống.

Do đó cần hỗ trợ hiển thị các thông tin cần thiết của organization ở màn hình login.


# Giải pháp

Khi nhập url login vào cms, cần truyền thêm `?org=<organization_code>` đại diện cho organization mà người dùng muốn đăng nhập.

Ví dụ url như sau:
 > `http://192.168.1.170:8066/login?org=NEPTUNE`

Khi load màn hình login, CMS sẽ đọc thêm tham số này từ url để tải thông tin của organization tương ứng về client để hiển thị.

Tải thông tin của organization:
1. CMS sex gọi qua neptune thông qua API để lấy dữ liệu của organization theo code, nếu không có sẽ query bảng của mục 2, nếu không có nữa sẽ dùng hình ảnh mặc định của jwebui 
2. Cần tạo thêm bảng để lưu caching các giá trị config của organization như `OrganizationCode, OrganizationName, Phone, welcome_logo, login_background,...` tương ứng trong bảng `AUTH_ORGANIZATION` hoặc lưu trữ trong `redis`.

```sql
CREATE TABLE `OrganizationParameter` (
  `organization_code` varchar(1000) NOT NULL,
  `parameter_code` varchar(1000) NOT NULL,
  `parameter_value` varchar(1000) NOT NULL,
  PRIMARY KEY (`organization_code` , `para_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
```


- Ngoài ra, trong bảng OrganizationParameter cần lưu thêm những dữ liệu để lấy token từ portal: 

```json
{
  "organization_code":"NEPTUNE",
  "parameter_code":"usercode",
  "parameter_value":"neptune"
},
{
  "organization_code":"NEPTUNE",
  "parameter_code":"password",
  "parameter_value":"123456"
},
{
  "organization_code":"NEPTUNE",
  "parameter_code":"expiration_in_minutes",
  "parameter_value":"10000"
},
{
  "organization_code":"NEPTUNE",
  "parameter_code":"token",
  "parameter_value":""
}
```
- Khi khởi tạo hệ thống, CMS sẽ mặc định dùng cặp `usercode/password` của  `"organization_code":"NEPTUNE" ` trong bảng để gọi `api/auth/get-token` để lấy ra `dynamic_token` trước, sau đó gán vào header để gọi `api/auth/get-static-token` với cặp `usercode/expiration_in_minutes` trong bảng trên để lấy `static_token` gán vào trường `token` trong bảng.

- Khi hệ thống chuẩn bị gọi màn hình login, client sẽ gọi `#sys:bo-projectLogin-start`, bo này sẽ được cấu hình thêm function để lấy thông tin organization từ url và trả về client các thông tin cần thiết để hiển thị giao diện từ bảng OrganizationParameter.


