# [Neptune 2.0]

- [Luồng xử lý dữ liệu](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BNeptune-2.0%5D-Lu%E1%BB%93ng-x%E1%BB%AD-l%C3%BD-giao-d%E1%BB%8Bch-tr%C3%AAn-core)

## Hướng dẫn cấu hình

### Cài đặt các database hỗ trợ trong Docker Engine

Xem [Cài đặt Database (MySQL, MariaDB, PostgresQL, SQLServer) trong Docker Engine](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/C%C3%A0i-%C4%91%E1%BA%B7t-Database-(MySQL,-MariaDB,-PostgresQL,-SQLServer)-trong-Docker-Engine)

### Cấu hình hoạt động

- [Load-Balancing-cho-service-CMS `14Oct2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/Load-Balancing-cho-service-CMS)
- [Hướng dẫn cấu hình kết nối Database Archiving `11Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-C%E1%BA%A5u-h%C3%ACnh-k%E1%BA%BFt-n%E1%BB%91i-Database-Archiving)
- [Hướng dẫn cấu hình kết nối Database Online `11Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGuide%5D-H%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-c%E1%BA%A5u-h%C3%ACnh-k%E1%BA%BFt-n%E1%BB%91i-Database-Online)
- [Hướng dẫn chạy Neptune server với database PostgresQL `27Sep2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-Ch%E1%BA%A1y-Neptune-server-v%E1%BB%9Bi-database-PostgresQL)
- [Hướng dẫn chạy Neptune server với database SQLServer `28Sep2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-H%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-ch%E1%BA%A1y-Neptune-server-v%E1%BB%9Bi-database-SQLServer)
- [Hướng dẫn chạy Neptune server với database MySQL/MariaDB `29Sep2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-H%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-ch%E1%BA%A1y-Neptune-server-v%E1%BB%9Bi-database-MySQL-ho%E1%BA%B7c-MariaDB)
- [Hướng dẫn cấu hình Clustering cho RabbitMQ `10Nov2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/Setup-Cluster-tr%C3%AAn-RabbitMQ)

### Cấu hình Workflow

- [Hướng dẫn cấu hình một bước không tham gia tiến trình Compensation `10Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-H%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-c%E1%BA%A5u-h%C3%ACnh-b%E1%BB%8F-qua-vi%E1%BB%87c-g%E1%BB%8Di-m%E1%BB%99t-b%C6%B0%E1%BB%9Bc-khi-th%E1%BB%B1c-hi%E1%BB%87n-giai-%C4%91o%E1%BA%A1n-%60Compensation%60)
- [Hướng dẫn thực thi một Workflow bằng NeptuneClient `09Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-Th%E1%BB%B1c-thi-m%E1%BB%99t-Workflow-th%C3%B4ng-qua-NeptuneClient-(gRPC))
- [Hướng dẫn cấu hình một workflow \[READONLY\] `11Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-H%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-c%E1%BA%A5u-h%C3%ACnh-m%E1%BB%99t-workflow-%5BREADONLY%5D)
- [Hướng dẫn thực hiện `approve` giao dịch trên màn hình F8 `13Sep2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/issues/2055 "[GUIDE] Hướng dẫn thực hiện chức năng Approve một giao dịch có trạng thái [Pending to approve]")
- [Hướng dẫn `reverse` (Hủy) một workflow đã thực hiện thành công `10Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-H%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-h%E1%BB%A7y-b%E1%BB%8F-(Reverse)-m%E1%BB%99t-giao-d%E1%BB%8Bch-%C4%91%C3%A3-ho%C3%A0n-th%C3%A0nh-tr%C6%B0%E1%BB%9Bc-%C4%91%C3%B3)
- [Cấu hình điều kiện gửi message (`Sending Condition`) của workflow step đến service `30Sep2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-C%E1%BA%A5u-h%C3%ACnh-%C4%91i%E1%BB%81u-ki%E1%BB%87n-m%E1%BB%99t-workflow-step-c%C3%B3-g%E1%BB%ADi-%C4%91%E1%BA%BFn-service-hay-kh%C3%B4ng)

### Vận hành

- [Hướng dẫn tạm dừng AN TOÀN một service chức năng để bảo trì hoặc nâng cấp `10Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGuide%5D-H%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-t%E1%BA%A1m-d%E1%BB%ABng-m%E1%BB%99t-service-ch%E1%BB%A9c-n%C4%83ng-%C4%91%E1%BB%83-b%E1%BA%A3o-tr%C3%AC-ho%E1%BA%B7c-n%C3%A2ng-c%E1%BA%A5p)
- [Hiểu về tiến trình Archiving & Purging `11Nov2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/C%C6%A1-ch%E1%BA%BF-ho%E1%BA%A1t-%C4%91%E1%BB%99ng-c%E1%BB%A7a-ti%E1%BA%BFn-tr%C3%ACnh-Archiving-&-Purging)

## Hướng dẫn coding

- [Nhận biết mã lỗi từ service trả về trong message thực thi workflow `11Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-Nh%E1%BA%ADn-bi%E1%BA%BFt-m%C3%A3-l%E1%BB%97i-t%E1%BB%AB-service-tr%E1%BA%A3-v%E1%BB%81-trong-message-th%E1%BB%B1c-thi-workflow)
- [Hướng dẫn sử dụng lớp \[JITS.Neptune.NeptuneClient.Lib.mapping.NeptuneExpression\] `16Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-H%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-s%E1%BB%AD-d%E1%BB%A5ng-l%E1%BB%9Bp-%5BJITS.Neptune.NeptuneClient.Lib.mapping.NeptuneExpression%5D)
- [Lấy thông tin Worklow đã duyệt từ Execution ID của Workflow thực hiện ban đầu (case Approve Later) `03Oct2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGUIDE%5D-L%E1%BA%A5y-th%C3%B4ng-tin-Worklow-%C4%91%C3%A3-duy%E1%BB%87t-t%E1%BB%AB-Execution-ID-c%E1%BB%A7a-Workflow-th%E1%BB%B1c-hi%E1%BB%87n-ban-%C4%91%E1%BA%A7u-(case-Approve-Later))

## Tài liệu giải pháp

- [Gửi sự kiện giữa các service với nhau `15Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BSOLUTION%5D-G%E1%BB%ADi-s%E1%BB%B1-ki%E1%BB%87n-gi%E1%BB%AFa-c%C3%A1c-service-v%E1%BB%9Bi-nhau)
- [Hiển thị giao diện login theo Organization `17Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BSOLUTION%5D-Gi%E1%BA%A3i-ph%C3%A1p-hi%E1%BB%83n-th%E1%BB%8B-giao-di%E1%BB%87n-login-theo-Organization)
- [Đồng nhất dữ liệu về số giao dịch và thời gian thực hiện giao dịch trong các phân hệ Corebanking `24Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BSOLUTION%5D-%C4%90%E1%BB%93ng-nh%E1%BA%A5t-d%E1%BB%AF-li%E1%BB%87u-v%E1%BB%81-s%E1%BB%91-giao-d%E1%BB%8Bch-v%C3%A0-th%E1%BB%9Di-gian-th%E1%BB%B1c-hi%E1%BB%87n-giao-d%E1%BB%8Bch-trong-c%C3%A1c-ph%C3%A2n-h%E1%BB%87-Corebanking)
- [Tập hợp Log trong toàn hệ thống `24Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BSOLUTION%5D-T%E1%BA%ADp-h%E1%BB%A3p-Log-trong-to%C3%A0n-h%E1%BB%87-th%E1%BB%91ng)
- [Giải pháp chức năng Report `14Nov2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/Gi%E1%BA%A3i-ph%C3%A1p-ch%E1%BB%A9c-n%C4%83ng-Report "Giải pháp chức năng Report ")

### Giải pháp bảo mật

- [Giải pháp ngăn chặn brute password `15Sep2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/issues/2057 "[SOLUTION] Thêm tính năng ngăn chặn [brute password]")
- [Các loại Token và cách quản lý Token trong Neptune `13Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BSOLUTION%5D-Gi%E1%BA%A3i-ph%C3%A1p-qu%E1%BA%A3n-l%C3%BD-token-trong-Neptune)

## Tham chiếu

- [Các hàm built-in trong Neptune `18Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BREFERENCE%5D-Tham-chi%E1%BA%BFu-c%C3%A1c-h%C3%A0m-built-in-trong-x%E1%BB%AD-l%C3%BD-message-Workflow)

## Nhật ký thay đổi CSDL db `neptune`

- [Các thay đổi DDL & DML theo thời gian `11Aug2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BNeptune-Server%5D-Nh%E1%BA%ADt-k%C3%BD-thay-%C4%91%E1%BB%95i-CSLD-Neptune)

## Các phiên bản NeptuneClient đã phát hành

- [Các phiên bản NeptuneClient đã phát hành](https://hcm.jits.com.vn:8020/packages/jits.neptune.neptuneclient/1.1.4.1)

---

## Documents of Neptune system
- [Danh sách chức năng Portal app `14-Nov-2022`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BDOC%5D-Danh-s%C3%A1ch-ch%E1%BB%A9c-n%C4%83ng-Neptune-Portal)
- [02.Data_Dictionay_v1.7.docx `14-Nov-2022`](uploads/a53a75b3aba832649c5f2e29edb4df4d/02.Data_Dictionay_v1.7.docx)
- [02.Diagrams_v1.0.docx](uploads/96d5f4b242726cd2544d450894b2f710/02.Diagrams_v1.0.docx)
- [02.Operational_Process.docx](uploads/a9d3b020bc0ef2cbbfa32b01c753642f/02.Operational_Process.docx)
- [03.Development_Guide_v2.2.docx](uploads/1a10045978b93799430c6e04a23c6979/03.Development_Guide_v2.2.docx)
- [04.1.Management_Functional_Design_v1.3.docx](uploads/b27a77ac54028691acbc5dd845609a42/04.1.Management_Functional_Design_v1.3.docx)
- [04.2.Application_Process\_-\_v1.0.docx](uploads/a05f13c1e97b4752b3b4d9c5814b47dc/04.2.Application_Process\_-\_v1.0.docx)

# Functional Specification Documents

- [Neptune_Licence_Issuer\_-\_Functional_Design_Document_v1.1.docx](uploads/72a9107a2f4a72310f3dcffdae22b497/Neptune_Licence_Issuer\_-\_Functional_Design_Document_v1.1.docx)
- [5.0.1._FSD_-\_Neptune_Portal_App.docx](uploads/75d9daafd73c5fc774bc1a676134d9f2/5.0.1.\_FSD\_-\_Neptune_Portal_App.docx)
- [5.1.FSD\_-_Neptune_Event_Management_-\_v1.0.docx](uploads/7280deb771c40626ae7bb5199c3dcb8d/5.1.FSD\_-\_Neptune_Event_Management\_-\_v1.0.docx)
- [5.1.FSD\_-_Push_Notification_-\_v1.0.docx](uploads/cb8756db273ca518268f1a4b3abba079/5.1.FSD\_-\_Push_Notification\_-\_v1.0.docx)
- [5.1.FSD\_-_SendMail_-\_v1.0.docx](uploads/7bee7e11f780657b2693ee5680369759/5.1.FSD\_-\_SendMail\_-\_v1.0.docx)

# Deployent Document
- [05.Neptune_Core_Banking_-_Installation_Guide_v1.0.docx](uploads/c916953764ce77be21d5e3c1c90e0ec9/05.Neptune_Core_Banking_-_Installation_Guide_v1.0.docx)

## Tips

Cách triển khai JWebUI trên Docker: [Dockerizing_JWebUI_v1.0.docx](uploads/6f01243bdaf9cb41e3dbdf7c51409e36/Dockerizing_JWebUI_v1.0.docx)

## Specification:

- [Spec_của_voucher_v1.3.docx](uploads/cb3b9145e36d8fd4ac657f9556bb49b8/Spec_c%E1%BB%A7a_voucher_v1.3.docx)

<details>
<summary>Previous versions</summary>

- [02.Data_Dictionay_v1.3.docx](uploads/9a0717860a7cbdb4eb6d3e938b5891c9/02.Data_Dictionay_v1.3.docx)
- [02.Data_Dictionay_v1.4.docx](uploads/2e829d1efde5995d0fabed3ba6821820/02.Data_Dictionay_v1.4.docx)
- [02.Data_Dictionay_v1.6.docx](uploads/8659db9b808fa59413a671b2f441ebe3/02.Data_Dictionay_v1.6.docx)
- [03.Development_Guide_v1.3.docx](uploads/420511a071192da8fe313d2bbc08651c/03.Development_Guide_v1.3.docx)
- [03.Development_Guide_v1.4.docx](uploads/d7c9df24c9772b0944f9e0da8e5e7c19/03.Development_Guide_v1.4.docx)
- [03.Development_Guide_v1.5.docx](uploads/6e156658050c5cd43d659ac028788137/03.Development_Guide_v1.5.docx)
- [03.Development_Guide_v1.6.docx](uploads/177c956eaa9219284bbfd273a3cc5699/03.Development_Guide_v1.6.docx)
- [03.Development_Guide_v1.7.docx](uploads/d794ac5409e40d7beef4108e57fd2922/03.Development_Guide_v1.7.docx)
- [03.Development_Guide_v1.8.docx](uploads/691dd77566055741f89597a754cc288e/03.Development_Guide_v1.8.docx)
- [03.Development_Guide_v1.8.1.docx](uploads/fa53b9752b2ea18111827dbdd4ab52e1/03.Development_Guide_v1.8.1.docx)
- [03.Development_Guide_v1.9.docx](uploads/ebf58065622137ea90890040d416f881/03.Development_Guide_v1.9.docx)
- [04.1.Management_Functional_Design_v1.1.docx](uploads/948e2980d8e24e429fa87bc130ce81ba/04.1.Management_Functional_Design_v1.1.docx)
- [Neptune_Licence_Issuer\_-\_Functional_Design_Document_v1.0.docx](uploads/dbce4920c6713dff2b1d8f73280ae0ff/Neptune_Licence_Issuer\_-\_Functional_Design_Document_v1.0.docx)
- [5.0_FSD\_-\_Neptune_Portal_App.docx](uploads/d83a4e7b17c1c44ba449aeb6a00e6a44/5.0_FSD\_-\_Neptune_Portal_App.docx)
- [Spec_của_voucher.docx](uploads/c947be7c1900e17f77c5e23d452452c6/Spec_c%E1%BB%A7a_voucher.docx)
- [Spec_của_voucher_v1.1.docx](uploads/061f0ffea2c915d8fefd23fa15f70082/Spec_c%E1%BB%A7a_voucher_v1.1.docx)
- [Spec_của_voucher_v1.2.docx](uploads/69db575d15d18c172b45955285f98f8e/Spec_c%E1%BB%A7a_voucher_v1.2.docx)

</details>

