# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/Tariff​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_SEARCH_TARIFF`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Tariff code | tariff_code | `Number` |  |  |
| 2 | Tariff name | tariff_name | String |  |  |
| 3 | Condition | tariff_condition | String |  |  |
| 4 | Status | tariff_status | String |  |  |
| 5 | Tariff id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/Tariff​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_ADSEARCH_TARIFF`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Tariff code | tariff_code | No | `Number` |  |  |  |
| 2 | Tariff code from | tariff_code_from | No | `Number` |  |  |  |
| 3 | Tariff code to | tariff_code_to | No | `Number` |  |  |  |
| 4 | Tariff name | tariff_name | No | String |  |  |  |
| 5 | Condition | tariff_condition | No | String |  |  |  |
| 6 | Status | tariff_status | No | String |  |  |  |
| 7 | Page index | page_index | No | `Number` |  |  |  |
| 8 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Tariff code | tariff_code | `Number` |  |  |
| 2 | Tariff name | tariff_name | String |  |  |
| 3 | Condition | tariff_condition | String |  |  |
| 4 | Status | tariff_status | String |  |  |
| 5 | Tariff id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Tariff​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_INSERT_TARIFF`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Tariff code | tariff_code | No | `Number` |  |  | tự động gen |
| 2 | Tariff name | tariff_name | `Yes` | String |  |  |  |
| 3 | Tariff condition | tariff_condition | No | String |  |  |  |
| 4 | Description | tariff_description | No | String |  |  |  |
| 5 | Tariff status | tariff_status | `Yes` | String |  |  |  |
| 6 | IFC data | ifc_list | No | Array Object |  |  |  |
|  | IFC code |  | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tariff id | id | `Number` |  |  |
| 2 | Tariff code | tariff_code | `Number` |  |  |
| 3 | Tariff name | tariff_name | String |  |  |
| 4 | Tariff condition | tariff_condition | String |  |  |
| 5 | Description | tariff_description | String |  |  |
| 6 | Tariff status | tariff_status | String |  |  |
| 7 | IFC data | ifc_list | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  | lấy giá trị từ bảng d_ifctariff tương ứng tariff_code |
|  | IFC name | ifc_name | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | IFC value | ifc_value | `Number` |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | IFC type | ifc_type | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Tenor | ifc_tenor | `Number` |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Tenor unit | ifc_tenor_unit | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Active condition | ifc_condition | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Status | ifc_status | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/Tariff​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_VIEW_TARIFF`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Tariff id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 4
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tariff id | id | `Number` |  |  |
| 2 | Tariff code | tariff_code | `Number` |  |  |
| 3 | Tariff name | tariff_name | String |  |  |
| 4 | Tariff condition | tariff_condition | String |  |  |
| 5 | Description | tariff_description | String |  |  |
| 6 | Tariff status | tariff_status | String |  |  |
| 7 | IFC data | ifc_list | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  | lấy giá trị từ bảng d_ifctariff tương ứng tariff_code |
|  | IFC name | ifc_name | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | IFC value | ifc_value | `Number` |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | IFC type | ifc_type | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Tenor | ifc_tenor | `Number` |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Tenor unit | ifc_tenor_unit | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Active condition | ifc_condition | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Status | ifc_status | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Tariff​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_UPDATE_TARIFF`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Tariff id | id | `Yes` | `Number` |  |  | không được sửa |
| 2 | Tariff name | tariff_name | `Yes` | String |  |  |  |
| 3 | Tariff condition | tariff_condition | No | String |  |  |  |
| 4 | Description | tariff_description | No | String |  |  |  |
| 5 | Tariff status | tariff_status | `Yes` | String |  |  |  |
| 6 | IFC data | ifc_list | No | Array Object |  |  |  |
|  | IFC code |  | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tariff id | id | `Number` |  |  |
| 2 | Tariff code | tariff_code | `Number` |  |  |
| 3 | Tariff name | tariff_name | String |  |  |
| 4 | Tariff condition | tariff_condition | String |  |  |
| 5 | Description | tariff_description | String |  |  |
| 6 | Tariff status | tariff_status | String |  |  |
| 7 | IFC data | ifc_list | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  | lấy giá trị từ bảng d_ifctariff tương ứng tariff_code |
|  | IFC name | ifc_name | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | IFC value | ifc_value | `Number` |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | IFC type | ifc_type | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Tenor | ifc_tenor | `Number` |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Tenor unit | ifc_tenor_unit | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Active condition | ifc_condition | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
|  | Status | ifc_status | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/Tariff​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_DELETE_TARIFF`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Tariff id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 4
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tariff id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 4
}
```

## 6.2 Transaction flow
- Chỉ được xóa các tariff không có ifc đi theo và không được dùng trong các thông tin của account và catalog các module liên quan.

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list all IFC
- [IFC lookup trong "Rulefunc"](Specification/Common/01 Rulefunc)

# 8. Get list IFC by tariff code
## 8.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_GET_LIST_IFC_BY_TARIFF`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Tariff id | id | `Yes` | `Number` |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | IFC name | ifc_name | String |  |  |
|  | IFC value | ifc_value | `Number` |  |  |
|  | IFC type | ifc_type | String |  |  |
|  | Tenor | ifc_tenor | `Number` |  |  |
|  | Tenor unit | ifc_tenor_unit | String |  |  |
|  | Active condition | ifc_condition | String |  |  |
|  | Status | ifc_status | String |  |  |

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |