# 1. Search common
## 1.1 Field description
### Request message
**Workflow id:** `IFC_SEARCH_IFC`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC id | id | `Number` |  |  |
| 2 | IFC code | ifc_code | `Number` |  |  |
| 3 | IFC name | ifc_name | String |  |  |
| 4 | Value type | value_type | String |  |  |
| 5 | IFC type | ifc_type | String |  |  |
| 6 | Value | ifc_value | `Number` |  |  |
| 7 | Tenor | ifc_tenor | `Number` |  |  |
| 8 | Tenor unit | ifc_tenor_unit | String |  |  |
| 9 | Active condition | ifc_condition | String |  |  |
| 10 | Status | ifc_status | String |  |  |

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**Workflow id:** `IFC_ADSEARCH_IFC`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | IFC code from | ifc_code_from | No | `Number` |  |  |  |
| 2 | IFC code to | ifc_code_to | No | `Number` |  |  |  |
| 3 | IFC name | ifc_name | No | String |  |  |  |
| 4 | Value type | value_type | No | String |  |  |  |
| 5 | IFC type | ifc_type | No | String |  |  |  |
| 6 | Value from | ifc_value_from | No | `Number` |  |  | số có hai số thập phân |
| 7 | Value to | ifc_value_to | No | `Number` |  |  | số có hai số thập phân |
| 8 | Tenor from | ifc_tenor_from | No | `Number` |  |  | Số nguyên dương |
| 9 | Tenor to | ifc_tenor_to | No | `Number` |  |  | Số nguyên dương |
| 10 | Tenor unit | ifc_tenor_unit | No | String |  |  |  |
| 11 | Active condition | ifc_condition | No | String |  |  |  |
| 12 | Status | ifc_status | No | String |  |  |  |
| 13 | Page index | page_index | No | `Number` |  |  |  |
| 14 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC id | id | `Number` |  |  |
| 2 | IFC code | ifc_code | `Number` |  |  |
| 3 | IFC name | ifc_name | String |  |  |
| 4 | Value type | value_type | String |  |  |
| 5 | IFC type | ifc_type | String |  |  |
| 6 | Value | ifc_value | `Number` |  |  |
| 7 | Tenor | ifc_tenor | `Number` |  |  |
| 8 | Tenor unit | ifc_tenor_unit | String |  |  |
| 9 | Active condition | ifc_condition | String |  |  |
| 10 | Status | ifc_status | String |  |  |

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**Workflow id:** `IFC_INSERT_IFC`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | IFC name | ifc_name | `Yes` | String |  |  |  |
| 2 | IFC type | ifc_type | `Yes` | String |  |  |  |
| 3 | IFC sub type | ifc_sub_type | `Yes` | String |  |  |  |
| 4 | Value base | value_base | `Yes` | String |  |  |  |
| 5 | Is linked | is_linked | `Yes` | String |  |  |  |
| 6 | Value | ifc_value | `Yes` | `Number` |  |  | số có 5 số thập phân |
| 7 | IFC linkage | ifc_linkage | No | `Number` |  | `null` |  |
| 8 | IFC operator | ifc_operator | `Yes` | String |  | + |  |
| 9 | Margin value | margin_value | `Yes` | `Number` |  |  | số có 5 số thập phân |
| 10 | Value type | value_type | `Yes` | String |  |  |  |
| 11 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 12 | Floor value | floor_value | `Yes` | `Number` |  |  | số có 5 số thập phân |
| 13 | Ceiling value | ceiling_value | `Yes` | `Number` |  |  | số có 5 số thập phân |
| 14 | Value basic | value_basic | No | String |  |  |  |
| 15 | Tenor | ifc_tenor | `Yes` | `Number` |  | 0 | số nguyên dương |
| 16 | Tenor unit | ifc_tenor_unit | `Yes` | String |  |  |  |
| 17 | Active condition | ifc_condition | No | String |  |  |  |
| 18 | Rounding rule | rounding_rule | `Yes` | String |  |  |  |
| 19 | Rounding basis | rounding_basis | No | String |  |  |  |
| 20 | Rounding num | rounding_num | `Yes` | `Number` |  |  |  |
| 21 | Share fee | share_fee | No | `Number` |  |  | 0: No, 1: Yes |
| 22 | IFC status | ifc_status | `Yes` | String |  |  |  |
| 23 | Effect date | effect_value_date | No | `Date time` |  |  |  |
| 24 | Effect value | effect_value | No | `Number` |  |  | số có 5 số thập phân |
| 25 | Group id | group_id | No | `Number` |  |  |  |
| 26 | Created by | user_created | `Yes` | `Number` |  |  |  |
| 27 | Approve by | user_approved | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | IFC id | id | `Number` |  |  |
| 2 | IFC code | ifc_code | `Number` |  | tự động gen, không được nhập |
| 3 | IFC name | ifc_name | String |  |  |
| 4 | IFC type | ifc_type | String |  |  |
| 5 | IFC sub type | ifc_sub_type | String |  |  |
| 6 | Value base | value_base | String |  |  |
| 7 | Is linked | is_linked | String |  |  |
| 8 | Value | ifc_value | `Number` |  | số có 5 số thập phân |
| 9 | IFC linkage | ifc_linkage | `Number` |  |  |
| 10 | IFC operator | ifc_operator | String |  |  |
| 11 | Margin value | margin_value | `Number` |  | số có 5 số thập phân |
| 12 | Value type | value_type | String |  |  |
| 13 | Currency code | currency_code | String | 3 |  |
| 14 | Floor value | floor_value | `Number` |  | số có 5 số thập phân |
| 15 | Ceiling value | ceiling_value | `Number` |  | số có 5 số thập phân |
| 16 | Value basic | value_basic | String |  |  |
| 17 | Tenor | ifc_tenor | `Number` |  | số nguyên dương |
| 18 | Tenor unit | ifc_tenor_unit | String |  |  |
| 19 | Active condition | ifc_condition | String |  |  |
| 20 | Rounding rule | rounding_rule | String |  |  |
| 21 | Rounding basis | rounding_basis | String |  |  |
| 22 | Rounding num | rounding_num | `Number` |  |  |
| 23 | Share fee | share_fee | `Number` |  |  |
| 24 | IFC status | ifc_status | String |  |  |
| 25 | Created by | user_created | `Number` |  |  |
| 26 | Approved by | user_approved | `Number` |  |  |
| 27 | Effect date | effect_value_date | `Date time` |  |  |
| 28 | Effect value | effect_value | `Number` |  | số có 5 số thập phân |
| 29 | Group id | group_id | `Number` |  |  |
|  | Is net basic | isnet | String |  | không dùng, mặc định: null, Y: calculate on net amount, N: calculate on entire amount |
|  | REF name | refname | String |  | không dùng, mặc định: null, Function name or procedure name |
|  | Is leave | isleave | String |  | không dùng, mặc định: N, Leave item or item base on leave items (Y/N) |
|  | Is book | isbook | String |  | không dùng, mặc định: N, N: applied for IFCTYPE = I; T: ties to account; F: Follow calendar |
|  | Book tday | booktday | String |  | không dùng, mặc định: null, applied when ISBOOK = F,D: Daily; BOM: begin of month; EOM: End of month; BOW, EOW, BOQ, EOQ, BOH, EOH, BOY, EOY, 1..31: effect on a specified day,MON, TUE, WED, THU, FRI, SAT, SUN |
|  | Book tenor | book_tenor | `Number` |  | không dùng, mặc định: 0, Applied when ISBOOK = N, T. Tenor for posting (can using data e.g. 1.5 quarter, 2 month,…) |
|  | Book tenor unit | book_tenor_unit | String |  | không dùng, mặc định: M, Applied when ISBOOK = N, T. Tenor unit for posting: D: day, M: Month, Y: year, W: week, Q: Quarter, … |
|  | Pay rate | payrate | `Number` |  | không dùng, mặc định: 100, Payment rate of sending part. Effect only VALTYPE=C |

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**Workflow id:** `IFC_VIEW_IFC`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | IFC id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | IFC id | id | `Number` |  |  |
| 2 | IFC code | ifc_code | `Number` |  |  |
| 3 | IFC name | ifc_name | String |  |  |
| 4 | IFC type | ifc_type | String |  |  |
| 5 | IFC sub type | ifc_sub_type | String |  |  |
| 6 | Value base | value_base | String |  |  |
| 7 | Is linked | is_linked | String |  |  |
| 8 | Value | ifc_value | `Number` |  | số có 5 số thập phân |
| 9 | IFC linkage | ifc_linkage | `Number` |  |  |
| 10 | IFC operator | ifc_operator | String |  |  |
| 11 | Margin value | margin_value | `Number` |  | số có 5 số thập phân |
| 12 | Value type | value_type | String |  |  |
| 13 | Currency code | currency_code | String | 3 |  |
| 14 | Floor value | floor_value | `Number` |  | số có 5 số thập phân |
| 15 | Ceiling value | ceiling_value | `Number` |  | số có 5 số thập phân |
| 16 | Value basic | value_basic | String |  |  |
| 17 | Tenor | ifc_tenor | `Number` |  | số nguyên dương |
| 18 | Tenor unit | ifc_tenor_unit | String |  |  |
| 19 | Active condition | ifc_condition | String |  |  |
| 20 | Rounding rule | rounding_rule | String |  |  |
| 21 | Rounding basis | rounding_basis | String |  |  |
| 22 | Rounding num | rounding_num | `Number` |  |  |
| 23 | Share fee | share_fee | `Number` |  |  |
| 24 | IFC status | ifc_status | String |  |  |
| 25 | Created by | user_created | `Number` |  |  |
| 26 | Approved by | user_approved | `Number` |  |  |
| 27 | Effect date | effect_value_date | `Date time` |  |  |
| 28 | Effect value | effect_value | `Number` |  | số có 5 số thập phân |
| 29 | Group id | group_id | `Number` |  |  |
|  | Is net basic | isnet | String |  | không dùng, mặc định: null, Y: calculate on net amount, N: calculate on entire amount |
|  | REF name | refname | String |  | không dùng, mặc định: null, Function name or procedure name |
|  | Is leave | isleave | String |  | không dùng, mặc định: N, Leave item or item base on leave items (Y/N) |
|  | Is book | isbook | String |  | không dùng, mặc định: N, N: applied for IFCTYPE = I; T: ties to account; F: Follow calendar |
|  | Book tday | booktday | String |  | không dùng, mặc định: null, applied when ISBOOK = F,D: Daily; BOM: begin of month; EOM: End of month; BOW, EOW, BOQ, EOQ, BOH, EOH, BOY, EOY, 1..31: effect on a specified day,MON, TUE, WED, THU, FRI, SAT, SUN |
|  | Book tenor | book_tenor | `Number` |  | không dùng, mặc định: 0, Applied when ISBOOK = N, T. Tenor for posting (can using data e.g. 1.5 quarter, 2 month,…) |
|  | Book tenor unit | book_tenor_unit | String |  | không dùng, mặc định: M, Applied when ISBOOK = N, T. Tenor unit for posting: D: day, M: Month, Y: year, W: week, Q: Quarter, … |
|  | Pay rate | payrate | `Number` |  | không dùng, mặc định: 100, Payment rate of sending part. Effect only VALTYPE=C |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**Workflow id:** `IFC_UPDATE_IFC`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | IFC id | id | `Yes` | `Number` |  |  | không được sửa |
| 2 | IFC name | ifc_name | `Yes` | String |  |  |  |
| 3 | IFC sub type | ifc_sub_type | `Yes` | String |  |  |  |
| 4 | Value base | value_base | `Yes` | String |  |  |  |
| 5 | Is linked | is_linked | `Yes` | String |  |  |  |
| 6 | Value | ifc_value | `Yes` | `Number` |  |  | số có 5 số thập phân |
| 7 | IFC linkage | ifc_linkage | No | `Number` |  |  |  |
| 8 | IFC operator | ifc_operator | `Yes` | String |  | + |  |
| 9 | Margin value | margin_value | `Yes` | `Number` |  |  | số có 5 số thập phân |
| 10 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 11 | Floor value | floor_value | `Yes` | `Number` |  |  | số có 5 số thập phân |
| 12 | Ceiling value | ceiling_value | `Yes` | `Number` |  |  | số có 5 số thập phân |
| 13 | Value basic | value_basic | No | String |  |  |  |
| 14 | Tenor | ifc_tenor | `Yes` | `Number` |  | 0 | số nguyên dương |
| 15 | Tenor unit | ifc_tenor_unit | `Yes` | String |  |  |  |
| 16 | Active condition | ifc_condition | No | String |  |  |  |
| 17 | Rounding rule | rounding_rule | `Yes` | String |  |  |  |
| 18 | Rounding basis | rounding_basis | No | String |  |  |  |
| 19 | Rounding num | rounding_num | `Yes` | `Number` |  |  |  |
| 20 | Share fee | share_fee | No | `Number` |  |  |  |
| 21 | IFC status | ifc_status | `Yes` | String |  |  |  |
| 22 | Effect date | effect_value_date | No | `Date time` |  |  |  |
| 23 | Effect value | effect_value | No | `Number` |  |  | số có 5 số thập phân |
| 24 | Group id | group_id | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | IFC id | id | `Number` |  |  |
| 2 | IFC code | ifc_code | `Number` |  |  |
| 3 | IFC name | ifc_name | String |  |  |
| 4 | IFC type | ifc_type | String |  |  |
| 5 | IFC sub type | ifc_sub_type | String |  |  |
| 6 | Value base | value_base | String |  |  |
| 7 | Is linked | is_linked | String |  |  |
| 8 | Value | ifc_value | `Number` |  | số có 5 số thập phân |
| 9 | IFC linkage | ifc_linkage | `Number` |  |  |
| 10 | IFC operator | ifc_operator | String |  |  |
| 11 | Margin value | margin_value | `Number` |  | số có 5 số thập phân |
| 12 | Value type | value_type | String |  |  |
| 13 | Currency code | currency_code | String | 3 |  |
| 14 | Floor value | floor_value | `Number` |  | số có 5 số thập phân |
| 15 | Ceiling value | ceiling_value | `Number` |  | số có 5 số thập phân |
| 16 | Value basic | value_basic | String |  |  |
| 17 | Tenor | ifc_tenor | `Number` |  | số nguyên dương |
| 18 | Tenor unit | ifc_tenor_unit | String |  |  |
| 19 | Active condition | ifc_condition | String |  |  |
| 20 | Rounding rule | rounding_rule | String |  |  |
| 21 | Rounding basis | rounding_basis | String |  |  |
| 22 | Rounding num | rounding_num | `Number` |  |  |
| 23 | Share fee | share_fee | `Number` |  |  |
| 24 | IFC status | ifc_status | String |  |  |
| 25 | Created by | user_created | `Number` |  |  |
| 26 | Approved by | user_approved | `Number` |  |  |
| 27 | Effect date | effect_value_date | `Date time` |  |  |
| 28 | Effect value | effect_value | `Number` |  | số có 5 số thập phân |
| 29 | Group id | group_id | `Number` |  |  |
|  | Is net basic | isnet | String |  | không dùng, mặc định: null, Y: calculate on net amount, N: calculate on entire amount |
|  | REF name | refname | String |  | không dùng, mặc định: null, Function name or procedure name |
|  | Is leave | isleave | String |  | không dùng, mặc định: N, Leave item or item base on leave items (Y/N) |
|  | Is book | isbook | String |  | không dùng, mặc định: N, N: applied for IFCTYPE = I; T: ties to account; F: Follow calendar |
|  | Book tday | booktday | String |  | không dùng, mặc định: null, applied when ISBOOK = F,D: Daily; BOM: begin of month; EOM: End of month; BOW, EOW, BOQ, EOQ, BOH, EOH, BOY, EOY, 1..31: effect on a specified day,MON, TUE, WED, THU, FRI, SAT, SUN |
|  | Book tenor | book_tenor | `Number` |  | không dùng, mặc định: 0, Applied when ISBOOK = N, T. Tenor for posting (can using data e.g. 1.5 quarter, 2 month,…) |
|  | Book tenor unit | book_tenor_unit | String |  | không dùng, mặc định: M, Applied when ISBOOK = N, T. Tenor unit for posting: D: day, M: Month, Y: year, W: week, Q: Quarter, … |
|  | Pay rate | payrate | `Number` |  | không dùng, mặc định: 100, Payment rate of sending part. Effect only VALTYPE=C |

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**Workflow id:** `IFC_DELETE_IFC`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | IFC id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | IFC id | id | `Number` |  |  |

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list IFC status "Normal"
- **Điều kiện:** "ifc_status": "N"
- [IFC lookup trong "Rulefunc"](Specification/Common/01 Rulefunc)

# 8. Get list accounting group for fee
- **Điều kiện:** "module": "IFC"
- [Accounting group trong "Rulefunc"](Specification/Common/01 Rulefunc)

# 9. Search IFC evaluation
## 9.1 Field description
### Request message
**Workflow id:** `IFC_SEARCH_IFCEVALUATION`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Type | type | String |  |  |
| 2 | Evaluation status | evaluation_status | String |  |  |
| 3 | Item | item | String |  |  |
| 4 | Caption | caption | String |  |  |
| 5 | Example | example | String |  |  |
| 6 | IFC evaluation id | id | `Number` |  |  |

## 9.2 Transaction flow
Trả về danh sách các IFC evaluation:
- Type: DE: Deposit, CE: Credit,... Lấy trong CodeList `code_name=EVALUATIONTYPE, code_group=IFC`
- Evaluation status: A: Active, B: Block. Lấy trong CodeList `code_name=EVALUATIONSTATUS, code_group=IFC`
- Item: Tên trường thông tin có thể dùng, hàm (C#, Core) có thể sử dụng
- Caption: Diễn giải ý nghĩa của trường thông tin có thể dùng, hàm (C#, Core) có thể sử dụng
- Example: Ví dụ sử dụng trường thông tin có thể dùng, hàm (C#, Core) có thể sử dụng

# 10. Search IFC evaluation
## 10.1 Field description
### Request message
**Workflow id:** `IFC_EVALUATION_GETALL_ACTIVE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Results | items | Array Object |  |  |
| 1 | Type | type | String |  |  |
| 2 | Evaluation status | evaluation_status | String |  |  |
| 3 | Item | item | String |  |  |
| 4 | Caption | caption | String |  |  |
| 5 | Example | example | String |  |  |
| 6 | IFC evaluation id | id | `Number` |  |  |

## 10.2 Transaction flow
Chỉ trả về danh sách các IFC evaluation có Evaluation status=A (Active)
- Type: DE: Deposit, CE: Credit,... Lấy trong CodeList `code_name=EVALUATIONTYPE, code_group=IFC`
- Evaluation status: A: Active, B: Block. Lấy trong CodeList `code_name=EVALUATIONSTATUS, code_group=IFC`
- Item: Tên trường thông tin có thể dùng, hàm (C#, Core) có thể sử dụng
- Caption: Diễn giải ý nghĩa của trường thông tin có thể dùng, hàm (C#, Core) có thể sử dụng
- Example: Ví dụ sử dụng trường thông tin có thể dùng, hàm (C#, Core) có thể sử dụng

# 11. Test IFC evaluation
## 11.1 Field description
### Request message
**Workflow id:** `IFC_EXPRESSION_TEST`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Expression | expression | `Yes` | String |  |  |  |
| 2 | Module code | module_code | No | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Result | result | Object |  |  |
| 2 | Expression | expression | String |  |  |
| 3 | Module code | module_code | String |  |  |
| 4 | Account number | account_number | String |  |  |

## 11.2 Transaction flow
Trả về kết quả của trường, hàm (C#, Core):
- Expression: Tên trường thông tin có thể dùng, hàm (C#, Core) có thể sử dụng.
- Module code: Trong trường hợp `Expression` truyền vào là tên trường thông tin có thể dùng hoặc hàm của Core, thì cần chỉ rõ `Module code` sẽ được gọi `CRD` (Credit) hoặc `DPT` (Deposit).
- Result: Trả về kết quả của trường thông tin có thể dùng, hàm (C#, Core) có thể sử dụng.
- Ví dụ: 
  - expression=Math.Pow(2, 2) => result=4.0
  - expression=CreditLimit, module=CRD => result=0.00
  - expression=CreditLimit>0, module=CRD => result=true