# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/IFCAutoFee​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_SEARCH_IFCAUTOFEE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Transaction code | trans_code | String |  |  |
| 2 | Transaction name | transaction_name | String |  | lấy giá trị từ bảng s_txdef tương ứng txcode |
| 3 | IFC code | ifc_code | `Number` |  |  |
| 4 | IFC name | ifc_name | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
| 5 | Id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/IFCAutoFee​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_ADSEARCH_IFCAUTOFEE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction code | trans_code | No | String |  |  |  |
| 2 | Transaction name | transaction_name | No | String |  |  | lấy giá trị từ bảng s_txdef tương ứng txcode |
| 3 | IFC code | ifc_code | No | `Number` |  |  |  |
| 4 | IFC name | ifc_name | No | String |  |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
| 5 | Page index | page_index | No | `Number` |  |  |  |
| 6 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Transaction code | trans_code | String |  |  |
| 2 | Transaction name | transaction_name | String |  | lấy giá trị từ bảng s_txdef tương ứng txcode |
| 3 | IFC code | ifc_code | `Number` |  |  |
| 4 | IFC name | ifc_name | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
| 5 | Id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/IFCAutoFee​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_INSERT_IFCAUTOFEE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction code | trans_code | `Yes` | String |  |  |  |
| 2 | IFC code | ifc_code | `Yes` | `Number` |  |  |  |
| 3 | Active | inuse | `Yes` | `Number` |  |  | 1: check, 0: uncheck |
| 4 | Exchange | exchange | `Yes` | `Number` |  |  | 1: check, 0: uncheck |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Id | id | `Number` |  |  |
| 2 | Transaction code | trans_code | String |  |  |
| 3 | Transaction name | transaction_name | String |  | lấy giá trị từ bảng s_txdef tương ứng txcode |
| 4 | IFC code | ifc_code | `Number` |  |  |
| 5 | IFC name | ifc_name | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
| 6 | Active | inuse | `Number` |  | 1: check, 0: uncheck |
| 7 | Exchange | exchange | `Number` |  | 1: check, 0: uncheck |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/IFCAutoFee​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_VIEW_IFCAUTOFEE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 3
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Id | id | `Number` |  |  |
| 2 | Transaction code | trans_code | String |  |  |
| 3 | Transaction name | transaction_name | String |  | lấy giá trị từ bảng s_txdef tương ứng txcode |
| 4 | IFC code | ifc_code | `Number` |  |  |
| 5 | IFC name | ifc_name | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
| 6 | Active | inuse | `Number` |  | 1: check, 0: uncheck |
| 7 | Exchange | exchange | `Number` |  | 1: check, 0: uncheck |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/IFCAutoFee​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_UPDATE_IFCAUTOFEE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Id | id | `Yes` | `Number` |  |  |  |
| 2 | Active | inuse | `Yes` | `Number` |  |  | 1: check, 0: uncheck |
| 3 | Exchange | exchange | `Yes` | `Number` |  |  | 1: check, 0: uncheck |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Id | id | `Number` |  |  |
| 2 | Transaction code | trans_code | String |  |  |
| 3 | Transaction name | transaction_name | String |  | lấy giá trị từ bảng s_txdef tương ứng txcode |
| 4 | IFC code | ifc_code | `Number` |  |  |
| 5 | IFC name | ifc_name | String |  | lấy giá trị từ bảng d_ifclst tương ứng ifc_code |
| 6 | Active | inuse | `Number` |  | 1: check, 0: uncheck |
| 7 | Exchange | exchange | `Number` |  | 1: check, 0: uncheck |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/IFCAutoFee​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_DELETE_IFCAUTOFEE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 3
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 3
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list IFC custom fee
- "ifc_status": "N" and "ifc_type": "C"
- [IFC lookup trong "Rulefunc"](Specification/Common/01 Rulefunc)

# 8. Get all active transaction
## 8.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/IFCAutoFee​/GetAllActiveTransaction`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_GETALLACTIVETRANSACTION`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Results | items | Array Object |  |  |
| 1 | Transaction code | id | String |  |  |
| 2 | Transaction name | name | String |  |  |

**Example:**
```json
{
    
}
```

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |