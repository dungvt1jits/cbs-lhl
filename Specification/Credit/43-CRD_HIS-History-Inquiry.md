# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_HIS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | account_number | `Yes` | String | 25 |  |  | PCACC |
| 2 | From date | from_date | `Yes` | `Date time` |  | Working date |  | FROMDT |
| 3 | To date | to_date | `Yes` | `Date time` |  | Working date |  | TODT |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | account_number | String |  |  |
| 3 | From date | from_date | `Date time` |  |  |
| 4 | To date | to_date | `Date time` |  |  |
| 5 | Transaction date | transaction_date | `Date time` |  |  |
| 6 | User id | user_id | `Number` |  |  |
| 7 | Transaction status | status | String |  |  |
| 8 | Transaction history data | list_credit_history | Array Object |  |  |
|  | Transaction code | transaction_code | String |  |  |
|  | Transaction number | transaction_number | String |  |  |
|  | Transaction date | transaction_date | `Date time` |  |  |
|  | Created by | user_created | String |  | trả về code của cột UserId từ Admin |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Description | description | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists 
- Detail period of history.
- Allow see backward 366 days or more. It depends on setup parameter (CRD_HIS_DAYS).

**Flow of events:**
- Get back one table data with information in detail below: "Transaction code", "Transaction number", "Transaction date", "Created by", "Amount", "Description".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- This transaction is not allowed to reverse.

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | account_number | String |  |  |
| 3 | From date | from_datetime | `Date time` |  |  |
| 4 | To date | to_datetime | `Date time` |  |  |
| 5 | Transaction date | transaction_date | `Date time` |  |  |
| 6 | User id | user_id | String |  |  |
| 7 | Transaction status | status | String |  |  |
| 8 | Transaction history data | list_credit_history | Array Object |  |  |
|  | Transaction code |  | String |  |  |
|  | Transaction number |  | String |  |  |
|  | Transaction date |  | `Date time` |  |  |
|  | Created by |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Description |  | String |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_INFOMATION | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>