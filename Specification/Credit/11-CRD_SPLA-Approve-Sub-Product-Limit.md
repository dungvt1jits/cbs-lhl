# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_SPLA`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Sub product limit code | sub_product_limit_code | `Yes` | String |  |  |  | SPLCD |
| 2 | Sub product limit name | sub_product_limit_name | `Yes` | String | 250 |  |  | SPLNM |
| 3 | Customer type | customer_type | `Yes` | String | 1 |  |  | CCTMT |
| 4 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 5 | Reference id | reference_number | No | String |  |  |  | CRNUM |
| 6 | Product limit code | product_limit_code | `Yes` | String |  |  |  | PLCD |
| 7 | Currency | currency_code | `Yes` | String | 3 |  |  | CACCCR |
| 8 | Credit limit | credit_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | TXAMT |
| 9 | Status | product_limit_status | No | String | 1 |  |  | CSTS |
| 10 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 11 | Exchange rate | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 12 | Amount | amount | No | `Number` |  | 0 | trường ẩn | CCVT |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Sub product limit code | sub_product_limit_code | String |  |  |
| 3 | Sub product limit name | sub_product_limit_name | String |  |  |
| 4 | Customer type | customer_type | String |  |  |
| 5 | Customer code | customer_code | String | 8 |  |
| 6 | Reference id | reference_number | String |  |  |
| 7 | Product limit code | product_limit_code | String |  |  |
| 8 | Currency | currency_code | String | 3 |  |
| 9 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 10 | Status | product_limit_status | String |  |  |
| 11 | Description | description | String |  |  |
| 12 | Exchange rate | exchange_rate | `Number` |  |  |
| 13 | Amount | amount | `Number` |  |  |
| 14 | Transaction date | transaction_code | `Date time` |  |  |
| 15 | User id | user_id | String |  |  |
| 16 | Transaction status | status | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Sub product limit code: exists with status "Pending to approve".

**Flow of events:**
- System will change sub product limit status to "Normal".
- User approved: cập nhật user thực hiện approve

**Database:**
- d_crdspl
- d_crdsplhst
- d_crdspltran
- d_txinv
- d_crdspl_sp

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    - Sub product limit status to "Pending to approve".

**Database:**
- d_crdspl
- d_crdsplhst
- d_crdspltran
- d_txinv
- d_crdspl_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Sub product limit code |  | String |  |  |  |
| 3 | Sub product limit name |  | String |  |  |  |
| 4 | Customer type |  | String |  |  |  |
| 5 | Customer code |  | String | 8 |  |  |
| 6 | Reference id |  | String |  |  |  |
| 7 | Product limit code |  | String |  |  |  |
| 8 | Currency |  | String | 3 |  |  |
| 9 | Credit limit |  | `Number` |  |  | số có hai số thập phân |
| 10 | Status |  | String |  |  |  |
| 11 | Description |  | String |  |  |  |
| 12 | Transaction date |  | `Date time` |  |  |  |
| 13 | User id |  | String |  |  |  |
| 14 | Transaction status |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_crdspl
- d_crdsplhst
- d_crdspltran
- d_txinv
- d_crdspl_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_SPL | CRD_GET_INFO_SPL | [CRD_GET_INFO_SPL](Specification/Common/13 Credit Rulefuncs) |
| 2 | GET_INFO_PL | CRD_GET_INFO_PL | [CRD_GET_INFO_PL](Specification/Common/13 Credit Rulefuncs) |
| 3 | GET_INFO_ACNAME | CTM_GET_INFO_FULLNAME | [CTM_GET_INFO_FULLNAME](Specification/Common/12 Customer Rulefuncs) |
| 4 | GET_INFO_CBKEXR | FX_RULEFUNC_GET_INFO_CBKEXR | [FX_RULEFUNC_GET_INFO_CBKEXR](Specification/Common/20 FX Rulefuncs) |