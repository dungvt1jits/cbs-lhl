# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_RID`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account_number | `Yes` | String | 25 |  |  | PDACC |
| 2 | Deposit account number | deposit_account_number | `Yes` | String | 25 |  |  | PDOACC |
| 3 | Interest amount | interest_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | TXAMT |
| 4 | Description | description | `Yes` | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account_number | String |  |  |
| 3 | Deposit account number | deposit_account_number | String |  |  |
| 4 | Interest amount | interest_amount | `Number` |  | số có hai số thập phân |
| 5 | Description | description | String |  |  |
| 8 | Transaction status | status | String |  |  |
| 9 | Transaction date | transaction_date | `Date time` |  |  |
| 10 | User id | user_id | `Number` |  |  |
|  | Step code: `ACT_EXECUTE_POSTING` |  |  |  |  |
| 11 | Posting data | entry_journals | JSON Object |  |  |
|  | Group | accounting_entry_group | `Number` |  | số nguyên dương |
|  | Index in group | accounting_entry_index | `Number` |  | số nguyên dương |
|  | Posting side | debit_or_credit | String |  | D or C |
|  | Account number | bank_account_number | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Currency | currency_code | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Exist linkage between credit and deposit.
- Currency of credit account and deposit account must be same.
- Deposit account is type: current or saving, and status <> "Close".
- Credit has interest due or principal due or both of them.
- Deposit has balance.

**Flow of events:**
- Decrease amount of deposit account. Amount is balance but not include amount is blocked (hold).
- Firstly, collect amount to pay for interest due and then pay for principal due.
- If deposit account doesn’t have balance, system will skip collection auto.
- If deposit account has tiny amount and not enough. System will collect all amount that it has.
- Reverse provision principal if has collection principal.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

- Credit account status <> "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Interest collect | Currency of credit account |
| 1 | C | INTEREST {K=0,1,2,3,4} | Interest collect | Currency of credit account |
| 2 | D | PAID_INTEREST_S | Interest collect | Currency of credit account |
| 2 | C | PAID_INTEREST {K=2,3,4} | Interest collect | Currency of credit account |

{K=0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss}

- Credit account status is "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Interest collect | Currency of credit account |
| 1 | C | BAD_DEBT_INTEREST_WO | Interest collect | Currency of credit account |
| 2 | D | WOFF_PAID_INTEREST | Interest collect | Currency of credit account |
| 2 | C | WRITEOFF_INTEREST | Interest collect | Currency of credit account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |