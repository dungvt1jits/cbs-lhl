# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_CIPM`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Interest due amount | interest_due_amount | No | `Number` |  | 0 | số có hai số thập phân | INTDUE |
| 3 | Accrued interest amount | accrued_interest_amount | No | `Number` |  | 0 | số có hai số thập phân | INTAMT |
| 4 | Interest receivable amount | interest_receivable_amount | No | `Number` |  | 0 | số có hai số thập phân | IPBL |
| 5 | Total interest | total_interest_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | TT_I |
| 6 | Interest collect | interest_collect | `Yes` | `Number` |  | 0 | số có hai số thập phân | INT |
| 7 | Total principal amount | total_principal_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | TT_P |
| 8 | Principal due amount | principal_due_amount | No | `Number` |  | 0 | số có hai số thập phân | RAMT |
| 9 | Principal collect | principal_collect | `Yes` | `Number` |  | 0 | số có hai số thập phân | PRIN |
| 10 | Advance repayment principal mode | advance_repayment_principal_mode | No | String | 1 |  |  | ARMODE |
| 11 | Payee name | payee_name | `Yes` | String | 250 |  |  | CACNM |
| 12 | Payee code | payee_code | No | String | 15 |  |  | CCTMCD |
| 13 | Payee address | payee_address | No | String | 250 |  |  | CCTMA |
| 14 | Payee description | payee_description | No | JSON Object |  |  |  | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 15 | Value date | value_date | `Yes` | `Date time` |  | Working date |  | CVLDT |
| 16 | On balance sheet interest | on_balance_sheet_interest | No | `Number` |  | 0 | số có hai số thập phân | ONINT |
| 17 | Off balance sheet interest | off_balance_sheet_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | OFFINT |
| 18 | Pay for on-balance-sheet interest | pay_for_on_balance_sheet_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | CONINT |
| 19 | Pay for off-balance-sheet interest | pay_for_off_balance_sheet_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | COFFINT |
| 20 | Remaining provision amount | remaining_provision_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | PPAMT |
| 21 | Description | description | No | String | 250 |  |  | DESCS |
| 22 | Late payment amount | late_payment_amount | `Yes` | `Number` |  | 0 | trường ẩn | CLAMT |
| 23 | INTAMT | c_accrued_interest_amount | No | `Number` |  | 0 | trường ẩn | CINT, `intamt` đổi thành `c_accrued_interest_amount` |
| 24 | IPBL | c_interest_receivable_amount | No | `Number` |  | 0 | trường ẩn | CIPBL, `ipbl` đổi thành `c_interest_receivable_amount` |
| 25 | Pay for Interest due | pay_for_interest_due | No | `Number` |  | 0 | trường ẩn | CIDUE |
| 26 | Keep in interest prepaid | keep_in_interest_prepaid | No | `Number` |  | 0 | trường ẩn | CIPRE |
| 27 | Normal amount | normal_amount | No | `Number` |  | 0 | trường ẩn | CAMT |
| 28 | NPL amount | npl_amount | No | `Number` |  | 0 | trường ẩn | CAMT1 |
| 29 | Doubtful amount | doubtful_amount | No | `Number` |  | 0 | trường ẩn | CAMT2 |
| 30 | Overdue amount | overdue_amount | No | `Number` |  | 0 | trường ẩn | CAMT3 |
| 31 | Loss amount | loss_amount | No | `Number` |  | 0 | trường ẩn | CAMT4 |
| 32 | Repidtype | repidtype | No | String | 5 |  | trường ẩn | CREPIDTYPE |
| 33 | Sum amount | sum_amount | No | `Number` |  | 0 | trường ẩn | AMT |
| 34 | Currency of credit account | currency_of_credit_account | No | String | 3 |  | trường ẩn | CCCR |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Interest due amount | interest_due_amount | `Number` |  | số có hai số thập phân |
| 4 | Accrued interest amount | accrued_interest_amount | `Number` |  | số có hai số thập phân |
| 5 | Interest receivable amount | interest_receivable_amount | `Number` |  | số có hai số thập phân |
| 6 | Total interest | total_interest_amount | `Number` |  | số có hai số thập phân |
| 7 | Interest collect | interest_collect | `Number` |  | số có hai số thập phân |
| 8 | Total principal amount | total_principal_amount | `Number` |  | số có hai số thập phân |
| 9 | Principal due amount | principal_due_amount | `Number` |  | số có hai số thập phân |
| 10 | Principal collect | principal_collect | `Number` |  | số có hai số thập phân |
| 11 | Advance repayment principal mode | advance_repayment_principal_mode | String |  |  |
| 12 | Payee name | payee_name | String |  |  |
| 13 | Payee code | payee_code | String |  |  |
| 14 | Payee address | payee_address | String |  |  |
| 15 | Payee description | payee_description | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
| 16 | Value date | value_date | `Date time` |  |  |
| 17 | On balance sheet interest | on_balance_sheet_interest | `Number` |  | số có hai số thập phân |
| 18 | Off balance sheet interest | off_balance_sheet_interest | `Number` |  | số có hai số thập phân |
| 19 | Pay for on-balance-sheet interest | pay_for_on_balance_sheet_interest | `Number` |  | số có hai số thập phân |
| 20 | Pay for off-balance-sheet interest | pay_for_off_balance_sheet_interest | `Number` |  | số có hai số thập phân |
| 21 | Remaining provision amount | remaining_provision_amount | `Number` |  | số có hai số thập phân |
| 22 | Description | description | String |  |  |
| 23 | Late payment amount | late_payment_amount | `Number` |  |  |
| 24 | INTAMT | c_accrued_interest_amount | `Number` |  | `intamt` đổi thành `c_accrued_interest_amount` |
| 25 | IPBL | c_interest_receivable_amount | `Number` |  | `ipbl` đổi thành `c_interest_receivable_amount` |
| 26 | Pay for Interest due | pay_for_interest_due | `Number` |  |  |
| 27 | Keep in interest prepaid | keep_in_interest_prepaid | `Number` |  |  |
| 28 | Normal amount | normal_amount | `Number` |  |  |
| 29 | NPL amount | npl_amount | `Number` |  |  |
| 30 | Doubtful amount | doubtful_amount | `Number` |  |  |
| 31 | Overdue amount | overdue_amount | `Number` |  |  |
| 32 | Loss amount | loss_amount | `Number` |  |  |
| 33 | Repidtype | repidtype | String |  |  |
| 34 | Sum amount | sum_amount | `Number` |  |  |
| 35 | Currency of credit account | currency_of_credit_account | String | 3 |  |
| 36 | Transaction status | status | String |  |  |
| 37 | Transaction date |  | `Date time` |  |  |
| 38 | User id |  | `Number` |  |  |
| 39 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists with status "Normal".

**Flow of events:**
- Cash at counter of user will increase ([refer transaction flow: cash](Specification/Common/07 Transaction Flow Cash)).
- Collect principal and interest ([refer transaction flow: Credit](Specification/Common/05 Transaction Flow Credit)).

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>
- Credit account status <> “Write-off”

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASH | Principal collect | Currency of credit account |
| 1 | C | CREDIT {K=0,1,2,3,4} | Principal collect | Currency of credit account |
| 2 | D | CASH | Interest collect | Currency of credit account |
| 2 | C | INTEREST {K=0,1,2,3,4} | Interest collect | Currency of credit account |
| 3 | D | PROV_P_DR_RVS {K=0,1,2,3,4} | Reverse provision = Principal collect * Provision rate | Currency of credit account |
| 3 | C | PROV_P_CR_RVS {K=0,1,2,3,4} | Reverse provision = Principal collect * Provision rate | Currency of credit account |
| 4 | D | PAID_INTEREST_S | Interest collect | Currency of credit account |
| 4 | C | PAID_INTEREST {K=2,3,4} | Interest collect | Currency of credit account |
| Product limit = Shared |  |  |  |  |
| 5 | D | COMMITMENT_LIMIT | Principal collect * FX rate (Book rate) | Product limit's currency |
| 5 | C | COMMITMENT_REL | Principal collect * FX rate (Book rate) | Product limit's currency |

{K=0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss}

- Credit account status is “Write-off”

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASH | Principal collect | Currency of credit account |
| 1 | C | BAD_DEBT_PRINCIPLE | Principal collect | Currency of credit account |
| 2 | D | CASH | Interest collect | Currency of credit account |
| 2 | C | BAD_DEBT_INTEREST_WO | Interest collect | Currency of credit account |
| 3 | D | WOFF_PAID_LOAN | Principal collect | Currency of credit account |
| 3 | C | WRITEOFF_LOAN | Principal collect | Currency of credit account |
| 4 | D | WOFF_PAID_INTEREST | Interest collect | Currency of credit account |
| 4 | C | WRITEOFF_INTEREST | Interest collect | Currency of credit account |

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_crdbschd
- d_crdbschd_sp
- d_crschd
- d_crschd_sp
- d_ifcbal
- d_ifcbaldtl

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_crdbschd
- d_crdbschd_sp
- d_crschd
- d_crschd_sp
- d_ifcbal
- d_ifcbaldtl

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |  |  |
| 2 | GET_INFO_BCY_CACNM | CRD_GET_INFO_CRDACC | [CRD_GET_INFO_CRDACC](Specification/Common/13 Credit Rulefuncs) | `account_number` = `credit_account` | `currency_code` = `currency_of_credit_account`, `full_name` = `payee_name`, `address` = `payee_address`, `customer_code` = `payee_code`, `paper_type` = `repidtype`, `interest_due` = `late_payment_amount`, `balance` = `total_principal_amount`, `balance` = `principal_collect`, `total_interest` = `total_interest_amount`, `total_pay` = `interest_collect`, `interest_receivable` = `interest_receivable_amount`, `interest_amount` = `accrued_interest_amount`, `interest_due` = `interest_due_amount`, `on_balance_sheet_interest` = `on_balance_sheet_interest`, `off_balance_sheet_interest` = `off_balance_sheet_interest`, `amount` = `sum_amount`, `due_principal` = `principal_due_amount` |
| 3 | GET_INFO_INTEREST | CRD_GET_INFO_INTEREST | [CRD_GET_INFO_INTEREST](Specification/Common/13 Credit Rulefuncs) | `account_number` = `credit_account`, `interest_amount` = `interest_collect` | `interest_amount` = `c_accrued_interest_amount`, `interest_receivable` = `c_interest_receivable_amount`, `interest_due` = `pay_for_interest_due`, `interest_prepaid` = `keep_in_interest_prepaid`, `pay_on_balance_sheet_interest` = `pay_for_on_balance_sheet_interest`, `pay_off_balance_sheet_interest` = `pay_for_off_balance_sheet_interest` |
| 4 | GET_INFO_PRINCIPAL | CRD_GET_INFO_PRINCIPAL | [CRD_GET_INFO_PRINCIPAL](Specification/Common/13 Credit Rulefuncs) | `account_number` = `credit_account` | `provision_principal` = `remaining_provision_amount` |
| 5 | GET_INFO_PPAMT | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |  |  |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>