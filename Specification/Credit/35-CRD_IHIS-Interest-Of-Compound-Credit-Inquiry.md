# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_IHIS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | account_number | `Yes` | String | 25 |  |  | PCACC
| 2 | Due date | due_date | `Yes` | `Date time` |  | Working date |  | TODT |
| 3 | Description | description | `Yes` | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | account_number | String |  |  |
| 3 | Due date | due_date | `Date time` |  |  |
| 4 | Description | description | String |  |  |
| 5 | Transaction date | transaction_date | `Date time` |  |  |
| 6 | User id | user_id | String |  |  |
| 7 | Transaction status | status | String |  |  |
| 8 | Interest history data | list_credit_history | Array Object |  |  |
|  | From date | from_date | `Date time` |  |  |
|  | To date | to_date | `Date time` |  |  |
|  | Type | interest_type | String |  |  |
|  | Outstanding balance | out_balance | `Number` |  | số có hai số thập phân |
|  | Interest rate | rate | `Number` |  | số có hai số thập phân |
|  | Interest amount | amount | `Number` |  | số có hai số thập phân |
|  | Generate date | modify_date | `Date time` |  |  |

## 1.2 Transaction flow
**Conditions:**
- Interest computation mode is "Compound basic of installment" (C) or "Payment + Interest equally for compound" (P).

**Flow of events:**
- Get back one table data with information in detail below: From date, to date, Type, Outstanding balance, Interest rate, Interest amount, Generate date.

**Database:**
- d_crdhst
- d_crdtran
- d_txinv

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |
| 2 | Transaction status |  | String |  |  |
| 3 | User approve |  | String |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- This transaction is not allowed to reverse.

**Database:**
- d_crdhst
- d_crdtran
- d_txinv

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | account_number | String |  |  |
| 3 | Due date | due_date | `Date time` |  |  |
| 4 | Description | description | String |  |  |
| 5 | Transaction date | transaction_date | `Date time` |  |  |
| 6 | User id | user_id | String |  |  |
| 7 | Transaction status | status | String |  |  |
| 8 | Interest history data | list_credit_history | Array Object |  |  |
|  | From date | from_date | `Date time` |  |  |
|  | To date | to_date | `Date time` |  |  |
|  | Type | interest_type | String |  |  |
|  | Outstanding balance | out_balance | `Number` |  | số có hai số thập phân |
|  | Interest rate | rate | `Number` |  | số có hai số thập phân |
|  | Interest amount | amount | `Number` |  | số có hai số thập phân |
|  | Generate date | modify_date | `Date time` |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_crdhst
- d_crdtran
- d_txinv

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
- None

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>