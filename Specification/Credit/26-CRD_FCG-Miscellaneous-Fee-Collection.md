# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_FCG`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account_number | `Yes` | String | 25 |  |  | PCACC |
| 2 | GL account number | gl_account_number | `Yes` | String | 25 |  |  | PGACC |
| 3 | Total fee | total_fee | `Yes` | `Number` |  | > 0 | số có hai số thập phân | TXAMT |
| 4 | Amount for fee calculation | amount_for_fee_calculation | `Yes` | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 5 | Description | description | No | String | 250 |  |  | DESCS |
| 6 | Customer code | customer_code | `Yes` | String | 15 |  | trường ẩn | CCTMCD |
| 7 | Cash currency | cash_currency | `Yes` | String | 3 |  | trường ẩn | CCCR |
| 8 | Fee data | fee_data | `Yes` | JSON Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account_number | String |  |  |
| 3 | GL account number | gl_account_number | String |  |  |
| 4 | Total fee | total_fee | `Number` |  | số có hai số thập phân |
| 5 | Amount for fee calculation | amount_for_fee_calculation | `Number` |  | số có hai số thập phân |
| 6 | Description | description | String |  |  |
| 7 | Customer code | customer_code | String |  |  |
| 8 | Cash currency | cash_currency | String |  |  |
| 9 | Debit account name | debit_account_name | String |  |  |
| 10 | Transaction status | status | String |  |  |
| 11 | Transaction date |  | `Date time` |  |  |
| 12 | User id |  | `Number` |  |  |
| 13 | Fee data | fee_data | JSON Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 14 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Fee amount collection must be larger than zero.
- Credit account number: exists with status "Normal".
- GL account number: exists and same branch with user login and allow debit side.
- Currency of GL must be same with currency of credit account.
- Currency of Fee must be same with currency of credit account.

**Flow of events:**
- Currency of GL account and credit account must be same.
- Have two types fee as: flat amount and percentage.
    - Case fee is flat amount, allow user enter fee amount.
    - Case fee is percentage, allow user enter amount and percentage. 
System calculation fee amount = Amount * percentage.
- A transaction can collect two type fees at same time.
- Transaction complete: GL account debit side ([refer transaction flow: GL](Specification/Common/08 Transaction Flow GL))

**Database:**
- d_crdhst
- d_crdtran
- d_txinv

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | GL | Total fee amount | Currency of credit account |
| 1 | C | IFCC (income) | Total fee amount | Currency of credit account |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account_number | String |  |  |
| 3 | GL account number | gl_account_number | String |  |  |
| 4 | Total fee | total_fee | `Number` |  | số có hai số thập phân |
| 5 | Amount for fee calculation | amount_for_fee_calculation | `Number` |  | số có hai số thập phân |
| 6 | Description | description | String |  |  |
| 7 | Customer code | customer_code | String |  |  |
| 8 | Cash currency | cash_currency | String |  |  |
| 9 | Debit account name | debit_account_name | String |  |  |
| 10 | Transaction status | status | String |  |  |
| 11 | Transaction date |  | `Date time` |  |  |
| 12 | User id |  | `Number` |  |  |
| 13 | Fee data |  | JSON Object |  |  |
|  | IFC code |  | `Number` |  |  |
|  | Value type |  | String |  |  |
|  | Value |  | `Number` |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  | số có hai số thập phân |
|  | Floor |  | `Number` |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |
| 14 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_BCY_CACNM | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |
| 3 | GET_INFO_BCY_ACT | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 4 | LKP_DATA_BACNO | ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY | [ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY](Specification/Common/11 Accounting Rulefuncs) |
| 5 | TRAN_FEE_CRD_FCG/GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 6 | TRAN_FEE/GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 7 | TRAN_FEE_CRD_FCG/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) truyền `currency_code` là giá trị của `cash_currency` |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>