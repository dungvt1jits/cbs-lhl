# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_PLA`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Product limit code | product_limit_code | `Yes` | String |  |  |  | PLCD |
| 2 | Product limit name | product_limit_name | `Yes` | String | 250 |  |  | PLNM |
| 3 | Customer type | customer_type | `Yes` | String | 1 |  |  | CCTMT |
| 4 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 5 | Reference id | reference_number | No | String |  |  |  | CRNUM |
| 6 | Currency | currency_code | `Yes` | String | 3 |  |  | CACCCR |
| 7 | Credit limit | credit_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | TXAMT |
| 8 | Limit type | limit_type | `Yes` | String | 1 |  |  | LMTYPE |
| 9 | Status | product_status | No | String | 1 |  |  | CSTS |
| 10 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 11 | Customer name | customer_name | No | String | 250 |  | trường ẩn | ACNM |
| 12 | Exchange rate | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 13 | Amount | amount | No | `Number` |  | 0 | trường ẩn | CCVT |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Product limit code | product_limit_code | String |  |  |
| 3 | Product limit name | product_limit_name | String |  |  |
| 4 | Customer type | customer_type | String |  |  |
| 5 | Customer code | customer_code | String | 8 |  |
| 6 | Reference id | reference_number | String |  |  |
| 7 | Currency | currency_code | String | 3 |  |
| 8 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 9 | Limit type | limit_type | String |  |  |
| 10 | Status | product_status | String |  |  |
| 11 | Description | description | String |  |  |
| 12 | Customer name | customer_name | String |  |  |
| 13 | Exchange rate | exchange_rate | `NUmber` |  |  |
| 14 | Amount | amount | `NUmber` |  |  |
| 15 | Transaction date | transaction_date | `Date time` |  |  |
| 16 | User id | user_id | `NUmber` |  |  |
| 17 | Transaction status | status | String |  |  |
| 18 | Posting data |  | JSON Object |  |  |
|  | Group |  | String |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Product limit code: exists with status "Pending to approve".
- Check securing asset collateral:
    - Minimum secured amount = Minimum secured rate * Credit limit.
    - Minimum secured rate is defined on Product limit information. Allow adjust by manual.

**Flow of events:**
- System will change product limit status to "Normal".
- User approved: cập nhật user thực hiện approve

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | COMMITMENT_LIMIT | Limit amount | Currency of product limit |
| 1 | C | COMMITMENT_REL | Limit amount | Currency of product limit |

**Voucher:**
- `A3`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    - Product limit status to "Pending to approve".

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Product limit code | product_limit_code | String |  |  |
| 3 | Product limit name | product_limit_name | String |  |  |
| 4 | Customer type | customer_type | String |  |  |
| 5 | Customer code | customer_code | String | 8 |  |
| 6 | Reference id | reference_number | String |  |  |
| 7 | Currency | currency_code | String | 3 |  |
| 8 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 9 | Limit type | limit_type | String |  |  |
| 10 | Status | product_status | String |  |  |
| 11 | Description | description | String |  |  |
| 12 | Customer name | customer_name | String |  |  |
| 13 | Exchange rate | exchange_rate | `NUmber` |  |  |
| 14 | Amount | amount | `NUmber` |  |  |
| 15 | Transaction date | transaction_date | `Date time` |  |  |
| 16 | User id | user_id | `NUmber` |  |  |
| 17 | Transaction status | status | String |  |  |
| 18 | Posting data |  | JSON Object |  |  |
|  | Group |  | String |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PL | CRD_GET_INFO_PL | [CRD_GET_INFO_PL](Specification/Common/13 Credit Rulefuncs) |
| 2 | GET_INFO_CBKEXR | FX_RULEFUNC_GET_INFO_CBKEXR | [FX_RULEFUNC_GET_INFO_CBKEXR](Specification/Common/20 FX Rulefuncs) |

# 5. Signature
- None