# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_IFC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | account_number | `Yes` | String | 25 |  |  | PCACC |
| 2 | IFC code | ifc_code | `Yes` | `Number` |  |  |  | CIFCCD |
| 3 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 4 | Account name | account_name | `Yes` | String | 250 |  |  | ACNM |
| 5 | Current IFC amount | current_ifc_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | IFCAMT |
| 6 | Adjustment amount | adjustment_amount | `Yes` | `Number` |  | <> 0 | số có hai số thập phân, cho phép nhập < 0.00 và > 0.00 | TXAMT |
| 7 | IFC type | ifc_type | `Yes` | String | 1 |  |  | CTYPE |
| 8 | New IFC amount | new_ifc_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT |
| 9 | Adjusted accrual interest | adjusted_accrual_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | CINTAMT |
| 10 | Adjusted due interest | adjusted_due_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | CINTDUE |
| 11 | Adjusted payable interest | adjusted_payable_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | CINTPBL |
| 12 | Adjusted on-balance sheet interest | adjusted_on_balance_sheet_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | CONINT |
| 13 | Adjusted off-balance sheet interest | adjusted_off_balance_sheet_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | COFFINT |
| 14 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 15 | Accrual interest amount | accrual_interest_amount | `Yes` | `Number` |  | 0 | trường ẩn | IACR |
| 16 | Interest due amount | interest_due_amount | `Yes` | `Number` |  | 0 | trường ẩn | INTDUE |
| 17 | Interest repayable amount | interest_repayable_amount | `Yes` | `Number` |  | 0 | trường ẩn | IPBLE |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | account_number | String |  |  |
| 3 | IFC code | ifc_code | `Number` |  |  |
| 4 | Customer code | customer_code | String |  |  |
| 5 | Account name | account_name | String |  |  |
| 6 | Current IFC amount | current_ifc_amount | `Number` |  | số có hai số thập phân |
| 7 | Adjustment amount | adjustment_amount | `Number` |  | số có hai số thập phân |
| 8 | IFC type | ifc_type | String |  |  |
| 9 | New IFC amount | new_ifc_amount | `Number` |  | số có hai số thập phân |
| 10 | Adjusted accrual interest | adjusted_accrual_interest | `Number` |  | số có hai số thập phân |
| 11 | Adjusted due interest | adjusted_due_interest | `Number` |  | số có hai số thập phân |
| 12 | Adjusted payable interest | adjusted_payable_interest | `Number` |  | số có hai số thập phân |
| 13 | Adjusted on-balance sheet interest | adjusted_on_balance_sheet_interest | `Number` |  | số có hai số thập phân |
| 14 | Adjusted off-balance sheet interest | adjusted_off_balance_sheet_interest | `Number` |  | số có hai số thập phân |
| 15 | Description | description | String |  |  |
| 16 | Accrual interest amount | accrual_interest_amount | `Number` |  | số có hai số thập phân |
| 17 | Interest due amount | interest_due_amount | `Number` |  | số có hai số thập phân |
| 18 | Interest repayable amount | interest_repayable_amount | `Number` |  | số có hai số thập phân |
| 19 | Transaction date |  | `Date time` |  |  |
| 20 | User id |  | `Number` |  |  |
| 21 | Transaction status |  | String |  |  |
| 22 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists with status "Normal".
- "IFC code" exists with type is "Interest".
- adjustment_amount < or > 0.00
- If adjustment_amount < 0.00
  - ABS(adjustment_amount) < or = accrual_interest_amount + interest_due_amount
- new_ifc_amount > or = 0.00

**Flow of events:**
- Adjust with detail "IFC code".
- Allow adjustment increase. Adjustment amount will plus to interest accrual.
- Allow adjustment decrease. Adjustment amount will minus to interest due and then rest for interest accrual.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_ifcbal
- d_ifcbaldtl

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>
- In case adjustment increase:

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST {K=0,1} | Adjustment amount | Currency of credit account |
| 1 | C | PAID_INTEREST {K=0,1} | Adjustment amount | Currency of credit account |
| 2 | D | INTEREST {K=2,3,4} | Adjustment amount | Currency of credit account |
| 2 | C | PAID_INTEREST_S | Adjustment amount | Currency of credit account |

*{K=0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss}*

- In case adjustment decrease

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | PAID_INTEREST {K=0,1} | Adjustment amount | Currency of credit account |
| 1 | C | INTEREST {K=0,1} | Adjustment amount | Currency of credit account |
| 2 | D | PAID_INTEREST_S | Adjustment amount | Currency of credit account |
| 2 | C | INTEREST {K=2,3,4} | Adjustment amount | Currency of credit account |

*{K=0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss}*

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_ifcbal
- d_ifcbaldtl

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | account_number | String |  |  |
| 3 | IFC code | ifc_code | `Number` |  |  |
| 4 | Customer code | customer_code | String |  |  |
| 5 | Account name | account_name | String |  |  |
| 6 | Current IFC amount | current_ifc_amount | `Number` |  | số có hai số thập phân |
| 7 | Adjustment amount | adjustment_amount | `Number` |  | số có hai số thập phân |
| 8 | IFC type | ifc_type | String |  |  |
| 9 | New IFC amount | new_ifc_amount | `Number` |  | số có hai số thập phân |
| 10 | Adjusted accrual interest | adjusted_accrual_interest | `Number` |  | số có hai số thập phân |
| 11 | Adjusted due interest | adjusted_due_interest | `Number` |  | số có hai số thập phân |
| 12 | Adjusted payable interest | adjusted_payable_interest | `Number` |  | số có hai số thập phân |
| 13 | Adjusted on-balance sheet interest | adjusted_on_balance_sheet_interest | `Number` |  | số có hai số thập phân |
| 14 | Adjusted off-balance sheet interest | adjusted_off_balance_sheet_interest | `Number` |  | số có hai số thập phân |
| 15 | Description | description | String |  |  |
| 16 | Accrual interest amount | accrual_interest_amount | `Number` |  | số có hai số thập phân |
| 17 | Interest due amount | interest_due_amount | `Number` |  | số có hai số thập phân |
| 18 | Interest repayable amount | interest_repayable_amount | `Number` |  | số có hai số thập phân |
| 19 | Transaction date |  | `Date time` |  |  |
| 20 | User id |  | `Number` |  |  |
| 21 | Transaction status |  | String |  |  |
| 22 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_ifcbal
- d_ifcbaldtl

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_IFCAMT | CRD_IFC_GET_INFO_IFCAMT | [CRD_IFC_GET_INFO_IFCAMT](Specification/Common/21 IFC Rulefuncs) |
| 2 | GET_INFO_BCY_CACNM | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |
| 3 | GET_INFO_ADJUSTMENT | CRD_IFC_GET_INFO_ADJUSTMENT | [CRD_IFC_GET_INFO_ADJUSTMENT](Specification/Common/13 Credit Rulefuncs) |
| 4 | GET_IFC_TYPE | GET_IFC_TYPE | [GET_IFC_TYPE](Specification/Common/21 IFC Rulefuncs) |
| 5 | LKP_DATA_CIFCCD | IFC_LOOKUP_IFCTYPE_I_BY_CRDACNO | [IFC_LOOKUP_IFCTYPE_I_BY_CRDACNO](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>