# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_GSCH`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | account_number | `Yes` | String | 25 |  |  | PCACC |
| 2 | Customer type | customer_type | `Yes` | String | 1 |  |  | CCTMT |
| 3 | Account holder name | account_holder_name | `Yes` | String | 250 |  |  | CACNM |
| 4 | Catalogue code | catalog_code | `Yes` | String | 25 |  |  | CNCAT |
| 5 | Catalogue name | catalog_name | `Yes` | String | 100 |  |  | CCATNM |
| 6 | Credit limit | credit_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | TXAMT |
| 7 | Description | description | `Yes` | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | account_number | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Account holder name | account_holder_name | String |  |  |
| 5 | Catalogue code | catalog_code | String |  |  |
| 6 | Catalogue name | catalog_name | String |  |  |
| 7 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 8 | Description | description | String |  |  |
| 9 | Transaction date |  | `Date time` |  |  |
| 10 | User id |  | `Number` |  |  |
| 11 | Transaction status |  | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists with status "Normal".
- Enter data in the section "Payment schedule" of Credit account information:
    - From date: due date of principal
    - To date: due date of principal
    - Amount: <empty>
    - Interest rate: <empty>
    - Grace Principal amount: input amount >= 0.00 and < Principal due amount
    - From date = To date = Due date of principal and <= Working date
- Principal due amount > 0.00
- Interest computation mode is "Compound basic of installment" (C) or "Payment + Interest equally for compound" (P).

**Flow of events:**
- Interest computation mode is "Compound basic of installment" (C)
    - Principal payment: 
        - Principal due amount per tenor = "Grace principal amount" per tenor
        - Will generate schedule based on default information
    - Interest payment: Will generate schedule based on default information
- Interest computation mode is "Payment + Interest equally for compound" (P)
    - Principal payment:
        - Principal due amount per tenor = "Grace principal amount" per tenor
        - Will generate schedule based on default information
    - Interest payment: Will generate schedule based on default information
- Due amount = Sum (Grace Principal amount)

**Database:**
- d_crdhst
- d_crdtran
- d_txinv
- d_crschd
- d_crschd_sp
- d_crpmschd
- d_crpmschd_his
- d_crpmschd_sp
- d_crpmschd_his_sp

**Voucher:**
- `A2`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_crdhst
- d_crdtran
- d_txinv
- d_crschd
- d_crschd_sp
- d_crpmschd
- d_crpmschd_his
- d_crpmschd_sp
- d_crpmschd_his_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | account_number | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Account holder name | account_holder_name | String |  |  |
| 5 | Catalogue code | catalog_code | String |  |  |
| 6 | Catalogue name | catalog_name | String |  |  |
| 7 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 8 | Description | description | String |  |  |
| 9 | Transaction date |  | `Date time` |  |  |
| 10 | User id |  | `Number` |  |  |
| 11 | Transaction status |  | String |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_crdhst
- d_crdtran
- d_txinv
- d_crschd
- d_crschd_sp
- d_crpmschd
- d_crpmschd_his
- d_crpmschd_sp
- d_crpmschd_his_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CACNM1 | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |
| 2 | GET_INFO_CCTMT | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>