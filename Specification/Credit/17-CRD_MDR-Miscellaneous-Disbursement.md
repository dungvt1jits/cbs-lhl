# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_MDR`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Disbursement amount | disbursement_amount | `Yes` | `Number` |  | > 0 | số có hai số thập phân | PGAMT |
| 3 | Accounting account | accounting_account | `Yes` | String | 25 |  |  | PGACC |
| 4 | Receiver name | receiver_name | `Yes` | String | 250 |  |  | CACNM |
| 5 | Receiver code | receiver_code | `Yes` | String | 15 |  |  | CCTMCD |
| 6 | Receiver address | receiver_address | No | String | 250 |  |  | CCTMA |
| 7 | Receiver description | receiver_description | No | JSON Object |  |  |  | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 8 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 9 | Remaining provision amount | remaining_provision_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | PPAMT |
| 10 | Currency | currency_code | No | String | 3 |  | trường ẩn | PGCCR |
| 11 | Cross rate | cross_rate | `Yes` | `Number` |  | 0 | trường ẩn | CCRRATE |
| 12 | Accounting amount | accounting_amount | `Yes` | `Number` |  | 0 | trường ẩn | CTCVT |
| 13 | Exchange rate /BCY | exchange_rate | No | `Number` |  | 0 | trường ẩn | PGEXR |
| 14 | Amount to GL /BCY | amount_to_gl | No | `Number` |  | 0 | trường ẩn | PGCVT |
| 15 | Values date | values_date | No | `Date time` |  | Working date | trường ẩn | CVLDT |
| 16 | Currency of credit account | currency_of_credit_account | No | String | 3 |  | trường ẩn | CCCR |
| 17 | Amount (Debit account/BCY) | amount | No | `Number` |  | 0 | trường ẩn | CCVT |
| 18 | Exchange rate (Debit account/BCY) | exchange_rate_debit | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 19 | Repidtype | repidtype | No | String | 1 |  | trường ẩn | CREPIDTYPE |
| 20 | Total principal amount | total_principal_amount | `Yes` | `Number` |  | 0 | trường ẩn | TT_P |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Disbursement amount | disbursement_amount | `Number` |  | số có hai số thập phân |
| 4 | Accounting account | accounting_account | String |  |  |
| 5 | Receiver name | receiver_name | String |  |  |
| 6 | Receiver code | receiver_code | String |  |  |
| 7 | Receiver address | receiver_address | String |  |  |
| 8 | Receiver description | receiver_description | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
| 9 | Description | description | String |  |  |
| 10 | Remaining provision amount | remaining_provision_amount | `Number` |  | số có hai số thập phân |
| 11 | Currency | currency_code | No | String |  |  |
| 12 | Cross rate | cross_rate | `Yes` | `Number` |  |  |
| 13 | Accounting amount | accounting_amount | `Yes` | `Number` |  |  |
| 14 | Exchange rate /BCY | exchange_rate | No | `Number` |  |  |
| 15 | Amount to GL /BCY | amount_to_gl | No | `Number` |  |  |
| 16 | Values date | values_date | No | `Date time` |  |  |
| 17 | Currency of credit account | currency_of_credit_account | No | String |  |  |
| 18 | Amount (Debit account/BCY) | amount | No | `Number` |  |  |
| 19 | Exchange rate (Debit account/BCY) | exchange_rate_debit | No | `Number` |  |  |
| 20 | Repidtype | repidtype | No | String |  |  |
| 21 | Total principal amount | total_principal_amount | `Yes` | `Number` |  |  |
| 22 | Transaction date |  | `Date time` |  |  |
| 23 | User id |  | `Number` |  |  |
| 24 | Transaction status |  | String |  |  |
| 25 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Account number: exists with status "Normal"
- GL account: exists and same branch with user. 
- GL allows credit side.
- Currency of GL must be same with currency of credit account.
- {Total amount was disbursed of account} + {amount is going disburse} < or = credit limit.
- {Total amount was disbursed of all accounts belong sub product} + {amount is going disburse} < or = sub product limit. 
- Date disbursement is not smaller than begin contract date or not larger than ending contract date.
- All thought checking secured asset collateral at step approve. But so, allow adjustment (increase) credit limit of account.
- System will continue checking when disburse mode <> Non secured (N):
    - Minimum secured amount = Minimum secured rate * Credit limit.

**Flow of events:**
- Credit GL ([refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)) 
- And Loan's outstanding will increase ([refer transaction flow: Credit](Specification/Common/05 Transaction Flow Credit)).

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>
- Credit account status <> "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CREDIT | Disburse amount | Currency of credit account |
| 1 | C | GL | Disburse amount | Currency of credit account |
| 2 | D | COMMITMENT_REL | Disburse amount * FX rate (Book rate) | Product limit's currency |
| 2 | C | COMMITMENT_LIMIT | Disburse amount * FX rate (Book rate) | Product limit's currency |
| 3 | D | PROV_P_DR | Disburse amount * Provision rate | Currency of credit account |
| 3 | C | PROV_P_CR | Disburse amount * Provision rate | Currency of credit account |

- Credit account status is "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | BAD_DEBT_LOSS | Disburse amount | Currency of credit account |
| 1 | C | GL | Disburse amount | Currency of credit account |
| 2 | D | WRITEOFF_LOAN | Disburse amount | Currency of credit account |
| 2 | C | WOFF_PAID_LOAN | Disburse amount | Currency of credit account |

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_crdbschd
- d_crdbschd_sp
- d_crschd
- d_crschd_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_crdbschd
- d_crdbschd_sp
- d_crschd
- d_crschd_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_INFO_BCY_CACNM | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |  |  |
| 2 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |  |  |
| 3 | GET_INFO_CCRRATE | FX_RULEFUNC_GET_INFO_CRATE_TB_TA | [FX_RULEFUNC_GET_INFO_CRATE_TB_TA](Specification/Common/20 FX Rulefuncs) | `currency_code01` = `currency_of_credit_account`, `currency_code02` = `currency_code` | `cross_rate` = `cross_rate` |
| 4 | GET_INFO_PPAMT | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |  |  |
| 5 | GET_INFO_CBKEXR | FX_RULEFUNC_GET_INFO_CBKEXR | [FX_RULEFUNC_GET_INFO_CBKEXR](Specification/Common/20 FX Rulefuncs) | `currency_code` = `currency_of_credit_account` | `bk_rate_currency` = `exchange_rate_debit` |
| 6 | GET_INFO_CCRCD | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |  |  |
| 7 | GET_INFO_PCSEXR | FX_RULEFUNC_GET_INFO_PCSEXR | [FX_RULEFUNC_GET_INFO_PCSEXR](Specification/Common/20 FX Rulefuncs) | `currency_code` = `currency_of_credit_account` | `rate_currency01` = `exchange_rate` |
| 8 | GET_INFO_CCRRATE1 | FX_RULEFUNC_GET_INFO_CCRRATE_TB_TA | [FX_RULEFUNC_GET_INFO_CCRRATE_TB_TA](Specification/Common/20 FX Rulefuncs) | `currency_code01` = `currency_of_credit_account`, `currency_code02` = `currency_code`, `cross_rate` = `cross_rate` | `cross_rate` = `cross_rate` |
| 9 | LKP_DATA_BACNO | ACT_ACCHRT_LOOKUP_BY_BRANCHID | [ACT_ACCHRT_LOOKUP_BY_BRANCHID](Specification/Common/11 Accounting Rulefuncs) |  |  |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>