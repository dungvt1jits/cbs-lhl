# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_BLK`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Customer name | customer_name | `Yes` | String | 250 |  |  | CACNM |
| 3 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 4 | Customer address | customer_address | No | String | 250 |  |  | CCTMA |
| 5 | Customer description | customer_description | No | JSON Object |  |  |  | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 6 | Description | description | No | String | 250 |  |  | DESCS | 
| 7 | Value date | value_date | No | `Date time` |  | Woking date | trường ẩn | CVLDT |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Customer name | customer_name | String |  |  |
| 4 | Customer code | customer_code | String |  |  |
| 5 | Customer address | customer_address | String |  |  |
| 6 | Customer description | customer_description | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
| 7 | Description | description | String |  |  |
| 8 | Transaction date | transaction_date | `Date time` |  |  |
| 9 | Transaction date | transaction_date | `Date time` |  |  |
| 10 | User id | user_id | String |  |  |
| 11 | Transaction status | status | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists with status "Normal".

**Flow of events:**
- When complete transaction: credit account status is "Block" and system still calculate interest accrual or change classification.
- Don't allow to continue disbursement.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    - Credit account number status to "Normal".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Credit account number |  | String |  |  |  |
| 3 | Customer name |  | String |  |  |  |
| 4 | Customer code |  | String |  |  |  |
| 5 | Customer address |  | String |  |  |  |
| 6 | Customer description |  | JSON Object |  |  |  |
|  | Home |  | String |  |  |  |
|  | Office |  | String |  |  |  |
| 7 | Description |  | String |  |  |  |
| 8 | Transaction date |  | `Date time` |  |  |  |
| 9 | User id |  | String |  |  |  |
| 10 | Transaction status |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_CUSTOMER | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>