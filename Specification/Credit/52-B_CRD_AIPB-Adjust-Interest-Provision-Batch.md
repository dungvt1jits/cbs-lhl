# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_AIPB`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Currency | credit_account_currency | `Yes` | String | 3 |  |  | CCCR |
| 3 | Principle amount | principal_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | PRIN |
| 4 | Interest amount | payable_interest_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | CIPBL |
| 5 | Current provision amount of principal | current_provision_amount_of_principal | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_P1 |
| 6 | Current provision amount of interest | current_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_I1 |
| 7 | New provision amount of interest | new_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_I2 |
| 8 | Current group | current_group | `Yes` | `Number` |  | 0 | số nguyên dương | G1 |
| 9 | Adjust interest amount | adjust_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT |
| 10 | Off balance interest | on_balance_sheet_int_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | SPINT |
| 11 | Description | description | `Yes` | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Currency | credit_account_currency | String | 3 |  |
| 4 | Principle amount | principal_amount | `Number` |  | số có hai số thập phân |
| 5 | Interest amount | payable_interest_amount | `Number` |  | số có hai số thập phân |
| 6 | Current provision amount of principal | current_provision_amount_of_principal | `Number` |  | số có hai số thập phân |
| 7 | Current provision amount of interest | current_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 8 | New provision amount of interest | new_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 9 | Current group | current_group | `Number` |  | số nguyên dương |
| 10 | Adjust interest amount | adjust_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 11 | Off balance interest | on_balance_sheet_int_amount | `Number` |  | số có hai số thập phân |
| 12 | Description | description | String |  |  |
| 8 | Transaction status | status | String |  |  |
| 9 | Transaction date | transaction_date | `Date time` |  |  |
| 10 | User id | user_id | `Number` |  |  |
|  | Step code: `ACT_EXECUTE_POSTING` |  |  |  |  |
| 11 | Posting data | entry_journals | JSON Object |  |  |
|  | Group | accounting_entry_group | `Number` |  | số nguyên dương |
|  | Index in group | accounting_entry_index | `Number` |  | số nguyên dương |
|  | Posting side | debit_or_credit | String |  | D or C |
|  | Account number | bank_account_number | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Currency | currency_code | String |  |  |

## 1.2 Transaction flow
**Conditions:**

**Flow of events:**

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |