# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_AIPV`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Current group | current_group | `Yes` | `Number` |  | 0 |  | G1 |
| 3 | Principal amount | principal_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | PRIN |
| 4 | Payable interest amount | payable_interest_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | CIPBL |
| 5 | Current provision amount of principal | current_provision_amount_of_principal | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_P1 |
| 6 | Current provision amount of interest | current_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_I1 |
| 7 | New provision amount of interest | new_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_I2 |
| 8 | Adjust provision amount of interest | adjust_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT |
| 9 | Credit account currency | credit_account_currency | `Yes` | String | 3 |  |  | CCCR |
| 10 | On balance sheet int. amount | on_balance_sheet_int_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | SPINT |
| 11 | Description | description | No | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Current group | current_group | `Number` |  |  |
| 4 | Principal amount | principal_amount | `Number` |  | số có hai số thập phân |
| 5 | Payable interest amount | payable_interest_amount | `Number` |  | số có hai số thập phân |
| 6 | Current provision amount of principal | current_provision_amount_of_principal | `Number` |  | số có hai số thập phân |
| 7 | Current provision amount of interest | current_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 8 | New provision amount of interest | new_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 9 | Adjust provision amount of interest | adjust_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 10 | Credit account currency | credit_account_currency | String | 3 |  |
| 11 | On balance sheet int. amount | on_balance_sheet_int_amount | `Number` |  | số có hai số thập phân |
| 12 | Description | description | String |  |  |
| 13 | Transaction date |  | `Date time` |  |  |
| 14 | User id |  | String |  |  |
| 15 | Transaction status |  | String |  |  |

## 1.2 Transaction flow
**Conditions:**

**Flow of events:**

**Database:**

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Credit account number |  | String |  |  |  |
| 3 | Current group |  | String |  |  |  |
| 4 | Principal amount |  | `Number` |  |  | số có hai số thập phân |
| 5 | Payable interest amount |  | `Number` |  |  | số có hai số thập phân |
| 6 | Current provision amount of principal |  | `Number` |  |  | số có hai số thập phân |
| 7 | Current provision amount of interest |  | `Number` |  |  | số có hai số thập phân |
| 8 | New provision amount of interest |  | `Number` |  |  | số có hai số thập phân |
| 9 | Adjust provision amount of interest |  | `Number` |  |  | số có hai số thập phân |
| 10 | Credit account currency |  | String | 3 |  |  |
| 11 | On balance sheet int. amount |  | `Number` |  |  | số có hai số thập phân |
| 12 | Description |  | String |  |  |  |
| 13 | Transaction date |  | `Date time` |  |  |  |
| 14 | User id |  | String |  |  |  |
| 15 | Transaction status |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_CACNM | CRD_GET_INFO_BCY_CACNM | [CRD_GET_INFO_BCY_CACNM](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>