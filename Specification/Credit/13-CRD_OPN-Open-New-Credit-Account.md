# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_OPN`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | No | String | 25 |  |  | PCACC |
| 2 | Customer type | customer_type | `Yes` | String | 1 |  |  | CCTMT |
| 3 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 4 | Sub product limit code | sub_product_limit_code | `Yes` | String | 25 |  |  | SPL |
| 5 | Catalogue code | catalog_code | `Yes` | String | 25 |  |  | CNCAT |
| 6 | Catalogue name | catalog_name | `Yes` | String | 100 |  |  | CCATNM |
| 7 | Credit facility | credit_facility | `Yes` | String | 2 |  |  | CNSTS |
| 8 | Sub-product | sub_product | `Yes` | String | 2 |  | trường ẩn | SUBPRODUCT |
| 9 | Credit classification | credit_classification | `Yes` | String | 2 |  |  | CNSTSO |
| 10 | Seq number | seq_number | No | `Number` | 6 | 0 | cho phép `null` | CNSEQ |
| 11 | Currency code | currency_code | `Yes` | String | 3 |  |  | CACCCR |
| 12 | Account holder name | account_holder_name | `Yes` | String | 250 |  |  | CACNM |
| 13 | Maximum limit | maximum_limit | No | `Number` |  | 0 |  | PCSAMT |
| 14 | Credit limit | credit_limit | `Yes` | `Number` |  | > 0 |  | TXAMT |
| 15 | Margin | margin | `Yes` | `Number` |  | 0 | cho phép < 0, = 0 và > 0.00 | MARGIN |
| 16 | From date | from_date | `Yes` | `Date time` |  | Working date |  | FROMDT |
| 17 | To date | to_date | `Yes` | `Date time` |  | Working date |  | TODT |
| 18 | First prin. repayment date | principal_first_date | `Yes` | `Date time` |  | Working date |  | CDT1 |
| 19 | First int. repayment date | int_first_date | `Yes` | `Date time` |  | Working date |  | CDT2 |
| 20 | Amount (Debit account/BCY) | amount | No | `Number` |  | 0 | trường ẩn | CCVT |
| 21 | Exchange rate (Debit account/BCY) | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 22 | Branch cd | branch_code | `Yes` | String | 4 |  |  | CBRCDC |
| 23 | Description | description | No | String | 250 |  |  | DESCS |
| 24 | Product tenor type | product_tenor_type | `Yes` | String | 1 |  | trường ẩn | CTYPE, `Tenor type` get form catalog |
| 25 | Business Purpose Code | business_purpose_code | `Yes` | String | 7 |  |  | ISICCD |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Customer code | customer_code | String |  |  |
| 5 | Sub product limit code | sub_product_limit_code | String |  |  |
| 6 | Catalogue code | catalog_code | String |  |  |
| 7 | Catalogue name | catalog_name | String |  |  |
| 8 | Credit facility | credit_facility | String |  |  |
| 9 | Sub-product | sub_product | String |  |  |
| 10 | Credit classification | credit_classification | String |  |  |
| 11 | Seq number | seq_number | `Number` |  |  |
| 12 | Currency code | currency_code | String |  |  |
| 13 | Account holder name | account_holder_name | String |  |  |
| 14 | Maximum limit | maximum_limit | `Number` |  |  |
| 15 | Credit limit | credit_limit | `Number` |  |  |
| 16 | Margin | margin | `Number` |  |  |
| 17 | From date | from_date | Date time |  |  |
| 18 | To date | to_date | Date time |  |  |
| 19 | First prin. repayment date | principal_first_date | Date time |  |  |
| 20 | First int. repayment date | int_first_date | Date time |  |  |
| 21 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 22 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 23 | Branch cd | branch_code | String |  |  |
| 24 | Description | description | String |  |  |
| 25 | Product tenor type | product_tenor_type | String |  |  |
| 26 | Business Purpose Code | business_purpose_code | String |  |  |
| 27 | Transaction status | status | String |  |  |
| 28 | Transaction date |  | `Date time` |  |  |
| 29 | User id |  | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- Exists customer code.
- "Catalogue code" is defined.
- Have period of contract.
- Sub product limit exists with status "Normal".
- Credit limit smaller or equal sub product limit.
- If Product limit is type "Non-Shared" (F):
    - Credit Catalog with credit type = Fixed (F)
- If Product limit is type "Shared" (M):  
    - Credit Catalog with credit type = Blanket (B)

**Flow of events:**
- Account code will auto create.
- Allow creation account code by number sequence define.
- System auto converts credit line of sub product to suit with loan's currency. It is easy to compare limit. Use exchange rate is book rate.
- Prevent entering contract date is in past.
- Content of contract will copy from definition "Catalogue code".
- Allow to enter plus/minus "Margin rate".
- Allow deleting account if it has just opened.
- Allow to modify information contract after opening contract.
- When complete transaction: status is "Pending to approve".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_ifcbal

**Voucher:**
- `A2`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    - Credit account number will be deleted.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_ifcbal

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Customer code | customer_code | String |  |  |
| 5 | Sub product limit code | sub_product_limit_code | String |  |  |
| 6 | Catalogue code | catalog_code | String |  |  |
| 7 | Catalogue name | catalog_name | String |  |  |
| 8 | Credit facility | credit_facility | String |  |  |
| 9 | Sub-product | sub_product | String |  |  |
| 10 | Credit classification | credit_classification | String |  |  |
| 11 | Seq number | seq_number | `Number` |  |  |
| 12 | Currency code | currency_code | String |  |  |
| 13 | Account holder name | account_holder_name | String |  |  |
| 14 | Maximum limit | maximum_limit | `Number` |  |  |
| 15 | Credit limit | credit_limit | `Number` |  |  |
| 16 | Margin | margin | `Number` |  |  |
| 17 | From date | from_date | Date time |  |  |
| 18 | To date | to_date | Date time |  |  |
| 19 | First prin. repayment date | principal_first_date | Date time |  |  |
| 20 | First int. repayment date | int_first_date | Date time |  |  |
| 21 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 22 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 23 | Branch cd | branch_code | String |  |  |
| 24 | Description | description | String |  |  |
| 25 | Product tenor type | product_tenor_type | String |  |  |
| 26 | Business Purpose Code | business_purpose_code | String |  |  |
| 27 | Transaction status | status | String |  |  |
| 28 | Transaction date |  | `Date time` |  |  |
| 29 | User id |  | `Number` |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_ifcbal

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_CACNM | CRD_OPN_GET_INFO_CACNM | [CRD_OPN_GET_INFO_CACNM](Specification/Common/13 Credit Rulefuncs) |
| 3 | GET_INFO_SPL | CRD_OPN_GET_INFO_SPL | [CRD_OPN_GET_INFO_SPL](Specification/Common/13 Credit Rulefuncs) |
| 4 | GET_INFO_CCATNM | GET_INFO_CRDCAT | [GET_INFO_CRDCAT](Specification/Common/13 Credit Rulefuncs) |
| 5 | GET_INFO_CBKEXR | FX_RULEFUNC_GET_INFO_CBKEXR | [FX_RULEFUNC_GET_INFO_CBKEXR](Specification/Common/20 FX Rulefuncs) |
| 6 | GET_INFO_BRANCHID | GET_INFO_BRNAME | [GET_INFO_BRNAME](Specification/Common/17 Admin Rulefuncs) |
| 7 | LKP_DATA_BRANCHID | ADM_LOOKUP_BRANCH | [ADM_LOOKUP_BRANCH](Specification/Common/01 Rulefunc) |
| 8 | LKP_DATA_CCTMCD | CTM_LOOKUP_CTM_BY_CTMTYPE | [CTM_LOOKUP_CTM_BY_CTMTYPE](Specification/Common/12 Customer Rulefuncs) |
| 9 | LKP_DATA_CNCAT | CRD_LOOKUP_CRDCAT_BY_SPL | [CRD_LOOKUP_CRDCAT_BY_SPL](Specification/Common/13 Credit Rulefuncs) |
| 10 | LKP_DATA_WISICCD | ADM_LOOKUP_CODE_LIST | [ADM_LOOKUP_CODE_LIST](Specification/Common/01 Rulefunc) |
| 11 | LKP_DATA_SPLCD | CRD_LOOKUP_CRDSPL_BY_CTM | [CRD_LOOKUP_CRDSPL_BY_CTM](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>