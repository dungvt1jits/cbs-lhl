# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_NPL`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Current group | current_group | `Yes` | `Number` |  |  |  | G1 |
| 3 | New group | new_group | `Yes` | `Number` |  |  |  | G2 |
| 4 | Principal amount | principal_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | PRIN |
| 5 | Payable interest amount | payable_interest_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | CIPBL |
| 6 | Current provision amount of principal | current_provision_amount_of_principal | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_P1 |
| 7 | New provision amount of principal | new_provision_amount_of_principal | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_P2 |
| 8 | Current provision amount of interest | current_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_I1 |
| 9 | New provision amount of interest | new_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_I2 |
| 10 | Credit account currency | credit_account_currency | `Yes` | String | 3 |  |  | CCCR |
| 11 | On balance sheet int. amount | on_balance_sheet_int_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | SPINT |
| 12 | Description | description | No | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Current group | current_group | `Number` |  |  |
| 4 | New group | new_group | `Number` |  |  |
| 5 | Principal amount | principal_amount | `Number` |  | số có hai số thập phân |
| 6 | Payable interest amount | payable_interest_amount | `Number` |  | số có hai số thập phân |
| 7 | Current provision amount of principal | current_provision_amount_of_principal | `Number` |  | số có hai số thập phân |
| 8 | New provision amount of principal | new_provision_amount_of_principal | `Number` |  | số có hai số thập phân |
| 9 | Current provision amount of interest | current_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 10 | New provision amount of interest | new_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 11 | Credit account currency | credit_account_currency | String | 3 |  |
| 12 | On balance sheet int. amount | on_balance_sheet_int_amount | `Number` |  | số có hai số thập phân |
| 13 | Description | description | String |  |  |
| 14 | Transaction date |  | `Date time` |  |  |
| 15 | User id |  | String |  |  |
| 16 | Transaction status |  | String |  |  |
| 17 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists with status <> "Close", "Rejected", "Pending to approve".
- {New classification} <> {Current classification}.

**Flow of events:**
- Change manual: use transaction to change:
    - Classification = {new classification}
- Provision principal:
    - Reverse provision current.
    - Posting new provision = outstanding * provision rate principal of new classification.
    - Provision rate of new classification is setup on account information.
- Provision interest:
    - Reverse provision current.
    - Posting new provision = interest amount * provision rate interest of new classification.
    - Provision rate of new classification is setup on account information.
    - If provision rate = 0. It means: no provision.

| Group id | Group code | Group caption |
| -------- | ---------- | ------------- |
| 1 | Normal | N |
| 2 | Special mention | P |
| 3 | Substandard Debt | O |
| 4 | Doubtful Debt | D |
| 5 | Loss Debt | L |

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>
- Case 01: change from "On balance sheet" to "On balance sheet"
    - Current = {Normal} to New = {Special mention}
    - Current = {Special mention} to New = {Normal}

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CREDIT{New} | Principal amount | Currency of credit account |
| 1 | C | CREDIT{Current} | Principal amount | Currency of credit account |
| 2 | D | PROV_P_DR{New} | New provision amount of principal | Currency of credit account |
| 2 | C | PROV_P_CR{New} | New provision amount of principal | Currency of credit account |
| 3 | D | PROV_P_DR_RVS{Current} | Reverse current provision amount of principal | Currency of credit account |
| 3 | C | PROV_P_CR_RVS{Current} | Reverse current provision amount of principal | Currency of credit account |
| 4 | D | PAID_INTEREST{Current} | Interest income | Currency of credit account |
| 4 | C | PAID_INTEREST{New} | Interest income | Currency of credit account |
| 5 | D | INTEREST{New} | Interest amount | Currency of credit account |
| 5 | C | INTEREST{Current} | Interest amount | Currency of credit account |
| 6 | D | PROV_I_DR{New} | New provision amount of interest | Currency of credit account |
| 6 | C | PROV_I_CR{New} | New provision amount of interest | Currency of credit account |
| 7 | D | PROV_I_DR_RVS{Current} | Reverse current provision of interest | Currency of credit account |
| 7 | C | PROV_I_CR_RVS{Current} | Reverse current provision of interest | Currency of credit account |

- Case 02: change from "On balance sheet" to "Off balance sheet"
    - Current = {Normal} to New = {Substandard, Doubtful, Loss}
    - Current = {Special mention} to New = {Substandard, Doubtful, Loss}

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CREDIT{New} | Principal amount | Currency of credit account |
| 1 | C | CREDIT{Current} | Principal amount | Currency of credit account |
| 2 | D | PROV_P_DR{New} | New provision amount of principal | Currency of credit account |
| 2 | C | PROV_P_CR{New} | New provision amount of principal | Currency of credit account |
| 3 | D | PROV_P_DR_RVS{Current} | Reverse current provision amount of principal | Currency of credit account |
| 3 | C | PROV_P_CR_RVS{Current} | Reverse current provision amount of principal | Currency of credit account |
| 4 | D | PAID_INTEREST{Current} | Interest Income in Suspense | Currency of credit account |
| 4 | C | PAID_INTEREST_S | Interest Income in Suspense | Currency of credit account |
| 5 | D | INTEREST{New} | Interest amount | Currency of credit account |
| 5 | C | INTEREST{Current} | Interest amount | Currency of credit account |
| 6 | D | PROV_I_DR{New} | New provision amount of interest | Currency of credit account |
| 6 | C | PROV_I_CR{New} | New provision amount of interest | Currency of credit account |
| 7 | D | PROV_I_DR_RVS{Current} | Reverse current provision of interest | Currency of credit account |
| 7 | C | PROV_I_CR_RVS{Current} | Reverse current provision of interest | Currency of credit account |

- Case 03: change from "Off balance sheet" to "Off balance sheet"
    - Current = {Substandard} to New = {Doubtful, Loss}
    - Current = {Doubtful} to New = {Substandard, Loss}
    - Current = {Loss} to New = {Substandard, Doubtful}

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CREDIT{New} | Principal amount | Currency of credit account |
| 1 | C | CREDIT{Current} | Principal amount | Currency of credit account |
| 2 | D | PROV_P_DR{New} | New provision amount of principal | Currency of credit account |
| 2 | C | PROV_P_CR{New} | New provision amount of principal | Currency of credit account |
| 3 | D | PROV_P_DR_RVS{Current} | Reverse current provision amount of principal | Currency of credit account |
| 3 | C | PROV_P_CR_RVS{Current} | Reverse current provision amount of principal | Currency of credit account |
| 4 | D | PAID_INTEREST{Current} | Interest Income in Suspense | Currency of credit account |
| 4 | C | PAID_INTEREST_S | Interest Income in Suspense | Currency of credit account |
| 5 | D | INTEREST{New} | Interest amount | Currency of credit account |
| 5 | C | INTEREST{Current} | Interest amount | Currency of credit account |

- Case 04: change from "Off balance sheet" to "On balance sheet"
    - Current = {Substandard} to New = {Normal, Special mention}
    - Current = {Doubtful} to New = {Normal, Special mention}
    - Current = {Loss} to New = {Normal, Special mention}

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CREDIT{New} | Principal amount | Currency of credit account |
| 1 | C | CREDIT{Current} | Principal amount | Currency of credit account |
| 2 | D | PROV_P_DR{New} | New provision amount of principal | Currency of credit account |
| 2 | C | PROV_P_CR{New} | New provision amount of principal | Currency of credit account |
| 3 | D | PROV_P_DR_RVS{Current} | Reverse current provision amount of principal | Currency of credit account |
| 3 | C | PROV_P_CR_RVS{Current} | Reverse current provision amount of principal | Currency of credit account |
| 4 | D | PAID_INTEREST_S | Interest Income in Suspense | Currency of credit account |
| 4 | C | PAID_INTEREST{Current} | Interest Income in Suspense | Currency of credit account |
| 5 | D | INTEREST{New} | Interest amount | Currency of credit account |
| 5 | C | INTEREST{Current} | Interest amount | Currency of credit account |
| 6 | D | PROV_I_DR{New} | New provision amount of interest | Currency of credit account |
| 6 | C | PROV_I_CR{New} | New provision amount of interest | Currency of credit account |
| 7 | D | PROV_I_DR_RVS{Current} | Reverse current provision of interest | Currency of credit account |
| 7 | C | PROV_I_CR_RVS{Current} | Reverse current provision of interest | Currency of credit account |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number |credit_account  | String |  |  |
| 3 | Current group |current_group  | String |  |  |
| 4 | New group | new_group | String |  |  |
| 5 | Principal amount | principal_amount | `Number` |  | số có hai số thập phân |
| 6 | Payable interest amount | payable_interest_amount | `Number` |  | số có hai số thập phân |
| 7 | Current provision amount of principal | current_provision_amount_of_principal | `Number` |  | số có hai số thập phân |
| 8 | New provision amount of principal | new_provision_amount_of_principal | `Number` |  | số có hai số thập phân |
| 9 | Current provision amount of interest | current_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 10 | New provision amount of interest | new_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 11 | Credit account currency | credit_account_currency | String | 3 |  |
| 12 | On balance sheet int. amount | on_balance_sheet_int_amount | `Number` |  | số có hai số thập phân |
| 13 | Description | description | String |  |  |
| 14 | Transaction date |  | `Date time` |  |  |
| 15 | User id |  | String |  |  |
| 16 | Transaction status |  | String |  |  |
| 17 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_CACNM | CRD_GET_INFO_BCY_CACNM | [CRD_GET_INFO_BCY_CACNM](Specification/Common/13 Credit Rulefuncs) |
| 2 | GET_INFO_G2 | CRD_NPL_GET_INFO_G2 | [CRD_NPL_GET_INFO_G2](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>