# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_EXT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Credit limit | credit_limit | No | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 3 | Outstanding balance | oustanding_balance | No | `Number` |  | 0 | số có hai số thập phân | PGAMT |
| 4 | Old expire date | old_expire_date | No | `Date time` |  | Working date |  | CDT1 |
| 5 | New expire date | new_expire_date | `Yes` | `Date time` |  | Working date |  | CDT2 |
| 6 | Creditor name | creditor_name | No | String | 250 |  |  | CACNM |
| 7 | Creditor code | creditor_code | No | String | 15 |  |  | CCTMCD |
| 8 | Creditor address | creditor_address | No | String | 250 |  |  | CCTMA |
| 9 | Creditor description | creditor_description | No | JSON Object |  |  |  | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 10 | Description | description | No | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 4 | Outstanding balance | oustanding_balance| `Number` |  | số có hai số thập phân |
| 5 | Old expire date | old_expire_date | `Date time` |  |  |
| 6 | New expire date | new_expire_date | `Date time` |  |  |
| 7 | Creditor name | creditor_name | String |  |  |
| 8 | Creditor code | creditor_code | String |  |  |
| 9 | Creditor address | creditor_address | String |  |  |
| 10 | Creditor description | creditor_description | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
| 11 | Description | description | String |  |  |
| 12 | Transaction date |  | `Date time` |  |  |
| 13 | User id |  | `Number` |  |  |
| 14 | Transaction status | status | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists with status "Normal".
- Extend date must to be larger than end contract date current.

**Flow of events:**
- Update "End of tenor" to "New expire date".
- After extend date, allow user modify schedule principal or continue disbursement in period of contract.

**Database:**
- d_credit
  - Update "ToDate" to "New expire date".
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |
| 2 | Transaction status |  | String |  |  |
| 3 | User approve |  | String |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 4 | Outstanding balance | oustanding_balance| `Number` |  | số có hai số thập phân |
| 5 | Old expire date | old_expire_date | `Date time` |  |  |
| 6 | New expire date | new_expire_date | `Date time` |  |  |
| 7 | Creditor name | creditor_name | String |  |  |
| 8 | Creditor code | creditor_code | String |  |  |
| 9 | Creditor address | creditor_address | String |  |  |
| 10 | Creditor description | creditor_description | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
| 11 | Description | description | String |  |  |
| 12 | Transaction date |  | `Date time` |  |  |
| 13 | User id |  | String |  |  |
| 14 | Transaction status | status | String |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CACNM | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |
| 2 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>