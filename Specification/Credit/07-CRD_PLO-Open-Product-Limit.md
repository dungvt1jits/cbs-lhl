# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_PLO`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Product limit code | product_limit_code | No | String |  |  |  | PLCD |
| 2 | Product limit name | product_limit_name | `Yes` | String | 250 |  |  | PLNM |
| 3 | Customer type | customer_type | `Yes` | String | 1 |  |  | CCTMT |
| 4 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 5 | Reference id | reference_number | No | String |  |  |  | CRNUM |
| 6 | Currency | currency_code | `Yes` | String | 3 |  |  | CCCR |
| 7 | Limit amount | limit_amount | `Yes` | `Number` |  |  > 0 | số có hai số thập phân | TXAMT |
| 8 | Limit type | limit_type | `Yes` | String | 1 |  |  | LMTYPE |
| 9 | Accounting group | accounting_group | `Yes` | `Number` |  | 0 |  | ACTGRP |
| 10 | Secure type | secure_type | `Yes` | String | 1 | N |  | SECTYPE |
| 11 | Secure rate | secure_rate | No | `Number` |  | 0 | số có hai số thập phân | SECRT |
| 12 | Description | description | No | String | 250 |  |  | DESCS |
| 13 | Exchange rate | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 14 | Amount | amount | No | `Number` |  | 0 | trường ẩn | CCVT |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Product limit code | product_limit_code | String |  |  |
| 3 | Product limit name | product_limit_name | String |  |  |
| 4 | Customer type | customer_type | String |  |  |
| 5 | Customer code | customer_code | String | 8 |  |
| 6 | Reference id | reference_number | String |  |  |
| 7 | Currency | currency_code | String | 3 |  |
| 8 | Limit amount | limit_amount | `Number` |  | số có hai số thập phân |
| 9 | Limit type | limit_type | String |  |  |
| 10 | Accounting group | accounting_group | `Number` |  |  |
| 11 | Secure type | secure_type | String |  |  |
| 12 | Secure rate | secure_rate | `Number` |  | số có hai số thập phân |
| 13 | Description | description | String |  |  |
| 14 | Exchange rate | exchange_rate | `Number` |  |  |
| 15 | Amount | amount | `Number` |  |  |
| 16 | Transaction date | transaction_date | `Date time` |  |  |
| 17 | User id | user_id | `NUmber` |  |  |
| 18 | Transaction status | status | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Exists customer code with status Nomal (N).
- Total credit line of all "Product limit" < or = Customer limit.
  - Limit amount (`limit_amount`) > 0.00
  - [Limit amount (`limit_amount`) + Total Limit amount of all "Product limit" có status Normal (N) đã có trong DB] < or = Customer line tương ứng customer

**Flow of events:**
- A customer can open many "Product limit".
- Allow different currency with "Customer limit".
- Exchange rate based on book rate between currencies.
- Divide 2 types: share, non-shared
- Don’t allow "Product limit" to be larger than "Customer limit".
- Allow modify limit after opening.
- Allow modify information of "Product limit".
- Transaction complete: 
    - "Product limit" has status "Pending to approve".
    - Product limit code is created auto by system and it is unique.

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    - Product limit status to "Deleted".

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Product limit code | product_limit_code | String |  |  |
| 3 | Product limit name | product_limit_name | String |  |  |
| 4 | Customer type | customer_type | String |  |  |
| 5 | Customer code | customer_code | String | 8 |  |
| 6 | Reference id | reference_number | String |  |  |
| 7 | Currency | currency_code | String | 3 |  |
| 8 | Limit amount | limit_amount | `Number` |  | số có hai số thập phân |
| 9 | Limit type | limit_type | String |  |  |
| 10 | Accounting group | accounting_group | `Number` |  |  |
| 11 | Secure type | secure_type | String |  |  |
| 12 | Secure rate | secure_rate | `Number` |  | số có hai số thập phân |
| 13 | Description | description | String |  |  |
| 14 | Exchange rate | exchange_rate | `Number` |  |  |
| 15 | Amount | amount | `Number` |  |  |
| 16 | Transaction date | transaction_date | `Date time` |  |  |
| 17 | User id | user_id | `NUmber` |  |  |
| 18 | Transaction status | status | String |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CACNM | CTM_GET_INFO_FULLNAME | [CTM_GET_INFO_FULLNAME](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_CBKEXR | FX_RULEFUNC_GET_INFO_CBKEXR | [FX_RULEFUNC_GET_INFO_CBKEXR](Specification/Common/20 FX Rulefuncs) |
| 3 | GET_INFO_ACTGRP | ACT_ACGRPDEF_GET_INFO_ACTGRP | [ACT_ACGRPDEF_GET_INFO_ACTGRP](Specification/Common/11 Accounting Rulefuncs) |
| 4 | LKP_DATA_ACTGRP | ACT_ACGRPDEF_LOOKUP | [ACT_ACGRPDEF_LOOKUP](Specification/Common/11 Accounting Rulefuncs) |
| 5 | LKP_DATA_CTM | CTM_LOOKUP_CTM_BY_CTMTYPE | [CTM_LOOKUP_CTM_BY_CTMTYPE](Specification/Common/12 Customer Rulefuncs) |