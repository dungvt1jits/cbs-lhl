# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditGroupLimit​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_SP_CRDGRPLM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Group limit code | group_limit_code | String | 10 |  |  |
| 2 | Group limit name | group_limit_name | String |  |  |  |
| 3 | Currency code | currency_code | String | 3 |  |  |
| 4 | Limit amount | credit_limit | `Number` |  |  | số có hai số thập phân |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditGroupLimit​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_ADV_CRDGRPLM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group limit code | group_limit_code | No | String | 10 |  |  |
| 2 | Group limit name | group_limit_name | No | String |  |  |  |
| 3 | Currency code | currency_code | No | String | 3 |  |  |
| 4 | Limit amount | credit_limit | No | `Number` |  |  |  |
| 5 | Page index | page_index | No | `Number` |  |  |  |
| 6 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Group limit code | group_limit_code | String | 10 |  |  |
| 2 | Group limit name | group_limit_name | String |  |  |  |
| 3 | Currency code | currency_code | String | 3 |  |  |
| 4 | Limit amount | credit_limit | `Number` |  |  | số có hai số thập phân |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditGroupLimit​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_INSERT_CRDGRPLM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group name | group_limit_name | `Yes` | String |  |  |  |
| 2 | Currency | currency_code | `Yes` | String | 3 |  |  |
| 3 | Amount | credit_limit | `Yes` | `Number` |  |  | số có hai số thập phân |

**Example:**
```json
{

}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group code | group_limit_code | String | 10 |  |
| 2 | Group name | group_limit_name | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Amount | credit_limit | `Number` |  |  | số có hai số thập phân |
| 5 | Created by | user_created | `Number` |  | User login |
| 6 | Approved by | user_approved | `Number` |  |  |
| 7 | Group id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditGroupLimit​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_CRDGRPLM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | id | `Yes` | `Number` | 10 |  |  |

**Example:**
```json
{
    "id": 5
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group code | group_limit_code | String | 10 |  |
| 2 | Group name | group_limit_name | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Amount | credit_limit | `Number` |  | số có hai số thập phân |
| 5 | Available limit (based) |  | `Number` |  | tính theo tỉ giá và credit line của customer list đã dùng |
| 6 | Created by | user_created | `Number` |  | User login |
| 7 | Approved by | user_approved | `Number` |  |  |
| 8 | Group id | id | `Number` |  |  |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditGroupLimit​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_UPDATE_CRDGRPLM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | id | `Yes` | `Number` |  |  | không được sửa |
| 2 | Group name | group_limit_name | `Yes` | String |  |  |  |
| 3 | Currency | currency_code | `Yes` | String | 3 |  |  |
| 4 | Amount | credit_limit | `Yes` | `Number` |  |  | số có hai số thập phân |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group code | group_limit_code | String | 10 |  |
| 2 | Group name | group_limit_name | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Amount | credit_limit | `Number` |  | số có hai số thập phân |
| 5 | Available limit (based) |  | `Number` |  | tính theo tỉ giá và credit line của customer list đã dùng |
| 6 | Created by | user_created | `Number` |  | User login |
| 7 | Approved by | user_approved | `Number` |  |  |
| 8 | Group id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditGroupLimit​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_DELETE_CRDGRPLM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 5
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 8 | Group id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 5
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list "customer single và customer linkage" theo "group limit code"
## 7.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CTM_VIEW_GRP_LIMIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group limit code | group_limit_code | `Yes` | String |  |  | GroupId trong bảng SingleCustomer, GroupLimitCode trong bảng CustomerLinkage |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String |  | CustomerCode trong bảng SingleCustomer, LinkageCode trong bảng CustomerLinkage |
| 2 | Customer type | customer_type | String |  | C: Single customer, L: Customer linkage |
| 3 | Credit line | customer_credit_line | `Number` |  | CustomerCreditLine trong bảng SingleCustomer, LinkageCreditLine trong bảng CustomerLinkage |
| 4 | Customer currency | currency_code | String |  | CurrencyCode trong bảng SingleCustomer, CurrencyCode trong bảng CustomerLinkage |

**Example:**
```json
{
    
}
```

## 7.2 Transaction flow
- Lấy danh sách customer single và customer linkage có dùng group limit code

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |