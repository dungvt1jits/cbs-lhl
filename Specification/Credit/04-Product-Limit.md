# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditProductLimit​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_SP_CRDPL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Product limit code | product_limit_code | String |  |  |
| 2 | Product limit name | product_limit_name | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Customer code | customer_code | String | 8 | d_customer |
| 5 | Customer name | customer_name | String |  | d_customer |
| 6 | Limit type | limit_type | String |  |  |
| 7 | Currency code | currency_code | String | 3 |  |
| 8 | Limit amount | limit_amount | `Number` |  | số có hai số thập phân |
| 9 | Status | product_status | String |  |  |
| 10 | Product limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditProductLimit​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_ADV_CRDPL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Product limit code | product_limit_code | No | String |  |  |  |
| 2 | Product limit name | product_limit_name | No | String |  |  |  |
| 3 | Customer type | customer_type | No | String |  |  |  |
| 4 | Customer code | customer_code | No | String | 8 |  | d_customer |
| 5 | Customer name | customer_name | No | String |  |  | d_customer |
| 6 | Limit type | limit_type | No | String |  |  |  |
| 7 | Currency code | currency_code | No | String | 3 |  |  |
| 8 | Limit amount from | limit_amount_from | No | `Number` |  |  |  |
| 9 | Limit amount to | limit_amount_to | No | `Number` |  |  |  |
| 10 | Status | product_status | No | String |  |  |  |
| 11 | Page index | page_index | No | `Number` |  |  |  |
| 12 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Product limit code | product_limit_code | String |  |  |
| 2 | Product limit name | product_limit_name | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Customer code | customer_code | String | 8 | d_customer |
| 5 | Customer name | customer_name | String |  | d_customer |
| 6 | Limit type | limit_type | String |  |  |
| 7 | Currency code | currency_code | String | 3 |  |
| 8 | Limit amount | limit_amount | `Number` |  | số có hai số thập phân |
| 9 | Status | product_status | String |  |  |
| 10 | Product limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
- [Tham khảo thông tin chi tiết tại đây](Specification/Credit/07 CRD_PLO Open Product Limit)<br>

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditProductLimit​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_CRDPL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Product limit id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Product limit code | product_limit_code | String | 13 |  |
| 2 | Product name | product_limit_name | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Customer id | customer_id | `Number` |  |  |
| 5 | Reference code | reference_number | String |  |  |
| 6 | Currency | currency_code | String | 3 |  |
| 7 | Amount | limit_amount | `Number` |  | số có hai số thập phân |
| 8 | Available amount | available_amount | `Number` |  | số có hai số thập phân |
| 9 | Limit type | limit_type | String |  |  |
| 10 | Accounting group id | group_id | `Number` |  |  |
| 11 | Status | product_status | String |  |  |
| 12 | Created user | user_created | `Number` |  |  |
| 13 | Approved user | user_approved | `Number` |  |  |
| 14 | Secure type | secure_type | String |  |  |
| 15 | Secure rate | secure_rate | `Number` |  | số có hai số thập phân |
| 16 | Product limit id | id | `Number` |  |  |
|  | Customer code | customer_code | String |  | `trả thêm` |
|  | Customer name | customer_name | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Accounting group name | account_group_def | String |  | `trả thêm`, lấy giá trị `account_group_def` của `group_id` trong "Group Definition" tương ứng `group_id` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditProductLimit​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_UPDATE_CRDPL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Product limit id | id | `Yes` | `Number` |  |  |  |
| 2 | Product name | product_limit_name | `Yes` | String |  |  |  |
| 3 | Reference code | reference_number | No | String |  |  |  |
| 4 | Currency | currency_code | `Yes` | String | 3 |  |  |
| 5 | Amount | limit_amount | `Yes` | `Number` |  |  | số có hai số thập phân |
| 6 | Limit type | limit_type | `Yes` | String |  |  |  |
| 7 | Accounting group id | accounting_group | `Yes` | `Number` |  |  |  |
| 8 | Secure type | secure_type | No | String |  |  |  |
| 9 | Secure rate | secure_rate | No | `Number` |  |  | số có hai số thập phân |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Product limit code | product_limit_code | String | 13 |  |
| 2 | Product name | product_limit_name | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Customer id | customer_id | `Number` |  |  |
| 5 | Reference code | reference_number | String |  |  |
| 6 | Currency | currency_code | String | 3 |  |
| 7 | Amount | limit_amount | `Number` |  | số có hai số thập phân |
| 8 | Available amount | available_amount | `Number` |  | số có hai số thập phân |
| 9 | Limit type | limit_type | String |  |  |
| 10 | Accounting group id | group_id | `Number` |  |  |
| 11 | Status | product_status | String |  |  |
| 12 | Created user | user_created | `Number` |  |  |
| 13 | Approved user | user_approved | `Number` |  |  |
| 14 | Secure type | secure_type | String |  |  |
| 15 | Secure rate | secure_rate | `Number` |  | số có hai số thập phân |
| 16 | Product limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |