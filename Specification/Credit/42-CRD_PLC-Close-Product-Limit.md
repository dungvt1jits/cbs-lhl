# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_PLC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Product limit code | product_limit_code | `Yes` | String |  |  |  | PLCD |
| 2 | Currency | currency_code | No | String | 3 |  |  | CCCR |
| 3 | Product limit | product_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 4 | Available limit | available_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT |
| 5 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 6 | Customer type | customer_type | No | String | 1 |  | trường ẩn | CCTMT |
| 7 | Customer code | customer_code | No | String | 15 |  | trường ẩn | CCTMCD |
| 8 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | CVLDT |
| 9 | Amount (Debit account/BCY) | amount | No | `Number` |  | 0 | trường ẩn | CCVT=CAMT1*CBKEXR |
| 10 | Exchange rate (Debit account/BCY) | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Product limit code | product_limit_code | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Product limit | product_limit | `Number` |  | số có hai số thập phân |
| 5 | Available limit | available_limit | `Number` |  | số có hai số thập phân |
| 6 | Description | description | String |  |  |
| 7 | Customer type | customer_type | String |  |  |
| 8 | Customer code | customer_code | String |  |  |
| 9 | Value date | value_date | `Date time` |  | Working date |
| 10 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 11 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 12 | Transaction date |  | `Date time` |  |  |
| 13 | User id |  | `Number` |  |  |
| 14 | Transaction status | status | String |  |  |
| 15 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Product limit code: exists with status <> "Close".
- Limit amount not use anywhere. 
  - All "Sub product limit" belong it must be closed.
  - Tất cả các khoảng thế chấp của Product limit code đã release.

**Flow of events:**
- Status of product limit is "Close".

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | COMMITMENT_REL | Limit amount | Currency of product limit |
| 1 | C | COMMITMENT_LIMIT | Limit amount | Currency of product limit |

**Voucher:**
- None

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Product limit code | product_limit_code | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Product limit | product_limit | `Number` |  | số có hai số thập phân |
| 5 | Available limit | available_limit | `Number` |  | số có hai số thập phân |
| 6 | Description | description | String |  |  |
| 7 | Customer type | customer_type | String |  |  |
| 8 | Customer code | customer_code | String |  |  |
| 9 | Value date | value_date | `Date time` |  | Working date |
| 10 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 11 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 12 | Transaction date |  | `Date time` |  |  |
| 13 | User id |  | `Number` |  |  |
| 14 | Transaction status | status | String |  |  |
| 15 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_INFO_PL | CRD_GET_INFO_PL | [CRD_GET_INFO_PL](Specification/Common/13 Credit Rulefuncs) |  |  |
| 2 | GET_INFO_CBKEXR | FX_RULEFUNC_GET_INFO_CBKEXR | [FX_RULEFUNC_GET_INFO_CBKEXR](Specification/Common/20 FX Rulefuncs) | `currency_code` = `currency_code`, `branch_id_fx` = `branch_id` của user làm giao dịch | `bk_rate_currency` = `exchange_rate` |

# 5. Signature
- None