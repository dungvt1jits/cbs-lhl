# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_TID`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | account_number | `Yes` | String | 25 |  |  | PCACC |
| 2 | Interest amount | interest_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 3 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 4 | Interest pre | interest_pre | `Yes` | `Number` |  |  |  | PCSAMT1 |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Credit account number | account_number | String |  |  |
| 2 | Interest amount | interest_amount | `Number` |  | số có hai số thập phân |
| 3 | Description | description | String |  |  |
| 4 | Interest pre | interest_pre | `Number` |  |  |
| 4 | Transaction references | reference_id | String |  |  |  |
| 5 | Transaction status | status | String |  |  |
| 6 | Transaction date | transaction_date | `Date time` |  |  |
| 7 | User id | user_id | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- Working date is due date of Interest

**Flow of events:**
- If due date is holiday (Holiday, Sunday, Saturday). Then moving backward to the nearest normal working date.
- Transfer interest accrual to interest due:
    - Insert one row interest due on schedule interest.
- Transaction handles auto by system:
    - Interest due increase.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |