# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_API`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | account_number | `Yes` | String | 25 |  |  | PDACC |
| 2 | Prepaid interest | prepaid_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 3 | Description | description | `Yes` | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | account_number | String |  |  |
| 3 | Prepaid interest | prepaid_interest | `Number` |  | số có hai số thập phân |
| 4 | Description | description | String |  |  |
| 5 | Transaction status | status | String |  |  |
| 6 | Transaction date | transaction_date | `Date time` |  |  |
| 7 | User id | user_id | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**

**Flow of events:**

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |