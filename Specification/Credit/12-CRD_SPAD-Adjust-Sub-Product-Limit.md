# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_SPAD`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Sub product limit code | sub_product_limit_code | `Yes` | String |  |  |  | SPLCD |
| 2 | Current limit | current_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 3 | Available limit | available_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT |
| 4 | Adjustment amount | adjustment_amount | `Yes` | `Number` |  | <> 0 | số có hai số thập phân, cho phép < 0.00 và > 0.00 | CAMT1 |
| 5 | New limit amount | new_limit_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT2 |
| 6 | Description | description | `Yes` | String | 250 | Adjust sub product limit |  | DESCS |
| 7 | Currency | currency_code | `Yes` | String | 3 |  | trường ẩn | CCCR |
| 8 | Customer type | customer_type | `Yes` | String | 1 |  | trường ẩn | CCTMT |
| 9 | Customer code | customer_code | `Yes` | String | 15 |  | trường ẩn | CCTMCD |
| 10 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | CVLDT |
| 11 | Amount (Debit account/BCY) | amount | No | `Number` |  | 0 | trường ẩn | CCVT |
| 12 | Exchange rate (Debit account/BCY) | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 13 | Product limit code | product_limit_code | `Yes` | String |  |  | trường ẩn | PLCD |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Sub product limit code | sub_product_limit_code | String |  |  |
| 3 | Current limit | current_limit | `Number` |  | số có hai số thập phân |
| 4 | Available limit | available_limit | `Number` |  | số có hai số thập phân |
| 5 | Adjustment amount | adjustment_amount | `Number` |  | số có hai số thập phân, cho phép < 0.00 |
| 6 | New limit amount | new_limit_amount | `Number` |  | số có hai số thập phân |
| 7 | Description | description | String |  |  |
| 8 | Currency | currency_code |String |  |  |
| 9 | Customer type | customer_type |String |  |  |
| 10 | Customer code | customer_code | String |  |  |
| 11 | Value date | value_date | `Date time` |  |  |
| 12 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 13 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 14 | Product limit code | product_limit_code | String |  |  |
| 15 | Transaction date |  | `Date time` |  |  |
| 16 | User id |  | String |  |  |
| 17 | Transaction status |  | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Sub product limit code: exists with status "Normal".
- adjustment_amount <> 0.
- `sub_product_limit_code` thuộc `product_limit_code`
- `currency_code`, `customer_type`, `customer_code`, `current_limit`, `available_limit` thuộc `sub_product_limit_code`
- Allow adjustment increase (adjustment_amount > 0): 
    - Product is type "Non-shared" (F)
        - ∑ Limit of other sub products + new limit of this sub product < or = Product limit
    - Product is type "Shared" (M)
        - New limit < or = Product limit.
- Allow adjustment decrease (adjustment_amount < 0):
    - New limit > or = maximum (credit line of loans in the sub product).

**Flow of events:**
- Transaction complete: 
    - Limit of sub product = new limit.
        - LimitAmount `new` = LimitAmount `old` + adjustment_amount
        - AvailableAmount `new` = AvailableAmount `old` + adjustment_amount
        - Update "LimitAmount" `new` and "AvailableAmount" `new`.

**Database:**
- d_crdspl
- d_crdsplhst
- d_crdspltran
- d_txinv
- d_crdspl_sp

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    - Limit of sub product = Old limit.

**Database:**

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Sub product limit code |  | String |  |  |  |
| 3 | Current limit |  | `Number` |  |  | số có hai số thập phân |
| 4 | Available limit |  | `Number` |  |  | số có hai số thập phân |
| 5 | Adjustment amount |  | `Number` |  |  | số có hai số thập phân, cho phép < 0.00 |
| 6 | New limit amount |  | `Number` |  |  | số có hai số thập phân |
| 7 | Description |  | String |  |  |  |
| 8 | Transaction date |  | `Date time` |  |  |  |
| 9 | User id |  | String |  |  |  |
| 10 | Transaction status |  | String |  |  |  |


## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PL | CRD_GET_INFO_SPL | [CRD_GET_INFO_SPL](Specification/Common/13 Credit Rulefuncs) |
| 2 | GET_INFO_CBKEXR | FX_RULEFUNC_GET_INFO_CBKEXR | [FX_RULEFUNC_GET_INFO_CBKEXR](Specification/Common/20 FX Rulefuncs) |

# 5. Signature
- None