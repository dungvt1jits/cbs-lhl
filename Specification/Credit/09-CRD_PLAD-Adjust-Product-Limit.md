# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_PLAD`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Product limit code | product_limit_code | `Yes` | String |  |  |  | PLCD |
| 2 | Current limit | current_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 3 | Available limit | available_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT |
| 4 | Adjustment amount | adjustment_amount | `Yes` | `Number` |  | <> 0 | số có hai số thập phân, cho phép < 0.00 và > 0.00 | CAMT1 |
| 5 | New limit amount | new_limit_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT2 |
| 6 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 7 | Currency | currency_code | No | String | 3 |  | trường ẩn | CCCR |
| 8 | Customer type | customer_type | No | String | 1 |  | trường ẩn | CCTMT |
| 9 | Customer code | customer_code | No | String | 15 |  | trường ẩn | CCTMCD |
| 10 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | CVLDT |
| 11 | Amount (Debit account/BCY) | amount | No | `Number` |  | 0 | trường ẩn | CCVT |
| 12 | Exchange rate (Debit account/BCY) | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Product limit code | product_limit_code | String |  |  |
| 3 | Current limit | current_limit | `Number` |  | số có hai số thập phân |
| 4 | Available limit | available_limit | `Number` |  | số có hai số thập phân |
| 5 | Adjustment amount | adjustment_amount | `Number` |  | số có hai số thập phân, cho phép < 0.00 |
| 6 | New limit amount | new_limit_amount | `Number` |  | số có hai số thập phân |
| 7 | Description | description | String |  |  |
| 8 | Currency | currency_code |String |  |  |
| 9 | Customer type | customer_type | String |  |  |
| 10 | Customer code | customer_code | String |  |  |
| 11 | Value date | value_date | `Date time` |  | Working date |
| 12 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 13 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 14 | Transaction date |  | `Date time` |  |  |
| 15 | User id |  | `Number` |  |  |
| 16 | Transaction status | status | String |  |  |
| 17 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Product limit code: exists with status "Normal".
- adjustment_amount <> 0.00
- Allow adjustment increase (adjustment_amount > 0):
    - ∑ Limit of other products + new limit of this product < or = customer limit
- Allow adjustment decrease (adjustment_amount < 0):
    - New limit > or = maximum (sub product limit) tương đương  với ABS (Adjustment amount) <= Available limit

**Flow of events:**
- Transaction complete:
    - Limit of product = new limit.
        - LimitAmount `new` = LimitAmount `old` + adjustment_amount
        - AvailableAmount `new` = AvailableAmount `old` + adjustment_amount
        - Update "LimitAmount" `new` and "AvailableAmount" `new`.

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp ???

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>
- Case adjustment increase

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | COMMITMENT_LIMIT | Adjustment amount | Currency of product limit | 91500
| 1 | C | COMMITMENT_REL | Adjustment amount | Currency of product limit | 99150

- Case adjustment decrease

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | COMMITMENT_REL | Adjustment amount | Currency of product limit | 99150
| 1 | C | COMMITMENT_LIMIT | Adjustment amount | Currency of product limit | 91500

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    - Limit of product = Old limit.

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Product limit code |  | String |  |  |  |
| 3 | Current limit |  | `Number` |  |  | số có hai số thập phân |
| 4 | Available limit |  | `Number` |  |  | số có hai số thập phân |
| 5 | Adjustment amount |  | `Number` |  |  | số có hai số thập phân, cho phép < 0.00 |
| 6 | New limit amount |  | `Number` |  |  | số có hai số thập phân |
| 7 | Description |  | String |  |  |  |
| 8 | Transaction date |  | `Date time` |  |  |  |
| 9 | User id |  | String |  |  |  |
| 10 | Transaction status |  | String |  |  |  |
| 11 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_crdpl
- d_crdplhst
- d_crdpltran
- d_txinv
- d_crdpl_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PL | CRD_GET_INFO_PL | [CRD_GET_INFO_PL](Specification/Common/13 Credit Rulefuncs) |
| 2 | GET_INFO_CBKEXR | FX_RULEFUNC_GET_INFO_CBKEXR | [FX_RULEFUNC_GET_INFO_CBKEXR](Specification/Common/20 FX Rulefuncs) |

# 5. Signature
- None