# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists with status "Normal".

**Flow of events:**
- Transaction complete: Change credit status to "Write-off".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | BAD_DEBT_LOSS | Outstanding balance | Currency of credit account |
| 1 | C | CREDIT {K=0,1,2,3,4} | Outstanding balance | Currency of credit account |
| 2 | D | PROV_P_DR_RVS {K=0,1,2,3,4} | Current provision principal | Currency of credit account |
| 2 | C | PROV_P_CR_RVS4 | Current provision principal | Currency of credit account |
| 3 | D | PROV_I_DR_RVS {K=0,1,2,3,4} | Current provision interest | Currency of credit account |
| 3 | C | PROV_I_CR_RVS4  | Current provision interest | Currency of credit account |
| 4 | D | WRITEOFF_LOAN | Outstanding balance | Currency of credit account |
| 4 | C | WOFF_PAID_LOAN | Outstanding balance | Currency of credit account |
| 5 | D | WRITEOFF_INTEREST | Interest accrued | Currency of credit account |
| 5 | C | WOFF_PAID_INTEREST | Interest accrued | Currency of credit account |
| 6 | D | PAID_INTEREST {K=0,1} | Interest accrued | Currency of credit account |
| 6 | C | INTEREST {K=0,1} | Interest accrued | Currency of credit account |
| 7 | D | PAID_INTEREST_S | Interest accrued | Currency of credit account |
| 7 | C | INTEREST {K=2,3,4} | Interest accrued | Currency of credit account |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
- [Rulefunc](Specification/Common/01 Rulefunc)<br>

# 5. Signature
- [Signature](Specification/Common/02 Signature)<br>