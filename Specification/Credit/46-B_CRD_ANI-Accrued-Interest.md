# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_ANI`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Debit account number | debit_account_number | `Yes` | String | 25 |  |  | DAC |
| 2 | Credit account number | credit_account_number | `Yes` | String | 25 |  |  | CAC |
| 3 | Amount | amount | `Yes` | `Number` |  |  | số có hai số thập phân | AMT |
| 4 | Credit off-bal account | credit_off_bal_account | `Yes` | String | 25 |  |  | CROFF |
| 5 | Debit off-bal account | debit_off_bal_account | `Yes` | String | 25 |  |  | DROFF |
| 6 | NPL index | npl_index | `Yes` | `Number` |  |  | số nguyên dương | NPLINDEX |
| 7 | Catalogue id | catalogue_id | `Yes` | `Number` |  |  | số nguyên dương | CATID |
| 8 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 9 | Thread id | thread_id | `Yes` | `Number` |  |  | số nguyên dương | THREADID |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Debit account number | debit_account_number | String |  |  |
| 3 | Credit account number | credit_account_number | String |  |  |
| 4 | Amount | amount | `Number` |  | số có hai số thập phân |
| 5 | Credit off-bal account | credit_off_bal_account | String |  |  |
| 6 | Debit off-bal account | debit_off_bal_account | String |  |  |
| 7 | NPL index | npl_index | `Number` |  | số nguyên dương |
| 8 | Catalogue id | catalogue_id | `Number` |  | số nguyên dương |
| 9 | Description | description | String |  |  |
| 10 | Thread id | thread_id | `Number` |  | số nguyên dương |
| 11 | Transaction status | status | String |  |  |
| 12 | Transaction date | transaction_date | `Date time` |  |  |
| 13 | User id | user_id | `Number` |  |  |
|  | Step code: `ACT_EXECUTE_POSTING` |  |  |  |  |
| 14 | Posting data | entry_journals | JSON Object |  |  |
|  | Group | accounting_entry_group | `Number` |  | số nguyên dương |
|  | Index in group | accounting_entry_index | `Number` |  | số nguyên dương |
|  | Posting side | debit_or_credit | String |  | D or C |
|  | Account number | bank_account_number | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Currency | currency_code | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Credit accounts have outstanding > 0.00
- Interest rate > 0.00
- Apply calculation for all classification status.

**Flow of events:**
- [Interest daily amount] = Outstanding * interest rate of day.
- Calculate interest on end of day.
- Support calculation interest normal.
- Allow definition tenor of interest rate: year, month, week, days. 
    - Example: 
        - Interest rate = 4% / year (Interest rate of day = 4% / 360)
        - Interest rate = 2% / month (Interest rate of day = 2% / 30)
        - Interest rate = 1.5% / week (Interest rate of day = 1.5% / 7)
- Allow definition number decimal of Interest accrual.
- Flexibility in setup interest rate: Float rate + margin rate.
- Interest amount divide 2 type: "On balance sheet interest" and "Off balance sheet interest". So, Interest accrual will add into:
    - "On balance sheet interest" when account is normal, special mention.
    - "Off balance sheet interest" when account is substandard, doubtful, loss.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

- Case classification of account is {Normal: 0, Special mention: 1}

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST {K=0,1} | Interest daily amount | Currency of credit account |
| 1 | C | PAID_INTEREST {K=0,1} | Interest daily amount | Currency of credit account |

- Case classification of account is {Substandard: 2, Doubtful: 3, Loss: 4}

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST {K=2,3,4} | Interest daily amount | Currency of credit account |
| 1 | C | PAID_INTEREST_S | Interest daily amount | Currency of credit account |

- Case status account is “Write-off”

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | WRITEOFF_INTEREST | Interest daily amount | Currency of credit account |
| 1 | C | WOFF_PAID_INTEREST | Interest daily amount | Currency of credit account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |