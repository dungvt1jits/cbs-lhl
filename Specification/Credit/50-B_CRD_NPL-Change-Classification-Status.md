# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number |  | `Yes` | String | 25 |  |  |
| 2 | Provision principle amount 1 |  | `Yes` | `Number` |  | 0 | số có hai số thập phân |
| 3 | Provision principle amount 2 |  | `Yes` | `Number` |  | 0 | số có hai số thập phân |
| 4 | Provision interest amount 1 |  | `Yes` | `Number` |  | 0 | số có hai số thập phân |
| 5 | Provision interest amount 2 |  | `Yes` | `Number` |  | 0 | số có hai số thập phân |
| 6 | Group 1 |  | `Yes` | `Number` |  | 0 | số nguyên dương |
| 7 | Group 2 |  | `Yes` | `Number` |  | 0 | số nguyên dương |
| 8 | Description |  | `Yes` | String | 250 |  |  |
| 9 | Transaction date |  | `Yes` | `Date time` |  | Working date |  |
| 10 | User id |  | `Yes` | String |  | User login |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |
| 2 | Credit account number |  | String |  |  |
| 3 | Provision principle amount 1 |  | `Number` |  | số có hai số thập phân |
| 4 | Provision principle amount 2 |  | `Number` |  | số có hai số thập phân |
| 5 | Provision interest amount 1 |  | `Number` |  | số có hai số thập phân |
| 6 | Provision interest amount 2 |  | `Number` |  | số có hai số thập phân |
| 7 | Group 1 |  | `Number` |  | số nguyên dương |
| 8 | Group 2 |  | `Number` |  | số nguyên dương |
| 9 | Description |  | String |  |  |
| 10 | Transaction date |  | `Date time` |  |  |
| 11 | User id |  | String |  |  |
| 12 | Transaction status |  | String |  |  |
| 13 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**

**Flow of events:**

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- [See detail NPL Processing](Specification/Credit/36 CRD_NPL NPL Processing#12-transaction-flow)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |