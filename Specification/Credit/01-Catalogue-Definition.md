# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditCatalog​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_SP_CRDCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String | 8 |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Credit type | credit_type | String |  |  |
| 5 | Credit facility | tenor_type | String |  |  |
| 6 | Tenor type | credit_facility | String |  |  |
| 7 | Status | catalog_status | String |  |  |
| 8 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditCatalog​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_ADV_CRDCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String | 8 |  |  |
| 2 | Catalogue name | catalog_name | No | String |  |  |  |
| 3 | Currency | currency_code | No | String | 3 |  |  |
| 4 | Credit type | credit_type | No | String |  |  |  |
| 5 | Credit facility | credit_facility | No | String |  |  |  |
| 6 | Tenor type | tenor_type | No | String |  |  |  |
| 7 | Status | catalog_status | No | String |  |  |  |
| 8 | Page index | page_index | No | `Number` |  |  |  |
| 9 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String | 8 |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Credit type | credit_type | String |  |  |
| 5 | Credit facility | tenor_type | String |  |  |
| 6 | Tenor type | credit_facility | String |  |  |
| 7 | Status | catalog_status | String |  |  |
| 8 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditCatalog​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_INSERT_CRDCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | `Yes` | String | 25 |  | duy nhất, không được trùng |
| 2 | Catalogue name | catalog_name | `Yes` | String | 100 |  |  |
| 3 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 4 | Secure currency code | secure_currency_code | No | String | 3 |  | không sử dụng, xem xét xóa đi |
| 5 | Credit type | credit_type | `Yes` | String | 1 |  |  |
| 6 | Tenor type | tenor_type | `Yes` | String | 1 |  |  |
| 7 | Is syndicated? | is_syndicated | `Yes` | String | 1 |  |  |
| 8 | Interest computation mode | interest_computation_mode | `Yes` | String | 1 |  |  |
| 9 | Secure type | secure_type | `Yes` | String | 1 |  |  |
| 10 | Minimum secure rate | secure_rate | No | `Number` |  |  |  |
| 11 | Credit purpose | credit_purpose | No | String | 2 |  |  |
| 12 | Credit classification | credit_classification | No | String | 2 |  |  |
| 13 | Credit facility | credit_facility | `Yes` | String | 2 |  |  |
| 14 | Disbursement mode | disbursement_mode | `Yes` | String | 1 |  |  |
| 15 | Is provision? | is_provision | `Yes` | String | 1 |  |  |
| 16 | Provision tenor | provision_tenor | No | `Number` |  |  | số nguyên dương |
| 17 | Provision tenor unit | provision_tenor_unit | No | String | 1 |  |  |
| 18 | Rollover option | rollover_option | `Yes` | String | 1 |  | xem xét bỏ trường này |
| 19 | Classification option | restruct | `Yes` | String | 1 |  |  |
| 20 | Default sub product | subproduct | `Yes` | String | 1 |  |  |
| 21 | Status | catalog_status | `Yes` | String | 1 |  |  |
| 22 | Principal collection tenor | principal_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 23 | Principal collection tenor unit | principal_tenor_unit | `Yes` | String | 1 |  |  |
| 24 | Principal grace period | principal_grace_period | No | `Number` |  |  | số nguyên dương |
| 25 | Principal due on holiday | holiday_principal_due_on | `Yes` | `Number` |  |  | số nguyên dương |
| 26 | Interest collection tenor | interest_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 27 | Interest collection tenor unit | interest_tenor_unit | `Yes` | String | 1 |  |  |
| 28 | Interest grace period | interest_grace_period | No | `Number` |  |  | số nguyên dương |
| 29 | Interest due on holiday | holiday_interest_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 30 | Fine collection tenor | fine_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 31 | Fine collection tenor unit | fine_tenor_unit | `Yes` | String | 1 |  |  |
| 32 | Fine grace period | fine_grace_period | No | `Number` |  |  | số nguyên dương |
| 33 | Fine due on holiday | holiday_fine_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 34 | Discount rate | discount_rate | No | `Number` |  |  | số nguyên dương |
| 35 | Re discount rate | re_discount_rate | No | `Number` |  |  | số nguyên dương |
| 36 | Decimal rounding amount for principal | principal_decimal_rounding | No | `Number` |  |  | số nguyên dương |
| 37 | Decimal rounding amount for interest | interest_decimal_rounding | No | `Number` |  |  | số nguyên dương |
| 38 | Principal provision rate normal | principal_provision_rate0 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 39 | Principal provision rate special mention | principal_provision_rate1 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 40 | Principal provision rate sub standard | principal_provision_rate2 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 41 | Principal provision rate doubtful | principal_provision_rate3 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 42 | Principal provision rate lost | principal_provision_rate4 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 43 | Interest provision rate normal | interest_provision_rate0 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 44 | Interest provision rate special mention | interest_provision_rate1 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 45 | Interest provision rate sub standard | interest_provision_rate2 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 46 | Interest provision rate doubtful | interest_provision_rate3 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 47 | Interest provision rate lost | interest_provision_rate4 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 48 | Tariff code | tariff_code | `Yes` | `Number` |  |  |  |
| 49 | Group id | group_id | `Yes` | `Number` |  |  |  |
| 50 | List notification type | list_notification_type | `Yes` | Array Object |  |  |  chứa danh sách value: SMS (S), Email(E), Push Notification(P) |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue code | catalog_code | String |  |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |
| 4 | Secure currency code | secure_currency_code | String |  | không sử dụng, xem xét xóa đi |
| 5 | Credit type | credit_type | String |  |  |
| 6 | Tenor type | tenor_type | String |  |  |
| 7 | Is syndicated? | is_syndicated | String |  |  |
| 8 | Interest computation mode | interest_computation_mode | String |  |  |
| 9 | Secure type | secure_type | String |  |  |
| 10 | Minimum secure rate | secure_rate | `Number` |  |  |
| 11 | Credit purpose | credit_purpose | String |  |  |
| 12 | Credit classification | credit_classification | String |  |  |
| 13 | Credit facility | credit_facility | String |  |  |
| 14 | Disbursement mode | disbursement_mode | String |  |  |
| 15 | Is provision? | is_provision | String |  |  |
| 16 | Provision tenor | provision_tenor | `Number` |  |  |
| 17 | Provision tenor unit | provision_tenor_unit | String |  |  |
| 18 | Rollover option | rollover_option | String |  |  |
| 19 | Classification option | restruct | String |  |  |
| 20 | Default sub product | subproduct | String |  |  |
| 21 | Status | catalog_status | String |  |  |
| 22 | Principal collection tenor | principal_tenor | `Number` |  |  |
| 23 | Principal collection tenor unit | principal_tenor_unit | String |  |  |
| 24 | Principal grace period | principal_grace_period | `Number` |  |  |
| 25 | Principal due on holiday | holiday_principal_due_on | `Number` |  |  |
| 26 | Interest collection tenor | interest_tenor | `Number` |  |  |
| 27 | Interest collection tenor unit | interest_tenor_unit | String |  |  |
| 28 | Interest grace period | interest_grace_period | `Number` |  |  |
| 29 | Interest due on holiday | holiday_interest_tenor | `Number` |  |  |
| 30 | Fine collection tenor | fine_tenor | `Number` |  |  |
| 31 | Fine collection tenor unit | fine_tenor_unit | String |  |  |
| 32 | Fine grace period | fine_grace_period | `Number` |  |  |
| 33 | Fine due on holiday | holiday_fine_tenor | `Number` |  |  |
| 34 | Discount rate | discount_rate | `Number` |  |  |
| 35 | Re discount rate | re_discount_rate | `Number` |  |  |
| 36 | Decimal rounding amount for principal | principal_decimal_rounding | `Number` |  |  |
| 37 | Decimal rounding amount for interest | interest_decimal_rounding | `Number` |  |  |
| 38 | Principal provision rate normal | principal_provision_rate_0 | `Number` |  |  |
| 39 | Principal provision rate special mention | principal_provision_rate_1 | `Number` |  |  |
| 40 | Principal provision rate sub standard | principal_provision_rate_2 | `Number` |  |  |
| 41 | Principal provision rate doubtful | principal_provision_rate_3 | `Number` |  |  |
| 42 | Principal provision rate lost | principal_provision_rate_4 | `Number` |  |  |
| 43 | Interest provision rate normal | interest_provision_rate_0 | `Number` |  |  |
| 44 | Interest provision rate special mention | interest_provision_rate_1 | `Number` |  |  |
| 45 | Interest provision rate sub standard | interest_provision_rate_2 | `Number` |  |  |
| 46 | Interest provision rate doubtful | interest_provision_rate_3 | `Number` |  |  |
| 47 | Interest provision rate lost | interest_provision_rate_4 | `Number` |  |  |
| 48 | Tariff code | tariff_code | `Number` |  |  |
| 49 | Group id | group_id | `Number` |  |  |
| 50 | Created by | user_created | `Number` |  |  |
| 51 | Approved by | user_approved | `Number` |  |  |
| 52 | Catalogue id | id | `Number` |  |  |
| 53 | List notification type | list_notification_type | Array Object |  | chứa danh sách value: SMS (S), Email(E), Push Notification(P) |
| 54 | Notification type | notification_type | String |  | chuỗi cách nhau bởi dấu trị tuyệt đối |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditCatalog​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_CRDCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 60
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue code | catalog_code | String |  |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |
| 4 | Secure currency code | secure_currency_code | String |  | không sử dụng, xem xét xóa đi |
| 5 | Credit type | credit_type | String |  |  |
| 6 | Tenor type | tenor_type | String |  |  |
| 7 | Is syndicated? | is_syndicated | String |  |  |
| 8 | Interest computation mode | interest_computation_mode | String |  |  |
| 9 | Secure type | secure_type | String |  |  |
| 10 | Minimum secure rate | secure_rate | `Number` |  |  |
| 11 | Credit purpose | credit_purpose | String |  |  |
| 12 | Credit classification | credit_classification | String |  |  |
| 13 | Credit facility | credit_facility | String |  |  |
| 14 | Disbursement mode | disbursement_mode | String |  |  |
| 15 | Is provision? | is_provision | String |  |  |
| 16 | Provision tenor | provision_tenor | `Number` |  |  |
| 17 | Provision tenor unit | provision_tenor_unit | String |  |  |
| 18 | Rollover option | rollover_option | String |  |  |
| 19 | Classification option | restruct | String |  |  |
| 20 | Default sub product | subproduct | String |  |  |
| 21 | Status | catalog_status | String |  |  |
| 22 | Principal collection tenor | principal_tenor | `Number` |  |  |
| 23 | Principal collection tenor unit | principal_tenor_unit | String |  |  |
| 24 | Principal grace period | principal_grace_period | `Number` |  |  |
| 25 | Principal due on holiday | holiday_principal_due_on | `Number` |  |  |
| 26 | Interest collection tenor | interest_tenor | `Number` |  |  |
| 27 | Interest collection tenor unit | interest_tenor_unit | String |  |  |
| 28 | Interest grace period | interest_grace_period | `Number` |  |  |
| 29 | Interest due on holiday | holiday_interest_tenor | `Number` |  |  |
| 30 | Fine collection tenor | fine_tenor | `Number` |  |  |
| 31 | Fine collection tenor unit | fine_tenor_unit | String |  |  |
| 32 | Fine grace period | fine_grace_period | `Number` |  |  |
| 33 | Fine due on holiday | holiday_fine_tenor | `Number` |  |  |
| 34 | Discount rate | discount_rate | `Number` |  |  |
| 35 | Re discount rate | re_discount_rate | `Number` |  |  |
| 36 | Decimal rounding amount for principal | principal_decimal_rounding | `Number` |  |  |
| 37 | Decimal rounding amount for interest | interest_decimal_rounding | `Number` |  |  |
| 38 | Principal provision rate normal | principal_provision_rate_0 | `Number` |  |  |
| 39 | Principal provision rate special mention | principal_provision_rate_1 | `Number` |  |  |
| 40 | Principal provision rate sub standard | principal_provision_rate_2 | `Number` |  |  |
| 41 | Principal provision rate doubtful | principal_provision_rate_3 | `Number` |  |  |
| 42 | Principal provision rate lost | principal_provision_rate_4 | `Number` |  |  |
| 43 | Interest provision rate normal | interest_provision_rate_0 | `Number` |  |  |
| 44 | Interest provision rate special mention | interest_provision_rate_1 | `Number` |  |  |
| 45 | Interest provision rate sub standard | interest_provision_rate_2 | `Number` |  |  |
| 46 | Interest provision rate doubtful | interest_provision_rate_3 | `Number` |  |  |
| 47 | Interest provision rate lost | interest_provision_rate_4 | `Number` |  |  |
| 48 | Tariff code | tariff_code | `Number` |  |  |
| 49 | Group id | group_id | `Number` |  |  |
| 50 | Created by | user_created | `Number` |  |  |
| 51 | Approved by | user_approved | `Number` |  |  |
| 52 | Catalogue id | id | `Number` |  |  |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
| 53 | List notification type | list_notification_type | Array Object |  | chứa danh sách value: SMS (S), Email(E), Push Notification(P) |
| 54 | Notification type | notification_type | String |  | chuỗi cách nhau bởi dấu trị tuyệt đối |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditCatalog​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_UPDATE_CRDCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  | không được sửa |
| 2 | Catalogue name | catalog_name | `Yes` | String |  |  |  |
| 3 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 4 | Secure currency code | secure_currency_code | No | String |  |  | không sử dụng, xem xét xóa đi |
| 5 | Credit type | credit_type | `Yes` | String |  |  |  |
| 6 | Tenor type | tenor_type | `Yes` | String |  |  |  |
| 7 | Is syndicated? | is_syndicated | `Yes` | String |  |  |  |
| 8 | Interest computation mode | interest_computation_mode | `Yes` | String |  |  |  |
| 9 | Secure type | secure_type | `Yes` | String |  |  |  |
| 10 | Minimum secure rate | secure_rate | No | `Number` |  |  |  |
| 11 | Credit purpose | credit_purpose | No | String |  |  |  |
| 12 | Credit classification | credit_classification | No | String |  |  |  |
| 13 | Credit facility | credit_facility | `Yes` | String |  |  |  |
| 14 | Disbursement mode | disbursement_mode | `Yes` | String |  |  |  |
| 15 | Is provision? | is_provision | `Yes` | String |  |  |  |
| 16 | Provision tenor | provision_tenor | No | `Number` |  |  | số nguyên dương |
| 17 | Provision tenor unit | provision_tenor_unit | No | String |  |  |  |
| 18 | Rollover option | rollover_option | `Yes` | String |  |  | xem xét bỏ trường này |
| 19 | Classification option | restruct | `Yes` | String |  |  |  |
| 20 | Default sub product | subproduct | `Yes` | String |  |  |  |
| 21 | Status | catalog_status | `Yes` | String |  |  |  |
| 22 | Principal collection tenor | principal_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 23 | Principal collection tenor unit | principal_tenor_unit | `Yes` | String |  |  |  |
| 24 | Principal grace period | principal_grace_period | No | `Number` |  |  | số nguyên dương |
| 25 | Principal due on holiday | holiday_principal_due_on | `Yes` | `Number` |  |  | số nguyên dương |
| 26 | Interest collection tenor | interest_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 27 | Interest collection tenor unit | interest_tenor_unit | `Yes` | String |  |  |  |
| 28 | Interest grace period | interest_grace_period | No | `Number` |  |  | số nguyên dương |
| 29 | Interest due on holiday | holiday_interest_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 30 | Fine collection tenor | fine_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 31 | Fine collection tenor unit | fine_tenor_unit | `Yes` | String |  |  |  |
| 32 | Fine grace period | fine_grace_period | No | `Number` |  |  | số nguyên dương |
| 33 | Fine due on holiday | holiday_fine_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 34 | Discount rate | discount_rate | No | `Number` |  |  | số nguyên dương |
| 35 | Re discount rate | re_discount_rate | No | `Number` |  |  | số nguyên dương |
| 36 | Decimal rounding amount for principal | principal_decimal_rounding | No | `Number` |  |  | số nguyên dương |
| 37 | Decimal rounding amount for interest | interest_decimal_rounding | No | `Number` |  |  | số nguyên dương |
| 38 | Principal provision rate normal | principal_provision_rate0 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 39 | Principal provision rate special mention | principal_provision_rate1 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 40 | Principal provision rate sub standard | principal_provision_rate2 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 41 | Principal provision rate doubtful | principal_provision_rate3 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 42 | Principal provision rate lost | principal_provision_rate4 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 43 | Interest provision rate normal | interest_provision_rate0 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 44 | Interest provision rate special mention | interest_provision_rate1 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 45 | Interest provision rate sub standard | interest_provision_rate2 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 46 | Interest provision rate doubtful | interest_provision_rate3 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 47 | Interest provision rate lost | interest_provision_rate4 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 48 | Tariff code | tariff_code | `Yes` | `Number` |  |  |  |
| 49 | Group id | group_id | `Yes` | `Number` |  |  |  |
| 50 | List notification type | list_notification_type | `Yes` | Array Object |  |  |  chứa danh sách value: SMS (S), Email(E), Push Notification(P) |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue code | catalog_code | String |  |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |
| 4 | Secure currency code | secure_currency_code | String |  | không sử dụng, xem xét xóa đi |
| 5 | Credit type | credit_type | String |  |  |
| 6 | Tenor type | tenor_type | String |  |  |
| 7 | Is syndicated? | is_syndicated | String |  |  |
| 8 | Interest computation mode | interest_computation_mode | String |  |  |
| 9 | Secure type | secure_type | String |  |  |
| 10 | Minimum secure rate | secure_rate | `Number` |  |  |
| 11 | Credit purpose | credit_purpose | String |  |  |
| 12 | Credit classification | credit_classification | String |  |  |
| 13 | Credit facility | credit_facility | String |  |  |
| 14 | Disbursement mode | disbursement_mode | String |  |  |
| 15 | Is provision? | is_provision | String |  |  |
| 16 | Provision tenor | provision_tenor | `Number` |  |  |
| 17 | Provision tenor unit | provision_tenor_unit | String |  |  |
| 18 | Rollover option | rollover_option | String |  |  |
| 19 | Classification option | restruct | String |  |  |
| 20 | Default sub product | subproduct | String |  |  |
| 21 | Status | catalog_status | String |  |  |
| 22 | Principal collection tenor | principal_tenor | `Number` |  |  |
| 23 | Principal collection tenor unit | principal_tenor_unit | String |  |  |
| 24 | Principal grace period | principal_grace_period | `Number` |  |  |
| 25 | Principal due on holiday | holiday_principal_due_on | `Number` |  |  |
| 26 | Interest collection tenor | interest_tenor | `Number` |  |  |
| 27 | Interest collection tenor unit | interest_tenor_unit | String |  |  |
| 28 | Interest grace period | interest_grace_period | `Number` |  |  |
| 29 | Interest due on holiday | holiday_interest_tenor | `Number` |  |  |
| 30 | Fine collection tenor | fine_tenor | `Number` |  |  |
| 31 | Fine collection tenor unit | fine_tenor_unit | String |  |  |
| 32 | Fine grace period | fine_grace_period | `Number` |  |  |
| 33 | Fine due on holiday | holiday_fine_tenor | `Number` |  |  |
| 34 | Discount rate | discount_rate | `Number` |  |  |
| 35 | Re discount rate | re_discount_rate | `Number` |  |  |
| 36 | Decimal rounding amount for principal | principal_decimal_rounding | `Number` |  |  |
| 37 | Decimal rounding amount for interest | interest_decimal_rounding | `Number` |  |  |
| 38 | Principal provision rate normal | principal_provision_rate_0 | `Number` |  |  |
| 39 | Principal provision rate special mention | principal_provision_rate_1 | `Number` |  |  |
| 40 | Principal provision rate sub standard | principal_provision_rate_2 | `Number` |  |  |
| 41 | Principal provision rate doubtful | principal_provision_rate_3 | `Number` |  |  |
| 42 | Principal provision rate lost | principal_provision_rate_4 | `Number` |  |  |
| 43 | Interest provision rate normal | interest_provision_rate_0 | `Number` |  |  |
| 44 | Interest provision rate special mention | interest_provision_rate_1 | `Number` |  |  |
| 45 | Interest provision rate sub standard | interest_provision_rate_2 | `Number` |  |  |
| 46 | Interest provision rate doubtful | interest_provision_rate_3 | `Number` |  |  |
| 47 | Interest provision rate lost | interest_provision_rate_4 | `Number` |  |  |
| 48 | Tariff code | tariff_code | `Number` |  |  |
| 49 | Group id | group_id | `Number` |  |  |
| 50 | Created by | user_created | `Number` |  |  |
| 51 | Approved by | user_approved | `Number` |  |  |
| 52 | Catalogue id | id | `Number` |  |  |
| 53 | List notification type | list_notification_type | Array Object |  | chứa danh sách value: SMS (S), Email(E), Push Notification(P) |
| 54 | Notification type | notification_type | String |  | chuỗi cách nhau bởi dấu trị tuyệt đối |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditCatalog​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_DELETE_CRDCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 62
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 62
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# Business rule and constrains
- If Credit type is Blanket, principal tenor must be B: Bullet (Lump sum) or E: Any. And Interest Computation Mode cannot be one of {B: Flat basic of installment, C: Compound basic of installment}.
- Principal tenor must equal or longer than interest/fee tenor.
- If Interest Computation mode is one of {B: Flat basic of installment, C: Compound basic of installment, D: Daily rest, M: Monthly rest, Q: Quarterly rest, H: Half-anniversary rest, Y: Yearly rest}, Principal tenor and interest/fee tenor cannot be Bullet (Lump sum).
- If Credit facility is OD: Over draft, Credit type must be Blanket, and Principal tenor must be B: Bullet (Lump sum) or E: Any.
# Accounting group definition
- Accounting group is aimed to help system to generate accounting entry for appropriate transaction. With Neptune system, accounting group is assigned to catalogue in each business application.
- Credit application catalogue can assign to accounting group with the following structure:

| No | System account name | Description |
| -- | ------------------- | ----------- |
| 1 | CREDITK {K=0,1,2,3,4} | Debit posting when disbursement for account have group debt K |
|  |  | Credit posting when collect Principal for account have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 2 | INTERESTK {K=0,1,2,3,4} | Debit posting when accrued interest for accounts have group debt K |
|  |  | Credit posting when collect interest for accounts have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 3 | PAID_INTERESTK {K=0,1} | Credit posting when accrued interest for accounts "On balance sheet" |
|  |  | Debit posting when change NPL to "Off balance sheet" |
|  |  | Group debt K = {0: Normal, 1: Special mention} |
| 4 | PAID_INTEREST_S | Credit posting when accrued interest for accounts "Off balance sheet" |
|  |  | Debit posting when change NPL to "On balance sheet" |
|  |  | Off balance sheet = {Substandard, Doubtful, Loss} |
| 5 | PROV_P_DRK {K=0,1,2,3,4} | Debit posting when provision Principal for account have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 6 | PROV_P_CRK {K=0,1,2,3,4} | Credit posting when provision Principal for account have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 7 | PROV_P_DR_RVSK {K=0,1,2,3,4} | Debit posting when reverse provision Principal for account have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 8 | PROV_P_CR_RVSK {K=0,1,2,3,4} | Credit posting when reverse provision Principal for account have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 9 | COMMITMENT_LIMIT | Commitment account for un-utilize limit |
| 10 | COMMITMENT_REL | Released commitment account for un-utilize limit |
| 11 | PROV_I_DRK {K=0,1,2,3,4} | Debit posting when provision Interest for account have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 12 | PROV_I_CRK {K=0,1,2,3,4} | Credit posting when provision Interest for account have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 13 | PROV_I_DR_RVSK {K=0,1,2,3,4} | Debit posting when reverse provision Interest for account have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 14 | PROV_I_CR_RVSK {K=0,1,2,3,4} | Credit posting when reverse provision Interest for account have group debt K |
|  |  | Group debt K = {0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss} |
| 15 | BAD_DEBT_LOSS | Debit posting when change credit to write-off and disbursement for accounts write-off |
| 16 | WRITEOFF_LOAN | Debit posting when change credit to write-off and disbursement for accounts write-off |
|  |  | Credit posting when collect Principal for accounts write-off |
| 17 | WOFF_PAID_LOAN | Credit posting when change credit to write-off and disbursement for accounts write-off |
|  |  | Debit posting when collect Principal for accounts write-off |
| 18 | WRITEOFF_INTEREST | Debit posting when change credit to write-off and accrued interest for accounts write-off |
|  |  | Credit posting when collect interest for accounts write-off |
| 19 | WOFF_PAID_INTEREST | Credit posting when change credit to write-off and accrued interest for accounts write-off |
|  |  | Debit posting when collect interest for accounts write-off |
| 20 | BAD_DEBT_PRINCIPLE | Credit posting when collect Principal for accounts write-off |
| 21 | BAD_DEBT_INTEREST_WO | Credit posting when collect Interest for accounts write-off |

# 7. Get list tariff
- [Tariff lookup trong "Rulefunc"](Specification/Common/01 Rulefunc)

# 8. Get list accounting group for credit
- "module": "CRD"
- [Accounting group trong "Rulefunc"](Specification/Common/01 Rulefunc)