# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditSubProductLimit​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_SP_CRDSPL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Sub product limit code | sub_product_limit_code | String | 15 |  |  |
| 2 | Product limit name | sub_product_limit_name | String |  |  |  |
| 3 | Product limit code | product_limit_code | String | 13 |  |  |
| 4 | Customer type | customer_type | String |  |  |
| 5 | Customer code | customer_code | String | 8 |  |
| 6 | Customer name | customer_name | String |  | d_customer |
| 7 | Currency code | currency_code | String | 3 |  |
| 8 | Limit amount | limit_amount | `Number` |  | số có hai số thập phân |
| 9 | Credit facility | credit_facility | String |  |  |
| 10 | Status | sub_product_status | String |  |  |
| 11 | Sub product limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditSubProductLimit​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_ADV_CRDSPL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Sub product limit code | sub_product_limit_code | No | String | 13 |  |  |
| 2 | Sub product limit name | sub_product_limit_name | No | String |  |  |  |
| 3 | Product limit code | product_limit_code | No | String | 15 |  |  |
| 4 | Product limit name | product_name | No | String |  |  |  |
| 5 | Customer type | customer_type | No | String |  |  |  |
| 6 | Customer code | customer_code | No | String | 8 |  |  |
| 7 | Customer name | customer_name | No | String |  |  | d_customer |
| 8 | Currency | currency_code | No | String | 3 |  |  |
| 9 | Status | sub_product_status | No | String |  |  |  |
| 10 | Page index | page_index | No | `Number` |  |  |  |
| 11 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Sub product limit code | sub_product_limit_code | String | 15 |  |  |
| 2 | Product limit name | sub_product_limit_name | String |  |  |  |
| 3 | Product limit code | product_limit_code | String | 13 |  |  |
| 4 | Customer type | customer_type | String |  |  |
| 5 | Customer code | customer_code | String | 8 |  |
| 6 | Customer name | customer_name | String |  | d_customer |
| 7 | Currency code | currency_code | String | 3 |  |
| 8 | Limit amount | limit_amount | `Number` |  | số có hai số thập phân |
| 9 | Credit facility | credit_facility | String |  |  |
| 10 | Status | sub_product_status | String |  |  |
| 11 | Sub product limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
- [Tham khảo thông tin chi tiết tại đây](Specification/Credit/10 CRD_SPLO Open Sub-Product Limit)<br>

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditSubProductLimit​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_CRDSPL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Sub product limit id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Sub product limit code | sub_product_limit_code | String | 15 |  |
| 2 | Sub product name | sub_product_limit_name | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Customer id | customer_id | `Number` |  |  |
| 5 | Product code | product_limit_code | String | 13 |  |
| 6 | Reference code | reference_number | String |  |  |
| 7 | Currency | currency_code | String | 3 |  |
| 8 | Amount | limit_amount | `Number` |  | số có hai số thập phân |
| 9 | Available amount | available_amount | `Number` |  | số có hai số thập phân |
| 10 | Credit facility | credit_facility | String |  |  |
| 11 | Status | sub_product_status | String |  |  |  |
| 12 | Created user | user_created | `Number` |  |  |
| 13 | Approved user | user_approved | `Number` |  |  |
| 14 | Sub product limit id | id | `Number` |  |  |
|  | Customer code | customer_code | String |  | `trả thêm` |
|  | Customer name | customer_name | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Product name | product_limit_name | String |  | `trả thêm` |

**Example:**
```json
{

}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditSubProductLimit​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_UPDATE_CRDSPL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Sub product limit id | id | `Yes` | `Number` |  |  |  |
| 2 | Sub product name | sub_product_limit_name | `Yes` | String |  |  |  |
| 3 | Reference code | reference_number | No | String |  |  |  |
| 4 | Currency | currency_code | `Yes` | String | 3 |  |  |
| 5 | Amount | limit_amount | `Yes` | `Number` |  |  | số có hai số thập phân |
| 6 | Credit facility | credit_facility | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Sub product limit code | sub_product_limit_code | String | 15 |  |
| 2 | Sub product name | sub_product_limit_name | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Customer id | customer_id | `Number` |  |  |
| 5 | Product code | product_limit_code | String | 13 |  |
| 6 | Reference code | reference_number | String |  |  |
| 7 | Currency | currency_code | String | 3 |  |
| 8 | Amount | limit_amount | `Number` |  | số có hai số thập phân |
| 9 | Available amount | available_amount | `Number` |  | số có hai số thập phân |
| 10 | Credit facility | credit_facility | String |  |  |
| 11 | Status | sub_product_status | String |  |  |  |
| 12 | Created user | user_created | `Number` |  |  |
| 13 | Approved user | user_approved | `Number` |  |  |
| 14 | Sub product limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |