# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_APR`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | PCACC |
| 2 | Customer type | customer_type | `Yes` | String | 1 |  | trường ẩn | CCTMT |
| 3 | Account holder name | account_holder_name | `Yes` | String | 250 |  |  | CACNM |
| 4 | Catalogue code | catalog_code | `Yes` | String | 25 |  |  | CNCAT |
| 5 | Catalogue name | catalog_name | `Yes` | String | 100 |  |  | CCATNM |
| 6 | Seq number | seq_number | No | `Number` | 6 | 0 | cho phép `null` | CNSEQ |
| 7 | Currency code | currency_code | `Yes` | String | 3 |  |  | CACCCR |
| 8 | Credit limit | credit_limit | `Yes` | `Number` |  | 0 |  | TXAMT |
| 9 | Deposit account | deposit_account | No | String | 25 |  |  | PDACC |
| 10 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 11 | Currency of credit account | currency_of_credit_account | No | String | 3 |  | trường ẩn | CCCR |
| 12 | Amount (Debit account/BCY) | amount | `Yes` | `Number` |  | 0 | trường ẩn | CCVT |
| 13 | Exchange rate (Debit account/BCY) | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 14 | Sub product limit code | sub_product_limit_code | `Yes` | String |  |  | trường ẩn | SPLCD |
| 15 | Fee data | fee_data | No | Array Object |  |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Account holder name | account_holder_name | String |  |  |
| 5 | Catalogue code | catalog_code | String |  |  |
| 6 | Catalogue name | catalog_name | String |  |  |
| 7 | Seq number | seq_number | `Number` |  |  |
| 8 | Currency code | currency_code | String |  |  |
| 9 | Credit limit | credit_limit | `Number` |  |  |
| 10 | Deposit account | deposit_account_number | String |  |  |
| 11 | Description | description | String |  |  |
| 12 | Currency of credit account | currency_of_credit_account | String |  |  |
| 13 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 14 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 15 | Sub product limit code | sub_product_limit_code | String |  |  |
| 16 | Transaction status | status | String |  |  |
| 17 | Transaction date |  | `Date time` |  |  |
| 18 | User id |  | `Number` |  |  |
| 19 | Fee data | fee_data | Array Object |  |  |

## 1.2 Transaction flow
**Conditions:**
- Account number: exists with status "Pending to approve".
- Check securing asset collateral:
    - Minimum secured amount = Minimum secured rate * Credit limit.
    - Minimum secured rate is defined on account information. Allow adjust by manual.
- Credit limit smaller or equal `Available amount` of Sub product limit.

**Flow of events:**
- Allow to collect fee amount (if any). Collect fee by deduct deposit's balance.
    - Deposit account: exists and status <> "Normal". Type is current or saving. 
    - Currency of deposit must be same with currency of credit account.
- System will change credit account status to "Normal".
- System will create auto schedule principal and schedule disbursement.
    - Credit type is "Blanket" (B):
        - Interest computation mode is "Fixed" (F):
            - Principal payment:
                - Will generate schedule based on default information. In which: amount = 0.00, only one row, due date = end of tenor.
    - Credit type is "Fixed" (F):
        - Interest computation mode is "Fixed" (F):
            - Principal payment:
                - Will generate schedule based on default information.
        - Interest computation mode is "Principal equally for Fixed" (S):
            - Principal payment: 
                - Will generate schedule based on `payment schedule` information.
        - Interest computation mode is "Compound basic of installment" (C):
            - Principal payment:
                - Will generate schedule based on default information.
            - Interest payment:
                - Will generate schedule based on default information.
        - Interest computation mode is "Payment + Interest equally for compound" (P):
            - Principal payment:
                - Will generate schedule based on `payment schedule` information.
            - Interest payment: 
                - Will generate schedule based on `payment schedule` information.
- If tenors are holiday (Holiday, Sunday, Saturday). System will move backward to the nearest normal working date.
- Allow modify schedule by manual.

**Database:**
| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |
| 1 | Status | Normal |  |
| 2 | Approved by | Teller make transaction |  |
| 3 | Last transaction date | Working date |  |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>
- Case collect fee (fee amount > 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Total fee amount | Currency of deposit account |
| 1 | C | IFCC (income) | Total fee amount | Currency of deposit account |

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    - Credit account status "Pending to approve".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Customer type | customer_type | String |  |  |
| 4 | Account holder name | account_holder_name | String |  |  |
| 5 | Catalogue code | catalog_code | String |  |  |
| 6 | Catalogue name | catalog_name | String |  |  |
| 7 | Seq number | seq_number | `Number` |  |  |
| 8 | Currency code | currency_code | String |  |  |
| 9 | Credit limit | credit_limit | `Number` |  |  |
| 10 | Deposit account | deposit_account_number | String |  |  |
| 11 | Description | description | String |  |  |
| 12 | Currency of credit account | currency_of_credit_account | String |  |  |
| 13 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 14 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 15 | Sub product limit code | sub_product_limit_code | String |  |  |
| 16 | Transaction status | status | String |  |  |
| 17 | Transaction date |  | `Date time` |  |  |
| 18 | User id |  | `Number` |  |  |
| 19 | Fee data |  | Array Json Object |  |  |
|  | IFC code |  | `Number` |  |  |
|  | Value type |  | String |  |  |
|  | Value |  | `Number` |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  | số có hai số thập phân |
|  | Floor |  | `Number` |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |
| 20 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_CACNM | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |
| 3 | GET_INFO_CCTMT | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |
| 4 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 5 | GET_INFO_ACNAME | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>