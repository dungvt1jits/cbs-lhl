# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_CLA`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Current credit limit | current_credit_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 3 | Adjustment amount | adjustment_amount | `Yes` | `Number` |  | <> 0 | số có hai số thập phân, cho phép nhập < 0.00 và > 0.00 | CAMT2 |
| 4 | New credit amount | new_credit_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT1 |
| 5 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 6 | Limit effective from | limit_effective_from | `Yes` | `Date time` |  | Working date | trường ẩn | CDT1 |
| 7 | Limit effective to | limit_effective_to | `Yes` | `Date time` |  | Working date | trường ẩn | CDT2 |
| 8 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | CVLDT |
| 9 | Currency of credit account | currency_of_credit_account | No | String | 3 |  | trường ẩn | CCCR |
| 10 | Amount (Debit account/BCY) | amount | No | `Number` |  | 0 | trường ẩn | CCVT |
| 11 | Exchange rate (Debit account/BCY) | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 12 | Sub product limit code | sub_product_limit_code | No | String |  |  | trường ẩn | SPLCD |
| 13 | product limit code | product_limit_code | No | String |  |  | trường ẩn | PLCD |
| 14 | Customer code | customer_code | No | String | 15 |  | trường ẩn | CCTMCD |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Current credit limit | current_credit_limit | `Number` |  | số có hai số thập phân |
| 4 | Adjustment amount | adjustment_amount | `Number` |  | số có hai số thập phân |
| 5 | New credit amount | new_credit_amount | `Number` |  | số có hai số thập phân |
| 6 | Description | description | String |  |  |
| 7 | Limit effective from | limit_effective_from | `Date time` |  |  |
| 8 | Limit effective to | limit_effective_to | `Date time` |  |  |
| 9 | Value date | value_date | `Date time` |  |  |
| 10 | Currency of credit account | currency_of_credit_account | String |  |  |
| 11 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 12 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 13 | Sub product limit code | sub_product_limit_code | String |  |  |
| 14 | product limit code | product_limit_code | String |  |  |
| 15 | Customer code | customer_code | String |  |  |
| 16 | Transaction date |  | `Date time` |  |  |
| 17 | User id |  | String |  |  |
| 18 | Transaction status |  | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists with status "Normal".

**Flow of events:**
- Allow adjustment increase, that "New limit" is smaller or equal than sub product limit ([refer transaction flow: Credit](Specification/Common/05 Transaction Flow Credit)).
- Allow adjustment decrease, that "New limit" is larger or equal than amount was disbursed ([refer transaction flow: Credit](Specification/Common/05 Transaction Flow Credit)).
- Credit account with interest mode is Fixed (F) and Compound (C) will be re-gen the schedule.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Current credit limit | current_credit_limit | `Number` |  | số có hai số thập phân |
| 4 | Adjustment amount | adjustment_amount | `Number` |  | số có hai số thập phân |
| 5 | New credit amount | new_credit_amount | `Number` |  | số có hai số thập phân |
| 6 | Description | description | String |  |  |
| 7 | Limit effective from | limit_effective_from | `Date time` |  |  |
| 8 | Limit effective to | limit_effective_to | `Date time` |  |  |
| 9 | Value date | value_date | `Date time` |  |  |
| 10 | Currency of credit account | currency_of_credit_account | String |  |  |
| 11 | Amount (Debit account/BCY) | amount | `Number` |  |  |
| 12 | Exchange rate (Debit account/BCY) | exchange_rate | `Number` |  |  |
| 13 | Sub product limit code | sub_product_limit_code | String |  |  |
| 14 | product limit code | product_limit_code | String |  |  |
| 15 | Customer code | customer_code | String |  |  |
| 16 | Transaction date |  | `Date time` |  |  |
| 17 | User id |  | String |  |  |
| 18 | Transaction status |  | String |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CACNM | CRD_CLA_GET_INFO_CACNM | [CRD_CLA_GET_INFO_CACNM](Specification/Common/13 Credit Rulefuncs) |
| 2 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>