# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction date |  | `Yes` | `Date time` |  | Working date |  |
| 2 | User id |  | `Yes` | String |  | User login |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Transaction date |  | `Date` |  |  |  |
| 3 | User id |  | String |  |  |  |
| 4 | Transaction status |  | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Working date is due date of Interest/ Principal

**Flow of events:**
- If due date is holiday (Holiday, Sunday, Saturday). Then moving backward to the nearest normal working date.
- Transfer interest accrual to interest due:
    - Insert one row interest due on schedule interest.
- Transfer all amount that tenor of principal (< = working date) to principal due. 
- Transaction handles auto by system:
    - Interest due increase.
    - Principal due increase.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |