# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_CLS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Current group | current_group | `Yes` | `Number` |  | 0 |  | G1 |
| 3 | Current provision amount of interest | current_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_I1 |
| 4 | End provision amount of interest | end_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | PROV_I2 |
| 5 | Adjust provision amount of interest | adjust_provision_amount_of_interest | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT |
| 6 | On balance sheet int. amount | on_balance_sheet_int_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | SPINT |
| 7 | Creditor name | creditor_name | No | String | 250 |  |  | CACNM |
| 8 | Creditor code | creditor_code | No | String | 15 |  |  | CCTMCD |
| 9 | Creditor address | creditor_address | No | String | 250 |  |  | CCTMA |
| 10 | Creditor description | creditor_description | No | JSON Object |  |  |  | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 11 | Description | description | No | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Current group | current_group | `Number` |  |  |
| 4 | Current provision amount of interest | current_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 5 | End provision amount of interest | end_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 6 | Adjust provision amount of interest | adjust_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 7 | On balance sheet int. amount | on_balance_sheet_int_amount | `Number` |  | số có hai số thập phân |
| 8 | Creditor name | creditor_name | String |  |  |
| 9 | Creditor code | creditor_code | String |  |  |
| 10 | Creditor address | creditor_address | No | String |  |  |
| 11 | Creditor description | creditor_description | No | JSON Object |  |  |
|  | Home | home | No | String |  |  |
|  | Office | office | No | String |  |  |
| 12 | Description | description | String |  |  |
| 12 | Transaction date |  | `Date time` |  |  |
| 13 | User id |  | `Number` |  |  |
| 14 | Transaction status | status | String |  |  |
| 16 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Credit account number: exists with status "Normal", "Pending to approve".
- Repay all outstanding and interest amount.
- Release all assets that was secured by collateral.
- Credit account number and teller make transaction must be in the same branch.

**Flow of events:**
- When complete transaction: credit account status is "Close".
- Reverse provision interest if it has. (Because, provision interest at end of month, or change classification).

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>
- Product limit = Non shared

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | COMMITMENT_LIMIT | Total amount was disbursed * FX rate (Book rate) | Product limit's currency |
| 1 | C | COMMITMENT_REL | Total amount was disbursed * FX rate (Book rate) | Product limit's currency |

- Adjust provision interest > 0

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | PROV_I_DR {K=0,1,2,3,4} | adjust provision amount of interest | Currency of credit account |
| 1 | C | PROV_I_CR {K=0,1,2,3,4} | adjust provision amount of interest | Currency of credit account |

- Adjust provision interest < 0

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | PROV_I_DR_RVS {K=0,1,2,3,4} | adjust provision amount of interest | Currency of credit account |
| 1 | C | PROV_I_CR_RVS {K=0,1,2,3,4} | adjust provision amount of interest | Currency of credit account |

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Current group | current_group | String |  |  |
| 4 | Current provision amount of interest | current_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 5 | End provision amount of interest | end_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 6 | Adjust provision amount of interest | adjust_provision_amount_of_interest | `Number` |  | số có hai số thập phân |
| 7 | On balance sheet int. amount | on_balance_sheet_int_amount | `Number` |  | số có hai số thập phân |
| 8 | Creditor name | creditor_name | String |  |  |
| 9 | Creditor code | creditor_code | String |  |  |
| 10 | Creditor address | creditor_address | No | String |  |  |
| 11 | Creditor description | creditor_description | No | JSON Object |  |  |
|  | Home | home | No | String |  |  |
|  | Office | office | No | String |  |  |
| 12 | Description | description | String |  |  |
| 12 | Transaction date |  | `Date time` |  |  |
| 13 | User id |  | `Number` |  |  |
| 14 | Transaction status | status | String |  |  |
| 16 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**
- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_CACNM | CRD_GET_INFO_BCY_CACNM | [CRD_GET_INFO_BCY_CACNM](Specification/Common/13 Credit Rulefuncs) |
| 2 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_CACNM | CRD_CLS_GET_INFO_CACNM | [CRD_CLS_GET_INFO_CACNM](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>