# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_SP_CREDIT_AP`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String | 15 |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String | 3 |  |
| 4 | Customer code | customer_code | String | 8 |  |
| 5 | Category code | catalog_code | String | 8 |  |
| 6 | Credit type | credit_type | String |  |  |
| 7 | Tenor type | tenor_type | String |  |  |
| 8 | Status | credit_status | `Number` |  | cần check lại |
| 9 | Old a/c no | reference_number | String |  |  |
| 10 | Sub product limit | sub_product_limit_code | String |  |  |
| 11 | Pending approve | approve_status | String |  |  |
| 12 | Transaction id | id | `Number` |  |
| 13 | Account number def | def_account_number | String |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow
- Chỉ hiển thị các thông tin có approve_status = W or approve_status = I.

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_ADV_CREDIT_AP`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String | 15 |  |  |
| 2 | Account name | account_name | No | String |  |  |  |
| 3 | Currency code | currency_code | No | String | 3 |  |  |
| 4 | Customer code | customer_code | No | String | 8 |  |  |
| 5 | Category code | catalog_code | No | String | 8 |  |  |
| 6 | Credit type | credit_type | No | String |  |  |  |
| 7 | Tenor type | tenor_type | No | String |  |  |  |
| 8 | Status | credit_status | No | `Number` |  |  | cần check lại |
| 9 | Old a/c no | reference_number | No | String |  |  |  |
| 10 | Sub product limit | sub_product_limit_code | No | String |  |  |  |
| 11 | Pending approve | approve_status | No | String |  |  |  |
| 12 | Page index | page_index | No | `Number` |  |  |  |
| 13 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String | 15 |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String | 3 |  |
| 4 | Customer code | customer_code | String | 8 |  |
| 5 | Category code | catalog_code | String | 8 |  |
| 6 | Credit type | credit_type | String |  |  |
| 7 | Tenor type | tenor_type | String |  |  |
| 8 | Status | credit_status | `Number` |  | cần check lại |
| 9 | Old a/c no | reference_number | String |  |  |
| 10 | Sub product limit | sub_product_limit_code | String |  |  |
| 11 | Pending approve | approve_status | String |  |  |
| 12 | Transaction id | id | `Number` |  |
| 13 | Account number def | def_account_number | String |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow
- Chỉ hiển thị các thông tin có approve_status = W or approve_status = I.

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. View
- [View by account number def của "Account Information"](Specification/Credit/02 Account Information)

# 4. View user modify
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_GET_INFO_USER_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |

```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User name | user_name | String |  |  |
| 2 | Transaction reference id | tx_reference_id | String |  |  |
| 3 | Notification | notification | String |  |  |
| 4 | Approve status | is_approve | String |  |  |
| 5 | Transaction date | trans_date | `Date time` |  |  |

```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. View modify account information
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |
| 2 | Trans reference id | tx_reference_id | `Yes` | String |  |  |  |
| 3 | Module code | module_code | `Yes` | String |  |  | `CRD` |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 4 | Account name modify | account_name | String |  |  |
| 5 | Ranking status modify | ranking_status | String |  |  |
| 6 | Principal collection tenor modify | principal_tenor | `Number` |  | số nguyên dương |
| 7 | Principal collection tenor unit modify | principal_tenor_unit | String |  |  |
| 8 | Interest collection tenor modify | interest_tenor | `Number` |  | số nguyên dương |
| 9 | Interest collection tenor unit modify | interest_tenor_unit | String |  |  |
| 10 | Fine collection tenor modify | fine_tenor | `Number` |  |  |
| 11 | Fine collection tenor unit modify | fine_tenor_unit | String |  |  |
| 12 | Begin of tenor modify | from_date | `Date time` |  |  |
| 13 | End of tenor modify | to_date | `Date time` |  |  |
| 14 | Loan officer staff id modify | staff_id | `Number` |  |  |
| 15 | Business purpose code modify | business_purpose_code | String |  |  |
| 16 | Limit from third party modify | limit_from_third_party | `Number` |  | số có hai số thập phân |
| 17 | Operative limit from third party modify | operative_limit_from_third_party | `Number` |  | số có hai số thập phân |
| 18 | First date of interest payment modify | interest_first_date | `Date time` |  |  |
| 19 | Secure type modify | secure_type | String |  |  |
| 20 | Minimum secure rate modify | secure_rate | `Number` |  |  |
| 21 | Credit purpose modify | credit_purpose | String |  |  |
| 22 | Disbursement mode modify | disbursement_mode | String |  |  |
| 23 | Is restructured modify | is_restructured | String |  |  |
| 24 | Is provision? modify | is_provision | String |  |  |
| 25 | Provision tenor modify | provision_tenor | `Number` |  |  |
| 26 | Provision tenor unit modify | provision_tenor_unit | String |  |  |
| 27 | Classification option modify | restruct | String |  |  |
| 28 | Principal grace period modify | principal_grace_period | `Number` |  |  |
| 29 | Principal due on holiday modify | holiday_principal_due_on | `Number` |  |  |
| 30 | Interest grace period modify | interest_grace_period | `Number` |  |  |
| 31 | Interest due on holiday modify | holiday_interest_tenor | `Number` |  |  |
| 32 | Fine grace period modify | fine_grace_period | `Number` |  |  |
| 33 | Fine due on holiday modify | holiday_fine_tenor | `Number` |  |  |
| 34 | Discount rate modify | discount_rate | `Number` |  |  |
| 35 | Re discount rate modify | re_discount_rate | `Number` |  |  |
| 36 | Principal provision rate normal modify | principal_provision_rate0 | `Number` |  | số có hai số thập phân |
| 37 | Principal provision rate special mention modify | principal_provision_rate1 | `Number` |  | số có hai số thập phân |
| 38 | Principal provision rate sub standard modify | principal_provision_rate2 | `Number` |  | số có hai số thập phân |
| 39 | Principal provision rate doubtful modify | principal_provision_rate3 | `Number` |  | số có hai số thập phân |
| 40 | Principal provision rate lost modify | principal_provision_rate4 | `Number` |  | số có hai số thập phân |
| 41 | Interest provision rate normal modify | interest_provision_rate0 | `Number` |  | số có hai số thập phân |
| 42 | Interest provision rate special mention modify | interest_provision_rate1 | `Number` |  | số có hai số thập phân |
| 43 | Interest provision rate sub standard modify | interest_provision_rate2 | `Number` |  | số có hai số thập phân |
| 44 | Interest provision rate doubtful modify | interest_provision_rate3 | `Number` |  | số có hai số thập phân |
| 45 | Interest provision rate lost modify | interest_provision_rate4 | `Number` |  | số có hai số thập phân |
| 46 | Remark modify | remark | String |  |  |
| 47 | Reference id modify | reference_id | String |  |  |
| 48 | Provision of other modify | provision_of_other | `Number` |  | số có hai số thập phân |
| 49 | Loan officer staff code modify | staff_code | String |  | `trả thêm` |
| 50 | IFC data | list_ifc_balance | Array Object |  |  |
|  | IFC id | id | `Number` |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | IFC name | ifc_name | String |  |  |
|  | Base value | value_base | String |  |  |
|  | Is linked | is_linked | String |  |  |  |
|  | IFC value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Margin value | margin_value | `Number` |  | số có 5 số thập phân |
|  | Status | ifc_status | String |  |  |
|  | Outstanding | amount | `Number` |  | số có 5 số thập phân |
|  | Paid | paid | `Number` |  | số có 5 số thập phân |
|  | Amount payable | amtpbl | `Number` |  | số có 5 số thập phân |
| 50 | Payment schedule data | payment_list | Array Object |  |  |
|  | From date | from_date | `Date time` |  |  |
|  | To date | to_date | `Date time` |  |  
|  | Amount | amount | `Number` |  |  |
|  | Interest rate | interest_rate | `Number` |  |  |
|  | Grace principal amount | grace_principal_amount | `Number` |  |  |
|  | Modify date | modify_date | `Date time` |  |  |
| 51 | Principal schedule data | principal_list | Array Object |  |  |
|  | Principal payment id | id | `Number` |  |  |
|  | due_no | `Number` |  |  |
|  | Due date | due_date | `Date time` |  |  |
|  | Amount | amount | `Number` |  |  |  |
|  | Paid amount | paid_amount | `Number` |  |  |
| 52 | List notification type | list_notification_type | Array Object |  | chứa danh sách value: SMS (S), Email(E), Push Notification(P) |
| 53 | Notification type | notification_type | String |  | chuỗi cách nhau bởi dấu trị tuyệt đối |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 6. Approve modify
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_APPROVE_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |
| 2 | Trans reference id | tx_reference_id | `Yes` | String |  |  |  |
| 3 | Module code | module_code | `Yes` | String |  |  | `CRD` |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Trans reference id | tx_reference_id | String |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 7. Reject modify
## 7.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_REJECT_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |
| 2 | Trans reference id | tx_reference_id | `Yes` | String |  |  |  |
| 3 | Module code | module_code | `Yes` | String |  |  | `CRD` |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Trans reference id | tx_reference_id | String |  |  |

**Example:**
```json
{
    
}
```

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |