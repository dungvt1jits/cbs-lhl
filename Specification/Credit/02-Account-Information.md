# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditAccount​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_SP_CREDIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    "search_text": "",
    "page_size": 5,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String | 15 |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String | 3 |  |
| 4 | Customer code | customer_code | String | 8 |  |
| 5 | Category code | catalog_code | String | 8 |  |
| 6 | Credit type | credit_type | String |  |  |
| 7 | Tenor type | tenor_type | String |  |  |
| 8 | Status | credit_status | `Number` |  | cần check lại |
| 9 | Old a/c no | reference_number | String |  |  |
| 10 | Sub product limit | sub_product_limit_code | String |  |  |
| 11 | Credit account id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditAccount​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_SEARCH_ADV_CREDIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String | 15 |  |  |
| 2 | Account name | account_name | No | String |  |  |  |
| 3 | Currency code | currency_code | No | String | 3 |  |  |
| 4 | Customer code | customer_code | No | String | 8 |  |  |
| 5 | Category code | catalog_code | No | String | 8 |  |  |
| 6 | Credit type | credit_type | No | String |  |  |  |
| 7 | Tenor type | tenor_type | No | String |  |  |  |
| 8 | Status | credit_status | No | String |  |  |  |
| 9 | Old a/c no | reference_number | No | String |  |  |  |
| 10 | Sub product limit | sub_product_limit_code | No | String |  |  |  |
| 11 | Page index | page_index | No | `Number` |  |  |  |
| 12 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String | 15 |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String | 3 |  |
| 4 | Customer code | customer_code | String | 8 |  |
| 5 | Category code | catalog_code | String | 8 |  |
| 6 | Credit type | credit_type | String |  |  |
| 7 | Tenor type | tenor_type | String |  |  |
| 8 | Status | credit_status | `Number` |  | cần check lại |
| 9 | Old a/c no | reference_number | String |  |  |
| 10 | Sub product limit | sub_product_limit_code | String |  |  |
| 11 | Credit account id | id | `Number` |  |  |

**Example:**
```json
{

}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
- [Tham khảo thông tin chi tiết tại đây](Specification/Credit/13 CRD_OPN Open New Credit Account)<br>

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/CreditAccount​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_CREDIT​`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Credit account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Credit account id | id | `Number` |  |  |
| 2 | Account number def | def_account_number | String |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Application code | application_code | String |  |  |
| 6 | Currency code | currency_code | String |  |  |
| 7 | Secure by currency | secure_currency_code | String |  | không dùng, xem xét xóa đi |
| 8 | Account holder type | customer_type | String |  |  |
| 9 | Customer id | customer_id | `Number` |  |  |
| 10 | Branch id | branch_id | `Number` |  |  |
| 11 | Status | credit_status | `Number` |  | cần check lại |
| 12 | Classification status | classification_status | String |  |  |
| 13 | Realize status | realize_status | String |  |  |
| 14 | Ranking status | ranking_status | String |  |  |
| 15 | Sub product limit code | sub_product_limit_code | String |  |  |
| 16 | Catalogue code | catalog_code | String |  |  |
| 17 | Credit type | credit_type | String |  |  |
| 18 | Tenor type | tenor_type | String |  |  |
| 19 | Credit facility | credit_facility | String |  |  |
| 20 | Principal collection tenor | principal_tenor | `Number` |  | số nguyên dương |
| 21 | Principal collection tenor unit | principal_tenor_unit | String |  |  |
| 22 | Interest collection tenor | interest_tenor | `Number` |  | số nguyên dương |
| 23 | Interest collection tenor unit | interest_tenor_unit | String |  |  |
| 24 | Fine collection tenor | fine_tenor | `Number` |  | số nguyên dương |
| 25 | Fine collection tenor unit | fine_tenor_unit | String |  |  |
| 26 | Begin of tenor | from_date | `Date time` |  |  |
| 27 | End of tenor | to_date | `Date time` |  |  |
| 28 | Open date | open_date | `Date time` |  |  |
| 29 | Close date | close_date | `Date time` |  |  |
| 30 | Last marked date | last_marked_date | `Date time` |  |  |
| 31 | Last transaction date | last_transaction_date | `Date time` |  |  |
| 32 | Off balance sheet date | off_balance_sheet_date | `Date time` |  |  |
| 33 | Sub product | subproduct | String |  |  |
| 34 | Created by | user_created | `Number` |  |  |
| 35 | Approved by | user_approved | `Number` |  |  |
| 36 | Loan officer staff id | staff_id | `Number` |  | `staff_id` |
| 37 | Business purpose code | business_purpose_code | String |  |  |
| 38 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 39 | Earmark limit | earmark_limit | `Number` |  | số có hai số thập phân |
| 40 | Limit effective from | limit_effective_from | `Date time` |  |  |
| 41 | Limit effective to | limit_effective_to | `Date time` |  |  |
| 42 | Limit from third party | limit_from_third_party | `Number` |  | số có hai số thập phân |
| 43 | Operative limit from third party | operative_limit_from_third_party | `Number` |  | số có hai số thập phân |
| 44 | Disbursement amount | disbursement_amount | `Number` |  | số có hai số thập phân |
| 45 | Outstanding balance | balance | `Number` |  | số có hai số thập phân |
| 46 | Installment amount | installment_amount | `Number` |  | số có hai số thập phân |
| 47 | Normal principal amount | normal_principal_amount | `Number` |  | số có hai số thập phân |
| 48 | Due amount | due_amount | `Number` |  | số có hai số thập phân |
| 49 | Rounded amount of principal | rounded_amount_of_principal | `Number` |  | số có hai số thập phân, `ramt` |
| 50 | Write off amount of principal | write_off_amount_of_principal | `Number` |  | số có hai số thập phân |
| 51 | Principal paid amount | principal_paid_amount | `Number` |  | số có hai số thập phân |
| 52 | Write off amount of principal paid | write_off_amount_principal_paid | `Number` |  | số có hai số thập phân |
| 53 | Provision of principal | provision_principal | `Number` |  | số có hai số thập phân |
| 54 | Interest accrual amount | interest_amount | `Number` |  | số có hai số thập phân |
| 55 | Interest receivable | interest_payable | `Number` |  | số có hai số thập phân |
| 56 | Interest prepaid | interest_prepaid | `Number` |  | số có hai số thập phân |
| 57 | Interest due | interest_due | `Number` |  | số có hai số thập phân |
| 58 | On-balance sheet interest | on_balance_sheet_interest | `Number` |  | số có hai số thập phân, `INTSPAMT` |
| 59 | Off-balance sheet interest | off_balance_sheet_interest | `Number` |  | số có hai số thập phân, `INTPBL - INTSPAMT` |
| 60 | Interest paid | interest_paid | `Number` |  | số có hai số thập phân |
| 61 | Write off amount of interest paid | write_off_provision_interest | `Number` |  | số có hai số thập phân |
| 62 | Provision of interest | provision_interest | `Number` |  | số có hai số thập phân |
| 63 | Fine amount | fine_amount | `Number` |  | kiểm tra code, `fnamt` |
| 64 | Fee |  | `Number` |  | kiểm tra code, không dùng, xem xét xóa đi |
| 65 | Month average balance | month_average_amount | `Number` |  | số có hai số thập phân |
| 66 | Quarter average balance | quarter_average_amount | `Number` |  | số có hai số thập phân |
| 67 | Semi-annual average balance | semi_annual_average_amount | `Number` |  | số có hai số thập phân |
| 68 | Year average balance | year_average_amount | `Number` |  | số có hai số thập phân |
| 69 | Week debit | week_debit | `Number` |  | số có hai số thập phân |
| 70 | Week credit | week_credit | `Number` |  | số có hai số thập phân |
| 71 | Month debit | month_debit | `Number` |  | số có hai số thập phân |
| 72 | Month credit | month_credit | `Number` |  | số có hai số thập phân |
| 73 | Quarter debit | quarter_debit | `Number` |  | số có hai số thập phân |
| 74 | Quarter credit | quarter_credit | `Number` |  | số có hai số thập phân |
| 75 | Semi-annual debit | semi_annual_debit | `Number` |  | số có hai số thập phân |
| 76 | Semi-annual credit | semi_annual_credit | `Number` |  | số có hai số thập phân |
| 77 | Year debit | year_debit | `Number` |  | số có hai số thập phân |
| 78 | Year credit | year_credit | `Number` |  | số có hai số thập phân |
| 79 | First date of principal repayment | principal_first_date | `Date time` |  |  |
| 80 | First date of interest payment | interest_first_date | `Date time` |  |  |
| 81 | Is syndicated? | is_syndicated | String |  |  |
| 82 | Interest computation mode | interest_computation_mode | String |  |  |
| 83 | Secure type | secure_type | String |  |  |
| 84 | Minimum secure rate | secure_rate | `Number` |  |  |
| 85 | Credit purpose | credit_purpose | String |  |  |
| 86 | Credit classification | credit_classification | String |  |  |
| 87 | Disbursement mode | disbursement_mode | String |  |  |
| 88 | Is restructured | is_restructured | String |  |  |
| 89 | Is provision? | is_provision | String |  |  |
| 90 | Provision tenor | provision_tenor | `Number` |  |  |
| 91 | Provision tenor unit | provision_tenor_unit | String |  |  |
| 92 | Rollover option | rollover_option | String |  | không dùng, xem xét xóa đi |
| 93 | Classification option | restruct | String |  |  |
| 94 | Principal grace period | principal_grace_period | `Number` |  |  |
| 95 | Principal due on holiday | holiday_principal_due_on | `Number` |  |  |
| 96 | Interest grace period | interest_grace_period | `Number` |  |  |
| 97 | Interest due on holiday | holiday_interest_tenor | `Number` |  |  |
| 98 | Fine grace period | fine_grace_period | `Number` |  |  |
| 99 | Fine due on holiday | holiday_fine_tenor | `Number` |  |  |
| 100 | Discount rate | discount_rate | `Number` |  |  |
| 101 | Re discount rate | re_discount_rate | `Number` |  |  |
| 102 | Principal provision rate normal | principal_provision_rate_0 | `Number` |  | số có hai số thập phân |
| 103 | Principal provision rate special mention | principal_provision_rate_1 | `Number` |  | số có hai số thập phân |
| 104 | Principal provision rate sub standard | principal_provision_rate_2 | `Number` |  | số có hai số thập phân |
| 105 | Principal provision rate doubtful | principal_provision_rate_3 | `Number` |  | số có hai số thập phân |
| 106 | Principal provision rate lost | principal_provision_rate_4 | `Number` |  | số có hai số thập phân |
| 107 | Interest provision rate normal | interest_provision_rate_0 | `Number` |  | số có hai số thập phân |
| 108 | Interest provision rate special mention | interest_provision_rate_1 | `Number` |  | số có hai số thập phân |
| 109 | Interest provision rate sub standard | interest_provision_rate_2 | `Number` |  | số có hai số thập phân |
| 110 | Interest provision rate doubtful | interest_provision_rate_3 | `Number` |  | số có hai số thập phân |
| 111 | Interest provision rate lost | interest_provision_rate_4 | `Number` |  | số có hai số thập phân |
| 112 | Remark | remark | String |  |  |
| 113 | Reference id | reference_number | String |  |  |
| 114 | Provision of other | provision_of_other | `Number` |  | số có hai số thập phân, `fpamt` |
|  | Tariff id | tariff_id | `Number` |  |  |
|  | Commitment limit | commitment_limit | `Number` |  |  |
|  | Customer code | customer_code | String |  | `trả thêm` |
|  | Customer name | customer_name | String |  | `trả thêm` |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Loan officer staff code | staff_code | String |  | `trả thêm` |
|  | Loan officer staff name | staff_name | String |  | `trả thêm` |
|  | Business purpose name | business_purpose_name | String |  | `trả thêm` |
| 115 | List notification type | list_notification_type | Array Object |  | chứa danh sách value: SMS (S), Email(E), Push Notification(P) |
| 116 | Notification type | notification_type | String |  | chuỗi cách nhau bởi dấu trị tuyệt đối |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditAccount​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_UPDATE_CREDIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Credit account id | id | `Yes` | `Number` |  |  | không được sửa |
| 2 | Account name | account_name | `Yes` | String |  |  |  |
| 3 | Ranking status | ranking_status | `Yes` | String |  |  |  |
| 4 | Principal collection tenor | principal_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 5 | Principal collection tenor unit | principal_tenor_unit | `Yes` | String |  |  |  |
| 6 | Interest collection tenor | interest_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 7 | Interest collection tenor unit | interest_tenor_unit | `Yes` | String |  |  |  |
| 8 | Fine collection tenor | fine_tenor | `Yes` | `Number` |  |  |  |
| 9 | Fine collection tenor unit | fine_tenor_unit | `Yes` | String |  |  |  |
| 10 | Begin of tenor | from_date | `Yes` | `Date time` |  |  |  |
| 11 | End of tenor | to_date | `Yes` | `Date time` |  |  |  |
| 12 | Loan officer staff id | staff_id | No | `Number` |  |  |  |
| 13 | Business purpose code | business_purpose_code | `Yes` | String |  |  |  |
| 14 | Limit from third party | limit_from_third_party | `Yes` | `Number` |  |  | số có hai số thập phân |
| 15 | Operative limit from third party | operative_limit_from_third_party | `Yes` | `Number` |  |  | số có hai số thập phân |
| 16 | First date of interest payment | interest_first_date | `Yes` | `Date time` |  |  |  |
| 17 | Secure type | secure_type | `Yes` | String |  |  |  |
| 18 | Minimum secure rate | secure_rate | No | `Number` |  |  |  |
| 19 | Credit purpose | credit_purpose | No | String |  |  |  |
| 20 | Disbursement mode | disbursement_mode | `Yes` | String |  |  |  |
| 21 | Is restructured | is_restructured | `Yes` | String |  |  |  |
| 22 | Is provision? | is_provision | `Yes` | String |  |  |  |
| 23 | Provision tenor | provision_tenor | No | `Number` |  |  |  |
| 24 | Provision tenor unit | provision_tenor_unit | No | String |  |  |  |
| 25 | Classification option | restruct | `Yes` | String |  |  |  |
| 26 | Principal grace period | principal_grace_period | No | `Number` |  |  |  |
| 27 | Principal due on holiday | holiday_principal_due_on | No | `Number` |  |  |  |
| 28 | Interest grace period | interest_grace_period | No | `Number` |  |  |  |
| 29 | Interest due on holiday | holiday_interest_tenor | No | `Number` |  |  |  |
| 30 | Fine grace period | fine_grace_period | No | `Number` |  |  |  |
| 31 | Fine due on holiday | holiday_fine_tenor | No | `Number` |  |  |  |
| 32 | Discount rate | discount_rate | No | `Number` |  |  |  |
| 33 | Re discount rate | re_discount_rate | No | `Number` |  |  |  |
| 34 | Principal provision rate normal | principal_provision_rate_0 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 35 | Principal provision rate special mention | principal_provision_rate_1 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 36 | Principal provision rate sub standard | principal_provision_rate_2 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 37 | Principal provision rate doubtful | principal_provision_rate_3 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 38 | Principal provision rate lost | principal_provision_rate_4 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 39 | Interest provision rate normal | interest_provision_rate_0 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 40 | Interest provision rate special mention | interest_provision_rate_1 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 41 | Interest provision rate sub standard | interest_provision_rate_2 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 42 | Interest provision rate doubtful | interest_provision_rate_3 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 43 | Interest provision rate lost | interest_provision_rate_4 | `Yes` | `Number` |  |  | số có hai số thập phân |
| 44 | Remark | remark | No | String |  |  |  |
| 45 | Reference id | reference_number | No | String |  |  |  |
| 46 | Provision of other | provision_of_other | No | `Number` |  |  | số có hai số thập phân |
| 47 | Account number def | account_number_def | `Yes` | String |  |  | Không được sửa |
| 48 | Module code | module_code | `Yes` | String |  |  | `CRD` |
| 49 | Approve modify | approve_modify | `Yes` | `Boolean` |  |  |  |
| 50 | IFC data | list_ifc_balance | No |  Array Object |  |  |  |
|  | Module code | module_code | No | String |  |  | `DPT` |
|  | IFC code | ifc_code | No | `Number` |  |  | Không được sửa |
|  | Value base | value_base | No | String |  |  | Không được sửa |
|  | IFC value | ifc_value | No | `Number` |  |  | Không được sửa, số có 5 số thập phân |
|  | Margin value | margin_value | No | `Number` |  |  | Được phép sửa, số có 5 số thập phân |
|  | Amount | amount | No | `Number` |  |  | Không được sửa, số có 5 số thập phân |
|  | Paid | paid | No | `Number` |  |  | Không được sửa, số có 5 số thập phân |
|  | Status | ifc_status | No | String |  |  | Được phép sửa |
|  | Last date time | last_datetime | No | String |  |  | lấy ngày working date |
|  | Amount payable | amtpbl | No | `Number` |  |  | Không được sửa, số có 5 số thập phân |
| 51 | Payment schedule data | payment_list | No | Array Object |  |  |  |
|  | From date | from_date | `Yes` | `Date time` |  |  |  |
|  | To date | to_date | `Yes` | `Date time` |  |  |  |
|  | Amount | amount | No | `Number` |  | `Null` |  |
|  | Interest rate | interest_rate | No | `Number` |  | `Null` |  |
|  | Grace principal amount | grace_principal_amount | No | `Number` |  | `Null` |  |
|  | Modify date | modify_date | `Yes` | `Date time` |  |  |  |
| 52 | Principal schedule data | principal_list | No | Array Object |  |  |  |
|  | Principal payment id | id | `Yes` | `Number` |  |  |  |
|  | No | due_no | `Yes` | `Number` |  |  |  |
|  | Due date | due_date | `Yes` | `Date time` |  |  |  |
|  | Amount | amount | `Yes` | `Number` |  |  |  |
|  | Paid amount | paid_amount | `Yes` | `Number` |  |  |  |
| 53 | List notification type | list_notification_type | `Yes` | Array Object |  |  |  chứa danh sách value: SMS (S), Email(E), Push Notification(P) |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Credit account id | id | `Number` |  |  |
| 2 | Account number def | def_account_number | String |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Application code | application_code | String |  |  |
| 6 | Currency code | currency_code | String |  |  |
| 7 | Secure by currency | secure_currency_code | String |  | không dùng, xem xét xóa đi |
| 8 | Account holder type | customer_type | String |  |  |
| 9 | Customer id | customer_id | `Number` |  |  |
| 10 | Branch id | branch_id | `Number` |  |  |
| 11 | Status | credit_status | `Number` |  | cần check lại |
| 12 | Classification status | classification_status | String |  |  |
| 13 | Realize status | realize_status | String |  |  |
| 14 | Ranking status | ranking_status | String |  |  |
| 15 | Sub product limit code | sub_product_limit_code | String |  |  |
| 16 | Catalogue code | catalog_code | String |  |  |
| 17 | Credit type | credit_type | String |  |  |
| 18 | Tenor type | tenor_type | String |  |  |
| 19 | Credit facility | credit_facility | String |  |  |
| 20 | Principal collection tenor | principal_tenor | `Number` |  | số nguyên dương |
| 21 | Principal collection tenor unit | principal_tenor_unit | String |  |  |
| 22 | Interest collection tenor | interest_tenor | `Number` |  | số nguyên dương |
| 23 | Interest collection tenor unit | interest_tenor_unit | String |  |  |
| 24 | Fine collection tenor | fine_tenor | `Number` |  | số nguyên dương |
| 25 | Fine collection tenor unit | fine_tenor_unit | String |  |  |
| 26 | Begin of tenor | from_date | `Date time` |  |  |
| 27 | End of tenor | to_date | `Date time` |  |  |
| 28 | Open date | open_date | `Date time` |  |  |
| 29 | Close date | close_date | `Date time` |  |  |
| 30 | Last marked date | last_marked_date | `Date time` |  |  |
| 31 | Last transaction date | last_transaction_date | `Date time` |  |  |
| 32 | Off balance sheet date | off_balance_sheet_date | `Date time` |  |  |
| 33 | Sub product | subproduct | String |  |  |
| 34 | Created by | user_created | `Number` |  |  |
| 35 | Approved by | user_approved | `Number` |  |  |
| 36 | Loan officer staff id | staff_id | `Number` |  | `staff_id` |
| 37 | Business purpose code | business_purpose_code | String |  |  |
| 38 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 39 | Earmark limit | earmark_limit | `Number` |  | số có hai số thập phân |
| 40 | Limit effective from | limit_effective_from | `Date time` |  |  |
| 41 | Limit effective to | limit_effective_to | `Date time` |  |  |
| 42 | Limit from third party | limit_from_third_party | `Number` |  | số có hai số thập phân |
| 43 | Operative limit from third party | operative_limit_from_third_party | `Number` |  | số có hai số thập phân |
| 44 | Disbursement amount | disbursement_amount | `Number` |  | số có hai số thập phân |
| 45 | Outstanding balance | balance | `Number` |  | số có hai số thập phân |
| 46 | Installment amount | installment_amount | `Number` |  | số có hai số thập phân |
| 47 | Normal principal amount | normal_principal_amount | `Number` |  | số có hai số thập phân |
| 48 | Due amount | due_amount | `Number` |  | số có hai số thập phân |
| 49 | Rounded amount of principal | rounded_amount_of_principal | `Number` |  | số có hai số thập phân, `ramt` |
| 50 | Write off amount of principal | write_off_amount_of_principal | `Number` |  | số có hai số thập phân |
| 51 | Principal paid amount | principal_paid_amount | `Number` |  | số có hai số thập phân |
| 52 | Write off amount of principal paid | write_off_amount_principal_paid | `Number` |  | số có hai số thập phân |
| 53 | Provision of principal | provision_principal | `Number` |  | số có hai số thập phân |
| 54 | Interest accrual amount | interest_amount | `Number` |  | số có hai số thập phân |
| 55 | Interest receivable | interest_payable | `Number` |  | số có hai số thập phân |
| 56 | Interest prepaid | interest_prepaid | `Number` |  | số có hai số thập phân |
| 57 | Interest due | interest_due | `Number` |  | số có hai số thập phân |
| 58 | On-balance sheet interest | on_balance_sheet_interest | `Number` |  | số có hai số thập phân, `INTSPAMT` |
| 59 | Off-balance sheet interest | off_balance_sheet_interest | `Number` |  | số có hai số thập phân, `INTPBL - INTSPAMT` |
| 60 | Interest paid | interest_paid | `Number` |  | số có hai số thập phân |
| 61 | Write off amount of interest paid | write_off_provision_interest | `Number` |  | số có hai số thập phân, `write_off_provision_interest` |
| 62 | Provision of interest | provision_interest | `Number` |  | số có hai số thập phân |
| 63 | Fine amount | fine_amount | `Number` |  | kiểm tra code, `fnamt` |
| 64 | Fee |  | `Number` |  | kiểm tra code, không dùng, xem xét xóa đi |
| 65 | Month average balance | month_average_amount | `Number` |  | số có hai số thập phân |
| 66 | Quarter average balance | quarter_average_amount | `Number` |  | số có hai số thập phân |
| 67 | Semi-annual average balance | semi_annual_average_amount | `Number` |  | số có hai số thập phân |
| 68 | Year average balance | year_average_amount | `Number` |  | số có hai số thập phân |
| 69 | Week debit | week_debit | `Number` |  | số có hai số thập phân |
| 70 | Week credit | week_credit | `Number` |  | số có hai số thập phân |
| 71 | Month debit | month_debit | `Number` |  | số có hai số thập phân |
| 72 | Month credit | month_credit | `Number` |  | số có hai số thập phân |
| 73 | Quarter debit | quarter_debit | `Number` |  | số có hai số thập phân |
| 74 | Quarter credit | quarter_credit | `Number` |  | số có hai số thập phân |
| 75 | Semi-annual debit | semi_annual_debit | `Number` |  | số có hai số thập phân |
| 76 | Semi-annual credit | semi_annual_credit | `Number` |  | số có hai số thập phân |
| 77 | Year debit | year_debit | `Number` |  | số có hai số thập phân |
| 78 | Year credit | year_credit | `Number` |  | số có hai số thập phân |
| 79 | First date of principal repayment | principal_first_date | `Date time` |  |  |
| 80 | First date of interest payment | interest_first_date | `Date time` |  |  |
| 81 | Is syndicated? | is_syndicated | String |  |  |
| 82 | Interest computation mode | interest_computation_mode | String |  |  |
| 83 | Secure type | secure_type | String |  |  |
| 84 | Minimum secure rate | secure_rate | `Number` |  |  |
| 85 | Credit purpose | credit_purpose | String |  |  |
| 86 | Credit classification | credit_classification | String |  |  |
| 87 | Disbursement mode | disbursement_mode | String |  |  |
| 88 | Is restructured | is_restructured | String |  |  |
| 89 | Is provision? | is_provision | String |  |  |
| 90 | Provision tenor | provision_tenor | `Number` |  |  |
| 91 | Provision tenor unit | provision_tenor_unit | String |  |  |
| 92 | Rollover option | rollover_option | String |  | không dùng, xem xét xóa đi |
| 93 | Classification option | restruct | String |  |  |
| 94 | Principal grace period | principal_grace_period | `Number` |  |  |
| 95 | Principal due on holiday | holiday_principal_due_on | `Number` |  |  |
| 96 | Interest grace period | interest_grace_period | `Number` |  |  |
| 97 | Interest due on holiday | holiday_interest_tenor | `Number` |  |  |
| 98 | Fine grace period | fine_grace_period | `Number` |  |  |
| 99 | Fine due on holiday | holiday_fine_tenor | `Number` |  |  |
| 100 | Discount rate | discount_rate | `Number` |  |  |
| 101 | Re discount rate | re_discount_rate | `Number` |  |  |
| 102 | Principal provision rate normal | principal_provision_rate_0 | `Number` |  | số có hai số thập phân |
| 103 | Principal provision rate special mention | principal_provision_rate_1 | `Number` |  | số có hai số thập phân |
| 104 | Principal provision rate sub standard | principal_provision_rate_2 | `Number` |  | số có hai số thập phân |
| 105 | Principal provision rate doubtful | principal_provision_rate_3 | `Number` |  | số có hai số thập phân |
| 106 | Principal provision rate lost | principal_provision_rate_4 | `Number` |  | số có hai số thập phân |
| 107 | Interest provision rate normal | interest_provision_rate_0 | `Number` |  | số có hai số thập phân |
| 108 | Interest provision rate special mention | interest_provision_rate_1 | `Number` |  | số có hai số thập phân |
| 109 | Interest provision rate sub standard | interest_provision_rate_2 | `Number` |  | số có hai số thập phân |
| 110 | Interest provision rate doubtful | interest_provision_rate_3 | `Number` |  | số có hai số thập phân |
| 111 | Interest provision rate lost | interest_provision_rate_4 | `Number` |  | số có hai số thập phân |
| 112 | Remark | remark | String |  |  |
| 113 | Reference id | reference_number | String |  |  |
| 114 | Provision of other | provision_of_other | `Number` |  | số có hai số thập phân, `fpamt` |
|  | Tariff id | tariff_id | `Number` |  | không hiển thị trên giao diện |
|  | Operative credit limit of the account - currently assigned | operative_limit | `Number` |  | không hiển thị trên giao diện, `opcrlimit` |
|  | Total overdue amount | overdue_amount | `Number` |  | không hiển thị trên giao diện, `oamt` |
|  | Overdue amount apply for penalty interest calculation | overdue_interest_amount | `Number` |  | không hiển thị trên giao diện, `oiamt` |
|  | Day for principal payment every month | principal_day | `Number` |  | không hiển thị trên giao diện, `pday` |
|  | Late payment amount of principal | late_principal_amount | `Number` |  | apply for fine on late payment, không hiển thị trên giao diện, `lamt` |
|  | Interest overdue | interest_overdue | `Number` |  | không hiển thị trên giao diện, `intovd` |
|  | Fine paid | fine_paid | `Number` |  | không hiển thị trên giao diện, `fnpaid` |
|  | Day for interest payment every month | interest_day | `Number` |  | không hiển thị trên giao diện, `iday` |
|  | Late payment amount of interest | late_interest_amount | `Number` |  | apply for fine on late payment, không hiển thị trên giao diện, `lintamt` |
|  | Rounded amount of interest | rounded_interest | `Number` |  | không hiển thị trên giao diện, `riamt` |
|  | Write off amount of interest | write_off_amount_interest | `Number` |  | không hiển thị trên giao diện, `woiamt` |
|  | User define field | user_define_field | `Number` |  | không hiển thị trên giao diện, Reserve, json structure, `udfield_1` |
|  | acrt | acrt | `Number` |  | không hiển thị trên giao diện, có sử dụng không được xóa |
|  | plrt | plrt | `Number` |  | không hiển thị trên giao diện, có sử dụng không được xóa |
|  | splrt | splrt | `Number` |  | không hiển thị trên giao diện, có sử dụng không được xóa |
|  | Commitment limit | commitment_limit | `Number` |  |  |
|  | Commitment limit | commitment_limit | `Number` |  |  |
| 115 | List notification type | list_notification_type | Array Object |  | chứa danh sách value: SMS (S), Email(E), Push Notification(P) |
| 116 | Notification type | notification_type | String |  | chuỗi cách nhau bởi dấu trị tuyệt đối |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CreditAccount​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_DELETE_CREDIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Credit account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 1
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Credit account id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 1
}
```

## 6.2 Transaction flow
- Chỉ được delete credit account có status là Pending to approve (P)

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list signature by customer code
- [Get information của "Customer Media Files](Specification/Customer/04 Customer Media Files)

# 8. Get list IFC by credit account number def
## 8.1 Field description
### Request message
**HTTP Method:** ``

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_LISTIFCBYDEFACCNO_IFCBALANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |
| 2 | Module code | module_code | `Yes` | String |  |  | `CRD` |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |
| 4 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC id | id | `Number` |  |  |
| 2 | IFC code | ifc_code | `Number` |  |  |
| 3 | IFC name | ifc_name | String |  |  |
| 4 | Base value | value_base | String |  |  |
| 5 | Is linked | is_linked | String |  |  |  |
| 6 | IFC value | ifc_value | `Number` |  | số có 5 số thập phân |
| 7 | Margin value | margin_value | `Number` |  | số có 5 số thập phân |
| 8 | Status | ifc_status | String |  |  |
| 9 | Outstanding | amount | `Number` |  | số có 5 số thập phân |
| 10 | Paid | paid | `Number` |  | số có 5 số thập phân |

**Example:**
```json
{
    
}
```

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |

# 9. Get list payment schedule by credit account number def
## 9.1 Field description
### Request message
**HTTP Method:** ``

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_PAYMENT_SCHEDULE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Payment schedule data | payment_list | Array Object |  |  |
| 1 | Payment schedule id | id | `Number` |  |  |
| 2 | From date | from_date | `Date time` |  |  |
| 3 | To date | to_date | `Date time` |  |  |
| 4 | Amount | amount | `Number` |  |  |
| 5 | Interest rate | interest_rate | `Number` |  |  |
| 6 | Grace principal amount | grace_principal_amount | `Number` |  |  |
| 7 | Modify date | modify_date | `Date time` |  |  |

**Example:**
```json
{
    
}
```

## 9.2 Transaction flow

## 9.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |

# 10. Get list disbursement schedule by credit account number def
## 10.1 Field description
### Request message
**HTTP Method:** ``

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_DISBURSEMENT_SCHEDULE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Disbursement schedule data | disbursement_list | Array Object |  |  |
| 1 | Disbursement schedule id | id | `Number` |  |  |
| 2 | From date | from_date | `Date time` |  |  |
| 3 | To date | to_date | `Date time` |  |  |
| 4 | Disbursement amount | disbursement_amount | `Number` |  | `là trường Amount trên UI` |
| 5 | Unutilized amount | unutilized_amount | `Number` |  |  |
| 6 | Amount | amount | `Number` |  | `disbursement_amount - unutilized_amount` `là trường Disbursement amount trên UI` |

**Example:**
```json
{
    
}
```

## 10.2 Transaction flow

## 10.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |

# 11. Get list principal payment by credit account number def
## 11.1 Field description
### Request message
**HTTP Method:** ``

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_PRINCIPAL_PAYMENT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Principal payment data | principal_list | Array Object |  |  |
| 1 | Principal payment id | id | `Number` |  |  |
| 2 | No | due_number | `Number` |  |  |
| 3 | Due date | due_date | `Date time` |  |  |
| 4 | Amount | amount | `Number` |  |  |
| 5 | Paid amount | paid_amount | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 11.2 Transaction flow

## 11.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |

# 12. Get list interest payment by credit account number def
## 12.1 Field description
### Request message
**HTTP Method:** ``

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEW_INTEREST_PAYMENT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Interest payment data | interest_list | Array Object |  |  |
| 1 | Interest payment id | id | `Number` |  |  |
| 2 | No | due_number | `Number` |  |  |
| 3 | Due date | due_date | `Date time` |  |  |
| 4 | Amount | amount | `Number` |  |  |
| 5 | Paid amount | paid_amount | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 12.2 Transaction flow

## 12.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |

# 13. View by account number def
## 13.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/DepositAccount/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CRD_VIEWBYDEFACNO_CREDIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |

**Example:**
```json
{
    "account_number_def": ""
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Credit account id | id | `Number` |  |  |
| 2 | Account number def | def_account_number | String |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Application code | application_code | String |  |  |
| 6 | Currency code | currency_code | String |  |  |
| 7 | Secure by currency | secure_currency_code | String |  | không dùng, xem xét xóa đi |
| 8 | Account holder type | customer_type | String |  |  |
| 9 | Customer id | customer_id | `Number` |  |  |
| 10 | Branch id | branch_id | `Number` |  |  |
| 11 | Status | credit_status | `Number` |  | cần check lại |
| 12 | Classification status | classification_status | String |  |  |
| 13 | Realize status | realize_status | String |  |  |
| 14 | Ranking status | ranking_status | String |  |  |
| 15 | Sub product limit code | sub_product_limit_code | String |  |  |
| 16 | Catalogue code | catalog_code | String |  |  |
| 17 | Credit type | credit_type | String |  |  |
| 18 | Tenor type | tenor_type | String |  |  |
| 19 | Credit facility | credit_facility | String |  |  |
| 20 | Principal collection tenor | principal_tenor | `Number` |  | số nguyên dương |
| 21 | Principal collection tenor unit | principal_tenor_unit | String |  |  |
| 22 | Interest collection tenor | interest_tenor | `Number` |  | số nguyên dương |
| 23 | Interest collection tenor unit | interest_tenor_unit | String |  |  |
| 24 | Fine collection tenor | fine_tenor | `Number` |  | số nguyên dương |
| 25 | Fine collection tenor unit | fine_tenor_unit | String |  |  |
| 26 | Begin of tenor | from_date | `Date time` |  |  |
| 27 | End of tenor | to_date | `Date time` |  |  |
| 28 | Open date | open_date | `Date time` |  |  |
| 29 | Close date | close_date | `Date time` |  |  |
| 30 | Last marked date | last_marked_date | `Date time` |  |  |
| 31 | Last transaction date | last_transaction_date | `Date time` |  |  |
| 32 | Off balance sheet date | off_balance_sheet_date | `Date time` |  |  |
| 33 | Sub product | subproduct | String |  |  |
| 34 | Created by | user_created | `Number` |  |  |
| 35 | Approved by | user_approved | `Number` |  |  |
| 36 | Loan officer staff id | staff_id | `Number` |  | `staff_id` |
| 37 | Business purpose code | business_purpose_code | String |  |  |
| 38 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 39 | Earmark limit | earmark_limit | `Number` |  | số có hai số thập phân |
| 40 | Limit effective from | limit_effective_from | `Date time` |  |  |
| 41 | Limit effective to | limit_effective_to | `Date time` |  |  |
| 42 | Limit from third party | limit_from_third_party | `Number` |  | số có hai số thập phân |
| 43 | Operative limit from third party | operative_limit_from_third_party | `Number` |  | số có hai số thập phân |
| 44 | Disbursement amount | disbursement_amount | `Number` |  | số có hai số thập phân |
| 45 | Outstanding balance | balance | `Number` |  | số có hai số thập phân |
| 46 | Installment amount | installment_amount | `Number` |  | số có hai số thập phân |
| 47 | Normal principal amount | normal_principal_amount | `Number` |  | số có hai số thập phân |
| 48 | Due amount | due_amount | `Number` |  | số có hai số thập phân |
| 49 | Rounded amount of principal | rounded_amount_of_principal | `Number` |  | số có hai số thập phân, `ramt` |
| 50 | Write off amount of principal | write_off_amount_of_principal | `Number` |  | số có hai số thập phân |
| 51 | Principal paid amount | principal_paid_amount | `Number` |  | số có hai số thập phân |
| 52 | Write off amount of principal paid | write_off_amount_principal_paid | `Number` |  | số có hai số thập phân |
| 53 | Provision of principal | provision_principal | `Number` |  | số có hai số thập phân |
| 54 | Interest accrual amount | interest_amount | `Number` |  | số có hai số thập phân |
| 55 | Interest receivable | interest_payable | `Number` |  | số có hai số thập phân |
| 56 | Interest prepaid | interest_prepaid | `Number` |  | số có hai số thập phân |
| 57 | Interest due | interest_due | `Number` |  | số có hai số thập phân |
| 58 | On-balance sheet interest | on_balance_sheet_interest | `Number` |  | số có hai số thập phân, `INTSPAMT` |
| 59 | Off-balance sheet interest | off_balance_sheet_interest | `Number` |  | số có hai số thập phân, `INTPBL - INTSPAMT` |
| 60 | Interest paid | interest_paid | `Number` |  | số có hai số thập phân |
| 61 | Write off amount of interest paid | write_off_provision_interest | `Number` |  | số có hai số thập phân, `write_off_provision_interest` |
| 62 | Provision of interest | provision_interest | `Number` |  | số có hai số thập phân |
| 63 | Fine amount | fine_amount | `Number` |  | kiểm tra code, `fnamt` |
| 64 | Fee |  | `Number` |  | kiểm tra code, không dùng, xem xét xóa đi |
| 65 | Month average balance | month_average_amount | `Number` |  | số có hai số thập phân |
| 66 | Quarter average balance | quarter_average_amount | `Number` |  | số có hai số thập phân |
| 67 | Semi-annual average balance | semi_annual_average_amount | `Number` |  | số có hai số thập phân |
| 68 | Year average balance | year_average_amount | `Number` |  | số có hai số thập phân |
| 69 | Week debit | week_debit | `Number` |  | số có hai số thập phân |
| 70 | Week credit | week_credit | `Number` |  | số có hai số thập phân |
| 71 | Month debit | month_debit | `Number` |  | số có hai số thập phân |
| 72 | Month credit | month_credit | `Number` |  | số có hai số thập phân |
| 73 | Quarter debit | quarter_debit | `Number` |  | số có hai số thập phân |
| 74 | Quarter credit | quarter_credit | `Number` |  | số có hai số thập phân |
| 75 | Semi-annual debit | semi_annual_debit | `Number` |  | số có hai số thập phân |
| 76 | Semi-annual credit | semi_annual_credit | `Number` |  | số có hai số thập phân |
| 77 | Year debit | year_debit | `Number` |  | số có hai số thập phân |
| 78 | Year credit | year_credit | `Number` |  | số có hai số thập phân |
| 79 | First date of principal repayment | principal_first_date | `Date time` |  |  |
| 80 | First date of interest payment | interest_first_date | `Date time` |  |  |
| 81 | Is syndicated? | is_syndicated | String |  |  |
| 82 | Interest computation mode | interest_computation_mode | String |  |  |
| 83 | Secure type | secure_type | String |  |  |
| 84 | Minimum secure rate | secure_rate | `Number` |  |  |
| 85 | Credit purpose | credit_purpose | String |  |  |
| 86 | Credit classification | credit_classification | String |  |  |
| 87 | Disbursement mode | disbursement_mode | String |  |  |
| 88 | Is restructured | is_restructured | String |  |  |
| 89 | Is provision? | is_provision | String |  |  |
| 90 | Provision tenor | provision_tenor | `Number` |  |  |
| 91 | Provision tenor unit | provision_tenor_unit | String |  |  |
| 92 | Rollover option | rollover_option | String |  | không dùng, xem xét xóa đi |
| 93 | Classification option | restruct | String |  |  |
| 94 | Principal grace period | principal_grace_period | `Number` |  |  |
| 95 | Principal due on holiday | holiday_principal_due_on | `Number` |  |  |
| 96 | Interest grace period | interest_grace_period | `Number` |  |  |
| 97 | Interest due on holiday | holiday_interest_tenor | `Number` |  |  |
| 98 | Fine grace period | fine_grace_period | `Number` |  |  |
| 99 | Fine due on holiday | holiday_fine_tenor | `Number` |  |  |
| 100 | Discount rate | discount_rate | `Number` |  |  |
| 101 | Re discount rate | re_discount_rate | `Number` |  |  |
| 102 | Principal provision rate normal | principal_provision_rate_0 | `Number` |  | số có hai số thập phân |
| 103 | Principal provision rate special mention | principal_provision_rate_1 | `Number` |  | số có hai số thập phân |
| 104 | Principal provision rate sub standard | principal_provision_rate_2 | `Number` |  | số có hai số thập phân |
| 105 | Principal provision rate doubtful | principal_provision_rate_3 | `Number` |  | số có hai số thập phân |
| 106 | Principal provision rate lost | principal_provision_rate_4 | `Number` |  | số có hai số thập phân |
| 107 | Interest provision rate normal | interest_provision_rate_0 | `Number` |  | số có hai số thập phân |
| 108 | Interest provision rate special mention | interest_provision_rate_1 | `Number` |  | số có hai số thập phân |
| 109 | Interest provision rate sub standard | interest_provision_rate_2 | `Number` |  | số có hai số thập phân |
| 110 | Interest provision rate doubtful | interest_provision_rate_3 | `Number` |  | số có hai số thập phân |
| 111 | Interest provision rate lost | interest_provision_rate_4 | `Number` |  | số có hai số thập phân |
| 112 | Remark | remark | String |  |  |
| 113 | Reference id | reference_number | String |  |  |
| 114 | Provision of other | provision_of_other | `Number` |  | số có hai số thập phân, `fpamt` |
|  | Tariff id | tariff_id | `Number` |  |  |
|  | Commitment limit | commitment_limit | `Number` |  |  |
|  | Customer code | customer_code | String |  | `trả thêm` |
|  | Customer name | customer_name | String |  | `trả thêm` |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Loan officer staff code | staff_code | String |  | `trả thêm` |
|  | Loan officer staff name | staff_name | String |  | `trả thêm` |
|  | Business purpose name | business_purpose_name | String |  | `trả thêm` |
| 115 | List notification type | list_notification_type | Array Object |  | chứa danh sách value: SMS (S), Email(E), Push Notification(P) |
| 116 | Notification type | notification_type | String |  | chuỗi cách nhau bởi dấu trị tuyệt đối |

**Example:**
```json
{
    
}
```

## 13.2 Transaction flow

## 13.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# Business rule and constrains
- This part shows the brief description of some information of credit account:
    - Credit type: Fixed or Blanket
    - With fixed credit type, customer can drawdown up to total of credit limit. With Blanket credit type, when customer pay for principal, drawdown amount will be automatic increase. So, customer can use credit limit with revolving drawdown and payment. With Hybrid credit type, first credit account will be fixed, and then will be blanket or vice versa.
    - Tenor type: Short term, Medium type and Long term. This field for loan grouping by term. Short term for below 1-year account, Medium for 1-3 years. And Long term for over 3 years.
    - For syndicate loan: Loan account will be sponsored and provided by group of bank parties (include master and member party). 
    - Interest computation mode: F: Fixed, B: Flat basic of installment, C: Compound basic of installment, D: Daily rest, M: Monthly rest, Q: Quarterly rest, H: Half-anniversary rest, Y: Yearly rest
    - With Fixed: interest calculate on daily basic and base on outstanding amount of principal.

# 14. Get notification
## 14.1 Field description
### Request message

**Workflow id:** `CRD_GET_NOTI`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | `Yes` | String |  |  |  |

**Example:**
```json
{
    "account_number": "099901025071765"
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Credit account id | id | `Number` |  |  |
| 2 | Account number def | def_account_number | String |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Application code | application_code | String |  |  |
| 6 | Currency code | currency_code | String |  |  |
| 7 | Secure by currency | secure_currency_code | String |  | không dùng, xem xét xóa đi |
| 8 | Account holder type | customer_type | String |  |  |
| 9 | Customer id | customer_id | `Number` |  |  |
| 10 | Branch id | branch_id | `Number` |  |  |
| 11 | Status | credit_status | `Number` |  | cần check lại |
| 12 | Classification status | classification_status | String |  |  |
| 13 | Realize status | realize_status | String |  |  |
| 14 | Ranking status | ranking_status | String |  |  |
| 15 | Sub product limit code | sub_product_limit_code | String |  |  |
| 16 | Catalogue code | catalog_code | String |  |  |
| 17 | Credit type | credit_type | String |  |  |
| 18 | Tenor type | tenor_type | String |  |  |
| 19 | Credit facility | credit_facility | String |  |  |
| 20 | Principal collection tenor | principal_tenor | `Number` |  | số nguyên dương |
| 21 | Principal collection tenor unit | principal_tenor_unit | String |  |  |
| 22 | Interest collection tenor | interest_tenor | `Number` |  | số nguyên dương |
| 23 | Interest collection tenor unit | interest_tenor_unit | String |  |  |
| 24 | Fine collection tenor | fine_tenor | `Number` |  | số nguyên dương |
| 25 | Fine collection tenor unit | fine_tenor_unit | String |  |  |
| 26 | Begin of tenor | from_date | `Date time` |  |  |
| 27 | End of tenor | to_date | `Date time` |  |  |
| 28 | Open date | open_date | `Date time` |  |  |
| 29 | Close date | close_date | `Date time` |  |  |
| 30 | Last marked date | last_marked_date | `Date time` |  |  |
| 31 | Last transaction date | last_transaction_date | `Date time` |  |  |
| 32 | Off balance sheet date | off_balance_sheet_date | `Date time` |  |  |
| 33 | Sub product | subproduct | String |  |  |
| 34 | Created by | user_created | `Number` |  |  |
| 35 | Approved by | user_approved | `Number` |  |  |
| 36 | Loan officer staff id | staff_id | `Number` |  | `staff_id` |
| 37 | Business purpose code | business_purpose_code | String |  |  |
| 38 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 39 | Earmark limit | earmark_limit | `Number` |  | số có hai số thập phân |
| 40 | Limit effective from | limit_effective_from | `Date time` |  |  |
| 41 | Limit effective to | limit_effective_to | `Date time` |  |  |
| 42 | Limit from third party | limit_from_third_party | `Number` |  | số có hai số thập phân |
| 43 | Operative limit from third party | operative_limit_from_third_party | `Number` |  | số có hai số thập phân |
| 44 | Disbursement amount | disbursement_amount | `Number` |  | số có hai số thập phân |
| 45 | Outstanding balance | balance | `Number` |  | số có hai số thập phân |
| 46 | Installment amount | installment_amount | `Number` |  | số có hai số thập phân |
| 47 | Normal principal amount | normal_principal_amount | `Number` |  | số có hai số thập phân |
| 48 | Due amount | due_amount | `Number` |  | số có hai số thập phân |
| 49 | Rounded amount of principal | rounded_amount_of_principal | `Number` |  | số có hai số thập phân, `ramt` |
| 50 | Write off amount of principal | write_off_amount_of_principal | `Number` |  | số có hai số thập phân |
| 51 | Principal paid amount | principal_paid_amount | `Number` |  | số có hai số thập phân |
| 52 | Write off amount of principal paid | write_off_amount_principal_paid | `Number` |  | số có hai số thập phân |
| 53 | Provision of principal | provision_principal | `Number` |  | số có hai số thập phân |
| 54 | Interest accrual amount | interest_amount | `Number` |  | số có hai số thập phân |
| 55 | Interest receivable | interest_payable | `Number` |  | số có hai số thập phân |
| 56 | Interest prepaid | interest_prepaid | `Number` |  | số có hai số thập phân |
| 57 | Interest due | interest_due | `Number` |  | số có hai số thập phân |
| 58 | On-balance sheet interest | on_balance_sheet_interest | `Number` |  | số có hai số thập phân, `INTSPAMT` |
| 59 | Off-balance sheet interest | off_balance_sheet_interest | `Number` |  | số có hai số thập phân, `INTPBL - INTSPAMT` |
| 60 | Interest paid | interest_paid | `Number` |  | số có hai số thập phân |
| 61 | Write off amount of interest paid | write_off_provision_interest | `Number` |  | số có hai số thập phân |
| 62 | Provision of interest | provision_interest | `Number` |  | số có hai số thập phân |
| 63 | Fine amount | fine_amount | `Number` |  | kiểm tra code, `fnamt` |
| 64 | Fee |  | `Number` |  | kiểm tra code, không dùng, xem xét xóa đi |
| 65 | Month average balance | month_average_amount | `Number` |  | số có hai số thập phân |
| 66 | Quarter average balance | quarter_average_amount | `Number` |  | số có hai số thập phân |
| 67 | Semi-annual average balance | semi_annual_average_amount | `Number` |  | số có hai số thập phân |
| 68 | Year average balance | year_average_amount | `Number` |  | số có hai số thập phân |
| 69 | Week debit | week_debit | `Number` |  | số có hai số thập phân |
| 70 | Week credit | week_credit | `Number` |  | số có hai số thập phân |
| 71 | Month debit | month_debit | `Number` |  | số có hai số thập phân |
| 72 | Month credit | month_credit | `Number` |  | số có hai số thập phân |
| 73 | Quarter debit | quarter_debit | `Number` |  | số có hai số thập phân |
| 74 | Quarter credit | quarter_credit | `Number` |  | số có hai số thập phân |
| 75 | Semi-annual debit | semi_annual_debit | `Number` |  | số có hai số thập phân |
| 76 | Semi-annual credit | semi_annual_credit | `Number` |  | số có hai số thập phân |
| 77 | Year debit | year_debit | `Number` |  | số có hai số thập phân |
| 78 | Year credit | year_credit | `Number` |  | số có hai số thập phân |
| 79 | First date of principal repayment | principal_first_date | `Date time` |  |  |
| 80 | First date of interest payment | interest_first_date | `Date time` |  |  |
| 81 | Is syndicated? | is_syndicated | String |  |  |
| 82 | Interest computation mode | interest_computation_mode | String |  |  |
| 83 | Secure type | secure_type | String |  |  |
| 84 | Minimum secure rate | secure_rate | `Number` |  |  |
| 85 | Credit purpose | credit_purpose | String |  |  |
| 86 | Credit classification | credit_classification | String |  |  |
| 87 | Disbursement mode | disbursement_mode | String |  |  |
| 88 | Is restructured | is_restructured | String |  |  |
| 89 | Is provision? | is_provision | String |  |  |
| 90 | Provision tenor | provision_tenor | `Number` |  |  |
| 91 | Provision tenor unit | provision_tenor_unit | String |  |  |
| 92 | Rollover option | rollover_option | String |  | không dùng, xem xét xóa đi |
| 93 | Classification option | restruct | String |  |  |
| 94 | Principal grace period | principal_grace_period | `Number` |  |  |
| 95 | Principal due on holiday | holiday_principal_due_on | `Number` |  |  |
| 96 | Interest grace period | interest_grace_period | `Number` |  |  |
| 97 | Interest due on holiday | holiday_interest_tenor | `Number` |  |  |
| 98 | Fine grace period | fine_grace_period | `Number` |  |  |
| 99 | Fine due on holiday | holiday_fine_tenor | `Number` |  |  |
| 100 | Discount rate | discount_rate | `Number` |  |  |
| 101 | Re discount rate | re_discount_rate | `Number` |  |  |
| 102 | Principal provision rate normal | principal_provision_rate_0 | `Number` |  | số có hai số thập phân |
| 103 | Principal provision rate special mention | principal_provision_rate_1 | `Number` |  | số có hai số thập phân |
| 104 | Principal provision rate sub standard | principal_provision_rate_2 | `Number` |  | số có hai số thập phân |
| 105 | Principal provision rate doubtful | principal_provision_rate_3 | `Number` |  | số có hai số thập phân |
| 106 | Principal provision rate lost | principal_provision_rate_4 | `Number` |  | số có hai số thập phân |
| 107 | Interest provision rate normal | interest_provision_rate_0 | `Number` |  | số có hai số thập phân |
| 108 | Interest provision rate special mention | interest_provision_rate_1 | `Number` |  | số có hai số thập phân |
| 109 | Interest provision rate sub standard | interest_provision_rate_2 | `Number` |  | số có hai số thập phân |
| 110 | Interest provision rate doubtful | interest_provision_rate_3 | `Number` |  | số có hai số thập phân |
| 111 | Interest provision rate lost | interest_provision_rate_4 | `Number` |  | số có hai số thập phân |
| 112 | Remark | remark | String |  |  |
| 113 | Reference id | reference_number | String |  |  |
| 114 | Provision of other | provision_of_other | `Number` |  | số có hai số thập phân, `fpamt` |
|  | Tariff id | tariff_id | `Number` |  |  |
|  | Commitment limit | commitment_limit | `Number` |  |  |
|  | Customer code | customer_code | String |  | `trả thêm` |
|  | Customer name | customer_name | String |  | `trả thêm` |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Loan officer staff code | staff_code | String |  | `trả thêm` |
|  | Loan officer staff name | staff_name | String |  | `trả thêm` |
|  | Business purpose name | business_purpose_name | String |  | `trả thêm` |
| 115 | List notification type | list_notification_type | Array Object |  | chứa danh sách value: SMS (S), Email(E), Push Notification(P) |
| 116 | Notification type | notification_type | String |  | chuỗi cách nhau bởi dấu trị tuyệt đối |
| 117 | Email | email | String | | |
| 118 | Phone number | phone_number | String | | |
| 119 | Title | title | String | | |
| 120 | Principal tenor unit caption | principal_tenor_unit_caption | String | | |
| 121 | Interest tenor unit caption | interest_tenor_unit_caption | String | | |
| 122 | Interest rate | interest_rate | `Number | | |
| 123 | Interest rate unit caption | interest_rate_unit_caption | String | | |


**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |