# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction date |  | `Yes` | `Date` |  | Working date |  |
| 2 | User id |  | `Yes` | String |  | User login |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Transaction date |  | `Date` |  |  |  |
| 3 | User id |  | String |  |  |  |
| 4 | Transaction status |  | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Cash that users is keeping. (> 0.00)

**Flow of events:**
- System will auto transfer cash on end of date.
- Transfer all currencies.
- Reset value was transaction within day:
    - Opening balance = Closing balance of the previous day
    - Currency no change
    - Internal In = 0.00
    - Customer deposit = 0.00
    - Total in = 0.00
    - Internal out = 0.00
    - Customer withdrawal = 0.00
    - Total out = 0.00
    - Closing balance = Opening balance

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |