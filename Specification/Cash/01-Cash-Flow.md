# 1. Get list information
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_CASHFLOW_GETINFO`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch list | branchs | Array Object |  |  |
|  | Branch id | branch_id | `Number` |  |  |
|  | Branch code | branch_code | String |  |  |
|  | Branch name | branch_name | String |  |  |
|  | Type | type | String |  |  |
| 1.1 | Departments list | departments | Array Object |  |  |
|  | Department id | department_id | `Number` |  |  |
|  | Department code | department_code | String |  |  |
|  | Department name | department_name | String |  |  |
|  | Type | type | String |  |  |
| 1.1.1 | Cashier accounts list | cashier_accounts | Array Object |  |  |
|  | User id |  user_id | `Number` |  |  |
|  | User code | user_code | String |  |  |
|  | User name | login_name | String |  |  |
|  | Position | position | String |  |  |
|  | Type | type | String |  |  |

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 2. Get cash flow branch
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_CASHFLOW_BRANCH`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | branch_id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch id | branch_id | `Number` |  |  |
| 2 | Branch code | branch_code | String |  |  |
| 3 | Branch name | branch_name | String |  |  |
| 4 | Branch cash data | branch_cash_data | Array Object |  |  |
|  | Currency | currency | String | 3 |  |
|  | Opening balance | opening_balance | `Number` |  | số có hai số thập phân |
|  | Internal in | internal_in | `Number` |  | số có hai số thập phân |
|  | Customer deposit | customer_deposit | `Number` |  | số có hai số thập phân |
|  | Total in | total_in | `Number` |  | số có hai số thập phân |
|  | Internal out | internal_out | `Number` |  | số có hai số thập phân |
|  | Customer withdrawal | customer_withdrawal | `Number` |  | số có hai số thập phân |
|  | Total out | total_out | `Number` |  | số có hai số thập phân |
|  | Closing balance | closing_balance | `Number` |  | số có hai số thập phân |

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 3. Get cash flow department
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_CASHFLOW_DEPARTMENT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Department id | department_id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Department id | department_id | `Number` |  |  |
| 2 | Department code | department_code | String |  |  |
| 3 | Department name | department_name | String |  |  |
| 4 | Department cash data | department_cash_data | Array Object |  |  |
|  | Currency | currency | String | 3 |  |
|  | Opening balance | opening_balance | `Number` |  | số có hai số thập phân |
|  | Internal in | internal_in | `Number` |  | số có hai số thập phân |
|  | Customer deposit | customer_deposit | `Number` |  | số có hai số thập phân |
|  | Total in | total_in | `Number` |  | số có hai số thập phân |
|  | Internal out | internal_out | `Number` |  | số có hai số thập phân |
|  | Customer withdrawal | customer_withdrawal | `Number` |  | số có hai số thập phân |
|  | Total out | total_out | `Number` |  | số có hai số thập phân |
|  | Closing balance | closing_balance | `Number` |  | số có hai số thập phân |

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Get cash flow user
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_CASHFLOW_USER`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 2 | User id | user_id_cash_flow | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id |  user_id | `Number` |  |  |
| 2 | User code | user_code | String |  |  |
| 3 | User name | user_name | String |  |  |
| 4 | User cash data | user_cash_data | Array Object |  |  |
|  | Currency | currency | String | 3 |  |
|  | Opening balance | opening_balance | `Number` |  | số có hai số thập phân |
|  | Internal in | internal_in | `Number` |  | số có hai số thập phân |
|  | Customer deposit | customer_deposit | `Number` |  | số có hai số thập phân |
|  | Total in | total_in | `Number` |  | số có hai số thập phân |
|  | Internal out | internal_out | `Number` |  | số có hai số thập phân |
|  | Customer withdrawal | customer_withdrawal | `Number` |  | số có hai số thập phân |
|  | Total out | total_out | `Number` |  | số có hai số thập phân |
|  | Closing balance | closing_balance | `Number` |  | số có hai số thập phân |

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# Business rule and constrains
- Organizational tree of branch:
    - Show list users have cash.
    - Show list users haven't cash, but within day has transaction related with cash.
- Allow user to see cash by himself.
- User can keep many kinds of currencies.
- Allow user to see cash of other users if he has right is "Chief cashier" and same department.
- Information about cash flow:
    - Currency: currency of cash.
    - Opening balance: Cash of begin date.
    - Internal in: another user moved cash within day and confirmed.
    - Customer deposit: debit cash within day.
    - Total in = Internal in + Customer deposit.
    - Internal out: moved cash to another users within day and confirmed.
    - Customer withdrawal: credit cash within day.
    - Total out = Internal out + Customer withdrawal.
    - Closing balance = Staring balance + Total in - Total out.

- ∑ Cash of tellers = Cash of department.
- ∑ Cash of departments = Cash of branch.