# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CSH_MOV`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | From teller code | from_teller_code | `Yes` | String | 5 | User code login |  | CNCAT |
| 2 | To teller code | to_teller_code | `Yes` | String | 5 |  |  | USRCD |
| 3 | To teller name | to_teller_name | `Yes` | String | 250 |  |  | TO_USER_NAME |
| 4 | Amount | amount | `Yes` | `Number` |  | > 0 | số có hai số thập phân | TXAMT |
| 5 | Currency | currency | `Yes` | String | 3 |  |  | CACCCR |
| 6 | Description | description | No | String | 250 |  |  | DESCS |

`Không cần gửi FR_USER_NAME, FR_USER, TO_USER từ request message.`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | From teller code | from_teller_code | String |  |  |
| 3 | To teller code | to_teller_code | String |  |  |
| 4 | To teller name | to_teller_name | String |  |  |
| 5 | Amount | amount | `Number` |  |  |
| 6 | Currency | currency | String |  |  |
| 7 | Description | description | String |  |  |
| 8 | Transaction date | transaction_date | `Date time` |  |  |
| 9 | User id | user_id | `Number` |  |  |
| 10 | Transaction status | status | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Cash movement > 0.00
- Just allows moving from [Cashier to chief-cash] or [Chief-cash to Cashier].
  - It means. Don't allow moving between 2 Cashier or between 2 Chief-cash.
- Just allows moving with these users have same branch.

**Flow of events:**
- Transaction need receiver confirm.
- Transaction complete:
    - Cash of receiver increase, include: "Internal in", "Closing balance".
    - Cash of sender decrease, include: "Closing balance" and take note into "Internal out".

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Approve
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CNCAT | GET_INFO_USER_ACCOUNT | [GET_INFO_USER_ACCOUNT](Specification/Common/17 Admin Rulefuncs) |
| 2 | GET_INFO_DESCS | `select '@FR_USER_NAME' ' move '  '@TXAMT' ' ' '@CACCCR' ' cash to ' '@TO_USER_NAME' from dual` trên JWEB => `from_user_name` move `amount` `currency` cash to `to_teller_name` | Làm trên JWEB, `from_user_name` lấy từ rule GET_INFO_USER_ACCOUNT |
| 3 | GET_INFO_USRCD | GET_INFO_USER_ACCOUNT_CASHIER | [GET_INFO_USER_ACCOUNT_CASHIER](Specification/Common/17 Admin Rulefuncs) |
| 4 | LKP_DATA_USRCD | ADM_LOOKUP_USER_ACCOUNT_CASHIER | [ADM_LOOKUP_USER_ACCOUNT_CASHIER](Specification/Common/17 Admin Rulefuncs) |