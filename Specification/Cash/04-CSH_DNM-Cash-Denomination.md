# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CSH_DNM`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Currency | currency_code | `Yes` | String | 3 |  |  | CCCRCD |
| 2 | Closing cash balance | closing_cash_balance | `Yes` | `Number` |  | 0 | số có hai số thập phân | EDAY |
| 3 | Denom cash balance | denom_cash_balance | `Yes` | `Number` |  | 0 | số có hai số thập phân | DCASH |
| 4 | Remaining cash balance | remaining_cash_balance | `Yes` | `Number` |  | 0 | số có hai số thập phân | RCASH |
| 5 | Cash denom list | cash_denom_list | `Yes` | JSON Object |  |  |  | CASH_DENOM |
|  | Face value | face_value | `Yes` | `Number` |  | 0 | số nguyên dương | FACEVAL |
|  | Face type | face_type | `Yes` | String | 1 |  |  | FACETYPE |
|  | Face type caption | face_type_caption | `Yes` | String |  |  |  | FACETYPECT |
|  | Last sheet | last_sheet | `Yes` | `Number` |  | 0 | số nguyên dương | PRESHEET |
|  | Last amount | last_amount | `Yes` | `Number` |  | 0 |  | PREAMT |
|  | Amount in safe | amount_in_safe | `Yes` | `Number` |  | 0 | số nguyên dương | AMTSF |
|  | Sheet | sheet | `Yes` | `Number` |  | 0 | số nguyên dương | SHEET |
|  | Amount | amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | AMT |
| 6 | Last total amount | last_total_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | last_total_amount |
| 7 | Total amount | total_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | AMT |
| 8 | User approve | user_approve | `Yes` | String | 5 |  |  | APPROVERID |
| 9 | Description | description | No | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency | currency_code | `Yes` | String |  |  |
| 2 | Closing cash balance | closing_cash_balance | `Number` |  |  |
| 3 | Denom cash balance | denom_cash_balance | `Number` |  |  |
| 4 | Remaining cash balance | remaining_cash_balance | `Number` |  |  |
| 5 | Cash denom list | cash_denom_list | JSON Object |  |  |
|  | Face value | face_value | `Number` |  |  |
|  | Face type | face_type | String |  |  |
|  | Face type caption | face_type_caption | String |  |  |
|  | Last sheet | last_sheet | `Number` |  |  |
|  | Last amount | last_amount | `Number` |  |  |
|  | Amount in safe | amount_in_safe | `Number` |  |  |
|  | Sheet | sheet | `Number` |  |  |
|  | Amount | amount | `Number` |  |  |
| 6 | Last total amount | last_total_amount | `Number` |  |  |
| 7 | Total amount | total_amount | `Number` |  |  |
| 8 | User approve | user_approve | String |  |  |
| 9 | Description | description | No | String |  |  |
| 10 | Transaction references | reference_id | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Cash that users is keeping. (> 0.00)
- Do this before end of day.

**Flow of events:**
- User choose currency, real cash at counter.
    - System will load denomination list and cash closing balance.
- User input sheet for each denom.
    - Amount will be calculated by Face value * Sheet.
- After that, user need to check and enter user name and password for approval.
- Print voucher.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Voucher:**
- `C101`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Approve
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | CashFlow/GetBalance | CSH_RULEFUNC_GET_CURRENT_BALANCE | [CSH_RULEFUNC_GET_CURRENT_BALANCE](Specification/Common/18 Cash Rulefuncs) |
| 2 | CashFlow/ListDenom | CSH_RULEFUNC_GET_LIST_DENOM_BALANCE | [CSH_RULEFUNC_GET_LIST_DENOM_BALANCE](Specification/Common/18 Cash Rulefuncs) |