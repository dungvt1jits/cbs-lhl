# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Denomination​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_DENOM_SER_SIMPLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    "search_text": "",
    "page_size": 5,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Currency | currency_id | String | 3 |  |
| 2 | Face value | face_value | `Number` |  | số nguyên dương |
| 3 | Face type | face_type | String |  |  |
| 4 | Face description | face_description | String |  |  |
| 5 | Order | order | `Number` |  |  |
| 6 | Denom id | id | `Number` |  |  |

**Example:**
```json
{
    "total_count": 2,
    "total_pages": 1,
    "has_previous_page": false,
    "has_next_page": false,
    "items": [
        {
            "id": 3,
            "currency_id": "USD",
            "face_value": 0.0,
            "face_type": "N",
            "face_description": "",
            "order": 0
        },
        {
            "id": 1,
            "currency_id": "USD",
            "face_value": 100.0,
            "face_type": "N",
            "face_description": null,
            "order": 1
        }
    ],
    "page_index": 0,
    "page_size": 5
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Denomination​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_DENOM_SER_ADVANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency | currency_id | No | String | 3 |  |  |
| 2 | Face value | face_value | No | `Number` |  |  | số nguyên dương |
| 3 | Face type | face_type | No | String |  |  |  |
| 4 | Face description | face_description | No | String |  |  |  |
| 5 | Order | order | No | `Number` |  |  |  |
| 6 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 7 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    "currency_id": "",
    "face_value": null,
    "face_type": "",
    "face_description": "",
    "order": null,
    "page_index": 0,
    "page_size": 5
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Currency | currency_id | String | 3 |  |
| 2 | Face value | face_value | `Number` |  | số nguyên dương |
| 3 | Face type | face_type | String |  |  |
| 4 | Face description | face_description | String |  |  |
| 5 | Order | order | `Number` |  |  |
| 6 | Denom id | id | `Number` |  |  |

**Example:**
```json
{
    "total_count": 2,
    "total_pages": 1,
    "has_previous_page": false,
    "has_next_page": false,
    "items": [
        {
            "id": 3,
            "currency_id": "USD",
            "face_value": 0.0,
            "face_type": "N",
            "face_description": "",
            "order": 0
        },
        {
            "id": 1,
            "currency_id": "USD",
            "face_value": 100.0,
            "face_type": "N",
            "face_description": null,
            "order": 1
        }
    ],
    "page_index": 0,
    "page_size": 5
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Denomination​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_DENOM_INS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency | currency_id | `Yes` | String | 3 |  | is unique |
| 2 | Face value | face_value | `Yes` | `Number` |  |  | is unique |
| 3 | Face type | face_type | `Yes` | String |  | N |  |
| 4 | Face description | face_description | No | String |  |  |  |
| 5 | Order | order | `Yes` | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    "currency_id": "USD",
    "face_value": 10,
    "face_type": "N",
    "face_description": "",
    "order": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency | currency_id | String | 3 |  |
| 2 | Face value | face_value | `Number` |  | số nguyên dương |
| 3 | Face type | face_type | String |  |  |
| 4 | Face description | face_description | String |  |  |
| 5 | Order | order | `Number` |  |  |
| 6 | Denom id | id | `Number` |  |  |

**Example:**
```json
{
    "currency_id": "USD",
    "face_value": 10,
    "face_type": "N",
    "face_description": "",
    "order": 0,
    "id": 4
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Denomination​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_DENOM_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Denom id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 4
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency | currency_id | String | 3 |  |
| 2 | Face value | face_value | `Number` |  | số nguyên dương |
| 3 | Face type | face_type | String |  |  |
| 4 | Face description | face_description | String |  |  |
| 5 | Order | order | `Number` |  |  |
| 6 | Denom id | id | `Number` |  |  |

**Example:**
```json
{
    "currency_id": "USD",
    "face_value": 10,
    "face_type": "N",
    "face_description": "",
    "order": 0,
    "id": 4
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Denomination​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_DENOM_UPD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency | currency_id | `Yes` | String | 3 |  | is unique |
| 2 | Face value | face_value | `Yes` | `Number` |  |  | is unique |
| 3 | Face type | face_type | `Yes` | String |  | N |  |
| 4 | Face description | face_description | No | String |  |  |  |
| 5 | Order | order | `Yes` | `Number` |  |  | số nguyên dương |
| 6 | Denom id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 4,
    "currency_id": "USD",
    "face_value": 10,
    "face_type": "N",
    "face_description": "face_description",
    "order": 3
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency | currency_id | String | 3 |  |
| 2 | Face value | face_value | `Number` |  | số nguyên dương |
| 3 | Face type | face_type | String |  |  |
| 4 | Face description | face_description | String |  |  |
| 5 | Order | order | `Number` |  |  |
| 6 | Denom id | id | `Number` |  |  |

**Example:**
```json
{
    "currency_id": "USD",
    "face_value": 10,
    "face_type": "N",
    "face_description": "face_description",
    "order": 3,
    "id": 4
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Denomination​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_DENOM_DEL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Denom id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Denom id | id | `Number` |  |  |

**Example:**
```json
{
    "currency_id": "USD",
    "face_value": 10,
    "face_type": "N",
    "face_description": "face_description",
    "order": 3,
    "id": 4
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |