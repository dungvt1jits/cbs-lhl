# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Product code |  | `Yes` | String |  |  |  |
| 2 | Message code |  | `Yes` | String |  |  |  |
| 3 | Ordering institution |  | `Yes` | String |  |  |  |
| 4 | Nostro bank |  | `Yes` | String |  |  |  |
| 5 | Reference number |  | No | String |  |  |  |
| 6 | Sender id |  | `Yes` | String |  |  |  |
| 7 | Sender address |  | No | String |  |  |  |
| 8 | Sender name |  | `Yes` | String |  |  |  |
| 9 | Sender phone |  | No | String |  |  |  |
| 10 | Sender type |  | `Yes` | String |  |  |  |
| 11 | Receiver name |  | `Yes` | String |  |  |  |
| 12 | Receiver id |  | No | String |  |  |  |
| 13 | Receiver address |  | No | String |  |  |  |
| 14 | Receiver phone |  | No | String |  |  |  |
| 15 | Receiver type |  | `Yes` | String |  |  |  |
| 16 | Country |  | `Yes` | String |  |  |  |
| 17 | Purpose |  | `Yes` | String |  |  |  |
| 18 | Sender to receiver information |  | No | String |  |  |  |
| 19 | Other information |  | No | JSON Object |  |  |  |
|  | Goods type |  | No | String |  |  |  |
|  | Goods list |  | No | String |  |  |  |
| 20 | Remitting currency |  | `Yes` | String | 3 |  |  |
| 21 | Remitting amount |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 22 | Remitting exchange rate/BCY |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 23 | Remitting amount in BCY |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 24 | Commission in remitting currency |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 25 | Receive by |  | `Yes` | String |  |  |  |
| 26 | Account number |  | No | String |  |  |  |
| 27 | Receive currency |  | `Yes` | String | 3 |  |  |
| 28 | Original cross rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 29 | Cross rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 30 | Reverse rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 31 | Display rate |  | `Yes` | String |  |  |  |
| 32 | Amount before fees |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 33 | Commission |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 34 | Amount customer receives |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 35 | Receive exchange rate/BCY |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 36 | Description |  | `Yes` | String |  |  |  |
| 37 | Transaction date |  | `Yes` | `Date` |  | Working date |  |
| 38 | User id |  | `Yes` | String |  | User login |  |
| 39 | Total fee |  | No | `Number` |  |  | số có hai số thập phân |
| 40 | Fee data |  | No | JSON Object |  |  |  |
|  | IFC code |  | No | `Number` |  |  |  |
|  | Value type |  | No | String |  |  |  |
|  | Value |  | No | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | No | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | No | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | No | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | No | String | 3 |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Product code |  | String |  |  |  |
| 3 | Message code |  | String |  |  |  |
| 4 | Ordering institution |  | String |  |  |  |
| 5 | Nostro bank |  | String |  |  |  |
| 6 | Reference number |  | String |  |  |  |
| 7 | Sender id |  | String |  |  |  |
| 8 | Sender address |  | String |  |  |  |
| 9 | Sender name |  | String |  |  |  |
| 10 | Sender phone |  | String |  |  |  |
| 11 | Sender type |  | String |  |  |  |
| 12 | Receiver name |  | String |  |  |  |
| 13 | Receiver id |  | String |  |  |  |
| 14 | Receiver address |  | String |  |  |  |
| 15 | Receiver phone |  | String |  |  |  |
| 16 | Receiver type |  | String |  |  |  |
| 17 | Country |  | String |  |  |  |
| 18 | Purpose |  | String |  |  |  |
| 19 | Sender to receiver information |  | String |  |  |  |
| 20 | Other information |  | JSON Object |  |  |  |
|  | Goods type |  | String |  |  |  |
|  | Goods list |  | String |  |  |  |
| 21 | Remitting currency |  | String | 3 |  |  |
| 22 | Remitting amount |  | `Number` |  |  | số có hai số thập phân |
| 23 | Remitting exchange rate/BCY |  | `Number` |  |  | số có 9 số thập phân |
| 24 | Remitting amount in BCY |  | `Number` |  |  | số có hai số thập phân |
| 25 | Commission in remitting currency |  | `Number` |  |  | số có hai số thập phân |
| 26 | Receive by |  | String |  |  |  |
| 27 | Account number |  | String |  |  |  |
| 28 | Receive currency |  | String | 3 |  |  |
| 29 | Original cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 30 | Cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 31 | Reverse rate |  | `Number` |  |  | số có 9 số thập phân |
| 32 | Display rate |  | String |  |  |  |
| 33 | Amount before fees |  | `Number` |  |  | số có hai số thập phân |
| 34 | Commission |  | `Number` |  |  | số có hai số thập phân |
| 35 | Amount customer receives |  | `Number` |  |  | số có hai số thập phân |
| 36 | Receive exchange rate/BCY |  | `Number` |  |  | số có 9 số thập phân |
| 37 | Description |  | String |  |  |  |
| 38 | Transaction date |  | `Date` |  |  |  |
| 39 | User id |  | String |  |  |  |
| 40 | Transaction status |  | String |  |  |  |
| 41 | Total fee |  | `Number` |  |  | số có hai số thập phân |
| 42 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 43 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Catalogue" is Inward Domestic Transfer.
- Message exists in “Payment queue at center” with property "Payment center approve status": Approved
- Receive amount > 0.00

**Flow of events:**
- Support method to receive includes: Cash/Deposit/Accounting
    - If method is Cash:
        - User must have enough cash to payment.
    - If method is Deposit:
        - Deposit account exists with status <> "Close". Type is current or saving
        - Auto fill information of receiver. It gets from customer profile.
    - If method is GL:
        - GL exists in account chart and same branch with user login.
        - Allow credit side.
- Allow remitting currency and receive currency are different:
    - Debit rate get from table "foreign exchange" = Transferring buy (TB)
    - Credit rate get from table "foreign exchange" = Transferring sell (TA)
    - Cross rate = Debit rate / Credit rate
        - Number rounding is 9 decimals
- If Cross rate < 1, system is more flexible about show exchange rate on voucher. It can be:
    - Reverse rate = 1 / Cross rate.
        - Number rounding is 9 decimals
- If user do any action that impact to change Cross rate. System will suggest approve.
- Allow collect fee on transaction (if any).
- Show total value of receive include fee amount. 
- Support to print voucher for internal and receive transaction.
- Transaction complete:
    - Message status: Paid
    - Paid amount increase.
    - If Receive is method "Cash"
        - Cash of user will decrease
    - If Receive is method "Deposit"
        - Balance of deposit account will increase
    - If Receive is method "Accounting"
        - Credit GL

**Database:**


**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | GL (Nostro account) | Receive amount | Receive currency |
| 1 | C | CASH/DEPOSIT/GL | Receive amount | Receive currency |

- Note: "Nostro account" gets from domestic message.

- Case collect fee (fee amount > 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 2 | D | CASH/DEPOSIT/GL | Fee amount | Receive currency |
| 2 | C | IFCC (income) | Fee amount | Receive currency |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Product code |  | String |  |  |  |
| 3 | Message code |  | String |  |  |  |
| 4 | Ordering institution |  | String |  |  |  |
| 5 | Nostro bank |  | String |  |  |  |
| 6 | Reference number |  | String |  |  |  |
| 7 | Sender id |  | String |  |  |  |
| 8 | Sender address |  | String |  |  |  |
| 9 | Sender name |  | String |  |  |  |
| 10 | Sender phone |  | String |  |  |  |
| 11 | Sender type |  | String |  |  |  |
| 12 | Receiver name |  | String |  |  |  |
| 13 | Receiver id |  | String |  |  |  |
| 14 | Receiver address |  | String |  |  |  |
| 15 | Receiver phone |  | String |  |  |  |
| 16 | Receiver type |  | String |  |  |  |
| 17 | Country |  | String |  |  |  |
| 18 | Purpose |  | String |  |  |  |
| 19 | Sender to receiver information |  | String |  |  |  |
| 20 | Other information |  | JSON Object |  |  |  |
|  | Goods type |  | String |  |  |  |
|  | Goods list |  | String |  |  |  |
| 21 | Remitting currency |  | String | 3 |  |  |
| 22 | Remitting amount |  | `Number` |  |  | số có hai số thập phân |
| 23 | Remitting exchange rate/BCY |  | `Number` |  |  | số có 9 số thập phân |
| 24 | Remitting amount in BCY |  | `Number` |  |  | số có hai số thập phân |
| 25 | Commission in remitting currency |  | `Number` |  |  | số có hai số thập phân |
| 26 | Receive by |  | String |  |  |  |
| 27 | Account number |  | String |  |  |  |
| 28 | Receive currency |  | String | 3 |  |  |
| 29 | Original cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 30 | Cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 31 | Reverse rate |  | `Number` |  |  | số có 9 số thập phân |
| 32 | Display rate |  | String |  |  |  |
| 33 | Amount before fees |  | `Number` |  |  | số có hai số thập phân |
| 34 | Commission |  | `Number` |  |  | số có hai số thập phân |
| 35 | Amount customer receives |  | `Number` |  |  | số có hai số thập phân |
| 36 | Receive exchange rate/BCY |  | `Number` |  |  | số có 9 số thập phân |
| 37 | Description |  | String |  |  |  |
| 38 | Transaction date |  | `Date` |  |  |  |
| 39 | User id |  | String |  |  |  |
| 40 | Transaction status |  | String |  |  |  |
| 41 | Total fee |  | `Number` |  |  | số có hai số thập phân |
| 42 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 43 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
- [Rulefunc](Specification/Common/01 Rulefunc)<br>