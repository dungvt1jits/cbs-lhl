# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_SEARCH_ACCOUNTLINKAGE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Master module name | master_module_name | String |  |  |
| 2 | Master account number | account_number | String |  | `trả thêm`, account number tương ướng với `master_account_number` |
| 3 | Master account name | master_account_name | String |  | d_credit, d_deposit |
| 4 | Master account number | master_account_number | String |  | trường ẩn để thực hiện chức năng view, không show trên UI |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_ADSEARCH_ACCOUNTLINKAGE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Master module name | master_module_code | No | String |  |  |  |
| 2 | Master account number | master_account_number | No | String |  |  | `Search theo account_number tương ứng master_account_number` |
| 3 | Master account name | master_account_name | No | String |  |  | d_credit, d_deposit |
| 4 | Page index | page_index | No | `Number` |  |  |  |
| 5 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Master module name | master_module_name | String |  |  |
| 2 | Master account number | account_number | String |  | `trả thêm`, account number tương ướng với `master_account_number` |
| 3 | Master account name | master_account_name | String |  | d_credit, d_deposit |
| 4 | Master account number | master_account_number | String |  | trường ẩn để thực hiện chức năng view, không show trên UI |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_INSERT_ACCOUNTLINKAGE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Master account number | master_account_number | `Yes` | String |  |  |  |
| 2 | Master module name | master_module_code | `Yes` | String |  |  |  |
| 3 | List account linkage | list_account_linkage | No | Array Object |  |  |  |
|  | Linkage module name | linkage_module_code | `Yes` | String |  |  |  |
|  | Linkage account number | linkage_account_number | `Yes` | String |  |  |  |
|  | Linkage type | linkage_type | `Yes` | String |  |  |  |
|  | Linkage classification | linkage_class | `Yes` | String |  |  |  |
|  | Linkage description | linkage_description | No | String |  |  |  |
| 4 | Fee data | fee_data | No | Array Object |  |  |  | `gửi thêm` |
|  | IFC code | ifc_code | No | `Number` |  |  |  |  |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân |  |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân |  |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân |  |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân |  |
|  | Currency | currency_fee_code | No | String | 3 |  |  |  |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn |  |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn |  |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn |  |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn |  |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn |  |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn |  |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn |  |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn |  |
| 5 | Fee collect menthod | method | `Yes` | String | 3 | `CSH` or `DPT` or `ACT` | `gửi thêm` |  |
| 6 | Account number for fee | account_number_for_fee | `Yes` | String | 25 |  | `gửi thêm` |  |
| 7 | Currency | currency_code | No | String | 3 | |  | `gửi thêm` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Master module name | master_module_code | String |  |  |
| 2 | Master account number | master_account_number | String |  |  |
| 3 | Master account name |  | String | master_account_name | d_credit, d_deposit |
| 4 | List account linkage | list_account_linkage | Array Object |  |  |
|  | Linkage module name | linkage_module_code | String |  |  |
|  | Linkage account number | linkage_account_number | String |  |  |
|  | Linkage account name | linkage_account_name | String |  | d_credit, d_deposit |
|  | Linkage type | linkage_type | String |  |  |
|  | Linkage classification | linkage_class | String |  |  |
|  | Linkage description | linkage_description | String |  |  |
| 5 | Fee data | fee_data | JSON Object |  |  | `trả thêm` |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 6 | Currency code | currency_code | String |  | `trả thêm` |
| 7 | Fee collect menthod | method | String |  | `trả thêm` |
| 8 | Account number for fee | account_number_for_fee | String |  | `trả thêm` |

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_VIEW_ACCOUNTLINKAGE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Master account number | master_account_number | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Master module name | master_module_code | String |  |  |
| 2 | Master account number | master_account_number | String |  |  |
| 3 | Master account name |  | String | master_account_name | d_credit, d_deposit |
| 4 | Account number | account_number | String |  | `trả thêm`, account number tương ướng với `master_account_number` |
| 5 | List account linkage | list_account_linkage | Array Object |  |  |
|  | Linkage module name | linkage_module_code | String |  |  |
|  | Linkage account number | linkage_account_number | String |  |  |
|  | Linkage account name | linkage_account_name | String |  | d_credit, d_deposit |
|  | Linkage type | linkage_type | String |  |  |
|  | Linkage classification | linkage_class | String |  |  |
|  | Linkage description | linkage_description | String |  |  |
|  | Account number | account_number | String |  | `trả thêm`, account number tương ướng với `master_account_number` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_UPDATE_ACCOUNTLINKAGE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Master account number | master_account_number | `Yes` | String |  |  |  |
| 2 | Master module name | master_module_code | `Yes` | String |  |  |  |
| 3 | List account linkage | list_account_linkage | No | Array Object |  |  |  |
|  | Linkage module name | linkage_module_code | `Yes` | String |  |  |  |
|  | Linkage account number | linkage_account_number | `Yes` | String |  |  |  |
|  | Linkage type | linkage_type | `Yes` | String |  |  |  |
|  | Linkage classification | linkage_class | `Yes` | String |  |  |  |
|  | Linkage description | linkage_description | No | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Master module name | master_module_code | String |  |  |
| 2 | Master account number | master_account_number | String |  |  |
| 3 | Master account name |  | String | master_account_name | d_credit, d_deposit |
| 4 | List account linkage | list_account_linkage | Array Object |  |  |
|  | Linkage module name | linkage_module_code | String |  |  |
|  | Linkage account number | linkage_account_number | String |  |  |
|  | Linkage account name | linkage_account_name | String |  | d_credit, d_deposit |
|  | Linkage type | linkage_type | String |  |  |
|  | Linkage classification | linkage_class | String |  |  |
|  | Linkage description | linkage_description | String |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_DELETE_ACCOUNTLINKAGE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Master account number | master_account_number | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Master account number | master_account_number | String |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |