# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/FundTransferViaFast​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_SEARCH_SP_TXSFAST`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `​/api​/FundTransferViaFast​/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Transaction number | transaction_ref_id | String |  | d_txsfast |
| 2 | Message code | message_code | String |  |  |
| 3 | Transaction date | transaction_date_pmt | `Date time` |  |  |
| 4 | Payment information id | payment_infor_id | String |  |  |
| 5 | Message direction | message_direction | String |  |  |
| 6 | End to end status | end_to_end_status | String |  |  |
| 7 | Sender bank | send_bank | String |  |  |
| 8 | Receiver bank | receiver_bank | String |  |  |
| 9 | Send bank name | send_bank_name | String |  |  |
| 10 | Receive bank name | receiver_bank_name | String |  |  |
| 11 | Sender name | sender_name | String |  |  |
| 12 | Receiver name | receiver_name | String |  |  |
| 13 | Receiver account number | receiver_account_no | String |  |  |
| 14 | Remitting amount | remitting_amount | `Number` |  | số có hai số thập phân |
| 15 | Remitting currency | remitting_currency | String | 3 |  |
| 16 | Message status | message_status | String |  |  |
| 17 | Reason refund | reason_refund | String |  |  |
| 18 | Purpose | purpose | String |  |  |
| 19 | Fast status | fast_status | String |  |  |
| 20 | Info payment | info_payment | String |  |  |
| 21 | Ref message code | ref_message_code | String |  |  |
| 22 | Coregateway status | coregateway_status | String |  |  |
| 23 | Message via fast id | id | `Number` |  |  |


**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FundTransferViaFast​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_SEARCH_ADV_TXSFAST`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction number | transaction_ref_id | No | String |  |  |  |
| 2 | Message code | message_code | No | String |  |  |  |
| 3 | Transaction date | transaction_date_pmt | No | `Date time` |  |  |  |
| 4 | Payment information id | payment_infor_id | No | String |  |  |  |
| 5 | Message direction | message_direction | No | String |  |  |  |
| 6 | End to end status | end_to_end_status | No | String |  |  |  |
| 7 | Sender bank | send_bank | No | String |  |  |  |
| 8 | Receiver bank | receiver_bank | No | String |  |  |  |
| 9 | Send bank name | send_bank_name | No | String |  |  |  |
| 10 | Receive bank name | receiver_bank_name | No | String |  |  |  |
| 11 | Sender name | sender_name | No | String |  |  |  |
| 12 | Receiver name | receiver_name | No | String |  |  |  |
| 13 | Receiver account number | receiver_account_no | No | String |  |  |  |
| 14 | Remitting amount | remitting_amount | No | `Number` |  |  | số có hai số thập phân |
| 15 | Remitting currency | remitting_currency | No | String | 3 |  |  |
| 16 | Message status | message_status | No | String |  |  |  |
| 17 | Reason refund | reason_refund | No | String |  |  |  |
| 18 | Purpose | purpose | No | String |  |  |  |
| 19 | Fast status | fast_status | No | String |  |  |  |
| 20 | Info payment | info_payment | No | String |  |  |  |
| 21 | Ref message code | ref_message_code | No | String |  |  |  |
| 22 | Coregateway status | coregateway_status | No | String |  |  |  |
| 23 | Page index | page_index | No | `Number` |  |  |  |
| 24 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Transaction number | transaction_ref_id | String |  | d_txsfast |
| 2 | Message code | message_code | String |  |  |
| 3 | Transaction date | transaction_date_pmt | `Date time` |  |  |
| 4 | Payment information id | payment_infor_id | String |  |  |
| 5 | Message direction | message_direction | String |  |  |
| 6 | End to end status | end_to_end_status | String |  |  |
| 7 | Sender bank | send_bank | String |  |  |
| 8 | Receiver bank | receiver_bank | String |  |  |
| 9 | Send bank name | send_bank_name | String |  |  |
| 10 | Receive bank name | receiver_bank_name | String |  |  |
| 11 | Sender name | sender_name | String |  |  |
| 12 | Receiver name | receiver_name | String |  |  |
| 13 | Receiver account number | receiver_account_no | String |  |  |
| 14 | Remitting amount | remitting_amount | `Number` |  | số có hai số thập phân |
| 15 | Remitting currency | remitting_currency | String | 3 |  |
| 16 | Message status | message_status | String |  |  |
| 17 | Reason refund | reason_refund | String |  |  |
| 18 | Purpose | purpose | String |  |  |
| 19 | Fast status | fast_status | String |  |  |
| 20 | Info payment | info_payment | String |  |  |
| 21 | Ref message code | ref_message_code | String |  |  |
| 22 | Coregateway status | coregateway_status | String |  |  |
| 23 | Message via fast id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
- Tham khảo thông tin chi tiết tại đây:<br>
- [PMT_ODTF: Outward Fund Transfer Via Fast](Specification/Payment/11 PMT_ODTF Outward Fund Transfer Via Fast)<br>

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/FundTransferViaFast​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_VIEW_TXSFAST`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Message via fast id | id | `Yes` | `Number` |  |  | d_txsfast |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Message code | message_code | String |  | d_txsfast |
| 2 | Create date | transaction_date_pmt | `Date time` |  |  |
| 3 | Transaction references number | transaction_ref_id | String |  |  |
| 4 | Message direction | message_direction | String |  |  |
| 5 | Send bank code | send_bank | String |  |  |
| 6 | Send bank name | send_bank_name | String |  |  |
| 7 | Receiver bank code | receiver_bank | String |  |  |
| 8 | Receiver bank name | receiver_bank_name | String |  |  |
| 9 | Sender name | sender_name | String |  |  |
| 10 | Sender account | sender_account_no | String |  |  |
| 11 | Receiver name | receiver_name | String |  |  |
| 12 | Receiver account | receiver_account_no | String |  |  |
| 13 | Sender currency | sender_currency | String | 3 |  |
| 14 | Sender amount | send_amount | `Number` |  | số có hai số thập phân |
| 15 | Remitting amount | remitting_amount | `Number` |  | số có hai số thập phân |
| 16 | Remitting currency | remitting_currency | String | 3 |  |
| 17 | Fee amount | fee_amount | `Number` |  | số có hai số thập phân |
| 18 | Fee pay by | f_payment_by | String |  |  |
| 19 | Total paid amount | total_paid_amount | `Number` |  | số có hai số thập phân, send_amount + fee_amount |
| 20 | Sender to receiver information | info_payment | String |  |  |
| 21 | Message status | message_status | String |  |  |
| 22 | Fast status | fast_status | String |  |  |
| 23 | Message code reference | ref_message_code | String |  |  |
| 24 | Message via fast id | id | `Number` |  |  |
|  | Message id | message_id | String |  |  |
|  | Payment information id | payment_infor_id | String |  |  |
|  | Instruction id | instruction_id | String |  |  |
|  | System status | system_status | String |  |  |
|  | Product code | product_code | String |  | cncat |
|  | Send bank street | send_bank_street | String |  |  |
|  | Send bank building number | send_bank_building_number | String |  | sbbdmb |
|  | Send bank postal code | send_bank_postal_code | String |  | sbpcode |
|  | Send bank town | send_bank_town | String |  |  |
|  | Receiver bank street | receiver_bank_street | String |  |  |
|  | Receiver bank building number | receiver_bank_building_number | String |  | rbbdmb |
|  | Receiver bank postal code | receiver_bank_postal_code | String |  | rbpcode |
|  | Receiver bank town | receiver_bank_town | String |  |  |
|  | Sender street | sender_street | String |  |  |
|  | Sender building number | sender_building_number | String |  | sdbdmb |
|  | Sender postal code | sender_postal_code | String |  | sdpcode |
|  | Sender town | sender_town | String |  |  |
|  | Sender country | sender_coutry | String |  |  |
|  | Receiver street | receiver_street | String |  |  |
|  | Receiver building number | receiver_building_number | String |  | rcbdmb |
|  | Receiver postal code | receiver_postal_code | String |  | rcpcode |
|  | Receiver town | receiver_town | String |  |  |
|  | Receiver country | receiver_country | String |  |  |
|  | Receiver currency | receiver_currency | String |  |  |
|  | Payment method | payment_method | String |  | pmtmtd |
|  | Receive amount | receive_amount | `Number` |  |  |
|  | Charge bearer | charge_bearer | String |  | chrgbr |
|  | Purpose | purpose | String |  |  |
|  | Nostro | nostro | String |  |  |
|  | Referred document code | referred_document_code | String |  | refund_code |
|  | Referred document number | referred_document_number | String |  | refnb |
|  | Reason refund | reason_refund | String |  |  |
|  | Branch id | branch_id | `Number` |  | brid |
|  | Exchange rate | exchange_rate | `Number` |  |  |
|  | End to end status | end_to_end_status | String |  |  |
|  | Coregateway status | coregateway_status | String |  |  |
|  | Detail charges | detail_charges | String |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Approve
## 5.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 |  |  |  |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |

# 6. Reject
## 6.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 |  |  |  |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |