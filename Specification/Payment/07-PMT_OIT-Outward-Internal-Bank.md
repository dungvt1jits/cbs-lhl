# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `PMT_OIT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Beneficiary branch | beneficiary_branch | `Yes` | String |  |  |  | crbank |
| 2 | Product code | product_code | `Yes` | String |  |  |  | cncat |
| 3 | Reference number | reference_number | No | String |  |  |  | msgcode |
| 4 | Sender name | sender_name | `Yes` | String |  |  |  | ccatnm |
| 5 | Sender id | sender_id | `Yes` | String |  |  |  | c_license |
| 6 | Sender address | sender_address | No | String |  |  |  | cctma |
| 7 | Sender phone | sender_phone | No | String |  |  |  | cstel |
| 8 | Sender type | sender_type | `Yes` | String |  |  |  | cstype |
| 9 | Receiver name | receiver_name | `Yes` | String |  |  |  | crname |
| 10 | Receiver id | receiver_id | No | String |  |  |  | crid |
| 11 | Receiver address | receiver_address | No | String |  |  |  | craddr |
| 12 | Receiver phone | receiver_phone | No | String |  |  |  | crtel |
| 13 | Receiver type | receiver_type | `Yes` | String |  |  |  | crtype |
| 14 | Country | country | `Yes` | String |  |  |  | ccountry |
| 15 | Purpose | purpose | `Yes` | String |  |  |  | cspur |
| 16 | Sender to receiver info | sender_to_receiver_information | `Yes` | String |  |  |  | ctxg1 |
| 17 | TEST-KEY | test_key | No | String |  |  |  | ctxg2 |
| 18 | Other information | other_informations | No | JSON Object |  |  |  | minfo |
| | Goods type | goods_type | No | String |  |  |  | cgtype |
| | Goods list | goods_list | No | String |  |  |  | mglst |
| 19 | Payment by | payment_by | `Yes` | String |  |  |  | payby |
| 20 | Send account number | send_account_number | No | String |  |  |  | sacno |
| 21 | Cheque no | cheque_no | No | String |  |  |  | cchqno |
| 22 | Send currency | send_currency | `Yes` | String | 3 |  |  | cccr |
| 23 | Send amount | send_amount | `Yes` | `Number` |  |  | số có hai số thập phân | psamt |
| 24 | Total amount payable | total_amount_payable | `Yes` | `Number` |  |  | số có hai số thập phân | pcscvt |
| 25 | Receive currency | receive_currency | `Yes` | String | 3 |  |  | rccr |
| 26 | Cross rate | cross_rate | `Yes` | `Number` |  |  | số có 9 số thập phân | ccrrate |
| 27 | Receive amount | receive_amount | `Yes` | `Number` |  |  | số có hai số thập phân | pramt |
| 28 | Description | description | `Yes` | String |  |  |  | descs |
| 29 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | ifccd |
|  | Value | ifc_value | No | `Number` |  | 0 | số có 5 số thập phân | ifcval |
|  | Fee | ifc_amount | No | `Number` |  | 0 | số có hai số thập phân | ifcamt |
|  | Floor | floor_value | No | `Number` |  | 0 | số có hai số thập phân | flrval |
|  | Ceiling | ceiling_value | No | `Number` |  | 0 | số có hai số thập phân | ceival |
|  | Currency | currency_fee_code | No | String | 3 |  |  | ccrcd |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | ccrid |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | payrate |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | paysrc |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | ramt |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | rrate |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | samt |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | sfappl |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | srate |
| 30 | Available balance | available_balance | No | `Number` |  |  | trường ẩn | camt3 |
| 31 | Linkage amount | linkage_amount | No | `Number` |  |  | trường ẩn | camt4 |
| 32 | Amount convert to BCY | amount_convert_to_bcy | No | `Number` |  |  | trường ẩn | cbamt |
| 33 | Branch id | branch_id | No | `Number` |  |  | trường ẩn | cbrcd |
| 34 | Commission | commission | `Yes` | `Number` |  |  | trường ẩn | cfamt |
| 35 | Inclusive | inclusive | No | String |  |  | trường ẩn | cinc |
| 36 | Total send amount | total_send_amount | No | `Number` |  |  | trường ẩn | csfee |
| 37 | Accounting credit exchange rate/BCY | receive_accounting_credit_exchange_rate | No | `Number` |  |  | trường ẩn | ctxexr |
| 38 | Sender information | sender_information | No | String |  |  | trường ẩn | ctxg3 |
| 39 | Receiver information | receiver_information | No | String |  |  | trường ẩn | ctxg4 |
| 40 | Other information | other_information | No | String |  |  | trường ẩn | ctxg5 |
| 41 | Fee | fee | `Yes` | `Number` |  |  | trường ẩn | cvat |
| 42 | Payment instruction | payment_instruction | No | String |  |  | trường ẩn | mpii |
| 43 | Rate RCCR | rate_rccr | No | `Number` |  |  | trường ẩn | orgrate |
| 44 | Linkage account | linkage_account | No | String |  |  | trường ẩn | pdacc |
| 45 | Account number | account_number | No | String |  |  | trường ẩn | pdoacc |
| 46 | Accounting debit exchange rate/BCY | accounting_debit_exchange_rate | No | `Number` |  |  | trường ẩn | pgexr |
| 47 | Fax number | fax_number | No | String |  |  | trường ẩn | pmref |
| 48 | Receive by | receive_by | No | String |  |  | trường ẩn | recby |
| 49 | Branch for Deposit account | branch_for_deposit_account | No | String |  |  | trường ẩn | sbid |
| 50 | Share fee amount | share_fee_amount | No | `Number` |  |  | trường ẩn | sfamt |
| 51 | Share fee rate | share_fee_rate | No | `Number` |  |  | trường ẩn | sfrate |
| 52 | Total Fee for Accounting | total_fee_for_accounting | No | `Number` |  |  | trường ẩn | tfact |
| 53 | Total Fee for Cash | total_fee_for_cash | No | `Number` |  |  | trường ẩn | tfcsh |
| 54 | Total Fee for Deposit | total_fee_for_deposit | No | `Number` |  |  | trường ẩn | tfdpt |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references | beneficiary_branch | String |  |  |  |
| 2 | Beneficiary branch | beneficiary_branch | String |  |  |  |
| 3 | Product code | product_code | String |  |  |  |
| 4 | Reference number | reference_number | String |  |  |  |
| 5 | Sender name | sender_name | String |  |  |  |
| 6 | Sender id | sender_id | String |  |  |  |
| 7 | Sender address | sender_address | String |  |  |  |
| 8 | Sender phone | sender_phone | String |  |  |  |
| 9 | Sender type | sender_type | String |  |  |  |
| 10 | Receiver name | receiver_name | String |  |  |  |
| 11 | Receiver id | receiver_id | String |  |  |  |
| 12 | Receiver address | receiver_address | String |  |  |  |
| 13 | Receiver phone | receiver_phone | String |  |  |  |
| 14 | Receiver type | receiver_type | String |  |  |  |
| 15 | Country | country | String |  |  |  |
| 16 | Purpose | purpose | String |  |  |  |
| 17 | Sender to receiver info | sender_to_receiver_information | String |  |  |  |
| 18 | TEST-KEY | test_key | String |  |  |  |
| 19 | Other information | other_informations | JSON Object |  |  |  |
|  | Goods type | goods_type | String |  |  |  |
|  | Goods list | goods_list | String |  |  |  |
| 20 | Payment by | payment_by | String |  |  |  |
| 21 | Send account number | send_account_number | String |  |  |  |
| 22 | Cheque no | cheque_no | String |  |  |  |
| 23 | Send currency | send_currency | String | 3 | | |  | |  |
| 24 | Send amount | send_amount | `Number` |  |  | số có hai số thập phân | |  |
| 25 | Total amount payable | total_amount_payable | `Number` |  |  | số có hai số thập phân  |
| 26 | Receive currency | receive_currency | String | 3 | | |  | |  |
| 27 | Cross rate | cross_rate | `Number` |  |  | số có 9 số thập phân |
| 28 | Receive amount | receive_amount | `Number` |  |  | số có hai số thập phân |
| 29 | Description | description | String |  |  |  |
| 30 | Available balance | available_balance | `Number` |  |  |  |
| 31 | Linkage amount | linkage_amount | `Number` |  |  |  |
| 32 | Amount convert to BCY | amount_convert_to_bcy | `Number` |  |  |  |
| 33 | Branch id | branch_id | `Number` |  |  |  |
| 34 | Commission | commission | `Number` |  |  |  |
| 35 | Inclusive | inclusive | String |  |  |  |
| 36 | Total send amount | total_send_amount | `Number` |  |  |  |
| 37 | Accounting credit exchange rate/BCY | receive_accounting_credit_exchange_rate | `Number` |  |  |  |
| 38 | Sender information | sender_information | String |  |  |  |
| 39 | Receiver information | receiver_information | String |  |  |  |
| 40 | Other information | other_information | String |  |  |  |
| 41 | Fee | fee | `Number` |  |  |  |
| 42 | Payment instruction | payment_instruction | String |  |  |  |
| 43 | Rate RCCR | rate_rccr | `Number` |  |  |  |
| 44 | Linkage account | linkage_account | String |  |  |  |
| 45 | Account number | account_number | String |  |  |  |
| 46 | Accounting debit exchange rate/BCY | accounting_debit_exchange_rate | `Number` |  |  |  |
| 47 | Fax number | fax_number | String |  |  |  |
| 48 | Receive by | receive_by | String |  |  |  |
| 49 | Branch for Deposit account | branch_for_deposit_account | String |  |  |  |
| 50 | Share fee amount | share_fee_amount | `Number` |  |  |  |
| 51 | Share fee rate | share_fee_rate | `Number` |  |  |  |
| 52 | Total Fee for Accounting | total_fee_for_accounting | `Number` |  |  |  |
| 53 | Total Fee for Cash | total_fee_for_cash | `Number` |  |  |  |
| 54 | Total Fee for Deposit | total_fee_for_deposit | `Number` |  |  |  |
| 55 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 56 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 57 | Transaction status | status | String |  |  |  |
| 58 | Transaction date | transaction_date | `Date time` |  |  |  |
| 59 | User id | user_id | `Number` |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Catalogue is "Outward inter-branch transfer"
- Send and receive must be same currency.
- Send amount > 0.00
- Beneficiary branch must be register in "List/Correspondent Bank". With type "Internal".

**Flow of events:**
- Support method to payment includes: Cash/Deposit/Accounting
    - If method is Deposit:
        - Deposit account exists with status "Normal". Type is current or saving
        - Balance has enough to payment
        - Allow payment by cheque.
        - Auto fill information of sender get from customer profile.
    - If method is GL:
        - GL exists in account chart and same branch with user login.
        - Allow debit side.
- Allow collect fee on transaction (if any).
- Show total value of payment include send amount and fee amount. 
- Support to print voucher for internal and sender transaction.
- Transaction complete:
    - Create message payment into queue and wait approval.
        - Message direction: Outward
        - Message type: Internal
        - Message status: New
        - Branch Approve status: New
    - If Payment is method "Cash"
        - Cash of user will increase
    - If Payment is method "Deposit"
        - Balance of deposit account will decrease
    - If Payment is method "Accounting"
        - Debit GL
- When approve message, property will be changed:
    - Message status: Send
    - Branch approve status: Approved

![image](uploads/eb26dd1b5fe0e397d1ee057ae340aedf/image.png)

**Database:**


**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASH/DEPOSIT/GL | Remittance amount | Send currency |
| 1 | C | RMINTERNALSUP (Branch receive) | Remittance amount | Send currency |

- Note: **RMINTERNALSUP** is defined at "Accounting setup/Common account definition" with branch code is branch does this transaction, branch id is receive branch 

- Case collect fee (fee amount > 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 2 | D | CASH/DEPOSIT/GL | Fee amount | Send currency |
| 2 | C | IFCC (income) | Fee amount | Send currency |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Beneficiary branch |  | String |  |  |  |
| 3 | Product code |  | String |  |  |  |
| 4 | Reference number |  | String |  |  |  |
| 5 | Sender name |  | String |  |  |  |
| 6 | Sender id |  | String |  |  |  |
| 7 | Sender address |  | String |  |  |  |
| 8 | Sender phone |  | String |  |  |  |
| 9 | Sender type |  | String |  |  |  |
| 10 | Receiver name |  | String |  |  |  |
| 11 | Receiver id |  | String |  |  |  |
| 12 | Receiver address |  | String |  |  |  |
| 13 | Receiver phone |  | String |  |  |  |
| 14 | Receiver type |  | String |  |  |  |
| 15 | Country |  | String |  |  |  |
| 16 | Purpose |  | String |  |  |  |
| 17 | Sender to receiver info |  | String |  |  |  |
| 18 | TEST-KEY |  | String |  |  |  |
| 19 | Other information |  | JSON Object |  |  |  |
|  | Goods type |  | String |  |  |  |
|  | Goods list |  | String |  |  |  |
| 20 | Payment by |  | String |  |  |  |
| 21 | Account number |  | String |  |  |  |
| 22 | Cheque no |  | String |  |  |  |
| 23 | Send currency |  | String | 3 |  |  |
| 24 | Send amount |  | `Number` |  |  | số có hai số thập phân |
| 25 | Total amount payable |  | `Number` |  |  | số có hai số thập phân |
| 26 | Receive currency |  | String | 3 |  |  |
| 27 | Cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 28 | Receive amount |  | `Number` |  |  | số có hai số thập phân |
| 29 | Description |  | String |  |  |  |
| 30 | Transaction date |  | `Date` |  |  |  |
| 31 | User id |  | String |  |  |  |
| 32 | Transaction status |  | String |  |  |  |
| 33 | Total fee |  | `Number` |  |  | số có hai số thập phân |
| 34 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 35 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CRBANK | PMT_OIT_GET_INFO_AGENT_BANK | [PMT_OIT_GET_INFO_AGENT_BANK](Specification/Common/16 Payment Rulefuncs) |
| 2 | GET_INFO_DPT_CCCR | PMT_GET_INFO_DPT_CCCR | [PMT_GET_INFO_DPT_CCCR](Specification/Common/16 Payment Rulefuncs) |
| 3 | GET_INFO_ACT_CCCR | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 4 | GET_INFO_ACT_SACNO | PMT_GET_INFO_ACT_SACNO | [PMT_GET_INFO_ACT_SACNO](Specification/Common/16 Payment Rulefuncs) |
| 5 | GET_AVAI_BAL | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 6 | GET_INFO_CCHQNO | DPT_GET_INFO_PDACC_BY_CHEQUE_SERIAL | [DPT_GET_INFO_PDACC_BY_CHEQUE_SERIAL](Specification/Common/15 Deposit Rulefuncs) |
| 7 | LKP_DATA_CNCAT | PMT_LOOKUP_PAYMENT_CATALOG | [PMT_LOOKUP_PAYMENT_CATALOG](Specification/Common/16 Payment Rulefuncs) |
| 8 | LKP_DATA_CRBANK | PMT_LOOKUP_AGENTBANK_INTERNAL | [PMT_LOOKUP_AGENTBANK_INTERNAL](Specification/Common/16 Payment Rulefuncs) |
| 9 | LKP_DATA_ACT_SACNO | PMT_LKP_DATA_SACNO | [PMT_LKP_DATA_SACNO](Specification/Common/16 Payment Rulefuncs) |
| 10 | TRAN_FEE_PMT_OIT/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) |
| 11 | GET_INFO_SAME_CCCR | SELECT '@CCCR' FROM DUAL | `RCCR` = `CCCR` Làm trên JWEB |