# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/Catalog​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_SEARCH_SP_PMTCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `​/api​/Catalog​/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String |  |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Output format | output_format | String |  |  |
| 4 | Direction | direction | String |  |  |
| 5 | Instrument | instrument | String |  |  |
| 6 | Status | catalog_status | String |  |  |
| 7 | Export swift file | export_file | String |  |  |
| 8 | Send email | send_mail | String |  |  |
| 9 | Message type | message_type | String |  |  |
| 10 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Catalog​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_SEARCH_ADV_PMTCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  |  |
| 2 | Catalogue name | catalog_name | No | String |  |  |  |
| 3 | Output format | output_format | No | String |  |  |  |
| 4 | Direction | direction | No | String |  |  |  |
| 5 | Instrument | instrument | No | String |  |  |  |
| 6 | Status | catalog_status | No | String |  |  |  |
| 7 | Export swift file | export_file | No | String |  |  |  |
| 8 | Send email | send_mail | No | String |  |  |  |
| 9 | Message type | message_type | No | String |  |  |  |
| 10 | Page index | page_index | No | `Number` |  |  |  |
| 11 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String |  |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Output format | output_format | String |  |  |
| 4 | Direction | direction | String |  |  |
| 5 | Instrument | instrument | String |  |  |
| 6 | Status | catalog_status | String |  |  |
| 7 | Export swift file | export_file | String |  |  |
| 8 | Send email | send_mail | String |  |  |
| 9 | Message type | message_type | String |  |  |
| 10 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Catalog​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_INSERT_PMTCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | `Yes` | String | 8 |  | is unique |
| 2 | Catalogue name | catalog_name | `Yes` | String |  |  |  |
| 3 | Output format | output_format | `Yes` | String |  | S |  |
| 4 | Direction | direction | `Yes` | String |  | I |  |
| 5 | Instrument | instrument | `Yes` | String |  | B |  |
| 6 | Purpose | purpose | `Yes` | String |  | A |  |
| 7 | Holding days | holding_days | `Yes` | `Number` |  | 0 |  |
| 8 | Catalogue status | catalog_status | `Yes` | String |  | N |  |
| 9 | Message status | message_status | No | String |  |  | không dùng, xem xét xóa đi |
| 10 | Message type | message_type | `Yes` | String |  | MT103 |  |
| 11 | Export swift | export_file | `Yes` | String |  | Y |  |
| 12 | Send by email | send_mail | `Yes` | String |  | Y |  |
| 13 | Group code | group_pmt_ins_code | No | String |  |  |  |
| 14 | Tariff code | tariff_code | No | `Number` |  |  |  |
| 15 | Group id | accounting_group_id | No | `Number` |  |  |  |
| 16 | Payment classification | payment_classification | No | String |  |  | không dùng, xem xét xóa đi |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Catalogue name | catalog_name | String |  |  |
| 4 | Output format | output_format | String |  |  |
| 5 | Direction | direction | String |  |  |
| 6 | Instrument | instrument | String |  |  |
| 7 | Purpose | purpose | String |  |  |
| 8 | Holding days | holding_days | `Number` |  |  |
| 9 | Status | catalog_status | String |  |  |
| 10 | Created by | user_created | `Number` |  |  |
| 11 | Approved by | user_approved | `Number` |  |  |
| 12 | Message type | message_type | String |  |  |
| 13 | Export swift | export_file | String |  |  |
| 14 | Send by email | send_mail | String |  |  |
| 15 | Group code | group_pmt_ins_code | String |  |  |
| 16 | Tariff code | tariff_code | `Number` |  |  |
| 17 | Group id | accounting_group_id | `Number` |  |  |
| 18 | Message status | message_status | String |  | không dùng, xem xét xóa đi |
| 19 | Payment classification | payment_classification | String |  | không dùng, xem xét xóa đi |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/Catalog​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_VIEW_PMTCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

**Example:** `/api/Catalog​/View/8`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Catalogue name | catalog_name | String |  |  |
| 4 | Output format | output_format | String |  |  |
| 5 | Direction | direction | String |  |  |
| 6 | Instrument | instrument | String |  |  |
| 7 | Purpose | purpose | String |  |  |
| 8 | Holding days | holding_days | `Number` |  |  |
| 9 | Status | catalog_status | String |  |  |
| 10 | Created by | user_created | `Number` |  |  |
| 11 | Approved by | user_approved | `Number` |  |  |
| 12 | Message type | message_type | String |  |  |
| 13 | Export swift | export_file | String |  |  |
| 14 | Send by email | send_mail | String |  |  |
| 15 | Group code | group_pmt_ins_code | String |  |  |
| 16 | Tariff code | tariff_code | `Number` |  |  |
| 17 | Group id | accounting_group_id | `Number` |  |  |
| 18 | Message status | message_status | String |  | không dùng, xem xét xóa đi |
| 19 | Payment classification | payment_classification | String |  | không dùng, xem xét xóa đi |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `​/api​/Catalog​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_UPDATE_PMTCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |
| 2 | Catalogue name | catalog_name | `Yes` | String |  |  |  |
| 3 | Output format | output_format | `Yes` | String |  | S |  |
| 4 | Direction | direction | `Yes` | String |  | I |  |
| 5 | Instrument | instrument | `Yes` | String |  | B |  |
| 6 | Purpose | purpose | `Yes` | String |  | A |  |
| 7 | Holding days | holding_days | `Yes` | `Number` |  | 0 |  |
| 8 | Status | catalog_status | `Yes` | String |  | N |  |
| 9 | Message type | message_type | `Yes` | String |  | MT103 |  |
| 10 | Export swift | export_file | `Yes` | String |  | Y |  |
| 11 | Send by email | send_mail | `Yes` | String |  | Y |  |
| 12 | Group code | group_pmt_ins_code | No | String |  |  |  |
| 13 | Tariff code | tariff_code | No | `Number` |  |  |  |
| 14 | Group id | accounting_group_id | No | `Number` |  |  |  |
|  | Payment classification | payment_classification | String |  | không dùng, xem xét xóa đi |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Catalogue name | catalog_name | String |  |  |
| 4 | Output format | output_format | String |  |  |
| 5 | Direction | direction | String |  |  |
| 6 | Instrument | instrument | String |  |  |
| 7 | Purpose | purpose | String |  |  |
| 8 | Holding days | holding_days | `Number` |  |  |
| 9 | Status | catalog_status | String |  |  |
| 10 | Created by | user_created | `Number` |  |  |
| 11 | Approved by | user_approved | `Number` |  |  |
| 12 | Message type | message_type | String |  |  |
| 13 | Export swift | export_file | String |  |  |
| 14 | Send by email | send_mail | String |  |  |
| 15 | Group code | group_pmt_ins_code | String |  |  |
| 16 | Tariff code | tariff_code | `Number` |  |  |
| 17 | Group id | accounting_group_id | `Number` |  |  |
| 18 | Message status | message_status | String |  | không dùng, xem xét xóa đi |
| 19 | Payment classification | payment_classification | String |  | không dùng, xem xét xóa đi |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `​/api​/Catalog​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_DELETE_PMTCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

**Example:** `/api/Catalog/Delete?id=8`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list accounting group for payment
- "module": "PMT"
- [Accounting group trong "Rulefunc"](Specification/Common/01 Rulefunc)