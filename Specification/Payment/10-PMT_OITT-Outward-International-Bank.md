# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `PMT_OITT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Nostro bank | nostro_bank | `Yes` | String |  |  |  | `crbank` |
| 2 | Beneficiary bank | beneficiary_bank | `Yes` | String |  |  |  | `crbank2` |
| 3 | Bank address | bank_address | No | String |  |  |  | `addbank` |
| 4 | Bank country | bank_country | No | String |  |  |  | `cntrbank` |
| 5 | Product Code | product_code | `Yes` | String |  |  |  | `cncat` |
| 6 | Reference number | reference_number | No | String |  |  |  | `msgcode` |
| 7 | Sender name | sender_name | `Yes` | String |  |  |  | `ccatnm` |
| 8 | Sender id | sender_id | `Yes` | String |  |  |  | `c_license` |
| 9 | Sender address | sender_address | No | String |  |  |  | `cctma` |
| 10 | Sender phone | sender_phone | No | String | 3 |  | | `cstel` |
| 11 | Sender type | sender_type | No | String |  |  |  | `cstype` |
| 12 | beneficiary account number | beneficiary_account_number | `Yes` | String |  |  |  | `racc` |
| 13 | Receiver name | receiver_name | `Yes` | String |  |  |  | `crname` |
| 14 | Receiver id | receiver_id | No | String |  |  |  | `crid` |
| 15 | Receiver address | receiver_address | No | String | 3 |  | | `craddr` |
| 16 | Receiver phone | receiver_phone | No | String |  |  |  | `crtel` |
| 17 | Receiver type | receiver_type | No | String |  |  |  | `crtype` |
| 18 | Country | country | No | String |  |  |  | `ccountry` |
| 19 | Purpose | purpose | No | String |  |  |  | `cspur` |
| 20 | Sender to receiver info | sender_to_receiver_info | `Yes` | String |  |  |  | `ctxg1` |
| 21 | TEST-KEY number | test_key_number | No | String |  |  |  | `ctxg2` |
| 22 | Other information | other_information | No | String |  |  |  | `minfo` |
|  | Goods type | goods_type | No | String |  |  |  | `cgtype` |
|  | Goods list | goods_list | No | String |  |  |  | `mglst` |
| 23 | Payment by | payment_by | No | String |  |  |  | `payby` |
| 24 | Account number | account_number | No | String |  |  |  | `sacno` |
| 25 | Cheque No | cheque_no | No | String |  |  |  | `cchqno` |
| 26 | Currency | currency_code | `Yes` | String |  |  |  | `cccr` |
| 27 | Amount before fees | amount_before_fees | `Yes` | `Number` |  |  | số có hai số thập phân | `psamt` |
| 28 | Fee | fee | No | `Number` |  |  |  | `cfamt` |
| 29 | Amount receive from customer | amount_receive_from_customer | No | `Number` |  |  | số có hai số thập phân | `pcscvt` |
| 30 | Remitting currency | remitting_currency | `Yes` | String |  |  |  | `rccr` |
| 31 | Cross rate | cross_rate | No | `Number` |  |  | số có 9 số thập phân | `ccrrate` |
| 32 | Reverse rate | reverse_rate | No | `Number` |  |  | số có 9 số thập phân | `swrate` |
| 33 | Display rate | display_rate | No | String |  |  |  | `displayrate` |
| 34 | Remitting amount | remitting_amount | `Yes` | `Number` |  |  | số có hai số thập phân | `pramt` |
| 35 | Remitting amount in base currency | remitting_amount_in_base_currency | No | `Number` |  |  | số có hai số thập phân | `cbamt` |
| 36 | Description | description | No | String |  |  |   | `descs` |
| 37 | Payment exchange rate | payment_exchange_rate | No | `Number` |  |  | số có 9 số thập phân | `pgexr` |
| 38 | Remitting exchange rate | remitting_exchange_rate | No | `Number` |  |  | số có 9 số thập phân | `ctxexr` |
| 39 | Nostro Account | nostro_account | `Yes` | String |  |  |  |  `cnostro` |
| 40 | Detail of charges | detail_of_charges | `Yes` | String |  |  |  |  `dtlchr` |
| 41 | Fee in remitting currency | fee_in_remitting_currency | No | `Number` |  |  | số có hai số thập phân | `cfamt1` |
| 42 | Message type | message_type | No | String |  |  |  |  `cmsg` |
| 43 | Sender information | sender_information | No | String |  |  | trường ẩn | `ctxg3` |
| 44 | Receiver information | receiver_information | No | String |  |  | trường ẩn | `ctxg4` |
| 45 | Other information | other_information_ctxg5 | No | String |  |  | trường ẩn | `ctxg5` |
| 46 | Inclusive | inclusive | No | String |  |  | trường ẩn | `cinc` |
| 47 | VAT | vat | `Yes` | `Number` |  |  | trường ẩn | `cvat` |
| 48 | Receive by | receive_by | No | String |  |  | trường ẩn | `recby` |
| 49 | Branch cd | branch_cd | No | String |  |  | trường ẩn | `cbrcd` |
| 50 | Payment instruction | payment_instruction | No | String |  |  | trường ẩn | `mpii` |
| 51 | Linkage account | linkage_account | No | String |  |  | trường ẩn | `pdacc` |
| 52 | Linkage amount | linkage_amount | No | String |  |  | trường ẩn | `camt4` |
| 53 | Share fee rate | share_fee_rate | No | `Number` |  |  | trường ẩn | `sfrate` |
| 54 | Share fee amount | share_fee_amount | No | `Number` |  |  | trường ẩn | `sfamt` |
| 55 | Total Fee for Cash | total_fee_for_cash | No | `Number` |  |  | trường ẩn | `tfcsh` |
| 56 | Total send amount | total_send_amount | No | `Number` |  |  | trường ẩn | `csfee` |
| 57 | Total Fee for Deposit | total_fee_for_deposit | No | `Number` |  |  | trường ẩn | `tfdpt` |
| 58 | Total Fee for Accounting | total_fee_for_accounting | No | `Number` |  |  | trường ẩn | `tfact` |
| 59 | Branch for Deposit account | branch_for_deposit_account | No | String |  |  | trường ẩn | `sbid` |
| 60 | Current balance | current_balance | No | `Number` |  |  | trường ẩn | `camt2` |
| 61 | Available balance | available_balance_camt3 | `Yes` | `Number` |  |  | trường ẩn | `camt3` |
| 62 | Available balance | available_balance_cipaid | No | `Number` |  |  | trường ẩn | `cipaid` |
| 63 | Fax number | fax_number | No | String |  |  | trường ẩn | `pmref` |
| 64 | Current Deposit Account | current_deposit_account | No | String |  |  | trường ẩn | `pdoacc` |
| 65 | Fee in based currency | fee_in_based_currency | No | `Number` |  |  | trường ẩn | `txamt` |
| 66 | Rate RCCR | rate_rccr | No | `Number` |  |  | trường ẩn | `orgrate` |
| 67 | Check rate | check_rate | No | String |  |  | trường ẩn | `chkrate` |
| 68 | Nostro bank name | nostro_bank_name | No | String |  |  | trường ẩn | `ctxg8` |
| 69 | Beneficiary bank name | beneficiary_bank_name | No | String |  |  | trường ẩn | `ctxg9` |
| 70 | Fee data |  | No | JSON Object |  |  |  |
|  | IFC code |  | No | `Number` |  |  |  |
|  | Value type |  | No | String |  |  |  |
|  | Value |  | No | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | No | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | No | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | No | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | No | String | 3 |  |  |
| 71 | Transaction date |  | `Yes` | `Date` |  |  | Working date |  |  |
| 72 | User id |  | `Yes` | String |  |  | User login |  | 

### Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Nostro bank | nostro_bank | String |  |  |  |
| 3 | Beneficiary bank | beneficiary_bank | String |  |  |  |
| 4 | Bank address | bank_address | String |  |  |  |
| 5 | Bank country | bank_country | String |  |  |  |
| 6 | Product Code | product_code | String |  |  |  |
| 7 | Reference number | reference_number | String |  |  |  |
| 8 | Sender name | sender_name | String |  |  |  |
| 9 | Sender id | sender_id | String |  |  |  |
| 10 | Sender address | sender_address | String |  |  |  |
| 11 | Sender phone | sender_phone | String |  | |
| 12 | Sender type | sender_type | String |  |  |  |
| 13 | beneficiary account number | beneficiary_account_number | String |  |  |  |
| 14 | Receiver name | receiver_name | String |  |  |  |
| 15 | Receiver id | receiver_id | String |  |  |  |
| 16 | Receiver address | receiver_address | String |  | |
| 17 | Receiver phone | receiver_phone | String |  |  |  |
| 18 | Receiver type | receiver_type | String |  |  |  |
| 19 | Country | country | String |  |  |  |
| 20 | Purpose | purpose | String |  |  |  |
| 21 | Sender to receiver info | sender_to_receiver_info | String |  |  |  |
| 22 | TEST-KEY number | test_key_number | String |  |  |  |
| 23 | Other information | other_information | String |  |  |  |
|  | Goods type | goods_type | String |  |  |  |
|  | Goods list | goods_list | String |  |  |  |
| 24 | Payment by | payment_by | String |  |  |  |
| 25 | Account number | account_number | String |  |  |  |
| 26 | Cheque No | cheque_no | String |  |  |  |
| 27 | Currency | currency_code | String | 3 |  |  |
| 28 | Amount before fees | amount_before_fees | `Number` |  |  |  |
| 29 | Fee | fee | `Number` |  |  |  |
| 30 | Amount receive from customer | amount_receive_from_customer | `Number` |  |  |  |
| 31 | Remitting currency | remitting_currency | String | 3 |  |  |
| 32 | Cross rate | cross_rate | `Number` |  |  |  |
| 33 | Reverse rate | reverse_rate | `Number` |  |  |  |
| 34 | Display rate | display_rate | String |  |  |  |
| 35 | Remitting amount | remitting_amount | `Number` |  |  |  |
| 36 | Remitting amount in base currency | remitting_amount_in_base_currency | `Number` |  |  |  |
| 37 | Description | description | String |  |  |  |
| 38 | Payment exchange rate | payment_exchange_rate | `Number` |  |  |  |
| 39 | Remitting exchange rate | remitting_exchange_rate | `Number` |  |  |  |
| 40 | Nostro Account | nostro_account | String |  |  |  |
| 41 | Detail of charges | detail_of_charges | String |  |  |  |
| 42 | Fee in remitting currency | fee_in_remitting_currency | `Number` |  |  |  |
| 43 | Message type | message_type | String |  |  |  |
| 44 | Sender information | sender_information | String |  |  |  |
| 45 | Receiver information | receiver_information | String |  |  |  |
| 46 | Other information | other_information_ctxg5 | String |  |  |  |
| 47 | Inclusive | inclusive | String |  |  |  |
| 48 | VAT | vat | `Number` |  |  |  |
| 49 | Receive by | receive_by | String |  |  |  |
| 50 | Branch cd | branch_cd | String |  |  |  |
| 51 | Payment instruction | payment_instruction | String |  |  |  |
| 52 | Linkage account | linkage_account | String |  |  |  |
| 53 | Linkage amount | linkage_amount | String |  |  |  |
| 54 | Share fee rate | share_fee_rate | `Number` |  |  |  |
| 55 | Share fee amount | share_fee_amount | `Number` |  |  |  |
| 56 | Total Fee for Cash | total_fee_for_cash | `Number` |  |  |  |
| 57 | Total send amount | total_send_amount | `Number` |  |  |  |
| 58 | Total Fee for Deposit | total_fee_for_deposit | `Number` |  |  |  |
| 59 | Total Fee for Accounting | total_fee_for_accounting | `Number` |  |  |  |
| 60 | Branch for Deposit account | branch_for_deposit_account | String |  |  |  |
| 61 | Current balance | current_balance | `Number` |  |  |  |
| 62 | Available balance | available_balance_camt3 | `Number` |  |  |  |
| 63 | Available balance | available_balance_cipaid | `Number` |  |  |  |
| 64 | Fax number | fax_number | String |  |  |  |
| 65 | Current Deposit Account | current_deposit_account | String |  |  |  |
| 66 | Fee in based currency | fee_in_based_currency | `Number` |  |  |  |
| 67 | Rate RCCR | rate_rccr | `Number` |  |  |  |
| 68 | Check rate | check_rate | String |  |  |  |
| 69 | Nostro bank name | nostro_bank_name | String |  |  |  |
| 70 | Beneficiary bank name | beneficiary_bank_name | String |  |  |  |
| 71 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 72 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Catalogue" is Outward International Transfer.
- Send amount > 0.00 and Receive amount > 0.00
- "Nostro bank" must be register in "List/Correspondent Bank". With type "International"
- "Nostro account": is GL account, exists in "Bank account definition". And allow credit side.
    - "Nostro account" is defined at "List/Correspondent Bank" and allow entering by manual.

**Flow of events:**
- Support method to payment includes: Cash/ Deposit/ Accounting
    - If method is Deposit:
        - Deposit account exists with status "Normal". Type is current or saving
        - Balance has enough to payment
        - Allow payment by cheque.
        - Auto fill information of sender get from customer profile.
    - If method is GL:
        - GL exists in account chart and same branch with user login.
        - Allow debit side.
- "Beneficiary bank" and "Beneficiary account number" are mandatory enter by manual.
- Allow send currency and receive currency are different:
    - Debit rate get from table "foreign exchange" = Transferring buy (TB)
    - Credit rate get from table "foreign exchange" = Transferring sell (TA)
    - Cross rate = Debit rate / Credit rate.
        - Number rounding is 9 decimals
- If Cross rate < 1, system is more flexible about show exchange rate on voucher. It can be:
    - Reverse rate = 1 / Cross rate.
Number rounding is 9 decimals
- If user do any action that impact to change Cross rate. System will suggest approve.
- Allow collect fee on transaction (if any).
- Show total value of payment include send amount and fee amount. 
- Support to print voucher for internal and sender transaction.
- Transaction complete:
    - Create message payment into queue and wait approval.
        - Message direction: Outward
        - Message type: Swift
        - Message status: New 
        - Branch Approve status: New
    - Credit GL nostro
    - If Payment is method "Cash"
        - Cash of user will increase
    - If Payment is method "Deposit"
        - Balance of deposit account will decrease
    - If Payment is method "Accounting"
        - Debit GL
- When approve message, system will export message with standard "MT103".
    - Base setup parameter of catalogue "Outward International transfer".

**Database:**


**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASH/DEPOSIT/GL | Remittance amount | Send currency |
| 1 | C | GL (Nostro account) | Remittance amount | Send currency |

- Note: Nostro account is defined at "Correspondent Bank".

- Case collect fee (fee amount > 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 2 | D | CASH/DEPOSIT/GL | Fee amount | Send currency |
| 2 | C | IFCC (income) | Fee amount | Send currency |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_INFO_NOSTRO | GET_INFO_CIBNK_AND_NOSTRO_GL | id = nostro_bank, [GET_INFO_CIBNK_AND_NOSTRO_GL](Specification/Common/16 Payment Rulefuncs) |
| 2 | GET_INFO_CRBANK | PMT_OITT_GET_INFO_AGENT_BANK | [PMT_OITT_GET_INFO_AGENT_BANK](Specification/Common/16 Payment Rulefuncs) |
| 3 | GET_INFO_CRBANK2 | nhập `crbank2` trả `ctxg9` | Làm trên JWEB | `beneficiary_bank` | `beneficiary_bank_name` |
| 4 | GET_INFO_SAME_CCCR | nhập `cccr` trả `rccr` | Làm trên JWEB | `currency_code` | `remitting_currency` |
| 5 | GET_INFO_PAYBY_CSH | SELECT null,null,null,null,null,null FROM DUAL | Làm trên JWEB |  |  |
| 6 | LKP_DATA_CNCAT | PMT_LOOKUP_PAYMENT_CATALOG | [PMT_LOOKUP_PAYMENT_CATALOG](Specification/Common/16 Payment Rulefuncs) | direction = O, output_format=S |  |
| 7 | TRAN_FEE_PMT_OITT/GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) | `ifc_code` = `ifc_code`, `currency_code` = `currency_code` |  |
| 8 | TRAN_FEE_PMT_OITT/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) | `currency_code` = `currency_code` |  |
| 9 | LKP_DATA_SACNO | PMT_LKP_DATA_SACNO | [PMT_LKP_DATA_SACNO](Specification/Common/16 Payment Rulefuncs) |
| 10 | GET_INFO_DPT_CCCR | PMT_GET_INFO_DPT_CCCR | [PMT_GET_INFO_DPT_CCCR](Specification/Common/16 Payment Rulefuncs) |
| 11 | GET_INFO_ACT_RACNO | PMT_GET_INFO_ACT_SACNO | [PMT_GET_INFO_ACT_SACNO](Specification/Common/16 Payment Rulefuncs) |
| 12 | LKP_DATA_BACNO | ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY | [ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY](Specification/Common/11 Accounting Rulefuncs) |
| 13 | LKP_DATA_CRBANK | PMT_LOOKUP_AGENTBANK_INTERNATIONAL | [PMT_LOOKUP_AGENTBANK_INTERNATIONAL](Specification/Common/16 Payment Rulefuncs) |
| 14 | GET_INFO_ON_CCCR | PMT_GET_INFO_ON_CCCR | [PMT_GET_INFO_ON_CCCR](Specification/Common/16 Payment Rulefuncs) |
| 15 | crossrate | FX_GET_CROSS_RATE | [FX_GET_CROSS_RATE](Specification/Common/20 FX Rulefuncs) |  |  |
| 16 | BaseRate | FX_RULEFUNC_GET_FX_BCYRATE | [FX_RULEFUNC_GET_FX_BCYRATE](Specification/Common/20 FX Rulefuncs) | rate_type='TA', branch_id_fx= branch đang làm giao dịch |  |
| 17 | GET_INFO_ON_CCRRATE | SELECT ROUND (1 / '@CCRRATE', 9) as SWRATE FROM DUAL | làm trên JWEB | CCRRATE | SWRATE = 1/ CCRRATE (làm tròn 9) |
| 18 | GET_INFO_ON_SWRATE | SELECT ROUND (1 / '@SWRATE', 9) AS swrate FROM DUAL | làm trên JWEB | SWRATE | CCRRATE = 1/ SWRATE (làm tròn 9) |
| 19 | GET_INFO_ON_PRAMT | select ROUND('@PRAMT'/'@PSAMT',9) CCRRATE, ROUND('@PSAMT'/'@PRAMT',9) SWRATE, '@PRAMT'-'@CFAMT' from dual | làm trên JWEB [GET_INFO_ON_PRAMT](Specification/Common/16 Payment Rulefuncs) | | |