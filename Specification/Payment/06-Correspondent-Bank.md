# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/CorrespondentBank​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_SEARCH_SP_AGENTBANK`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `​/api​/CorrespondentBank​/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | BIC code of bank | bic_code | String |  |  |
| 2 | Bank name | bank_name | String |  |  |
| 3 | Present country code | country | String |  |  |
| 4 | Bank type | bank_type | String |  |  |
| 5 | Status | bank_status | String |  |  |
| 6 | Bank id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CorrespondentBank​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_SEARCH_ADV_AGENTBANK`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | BIC code of bank | bic_code | No | String |  |  |  |
| 2 | Bank name | bank_name | No | String |  |  |  |
| 3 | Present country code | country | No | String |  |  |  |
| 4 | Bank type | bank_type | No | String |  |  |  |
| 5 | Status | bank_status | No | String |  |  |  |
| 6 | Page index | page_index | No | `Number` |  |  |  |
| 7 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | BIC code of bank | bic_code | String |  |  |
| 2 | Bank name | bank_name | String |  |  |
| 3 | Present country code | country | String |  |  |
| 4 | Bank type | bank_type | String |  |  |
| 5 | Status | bank_status | String |  |  |
| 6 | Bank id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CorrespondentBank​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_INSERT_AGENTBANK`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | BIC code/ SWIFT code | bic_code | Yes | String |  |  | is unique |
| 2 | Bank name | bank_name | Yes | String |  |  |  |
| 3 | Present country | country | Yes | String |  |  |  |
| 4 | City name | city_name | No | String |  |  |  |
| 5 | Address | address | Yes | String |  |  |  |
| 6 | Code of bank head office | head_office_code | No | String |  |  |  |
| 7 | Branch code | branch_id | No | String |  |  |  |
| 8 | Type | bank_type | Yes | String |  |  |  |
| 9 | Vostro account | vostro_account | No | JSON Object |  |  |  |
|  | Vostro account 1 | vostro_account_1 | No | String |  |  |  |
|  | Vostro account 2 | vostro_account_2 | No | String |  |  |  |
|  | Vostro account 3 | vostro_account_3 | No | String |  |  |  |
| 10 | More description | more_description | No | String |  |  |  |
| 11 | Status | bank_status | Yes | String |  |  |  |
| 12 | In short list | partner | Yes | String |  |  |  |
| 13 | Instruction of the bank | instruction_bank | No | String |  |  |  |
| 14 | Nostro account | nostro_account | No | JSON Object |  |  |  |
|  | Nostro account 1 | nostro_account_1 | No | String |  |  |  |
|  | Nostro account 2 | nostro_account_2 | No | String |  |  |  |
|  | Nostro account 3 | nostro_account_3 | No | String |  |  |  |
| 15 | Sending share fee rate | sending_share_fee_rate | No | `Number` |  |  | số có hai số thập phân |
| 16 | Receiving share fee rate | recieving_share_fee_rate | No | `Number` |  |  | số có hai số thập phân |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Bank id | id | `Number` |  |  |
| 2 | BIC code/ SWIFT code | bic_code | String |  |  |
| 3 | Bank name | bank_name | String |  |  |
| 4 | Present country | country | String |  |  |
| 5 | City name | city_name | String |  |  |
| 6 | Address | address | String |  |  |
| 7 | Code of bank head office | head_office_code | String |  |  |
| 8 | Branch code | branch_code | String |  |  |
| 9 | Type | bank_type | String |  |  |
| 10 | Vostro account | vostro_account | JSON Object |  |  |
|  | Vostro account 1 | vostro_account_1 | String |  |  |
|  | Vostro account 2 | vostro_account_2 | String |  |  |
|  | Vostro account 3 | vostro_account_3 | String |  |  |
| 11 | More description | more_description | String |  |  |
| 12 | Status | bank_status | String |  |  |
| 13 | In short list | partner | String |  |  |
| 14 | Instruction of the bank | instruction_bank | String |  |  |
| 15 | Nostro account | nostro_account  | JSON Object |  |  |
|  | Nostro account 1 | nostro_account_1 | String |  |  |
|  | Nostro account 2 | nostro_account_2 | String |  |  |
|  | Nostro account 3 | nostro_account_3 | String |  |  |
| 16 | Sending share fee rate | sending_share_fee_rate | `Number` |  | số có hai số thập phân |
| 17 | Receiving share fee rate | recieving_share_fee_rate | `Number` |  | số có hai số thập phân |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/CorrespondentBank​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_VIEW_AGENTBANK`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Bank id | id | `Yes` | `Number` |  |  |  |

**Example:** `/api/CorrespondentBank/View/1`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Bank id | id | `Number` |  |  |
| 2 | BIC code/ SWIFT code | bic_code | String |  |  |
| 3 | Bank name | bank_name | String |  |  |
| 4 | Present country | country | String |  |  |
| 5 | City name | city_name | String |  |  |
| 6 | Address | address | String |  |  |
| 7 | Code of bank head office | head_office_code | String |  |  |
| 8 | Branch code | branch_code | String |  |  |
| 9 | Type | bank_type | String |  |  |
| 10 | Vostro account | vostro_account | JSON Object |  |  |
|  | Vostro account 1 | vostro_account_1 | String |  |  |
|  | Vostro account 2 | vostro_account_2 | String |  |  |
|  | Vostro account 3 | vostro_account_3 | String |  |  |
| 11 | More description | more_description | String |  |  |
| 12 | Status | bank_status | String |  |  |
| 13 | In short list | partner | String |  |  |
| 14 | Instruction of the bank | instruction_bank | String |  |  |
| 15 | Nostro account | nostro_account  | JSON Object |  |  |
|  | Nostro account 1 | nostro_account_1 | String |  |  |
|  | Nostro account 2 | nostro_account_2 | String |  |  |
|  | Nostro account 3 | nostro_account_3 | String |  |  |
| 16 | Sending share fee rate | sending_share_fee_rate | `Number` |  | số có hai số thập phân |
| 17 | Receiving share fee rate | recieving_share_fee_rate | `Number` |  | số có hai số thập phân |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `​/api​/CorrespondentBank​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_UPDATE_AGENTBANK`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Bank id | id | `Yes` | `Number` |  |  |  |
| 2 | BIC code/ SWIFT code | bic_code | `Yes` | String |  |  |  |
| 3 | Bank name | bank_name | `Yes` | String |  |  |  |
| 4 | Present country | country | `Yes` | String |  |  |  |
| 5 | City name | city_name | No | String |  |  |  |
| 6 | Address | address | `Yes` | String |  |  |  |
| 7 | Code of bank head office | head_office_code | No | String |  |  |  |
| 8 | Branch code | branch_code | No | String |  |  |  |
| 9 | Type | bank_type | `Yes` | String |  |  |  |
| 10 | Vostro account | vostro_account | No | JSON Object |  |  |  |
|  | Vostro account 1 | vostro_account_1 | No | String |  |  |  |
|  | Vostro account 2 | vostro_account_2 | No | String |  |  |  |
|  | Vostro account 3 | vostro_account_3 | No | String |  |  |  |
| 11 | More description | more_description | No | String |  |  |  |
| 12 | Status | bank_status | `Yes` | String |  |  |  |
| 13 | In short list | partner | `Yes` | String |  |  |  |
| 14 | Instruction of the bank | instruction_bank | No | String |  |  |  |
| 15 | Nostro account | nostro_account | No | JSON Object |  |  |  |
|  | Nostro account 1 | nostro_account_1 | No | String |  |  |  |
|  | Nostro account 2 | nostro_account_2 | No | String |  |  |  |
|  | Nostro account 3 | nostro_account_3 | No | String |  |  |  |
| 16 | Sending share fee rate | sending_share_fee_rate | No | `Number` |  |  | số có hai số thập phân |
| 17 | Receiving share fee rate | recieving_share_fee_rate | No | `Number` |  |  | số có hai số thập phân |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Bank id | id | `Number` |  |  |
| 2 | BIC code/ SWIFT code | bic_code | String |  |  |
| 3 | Bank name | bank_name | String |  |  |
| 4 | Present country | country | String |  |  |
| 5 | City name | city_name | String |  |  |
| 6 | Address | address | String |  |  |
| 7 | Code of bank head office | head_office_code | String |  |  |
| 8 | Branch code | branch_code | String |  |  |
| 9 | Type | bank_type | String |  |  |
| 10 | Vostro account | vostro_account | JSON Object |  |  |
|  | Vostro account 1 | vostro_account_1 | String |  |  |
|  | Vostro account 2 | vostro_account_2 | String |  |  |
|  | Vostro account 3 | vostro_account_3 | String |  |  |
| 11 | More description | more_description | String |  |  |
| 12 | Status | bank_status | String |  |  |
| 13 | In short list | partner | String |  |  |
| 14 | Instruction of the bank | instruction_bank | String |  |  |
| 15 | Nostro account | nostro_account  | JSON Object |  |  |
|  | Nostro account 1 | nostro_account_1 | String |  |  |
|  | Nostro account 2 | nostro_account_2 | String |  |  |
|  | Nostro account 3 | nostro_account_3 | String |  |  |
| 16 | Sending share fee rate | sending_share_fee_rate | `Number` |  | số có hai số thập phân |
| 17 | Receiving share fee rate | recieving_share_fee_rate | `Number` |  | số có hai số thập phân |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `​/api​/CorrespondentBank​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_DELETE_AGENTBANK`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Bank id | id | `Yes` | `Number` |  |  |  |

**Example:** `/api/CorrespondentBank/Delete?id=1`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Bank id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |