# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Nostro bank |  | `Yes` | String |  |  |  |
| 2 | Beneficiary bank |  | `Yes` | String |  |  |  |
| 3 | Bank address |  | No | String |  |  |  |
| 4 | Bank country |  | `Yes` | String |  |  |  |
| 5 | Product code |  | `Yes` | String |  |  |  |
| 6 | Reference number |  | No | String |  |  |  |
| 7 | Sender name |  | `Yes` | String |  |  |  |
| 8 | Sender id |  | `Yes` | String |  |  |  |
| 9 | Sender address |  | No | String |  |  |  |
| 10 | Sender phone |  | No | String |  |  |  |
| 11 | Sender type |  | `Yes` | String |  |  |  |
| 12 | Beneficiary account number |  | `Yes` | String |  |  |  |
| 13 | Receiver name |  | `Yes` | String |  |  |  |
| 14 | Receiver id |  | No | String |  |  |  |
| 15 | Receiver address |  | No | String |  |  |  |
| 16 | Receiver phone |  | No | String |  |  |  |
| 17 | Receiver type |  | `Yes` | String |  |  |  |
| 18 | Country |  | `Yes` | String |  |  |  |
| 19 | Purpose |  | `Yes` | String |  |  |  |
| 20 | Sender to receiver info |  | `Yes` | String |  |  |  |
| 21 | TEST-KEY |  | No | String |  |  |  |
| 22 | Other information |  | No | JSON Object |  |  |  |
|  | Goods type |  | No | String |  |  |  |
|  | Goods list |  | No | String |  |  |  |
| 23 | Payment by |  | `Yes` | String |  |  |  |
| 24 | Account number |  | No | String |  |  |  |
| 25 | Cheque no |  | No | String |  |  |  |
| 26 | Currency |  | `Yes` | String | 3 |  |  |
| 27 | Amount before fees |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 28 | Fee |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 29 | Amount receive from customer |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 30 | Remitting currency |  | `Yes` | String | 3 |  |  |
| 31 | Cross rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 32 | Reverse rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 33 | Display rate |  | `Yes` | String |  |  |  |
| 34 | Remitting amount |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 35 | Amount convert to BCY |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 36 | Description |  | `Yes` | String |  |  |  |
| 37 | Payment exchange rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 38 | Remitting exchange rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 39 | Nostro account |  | `Yes` | String |  |  |  |
| 40 | Detail of charges |  | `Yes` | String |  |  |  |
| 41 | Commission temp before calculate fee |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 42 | Original rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 43 | Message type |  | `Yes` | String |  |  |  |
| 44 | Transaction date |  | `Yes` | `Date` |  | Working date |  |
| 45 | User id |  | `Yes` | String |  | User login |  |
| 46 | Total fee |  | No | `Number` |  |  | số có hai số thập phân |
| 47 | Fee data |  | No | JSON Object |  |  |  |
|  | IFC code |  | No | `Number` |  |  |  |
|  | Value type |  | No | String |  |  |  |
|  | Value |  | No | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | No | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | No | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | No | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | No | String | 3 |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Nostro bank |  | String |  |  |  |
| 3 | Beneficiary bank |  | String |  |  |  |
| 4 | Bank address |  | String |  |  |  |
| 5 | Bank country |  | String |  |  |  |
| 6 | Product code |  | String |  |  |  |
| 7 | Reference number |  | String |  |  |  |
| 8 | Sender name |  | String |  |  |  |
| 9 | Sender id |  | String |  |  |  |
| 10 | Sender address |  | String |  |  |  |
| 11 | Sender phone |  | String |  |  |  |
| 12 | Sender type |  | String |  |  |  |
| 13 | Beneficiary account number |  | String |  |  |  |
| 14 | Receiver name |  | String |  |  |  |
| 15 | Receiver id |  | String |  |  |  |
| 16 | Receiver address |  | String |  |  |  |
| 17 | Receiver phone |  | String |  |  |  |
| 18 | Receiver type |  | String |  |  |  |
| 19 | Country |  | String |  |  |  |
| 20 | Purpose |  | String |  |  |  |
| 21 | Sender to receiver info |  | String |  |  |  |
| 22 | TEST-KEY |  | String |  |  |  |
| 23 | Other information |  | JSON Object |  |  |  |
|  | Goods type |  | String |  |  |  |
|  | Goods list |  | String |  |  |  |
| 24 | Payment by |  | String |  |  |  |
| 25 | Account number |  | String |  |  |  |
| 26 | Cheque no |  | String |  |  |  |
| 27 | Currency |  | String | 3 |  |  |
| 28 | Amount before fees |  | `Number` |  |  | số có hai số thập phân |
| 29 | Fee |  | `Number` |  |  | số có hai số thập phân |
| 30 | Amount receive from customer |  | `Number` |  |  | số có hai số thập phân |
| 31 | Remitting currency |  | String | 3 |  |  |
| 32 | Cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 33 | Reverse rate |  | `Number` |  |  | số có 9 số thập phân |
| 34 | Display rate |  | String |  |  |  |
| 35 | Remitting amount |  | `Number` |  |  | số có hai số thập phân |
| 36 | Amount convert to BCY |  | `Number` |  |  | số có hai số thập phân |
| 37 | Description |  | String |  |  |  |
| 38 | Payment exchange rate |  | `Number` |  |  | số có 9 số thập phân |
| 39 | Remitting exchange rate |  | `Number` |  |  | số có 9 số thập phân |
| 40 | Nostro account |  | String |  |  |  |
| 41 | Detail of charges |  | String |  |  |  |
| 42 | Commission temp before calculate fee |  | `Number` |  |  | số có hai số thập phân |
| 43 | Original rate |  | `Number` |  |  | số có 9 số thập phân |
| 44 | Message type |  | String |  |  |  |
| 45 | Transaction date |  | `Date` |  |  |  |
| 46 | User id |  | String |  |  |  |
| 47 | Transaction status |  | String |  |  |  |
| 48 | Total fee |  | `Number` |  |  | số có hai số thập phân |
| 49 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 50 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Catalogue" is Outward Domestic Transfer.
- Send amount > 0 and Receive amount > 0.00
- "Nostro bank" must be register in correspondent bank. With type "Domestic"
- "Nostro account": is GL account, exists in "Bank account definition". And allow credit side
    - "Nostro account" is defined at "List/Correspondent Bank" and allow entering by manual.

**Flow of events:**
- Support method to payment includes: Cash/Deposit/Accounting
    - If method is Deposit:
        - Deposit account exists with status "Normal". Type is current or saving
        - Balance has enough to payment
        - Allow payment by cheque.
        - Auto fill information of sender get from customer profile.
    - If method is GL:
        - GL exists in account chart and same branch with user login.
        - Allow debit side.
- "Beneficiary bank" and "Beneficiary account number" are mandatory enter by manual.
- Allow send currency and receive currency are different:
    - Debit rate get from table "foreign exchange" = Transferring buy (TB)
    - Credit rate get from table "foreign exchange" = Transferring sell (TA)
    - Cross rate = Debit rate / Credit rate 
        - Number rounding is 9 decimals
- If Cross rate < 1, system is more flexible about show exchange rate on voucher. It can be:
    - Reverse rate = 1 / Cross rate.
        - Number rounding is 9 decimals
- Exchange rate allows exchanging 9 decimal digits.
- If user do any action that impact to change Cross rate. System will suggest approve.
- Allow collect fee on transaction (if any).
- Show total value of payment include send amount and fee amount. 
- Support to print voucher for internal and sender transaction.
- Transaction complete:
    - Create message payment into queue and wait approval.
        - Message direction: Outward
        - Message type: Domestic
        - Message status: New 
        - Branch Approve status: New
    - Credit GL nostro
    - If Payment is method "Cash"
        - Cash of user will increase
    - If Payment is method "Deposit"
        - Balance of deposit account will decrease
    - If Payment is method "Accounting"
        - Debit GL
- When approve message, system will export message with standard "MT103".
Base setup parameter of catalogue "Outward Domestic Transfer".

**Database:**


**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASH/DEPOSIT/GL | Remittance amount | Send currency |
| 1 | C | GL (Nostro account) | Remittance amount | Send currency |

- Note: Nostro account is defined at "Correspondent Bank".

- Case collect fee (fee amount > 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 2 | D | CASH/DEPOSIT/GL | Fee amount | Send currency |
| 2 | C | IFCC (income) | Fee amount | Send currency |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Nostro bank |  | String |  |  |  |
| 3 | Beneficiary bank |  | String |  |  |  |
| 4 | Bank address |  | String |  |  |  |
| 5 | Bank country |  | String |  |  |  |
| 6 | Product code |  | String |  |  |  |
| 7 | Reference number |  | String |  |  |  |
| 8 | Sender name |  | String |  |  |  |
| 9 | Sender id |  | String |  |  |  |
| 10 | Sender address |  | String |  |  |  |
| 11 | Sender phone |  | String |  |  |  |
| 12 | Sender type |  | String |  |  |  |
| 13 | Beneficiary account number |  | String |  |  |  |
| 14 | Receiver name |  | String |  |  |  |
| 15 | Receiver id |  | String |  |  |  |
| 16 | Receiver address |  | String |  |  |  |
| 17 | Receiver phone |  | String |  |  |  |
| 18 | Receiver type |  | String |  |  |  |
| 19 | Country |  | String |  |  |  |
| 20 | Purpose |  | String |  |  |  |
| 21 | Sender to receiver info |  | String |  |  |  |
| 22 | TEST-KEY |  | String |  |  |  |
| 23 | Other information |  | JSON Object |  |  |  |
|  | Goods type |  | String |  |  |  |
|  | Goods list |  | String |  |  |  |
| 24 | Payment by |  | String |  |  |  |
| 25 | Account number |  | String |  |  |  |
| 26 | Cheque no |  | String |  |  |  |
| 27 | Currency |  | String | 3 |  |  |
| 28 | Amount before fees |  | `Number` |  |  | số có hai số thập phân |
| 29 | Fee |  | `Number` |  |  | số có hai số thập phân |
| 30 | Amount receive from customer |  | `Number` |  |  | số có hai số thập phân |
| 31 | Remitting currency |  | String | 3 |  |  |
| 32 | Cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 33 | Reverse rate |  | `Number` |  |  | số có 9 số thập phân |
| 34 | Display rate |  | String |  |  |  |
| 35 | Remitting amount |  | `Number` |  |  | số có hai số thập phân |
| 36 | Amount convert to BCY |  | `Number` |  |  | số có hai số thập phân |
| 37 | Description |  | String |  |  |  |
| 38 | Payment exchange rate |  | `Number` |  |  | số có 9 số thập phân |
| 39 | Remitting exchange rate |  | `Number` |  |  | số có 9 số thập phân |
| 40 | Nostro account |  | String |  |  |  |
| 41 | Detail of charges |  | String |  |  |  |
| 42 | Commission temp before calculate fee |  | `Number` |  |  | số có hai số thập phân |
| 43 | Original rate |  | `Number` |  |  | số có 9 số thập phân |
| 44 | Message type |  | String |  |  |  |
| 45 | Transaction date |  | `Date` |  |  |  |
| 46 | User id |  | String |  |  |  |
| 47 | Transaction status |  | String |  |  |  |
| 48 | Total fee |  | `Number` |  |  | số có hai số thập phân |
| 49 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 50 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |


# 4. Rule function
- [Rulefunc](Specification/Common/01 Rulefunc)<br>