# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Nostro bank |  | `Yes` | String |  |  |  |
| 2 | Product code |  | `Yes` | String |  |  |  |
| 3 | Reference number |  | No | String |  |  |  |
| 4 | Receiver bank |  | `Yes` | String |  |  |  |
| 5 | Receive bank code |  | `Yes` | String |  |  |  |
| 6 | Receive bank name |  | `Yes` | String |  |  |  |
| 7 | Sender account |  | `Yes` | String |  |  |  |
| 8 | Sender name |  | `Yes` | String |  |  |  |
| 9 | Sender building number |  | `Yes` | String |  |  |  |
| 10 | Sender street |  | `Yes` | String |  |  |  |
| 11 | Sender town name |  | `Yes` | String |  |  |  |
| 12 | Postal code |  | `Yes` | String |  |  |  |
| 13 | Receiver account |  | `Yes` | String |  |  |  |
| 14 | Receiver name |  | `Yes` | String |  |  |  |
| 15 | Receiver building number |  | `Yes` | String |  |  |  |
| 16 | Receiver street |  | `Yes` | String |  |  |  |
| 17 | Receiver town name |  | `Yes` | String |  |  |  |
| 18 | Postal code |  | `Yes` | String |  |  |  |
| 19 | Sender currency |  | `Yes` | String | 3 |  |  |
| 20 | Send amount |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 21 | Fee amount |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 22 | Fee Pay by |  | `Yes` | String |  |  |  |
| 23 | Total deposit amount |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 24 | Total cash amount |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 25 | Remitting currency |  | `Yes` | String | 3 |  |  |
| 26 | Cross rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 27 | Reverse rate |  | `Yes` | `Number` |  |  | số có 9 số thập phân |
| 28 | Display rate |  | `Yes` | String |  |  |  |
| 29 | Remitting amount |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 30 | Remitting amount convert to KHR |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 31 | Nostro account |  | `Yes` | String |  |  |  |
| 32 | Sender to receiver information |  | `Yes` | String |  |  |  |
| 33 | Description |  | `Yes` | String |  |  |  |
| 34 | Referred document code |  | No | String |  |  |  |
| 35 | Referred document number |  | No | String |  |  |  |
| 36 | Transaction date |  | `Yes` | `Date` |  | Working date |  |
| 37 | User id |  | `Yes` | String |  | User login |  |
| 38 | Total fee |  | No | `Number` |  |  | số có hai số thập phân |
| 39 | Fee data |  | No | JSON Object |  |  |  |
|  | IFC code |  | No | `Number` |  |  |  |
|  | Value type |  | No | String |  |  |  |
|  | Value |  | No | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | No | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | No | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | No | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | No | String | 3 |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Nostro bank |  | String |  |  |  |
| 3 | Product code |  | String |  |  |  |
| 4 | Reference number |  | String |  |  |  |
| 5 | Receiver bank |  | String |  |  |  |
| 6 | Receive bank code |  | String |  |  |  |
| 7 | Receive bank name |  | String |  |  |  |
| 8 | Sender account |  | String |  |  |  |
| 9 | Sender name |  | String |  |  |  |
| 10 | Sender building number |  | String |  |  |  |
| 11 | Sender street |  | String |  |  |  |
| 12 | Sender town name |  | String |  |  |  |
| 13 | Postal code |  | String |  |  |  |
| 14 | Receiver account |  | String |  |  |  |
| 15 | Receiver name |  | String |  |  |  |
| 16 | Receiver building number |  | String |  |  |  |
| 17 | Receiver street |  | String |  |  |  |
| 18 | Receiver town name |  | String |  |  |  |
| 19 | Postal code |  | String |  |  |  |
| 20 | Sender currency |  | String | 3 |  |  |
| 21 | Send amount |  | `Number` |  |  | số có hai số thập phân |
| 22 | Fee amount |  | `Number` |  |  | số có hai số thập phân |
| 23 | Fee Pay by |  | String |  |  |  |
| 24 | Total deposit amount |  | `Number` |  |  | số có hai số thập phân |
| 25 | Total cash amount |  | `Number` |  |  | số có hai số thập phân |
| 26 | Remitting currency |  | String | 3 |  |  |
| 27 | Cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 28 | Reverse rate |  | `Number` |  |  | số có 9 số thập phân |
| 29 | Display rate |  | String |  |  |  |
| 30 | Remitting amount |  | `Number` |  |  | số có hai số thập phân |
| 31 | Remitting amount convert to KHR |  | `Number` |  |  | số có hai số thập phân |
| 32 | Nostro account |  | String |  |  |  |
| 33 | Sender to receiver information |  | String |  |  |  |
| 34 | Description |  | String |  |  |  |
| 35 | Referred document code |  | String |  |  |  |
| 36 | Referred document number |  | String |  |  |  |
| 37 | Transaction date |  | `Date` |  |  |  |
| 38 | User id |  | String |  |  |  |
| 39 | Transaction status |  | String |  |  |  |
| 40 | Total fee |  | `Number` |  |  | số có hai số thập phân |
| 41 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 42 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Catalogue" is Outward Domestic Transfer.
- This transaction only finished if catalogue's status is Normal.
- Send amount > 0.00 and Receive amount > 0.00
- "Nostro bank" must be register in correspondent bank. With type "Fast"
- "Nostro account": is GL account, exists in "Bank account definition". And allow debit side
    - "Nostro account" is defined at "Correspondent Bank" and allow entering by manual.

**Flow of events:**
- Support method to send includes: Deposit
    - Deposit account exists with status <> "Close". Type is current or saving
    - Auto fill information of sender. It gets from customer profile.
- Allow remitting currency and receive currency are different: KHR and USD only
    - Debit rate get from table "foreign exchange" = Transferring buy (TB)
    - Credit rate get from table "foreign exchange" = Transferring sell (TA)
    - Cross rate = Debit rate / Credit rate.
        - Number rounding is 9 decimals
- If Cross rate < 1, system is more flexible about show exchange rate on voucher. It can be:
    - Reverse rate = 1 / Cross rate.
        - Number rounding is 9 decimals
- If user do any action that impact to change Cross rate. System will suggest approve.
- Allow collect fee on transaction (if any). Fee amount will be calculated to follow converting amount, detail: 
    - If Remitting amount convert to KHR (0 - 4 MKHR). Value = 4,000 KHR
    - If Remitting amount convert to KHR (4 MKHR - 20 MKHR). Value = 8,000 KHR
    - If Remitting amount convert to KHR (20 MKHR - 40 MKHR). Value = 12,000 KHR
- Support to print voucher for internal and receive transaction.
- Transaction complete:
    - Purpose: Purchase sale of goods.
    - Message status: Outward paid
    - Fast status: Pending
    - End to end status: Pending for approver
    - Paid amount increase.
    - Balance of deposit account will decrease.
    - Credit GL nostro

**Database:**


**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Send amount | Send currency |
| 1 | C | GL (Nostro account) | Send amount | Send currency |

- Note: Nostro account is defined at "Correspondent Bank".

- Case collect fee (fee amount > 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 2 | D | CASH/DEPOSIT | Fee amount | Send currency |
| 2 | C | IFCC (income) | Fee amount | Send currency |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Nostro bank |  | String |  |  |  |
| 3 | Product code |  | String |  |  |  |
| 4 | Reference number |  | String |  |  |  |
| 5 | Receiver bank |  | String |  |  |  |
| 6 | Receive bank code |  | String |  |  |  |
| 7 | Receive bank name |  | String |  |  |  |
| 8 | Sender account |  | String |  |  |  |
| 9 | Sender name |  | String |  |  |  |
| 10 | Sender building number |  | String |  |  |  |
| 11 | Sender street |  | String |  |  |  |
| 12 | Sender town name |  | String |  |  |  |
| 13 | Postal code |  | String |  |  |  |
| 14 | Receiver account |  | String |  |  |  |
| 15 | Receiver name |  | String |  |  |  |
| 16 | Receiver building number |  | String |  |  |  |
| 17 | Receiver street |  | String |  |  |  |
| 18 | Receiver town name |  | String |  |  |  |
| 19 | Postal code |  | String |  |  |  |
| 20 | Sender currency |  | String | 3 |  |  |
| 21 | Send amount |  | `Number` |  |  | số có hai số thập phân |
| 22 | Fee amount |  | `Number` |  |  | số có hai số thập phân |
| 23 | Fee Pay by |  | String |  |  |  |
| 24 | Total deposit amount |  | `Number` |  |  | số có hai số thập phân |
| 25 | Total cash amount |  | `Number` |  |  | số có hai số thập phân |
| 26 | Remitting currency |  | String | 3 |  |  |
| 27 | Cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 28 | Reverse rate |  | `Number` |  |  | số có 9 số thập phân |
| 29 | Display rate |  | String |  |  |  |
| 30 | Remitting amount |  | `Number` |  |  | số có hai số thập phân |
| 31 | Remitting amount convert to KHR |  | `Number` |  |  | số có hai số thập phân |
| 32 | Nostro account |  | String |  |  |  |
| 33 | Sender to receiver information |  | String |  |  |  |
| 34 | Description |  | String |  |  |  |
| 35 | Referred document code |  | String |  |  |  |
| 36 | Referred document number |  | String |  |  |  |
| 37 | Transaction date |  | `Date` |  |  |  |
| 38 | User id |  | String |  |  |  |
| 39 | Transaction status |  | String |  |  |  |
| 40 | Total fee |  | `Number` |  |  | số có hai số thập phân |
| 41 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 42 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
- [Rulefunc](Specification/Common/01 Rulefunc)<br>