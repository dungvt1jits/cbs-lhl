# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `PMT_IIT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Product code | product_code | `Yes` | String | | | | cncat |
| 2 | Message code | message_code | `Yes` | String | | | | msgcode |
| 3 | Ordering institution | ordering_institution | `Yes` | String | | | | crbank2 |
| 4 | Remitting branch | remitting_branch | `Yes` | String | | | | crbank |
| 5 | Sender name | sender_name | `Yes` | String |  |  |  | ccatnm |
| 6 | Sender id | sender_id | `Yes` | String |  |  |  | c_license |
| 7 | Sender address | sender_address | No | String |  |  |  | cctma |
| 8 | Sender phone | sender_phone | No | String |  |  |  | cstel |
| 9 | Sender type | sender_type | `Yes` | String |  |  |  | cstype |
| 10 | Receiver name | receiver_name | `Yes` | String |  |  |  | crname |
| 11 | Receiver id | receiver_id | No | String |  |  |  | crid |
| 12 | Receiver address | receiver_address | No | String |  |  |  | craddr |
| 13 | Receiver phone | receiver_phone | No | String |  |  |  | crtel |
| 14 | Receiver type | receiver_type | `Yes` | String |  |  |  | crtype |
| 15 | Country | country | `Yes` | String |  |  |  | ccountry |
| 16 | Purpose | purpose | `Yes` | String |  |  |  | cspur |
| 17 | Sender to receiver information | sender_to_receiver_information | No | String |  |  |  | ctxg1 |
| 18 | TEST-KEY | test_key | No | String |  |  |  | ctxg2 |
| 19 | Other information | other_informations | No | JSON Object |  |  |  | minfo |
|  | Goods type | goods_type | No | String |  |  |  | cgtype |
|  | Goods list | goods_list | No | String |  |  |  | mglst |
| 20 | Send currency | send_currency | `Yes` | String | 3 | | | cccr |
| 21 | Send amount | send_amount | `Yes` | `Number` | | | số có hai số thập phân | psamt |
| 22 | Receive by | receive_by | `Yes` | String |  |  |  | recby |
| 23 | Receive account number | receive_account_number | No | String |  |  |  | racno |
| 24 | Receive currency | receive_currency | `Yes` | String | 3 | | | rccr |
| 25 | Cross rate | cross_rate | `Yes` | `Number` | | | số có 9 số thập phân | ccrrate |
| 26 | Receive amount | receive_amount | `Yes` | `Number` | | | số có hai số thập phân | pramt |
| 27 | Description | fee_data | `Yes` | String |  |  |  | descs |
| 28 | Receive branch id | receive_branch_id | `Yes` | String |  |  |  | cbrcd |
| 29 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | ifccd |
|  | Value | ifc_value | No | `Number` |  | 0 | số có 5 số thập phân | ifcval |
|  | Fee | ifc_amount | No | `Number` |  | 0 | số có hai số thập phân | ifcamt |
|  | Floor | floor_value | No | `Number` |  | 0 | số có hai số thập phân | flrval |
|  | Ceiling | ceiling_value | No | `Number` |  | 0 | số có hai số thập phân | ceival |
|  | Currency | currency_fee_code | No | String | 3 |  |  | ccrcd |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | ccrid |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | payrate |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | paysrc |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | ramt |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | rrate |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | samt |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | sfappl |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | srate |
| 30 | BANKNAME | bankname | No | String | | | trường ẩn | ctxg3 |
| 31 | PONUM | ponum | No | `Number` | | | trường ẩn | ctxg4 |
| 32 | Nostro Account | nostro_account | No | String | | | trường ẩn | cnostro |
| 33 | Share fee rate | share_fee_rate | No | `Number` | | | trường ẩn | sfrate |
| 34 | Accounting debit exchange rate/BCY | receive_accounting_debit_exchange_rate | No | `Number` | | | trường ẩn | ctxexr |
| 35 | Inclusive | inclusive | No | String | | | trường ẩn | cinc |
| 36 | Payment by | payment_by | No | String | | | trường ẩn | payby |
| 37 | Amount convert to BCY | amount_convert_to_bcy | No | `Number` | | | trường ẩn | cbamt |
| 38 | Commission | commission | No | `Number` | | | trường ẩn | cfamt |
| 39 | Fee | fee | No | `Number` | | | trường ẩn | cvat |
| 40 | Total amount | total_amount | No | `Number` | | | trường ẩn | pcscvt |
| 41 | Paid amount | paid_amount | No | `Number` | | | trường ẩn | camt1 |
| 42 | Accounting debit exchange rate/BCY | accounting_debit_exchange_rate | No | `Number` | | | trường ẩn | pgexr |
| 43 | PO type | po_type | No | String | | | trường ẩn | potype |
| 44 | PO number | po_number | No | `Number` | | | trường ẩn | pmref |
| 45 | Share fee amount | share_fee_amount | No | `Number` | | | trường ẩn | sfamt |
| 46 | Total Fee for Cash | total_fee_for_cash | No | `Number` | | | trường ẩn | tfcsh |
| 47 | Rate RCCR | rate_rccr | No | `Number` | | | trường ẩn | orgrate |
| 48 | Total send amount | total_send_amount | No | `Number` | | | trường ẩn | csfee |
| 49 | Account number | account_number | No | String | | | trường ẩn | pdacc |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Product code | product_code | String |  |  |  |
| 3 | Message code | message_code | String |  |  |  |
| 4 | Ordering institution | ordering_institution | String |  |  |  |
| 5 | Remitting branch | remitting_branch | String |  |  |  |
| 6 | Sender name | sender_name | String |  |  |  |
| 7 | Sender id | sender_id | String |  |  |  |
| 8 | Sender address | sender_address | String |  |  |  |
| 9 | Sender phone | sender_phone | String |  |  |  |
| 10 | Sender type | sender_type | String |  |  |  |
| 11 | Receiver name | receiver_name | String |  |  |  |
| 12 | Receiver id | receiver_id | String |  |  |  |
| 13 | Receiver address | receiver_address | String |  |  |  |
| 14 | Receiver phone | receiver_phone | String |  |  |  |
| 15 | Receiver type | receiver_type | String |  |  |  |
| 16 | Country | country | String |  |  |  |
| 17 | Purpose | purpose | String |  |  |  |
| 18 | Sender to receiver information | sender_to_receiver_information | String |  |  |  |
| 19 | TEST-KEY | test_key | String |  |  |  |
| 20 | Other information | other_informations | JSON Object |  |  |  |
|  | Goods type | goods_type | String |  |  |  |
|  | Goods list | goods_list | String |  |  |  |
| 21 | Send currency | send_currency | String | 3 |  |  |
| 22 | Send amount | send_amount | `Number` |  |  | số có hai số thập phân |
| 23 | Receive by | receive_by | String |  |  |  |
| 24 | Receive account number | receive_account_number | String |  |  |  |
| 25 | Receive currency | receive_currency | String | 3 |  |  | |
| 26 | Cross rate | cross_rate | `Number` |  |  | số có 9 số thập phân |
| 27 | Receive amount | receive_amount | `Number` |  |  | số có hai số thập phân |
| 28 | Description | fee_data | String |  |  |  |
| 29 | Receive branch id | receive_branch_id | String |  |  |  |
| 30 | BANKNAME | bankname | String |  |  |  |
| 31 | PONUM | ponum | `Number` |  |  |  |
| 32 | Nostro Account | nostro_account | String |  |  |  |
| 33 | Share fee rate | share_fee_rate | `Number` |  |  |  |
| 34 | Accounting debit exchange rate/BCY | receive_accounting_debit_exchange_rate | `Number` |  |  |  |
| 35 | Inclusive | inclusive | String |  |  |  |
| 36 | Payment by | payment_by | String |  |  |  |
| 37 | Amount convert to BCY | amount_convert_to_bcy | `Number` |  |  |  |
| 38 | Commission | commission | `Number` |  |  |  |
| 39 | Fee | fee | `Number` |  |  |  |
| 40 | Total amount | total_amount | `Number` |  |  |  |
| 41 | Paid amount | paid_amount | `Number` |  |  |  |
| 42 | Accounting debit exchange rate/BCY | accounting_debit_exchange_rate | `Number` |  |  |  |
| 43 | PO type | po_type | String |  |  |  |
| 44 | PO number | po_number | `Number` |  |  |  |
| 45 | Share fee amount | share_fee_amount | `Number` |  |  |  |
| 46 | Total Fee for Cash | total_fee_for_cash | `Number` |  |  |  |
| 47 | Rate RCCR | rate_rccr | `Number` |  |  |  |
| 48 | Total send amount | total_send_amount | `Number` |  |  |  |
| 49 | Account number | account_number | String |  |  |  |
| 50 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 51 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 52 | Transaction status | status | String |  |  |  |
| 53 | Transaction date | transaction_date | `Date time` |  |  |  |
| 54 | User id | user_id | `Number` |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Catalogue" is Inward inter-branch transfer.
- Receive currency must be same with send currency.
- Receive amount > 0.00
- Beneficiary branch must be same branch with user login.
- Branch Approve status: Approved
- Message status: Send

**Flow of events:**
- Support method to receive includes: Cash/Deposit/Accounting
    - If method is Deposit:
        - Deposit account exists with status <> "Close". Type is current or saving
        - Auto fill information of receiver. It gets from customer profile.
    - If method is GL:
        - GL exists in account chart and same branch with user login.
        - Allow credit side.
- Allow collect fee on transaction (if any).
- Show total value of receive include fee amount. 
- Support to print voucher for internal and receive transaction.
- Transaction complete:
    - Message status is "Paid".
    - If Payment is method "Cash"
        - Cash of user will decrease
    - If Payment is method "Deposit"
        - Balance of deposit account will increase
    - If Payment is method "Accounting"
        - Credit GL

![image](uploads/70d5d3090070a46511de1b2d314dd0a6/image.png)

**Database:**


**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | RMINTERNALSUP | Remittance amount | Receive currency |
| 1 | C | CASH/DEPOSIT/GL | Remittance amount | Receive currency |

- Note: **RMINTERNALSUP** is defined at "Accounting setup/Common account definition" with branch code is branch does this transaction, branch id is receive branch 

- Case collect fee (fee amount > 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 2 | D | CASH/DEPOSIT/GL | Fee amount | Receive currency |
| 2 | C | IFCC (income) | Fee amount | Receive currency |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Product code |  | String |  |  |  |
| 3 | Message code |  | String |  |  |  |
| 4 | Ordering institution |  | String |  |  |  |
| 5 | Remitting branch |  | String |  |  |  |
| 6 | Sender name |  | String |  |  |  |
| 7 | Sender id |  | String |  |  |  |
| 8 | Sender address |  | String |  |  |  |
| 9 | Sender phone |  | String |  |  |  |
| 10 | Sender type |  | String |  |  |  |
| 11 | Receiver name |  | String |  |  |  |
| 12 | Receiver id |  | String |  |  |  |
| 13 | Receiver address |  | String |  |  |  |
| 14 | Receiver phone |  | String |  |  |  |
| 15 | Receiver type |  | String |  |  |  |
| 16 | Country |  | String |  |  |  |
| 17 | Purpose |  | String |  |  |  |
| 18 | Sender to receiver information |  | String |  |  |  |
| 19 | TEST-Key |  | String |  |  |  |
| 20 | Other information |  | JSON Object |  |  |  |
|  | Goods type |  | String |  |  |  |
|  | Goods list |  | String |  |  |  |
| 21 | Send currency |  | String | 3 |  |  |
| 22 | Send amount |  | `Number` |  |  | số có hai số thập phân |
| 23 | Receive by |  | String |  |  |  |
| 24 | Account number |  | String |  |  |  |
| 25 | Receive currency |  | String | 3 |  |  |
| 26 | Cross rate |  | `Number` |  |  | số có 9 số thập phân |
| 27 | Receive amount |  | `Number` |  |  | số có hai số thập phân |
| 28 | Description |  | String |  |  |  |
| 29 | Receive branch id |  | String |  |  |  |
| 30 | Transaction date |  | `Date` |  |  |  |
| 31 | User id |  | String |  |  |  |
| 32 | Transaction status |  | String |  |  |  |
| 33 | Total fee |  | `Number` |  |  | số có hai số thập phân |
| 34 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 35 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_MSGCODE | PMT_GET_INFO_MESSAGE_INWARD_INTERNAL | [PMT_GET_INFO_MESSAGE_INWARD_INTERNAL](Specification/Common/16 Payment Rulefuncs) |
| 2 | GET_INFO_CRBANK1 | PMT_IIT_GET_INFO_AGENT_BANK | [PMT_IIT_GET_INFO_AGENT_BANK](Specification/Common/16 Payment Rulefuncs) |
| 3 | GET_INFO_ACT_CCCR | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 4 | GET_INFO_ACT_RACNO | PMT_GET_INFO_ACT_RACNO | [PMT_GET_INFO_ACT_RACNO](Specification/Common/16 Payment Rulefuncs) |
| 5 | GET_INFO_DPT_RCCR | PMT_GET_INFO_DPT_CCCR | [PMT_GET_INFO_DPT_CCCR](Specification/Common/16 Payment Rulefuncs) |
| 6 | LKP_DATA_CNCAT | PMT_LOOKUP_PAYMENT_CATALOG | [PMT_LOOKUP_PAYMENT_CATALOG](Specification/Common/16 Payment Rulefuncs) |
| 7 | LKP_DATA_CRBANK | PMT_LOOKUP_AGENTBANK_INTERNAL | [PMT_LOOKUP_AGENTBANK_INTERNAL](Specification/Common/16 Payment Rulefuncs) |(Specification/Common/11 Accounting Rulefuncs) |
| 8 | LKP_DATA_MSGCODE | PMT_LOOKUP_MESSAGE_INWARD_INTERNAL | [PMT_LOOKUP_MESSAGE_INWARD_INTERNAL](Specification/Common/16 Payment Rulefuncs) |
| 9 | LKP_DATA_ACT_RACNO | PMT_LKP_DATA_SACNO | [PMT_LKP_DATA_SACNO](Specification/Common//16 Payment Rulefuncs) |
| 11 | TRAN_FEE_PMT_IIT/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) |
| 12 | GET_INFO_CTXG4 | SELECT '@PMREF' FROM DUAL | `CTXG4` = `PMREF` Làm trên JWEB |
| 13 | GET_INFO_PONUM | SELECT O9PMT.GET_PONUM(1201,'@CRBANK', '@CTXG4') FROM DUAL | không dùng |
| 14 | GET_INFO_PONUM2 | SELECT O9PMT.GET_PONUM(1201,'@CRBANK', '@PMREF') FROM DUAL | không dùng |