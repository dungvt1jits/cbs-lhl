# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `PMT_KITT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Product code | product_code | `Yes` | String |  |  |  | `cncat` |
| 2 | Reference number | reference_number | No | String |  |  |  | `pmref` |
| 3 | Ordering institution | ordering_institution | `Yes` | String |  |  |  | `crbank2` |
| 4 | Nostro bank | nostro_bank | `Yes` | String |  |  |  | `crbank` |
| 5 | Bank address | bank_address | No | String |  |  |  | `addbank` |
| 6 | Bank country | bank_country | `Yes` | String |  |  |  | `cntrbank` |
| 7 | Sender id | sender_id | `Yes` | String |  |  |  | `c_license` |
| 8 | Sender name | sender_name | `Yes` | String |  |  |  | `ccatnm` |
| 9 | Sender address | sender_address | No | String |  |  |  | `cctma` |
| 10 | Sender phone | sender_phone | No | String |  |  |  | `cstel` |
| 11 | Sender type | sender_type | `Yes` | String |  |  |  | `cstype` |
| 12 | Receiver name | receiver_name | `Yes` | String |  |  |  | `crname` |
| 13 | Receiver id | receiver_id | No | String |  |  |  | `crid` |
| 14 | Receiver address | receiver_address | No | String |  |  |  | `craddr` |
| 15 | Receiver phone | receiver_phone | No | String |  |  |  | `crtel` |
| 16 | Receiver type | receiver_type | `Yes` | String |  |  |  | `crtype` |
| 17 | Country | country | `Yes` | String |  |  |  | `ccountry` |
| 18 | Purpose | purpose | `Yes` | String |  |  |  | `cspur` |
| 19 | Sender to receiver information | sender_to_receiver_information | No | String |  |  |  | `ctxg1` |
| 20 | Others information | other_information | No | JSON Object |  |  |  | `minfo` |
|  | Goods type | goods_type | No | String |  |  |  | `cgtype` |
|  | Goods list | goods_list | No | String |  |  |  | `mglst` |
| 21 | Remitting currency | remitting_currency | `Yes` | String | 3 |  |  | `cccr` |
| 22 | Remitting amount | remitting_amount | `Yes` | `Number` |  |  |số có hai số thập phân | `psamt` |
| 23 | Receive by | receive_by | `Yes` | String |  |  |  | `recby` |
| 24 | Receive account number | receive_account_number | No | String |  |  |  | `racno` |
| 25 | Receive currency | receive_currency | `Yes` | String | 3 |  |  | `rccr` |
| 26 | Cross rate | cross_rate | `Yes` | `Number` |  |  | số có 9 số thập phân | `ccrrate` |
| 27 | Reverse rate | reverse_rate | `Yes` | `Number` |  |  | số có 9 số thập phân | `swrate` |
| 28 | Display rate | display_rate | `Yes` | String |  |  |  | `displayrate` |
| 29 | Receive amount | receive_amount | `Yes` | `Number` |  |  | số có hai số thập phân | `pramt` |
| 30 | Nostro account | nostro_account | `Yes` | String |  |  |  | `cnostro` |
| 31 | PO type | po_type | No | String |  |  | trường ẩn | `potype` |
| 32 | Amount customer receive | amount_customer_receive | No | `Number` |  |  | trường ẩn | `pcscvt` |
| 33 | Amount convert to BCY | amount_convert_to_bcy | No | `Number` |  |  | trường ẩn | `cbamt` |
| 34 | Accounting debit exchange rate/BCY | accounting_debit_exchange_rate_bcy_ctxexr | No | `Number` |  |  | trường ẩn | `ctxexr` |
| 35 | Description |  | No | String |  |  | trường ẩn | `descs` |
| 36 | Payment by | payment_by | No | String |  |  | trường ẩn | `payby` |
| 37 | PONUM | ponum | No | String |  |  | trường ẩn | `ctxg4` |
| 38 | BANKNAME | bankname | No | String |  |  | trường ẩn | `ctxg3` |
| 39 | Nostro Account | nostro_account | No | String |  |  | trường ẩn | `cnostro` |
| 40 | Receive Branch id | receive_branch_id | No | String |  |  | trường ẩn | `cbrcd` |
| 41 | Share fee rate | share_fee_rate | No | `Number` |  |  | trường ẩn | `sfrate` |
| 42 | Share fee amount | share_fee_amount | No | `Number` |  |  | trường ẩn | `sfamt` |
| 43 | Total Fee for Cash | total_fee_for_cash | No | `Number` |  |  | trường ẩn | `tfcsh` |
| 44 | Rate RCCR | rate_rccr | No | `Number` |  |  | trường ẩn | `orgrate` |
| 45 | Check rate | check_rate | No | String |  |  | trường ẩn | `chkrate` |
| 46 | Message code | message_code | No | String |  |  | trường ẩn | `msgcode` |
| 47 | Total send amount | total_send_amount | No | `Number` |  |  | trường ẩn | `csfee` |
| 48 | Account number | account_number | No | String |  |  | trường ẩn | `pdacc` |
| 49 | Paid amount | paid_amount | No | `Number` |  |  | trường ẩn | `camt1` |
| 50 | Inclusive | inclusive | No | String |  |  | trường ẩn | `cinc` |
| 51 | TEST-Key | test_key | No | String |  |  | trường ẩn | `ctxg2` |
| 52 | Commission | commission | No | `Number` |  |  | trường ẩn | `cfamt` |
| 53 | Fee | fee | No | `Number` |  |  | trường ẩn | `cvat` |
| 54 | Accounting debit exchange rate/BCY | accounting_debit_exchange_rate_bcy_pgexr | No | String |  |  | trường ẩn | `pgexr` |
| 55 | Transaction date |  | `Yes` | `Date` |  |  | Working date |  |  |
| 56 | User id |  | `Yes` | String |  |  | User login |  | 


### Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Product code |  | String |  |  |  |
| 2 | Product code | product_code | String |  |  |  |
| 3 | Reference number | reference_number | String |  |  |  |
| 4 | Ordering institution | ordering_institution | String |  |  |  |
| 5 | Nostro bank | nostro_bank | String |  |  |  |
| 6 | Bank address | bank_address | String |  |  |  |
| 7 | Bank country | bank_country | String |  |  |  |
| 8 | Sender id | sender_id | String |  |  |  |
| 9 | Sender name | sender_name | String |  |  |  |
| 10 | Sender address | sender_address | String |  |  |  |
| 11 | Sender phone | sender_phone | String |  |  |  |
| 12 | Sender type | sender_type | String |  |  |  |
| 13 | Receiver name | receiver_name | String |  |  |  |
| 14 | Receiver id | receiver_id | String |  |  |  |
| 15 | Receiver address | receiver_address | String |  |  |  |
| 16 | Receiver phone | receiver_phone | String |  |  |  |
| 17 | Receiver type | receiver_type | String |  |  |  |
| 18 | Country | country | String |  |  |  |
| 19 | Purpose | purpose | String |  |  |  |
| 20 | Sender to receiver information | sender_to_receiver_information | String |  |  |  |
| 21 | Others information | other_information | JSON Object |  |  |  |
|  | Goods type | goods_type | String |  |  |  |
|  | Goods list | goods_list | String |  |  |  |
| 22 | Remitting currency | remitting_currency | String | 3 |  |  |
| 23 | Remitting amount | remitting_amount | `Number` |  |  |  |
| 24 | Receive by | receive_by | String |  |  |  |
| 25 | Receive account number | receive_account_number | String |  |  |  |
| 26 | Receive currency | receive_currency | String | 3 |  |  |
| 27 | Cross rate | cross_rate | `Number` |  |  |  |
| 28 | Reverse rate | reverse_rate | `Number` |  |  |  |
| 29 | Display rate | display_rate | String |  |  |  |
| 30 | Receive amount | receive_amount | `Number` |  |  |  |
| 31 | Description |  | String |  |  |  |
| 32 | Nostro account | nostro_account | String |  |  |  |
| 33 | Amount customer receive | amount_customer_receive | `Number` |  |  |  |
| 34 | Amount convert to BCY | amount_convert_to_bcy | `Number` |  |  |  |
| 35 | Accounting debit exchange rate/BCY | accounting_debit_exchange_rate_bcy_ctxexr | `Number` |  |  |  |
| 36 | Description |  | String |  |  |  |
| 37 | Payment by | payment_by | String |  |  |  |
| 38 | PONUM | ponum | String |  |  |  |
| 39 | BANKNAME | bankname | String |  |  |  |
| 40 | Nostro Account | nostro_account | String |  |  |  |
| 41 | Receive Branch id | receive_branch_id | String |  |  |  |
| 42 | Share fee rate | share_fee_rate | `Number` |  |  |  |
| 43 | Share fee amount | share_fee_amount | `Number` |  |  |  |
| 44 | Total Fee for Cash | total_fee_for_cash | `Number` |  |  |  |
| 45 | Rate RCCR | rate_rccr | `Number` |  |  |  |
| 46 | Check rate | check_rate | String |  |  |  |
| 47 | Message code | message_code | String |  |  |  |
| 48 | Total send amount | total_send_amount | `Number` |  |  |  |
| 49 | Account number | account_number | String |  |  |  |
| 50 | Paid amount | paid_amount | `Number` |  |  |  |
| 51 | Inclusive | inclusive | String |  |  |  |
| 52 | TEST-Key | test_key | String |  |  |  |
| 53 | Commission | commission | `Number` |  |  |  |
| 54 | Fee | fee | `Number` |  |  |  |
| 55 | Accounting debit exchange rate/BCY | accounting_debit_exchange_rate_bcy_pgexr | String |  |  |  |
| 56 | PO type | po_type | String |  |  |  |
| 57 | Transaction date |  | `Date` |  |  |  |
| 58 | User id |  | String |  |  |  |
| 59 | Transaction status |  | String |  |  |  |

## 1.2 Transaction flow
### 1.2.1 Message for inward domestic
**Conditions:**
- "Catalogue" is Inward Domestic Transfer.
- Remitting amount > 0 and Receive amount > 0.00
- "Nostro bank" must be register in "List/Correspondent Bank". With type "Domestic"
- "Nostro account": is GL account, exists in "Bank account definition". And allow debit side
    - "Nostro account" is defined at "List/Correspondent Bank" and allow entering by manual.

**Flow of events:**
- Transaction complete:
    - Create message payment into queue at center and wait approval.
        - Message direction: Inward
        - Message type: Domestic
        - Message status: New
        - Branch Approve status: New
- When approve message:
    - Payment center approve status: Approved

### 1.2.2 Message for inward international
**Conditions:**
- "Catalogue" is Inward International transaction
- Remitting amount > 0.00 and Receive amount > 0.00
- "Nostro bank" must be register in correspondent bank. With type "Swift"
- "Nostro account": is GL account, exists in "Bank account definition". And allow debit side
"Nostro account" is defined at "List/Correspondent Bank" and allow entering by manual.

**Flow of events:**
- Transaction complete:
    - Create message payment into queue at center and wait approval.
        - Message direction: Inward
        - Message type: Swift
        - Message status: New
        - Branch Approve status: New
- When approve message:
    - Payment center approve status: Approved

**Database:**

**Voucher:**
- `A1`, `A18`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CRBANK | PMT_KIIT_GET_INFO_AGENT_BANK | [PMT_KIIT_GET_INFO_AGENT_BANK](Specification/Common/16 Payment Rulefuncs) |
| 2 | GET_INFO_NOSTRO | GET_INFO_CIBNK_AND_NOSTRO_GL | id = nostro_bank, [GET_INFO_CIBNK_AND_NOSTRO_GL](Specification/Common/16 Payment Rulefuncs) |
| 3 | GET_INFO_CCRRATE | PMT_GET_INFO_CCRRATE | [PMT_GET_INFO_CCRRATE](Specification/Common/16 Payment Rulefuncs) |
| 4 | GET_INFO_SAME_CCCR | SELECT '@CCCR','CSH',null FROM DUAL | `RCCR` = `CCCR`, `RECBY` = `CSH` Làm trên JWEB |
| 5 | GET_INFO_PGEXR | FX_RULEFUNC_GET_INFO_PCSEXR | [FX_RULEFUNC_GET_INFO_PCSEXR](Specification/Common/20 FX Rulefuncs) |
| 6 | GET_INFO_ACT_RCCR | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 7 | GET_INFO_RECBY_CSH | SELECT '@CCCR',null,null,null,null,null FROM DUAL | `RCCR` = `CCCR` Làm trên JWEB |
| 8 | GET_INFO_RCUSTOMER | PMT_GET_INFO_DPT_CCCR | [PMT_GET_INFO_DPT_CCCR](Specification/Common/16 Payment Rulefuncs) |
| 9 | LKP_DATA_CNCAT | PMT_KITT_LOOKUP_PAYMENT_CATALOG | [PMT_KITT_LOOKUP_PAYMENT_CATALOG](Specification/Common/16 Payment Rulefuncs) |
| 10 | LKP_DATA_CRBANK | PMT_KITT_LOOKUP_AGENTBANK | [PMT_KITT_LOOKUP_AGENTBANK](Specification/Common/16 Payment Rulefuncs) |
| 11 | GET_INFO_ACT_RACNO | PMT_GET_INFO_ACT_SACNO | [PMT_GET_INFO_ACT_SACNO](Specification/Common/16 Payment Rulefuncs) |
| 12 | LKP_DATA_ACT_RACNO | PMT_LKP_DATA_SACNO | [PMT_LKP_DATA_SACNO](Specification/Common/16 Payment Rulefuncs) |