# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/QueueForOutward​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_SEARCH_SP_QUEUE_OUTWAR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `​/api​/QueueForOutward​/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Message code | message_code | String |  |  |
| 2 | Ref no | reference_number| String |  | d_pmttran |
| 3 | Tran reference | transaction_ref_id | String |  |  |
| 4 | Tran date | transaction_date_pmt | `Date time` |  |  |
| 5 | Message direction | message_direction | String |  |  |
| 6 | Message type | payment_type | String |  |  |
| 7 | Message status | message_status | String |  |  |
| 8 | Currency | debit_currency | String | 3 | d_pmttran |
| 9 | Amount | amount | `Number` |  | số có hai số thập phân |
| 10 | Receive bank | receiver_bank | String |  |  |
| 11 | Send bank | send_bank | String |  |  |
| 12 | Receive branch | receiving_branch | String |  |  |
| 13 | Send branch | sending_branch | String |  |  |
| 14 | Share fee amount | fee_amount | `Number` |  | số có hai số thập phân |
| 15 | Share fee amount cleared | fee_paid | `Number` |  | số có hai số thập phân |
| 16 | Location | location | String |  |  |
| 17 | Branch approve status | approve_status_branch | String |  |  |
| 18 | Branch process status | process_status_branch | String |  |  |
| 19 | Payment center approve status | apr_status_pmt_center | String |  |  |
| 20 | Payment center process status | process_status_pmt_center | String |  |  |
| 21 | Receive institution bank | ordinstitution | String |  | d_pmttran |
| 22 | API status | api_status | String |  |  |
| 23 | API last update | api_last_update_status | String |  |  |
| 24 | Message outward id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/QueueForOutward​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_SEARCH_ADV_QUEUE_OUTWAR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Message code | message_code | No | String |  |  |  |
| 2 | Ref no | reference_number| No | String |  |  |d_pmttran  |
| 3 | Tran reference | transaction_ref_id | No | String |  |  |  |
| 4 | Tran date | transaction_date_pmt | No | `Date time` |  |  |  |
| 5 | Message direction | message_direction | No | String |  |  |  |
| 6 | Message type | payment_type | No | String |  |  |  |
| 7 | Message status | message_status | No | String |  |  |  |
| 8 | Currency | debit_currency | No | String | 3 |  | d_pmttran |
| 9 | Amount | amount | No | `Number` |  |  | số có hai số thập phân |
| 10 | Receive bank | receiver_bank | No | String |  |  |  |
| 11 | Send bank | send_bank | No | String |  |  |  |
| 12 | Receive branch | receiving_branch | No | String |  |  |  |
| 13 | Send branch | sending_branch | No | String |  |  |  |
| 14 | Location | location | No | String |  |  |  |
| 15 | Branch approve status | approve_status_branch | No | String |  |  |  |
| 16 | Branch process status | process_status_branch | No | String |  |  |  |
| 17 | Payment center approve status | apr_status_pmt_center | No | String |  |  |  |
| 18 | Payment center process status | process_status_pmt_center | No | String |  |  |  |
| 19 | Receive institution bank | ordinstitution | No | String |  |  | d_pmttran |
| 20 | API status | api_status | No | String |  |  |  |
| 21 | Page index | page_index | No | `Number` |  |  |  |
| 22 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Message code | message_code | String |  |  |
| 2 | Ref no | reference_number| String |  | d_pmttran |
| 3 | Tran reference | transaction_ref_id | String |  |  |
| 4 | Tran date | transaction_date_pmt | `Date time` |  |  |
| 5 | Message direction | message_direction | String |  |  |
| 6 | Message type | payment_type | String |  |  |
| 7 | Message status | message_status | String |  |  |
| 8 | Currency | debit_currency | String | 3 | d_pmttran |
| 9 | Amount | amount | `Number` |  | số có hai số thập phân |
| 10 | Receive bank | receiver_bank | String |  |  |
| 11 | Send bank | send_bank | String |  |  |
| 12 | Receive branch | receiving_branch | String |  |  |
| 13 | Send branch | sending_branch | String |  |  |
| 14 | Share fee amount | fee_amount | `Number` |  | số có hai số thập phân |
| 15 | Share fee amount cleared | fee_paid | `Number` |  | số có hai số thập phân |
| 16 | Location | location | String |  |  |
| 17 | Branch approve status | approve_status_branch | String |  |  |
| 18 | Branch process status | process_status_branch | String |  |  |
| 19 | Payment center approve status | apr_status_pmt_center | String |  |  |
| 20 | Payment center process status | process_status_pmt_center | String |  |  |
| 21 | Receive institution bank | ordinstitution | String |  | d_pmttran |
| 22 | API status | api_status | String |  |  |
| 23 | API last update | api_last_update_status | String |  |  |
| 24 | Message outward id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
- Tham khảo thông tin chi tiết tại đây:<br>
- [PMT_OIT: Outward Internal Bank](Specification/Payment/08 PMT_OIT Outward Internal Bank)<br>
- [PMT_ODT: Outward Domestic Bank](Specification/Payment/09 PMT_ODT Outward Domestic Bank)<br>
- [PMT_OITT: Outward International Bank](Specification/Payment/10 PMT_OITT Outward International Bank)<br>

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/QueueForOutward​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `PMT_VIEW_QUEUE_OUTWAR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Message outward id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Message code | message_code | String |  |  |
| 2 | Statement date | transaction_date_pmt | `Date time` |  |  |
| 3 | Transaction references number | transaction_ref_id | String |  |  |
| 4 | Message direction | message_direction | String |  |  |
| 5 | Message type | payment_type | String |  |  |
| 6 | Sender name | sender_name | String |  |  |
| 7 | Receiver name | receiver_name | String |  |  |
| 8 | Amount | amount | `Number` |  | số có hai số thập phân |
| 9 | Paid amount | paid_amount | `Number` |  | số có hai số thập phân |
| 10 | Share fee amount | fee_amount | `Number` |  | số có hai số thập phân |
| 11 | Share fee paid | fee_paid | `Number` |  | số có hai số thập phân |
| 12 | Message status | message_status | String |  |  |
| 13 | Send bank code | send_bank | String |  |  |
| 14 | Receiver bank code | receiver_bank | String |  |  |
| 15 | Print status | print_status | String |  |  |
| 16 | User create | user_created | `Number` |  |  |  |
| 17 | Who approve | user_approved | `Number` |  |  |  |
| 18 | Location | location | String |  |  |
| 19 | Payment center approve status | apr_status_pmt_center | String |  |  |
| 20 | Payment center process status | process_status_pmt_center | String |  |  |
| 21 | Branch approve status | approve_status_branch | String |  |  |
| 22 | Branch process status | process_status_branch | String |  |  |
| 23 | Message outward id | id | `Number` |  |  |
|  | Group payment intruction | group_pmt_inst | String |  |  |
|  | Sending branch code | sending_branch | String |  |  |
|  | Receiver branch code | receiving_branch | String |  |  |
|  | Payment by | payment_by | String |  |  |
|  | Approve user payment center | approve_user_pmt_center | `Number` |  |  |
|  | Process user payment center | process_user_pmt_center | `Number` |  |  |
|  | Reject user payment center | reject_user_pmt_center | `Number` |  |  |
|  | Approve_user_branch | approve_user_branch | `Number` |  |  |
|  | Process user branch | process_user_branch | `Number` |  |  |
|  | Reject user branch | reject_user_branch | `Number` |  |  |
|  | API status | api_status | String |  |  |
|  | API error | api_error | String |  |  |
|  | API last update status | api_last_update_status | `Date time` |  |  |
|  | API expected effective date | api_expected_effective_date | `Date time` |  |  |
|  | Detail charges | detail_charges | String |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Approve
## 5.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 |  |  |  |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |

# 6. Reject
## 6.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 |  |  |  |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |