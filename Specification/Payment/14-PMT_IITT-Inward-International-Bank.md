PMT_IITT: Inward International Bank
# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `PMT_IITT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Product Code | product_code | No | String |  |  |  | `cncat` |
| 2 | Message code | message_code | `Yes` | String |  |  |  | `msgcode` |
| 3 | Ordering Institution | ordering_institution | `Yes` | String |  |  |  | `crbank2` |
| 4 | Nostro Bank | nostro_bank | `Yes` | String |  |  |  | `crbank` |
| 5 | Reference number | reference_number | No | String |  |  |  | `pmref` |
| 6 | Transaction number | transaction_number | No | String |  |  |  | `pmtnum` |
| 7 | Sender id | sender_id | `Yes` | String |  |  |  | `c_license` |
| 8 | Sender address | sender_address | No | String |  |  |  | `cctma` |
| 9 | Sender name | sender_name | `Yes` | String |  |  |  | `ccatnm` |
| 10 | Sender phone | sender_phone | No | String |  |  |  | `cstel` |
| 11 | Sender type | sender_type | No | String |  |  |  | `cstype` |
| 12 | Receiver name | receiver_name | `Yes` | String |  |  |  | `crname` |
| 13 | Receiver id | receiver_id | No | String |  |  |  | `crid` |
| 14 | Receiver address | receiver_address | No | String |  |  |  | `craddr` |
| 15 | Receiver phone | receiver_phone | No | String |  |  |  | `crtel` |
| 16 | Receiver type | receiver_type | No | String |  |  |  | `crtype` |
| 17 | Country | country | `Yes` | String |  |  |  | `ccountry` |
| 18 | Purpose | purpose | No | String |  |  |  | `cspur` |
| 19 | Sender to receiver information | sender_to_receiver_information | No | String |  |  |  | `ctxg1` |
| 20 | Other information | other_information_minfo | No | String |  |  |  | `minfo` |
|  | Goods type | goods_type | No | String |  |  |  | `cgtype` |
|  | Goods list | goods_list | No | String |  |  |  | `mglst` |
| 21 | Remitting currency | remitting_currency | `Yes` | String | 3 |  |  | `cccr` |
| 22 | Remitting amount | remitting_amount | `Yes` | `Number` |  |  |  | `psamt` |
| 23 | Remitting exchange rate/BCY | remitting_exchange_rate_bcy | No | `Number` |  |  |  | `pgexr` |
| 24 | Remitting amount in BCY | remitting_amount_in_bcy | No | `Number` |  |  |  | `cbamt` |
| 25 | Commission in remitting currency | commission_in_remitting_currency | No | `Number` |  |  |  | `camt` |
| 26 | Receive by | receive_by | `Yes` | String |  |  |  | `recby` |
| 27 | Account number | account_number_pdacc | No | String |  |  |  | `pdacc` |
| 28 | Receive currency | receive_currency | No | String | 3 |  |  | `rccr` |
| 29 | Original cross rate | original_cross_rate | No | `Number` |  |  |  | `orgrate` |
| 30 | Cross rate | cross_rate | `Yes` | `Number` |  |  |  | `ccrrate` |
| 31 | Reverse rate | reverse_rate | `Yes` | `Number` |  |  |  | `swrate` |
| 32 | Display rate | display_rate | `Yes` | String |  |  |  | `displayrate` |
| 33 | Amount before fees | amount_before_fees | `Yes` | `Number` |  |  |  | `pramt` |
| 34 | Commission | commission | No | `Number` |  |  |  | `cfamt` |
| 35 | Amount customer receives | amount_customer_receives | `Yes` | `Number` |  |  |  | `pcscvt` |
| 36 | Receive exchange rate/BCY | receive_exchange_rate_bcy | `Yes` | `Number` |  |  |  | `ctxexr` |
| 37 | Description | description | No | String |  |  |  | `descs` |
| 38 | Detail of charges | detail_of_charges | `Yes` | String |  |  |  | `dtlchr` |
| 39 | Other information | other_informations | No | String |  |  | trường ẩn | `ctxg5` |
| 40 | TEST-Key | test_key | No | String |  |  | trường ẩn | `ctxg2` |
| 41 | Inclusive | inclusive | No | String |  |  | trường ẩn | `cinc` |
| 42 | Payment by | payment_by | No | String |  |  | trường ẩn | `payby` |
| 43 | PONUM | ponum | No | String |  |  | trường ẩn | `ctxg4` |
| 44 | BANKNAME | bankname | No | String |  |  | trường ẩn | `ctxg3` |
| 45 | Nostro Account | nostro_account | No | String |  |  | trường ẩn | `cnostro` |
| 46 | Receive Branch id | receive_branch_id | No | String |  |  | trường ẩn | `cbrcd` |
| 47 | Share fee rate | share_fee_rate | No | `Number` |  |  | trường ẩn | `sfrate` |
| 48 | Share fee amount | share_fee_amount | No | `Number` |  |  | trường ẩn | `sfamt` |
| 49 | Total Fee for Cash | total_fee_for_cash | No | `Number` |  |  | trường ẩn | `tfcsh` |
| 50 | Total send amount | total_send_amount | No | `Number` |  |  | trường ẩn | `csfee` |
| 51 | Check rate | check_rate | No | String |  |  | trường ẩn | `chkrate` |
| 52 | Beneficiary bank name | beneficiary_bank_name | No | String |  |  | trường ẩn | `ctxg9` |
| 53 | nostro bank branchcd | nostro_bank_branchcd | No | String |  |  | trường ẩn | `cbrcdc` |
| 54 | Commission in based | commission_in_based | No | `Number` |  |  | trường ẩn | `cvat` |
| 55 | Nostro Bank name  | nostro_bank_name | No | String |  |  | trường ẩn | `ctxg8` |
| 56 | Commission in base currency | commission_in_base_currency | No | `Number` |  |  | trường ẩn | `acvat` |
| 57 | Paid amount | paid_amount | No | `Number` |  |  | trường ẩn | `camt1` |
| 58 | Account number | account_number_racno | No | String |  |  | trường ẩn | `racno` |
| 59 | Fee data |  | No | JSON Object |  |  |  |
|  | IFC code |  | No | `Number` |  |  |  |
|  | Value type |  | No | String |  |  |  |
|  | Value |  | No | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | No | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | No | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | No | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | No | String | 3 |  |  |
| 60 | Transaction date |  | `Yes` | `Date` |  |  | Working date |  |  |
| 61 | User id |  | `Yes` | String |  |  | User login |  | 

### Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Product Code | product_code | String |  |  |  |
| 3 | Message code | message_code | String |  |  |  |
| 4 | Ordering Institution | ordering_institution | String |  |  |  |
| 5 | Nostro Bank | nostro_bank | String |  |  |  |
| 6 | Reference number | reference_number | String |  |  |  |
| 7 | Transaction number | transaction_number | String |  |  |  |
| 8 | Sender id | sender_id | String |  |  |  |
| 9 | Sender address | sender_address | String |  |  |  |
| 10 | Sender name | sender_name | String |  |  |  |
| 11 | Sender phone | sender_phone | String |  |  |  |
| 12 | Sender type | sender_type | String |  |  |  |
| 13 | Receiver name | receiver_name | String |  |  |  |
| 14 | Receiver id | receiver_id | String |  |  |  |
| 15 | Receiver address | receiver_address | String |  |  |  |
| 16 | Receiver phone | receiver_phone | String |  |  |  |
| 17 | Receiver type | receiver_type | String |  |  |  |
| 18 | Country | country | String |  |  |  |
| 19 | Purpose | purpose | String |  |  |  |
| 20 | Sender to receiver information | sender_to_receiver_information | String |  |  |  |
| 21 | Other information | other_information_minfo | String |  |  |  |
| 22 | Remitting currency | remitting_currency | String | 3 |  |  |
| 23 | Remitting amount | remitting_amount | `Number` |  |  |  |
| 24 | Remitting exchange rate/BCY | remitting_exchange_rate_bcy | `Number` |  |  |  |
| 25 | Remitting amount in BCY | remitting_amount_in_bcy | `Number` |  |  |  |
| 26 | Commission in remitting currency | commission_in_remitting_currency | `Number` |  |  |  |
| 27 | Receive by | receive_by | String |  |  |  |
| 28 | Account number | account_number_pdacc | String |  |  |  |
| 29 | Receive currency | receive_currency | String | 3 |  |  |
| 30 | Original cross rate | original_cross_rate | `Number` |  |  |  |
| 31 | Cross rate | cross_rate | `Number` |  |  |  |
| 32 | Reverse rate | reverse_rate | `Number` |  |  |  |
| 33 | Display rate | display_rate | String |  |  |  |
| 34 | Amount before fees | amount_before_fees | `Number` |  |  |  |
| 35 | Commission | commission | `Number` |  |  |  |
| 36 | Amount customer receives | amount_customer_receives | `Number` |  |  |  |
| 37 | Receive exchange rate/BCY | receive_exchange_rate_bcy | `Number` |  |  |  |
| 38 | Description | description | String |  |  |  |
| 39 | Detail of charges | detail_of_charges | String |  |  |  |
| 40 | Other information | other_informations | String |  |  |  |
| 41 | TEST-Key | test_key | String |  |  |  |
| 42 | Inclusive | inclusive | String |  |  |  |
| 43 | Payment by | payment_by | String |  |  |  |
| 44 | PONUM | ponum | String |  |  |  |
| 45 | BANKNAME | bankname | String |  |  |  |
| 46 | Nostro Account | nostro_account | String |  |  |  |
| 47 | Receive Branch id | receive_branch_id | String |  |  |  |
| 48 | Share fee rate | share_fee_rate | `Number` |  |  |  |
| 49 | Share fee amount | share_fee_amount | `Number` |  |  |  |
| 50 | Total Fee for Cash | total_fee_for_cash | `Number` |  |  |  |
| 51 | Total send amount | total_send_amount | `Number` |  |  |  |
| 52 | Check rate | check_rate | String |  |  |  |
| 53 | Beneficiary bank name | beneficiary_bank_name | String |  |  |  |
| 54 | nostro bank branchcd | nostro_bank_branchcd | String |  |  |  |
| 55 | Commission in based | commission_in_based | `Number` |  |  |  |
| 56 | Nostro Bank name  | nostro_bank_name | String |  |  |  |
| 57 | Commission in base currency | commission_in_base_currency | `Number` |  |  |  |
| 58 | Paid amount | paid_amount | `Number` |  |  |  |
| 59 | Account number | account_number_racno | String |  |  |  |
| 60 | Fee data |  | JSON Object |  |  |  |
|  | IFC code |  | `Number` |  |  |  |
|  | Value type |  | String |  |  |  |
|  | Value |  | `Number` |  |  | số có 5 số thập phân |
|  | Fee |  | `Number` |  |  | số có hai số thập phân |
|  | Floor |  | `Number` |  |  | số có hai số thập phân |
|  | Ceiling |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |
| 61 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Catalogue" is Inward International Transfer
- Message exists in queue with property:
    - "Payment center approve status": Approved
    - Message type is "international".
- Receive amount > 0.00

**Flow of events:**
- Support method to receive includes: Cash/ Deposit/ Accounting
    - If method is Cash:
        - User must have enough cash to payment.
    - If method is Deposit:
        - Deposit account exists with status <> "Close". Type is current or saving
        - Auto fill information of receiver. It gets from customer profile.
    - If method is GL:
        - GL exists in account chart and same branch with user login.
        - Allow credit side.
- Allow remitting currency and receive currency are different:
    - Debit rate get from table "foreign exchange" = Transferring buy (TB)
    - Credit rate get from table "foreign exchange" = Transferring sell (TA)
    - Cross rate = Debit rate / Credit rate.
        - Number rounding is 9 decimals
- If Cross rate < 1, system is more flexible about show exchange rate on voucher. It can be:
    - Reverse rate = 1 / Cross rate.
        - Number rounding is 9 decimals
- If user do any action that impact to change Cross rate. System will suggest approve.
- Allow collect fee on transaction (if any).
- Show total value of receive include fee amount. 
- Support to print voucher for internal and receive transaction.
- Transaction complete:
    - Message status: Paid
    - Paid amount increase.
    - If Receive is method "Cash"
        - Cash of user will decrease
    - If Receive is method "Deposit"
        - Balance of deposit account will increase
    - If Receive is method "Accounting"
        - Credit GL

**Database:**


**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | GL (Nostro account) | Receive amount | Receive currency |
| 1 | C | CASH/DEPOSIT/GL | Receive amount | Receive currency |

- Note: "Nostro account" gets from inward swift message.

- Case collect fee (fee amount > 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 2 | D | CASH/DEPOSIT/GL | Fee amount | Receive currency |
| 2 | C | IFCC (income) | Fee amount | Receive currency |

**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_INFO_PMTCAT | GET_INFO_PMTCAT | [GET_INFO_PMTCAT](Specification/Common/16 Payment Rulefuncs) |
| 2 | GET_INFO_MSGCODE | PMT_GET_INFO_MESSAGE_INWARD_INTERNATIONAL | [PMT_GET_INFO_MESSAGE_INWARD_INTERNATIONAL](Specification/Common/16 Payment Rulefuncs) |
| 3 | GET_INFO_RMREF | PMT_GET_INFO_RMREF | [PMT_GET_INFO_RMREF](Specification/Common/16 Payment Rulefuncs) |
| 4 | GET_INFO_CRBANK2 | SELECT '@CRBANK2' CTXG9 FROM DUAL  | `CRBANK2` = `CTXG9` Làm trên JWEB |
| 5 | GET_INFO_CSH_RCUSTOMER | PMT_GET_INFO_MESSAGE_INWARD_INTERNATIONAL  | [PMT_GET_INFO_MESSAGE_INWARD_INTERNATIONAL](Specification/Common/16 Payment Rulefuncs) |
| 6 | GET_INFO_DPT_RCUSTOMER | PMT_GET_INFO_DPT_CCCR | [PMT_GET_INFO_DPT_CCCR](Specification/Common/16 Payment Rulefuncs) |
| 7 | GET_INFO_RACNO | PMT_GET_INFO_MESSAGE_INWARD_INTERNATIONAL | [PMT_GET_INFO_MESSAGE_INWARD_INTERNATIONAL](Specification/Common/16 Payment Rulefuncs) |
| 8 | GET_INFO_CRBANK | PMT_IITT_GET_INFO_AGENT_BANK | [PMT_IITT_GET_INFO_AGENT_BANK](Specification/Common/16 Payment Rulefuncs) |
| 9 | GET_INFO_ON_RCCR | PMT_GET_INFO_ON_RECBY | [PMT_GET_INFO_ON_RECBY](Specification/Common/16 Payment Rulefuncs) |
| 10 | GET_INFO_ON_RECBY | PMT_GET_INFO_ON_RECBY | [PMT_GET_INFO_ON_RECBY](Specification/Common/16 Payment Rulefuncs) |
| 11 | GET_INFO_RECBY_CSH | SELECT '@CCCR',null,null,null,null,null FROM DUAL | `RCCR` = `CCCR` Làm trên JWEB |
| 12 | GET_INFO_ACT_RCCR | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 13 | GET_INFO_ON_CCRRATE | SELECT ROUND (1 / '@CCRRATE', 9) as SWRATE FROM DUAL | làm trên JWEB | CCRRATE | SWRATE = 1/ CCRRATE (làm tròn 9) |
| 14 | GET_INFO_ON_SWRATE | SELECT ROUND (1 / '@SWRATE', 9) AS swrate FROM DUAL | làm trên JWEB | SWRATE | CCRRATE = 1/ SWRATE (làm tròn 9) |
| 15 | GET_INFO_ON_PRAMT | select ROUND('@PRAMT'/'@PSAMT',9) CCRRATE, ROUND('@PSAMT'/'@PRAMT',9) SWRATE, '@PRAMT'-'@CFAMT' from dual | làm trên JWEB [GET_INFO_ON_PRAMT](Specification/Common/16 Payment Rulefuncs) | | |
| 16 | LKP_DATA_PMTCAT | PMT_LOOKUP_PAYMENT_CATALOG | [PMT_LOOKUP_PAYMENT_CATALOG](Specification/Common/16 Payment Rulefuncs) |
| 17 | LKP_DATA_ACT_RACNO | PMT_LKP_DATA_SACNO | [PMT_LKP_DATA_SACNO](Specification/Common/16 Payment Rulefuncs) |
| 18 | GET_INFO_ACT_RACNO | PMT_GET_INFO_ACT_SACNO | [PMT_GET_INFO_ACT_SACNO](Specification/Common/16 Payment Rulefuncs) |
| 19 | LKP_DATA_MSGCODE | PMT_LOOKUP_MESSAGE_INWARD_INTERNATIONAL | [PMT_LOOKUP_MESSAGE_INWARD_INTERNATIONAL](Specification/Common/16 Payment Rulefuncs) |
| 20 | TRAN_FEE_PMT_IITT/GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 21 | TRAN_FEE_PMT_IITT/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) |
| 22 | crossrate | FX_GET_CROSS_RATE | [FX_GET_CROSS_RATE](Specification/Common/20 FX Rulefuncs) |  |  |
| 23 | BaseRate | FX_RULEFUNC_GET_FX_BCYRATE | [FX_RULEFUNC_GET_FX_BCYRATE](Specification/Common/20 FX Rulefuncs) | rate_type='TA', branch_id_fx= branch đang làm giao dịch |  |