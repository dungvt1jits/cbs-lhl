# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CIQ`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | Serial number | serial_number | `Yes` | String | 20 | | | STK_NO |
| 3 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | 15 | |
| 3 | Serial number | serial_number | String | | |
| 4 | Transaction history data | dataset | Array Object | | |
|  | Serial No. | serial_no | String | | |
|  | Status | status| String | | |
| 5 | Transaction status | status | String | | |
| 6 | Transaction date | transaction_date | `Date time` |  |  |
| 7 | User id | user_id | String |  |  |
| 8 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Account number is existing in the system.
-	"Serial number" must belong a cheque book. It was defined at: "Stock Inventory".
-	"Serial number" issued to account number.

**Flow of events:**
-	Get a data sheet with details: "Serial No", "Stock leaves status".

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_ACNAME | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |