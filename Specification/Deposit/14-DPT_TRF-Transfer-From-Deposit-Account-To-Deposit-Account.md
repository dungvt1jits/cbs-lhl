# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_TRF`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Debit account | debit_account | `Yes` | String | 25 |  |  | PDACC |
| 2 | Balance debit account | balance_debit_account | No | `Number` |  | 0 |  | CAMT2 |
| 3 | Available balance debit account | available_balance_debit_account | No | `Number` |  | 0 |  | CAMT3 |
| 4 | Account linkage | account_linkage | No | String | 25 |  | trường ẩn | PGACC |
| 5 | Amount linkage | amount_linkage | No | `Number` |  | 0 | trường ẩn | CAMT4 |
| 6 | Amount | amount | `Yes` | `Number` |  | > 0 |  | CTXAMT1 |
| 7 | Exchange rate (Debit account/BCY) | t_exchange_rate_debit_account_bcy | No | `Number` |  | 0 | trường ẩn | CTXEXR |
| 8 | Amount equivalent in BCY | amount_equivalent_in_bcy | No | `Number` |  | 0 | trường ẩn | CAMTCVT |
| 9 | Credit account | credit_account | `Yes` | String | 25 |  |  | PDOACC |
| 10 | Currency of debit account | currency_of_debit_account | No | String | 3 |  | trường ẩn | CCCR |
| 11 | Currency of credit account | currency_of_credit_account | No | String | 3 |  | trường ẩn | CCCR1 |
| 12 | Cross rate | cross_rate | No | `Number` |  | 0 | trường ẩn | CCRRATE |
| 13 | Credit amount | credit_amount | No | `Number` |  | 0 | trường ẩn | PDOCVT |
| 14 | Exchange rate (Credit Ac/BCY) | exchange_rate_credit_ac_bcy | No | `Number` |  | 0 | trường ẩn | PGEXR |
| 15 | Amount to credit/BCY | amount_to_credit_bcy | No | `Number` |  | 0 | trường ẩn | PGCVT |
| 16 | Debit account name | debit_account_name | No | String | 250 |  |  | CACNM |
| 17 | Customer code | customer_code | No | String | 15 |  |  | CCTMCD |
| 18 | Customer address | customer_address | No | String | 250 |  |  | CCTMA |
| 19 | Customer description | customer_description | No | JSON Object |  |  |  | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 20 | Value date | value_date | No | `Date time` |  | Working date | trường ẩn | CVLDT |
| 21 | Total fee/Credit account | total_fee_credit_account | No | `Number` |  | 0 | trường ẩn | PGAMT |
| 22 | Paper number of debit | paper_number_of_debit | No | String | 100 |  | trường ẩn | CREPID |
| 23 | Issue date of debit | issue_date_of_debit | No | `Date time` |  | Working date | trường ẩn | CIDDT |
| 24 | Total amount payable | total_amount_payable | `Yes` | `Number` |  | 0 | trường ẩn | PCSCVT |
| 25 | Issue place of debit | issue_place_of_debit | No | String | 90 |  | trường ẩn | CIDPLACE |
| 26 | Paper number of credit | paper_number_of_credit | No | String | 100 |  | trường ẩn | CREPID2 |
| 27 | Issue date of credit | issue_date_of_credit | No | `Date time` |  | Working date | trường ẩn | CIDDT2 |
| 28 | Total fee | total_fee | No | `Number` |  | 0 | trường ẩn | CFAMT |
| 29 | Issue place of credit | issue_place_of_credit | No | String | 90 |  | trường ẩn | CIDPLACE2 |
| 30 | Credit account name | credit_account_name | No | String | 250 |  | trường ẩn | CACNM2 |
| 31 | Exchange rate (Debit account/BCY) | b_exchange_rate_debit_account_bcy | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 32 | Amount (Debit account/BCY) | amount_debit_account_bcy | No | `Number` |  | 0 | trường ẩn | CCVT |
| 33 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 34 | Description | description | No | String | 250 |  |  | DESCS |
| 35 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |  |
| 3 | Debit account | debit_account | String |  |  |
| 4 | Balance debit account | balance_debit_account | `Number` |  |  |
| 5 | Available balance debit account | available_balance_debit_account | `Number` |  |  |
| 6 | Account linkage | account_linkage | String |  | trường ẩn |
| 7 | Amount linkage | amount_linkage | `Number` |  | trường ẩn |
| 8 | Amount | amount | `Number` |  |  |
| 9 | Exchange rate (Debit account/BCY) | t_exchange_rate_debit_account_bcy | `Number` |  | trường ẩn |
| 10 | Amount equivalent in BCY | amount_equivalent_in_bcy | `Number` |  | trường ẩn |
| 11 | Credit account | credit_account | String |  |  |
| 12 | Currency of debit account | currency_of_debit_account | String |  | trường ẩn |
| 13 | Currency of credit account | currency_of_credit_account | String |  | trường ẩn |
| 14 | Cross rate | cross_rate | `Number` |  | trường ẩn |
| 15 | Credit amount | credit_amount | `Number` |  | trường ẩn |
| 16 | Exchange rate (Credit Ac/BCY) | exchange_rate_credit_ac_bcy | `Number` |  | trường ẩn |
| 17 | Amount to credit/BCY | amount_to_credit_bcy | `Number` |  | trường ẩn |
| 18 | Debit account name | debit_account_name | String |  |  |
| 19 | Customer code | customer_code | String |  |  |
| 20 | Customer address | customer_address | String |  |  |
| 21 | Customer description | customer_description | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
| 22 | Value date | value_date | `Date time` |  | trường ẩn |
| 23 | Total fee/Credit account | total_fee_credit_account | `Number` |  | trường ẩn |
| 24 | Paper number of debit | paper_number_of_debit | String |  | trường ẩn |
| 25 | Issue date of debit | issue_date_of_debit | `Date time` |  | trường ẩn |
| 26 | Total amount payable | total_amount_payable | `Number` |  | trường ẩn |
| 27 | Issue place of debit | issue_place_of_debit | String |  | trường ẩn |
| 28 | Paper number of credit | paper_number_of_credit | String |  | trường ẩn |
| 29 | Issue date of credit | issue_date_of_credit | `Date time` |  | trường ẩn |
| 30 | Total fee | total_fee | `Number` |  | trường ẩn |
| 31 | Issue place of credit | issue_place_of_credit | String |  | trường ẩn |
| 32 | Credit account name | credit_account_name | String |  | trường ẩn |
| 33 | Exchange rate (Debit account/BCY) | b_exchange_rate_debit_account_bcy | `Number` |  | trường ẩn |
| 34 | Amount (Debit account/BCY) | amount_debit_account_bcy | `Number` |  | trường ẩn |
| 35 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 36 | Description | description | String |  |  |
| 56 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |
| 57 | Transaction status | status | String |  |  |
| 58 | Transaction date | transaction_date | `Date time` |  |  |
| 59 | User id | user_id | `Number` |  |  |
| 60 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	"Debit account": exists with status is "Normal".
    + Deposit type: FD 
	  + Multiple Withdrawal Allow = Y/N 
	  + Early withdrawal = Y 
	  + Open date + Minimum Tenor Allow Early Withdrawal <= Working date
	  + Amount = Available balance
	+ Deposit type: Saving 
	  + PassbookOrReceiptStatus = N/ U
-	"Credit account": exists with status is "New"/"Normal"
    + Deposit type: FD 
	  + MultipleDepositAllow = Y (Status: Normal)
	+ Deposit type: Saving 
	  + PassbookOrReceiptStatus = N/ U
-	Accounts must be same currency.
-	"Debit account" has enough amount to transfer (not include amount is keeping).
-   "Debit account" <> "Credit account"

**Flow of events:**
-	If first time deposit. Amount of credit account must be larger than "Initial deposit amount".
-	If setup parameter: allow deposit one-time. System will not allow to continue deposit amount.
-	Change status of credit account if first time deposit:
	+	Account type is current, change status to "Normal".
	+	Account type is saving, change status to "Normal".
	+	Account type is fixed deposit, change status to "Actived" (if allow deposit: only one time).
	+	Account type is fixed deposit, change status to "Normal" (if allow deposit: multi-time).
-	Allow collection fees for this transaction (if any).
-	Credit account will increase amount.
-	Debit account will decrease amount.
-   If transaction has collect fee, system will collect fee from deposit amount
<br>Ex: Deposit amount = 100, Fee = 10 => Deposit account (receive) will receive: 90, system collect fee from deposit account (transfer): 10 


**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT (transfer) | Amount deposit | Currency of account |
| 1 | C | DEPOSIT (receive) | Amount deposit | Currency of account |
| Case collect fee (fee amount > 0) |  |  |  |  |
| 2 | D | DEPOSIT (transfer) | Fee amount | Currency of account |
| 2 | C | IFCC (income) | Fee amount | Currency of account |

**Cập nhật thông tin deposit account cho tài khoản gửi tiền:**
[Tham khảo thông tin chi tiết tại mục 5](Specification/Common/06 Transaction Flow Deposit)<br>

**Cập nhật thông tin deposit account cho tài khoản nhận tiền:**
[Tham khảo thông tin chi tiết tại mục 4](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Deposit type is invalid | Deposit type không hợp lệ |
|  | Account type is invalid | Account type không hợp lệ |
|  | Balance has not enough | Balance của tài khoản gửi không đủ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_ACNAME | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_ACLINK | DPT_GET_INFO_ACLINK | [DPT_GET_INFO_ACLINK](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_SIG_PDOACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 5 | GET_INFO_OTHER_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 6 | TRAN_FEE_DPT_TRF\GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 7 | TRAN_FEE\GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 8 | TRAN_FEE_DPT_TRF\LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>