# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CDP`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | PDACC |
| 2 | Amount deposit | amount_deposit | `Yes` | `Number` |  | > 0 | số có hai số thập phân | PCSAMT |
| 3 | Depositor name | account_name | `Yes` | String | 250 |  |  | CACNM |
| 4 | Depositor code | customer_code | No | String | 15 |  |  | CCTMCD |
| 5 | Depositor address | depositor_address | No | String | 250 |  |  | CCTMA |
| 6 | Depositor description | depositor_description | No | JSON Object | | | | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 7 | Description | description | No | String | 250 |  |  | DESCS |
| 8 | ID issue date | id_issue_date | No | `Date` |  |  | trường ẩn | CIDDT |
| 9 | ID place | id_place | No | String | 90 |  | trường ẩn | CIDPLACE |
| 10 | Identification number | identification_number | No | String | 100 |  | trường ẩn | CREPID |
| 11 | Values date | values_date | No | `Date` |  |  | trường ẩn | CVLDT |
| 12 | Prepaid Interest | prepaid_interest | No | `Number` |  | 0 | trường ẩn | CTINT |
| 13 | Interest tenor unit | interest_tenor_unit | No | String | 1 |  | trường ẩn | CTRN |
| 14 | Inclusive | inclusive | No | String |  |  | trường ẩn | CINC |
| 15 | Exchange rate | exchange_rate | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 16 | Currency of deposit account | currency_deposit | No | String | 3 |  | trường ẩn | CCCR |
| 17 | Cross rate | cross_rate | `Yes` | `Number` |  | 0 | trường ẩn | CCRRATE |
| 18 | Commission | commission | No | `Number` |  | 0 | trường ẩn | CFAMT |
| 19 | Cash exchange rate/BCY | cash_exchange_rate | `Yes` | `Number` |  | 0 | trường ẩn | PCSEXR |
| 20 | Cash currency | cash_currency | `Yes` | String | 3 |  | trường ẩn | PCSCCR |
| 21 | Cash amount/BCY | cash_amount_bcy | No | `Number` |  | 0 | trường ẩn | PCSCVT |
| 22 | Cash amount | cash_amount | `Yes` | `Number` |  | 0 | trường ẩn | CTCVT |
| 23 | Deposit type | deposit_type | No | String | 1 |  | trường ẩn | DPTTYPE |
| 24 | Amount (Debit account/BCY) | amount | No | `Number` |  | 0 | trường ẩn | CCVT |
| 25 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  | 0 | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  | 0 | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  | 0 | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  | 0 | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 26 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String | | |
| 3 | Amount deposit | amount_deposit | `Number` | | |
| 4 | Depositor name | account_name | String | | |
| 5 | Depositor code | customer_code | String | | |
| 6 | Depositor address | depositor_address | String | | |
| 7 | Depositor description | depositor_description | JSON Object | | |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 8 | Description | description | String | | |
| 9 | ID issue date | id_issue_date | `Date` | | |
| 10 | ID place | id_place | String | | |
| 11 | Identification number | identification_number | String | | |
| 12 | Values date | values_date | `Date` | | |
| 13 | Prepaid Interest | prepaid_interest | `Number` | | |
| 14 | Interest tenor unit | interest_tenor_unit | String | | |
| 15 | Inclusive | inclusive | String | | |
| 16 | Exchange rate | exchange_rate | `Number` | | |
| 17 | Currency of deposit account | currency_deposit | String | | |
| 18 | Cross rate | cross_rate | `Number` | | |
| 19 | Commission | commission | `Number` | | |
| 20 | Cash exchange rate /BCY | cash_exchange_rate | `Number` | | |
| 21 | Cash currency | cash_currency | String | | |
| 22 | Cash amount/BCY | cash_amount_bcy | `Number` | | |
| 23 | Cash amount | cash_amount | `Number` | | |
| 24 | Deposit type | deposit_type | String | | |
| 25 | Amount (Debit account/BCY) | amount | `Number` | | |
| 26 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 27 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |
| 28 | Transaction status | status | String | | |
| 29 | Transaction date | transaction_date | `Date time` |  |  |
| 30 | User id | user_id | `Number` |  |  |
| 31 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**

-	"Account number": exists with status is "New"/ "Normal"/ ~~"Dormant"~~.
-	"Amount deposit": must be larger than zero and larger than "Initial deposit amount" (if any)
- Teller does this transaction:
  - User has right to do transaction related to cash.
-	Don't allow deposit cash that different currency with account.

**Flow of events:**
-	If first time deposit. Amount deposit must be larger than "Initial deposit amount".
-	If setup parameter: allow deposit one-time. System will not allow to continue deposit amount.
-	Change status of account if first time deposit:
	+	Account type is current, change status to "Normal".
	+	Account type is saving, change status to "Normal".
	+	Account type is fixed deposit, change status to "Actived" (if allow deposit: only one time).
	+	Account type is fixed deposit, change status to "Normal" (if allow deposit: multi-time).
-	Allow collection fees for this transaction (if any).
-	Increase cash at counter.
-	Balance of account will increase.
-   If transaction has collect fee, system will collect fee from deposit amount
<br>Ex: Deposit amount = 100, Fee = 10 => Deposit account will receive: 100, system collect fee from cash: 10 

**Posting:**
-	Case: Customer deposits money at branch open account.

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASH | Amount deposit | Currency of account |
| 1 | C | DEPOSIT | Amount deposit | Currency of account |
| Case collect fee (fee amount > 0) |  |  |  |  |
| 2 | D | CASH | Fee amount | Currency of account |
| 2 | C | IFCC (income) | Fee amount | Currency of account |

-	Case: Customer deposit at Branch 1 (BR1), for account opens at Branch 2 (BR2).

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | BR1.CASH | Amount deposit | Currency of account |
| 1 | C | BR1.IBT.HO | Amount deposit | Currency of account |
| 2 | D | HO.IBT.BR1 | Amount deposit | Currency of account |
| 2 | C | HO.IBT.BR2 | Amount deposit | Currency of account |
| 3 | D | BR2.IBT.HO | Amount deposit | Currency of account |
| 3 | C | BR2.DEPOSIT | Amount deposit | Currency of account |

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 4](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
`A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  |  | `Yes` | String |  |  |  |
| 2 | Username |  |  | `Yes` | String |  |  |  |
| 3 | Password |  |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_CACNM | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 4 | LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>