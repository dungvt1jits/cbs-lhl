# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CCD`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Cheque no | cheque_no | `Yes` | String | 20 | | | CCHQNO |
| 2 | Issuing bank | issuing_bank | `Yes` | String | 25 | | | SBNAME |
| 3 | Clearing bank | clearing_bank | `Yes` | String | 25 | | | CIBNK |
| 4 | Cheque currency | cheque_currency | No | String | 3 | | | CCCR |
| 5 | Cheque amount | cheque_amount | `Yes` | `Number` | | 0 | | CAMT |
| 6 | Debit GL account | debit_gl_account | `Yes` | String | 25 | | | PGACC |
| 7 | Beneficiary account | beneficiary_account | `Yes` | String | 25 | | | PDACC |
| 8 | Holding days | holding_days | No | `Number` | | 0 | trường ẩn | CAMT1 |
| 9 | Drawer name | drawer_name | No | String | 250 | | | CACNM2 |
| 10 | Drawer paper number | drawer_paper_number | No | String | 25 | | | CREPID2 |
| 11 | Drawer address | drawer_address | No | String | 250 | | | SBADDR |
| 12 | Beneficiary amount | beneficiary_amount | No | `Number` | | 0 | | PCSAMT |
| 13 | Beneficiary currency | beneficiary_currency | No | String | 3 | | | RCCR |
| 14 | Exchange rate | exchange_rate | No | `Number` | | 0 | | CCRRATE |
| 15 | base amount | base_amount | No | `Number` | | 0 | | CBAMT |
| 16 | Description | description | No | String | 250 | | | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 3 | Cheque no | cheque_no | String | | |
| 4 | Issuing bank | issuing_bank | String | | |
| 5 | Clearing bank | clearing_bank | String | | |
| 6 | Cheque currency | cheque_currency | String | | |
| 7 | Cheque amount | cheque_amount | `Number` | | |
| 8 | Debit GL account | debit_gl_account | String | | |
| 9 | Beneficiary account | beneficiary_account | String | | |
| 10 | Holding days | holding_days | `Number` | | trường ẩn |
| 11 | Drawer name | drawer_name | String | | |
| 12 | Drawer paper number | drawer_paper_number | String | | |
| 13 | Drawer address | drawer_address | String | | |
| 14 | Beneficiary amount | beneficiary_amount | `Number` | | |
| 15 | Beneficiary currency | beneficiary_currency | String | | |
| 16 | Exchange rate | exchange_rate | `Number` | | |
| 17 | base amount | base_amount | `Number` | | |
| 18 | Description | description | String | | |
| 19 | Available balance  | | `Number` | | | số có hai số thập phân |
| 20 | Earmark (book) amount | | `Number` | | | số có hai số thập phân |
| 21 | Transaction status | status | String |  |  |
| 22 | Transaction date | transaction_date | `Date time` |  |  |
| 23 | User id | user_id | `Number` |  | 

## 1.2 Transaction flow
**Conditions:**
-   Cheque no, Clearing bank, Issuing baank is unique
-	Customer must have beneficiary account (deposit account) to bank payment:
    +	Account type is current or saving. 
    +	Account status is "New"/"Normal".
-	GL allows debit side.
-	Exist clearing bank in system.
-   Debit GL account and beneficiary account must same branch with branch does this tranaction
-   Cheque currency and debit GL account have same currency

**Flow of events:**
-	Payment by transfer to deposit account.
-	Allow payment amount that currency is different with cheque's currency.
-	Transaction complete:
    +	Beneficiary account's balance will increase. But amount is kept.
    +	Add into queue to accept payment or not.
-	To finish payment the cheque issued by other banks:
    +	Do transaction **"Outward cheque cleared"** to accept payment. 
    +	Or do transaction **"Outward cheque returned"** to cancel payment.

**Database:**
ClearingCheck
DepositStatement
DepositTransaction
DepositHistory

**Posting:**
-	If currency of cheque and beneficiary account is different:

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DPTCLR (GL Domestic cheque) | Cheque amount | Cheque currency |
| 1 | C | General Exchange Foreign Currencies | Cheque amount | Cheque currency |
| 2 | D | General Exchange Foreign Currencies | Beneficiary amount | Beneficiary currency |
| 2 | C | DEPOSIT | Beneficiary amount | Beneficiary currency |

-	If currency of cheque and beneficiary account is same:

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DPTCLR (GL Domestic cheque) | Cheque amount | Cheque currency |
| 1 | C | DEPOSIT | Cheque amount | Cheque currency |

-	Note:
    +	GL Domestic cheque: DPTCLR is defined at “Accounting setup/Common account definition”.
    +	General Exchange Foreign Currencies: is defined at “Accounting setup/ Foreigner exchange account definition”.

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CIBNK | GET_INFO_CIBNK_AND_NOSTRO_GL | [GET_INFO_CIBNK_AND_NOSTRO_GL](Specification/Common/16 Payment Rulefuncs) |
| 2 | GET_INFO_CCRRATE | FX_RULEFUNC_GET_INFO_CCRRATE | [FX_RULEFUNC_GET_INFO_CCRRATE](Specification/Common/20 FX Rulefuncs) |
| 3 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 4 | GET_INFO_CUSTOMER | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 5 | LKP_DATA_CIBNK | PMT_LOOKUP_AGENTBANK | [PMT_LOOKUP_AGENTBANK](Specification/Common/16 Payment Rulefuncs) |
| 6 | LKP_DATA_VOSTRO | ACT_ACCHRT_LOOKUP_BY_BRANCHID | [ACT_ACCHRT_LOOKUP_BY_BRANCHID](Specification/Common/11 Accounting Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>