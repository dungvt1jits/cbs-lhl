# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_DMN`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | PDACC |
| 2 | Account name | account_name | `Yes` | String | 250 |  |  | CACNM |
| 3 | Account status | account_status | `Yes` | String | 1 |  |  | DPTSTS |
| 4 | Account currency | account_currency | `Yes` | String | 3 |  |  | PCSCCR |
| 5 | Amount | amount | `Yes` | `Number` |  | > 0 |  | PCSAMT |
| 6 | Deposit method | deposit_method | `Yes` | String | 3 |  |  | PAYBY |
| 7 | Debit account | debit_account | No | String | 25 |  |  | PGACC |
| 8 | Debit account name | debit_account_name | No | String | 250 |  |  | CACNM1 |
| 9 | Debit account currency | debit_account_currency | No | String | 3 |  |  | PGCCR |
| 10 | Total fee | total_fee | No | `Number` |  | 0 |  | CFAMT |
| 11 | Amount receive from customer | amount_receive_from_customer | `Yes` | `Number` |  | 0 |  | PCSCVT |
| 12 | Description | description | No | String | 250 |  |  | DESCS |
| 13 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 14 | Inclusive | inclusive | No | String |  |  | trường ẩn | CINC |
| 15 | Prepaid Interest | prepaid_interest | No | `Number` |  | 0 | trường ẩn | CTINT |
| 16 | Interest tenor unit | interest_tenor_unit | No | String | 1 |  | trường ẩn | CTRN |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String | | |
| 3 | Account name | account_name | String | | |
| 4 | Account status | account_status | String | | |
| 5 | Account currency | account_currency | String | | |
| 6 | Amount | amount | `Number` | | |
| 7 | Deposit method | deposit_method | String | | |
| 8 | Debit account | debit_account | String | | |
| 9 | Debit account name | debit_account_name | String | | |
| 10 | Debit account currency | debit_account_currency | String | | |
| 11 | Total fee | total_fee | `Number` | | |
| 12 | Amount receive from customer | amount_receive_from_customer | `Number` | | |
| 13 | Description | description | String | | |
| 14 | Fee data | fee_data | JSON Object | | |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 15 | Inclusive | inclusive | String | | |
| 16 | Prepaid Interest | prepaid_interest | `Number` | | |
| 17 | Interest tenor unit | interest_tenor_unit | String | | |
| 18 | Transaction status | status | String |  |  |
| 19 | Transaction date | transaction_date | `Date time` |  |  |
| 20 | User id | user_id | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- Deposit by Cash: Tham khảo giao dịch [DPT_CDP: 1120: Cash withdrawal
](Specification/Deposit/16-DPT_CWR-Cash-Withdrawal)
- Deposit by GL: Tham khảo giao dịch [DPT_MDP: 1122: Miscellaneous withdrawal
](Specification/Deposit/17-DPT_MWR-Miscellaneous-Withdrawal)
- Deposit by Deposit: Tham khảo giao dịch [DPT_TRF: 1130: Transfer from deposit account to deposit account](Specification/Deposit/14-DPT_TRF-Transfer-From-Deposit-Account-To-Deposit-Account)

**Flow of events:**
- Deposit by Cash: Tham khảo giao dịch [DPT_CDP: 1120: Cash withdrawal
](Specification/Deposit/16-DPT_CWR-Cash-Withdrawal)
- Deposit by GL: Tham khảo giao dịch [DPT_MDP: 1122: Miscellaneous withdrawal
](Specification/Deposit/17-DPT_MWR-Miscellaneous-Withdrawal)
- Deposit by Deposit: Tham khảo giao dịch [DPT_TRF: 1130: Transfer from deposit account to deposit account](Specification/Deposit/14-DPT_TRF-Transfer-From-Deposit-Account-To-Deposit-Account)

-   If transaction has collect fee, user must pay more for fee
<br>Ex: Deposit amount = 100, Fee = 10 => Deposit account will receive: 100, user pay more for fee: 10 

**Database:**


**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Account number |  | String |  |  |  |
| 3 | From date |  | `Date time` |  |  |  |
| 4 | To date |  | `Date time` |  |  |  |
| 5 | Transaction date |  | `Date time` |  |  |  |
| 6 | User id |  | String |  |  |  |
| 7 | Transaction status |  | String |  |  |  |
| 8 | Transaction list |  | JSON Object |  |  |  |
|  | Transaction code |  | String |  |  |  |
|  | Transaction number |  | `Date time` |  |  |  |
|  | Transaction date |  | String |  |  |  |
|  | Created by |  | String |  |  |  |
|  | DORC |  | `Number` |  |  | số có hai số thập phân |
|  | Amount |  | String |  |  |  |
|  | Description |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_PDACC | DPT_GET_INFO_DPTACC_CASA chuyển sang GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_DESCS | `select '1110: Deposit money by ' CASE '@PAYBY' WHEN 'CSH' THEN 'cash' WHEN 'DPT' then 'deposit' WHEN 'ACT' THEN 'miscellaneous' ELSE 'cheque' end  from dual` | Làm trên JWEB |
| 4 | GET_INFO_PGACC | DPT_GET_INFO_SACNO | [DPT_GET_INFO_SACNO](Specification/Common/15 Deposit Rulefuncs) |
| 5 | TRAN_FEE_DPT_DMN/GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 6 | TRAN_FEE/GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 7 | TRAN_FEE_DPT_DMN/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) |
| 8 | LKP_DATA_SACNO | DPT_LKP_DATA_SACNO | [DPT_LKP_DATA_SACNO](Specification/Common/15 Deposit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>