# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CDT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Stock prefix | stock_prefix | `Yes` | String | 5 |  | trường ẩn | CSTKPRE |
| 2 | Cheque no | cheque_no | `Yes` | String | 20 |  |  | CFRSER |
| 3 | Debit account | debit_account | `Yes` | String | 25 |  |  | PDACC |
| 4 | Balance | balance | No | `Number` |  | 0 |  | CAMT2 |
| 5 | Available balance | available_balance | No | `Number` |  | 0 |  | CAMT3 |
| 6 | Total Fee for Credit account | total_fee_for_credit_account | No | `Number` |  | 0 | trường ẩn | AC2FEE |
| 7 | Total vat for Credit account | total_vat_for_credit_account | No | `Number` |  | 0 | trường ẩn | AC2VAT |
| 8 | Debit amount | debit_amount | `Yes` | `Number` |  | > 0 |  | CTXAMT1 |
| 9 | Exchange rate (Debit account/BCY) | exchange_rate_ctxexr | No | `Number` |  | 0 | trường ẩn | CTXEXR |
| 10 | Amount equivalent in BCY | amount_equivalent_in_bcy | No | `Number` |  | 0 | trường ẩn | CAMTCVT |
| 11 | Credit account | credit_account | `Yes` | String | 25 |  |  | PDOACC |
| 12 | Currency of debit account | currency_of_debit_account | No | String | 3 |  | trường ẩn | CCCR |
| 13 | Currency of credit account | currency_of_credit_account | No | String | 3 |  | trường ẩn | CCCR1 |
| 14 | Cross rate | cross_rate | No | `Number` |  | 0 | trường ẩn | CCRRATE |
| 15 | Credit amount | credit_amount | No | `Number` |  | 0 |  | PDOCVT |
| 16 | Exchange rate (Credit Ac/BCY) | exchange_rate_credit_ac_bcy | No | `Number` |  | 0 | trường ẩn | PGEXR |
| 17 | Amount to credit/BCY | amount_to_credit_bcy | No | `Number` |  | 0 | trường ẩn | PGCVT |
| 18 | Debit account name | debit_account_name | No | String | 250 |  |  | CACNM |
| 19 | Customer code | customer_code | No | String | 15 |  |  | CCTMCD |
| 20 | Customer address | customer_address | No | String | 250 |  |  | CCTMA |
| 21 | Customer description | customer_description | No | JSON Object | 50 |  |  | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 22 | Commission | commission | No | `Number` |  | 0 | trường ẩn | CFAMT |
| 23 | Value date | value_date | No | `Date time` |  | Working date | trường ẩn | CVLDT |
| 24 | Total fee/Credit account | total_fee_credit_account | No | `Number` |  | 0 | trường ẩn | PGAMT |
| 25 | Paper number of debit | paper_number_of_debit | No | String | 100 |  | trường ẩn | CREPID |
| 26 | Issue date of debit | issue_date_of_debit | No | `Date time` |  | Working date | trường ẩn | CIDDT |
| 27 | Issue place of debit | issue_place_of_debit | No | String | 90 |  | trường ẩn | CIDPLACE |
| 28 | Paper number of credit | paper_number_of_credit | No | String | 100 |  | trường ẩn | CREPID2 |
| 29 | Issue date of credit | issue_date_of_credit | No | `Date time` |  | Working date | trường ẩn | CIDDT2 |
| 30 | Issue place of credit | issue_place_of_credit | No | String | 90 |  | trường ẩn | CIDPLACE2 |
| 31 | Credit account name | credit_account_name | No | String | 250 |  | trường ẩn | CACNM2 |
| 32 | Exchange rate (Debit account/BCY) | exchange_rate_cbkexr | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 33 | Amount (Debit account/BCY) | amount_debit_account_bcy | No | `Number` |  | 0 | trường ẩn | CCVT |
| 34 | Account Linkage | account_linkage | No | String | 25 |  | trường ẩn | SACNO |
| 35 | Amount linkage | amount_linkage | No | `Number` |  | 0 | trường ẩn | CAMT4 |
| 36 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 37 | Description | description | No | String | 250 |  |  | DESCS |
| 38 | Branch name | branch_name | No | String | | | `gửi thêm` | |

TXB_PDOACC|CACNM2

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Stock prefix | stock_prefix | String |  | trường ẩn |
| 3 | Cheque no | cheque_no | String |  |  |
| 4 | Debit account | debit_account | String |  |  |
| 5 | Balance | balance | `Number` |  |  |
| 6 | Available balance | available_balance | `Number` |  |  |
| 7 | Total Fee for Credit account | total_fee_for_credit_account | `Number` |  | trường ẩn |
| 8 | Total vat for Credit account | total_vat_for_credit_account | `Number` |  | trường ẩn |
| 9 | Debit amount | debit_amount | `Number` |  |  |
| 10 | Exchange rate (Debit account/BCY) | exchange_rate_ctxexr | `Number` |  | trường ẩn |
| 11 | Amount equivalent in BCY | amount_equivalent_in_bcy | `Number` |  | trường ẩn |
| 12 | Credit account | credit_account | String |  |  |
| 13 | Currency of debit account | currency_of_debit_account | String |  | trường ẩn |
| 14 | Currency of credit account | currency_of_credit_account | String |  | trường ẩn |
| 15 | Cross rate | cross_rate | `Number` |  | trường ẩn |
| 16 | Credit amount | credit_amount | `Number` |  |  |
| 17 | Exchange rate (Credit Ac/BCY) | exchange_rate_credit_ac_bcy | `Number` |  | trường ẩn |
| 18 | Amount to credit/BCY | amount_to_credit_bcy | `Number` |  | trường ẩn |
| 19 | Debit account name | debit_account_name | String |  |  |
| 20 | Customer code | customer_code | String |  |  |
| 21 | Customer address | customer_address | String |  |  |
| 22 | Customer description | customer_description | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
| 23 | Commission | commission | `Number` |  | trường ẩn |
| 24 | Value date | value_date | `Date time` |  | trường ẩn |
| 25 | Total fee/Credit account | total_fee_credit_account | `Number` |  | trường ẩn |
| 26 | Paper number of debit | paper_number_of_debit | String |  | trường ẩn |
| 27 | Issue date of debit | issue_date_of_debit | `Date time` |  | trường ẩn |
| 28 | Issue place of debit | issue_place_of_debit | String |  | trường ẩn |
| 29 | Paper number of credit | paper_number_of_credit | String |  | trường ẩn |
| 30 | Issue date of credit | issue_date_of_credit | `Date time` |  | trường ẩn |
| 31 | Issue place of credit | issue_place_of_credit | String |  | trường ẩn |
| 32 | Credit account name | credit_account_name | String |  | trường ẩn |
| 33 | Exchange rate (Debit account/BCY) | exchange_rate_cbkexr | `Number` |  | trường ẩn |
| 34 | Amount (Debit account/BCY) | amount_debit_account_bcy | `Number` |  | trường ẩn |
| 35 | Account Linkage | account_linkage | String |  | trường ẩn |
| 36 | Amount linkage | amount_linkage | `Number` |  | trường ẩn |
| 37 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String |  |  |
|  | Currency account code | currency_account_code | String |  | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 38 | Description | description | String |  |  |
| 39 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String |  |  |
| 40 | Transaction status | status | String |  |  |
| 41 | Transaction date | transaction_date | `Date time` |  |  |
| 42 | User id | user_id | `Number` |  | 
| 43 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Cheque no.: exists and status "Unpaid"/"Earmark".
 	+ Status of cheque is "Unpaid": Amount to transfer (not include amount is keeping).
 	+ Status of cheque is "Earmark": Amount to transfer = Holding amount
-	Transferring account: exists and status <> {"Closed", "Block"}. 
-	Receive account: exists and status <> "Closed". Account type is current, saving.
-	Accounts must be same currency.
-	Cheque amount must be larger than zero.

**Flow of events:**
-	Allow to collect fee.
-	Transaction complete:
    +	Cheque status: Paid
    +	Transferring account will decrease balance.
    +	Receive account will increase balance.
-   If transaction has collect fee, system will collect fee from deposit account (receive)
<br>Ex: Deposit amount = 100, Fee = 10 => Deposit account will receive: 90, system collect fee from deposit account: 10 

**Posting**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT (transfer) | Amount transfer | Currency of deposit account |
| 1 | C | DEPOSIT (receive) | Amount transfer | Currency of deposit account |
| Case collect fee (fee amount > 0) |  |  |  |  |
| 2 | D | DEPOSIT (receive) | Fee amount | Currency of deposit account |
| 2 | C | IFCC (income) | Fee amount | Currency of deposit account |

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_INFO_EMK | DPT_GET_INFO_EMK_BY_SERIAL | [DPT_GET_INFO_EMK_BY_SERIAL](Specification/Common/15 Deposit Rulefuncs) |  |  |
| 2 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX_C | [DPT_GET_STOCK_PREFIX_C](Specification/Common/15 Deposit Rulefuncs) |  |  |
| 3 | GET_INFO_AC | DPT_GET_INFO_PDACC_BY_CHEQUE_SERIAL | [DPT_GET_INFO_PDACC_BY_CHEQUE_SERIAL](Specification/Common/15 Deposit Rulefuncs) |  |  |
| 4 | GET_INFO_BCY_ACNAME | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |  |  |
| 5 | GET_INFO_CUSTOMER | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |  |  |
| 6 | GET_INFO_ACLINK | DPT_GET_INFO_ACLINK | [DPT_GET_INFO_ACLINK](Specification/Common/15 Deposit Rulefuncs) |  |  |
| 7 | GET_INFO_OTHER_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) | `account_number` = `credit_account` | `account_name` = `credit_account_name`, `currency_code` = `currency_of_credit_account` |
| 8 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |  |  |
| 9 | GET_SIG_PDOACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |  |  |
| 10 | GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |  |  |
| 11 | LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |  |  |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>