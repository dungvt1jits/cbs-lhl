# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CCW`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Stock amount | stock_amount | `Yes` | `Number` | | 0 | | CAMT1 |
| 2 | Serial no | serial_no | `Yes` | String | 15 | | | CFRSER  | |
| 3 | Withdrawal amount | withdrawal_amount | `Yes` | `Number` | | > 0 | | PCSAMT |
| 4 | Currency | currency_code | No | String | 3 | | | CCCR  |
| 5 | Withdrawal method | withdrawal_method | `Yes` | String | 3 | | | PAYBY |
| 6 | Credit account | account_number | No | String | 25 | | | SACNO |
| 7 | Stock prefix | stock_prefix | No | String | 5 | | trường ẩn | CSTKPRE |
| 8 | Branch id | branch_id | No | `Number` | | branch id của user login | trường ẩn | CBRCD |
| 9 | Description | description | No | String | 250 | | | DESCS |
| 10 | Expired date | expired_date | No | `Date time` |  |  | `gửi thêm` |  |
| 11 | Fee data | fee_data | No | Array Object |  |  | `gửi thêm` |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  |  |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân |  |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân |  |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân |  |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân |  |
|  | Currency | currency_fee_code | No | String | 3 |  |  |  |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn |  |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn |  |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT or ACT | trường ẩn |  |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn |  |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn |  |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn |  |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn |  |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn |  |
| 12 | Beneficiary name | beneficiary_name | No | String |  |  | `gửi thêm` |  |
| 13 | Beneficiary ID number | beneficiary_id_number | No | String |  |  | `gửi thêm` |  |
| 14 | Beneficiary contact number | beneficiary_contact_number | No | String |  |  | `gửi thêm` |  |
| 15 | Beneficiary address | beneficiary_address | No | String |  |  | `gửi thêm` |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Stock amount | stock_amount | `Number` | | |
| 3 | Serial no | serial_no | String | | |
| 4 | Withdrawal amount | withdrawal_amount | `Number` | | |
| 5 | Currency | currency_code | String | | |
| 6 | Withdrawal method | withdrawal_method | String | | |
| 7 | Credit account | account_number | String | | |
| 8 | Stock prefix | stock_prefix | String | | trường ẩn |
| 9 | Branch id | branch_id | `Number` | | trường ẩn |
| 10 | Description | description | String | | |
| 11 | Transaction status | status | String |  |  |
| 12 | Transaction date | transaction_date | `Date time` |  |  |
| 13 | User id | user_id | `Number` |  |  |
| 14 | Expired date | expired_date | `Date time` |  | `trả thêm` |
| 15 | Fee data | fee_data | JSON Object | | | |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 16 | Beneficiary name | beneficiary_name | String |  | `trả thêm` |
| 17 | Beneficiary ID number | beneficiary_id_number | String |  | `trả thêm` |
| 18 | Beneficiary contact number | beneficiary_contact_number | String |  | `trả thêm` |
| 19 | Beneficiary address | beneficiary_address | String |  | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
- With cheque book
  - Book status: Normal
  - Status of stock leaves: Unpaid
  - Stock balance > 0
- 0 < Withdraw amount = Stock amount
- Withdraw by **Cash**:
  - User must have enough cash to withdraw.
  - User has right to do transaction related to cash.
  - User must have enough cash to give customer.
- Withdraw by **Deposit**:
  - Deposit type is Current/ Saving 
  - Status is Normal (KTB chưa xét trường hợp khi người dùng nhập tay deposit account là Fixed deposit và có status là Normal thay vì chọn dữ liệu trong lookup)
  - Deposit account and stock must be same branch
  - Deposit's currency and stock's currency must be the same
- Withdraw by **Accounting**:
  - GL account is active
  - GL's currency and stock's currency must be the same
  - Balance side is Both/ Credit (KTB chưa xét điều kiện này)
  - GL must belongs to stock's branch
- Account number for fee:
    - Nếu là deposit account number
      - Deposit type là Current hoặc Saving
      - Deposit status là: Normal
      - Deposit account có available balance > or = total fee amount
    - Nếu là accounting number
      - Chung currency với currency_code
      - Chung branch với branch của user đang login
      - Không phải là master account

**Flow of events:**
- When complete transaction: 
  - Stock leaves status is "Paid".
  - Stock balance will decrease
  - Cash will decrease (if withdraw method is Cash)
  - Deposit balance will increase (if withdraw method is Deposit)
- Collect fee:
  - Cash will increase (if withdraw method is Cash)
  - Deposit balance decrease (if withdraw method is Deposit)

**Database**

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASHIER CHEQUE | Withdraw amount | Currency of cheque |
| 1 | C | CASH/DEPOSIT/GL | Withdraw amount | Currency of cheque |
| Case collect fee (fee amount > 0) | | | | |
| 1 | D | CASH/DEPOSIT/GL | Fee amount | Currency of cheque |
| 1 | C | IFCC (income) | Fee amount | Currency of cheque |

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Account number |  | String |  |  |  |
| 3 | From date |  | `Date time` |  |  |  |
| 4 | To date |  | `Date time` |  |  |  |
| 5 | Transaction date |  | `Date time` |  |  |  |
| 6 | User id |  | String |  |  |  |
| 7 | Transaction status |  | String |  |  |  |
| 8 | Transaction list |  | JSON Object |  |  |  |
|  | Transaction code |  | String |  |  |  |
|  | Transaction number |  | `Date time` |  |  |  |
|  | Transaction date |  | String |  |  |  |
|  | Created by |  | String |  |  |  |
|  | DORC |  | `Number` |  |  | số có hai số thập phân |
|  | Amount |  | String |  |  |  |
|  | Description |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CCR | DPT_GET_INFO_STOCK_BY_CASHIER_CHEQUE | [DPT_GET_INFO_STOCK_BY_CASHIER_CHEQUE](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX_G | [DPT_GET_STOCK_PREFIX_G](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_PAYBY_CSH | `SELECT null,null FROM DUAL` | `SACNO, TXB_SACNO` Làm trên JWEB |
| 4 | GET_INFO_DESCS | `select '11807: Cashier Cheque withdrawal by ' CASE '@PAYBY' WHEN 'CSH' THEN 'cash' WHEN 'DPT' then 'deposit' WHEN 'ACT' THEN 'miscellaneous' ELSE '' end  from dual` | Làm trên JWEB |
| 5 | GET_INFO_ACT_SACNO | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 6 | GET_INFO_DPT_CCCR | DPT_GET_INFO_SACNO | [DPT_GET_INFO_SACNO](Specification/Common/15 Deposit Rulefuncs) |
| 7 | LKP_DATA_SACNO | DPT_LKP_DATA_SACNO | [DPT_LKP_DATA_SACNO](Specification/Common/15 Deposit Rulefuncs) |
| 8 | Get thông tin IFC theo `ifc_code` và `currency_code` | IFC_GET_INFO_IFCCD_BY_CURRENCY_AND_TRANSCODE | [IFC_GET_INFO_IFCCD_BY_CURRENCY_AND_TRANSCODE](Specification/Common/21 IFC Rulefuncs) |
| 9 | Get list IFC theo `currency_code` | IFC_LOOKUP_BY_CURRENCY_AND_TRANSCODE | [IFC_LOOKUP_BY_CURRENCY_AND_TRANSCODE](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>