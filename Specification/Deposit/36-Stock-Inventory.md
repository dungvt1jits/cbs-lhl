# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/StockInventory​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_SEARCH_STOCKINVENTORY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Stock no | stock_no | String |  |  |  |
| 2 | Stock prefix | stock_prefix | String |  |  |  |
| 3 | From serial | from_serial | `Number` |  |  |  |
| 4 | To serial | to_serial | `Number` |  |  |  |
| 5 | Book status | book_status | String |  |  |  |
| 6 | Confirm status | confirm_status | String |  |  |  |
| 7 | Branch name | branch_name | String |  |  |  |
| 8 | User name | user_name | String |  |  |  |
| 9 | Status of stock leaves | stock_leaves_status | String |  |  |  |
| 10 | Stock type | stock_type | String |  |  |  |
| 11 | Account number | ref_account_no | String | 15 |  |  |
| 12 | Stock id | id | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/StockInventory​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_ADSEARCH_STOCKINVENTORY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Stock no | stock_no | No | String |  |  |  |
| 2 | Stock prefix | stock_prefix | No | String |  |  |  |
| 3 | From serial from | from_serial_from | No | `Number` |  |  |  |
| 4 | From serial to |from_serial_to  | No | `Number` |  |  |  |
| 5 | To serial from | to_serial_from | No | `Number` |  |  |  |
| 6 | To serial to | to_serial_to | No | `Number` |  |  |  |
| 7 | Book status | book_status | No | String |  |  |  |
| 8 | Confirm status | confirm_status | No | String |  |  |  |
| 9 | Branch name | branch_name | No | String |  |  |  |
| 10 | User name | user_name | No | String |  |  |  |
| 11 | Status of stock leaves | stock_leaves_status | No | String |  |  |  |
| 12 | Stock type | stock_type | No | String |  |  |  |
| 13 | Account number | ref_account_no | No | String | 15 |  |  |
| 14 | Page index | page_index | No | `Number` |  |  |  |
| 15 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Stock no | stock_no | String |  |  |  |
| 2 | Stock prefix | stock_prefix | String |  |  |  |
| 3 | From serial | from_serial | `Number` |  |  |  |
| 4 | To serial | to_serial | `Number` |  |  |  |
| 5 | Book status | book_status | String |  |  |  |
| 6 | Confirm status | confirm_status | String |  |  |  |
| 7 | Branch name | branch_name | String |  |  |  |
| 8 | User name | user_name | String |  |  |  |
| 9 | Status of stock leaves | stock_leaves_status | String |  |  |  |
| 10 | Stock type | stock_type | String |  |  |  |
| 11 | Account number | ref_account_no | String | 15 |  |  |
| 12 | Stock id | id | `Number` |  |  |  |

**Example:**
```json
{

}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
[Tham khảo thông tin chi tiết tại đây](Specification/Deposit/38 DPT_SRG Stock Registration)<br>

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/StockInventory​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_VIEW_STOCKINVENTORY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Stock id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 2
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock no. | stock_no | String |  |  |
| 2 | Module | module | String |  |  |
| 3 | Stock type | stock_type | String | 1 |  |
| 4 | From serial | from_serial | String | 15 |  |
| 5 | To serial | to_serial | String | 15 |  |
| 6 | Stock prefix | stock_prefix | String | 5 |  |
| 7 | No. of leaves/book | no_of_leaves | `Number` |  |  |
| 8 | No. of books | no_of_books | `Number` |  |  |
| 9 | Stock leaves status | stock_leaves_status | String |  |  |
| 10 | Confirm status | confirm_status | String |  |  |
| 11 | Book status | book_status | String |  |  |
| 12 | Ref.account No. | ref_account_no | String |  |  |
| 13 | Issuing name | issuing_name | String |  |  |
| 14 | Amount | amount | `Number` |  | số có hai số thập phân  |
| 15 | Currency | currency | String |  |  |
| 16 | Beneficiary name | beneficiary_name | String |  |  |
| 17 | Contact | contact | String |  |  |
| 18 | Purpose | purpose | String |  |  |
| 19 | Stock balance | stock_balance | `Number` |  | số có một số thập phân  |
| 20 | Branch id | branch_id | `Number` |  |  |
| 21 | Assigned teller id | assigned_teller_id | `Number` |  |  |
| 22 | Created by | user_created | `Number` |  |  |
| 23 | Approved by | user_approved | `Number` |  |  |
| 24 | Previous status | pslsts | String |  | Không hiện trên UI |
| 25 | Last date | last_date | `Date time` |  | Không hiện trên UI |
| 26 | Stock id | id | `Number` |  |  |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Assigned teller code | assigned_teller_code | String |  | `trả thêm` |
|  | Assigned teller name | assigned_teller_name | String |  | `trả thêm` |

**Example:**
```json
{

}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Delete
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/StockInventory​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_DELETE_STOCKINVENTORY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Stock id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 2
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 2
}

## 5.2 Transaction flow
- Check book status = H or = B thì có thể delete

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |