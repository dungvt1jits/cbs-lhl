# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_OCR`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Cheque no | cheque_no | `Yes` | String | 20 | | | CCHQNO |
| 2 | Issuing bank | issuing_bank | No | String | | | | SBNAME |
| 3 | Clearing bank | clearing_bank | `Yes` | String | | | | CIBNK |
| 4 | Cheque currency | cheque_currency | `Yes` | String | 3 | | | CCCR |
| 5 | Cheque amount | cheque_amount | `Yes` | `Number` | | 0 | | CAMT |
| 6 | Beneficiary account | beneficiary_account | `Yes` | String | 25 | | | PDACC |
| 7 | Beneficiary currency | beneficiary_currency | No | String | 3 | | | RCCR |
| 8 | Beneficiary amount | beneficiary_amount | No | `Number` | | 0 | | PCSAMT |
| 9 | Clearing bank nostro | clearing_bank_nostro | `Yes` | String | 25 | | | PGACC |
| 10 | Drawer name | drawer_name | No | String | 250 | | | CACNM2 |
| 11 | Drawer paper number | drawer_paper_number | No | String | 100 | | | CREPID2 |
| 12 | Drawer address | drawer_address | No | String | 250 | | | SBADDR |
| 13 | Description | description | No | String | 250 | | | DESCS |
| 14 | base amount | base_amount | No | `Number` | | 0 | | CBAMT |
| 15 | Debit note account | debit_note_account | No | String | 25 | | trường ẩn | PDOACC |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Cheque no | cheque_no | String | | |
| 3 | Issuing bank | issuing_bank | String | | |
| 4 | Clearing bank | clearing_bank | String | | |
| 5 | Cheque currency | cheque_currency | String | | |
| 6 | Cheque amount | cheque_amount | `Number` | | |
| 7 | Beneficiary account | beneficiary_account | String | | |
| 8 | Beneficiary currency | beneficiary_currency | String | | |
| 9 | Beneficiary amount | beneficiary_amount | `Number` | | |
| 10 | Clearing bank nostro | clearing_bank_nostro | String | | |
| 11 | Drawer name | drawer_name | String | | |
| 12 | Drawer paper number | drawer_paper_number | String | | |
| 13 | Drawer address | drawer_address | String | | |
| 14 | Description | description | String | | |
| 15 | base amount | base_amount | `Number` | | |
| 16 | Debit note account | debit_note_account | String | | |
| 17 | Cheque status | | String | | | |
| 18 | Transaction status | status | String | | |
| 19 | Transaction date | transaction_date | `Date time` |  |  |
| 20 | User id | user_id | String |  |  |

## 1.2 Transaction flow
**Conditions:**
-	Outward cheque exists with status <> "C"/ "S" in queue.
-   All information map with cheque no

**Flow of events:**
-	Select it in queue and click button "Return".
-	System will auto create transaction to return payment and information is taken from Outward cheque.
-	Transaction complete: 
    +	Outward cheque doesn't show in queue.
    +	Customer cannot use beneficiary amount. (Decrease balance account).

**Database:**
ClearingCheck
DepositStatement
DepositTransaction
DepositHistory

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

**Posting:**
-	If currency of cheque and beneficiary account is different:

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Beneficiary amount | Beneficiary currency |
| 1 | C | General Exchange Foreign Currencies | Beneficiary amount | Beneficiary currency |
| 2 | D | General Exchange Foreign Currencies | Cheque amount | Cheque currency |
| 2 | C | DPTCLR (GL Domestic cheque) | Cheque amount | Cheque currency |

-	If currency of cheque and beneficiary account is same:

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Cheque amount | Cheque currency |
| 1 | C | DPTCLR (GL Domestic cheque) | Cheque amount | Cheque currency |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CHEQUE | DPT_GET_INFO_CHEQUE | [DPT_GET_INFO_CHEQUE](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_INFO_CUST | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_CIBNK | GET_INFO_CIBNK | [GET_INFO_CIBNK](Specification/Common/16 Payment Rulefuncs) |
| 4 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 5 | LKP_DATA_VOSTRO | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 6 | LKP_DATA_CIBNK | PMT_LOOKUP_AGENTBANK | [PMT_LOOKUP_AGENTBANK](Specification/Common/16 Payment Rulefuncs) |
| 7 | LKP_DATA_PDOACC | ACT_ACCHRT_LOOKUP_BY_BRANCHID | [ACT_ACCHRT_LOOKUP_BY_BRANCHID](Specification/Common/11 Accounting Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>