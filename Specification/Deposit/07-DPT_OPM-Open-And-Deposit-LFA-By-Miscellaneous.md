# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | | No | String | 25 | | |
| 2 | Master account number | | `Yes` | String | 25 | | |
| 3 | Customer type | | `Yes` | String | 1 | | |
| 4 | Customer code | | `Yes` | String | 15 | | |
| 5 | Catalogue code | | `Yes` | String | 25 | | |
| 6 | Catalogue name | | `Yes` | String | 100 | | |
| 7 | Deposit type | | `Yes` | String | 1 | | |
| 8 | Deposit purpose | | `Yes` | String | 1 | | |
| 9 | Account type | | `Yes` | String | 2 | | |
| 10 | Account sub type | | `Yes` | String | 1 | | |
| 11 | GL account | | `Yes` | String | 25 | | |
| 12 | Amount deposit | | `Yes` | `Number` | | > 0 | số có hai số thập phân |
| 13 | Seq number | | No | `Number` | 6 | 0 | cho phép `null` |
| 14 | Description | | No | String | 250 | | |
| 15 | Account holder name | | `Yes` | String | 250 | | |
| 16 | Branch id | | `Yes` | String | | | Branch login |
| 17 | Working date | | `Yes` | `Date` | | | |
| 18 | User id | | `Yes` | String | | | User login |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references | | String | | | |
| 2 | Account number def | DEFACNO | String | 36 | | |
| 3 | Account number | | String | 15 | | |
| 4 | Master account number | | String | 15 | | |
| 5 | Customer type | | String | | | |
| 6 | Customer code | | String | 8 | | |
| 7 | Catalogue code | | String | 8 | | |
| 8 | Catalogue name | | String | | | |
| 9 | Deposit type | | String | | | |
| 10 | Deposit purpose | | String | | | |
| 11 | Account type | | String | | | |
| 12 | Account sub type | | String | | | |
| 13 | GL account | | String | 21 | | |
| 14 | Amount deposit | | `Number` | | | số có hai số thập phân |
| 15 | Seq number | | `Number` | | | |
| 16 | Description | | String | | | |
| 17 | Account holder name | | String | | | |
| 18 | Branch id | | String | | | Branch login |
| 19 | Account status | | String | | | |
| 20 | Open date | | `Date` | | | |
| 21 | Created by | | String | | | User login |
| 22 | Transaction status | | String | | | |

## 1.2 Transaction flow
**Conditions:**

-	"Master Fixed Deposit account": exists with status "Actived", "Normal".
-	"Amount deposit": must be larger than zero.
-	GL account: exists and same branch with user. 
-	GL allows debit side.
-	Currency of GL must be same with currency of deposit account.

**Flow of events:**
-	"Link Fixed Deposit account" status is "Pending to approve".
-	Balance link fixed deposit account will increase.


**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 1](Specification/Common/06 Transaction Flow Deposit)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |