# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CER`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | Cerfiticate serial | cerfiticate_serial | `Yes` | String | 20 | | | CFRSER |
| 3 | Description | description | No | String | 250 | | | DESCS |
| 4 | Stock prefix | stock_prefix | `Yes` | String | 5 | | trường ẩn | CSTKPRE |
| 5 | Currency code | currency_code | No | String | 3 |  | `gửi thêm`, giá trị load theo `account_number` |  |
| 6 | Fee collect menthod | method | `Yes` | String | 3 | `CSH` or `DPT` or `ACT` | `gửi thêm` |  |
| 7 | Account number for fee | account_number_for_fee | `Yes` | String | 25 |  | `gửi thêm` |  |
| 8 | Fee data | fee_data | No | Array Object |  |  | `gửi thêm` |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  |  |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân |  |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân |  |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân |  |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân |  |
|  | Currency | currency_fee_code | No | String | 3 |  |  |  |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn |  |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn |  |
|  | Payable source | pay_source | No | String | 3 | `CSH` or `DPT` or `ACT` | trường ẩn |  |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn |  |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn |  |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn |  |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn |  |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn |  |
| 9 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | 15 | |
| 3 | Cerfiticate serial | cerfiticate_serial | String | | |
| 4 | Description | description | String | | |
| 5 | Stock prefix | stock_prefix | String | | |
| 6 | Transaction status | status | String | | |
| 7 | Transaction date |  | `Date time` |  |  |
| 8 | User id |  | `Number` |  |  |
| 9 | Currency code | currency_code | String |  |  |
| 10 | Fee collect menthod | method | String |  |  |
| 11 | Account number for fee | account_number_for_fee | String | |  |
| 12 | Fee data | fee_data | JSON Object |  |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 13 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Books exist in Teller (user do transaction is who is managing these books).
    +	Stock type: Receipt
    +	Book status: Assigned to teller
    +	Confirm status: Confirm
    +	Status of stock leaves: Normal
-	Customer has FD account with
    +	Status is {"Normal","Dormant","Pending to approve", "Actived", "Maturity", "O"}
    +	Have no receipt
    +   Same branch with stock
- Account number for fee:
    - Nếu là deposit account number
      - Deposit type là Current hoặc Saving
      - Deposit status là: Normal
      - Deposit account có available balance > or = total fee amount
    - Nếu là accounting number
      - Chung currency với currency_code
      - Chung branch với branch của user đang login
      - Không phải là master account

**Flow of events:**
-	Allow collect fee for this transaction: fee issue receipt or other fees.
-	Don't allow issued receipt for other account. Except account is closed.
-	Transaction complete:
    +	Books have status "Normal".
    +	Allow print receipt.

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| Case collect fee (fee amount > 0) | | | | |
| 1 | D | CASH/DEPOSIT/GL | Fee amount | Currency of deposit account |
| 1 | C | IFCC (income) | Fee amount | Currency of deposit account |

**Cập nhật thông tin stock:**
[Tham khảo thông tin chi tiết tại mục 8](Specification/Common/06 Transaction Flow Deposit)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- This transaction is not allowed to reverse.
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_PRIX | Đổi `DPT_GET_STOCK_PREFIX_F` thành `DPT_GET_STOCK_PREFIX_R` | [DPT_GET_STOCK_PREFIX_R](Specification/Common/15 Deposit Rulefuncs) |
| 4 | Get thông tin IFC theo `ifc_code` và `currency_code` | IFC_GET_INFO_IFCCD_BY_CURRENCY_AND_TRANSCODE | [IFC_GET_INFO_IFCCD_BY_CURRENCY_AND_TRANSCODE](Specification/Common/21 IFC Rulefuncs) |
| 5 | Get list IFC theo `currency_code` | IFC_LOOKUP_BY_CURRENCY_AND_TRANSCODE | [IFC_LOOKUP_BY_CURRENCY_AND_TRANSCODE](Specification/Common/21 IFC Rulefuncs) |
| 6 | Get thông tin `account_number_for_fee` theo `account_number` | DPT_GET_INFO_ACLINK | [DPT_GET_INFO_ACLINK](Specification/Common/15 Deposit Rulefuncs) |
| 7 | Lookup theo `currency_code` | DPT_LKP_DATA_SACNO | [DPT_LKP_DATA_SACNO](Specification/Common/15 Deposit Rulefuncs) |