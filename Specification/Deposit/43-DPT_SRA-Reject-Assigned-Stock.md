# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_SRA`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Stock type | stock_type | `Yes` | String | 1 | | | CSTKTP |
| 2 | From serial | from_serial | `Yes` | String | 20 | | | CFRSER |
| 3 | To serial | to_serial | `Yes` | String | 20 | | | CTOSER |
| 4 | Description | description | No | String | 250 | | | DESCS |
| 5 | Stock prefix | stock_prefix | `Yes` | String | 5 | | trường ẩn | CSTKPRE |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Stock type | stock_type | String | | |
| 3 | From serial | from_serial | String | | |
| 4 | To serial | to_serial | String | | |
| 5 | Description | description | String | | |
| 6 | Transaction status | status | String | | |
| 7 | Stock prefix | stock_prefix | String | | |
|  | Transaction date |  | `Date time` |  |  |
|  | User id |  | String |  |  |

## 1.2 Transaction flow
**Conditions:**
-	Books exist with:
    - Branch to Branch:
      +	Book status: In stock of Branch
      + Confirm of status: Pending confirm HO send to branch
    - Branch to Teller:
      +	Book status: Assigned to teller
      + Confirm of status: Pending confirm branch send to teller
    - Teller to Teller:
      +	Book status: Assigned to teller
      + Confirm of status: Pending confirm teller send to teller

**Flow of events:**
-	Allow reject assigned many books at same time.
-	Allow reject many times. (Divide times).
-	If some reasons branch/teller is not received books. Can reject assigned by another transaction.
-	Branch to Branch: user do transaction must same branch with branch assigned.
- Branch to Teller: user reject == Assigned user
- Teller to Teller: user reject is "user approved/ assigned teller" of stock
-	Transaction complete:
    +	Branch to Branch: 
        +	Book status: In stock of HO
        +	Confirm status: Confirmed
        +   Branch code: Null
    +	Branch to Teller: 
        +	Book status: In stock of branch
        +	Confirm status: Confirmed
        +	Assigned teller code: Null
        +	Approved by: Teller rejected
        +	User name: Null
    +	Teller to Teller:
        +	Confirm status: Confirmed
        +	Assigned teller code: Null
        +	Approved by: Teller rejected
        +	User name: Null

> Cần kiểm tra lại với môi trường hiện thực

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".
- Giao dịch này không cho phép reverse.

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX | [DPT_GET_STOCK_PREFIX](Specification/Common/15 Deposit Rulefuncs) |