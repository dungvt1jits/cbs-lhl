# 1. Deposit account status

| Sr No. | Type of Status | JITS Account Status Types | Current Account | Savings Account | Fixed deposit | Definition | Status Nature |
|--------|----------------|---------------------------|-----------------|-----------------|---------------|------------|---------------|
| 1 | Deposit Account Status | Pending to approve | Yes | Yes | Yes | Open new | When account open, pending to approve status will change automatically |
| 2 | Deposit Account Status | New | Yes | Yes | Yes | After approve | After officer approved the account opened at with transaction "DPT_APR: Approve deposit account", "New" status will change automatically |
| 3 | Deposit Account Status | Normal | Yes | Yes | - | After credit, Credit (All channel), Debit (All channel) allowed | Normal status will change automatically when initial deposit credit to the account. All channel Debit and credit transaction can do |
| 4 | Deposit Account Status | Actived | - | - | Yes | After credit, Credit (All channel) allowed, Debit allowed when close Fixed deposit account | when teller open FD deposit account and deposit amount credit to FD account, activated status will change automatically. One time for credit and debit transaction allowed at OTC. |
| 5 | Deposit Account Status | Closed | Yes | Yes | Yes | Do not allow make transaction. Except change status from "Closed" to "Decease" with transaction "DPT_CAS: 11841: Change account status" | when teller close fixed deposit account (1190, 1191, 1193) "Closed" status will change automatically. |
| 6 | Deposit Account Status | Dormant | Yes | Yes | - | JITS team will perform follow request code `DPT-REQ-022` in GAP document (v1.4) #41 | "Dormant" status will change automatically after Dormant period time |
| 7 | Deposit Account Status | Block | Yes | Yes | - | Do not allow make transaction. Except change status | Teller change status from "Normal" to "Block" with transaction "DPT_BLK: 11840: Block account". Teller change status from "Block" to "Normal" with transaction "DPT_RLS: 11843: Release block account". |
| 8 | Deposit Account Status | Maturity | - | - | Yes | When Fixed deposit account is due, Credit do not allow, Debit allowed when close Fixed deposit account | "Maturity" status will change automatically when Fixed deposit account is due (Working date = End of tenor) |
| 9 | Deposit Account Status | Decease | Yes | Yes | Yes | Do not allow make transaction | Teller change status from "Closed" to "Decease" with transaction "DPT_CAS: 11841: Change account status" |

> Note: For "Dormant" status, JITS team will perform follow request code `DPT-REQ-022` in GAP document (v1.4) #41

# 2. Customer status

| Sr No. | Type of Status | JITS CIF Status Types | Definition | Status Nature |
|--------|----------------|-----------------------|------------|---------------|
| 1 | CIF Status | Pending Approval | Open new | Teller open new customer at "CTM-Customer Profile-Add" screen |
| 2 | CIF Status | Normal | After approve | Teller approve customer with transaction "CTM_APR: Approve customer" |
| 3 | CIF Status | Block | Do not allow to open account | Teller change status from "Normal" to "Block" or from "Block" to "Normal" with transaction "CTM_CAS: Change customer status". |
| 4 | CIF Status | Closed | Do not allow to open account, Do not allow to change status | Teller change status from "Normal" to "Closed" or "Block" to "Closed" with transaction "CTM_CAS: Change customer status". |
| 5 | CIF Status | Decease claim | Do not allow to open account, Do not allow to change status | Teller change status from "Normal" to "Decease claim"  or "Block" to "Decease claim" with transaction "CTM_CAS: Change customer status". |

> Note: For transaction "CTM_CAS: Change customer status", JITS team will perform follow request code `CTM-REQ-002` in GAP document (v1.4) #41

# 3. Stock status

| Sr No. | Type of Stock Status | JITS Stock Status Types | Definition | Status Nature |
|--------|----------------------|-------------------------|------------|---------------|
|  | Passbook |  |  |  |
| 1 | Passbook | Normal | Teller confirm received | Teller confirm received with transaction "DPT_CCR: 11834: Stock confirm received". |
| 2 | Passbook | Lost | Do not allow make transaction, except change status | Teller change status from "Normal" to "Lost" or from "Lost" to "Normal" with transaction "DPT_CTS: 11837: Change status of stock" |
| 3 | Passbook | Damage | Do not allow make transaction, except change status | Teller change status from "Normal" to "Damage" or from "Damage" to "Normal" with transaction "DPT_CTS: 11837: Change status of stock" |
| 4 | Passbook | Stop payment | Do not allow make transaction, except change status | JITS will hide this status |
| 5 | Passbook | Stop | Do not allow make transaction, except change status | Teller change status from "Normal" to "Stop" or from "Stop" to "Normal" with transaction "DPT_CTS: 11837: Change status of stock" |
|  | FD Receipt |  |  |  |
| 1 | FD Receipt | Normal | Teller confirm received | Teller confirm received with transaction "DPT_CCR: 11834: Stock confirm received". |
| 2 | FD Receipt | Lost | Do not allow make transaction, except change status | Teller change status from "Normal" to "Lost" or from "Lost" to "Normal" with transaction "DPT_CTS: 11837: Change status of stock" |
| 3 | FD Receipt | Damage | Do not allow make transaction, except change status | Teller change status from "Normal" to "Damage" or from "Damage" to "Normal" with transaction "DPT_CTS: 11837: Change status of stock" |
| 4 | FD Receipt | Stop payment | Do not allow make transaction, except change status | JITS will hide this status |
| 5 | FD Receipt | Stop | Do not allow make transaction, except change status | Teller change status from "Normal" to "Stop" or from "Stop" to "Normal" with transaction "DPT_CTS: 11837: Change status of stock" |
|  | Cheque |  |  |  |
| 1 | Cheque | Normal | Teller confirm received | Teller confirm received with transaction "DPT_CCR: 11834: Stock confirm received". |
| 2 | Cheque | Unpaid | After issued with transaction "DPT_CIS: 11801: Cheque book issued", allow make transaction | Teller issued with transaction "DPT_CIS: 11801: Cheque book issued" |
| 3 | Cheque | Paid | After withdrawal. Do not allow make transaction, `even` change status | Teller withdrawal with transaction "DPT_CWC: 1121: Cash withdrawal by cheque" or "DPT_CWM: 1125: Misellaneous debit by cheque" |
| 4 | Cheque | Earmark | Do not allow make transaction, except change status from "Earmark" to "Unpaid" with transaction "DPT_REC: 11852: Release hold balance for cheque" | Teller change status from "Unpaid" to "Earmark" with transaction "DPT_CEI: 11850: Issued hold balance for cheque". Teller change status from "Earmark" to "Unpaid" with transaction "DPT_REC: 11852: Release hold balance for cheque" |
| 5 | Cheque | Lost | Do not allow make transaction, except change status | Teller change status from "Unpaid" to "Lost" or from "Lost" to "Unpaid" with transaction "DPT_CTS: 11837: Change status of stock" | 
| 6 | Cheque | Damage | Do not allow make transaction, except change status | Teller change status from "Unpaid" to "Damage" or from "Damage" to "Unpaid" with transaction "DPT_CTS: 11837: Change status of stock" |
| 7 | Cheque | Stop | Do not allow make transaction, except change status | Teller change status from "Unpaid" to "Stop" or from "Stop" to "Unpaid" with transaction "DPT_CTS: 11837: Change status of stock" |
| 8 | Cheque | Cancelled | Do not allow make transaction, `even` change status | Teller change status from "Unpaid" to "Cancelled" with transaction "DPT_CTS: 11837: Change status of stock" |
|  | Cashier Cheque (Gift Cheque, Payment Order) |  |  |  |
| 1 | Cashier Cheque (Gift Cheque, Payment Order) | Normal | Teller confirm received | Teller confirm received with transaction "DPT_CCR: 11834: Stock confirm received". |
| 2 | Cashier Cheque (Gift Cheque, Payment Order) | Unpaid | After issued | Teller issued with transaction "DPT_CCI: 11806: Cashier cheque issued" |
| 3 | Cashier Cheque (Gift Cheque, Payment Order) | Paid | After withdrawal | Teller withdrawal with transaction "DPT_CCW: 11807: Cashier Cheque withdrawal" |
| 4 | Cashier Cheque (Gift Cheque, Payment Order) | Refund | After return | Teller return with transaction "DPT_RCC: 11808: Cashier cheque return" |

> Note: For Cashier Cheque (Gift Cheque, Payment Order), JITS team will perform follow request code `DPT-REQ-009` in GAP document (v1.4) #41