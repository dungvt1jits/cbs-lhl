# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_SAT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Stock type | stock_type | `Yes` | String | 1 | | | CSTKTP |
| 2 | From serial | from_serial | `Yes` | String | 20 | | | CFRSER |
| 3 | To serial | to_serial | `Yes` | String | 20 | | | CTOSER |
| 4 | Assigned staff code | assigned_staff_code | `Yes` | String | 5 | | | CTLID |
| 5 | Description | description | No | String | 250 | | | DESCS |
| 6 | Stock prefix | stock_prefix | `Yes` | String | 5 | | trường ẩn | CSTKPRE |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Stock type | stock_type | String | | |
| 3 | From serial | from_serial | String | | |
| 4 | To serial | to_serial | String | | |
| 5 | Assigned staff code | assigned_staff_code | String | | |
| 6 | Description | description | String | | |
| 7 | Stock prefix | stock_prefix | String | | |
| 8 | Transaction status | status | String |  |  |
| 9 | Transaction date | transaction_date | `Date time` |  |  |
| 10 | User id | user_id | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
-	Books exist in branch with:
    + Branch to Teller:
      +	Book status: In stock of HO/ In stock of Branch
      + Status of stock leaves: Normal
      + Confirm of status: Confirmed
    + Teller to Teller:
      +	Book status : Assigned to teller
      + Status of stock leaves: Normal
      + Confirm of status: Confirmed
-	Teller code:
    - Status is normal
    - Teller must same branch with stock and branch does this transaction

**Flow of events:**
-	Two kind assigned to Teller:
    +	Branch to teller.
    +	Teller to teller.
-	Allow assign many books at the same time to teller.
-	Transaction complete. Books assigned from:
    +	Branch to Teller: 
        +	Book status: Assigned to teller
        +	Confirm status: Pending confirm branch send to teller
        +	Assigned teller code: Assigned teller code
        +	User name: Name of assigned teller
    +	Teller to Teller:
        +	Confirm status: Pending confirm teller send to teller
        +	Assigned teller code: Assigned teller code
        +	User name: Name of assigned teller    

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller {} is invalid | Teller không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".
- Giao dịch này không cho phép reverse.

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- This transaction is not allowed to reverse.
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX | [DPT_GET_STOCK_PREFIX](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_INFO_TELLER | GET_INFO_USER_ACCOUNT | [GET_INFO_USER_ACCOUNT](Specification/Common/17 Admin Rulefuncs) |
| 3 | LKP_DATA_TLID | ADM_LOOKUP_USER_ACCOUNT_BY_BRANCHID | [ADM_LOOKUP_USER_ACCOUNT_BY_BRANCHID](Specification/Common/17 Admin Rulefuncs) |