# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_DMF`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | Cr account number | cr_account_number | `Yes` | String | 25 | | | PDOACC |
| 3 | Dormant pay | dormant_pay | `Yes` | `Number` | | 0 | | DMPAY |
| 4 | IFC code | ifc_code | `Yes` | `Number` | | | | CIFCCD |
| 5 | Description | description | `Yes` | String | 250 | | | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Cr account number | cr_account_number | String |  |  |
| 4 | Dormant pay | dormant_pay | `Number` |  |  |
| 5 | IFC code | ifc_code | `Number` |  |  |
| 6 | Description | description | String |  |  |
| 7 | Transaction status | status | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Deposit account type is “Saving”.
- Dormant period + Last transaction date < or = Working date
- Accounts have status "Normal".
- Current balance of account < Minimum dormant amount
- Both current balance and dormant amount are equal to zero (0).

**Flow of events:**
- Allow to setup parameter "Dormant period" on account.
- Change status "Dormant" on end of date.
- If accounts in period dormant, system will auto:
    - Change status "Dormant" 
    - Update dormant date = working date
- If account is "Dormant", allow withdraw amount or deposit amount.
- "Dormant" status just signals to collect fee following rule bank. To collect dormant fee, System supports to setup for calculation fee in IFC module.
- Calculation dormant fee:
    - Dormant fee is calculated when each time account has dormant status and generate dormant fee every monthly and collect dormant fee in the end of month.
    - Dormant fee has not paid will plus into [Outstanding] of IFC list tag of deposit account.
    - IFC information: Fee – Dormant fee
- Collection dormant fee:
    - Collection the fee from itself deposit account.
    - Time of charging: Collection fee on working date at end of each month.
    - Collection from amount of [Available balance] of deposit account.
    - Allow collection even in case [Available balance] amount less than dormant fee amount ([Outstanding] amount in IFC list).
    - Example: 
        - Dormant fee: 2000 USD, [Available Balance] is 500 USD, the system will collect 500 USD
        - When IFC list record follow:
        - [Paid amount] field: 500 USD
        - [Outstanding] field: 1500 USD
- Close deposit account has account status is dormant:
    - Validation:
        - Account status is dormant.
        - Amount of [current balance] is zero (0).
        - No debt other payment (this payment is difference dormant fee)
    - The order of step:
        - Step 1: collection fee (new step)
        - Step 2: auto close account (new step)
        - Step n (before change working date): Convert GL to base currency in EOM, YOY (Current step)
    - Only processing with account depend collection fee cycle.
    - In the case of only partial collection or collection dormant fee is fail, before closing account, the system will change amount of dormant fee have not obtained Dormant fee to zero (0)

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| Collection dormant fee |  |  |  |  |
| 1 | D | DEPOSIT | Amount dormant fee is collected | Currency of account |
| 1 | C | IFCC (income) | Amount dormant fee is collected | Currency of account |
| Automatically closed, in case interest accrued > 0.00 |  |  |  |  |
| 1 | D | INTEREST | Interest accrued | Currency of account |
| 1 | C | REVERT_INTEREST | Interest accrued | Currency of account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |