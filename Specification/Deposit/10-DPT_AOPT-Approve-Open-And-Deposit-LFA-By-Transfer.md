# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | | `Yes` | String | 25 | | |
| 2 | Customer type | | `Yes` | String | 1 | | |
| 3 | Customer code | | `Yes` | String | 15 | | |
| 4 | Master FD account | | `Yes` | String | 25 | | |
| 5 | Catalogue code | | `Yes` | String | 25 | | |
| 6 | Catalogue name | | `Yes` | String | 100 | | |
| 7 | Deposit type | | `Yes` | String | 1 | | |
| 8 | Account type | | `Yes` | String | 2 | | |
| 9 | Account sub type | | `Yes` | String | 1 | | |
| 10 | Debit from account | | `Yes` | String | 25 | | |
| 11 | Deposit amount | | `Yes` | `Number` | | 0 | số có hai số thập phân |
| 12 | Seq number | | No | `Number` | 6 | 0 | cho phép `null` |
| 13 | Description | | No | String | | 250 | |
| 14 | Account holder name | | `Yes` | String | 250 | | |
| 15 | Branch id | | `Yes` | String | | | Branch login |
| 16 | Working date | | `Yes` | `Date` | | | |
| 17 | User id | | `Yes` | String | | | User login |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references | | String | | | |
| 2 | Account number def | DEFACNO | String | 36 | | |
| 3 | Account number | | String | 15 | | |
| 4 | Customer type | | String | | | |
| 5 | Customer code | | String | 8 | | |
| 6 | Master FD account | | String | 15 | | |
| 7 | Catalogue code | | String | 8 | | |
| 8 | Catalogue name | | String | | | |
| 9 | Deposit type | | String | | | |
| 10 | Account type | | String | | | |
| 11 | Account sub type | | String | | | |
| 12 | Debit from account | | String | 15 | | |
| 13 | Deposit amount | | `Number` | | | số có hai số thập phân |
| 14 | Seq number | | `Number` | | | |
| 15 | Description | | String | | | |
| 1 | Transaction references | | String | | | |
| 2 | Account number def | DEFACNO | String | 36 | | |
| 3 | Account number | | String | 15 | | |
| 4 | Customer type | | String | | | |
| 5 | Customer code | | String | 8 | | |
| 6 | Master FD account | | String | 15 | | |
| 7 | Catalogue code | | String | 8 | | |
| 8 | Catalogue name | | String | | | |
| 9 | Deposit type | | String | | | |
| 10 | Account type | | String | | | |
| 11 | Account sub type | | String | | | |
| 12 | Debit from account | | String | 15 | | |
| 13 | Deposit amount | | `Number` | | | số có hai số thập phân |
| 14 | Seq number | | String | | | |
| 15 | Description | | String | | | |
| 16 | Account holder name | | String | | | |
| 17 | Branch id | | String | | | Branch login |
| 18 | Account status | | String | | | |
| 19 | Begin of tenor | | `Date` | | | Working date |
| 20 | End of tenor | | `Date` | | | [begin of tenor] + kỳ hạn catalogue |
| 21 | Current balance (TK gửi) | | `Number` | | | số có hai số thập phân |
| 22 | Available balance (TK gửi) | | `Number` | | | số có hai số thập phân |
| 23 | Withdraw amount |  | `Number` | | | số có hai số thập phân |
| 24 | Week debit | | `Number` | | | số có hai số thập phân |
| 25 | Month debit | | `Number` | | | số có hai số thập phân |
| 26 | Quarter debit | | `Number` | | | số có hai số thập phân |
| 27 | Semi-annual debit | | `Number` | | | số có hai số thập phân |
| 28 | Year debit | | `Number` | | | số có hai số thập phân |
| 29 | Current balance (TK nhận) | | `Number` | | | số có hai số thập phân |
| 30 | Available balance (TK nhận) | | `Number` | | | số có hai số thập phân |
| 31 | Deposit amount |  | `Number` | | | số có hai số thập phân |
| 32 | Week credit | | `Number` | | | số có hai số thập phân |
| 33 | Month credit | | `Number` | | | số có hai số thập phân |
| 34 | Quarter credit | | `Number` | | | số có hai số thập phân |
| 35 | Semi-annual credit | | `Number` | | | số có hai số thập phân |
| 36 | Year credit | | `Number` | | | số có hai số thập phân |
| 37 | Posting data | | JSON Object | | | |
| | Group | | `Number` | | | |
| | Index in group | | `Number` | | | |
| | Posting side | | String | | | |
| | System account name | | String | | | |
| | GL account number | | String | | | |
| | Amount | | `Number` | | | số có hai số thập phân |
| | Currency | | String | 3 | | |
| 38 | Transaction status | | String | | | |

## 1.2 Transaction flow
**Conditions:**

-	"Link Fixed Deposit account": exists with status "Pending to approve".

**Flow of events:**
-	Change status of "Link Fixed Deposit account":
    +	Change status to "Actived" (if allow deposit: only one time).
    +	Change status to "Normal" (if allow deposit: multi-time).
-	Debit account will decrease amount.

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT (transfer) | Amount deposit | Currency of account |
| 1 | C | DEPOSIT (LFA) | Amount deposit | Currency of account |

**Cập nhật thông tin deposit account cho tài khoản gửi tiền:**
[Tham khảo thông tin chi tiết tại mục 5](Specification/Common/06 Transaction Flow Deposit)<br>

**Cập nhật thông tin deposit account cho tài khoản nhận tiền:**
[Tham khảo thông tin chi tiết tại mục 3](Specification/Common/06 Transaction Flow Deposit)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Deposit type is invalid | Deposit type không hợp lệ |
|  | Account type is invalid | Account type không hợp lệ |
|  | Balance has not enough | Balance của tài khoản gửi không đủ |

# 2. Reverse
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |