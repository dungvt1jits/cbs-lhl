# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_MUL`

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Posting data | posting_data | `Yes` | JSON Object |  |  |  |
|  | Posting side | posting_side | `Yes` | String | 1 |  |  |
|  | GL account number | gl_account_number | `Yes` | String | 25 |  |  |
|  | Currency | currency | `Yes` | String | 3 |  |  |
|  | Credit amount | credit_amount | `Yes` | `Number` |  | > 0 | số có 2 thập phân |
|  | Debit amount | debit_amount | `Yes` | `Number` |  | > 0 | số có 2 thập phân |
|  | Cost center | cost_center | `Yes` | String | 25 |  |  |
|  | Product code | product_code | `Yes` | String | 25 |  |  |
|  | Inter-company | inter_company | `Yes` | String | 25 |  |  |
| 2 | Value date | value_date | `Yes` | `Date time` |  |  |  |
| 3 | Reference document no. | reference_document_no | No | String | 25 |  |  |
| 4 | Customer ID | customer_id | No | String | 15 |  |  |
| 5 | Customer Account ID | customer_account_id | No | String | 25 |  |  |
| 6 | User defined 4 | user_defined4 | No | String | 25 |  |  |
| 7 | User defined 5 | user_defined5 | No | String | 25 |  |  |
| 8 | Description | description | No | String |  | 250 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Posting data | posting_data | JSON Object |  |  |
|  | Posting side | posting_side | String |  |  |
|  | GL account number | gl_account_number | String |  |  |
|  | Currency | currency | String |  |  |
|  | Amount credit | credit_amount | `Number` |  | số có 2 thập phân |
|  | Amount debit | debit_amount | `Number` |  | số có 2 thập phân |
|  | Cost center | cost_center | String |  |  |
|  | Product code | product_code | String |  |  |
|  | Inter-company | inter_company | String |  |  |
| 3 | Value date | value_date | `Date time` |  |  |
| 4 | Reference document no. | reference_document_no | String |  |  |
| 5 | Customer ID | customer_id | String |  |  |
| 6 | Customer Account ID | customer_account_id | String |  |  |
| 7 | User defined 4 | user_defined4 | String |  |  |
| 8 | User defined 5 | user_defined5 | String |  |  |
| 9 | Description | description | String |  |  |
| 10 | Transaction date | transaction_date | `Date time` |  |  |
| 11 | User id | user_id | `Number` |  |  |
| 12 | Transaction status | status | String |  |  |
| 13 | Posting data | posting_data_reponse | JSON Object |  |  |
|  | Group | group | `Number` |  | số nguyên dương |
|  | Index in group | index_in_group | `Number` |  | số nguyên dương |
|  | Posting side | posting_size | String |  | D or C |
|  | Account number | gl_account_number | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Currency | currency | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Total amount of debit must be equal total amount of credit amount, and currency of each entry must be same.

**Flow of events:**
- This part shows the brief description of some information of accounting transaction:
    - Multi posting entry: Allow teller to key in multi posting entry to internal account meet posting rule: with one currency, only [0-1]-n or n-[0-1] Debit-Credit are allow, n-n Debit Credit are not allow. For example, 0-n Debit Credit posting is:
        - Credit Account: account 1 with amount -1000
        - Credit Account: account 2 with amount 1000.
    - In balance sheet and off-balance sheet (memorandum) posting are allow together. Teller can key in only one posting entry of off-balance sheet with one currency into transaction.
    - Allow transaction have multi-currency on it.

- For example, the most complicated transaction of posting as follow:
    - Entry 1:
        - Debit: Account 1 LAK 1,400
        - Credit: Account 2 LAK 1,200
        - Credit: Account 3 LAK 200
    - Entry 2:
        - Debit: Account 4 USD 500
        - Credit: Account 5 USD 500
    - Entry 3:
        - Debit: Account 6 EUR 780
        - Credit: Account 7 EUR 700
        - Credit: Account 8 EUR 80
    - Entry 4:
        - Debit: Off balance sheet account 9 LAK 500
        - Credit: Off balance sheet account 10 LAK 500
    - Entry 5: 
        - Debit: Off balance sheet account 11 USD 20
        - Credit: Off balance sheet account 12 USD 20
    - ….
    - Entry n
    - Total number of accounting entry can be up to 20 entries.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | GL (Debit) | Debit amount | Currency of GL |
| 1 | C | GL (Credit) | Credit amount | Currency of GL |

*Note: Tùy thuộc vào thông tin đầu vào.*

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PACNOS | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/20 FX Rulefuncs) |
| 2 | LKP_DATA_PGACC_ACT | ACT_ACCHRT_LOOKUP_BY_BRANCHID | [ACT_ACCHRT_LOOKUP_BY_BRANCHID](Specification/Common/20 FX Rulefuncs) |