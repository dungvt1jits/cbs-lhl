# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_DIP`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Deposit account | deposit_account | `Yes` | String | 25 | | | PDACC |
| 2 | Interest accrual | interest_accrual | No | `Number` | | 0 | trường ẩn | CINT  |
| 3 | Interest prepaid | interest_prepaid | No | `Number` | | 0 | trường ẩn | CIPRE |
| 4 | Interest payable/receivable | interest_payable_receivable | No | `Number` | | 0 | | CIPBL |
| 5 | Interest due | interest_due | No | `Number` | | 0 | | CIDUE |
| 6 | Interest overdue | interest_overdue | No | `Number` | | 0 | | CIOVD |
| 7 | Interest suspense | interest_suspense | No | `Number` | | 0 | | CISPS |
| 8 | Interest not paid | interest_not_paid | No | `Number` | | 0 | | CINOTPAID |
| 9 | Interest amount | interest_amount | `Yes` | `Number` | | 0 | | CTCVT |
| 10 | Cross interest amount | cross_interest_amount | No | `Number` | | <> 0 | | TXAMT |
| 11 | Withholding tax accrual actural | withholding_tax_accrual_actural | No | `Number` | | 0 | trường ẩn | WHTAMT    |
| 12 | Withholding tax amount calc | withholding_tax_amount_calc | No | `Number` | | 0 | | CWHTA |
| 13 | Withholding tax accrued | withholding_tax_accrued | No | `Number` | | 0 | | CFAMT |
| 14 | Paid to this deposit account | paid_to_this_deposit_account | `Yes` | String | 25 | | | PDOACC    |
| 15 | Deposit other currency | deposit_other_currency | `Yes` | String | 3 | | trường ẩn | CTXCCR    |
| 16 | Exchange rate | exchange_rate | `Yes` | `Number` | | 0 | trường ẩn | CTXEXR    |
| 17 | Amount equivalent in BCY | amount_equivalent_in_bcy | `Yes` | `Number` | | 0 | trường ẩn | PDOCVT    |
| 18 | Depositor name | depositor_name | No | String | 250 | | | CACNM |
| 19 | Depositor id | depositor_id | No | String | 15 | | | CCTMCD    |
| 20 | Depositor address | depositor_address | No | String | 250 | | | CCTMA |
| 21 | Depositor description | depositor_description | No | JSON Object | | | | MDESC |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 22 | Deposit currency | deposit_currency | No | String | 3 | | trường ẩn | CCCR  |
| 23 | Fee VAT | fee_vat | No | `Number` | | 0 | trường ẩn | CVAT  |
| 24 | Values date | values_date | No | `Date time` | | working date | trường ẩn | CVLDT |
| 25 | Description | description | No | String | 250 | | | DESCS |
| 26 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | | |
| 3 | Deposit account | deposit_account | String | | | |
| 4 | Interest accrual | interest_accrual | `Number` | | trường ẩn |
| 5 | Interest prepaid | interest_prepaid | `Number` | | trường ẩn |
| 6 | Interest payable/receivable | interest_payable_receivable | `Number` | | |
| 7 | Interest due | interest_due | `Number` | | |
| 8 | Interest overdue | interest_overdue | `Number` | | |
| 9 | Interest suspense | interest_suspense | `Number` | | |
| 10 | Interest not paid | interest_not_paid | `Number` | | |
| 11 | Interest amount | interest_amount | `Number` | | |
| 12 | Cross interest amount | cross_interest_amount | `Number` | | |
| 13 | Withholding tax accrual actural | withholding_tax_accrual_actural | `Number` | | trường ẩn |
| 14 | Withholding tax amount calc | withholding_tax_amount_calc | `Number` | | |
| 15 | Withholding tax accrued | withholding_tax_accrued | `Number` | | |
| 16 | Paid to this deposit account | paid_to_this_deposit_account | String | | |
| 17 | Deposit other currency | deposit_other_currency | String | | trường ẩn |
| 18 | Exchange rate | exchange_rate | `Number` | | trường ẩn |
| 19 | Amount equivalent in BCY | amount_equivalent_in_bcy | `Number` | | trường ẩn |
| 20 | Depositor name | depositor_name | String | | |
| 21 | Depositor id | depositor_id | String | | |
| 22 | Depositor address | depositor_address | String | | |
| 23 | Depositor description | depositor_description | JSON Object | | |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 24 | Deposit currency | deposit_currency | String | | trường ẩn |
| 25 | Fee VAT | fee_vat | `Number` | | trường ẩn |
| 26 | Values date | values_date | `Date time` | | trường ẩn |
| 27 | Description | description | String | | |
| 40 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | |
| 41 | Transaction status | status | String | | |
| 42 | Transaction date | transaction_date | `Date time` |  |  |
| 43 | User id | user_id | `Number` |  |  |
| 44 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
**Conditions:**
-	Account number: exists with status {"Normal", "Dormant", "Maturity"}.
    +	Account has interest amount.
    +   0 < Cross interest amount <= Interest payable/receivable + Interest due + Interest not paid + Interest overdue
-	Account receive: exists with status {"Normal", "Dormant"}. Type is current or saving. 
-	Currency of both accounts must be same.
-   If deposit account/ paid to this deposit account has status is "Dormant", system will show popup approve

**Flow of events:**
-	If account is type "fixed deposit", just allows repayment interest due. If withdrawal before maturity:
    +	In case customer withdraws before 3 months, no interest received or calculate interest based on defined rate, reverse accrued interest (not paid), reverse withholding tax.
    +	In case term is over 3 months and 3 months < deposit period < term: Calculate based on saving interest rate and deduct withholding tax.
-	Record repayment interest:  Decrease interest amount.
-	~~If before transaction completed, account has status "Dormant". System will auto change status "Normal"~~ (KTB thay đổi nghiệp vụ không cho thực hiện giao dịch với tài khoản có status là Dormant).

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST | Amount repay | Currency of account |
| 1 | C | DEPOSIT | Amount repay | Currency of account |

**Cập nhật thông tin deposit account thu lãi:**
[Tham khảo thông tin chi tiết tại mục 6](Specification/Common/06 Transaction Flow Deposit)<br>

**Cập nhật thông tin deposit account nhận lãi:**
[Tham khảo thông tin chi tiết tại mục 4](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A204`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Deposit type is invalid | Deposit type không hợp lệ |
|  | Account type is invalid | Account type không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_INT | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_CUSTOMER | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_INFO_DEF_INT_PAID_TOCUS | DPT_GET_REDEMPTION_INTEREST | [DPT_GET_REDEMPTION_INTEREST](Specification/Common/15 Deposit Rulefuncs) |
| 6 | GET_INFO_WHTAMT | DPT_GET_INFO_WHTAMT | [DPT_GET_INFO_WHTAMT](Specification/Common/15 Deposit Rulefuncs) |
| 7 | CCL_NUMB_CWHTA | DPT_GET_INTEREST_WITHOUT_WHTAX | [DPT_GET_INTEREST_WITHOUT_WHTAX](Specification/Common/15 Deposit Rulefuncs) |
| 8 | GET_INFO_OTHER_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>