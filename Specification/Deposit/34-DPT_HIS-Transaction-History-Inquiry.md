# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_HIS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | From date | from_date | `Yes` | `Date time` | | Working date | | FROMDT |
| 3 | To date | to_date | `Yes` | `Date time` | | Working date | | TODT |
| 4 | Description | description | No | String | 250 | | | DESCS |
| 5 | Branch name | branch_name | No | String | | | | `gửi thêm` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | 15 | |
| 3 | From date | from_date | `Date time` | | |
| 4 | To date | to_date | `Date time` | | |
| 5 | Description | description | String | | |
| 6 | Transaction history data | dataset | Array Object | | |
| | Transaction code | 0 | String | | |
| | Transaction number | 1 | String | | |
| | Transaction date | 2 | `Date time` | | |
| | Created by | 3 | String | | trả về code của cột UserId từ Admin |
| | Debit | 4 | `Number` | | |
| | Credit | 5 | `Number` | | |
| | Balance | 6 | `Number` | | |
| | Description | 7 | String | | |
| 7 | Transaction status | status | String | | |
| 8 | Transaction date |  | `Date time` |  |  |
| 9 | User id |  | `Number` |  |  |
| 10 | Branch name | branch_name | No | String | | | `gửi thêm` | |

## 1.2 Transaction flow
**Conditions:**
-	Account number is existing in the system.
-   Allow see backward 366 days or more. It depends on setup parameter (DPT_HIS_DAYS).

**Flow of events:**
-	Get back one table data with information in detail below: "Transaction code", "Transaction number", "Transaction date", "Created by", "Amount", "Balance", "Description" and "Valued date".

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |