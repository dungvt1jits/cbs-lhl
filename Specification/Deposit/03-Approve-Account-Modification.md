# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/ApproveDepositAccount/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_SEARCH_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    "search_text": "",
    "page_size": 10,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Transaction id | id | `Number` |  |  |
| 2 | Account number def | account_number_def | String |  |  |
| 3 | Account number | account_number | String | 15 |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Currency code | currency_code | String | 3 |  |
| 6 | Customer code | customer_code | String | 8 |  |
| 7 | Customer type | customer_type |  String |  |  |
| 8 | Catalogue code | catalog_code | String | 8 |  |
| 9 | Deposit status | deposit_status | String |  |  |
| 10 | Deposit type | deposit_type | String |  |  |
| 11 | Old a/c no | reference_number | String |  |  |
| 12 | Pending approve | approve_status | String |  |  |

```json
{
    
}
```

## 1.2 Transaction flow
- Chỉ hiển thị các thông tin có approve_status = W or approve_status = I.

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/ApproveDepositAccount/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_ADSEARCH_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String | 15 |  |  |
| 2 | Account name | account_name | No | String |  |  |  |
| 3 | Currency code | currency_code | No | String | 3 |  |  |
| 4 | Customer code | customer_code | No | String | 8 |  |  |
| 5 | Customer type | customer_type | No | String |  |  |  |
| 6 | Catalogue code | catalog_code | No | String | 8 |  |  |
| 7 | Deposit status | deposit_status | No | String |  |  |  |
| 8 | Deposit type | deposit_type | No | String |  |  |  |
| 9 | Old a/c no | reference_number | No | String |  |  |  |
| 10 | Pending approve | approve_status | No | String |  |  |  |
| 11 | Page index | page_index | No | `Number` |  |  |  |
| 12 | Page size | page_size | No | `Number` |  |  |  |

```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Transaction id | id | `Number` |  |  |
| 2 | Account number def | account_number_def | String |  |  |
| 3 | Account number | account_number | String | 15 |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Currency code | currency_code | String | 3 |  |
| 6 | Customer code | customer_code | String | 8 |  |
| 7 | Customer type | customer_type |  String |  |  |
| 8 | Catalogue code | catalog_code | String | 8 |  |
| 9 | Deposit status | deposit_status | String |  |  |
| 10 | Deposit type | deposit_type | String |  |  |
| 11 | Old a/c no | reference_number | String |  |  |
| 12 | Pending approve | approve_status | String |  |  |

```json
{
    
}
```

## 2.2 Transaction flow
- Chỉ hiển thị các thông tin có approve_status = W or approve_status = I.

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. View
- [View by account number def của "Account Information"](Specification/Deposit/02 Account Information)

# 4. View user modify
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_GET_INFO_USER_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |

```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User name | user_name | String |  |  |
| 2 | Transaction reference id | tx_reference_id | String |  |  |
| 3 | Notification | notification | String |  |  |
| 4 | Approve status | is_approve | String |  |  |
| 5 | Transaction date | trans_date | `Date time` |  |  |

```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. View modify account information
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_VIEW_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |
| 2 | Trans reference id | tx_reference_id | `Yes` | String |  |  |  |
| 3 | Module code | module_code | `Yes` | String |  |  | `DPT` |

```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account name modify | account_name | String |  |  |
| 2 | Business purpose code modify | business_purpose_code | String |  |  |
| 3 | Minimum deposit amount modify | minimum_deposit_amount | `Number` |  | số có hai số thập phân |
| 4 | Initial deposit amount modify | initial_deposit_amount | `Number` |  | số có năm số thập phân |
| 5 | Interest tenor modify | interest_tenor | `Number` |  | số nguyên dương |
| 6 | Interest tenor unit modify | interest_tenor_unit | String |  |  |
| 7 | Minimum tenor modify | minimum_tenor | `Number` |  | số nguyên dương |
| 8 | Minimum tenor unit modify | minimum_tenor_unit | String |  |  |
| 9 | Multiple deposit allow modify | multiple_deposit_allow | String |  |  |
| 10 | Multiple withdrawal allow modify | multiple_withdrawal_allow | String |  |  |
| 11 | Early withdrawal (Y/N) modify | early_withdrawal | String |  |  |
| 12 | Minimum tenor allow early withdrawal modify | minimum_tenor_allow_early_withdrawal | `Number` |  | số nguyên dương |
| 13 | Minimum tenor allow early withdrawal unit modify | minimum_tenor_allow_early_withdrawal_unit | String |  |  |
| 14 | Credit interest (Y/N) modify | credit_interest | String |  |  |
| 15 | Credit interest tenor modify | credit_interest_tenor | `Number` |  | số nguyên dương |
| 16 | Credit interest tenor unit modify | credit_interest_tenor_unit | String |  |  |
| 17 | The day of tenor for crediting interest modify | crediting_interest | `Number` |  | số nguyên dương |
| 18 | Dormant period modify | dormant_period | `Number` |  | số nguyên dương |
| 19 | Dormant period unit modify | dormant_period_unit | String |  |  |
| 20 | Rollover option modify | rollover | String |  |  |
| 21 | Rollover to catalogue modify | rollover_to_catalog | No | String |  |  |
| 22 | Interest due on holiday modify | interest_due_on_holiday | No | `Number` |  |  |
| 23 | Principal due on holiday modify | principal_due_on_holiday | No | `Number` |  |  |
| 24 | Statement tenor modify | statement_tenor | No | String |  |  |
| 25 | Statement tenor unit modify | statement_tenor_unit | No | `Number` |  |  |
| 26 | Statement format modify | statement_format | No | String |  |  |
| 27 | IFC data |  | Array Object |  |  |
|  | IFC id | id | `Number` |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | IFC name | ifc_name | String |  |  |
|  | Base value | value_base | String |  |  |
|  | Is linked | is_linked | String |  |  |  |
|  | IFC value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Margin value | margin_value | `Number` |  | số có 5 số thập phân |
|  | Status | ifc_status | String |  |  |
|  | Outstanding | amount | `Number` |  | số có 5 số thập phân |
|  | Paid | paid | `Number` |  | số có 5 số thập phân |
|  | Amount payable | amtpbl | `Number` |  | số có 5 số thập phân |

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 6. Approve modify
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_APPROVE_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |
| 2 | Trans reference id | tx_reference_id | `Yes` | String |  |  |  |
| 3 | Module code | module_code | `Yes` | String |  |  | `DPT` |

```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Trans reference id | tx_reference_id | String |  |  |

```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 7. Reject modify
## 7.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_REJECT_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |
| 2 | Trans reference id | tx_reference_id | `Yes` | String |  |  |  |
| 3 | Module code | module_code | `Yes` | String |  |  | `DPT` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Trans reference id | tx_reference_id | String |  |  |

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |