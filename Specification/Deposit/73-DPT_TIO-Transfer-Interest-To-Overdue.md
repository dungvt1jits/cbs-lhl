# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_TIO`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | System account number | system_account_number | `Yes` | String | 36 | | | PDOACC |
| 3 | Due interest | due_interest | `Yes` | `Number` | | 0 | | CIDUE |
| 4 | Description | description | `Yes` | String | 250 | | | MDESC |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |  |
| 2 | Account number | account_number | String |  |  |  |
| 3 | System account number | system_account_number | String |  |  |  |
| 4 | Due interest | due_interest | `Number` |  |  |  |
| 5 | Description | description | String |  |  |  |
| 6 | Transaction status | status | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Accounts has interest due (interest due > 0.00). And status <> "Closed".
- Last date system transfer interest to due < working date

**Flow of events:**
- ???

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |