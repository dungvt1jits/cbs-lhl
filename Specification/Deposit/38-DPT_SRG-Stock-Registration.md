# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_SRG`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Module | module | `Yes` | String | 3 |  |  | C_MODULE |
| 2 | Stock type | stock_type | `Yes` | String | 1 |  |  | CSTKTP |
| 3 | From serial | from_serial | `Yes` | String | 20 |  |  | CFRSER |
| 4 | To serial | to_serial | `Yes` | String | 20 |  |  | CTOSER |
| 5 | Stock prefix | stock_prefix | `Yes` | String | 5 |  |  | CSTKPRE |
| 6 | Number of leaves | no_of_leaves | `Yes` | `Number` |  | 0 |  | CAMT1 |
| 7 | Number of book | no_of_books | `Yes` | `Number` |  | 0 |  | C_UKN_AMT2 |
| 8 | Description | description | No | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Module | module | String |  |  |
| 3 | Stock type | stock_type | String |  |  |
| 4 | From serial | from_serial | String |  |  |
| 5 | To serial | to_serial | String |  |  |
| 6 | Stock prefix | stock_prefix | String |  |  |
| 7 | Number of leaves | no_of_leaves | `Number` |  |  |
| 8 | Number of book | no_of_books | `Number` |  |  |
| 9 | Description | description | String |  |  |
| 10 | Transaction status | status | String |  |  |
| 11 | Transaction date | transaction_date | `Date time` |  |  |
| 12 | User id | user_id | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
-	"Prefix", "Serial no" of each type: not exists is system.
-	Unique when register: Prefix + Serial no + Type (cheque, passbook, receipt).
-	Number of leaves in a book of passbook, receipt, cashier cheque always is = 1.
-   Stock prefix of from serial and to serial must be the same

**Flow of events:**
-	Support register "serial no: From … To…
-	Allow to define number leaves on a book (cheque). System will split number book:
    +	Number books = total leaves / number leaves of a book.
    +	Total leaves = "To serial" - "From serial" + 1.
-	Transaction complete, system will create number of stocks (= number of books) in list stock to manage. Stock is in branch: 
    +	Book status: In stock of branch
    +	Stock leaves status: Normal
    +	Confirm status: Confirmed
    +   Branch code: Branch opened
    +   Created by: User opened
    +   Assigned teller code: Null
    +   Approved by: Null
    +   User name: Null

For example:
- From serial: CQ-5000000001
- To serial: CQ-5000000010
- Number of leaves = 2
- Number of book = 5

=> Number of stocks:
  - From serial: CQ-5000000001 - To serial: CQ-5000000002
  - From serial: CQ-5000000003 - To serial: CQ-5000000004
  - From serial: CQ-5000000005 - To serial: CQ-5000000006
  - From serial: CQ-5000000007 - To serial: CQ-5000000008
  - From serial: CQ-5000000009 - To serial: CQ-5000000010

> Cần kiểm tra lại với môi trường hiện thực

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".
- Giao dịch này không cho phép reverse.

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- This transaction is not allowed to reverse.
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX | [DPT_GET_STOCK_PREFIX](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_INFO_NO_LEAF | DPT_SRG_GET_INFO_NO_LEAF | [DPT_SRG_GET_INFO_NO_LEAF](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_NO_BOOK | DPT_SRG_GET_INFO_NO_BOOK | [DPT_SRG_GET_INFO_NO_BOOK](Specification/Common/15 Deposit Rulefuncs) |