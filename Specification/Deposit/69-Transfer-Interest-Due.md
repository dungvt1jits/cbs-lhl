# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_TID`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | Interest amount | interest_amount | `Yes` | `Number` | | 0 | | PCSAMT |
| 3 | Tax amount | tax_amount | `Yes` | `Number` | | 0 | | CWHTA |
| 4 | Description | description | `Yes` | String | 250 | | | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |  |
| 2 | Account number | account_number | String | 25 |  |  |  |
| 3 | Interest amount | interest_amount | `Number` |  |  |  |  |
| 4 | Tax amount | tax_amount | `Number` |  |  |  |  |
| 5 | Description | description | String |  |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Accounts has interest accrual. And status <> "Closed".
- Last date system transfer interest to due + tenor = working date

**Flow of events:**

- Accounts allow repayment interest amount:  monthly, or quarterly, or yearly.
- FD accounts allow repayment interest amount monthly, but term can be many months.
- On end of date: 25-June, 25-December yearly, DD accounts transfer interest accrual to interest due and plus interest into Principal.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |