# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_APR`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | Account holder name | account_holder_name | `Yes` | String | 250 | | | CACNM |
| 3 | Description | description | `Yes` | String | 250 | | | DESCS |
| 4 | Branch name | branch_name | No | String | | | `gửi thêm` | |
| 5 | Catalogue code | catalog_code | No | String | 50 |  |  | `Gửi thêm` |
| 6 | Customer segmentation | account_type | No | String | 250 |  |  | `Gửi thêm` |
| 7 | Catalogue name | catalog_name | No | String | 100 |  |  | `Gửi thêm` |
| 8 | Deposit type | deposit_type | No | String | 100 |  |  | `Gửi thêm` |
| 9 | Deposit sub type | deposit_sub_type | No | String | 100 |  |  | `Gửi thêm` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | 15 | |
| 3 | Account holder name | account_name | String | | |
| 4 | Description | description | String | | |
| 5 | Transaction status | status | String | | |
| 6 | Transaction date | transaction_date | `Date time` |  |  |
| 7 | User id | user_id | String |  |  |
| 8 | Branch name | branch_name | String | | `trả thêm` |
| 9 | Catalogue code | catalog_code | String |  | `trả thêm` |
| 10 | Customer segmentation | account_type | String |  | `trả thêm` |
| 11 | Catalogue name | catalog_name | String |  | `trả thêm` |
| 12 | Deposit type | deposit_type | String |  | `trả thêm` |
| 13 | Deposit sub type | deposit_sub_type | String |  | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Account number: exists with status "Pending to approve"

**Flow of events:**
-	Change status of Deposit account to New

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại đây](Specification/Common/06 Transaction Flow Deposit)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 |  | GET_INFO_DPTCAT | [GET_INFO_DPTCAT](Specification/Common/15 Deposit Rulefuncs) |
