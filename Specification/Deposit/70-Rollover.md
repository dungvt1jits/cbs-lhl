# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Accounts has status "Maturity".
- Type roll-over: "Roll-over principal only" or "Roll-over principal plus interest"

**Flow of events:**
- Case rollover is "Roll-over principal only": 
    - Continue to calculate interest based on Principal 
        - Principal = current balance.
    - Interest amount keep or paid to other DD account.
    - Update "Begin tenor" and "End tenor".
    - Allow auto change other catalogue (interest rate, tenor). 
        - Example: change "term 6 months" to "term 3 months".
- Case rollover is "Roll-over principal plus interest": 
    - Continue to calculate interest based on Principal:
        - Principal = current balance + interest amount - withholding tax amount
    - Interest amount = 0.
    - Update "Begin tenor" and "End tenor".
    - Allow auto change other catalogue (interest rate, tenor). 
- Case rollover is "no roll-over": 
    - Stop calculation interest.
    - Interest amount keep.
    - No update "Begin tenor" and "End tenor".

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

-	Case: plus interest to principal

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST | Interest amount - withholding tax amount | Currency of deposit account |
| 1 | C | DEPOSIT | Interest amount - withholding tax amount | Currency of deposit account |
| 2 | D | INTEREST | Withholding tax amount | Currency of deposit account |
| 2 | C | PAID_WHT | Withholding tax amount | Currency of deposit account |

-	Case: paid interest to another account

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST | Interest amount - withholding tax amount | Currency of deposit account |
| 1 | C | DEPOSIT (receive account) | Interest amount - withholding tax amount | Currency of deposit account |
| 2 | D | INTEREST | Withholding tax amount | Currency of deposit account |
| 2 | C | PAID_WHT | Withholding tax amount | Currency of deposit account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |