# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CWR`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | PDACC  |
| 2 | Current balance | current_balance | No | `Number` |  | 0 |  | CAMT2  |
| 3 | Avaiable balance | available_balance | No | `Number` |  | 0 |  | CAMT3  |
| 4 | Withdraw amount | withdraw_amount | `Yes` | `Number` |  | >0 |  | PCSAMT |
| 5 | Cash currency | cash_currency | `Yes` | String | 3 |  | trường ẩn | PCSCCR |
| 6 | Cross rate | cross_rate | `Yes` | `Number` |  | 0 | trường ẩn | CCRRATE  | |
| 7 | Cash amount | cash_amount | No | `Number` |  | 0 | trường ẩn | CTCVT  |
| 8 | Exchange rate | exchange_rate | `Yes` | `Number` |  | 0 | trường ẩn | PCSEXR |
| 9 | Cash amount /BCY | cash_amount_bcy | No | `Number` |  | 0 | trường ẩn | PCSCVT |
| 10 | Withdrawer name | withdrawer_name | `Yes` | String | 250 |  |  | CACNM  |
| 11 | Withdrawer id | withdrawer_id | No | String | 15 |  |  | CCTMCD |
| 12 | Withdrawer address | withdrawer_address | No | String | 250 |  |  | CCTMA  |
| 13 | Withdrawer description | withdrawer_description | No | JSON Object |  |  |  | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 14 | ID issue date | id_issue_date | No | `Date time` |  | Working date | trường ẩn | CIDDT  |
| 15 | ID place | id_place | No | String | 90 |  | trường ẩn | CIDPLACE | |
| 16 | Identification number | identification_number | No | String | 100 |  | trường ẩn | CREPID |
| 17 | Value date | value_date | No | `Date time` |  | Working date | trường ẩn | CVLDT  |
| 18 | Currency of deposit account | currency_of_deposit_account | No | String | 3 |  | trường ẩn | CCCR   |
| 19 | Exchange rate (Debit account/BCY) | exchange_rate_debit_account_bcy | No | `Number` |  | 0 | trường ẩn | CBKEXR |
| 20 | Amount (Debit account/BCY) | amount_debit_account_bcy | No | `Number` |  | 0 | trường ẩn | CCVT   |
| 21 | Commission | commission | No | `Number` |  | 0 | trường ẩn | CFAMT  |
| 22 | Interest from Earlywdr | interest_from_earlywdr | No | `Number` |  | 0 | trường ẩn | CTINT  |
| 23 | Due date | due_date | No | `Date time` |  | Working date | trường ẩn | DUEDT  |
| 24 | Interest prepaid | interest_prepaid | No | `Number` |  | 0 | trường ẩn | CIPRE  |
| 25 | Total VAT for Cash | total_vat_for_cash | No | `Number` |  | 0 | trường ẩn | CSVAT  |
| 26 | Account Linkage | account_linkage | No | String | 25 |  | trường ẩn | PDOACC |
| 27 | Amount linkage | amount_linkage | No | `Number` |  | 0 | trường ẩn | CAMT4  |
| 28 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 29 | Description | description | No | String | 250 |  |  | DESCS  |
| 30 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | | |
| 2 | Account number | account_number | String |  |  |
| 3 | Current balance | current_balance | `Number` |  |  |
| 4 | Avaiable balance | available_balance | `Number` |  |  |
| 5 | Withdraw amount | withdraw_amount | `Number` |  |  |
| 6 | Cash currency | cash_currency | String |  |  |
| 7 | Cross rate | cross_rate | `Number` |  |  |
| 8 | Cash amount | cash_amount | `Number` |  |  |
| 9 | Exchange rate | exchange_rate | `Number` |  |  |
| 10 | Cash amount /BCY | cash_amount_bcy | `Number` |  |  |
| 11 | Withdrawer name | withdrawer_name | String |  |  |
| 12 | Withdrawer id | withdrawer_id | String |  |  |
| 13 | Withdrawer address | withdrawer_address | String |  |  |
| 14 | Withdrawer description | withdrawer_description | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
| 15 | ID issue date | id_issue_date | `Date time` |  |  |
| 16 | ID place | id_place | String |  |  |
| 17 | Identification number | identification_number | String |  |  |
| 18 | Value date | value_date | `Date time` |  |  |
| 19 | Currency of deposit account | currency_of_deposit_account | String |  |  |
| 20 | Exchange rate (Debit account/BCY) | exchange_rate_debit_account_bcy | `Number` |  |  |
| 21 | Amount (Debit account/BCY) | amount_debit_account_bcy | `Number` |  |  |
| 22 | Commission | commission | `Number` |  |  |
| 23 | Interest from Earlywdr | interest_from_earlywdr | `Number` |  |  |
| 24 | Due date | due_date | `Date time` |  |  |
| 25 | Interest prepaid | interest_prepaid | `Number` |  |  |
| 26 | Total VAT for Cash | total_vat_for_cash | `Number` |  |  |
| 27 | Account Linkage | account_linkage | String |  |  |
| 28 | Amount linkage | amount_linkage | `Number` |  |  |
| 29 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 30 | Description | description | String |  |  |
| 31 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | |
| 32 | Transaction status | status | String |  |  |
| 33 | Transaction date | transaction_date | `Date time` |  |  |
| 34 | User id | user_id | `Number` |  |  |
| 35 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Account number: exists with status {"Normal", ~~"Dormant"~~}. Type is Current/ Saving
-	Account must have balance to withdraw (not include amount is keeping).
- Teller do this transaction:
  - User must have enough cash to withdraw.
  - User has right to do transaction related to cash.
  - User must have enough cash to give customer.

**Flow of events:**
-	Don't allow withdraw amount that different currency with account.
-	Allow to collect fee if withdrawal different branch and another fee.
-	Change status to Normal if account is "Dormant". (KTB thay đổi nghiệp vụ, deposit account có status là Dormant không cho phép thực hiện giao dịch)
-	Balance will decrease.
-   If transaction has collect fee, system will collect fee from withdraw amount
<br>Ex: Withdraw amount = 100, Fee = 10 => Customer will receive: 90, system collect fee from deposit account: 10

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Amount withdraw | Currency of account |
| 1 | C | CASH | Amount withdraw | Currency of account |
| Case collect fee (fee amount > 0) | | | | |
| 2 | D | CASH | Fee amount | Currency of account |
| 2 | C | IFCC (income) | Fee amount | Currency of account |

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 5](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Teller has not enough cash | Teller không đủ tiền mặt |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_BCY_CACNM | GET_INFO_ACCOUNT_BY_ACNO_DPTTYPE | [GET_INFO_ACCOUNT_BY_ACNO_DPTTYPE](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_ACLINK | DPT_GET_INFO_ACLINK | [DPT_GET_INFO_ACLINK](Specification/Common/15 Deposit Rulefuncs) |
| 4 | TRAN_FEE\GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 5 | TRAN_FEE\LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>