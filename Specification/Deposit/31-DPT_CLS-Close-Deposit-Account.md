# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CLS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | Balance | balance | No | `Number` | | 0 | | CPRIN  |
| 3 | Interest accrual | interest_accrual | No | `Number` | | 0 | trường ẩn | CINT |
| 4 | Withholding tax accrual actural | withholding_tax_accrual_actural | No | `Number` | | 0 | trường ẩn | WHTAMT |
| 5 | Withholding tax accrual | withholding_tax_accrual | No | `Number` | | 0 | | CAMT   |
| 6 | Interest payable/receivable | interest_payable_receivable | No | `Number` | | 0 | | CIPBL  |
| 7 | Interest due | interest_due | No | `Number` | | 0 | | CIDUE  |
| 8 | Gross paid interest amount | gross_paid_interest_amount | No | `Number` | | 0 | | CTINT  |
| 9 | Withholding tax rate | withholding_tax_rate | No | `Number` | | 0 | trường ẩn | CWHTR  |
| 10 | Withholding tax amount | withholding_tax_amount | No | `Number` | | 0 | | CWHTA  |
| 11 | Fee amount | fee_amount | No | `Number` | | 0 | trường ẩn | CFAMT  |
| 12 | IFCCD | ifc_code | No | `Number` | |  | trường ẩn | CVAT   |
| 13 | Depositor name | depositor_name | No | String | 250 | | | CACNM  |
| 14 | Depositor id | depositor_id | No | String | 15 | | | CCTMCD |
| 15 | Depositor address | depositor_address | No | String | 250 | | | CCTMA  |
| 16 | Depositor description | depositor_description | No | JSON Object | | | | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 17 | Currency | currency_code | No | String | 3 | | trường ẩn | CCCR |
| 18 | Sum amount | sum_amount | No | `Number` | | 0 | trường ẩn | AMT |
| 19 | Accrual interest amount decimal adjust | accrual_interest_amount_decimal_adjust | No | `Number` | | 0 | trường ẩn | IACR |
| 20 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 21 | Description | description | No | String | 250 | | | DESCS  |
| 21 | Branch name | branch_name | No | String | | | | `gửi thêm` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | | |
| 3 | Balance | balance | `Number` | | |
| 4 | Interest accrual | interest_accrual | `Number` | | |
| 5 | Withholding tax accrual actural | withholding_tax_accrual_actural | `Number` | | |
| 6 | Withholding tax accrual | withholding_tax_accrual | `Number` | | |
| 7 | Interest payable/receivable | interest_payable_receivable | `Number` | | |
| 8 | Interest due | interest_due | `Number` | | |
| 9 | Gross paid interest amount | gross_paid_interest_amount | `Number` | | |
| 10 | Withholding tax rate | withholding_tax_rate | `Number` | | |
| 11 | Withholding tax amount | withholding_tax_amount | `Number` | | |
| 12 | Fee amount | fee_amount | `Number` | | |
| 13 | IFCCD | ifc_code | `Number` | | |
| 14 | Depositor name | depositor_name | String | | |
| 15 | Depositor id | depositor_id | String | | |
| 16 | Depositor address | depositor_address | String | | |
| 17 | Depositor description | depositor_description | JSON Object | | |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 18 | Currency | currency_code | String | | |
| 19 | Sum amount | sum_amount | `Number` | | |
| 20 | Accrual interest amount decimal adjust | accrual_interest_amount_decimal_adjust | `Number` | | |
| 21 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 22 | Description | description | String | | |
| 23 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | |
| 24 | Transaction status | status | String |  |  |
| 25 | Transaction date |  | `Date time` |  |  |
| 26 | User id |  | `Number` |  |  |
| 27 | Branch name | branch_name | No | String | | | `gửi thêm` | |

## 1.2 Transaction flow
**Conditions:**

-	Account number:
    + Deposit type is Current/ Saving: exists with status "Normal"
    + Deposit type is FD: 
      + Account type is LFA: exists with status <> "Closed"
      + Account type is MFA: exists with status <> "Closed", all of LFA were closed
      + Open date + Minimum Tenor Allow Early Withdrawal <= Working date
-	Don't allow close different branch with account.
-   Earmark = 0
-   Witholding tax <= Gross paid interest amount <= Interest payable + Interest due
-	Gross paid interest amount can be zero.
- Teller do this transaction:
  - User must have enough cash to withdraw.
  - User has right to do transaction related to cash.
  - User must have enough cash to give customer.
-   Account must same branch with branch does this transaction

**Flow of events:**

-	Calculate "withholding tax" amount based interest amount repayment:
    +	Withholding tax = Gross paid interest amount * rate holding tax.
Condition of rate holding tax is define on account information.
-	Real interest amount to receive = Gross paid interest amount - withholding tax.
-	User must have enough cash to repay balance and real interest amount.
-	When complete transaction: 
    +	Status is "Closed" 
    +	Balance = 0
    +	Interest amount = 0
    +   Withholding tax = 0
-	Allow collection fees for this transaction (if any)

**Posting**
-	Case: Gross paid interest amount < [Interest receivable]

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Balance | Currency of account |
| 1 | C | CASH | Balance | Currency of account |
| 2 | D | INTEREST | Gross paid interest amount - Withholding tax amount | Currency of account |
| 2 | C | CASH | Gross paid interest amount - Withholding tax amount | Currency of account |
| 3 | D | INTEREST | [Interest payable or receivable] - Withholding tax amount | Currency of account |
| 3 | C | REVERT_INTEREST | [Interest payable or receivable] - Withholding tax amount | Currency of account |
| 4 | D | WITHHOLDING_TAX | [Withholding tax accrual] - [Withholding tax amount] | Currency of account |
| 4 | C | PAID_WHT | [Withholding tax accrual] - [Withholding tax amount] | Currency of account |
| 5 | D | INTEREST | Withholding tax amount | Currency of account |
| 5 | C | PAID_WHT | Withholding tax amount | Currency of account |

-	Case: Gross paid interest amount = [Interest receivable]

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Balance | Currency of account |
| 1 | C | CASH | Balance | Currency of account |
| 2 | D | INTEREST | Gross paid interest amount - Withholding tax amount | Currency of account |
| 2 | C | CASH | Gross paid interest amount - Withholding tax amount | Currency of account |
| 3 | D | INTEREST | Withholding tax amount | Currency of account |
| 3 | C | PAID_WHT | Withholding tax amount | Currency of account |

-	Case: Gross paid interest amount = 0

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Balance | Currency of account |
| 1 | C | CASH | Balance | Currency of account |
| 2 | D | INTEREST | [Interest payable or receivable] | Currency of account |
| 2 | C | REVERT_INTEREST | [Interest payable or receivable] | Currency of account | |
| 3 | D | WITHHOLDING_TAX | [Withholding tax accrual] - [Withholding tax amount] | Currency of account |
| 3 | C | PAID_WHT | [Withholding tax accrual] - [Withholding tax amount] | Currency of account |

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 7](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A204`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Teller has not enough cash | Teller không đủ tiền mặt |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_CUSTOMER | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_INT | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_INFO_DEF_INT_PAID_TOCUS | DPT_GET_REDEMPTION_INTEREST | [DPT_GET_REDEMPTION_INTEREST](Specification/Common/15 Deposit Rulefuncs) |
| 6 | GET_INFO_WHTAMT | DPT_GET_INFO_WHTAMT | [DPT_GET_INFO_WHTAMT](Specification/Common/15 Deposit Rulefuncs) |
| 7 | GET_INFO_CWHTA | DPT_GET_INTEREST_WITHOUT_WHTAX | [DPT_GET_INTEREST_WITHOUT_WHTAX](Specification/Common/15 Deposit Rulefuncs) |
| 8 | TRAN_FEE\GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 9 | TRAN_FEE\LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>