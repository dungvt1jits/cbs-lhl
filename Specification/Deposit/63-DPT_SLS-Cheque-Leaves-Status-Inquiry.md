# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_SLS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | From serial | from_serial | `Yes` | String | 20 | | | CFRSER |
| 2 | To serial | to_serial | `Yes` | String | 20 | | | CTOSER |
| 3 | Stock type | stock_type | `Yes` | String | 1 | | trường ẩn | CSTKTP |
| 4 | Stock prefix | stock_prefix | No | String | 5 | | trường ẩn | CSTKPRE |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | From serial | from_serial | String | | |
| 3 | To serial | to_serial | String | | |
| 6 | Transaction status | status | String | | |
| 7 | Stock type | stock_type | String | | |
| 8 | Stock prefix | stock_prefix | String | | |
| 9 | Transaction history | dataset | Array object | | |
|  | Serrial no |  | String | | |
|  | Stock leave status |  | String | | |
| 10 | Transaction date |  | `Date time` |  |  |
| 11 | User id |  | String |  |  |

## 1.2 Transaction flow
**Conditions:**
-	Account number is existing in the system.
-	"Serial number" must belong a cheque book. It was defined at: "Stock Inventory".
-	"Serial number" issued to account number.
-   Only view 1 book at the same time

**Flow of events:**
-	Get a data sheet with details: "Serial No", "Stock leaves status".

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX | [DPT_GET_STOCK_PREFIX](Specification/Common/15 Deposit Rulefuncs) |