# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/DepositCatalog​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_SEARCH_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    "search_text": "",
    "page_size": 10,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String | 8 |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Currency code | currency_code | String | 3 |  |
| 4 | Deposit type | deposit_type | String |  |  |
| 5 | Passbook or statement | passbook_or_statement_or_receipt | String |  |  |
| 6 | Tenor | tenor | `Number` |  | số nguyên dương |
| 7 | Tenor unit | tenor_unit | String |  |  |
| 8 | Status | catalog_status | String |  |  |
| 9 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/DepositCatalog​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_ADSEARCH_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- | 
| 1 | Catalogue code | catalog_code | No | String | 8 |  |  |
| 2 | Catalogue name | catalog_name | No | String |  |  |  |
| 3 | Currency code | currency_code | No | String | 3 |  |  |
| 4 | Deposit type | deposit_type | No | String |  |  |  |
| 5 | Passbook or statement | passbook_or_statement_or_receipt | No | String |  |  |  |
| 6 | Tenor | tenor | No | `Number` |  |  | số nguyên dương |
| 7 | Tenor from | tenor_from | No | `Number` |  |  | số nguyên dương |
| 8 | Tenor to | tenor_to | No | `Number` |  |  | số nguyên dương |
| 9 | Tenor unit | tenor_unit | No | String |  |  |  |
| 10 | Status | catalog_status | No | String |  |  |  |
| 11 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 12 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String | 8 |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Currency code | currency_code | String | 3 |  |
| 4 | Deposit type | deposit_type | String |  |  |
| 5 | Passbook or statement | passbook_or_statement_or_receipt | String |  |  |
| 6 | Tenor | tenor | `Number` |  | số nguyên dương |
| 7 | Tenor unit | tenor_unit | String |  |  |
| 8 | Status | catalog_status | String |  |  |
| 9 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/DepositCatalog​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_INSERT_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | `Yes` | String | 8 |  | is unique |
| 2 | Catalogue name | catalog_name | `Yes` | String |  |  |  |
| 3 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 4 | Secure currency code | secure_currency | No | String |  |  | không sử dụng, xem xét xóa đi |
| 5 | Deposit type | deposit_type | `Yes` | String |  |  |  |
| 6 | Deposit purpose | deposit_purpose | `Yes` | String |  |  |  |
| 7 | Deposit classification | deposit_classification | `Yes` | String |  |  |  |
| 8 | Passbook or statement or receipt | passbook_or_statement_or_receipt | `Yes` | String |  |  |  |
| 9 | Minimum deposit amount | minimum_deposit_amount | No | `Number` |  |  | số có hai số thập phân |
| 10 | Catalogue status | catalog_status | `Yes` | String |  |  |  |
| 12 | Tenor 1 | tenor | `Yes` | `Number` |  | 0 | số nguyên dương, `deposit_type=T phải nhập > 0` |
| 13 | Tenor unit 1 | tenor_unit | `Yes` | String |  | M |  |
| 14 | Tenor 2 | tenor2 | `Yes` | `Number` |  | 0 | số nguyên dương |
| 15 | Tenor unit 2 | tenor_unit2 | `Yes` | String |  | D |  |
| 16 | Deposit tenor | deposit_tenor | `Yes` | `Number` |  | 0 | số nguyên dương |
| 17 | Deposit tenor unit | deposit_tenor_unit | `Yes` | String |  | M |  |
| 18 | Interest tenor | interest_tenor | `Yes` | `Number` |  | 0 | số nguyên dương, `deposit_type=T,S` phải nhập > 0 nếu `interest_tenor_unit <> L` |
| 19 | Interest tenor unit | interest_tenor_unit | `Yes` | String |  | M |  |
| 20 | Minimum tenor | minimum_tenor | `Yes` | `Number` |  | 0 | số nguyên dương |
| 21 | Minimum tenor unit | minimum_tenor_unit | `Yes` | String |  | D |  |
| 22 | Multiple deposit allow | multi_deposit | `Yes` | String |  | Y |  |
| 23 | Multiple withdrawal allow | multi_withdraw | `Yes` | String |  | Y |  |
| 24 | Early withdrawal | early_withdraw | `Yes` | String |  | Y |  |
| 25 | Minimum tenor allow early withdrawal | minimum_tenor_allow_early_withdrawal | `Yes` | `Number` |  | 0 | số nguyên dương |
| 26 | Minimum tenor allow early withdrawal unit | minimum_tenor_allow_early_withdrawal_unit | `Yes` | String |  | D |  |
| 27 | Credit interest (Y/N) | credit_interest | `Yes` | String |  | Y |  |
| 28 | Credit interest tenor | credit_interest_tenor | `Yes` | `Number` |  | 1 | số nguyên dương |
| 29 | Credit interest tenor unit | credit_interest_tenor_unit | `Yes` | String |  | M |  |
| 30 | The day of tenor for crediting interest | crediting_interest | No | `Number` |  | 0 | số nguyên dương, `deposit_type=S` phải nhập > 0 |
| 31 | Minimum dormant amount | minimum_dormant_amount | No | `Number` |  |  | số có hai số thập phân |
| 32 | Dormant period | dormant_period | `Yes` | `Number` |  | 180 | số nguyên dương |
| 33 | Type of dormant period | dormant_period_unit | `Yes` | String |  | D |  |
| 34 | Rollover option | rollover | `Yes` | String |  |  |  |
| 35 | Rollover to catalogue | rollover_to_catalog | No | `Number` |  | `Null` |  |
| 36 | Interest due on holiday | interest_due_on_holiday | `Yes` | `Number` |  | 0 | số nguyên dương |
| 37 | Principal due on holiday | principal_due_on_holiday | `Yes` | `Number` |  | 0 | số nguyên dương |
| 38 | Statement format | statement_format | `Yes` | String |  |  |  |
| 39 | Statement tenor | statement_tenor | `Yes` | `Number` |  | 1 | số nguyên dương |
| 40 | Statement tenor unit | statement_tenor_unit | `Yes` | String |  | M |  |
| 41 | Initial deposit amount | inital_depost_amount | No | `Number` |  |  | số có hai số thập phân |
| 42 | Periodic deposit amount | periodic_deposit_amount | No | `Number` |  |  | số có hai số thập phân |
| 43 | Periodic deposit tenor | periodic_deposit_tenor | No | `Number` |  |  | số nguyên dương |
| 44 | Periodic deposit tenor unit | periodic_deposit_tenor_unit | No | String |  |  |  |
| 45 | Tariff code | tariff_code | No | `Number` |  | `Null` |  |
| 46 | Group ID | group_id | `Yes` | `Number` |  |  |  |
| 47 | Created by | user_created | `Yes` | `Number` |  |  |  |
| 48 | Approved by | user_approved | No | `Number` |  | `Null` |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Name | catalog_name | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Secure currency code | secure_currency | String |  | không sử dụng, xem xét xóa đi |
| 6 | Deposit type | deposit_type | String |  |  |
| 7 | Deposit purpose | deposit_purpose | String |  |  |
| 8 | Deposit classification | deposit_classification | String |  |  |
| 9 | Passbook or statement or receipt | passbook_or_statement_or_receipt | String |  |  |
| 10 | Minimum deposit amount | minimum_deposit_amount | `Number` |  | số có hai số thập phân |
| 11 | Catalogue status | catalog_status | String |  |  |
| 12 | Created by | user_created | `Number` |  |  |
| 13 | Approved by | user_approved | `Number` |  |  |
| 14 | Tenor 1 | tenor | `Number` |  | số nguyên dương |
| 15 | Tenor unit 1 | tenor_unit | String |  |  |
| 16 | Tenor 2 | tenor2 | `Number` |  | số nguyên dương |
| 17 | Tenor unit 2 | tenor_unit2 | String |  |  |
| 18 | Deposit tenor | deposit_tenor | `Number` |  | số nguyên dương |
| 19 | Deposit tenor unit | deposit_tenor_unit | String |  |  |
| 20 | Interest tenor | interest_tenor | `Number` |  | số nguyên dương |
| 21 | Interest tenor unit | interest_tenor_unit | String |  |  |
| 22 | Minimum tenor | minimum_tenor | `Number` |  | số nguyên dương |
| 23 | Minimum tenor unit | minimum_tenor_unit | String |  |  |
| 24 | Multiple deposit allow | multi_deposit | String |  |  |
| 25 | Multiple withdrawal allow | multi_withdraw | String |  |  |
| 26 | Early withdrawal | early_withdraw | String |  |  |
| 27 | Minimum tenor allow early withdrawal | minimum_tenor_allow_early_withdrawal | `Number` |  | số nguyên dương |
| 28 | Minimum tenor allow early withdrawal unit | minimum_tenor_allow_early_withdrawal_unit | String |  |  |
| 29 | Credit interest (Y/N) | credit_interest | String |  |  |
| 30 | Credit interest tenor | credit_interest_tenor | `Number` |  | số nguyên dương |
| 31 | Credit interest tenor unit | credit_interest_tenor_unit | String |  |  |
| 32 | The day of tenor for crediting interest | crediting_interest | `Number` |  | số nguyên dương |
| 33 | Minimum dormant amount | minimum_dormant_amount | `Number` |  | số có hai số thập phân |
| 34 | Dormant period | dormant_period | `Number` |  | số nguyên dương |
| 35 | Dormant period unit | dormant_period_unit | String |  |  |
| 36 | Rollover option | rollover | String |  |  |
| 37 | Rollover to catalogue | rollover_to_catalog | `Number` |  |  |
| 38 | Interest due on holiday | interest_due_on_holiday | `Number` |  | số nguyên dương |
| 39 | Principal due on holiday | principal_due_on_holiday | `Number` |  | số nguyên dương |
| 40 | Statement format | statement_format | String |  |  |
| 41 | Statement tenor | statement_tenor | `Number` |  | số nguyên dương |
| 42 | Statement tenor unit | statement_tenor_unit | String |  |  |
| 43 | Initial deposit amount | inital_depost_amount | `Number` |  | số có hai số thập phân |
| 44 | Periodic deposit amount | periodic_deposit_amount | `Number` |  | số có hai số thập phân |
| 45 | Periodic deposit tenor | periodic_deposit_tenor | `Number` |  | số nguyên dương |
| 46 | Periodic deposit tenor unit | periodic_deposit_tenor_unit | String |  |  |
| 47 | Tariff code | tariff_code | `Number` |  |  |
| 48 | Group ID | group_id | `Number` |  |  |
| 49 | Interest accrual rate | intacrrt | `Number` |  | mặc định: 100 |
| 50 | Deposit group | deposit_group | String |  | mặc định: 1, Deposit group: Money market, individual, enterprise, etc. The bank can define deposit group |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/DepositCatalog​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_VIEW_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 30
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Name | catalog_name | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Secure currency code | secure_currency | String |  | không sử dụng, xem xét xóa đi |
| 6 | Deposit type | deposit_type | String |  |  |
| 7 | Deposit purpose | deposit_purpose | String |  |  |
| 8 | Deposit classification | deposit_classification | String |  |  |
| 9 | Passbook or statement or receipt | passbook_or_statement_or_receipt | String |  |  |
| 10 | Minimum deposit amount | minimum_deposit_amount | `Number` |  | số có hai số thập phân |
| 11 | Catalogue status | catalog_status | String |  |  |
| 12 | Created by | user_created | `Number` |  |  |
| 13 | Approved by | user_approved | `Number` |  |  |
| 14 | Tenor 1 | tenor | `Number` |  | số nguyên dương |
| 15 | Tenor unit 1 | tenor_unit | String |  |  |
| 16 | Tenor 2 | tenor2 | `Number` |  | số nguyên dương |
| 17 | Tenor unit 2 | tenor_unit2 | String |  |  |
| 18 | Deposit tenor | deposit_tenor | `Number` |  | số nguyên dương |
| 19 | Deposit tenor unit | deposit_tenor_unit | String |  |  |
| 20 | Interest tenor | interest_tenor | `Number` |  | số nguyên dương |
| 21 | Interest tenor unit | interest_tenor_unit | String |  |  |
| 22 | Minimum tenor | minimum_tenor | `Number` |  | số nguyên dương |
| 23 | Minimum tenor unit | minimum_tenor_unit | String |  |  |
| 24 | Multiple deposit allow | multi_deposit | String |  |  |
| 25 | Multiple withdrawal allow | multi_withdraw | String |  |  |
| 26 | Early withdrawal | early_withdraw | String |  |  |
| 27 | Minimum tenor allow early withdrawal | minimum_tenor_allow_early_withdrawal | `Number` |  | số nguyên dương |
| 28 | Minimum tenor allow early withdrawal unit | minimum_tenor_allow_early_withdrawal_unit | String |  |  |
| 29 | Credit interest (Y/N) | credit_interest | String |  |  |
| 30 | Credit interest tenor | credit_interest_tenor | `Number` |  | số nguyên dương |
| 31 | Credit interest tenor unit | credit_interest_tenor_unit | String |  |  |
| 32 | The day of tenor for crediting interest | crediting_interest | `Number` |  | số nguyên dương |
| 33 | Minimum dormant amount | minimum_dormant_amount | `Number` |  | số có hai số thập phân |
| 34 | Dormant period | dormant_period | `Number` |  | số nguyên dương |
| 35 | Dormant period unit | dormant_period_unit | String |  |  |
| 36 | Rollover option | rollover | String |  |  |
| 37 | Rollover to catalogue | rollover_to_catalog | `Number` |  |  |
| 38 | Interest due on holiday | interest_due_on_holiday | `Number` |  | số nguyên dương |
| 39 | Principal due on holiday | principal_due_on_holiday | `Number` |  | số nguyên dương |
| 40 | Statement format | statement_format | String |  |  |
| 41 | Statement tenor | statement_tenor | `Number` |  | số nguyên dương |
| 42 | Statement tenor unit | statement_tenor_unit | String |  |  |
| 43 | Initial deposit amount | inital_depost_amount | `Number` |  | số có hai số thập phân |
| 44 | Periodic deposit amount | periodic_deposit_amount | `Number` |  | số có hai số thập phân |
| 45 | Periodic deposit tenor | periodic_deposit_tenor | `Number` |  | số nguyên dương |
| 46 | Periodic deposit tenor unit | periodic_deposit_tenor_unit | String |  |  |
| 47 | Tariff code | tariff_code | `Number` |  |  |
| 48 | Group ID | group_id | `Number` |  |  |
| 49 | Interest accrual rate | intacrrt | `Number` |  | mặc định: 100 |
| 50 | Deposit group | deposit_group | String |  | mặc định: 1, Deposit group: Money market, individual, enterprise, etc. The bank can define deposit group |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/DepositCatalog​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_UPDATE_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  | Không được sửa |
| 2 | Name | catalog_name | `Yes` | String |  |  |  |
| 3 | Currency code | currency_code | `Yes` | String |  |  |  |
| 4 | Secure currency code | secure_currency | No | String |  |  | không sử dụng, xem xét xóa đi |
| 5 | Deposit type | deposit_type | `Yes` | String |  |  |  |
| 6 | Deposit purpose | deposit_purpose | `Yes` | String |  |  |  |
| 7 | Deposit classification | deposit_classification | `Yes` | String |  |  |  |
| 8 | Passbook or statement or receipt | passbook_or_statement_or_receipt | `Yes` | String |  |  |  |
| 9 | Minimum deposit amount | minimum_deposit_amount | No | `Number` |  |  | số có hai số thập phân |
| 10 | Catalogue status | catalog_status | `Yes` | String |  |  |  |
| 11 | Multiple deposit allow | multi_deposit | `Yes` | String |  |  |  |
| 12 | Multiple withdrawal allow | multi_withdraw | `Yes` | String |  |  |  |
| 13 | Credit interest (Y/N) | credit_interest | `Yes` | String |  |  |  |
| 14 | Credit interest tenor | credit_interest_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 15 | Credit interest tenor unit | credit_interest_tenor_unit | `Yes` | String |  |  |  |
| 16 | The day of tenor for crediting interest | crediting_interest | No | `Number` |  |  | số nguyên dương |
| 17 | Minimum dormant amount | minimum_dormant_amount | No | `Number` |  |  | số có hai số thập phân |
| 18 | Dormant period | dormant_period | `Yes` | `Number` |  |  | số nguyên dương |
| 19 | Dormant period unit | dormant_period_unit | `Yes` | String |  |  |  |
| 20 | Statement format | statement_format | `Yes` | String |  |  |  |
| 21 | Statement tenor | statement_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 22 | Statement tenor unit | statement_tenor_unit | `Yes` | String |  |  |  |
| 23 | Initial deposit amount | inital_depost_amount | No | `Number` |  |  | số có hai số thập phân |
| 24 | Tariff code | tariff_code | No | `Number` |  | `Null` |  |
| 25 | Group ID | group_id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Name | catalog_name | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Secure currency code | secure_currency | String |  | không sử dụng, xem xét xóa đi |
| 6 | Deposit type | deposit_type | String |  |  |
| 7 | Deposit purpose | deposit_purpose | String |  |  |
| 8 | Deposit classification | deposit_classification | String |  |  |
| 9 | Passbook or statement or receipt | passbook_or_statement_or_receipt | String |  |  |
| 10 | Minimum deposit amount | minimum_deposit_amount | `Number` |  | số có hai số thập phân |
| 11 | Catalogue status | catalog_status | String |  |  |
| 12 | Created by | user_created | `Number` |  |  |
| 13 | Approved by | user_approved | `Number` |  |  |
| 14 | Tenor 1 | tenor | `Number` |  | số nguyên dương |
| 15 | Tenor unit 1 | tenor_unit | String |  |  |
| 16 | Tenor 2 | tenor2 | `Number` |  | số nguyên dương |
| 17 | Tenor unit 2 | tenor_unit2 | String |  |  |
| 18 | Deposit tenor | deposit_tenor | `Number` |  | số nguyên dương |
| 19 | Deposit tenor unit | deposit_tenor_unit | String |  |  |
| 20 | Interest tenor | interest_tenor | `Number` |  | số nguyên dương |
| 21 | Interest tenor unit | interest_tenor_unit | String |  |  |
| 22 | Minimum tenor | minimum_tenor | `Number` |  | số nguyên dương |
| 23 | Minimum tenor unit | minimum_tenor_unit | String |  |  |
| 24 | Multiple deposit allow | multi_deposit | String |  |  |
| 25 | Multiple withdrawal allow | multi_withdraw | String |  |  |
| 26 | Early withdrawal | early_withdraw | String |  |  |
| 27 | Minimum tenor allow early withdrawal | minimum_tenor_allow_early_withdrawal | `Number` |  | số nguyên dương |
| 28 | Minimum tenor allow early withdrawal unit | minimum_tenor_allow_early_withdrawal_unit | String |  |  |
| 29 | Credit interest (Y/N) | credit_interest | String |  |  |
| 30 | Credit interest tenor | credit_interest_tenor | `Number` |  | số nguyên dương |
| 31 | Credit interest tenor unit | credit_interest_tenor_unit | String |  |  |
| 32 | The day of tenor for crediting interest | crediting_interest | `Number` |  | số nguyên dương |
| 33 | Minimum dormant amount | minimum_dormant_amount | `Number` |  | số có hai số thập phân |
| 34 | Dormant period | dormant_period | `Number` |  | số nguyên dương |
| 35 | Dormant period unit | dormant_period_unit | String |  |  |
| 36 | Rollover option | rollover | String |  |  |
| 37 | Rollover to catalogue | rollover_to_catalog | `Number` |  |  |
| 38 | Interest due on holiday | interest_due_on_holiday | `Number` |  | số nguyên dương |
| 39 | Principal due on holiday | principal_due_on_holiday | `Number` |  | số nguyên dương |
| 40 | Statement format | statement_format | String |  |  |
| 41 | Statement tenor | statement_tenor | `Number` |  | số nguyên dương |
| 42 | Statement tenor unit | statement_tenor_unit | String |  |  |
| 43 | Initial deposit amount | inital_depost_amount | `Number` |  | số có hai số thập phân |
| 44 | Periodic deposit amount | periodic_deposit_amount | `Number` |  | số có hai số thập phân |
| 45 | Periodic deposit tenor | periodic_deposit_tenor | `Number` |  | số nguyên dương |
| 46 | Periodic deposit tenor unit | periodic_deposit_tenor_unit | String |  |  |
| 47 | Tariff code | tariff_code | `Number` |  |  |
| 48 | Group ID | group_id | `Number` |  |  |
| 49 | Interest accrual rate | intacrrt | `Number` |  | mặc định: 100 |
| 50 | Deposit group | deposit_group | String |  | mặc định: 1, Deposit group: Money market, individual, enterprise, etc. The bank can define deposit group |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/DepositCatalog​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_DELETE_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 30
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 30
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list tariff
- [Tariff lookup trong "Rulefunc"](Specification/Common/01 Rulefunc)

# 8. Get list accounting group for deposit
- "module": "DPT"
- [Accounting group trong "Rulefunc"](Specification/Common/01 Rulefunc)

# Business rule and constrains
- The bank can apply a flexible and extensible IFC tariff for a category e.g. multi-layer interest rate. For more information, see IFC specification document.
- Each branch can have different IFC tariff (mean different interest, commission, fee and charge list). This is usefully for the bank has overseas branch can have different interest policy apply for it. For more information, see IFC specification document.
- The bank can define IFC Tariff as tier rate base on amount or period of time and assign to category as display in matrix below:

| Amount | Period |  |  |  |  |
| ------ | ------ |--|--|--|--|
|  | 1 month | 2 months | 3 months | 4 months | 6 months |
|  | Interest rate |  |  |  |  |
| 0 - 1 million | 3 | 3.1 | 3.2 | 3.3 | 3.4 |
| > 1 million - 5 million | 3.1 | 3.2 | 3.3 | 3.4 | 3.5 |
| >5 million - 10 million | 3.2 | 3.3 | 3.4 | 3.5 | 3.6 |
| > 10 million - 50 million | 3.3 | 3.4 | 3.5 | 3.6 | 3.7 |
| >50 million | 3.5 | 3.6 | 3.7 | 3.8 | 3.9 |

# Accounting group definition
- Accounting group is aimed to help system to generate accounting entry for appropriate transaction. With Neptune system, accounting group is assigned to categories in each business application. 
- Deposit application category can assign to accounting group with the following structure:

| No | System account name | Description |
| -- | ------------------- | ----------- |
| 1 | DEPOSIT | Debit positing when deposit. |
|  |  | Credit positing when withdraw. |
| 2 | INTEREST | Debit positing when repay interest. |
|  |  | Credit positing when accrued interest. |
| 3 | PAID_INTEREST | Debit positing when accrued interest. |
| 4 | PAID_WHT | Credit positing when collect withholding tax. |
| 5 | REVERT_INTEREST | Credit posting when gross paid interest amounts smaller than interest amount |
| 6 | WITHHOLDING_TAX | Debit posting when collect "Withholding tax amount" smaller than "Withholding tax accrual". |
|  |  | Credit posting when calculate withholding tax daily. |