# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_TRT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Master FD account | master_fd_account | `Yes` | String | 25 | | | PDMACC  |
| 2 | Link FD account | link_fd_account | `Yes` | String | 25 | | | PDACC   |
| 3 | Link FD account name | link_fd_account_name | No | String | 250 | | | CACNM2  |
| 4 | Current balance | current_balance | No | `Number` | | 0 | | CAMT2   |
| 5 | Amount | amount | `Yes` | `Number` | | >0 | | CTXAMT1 |
| 6 | Credit to account  | credit_to_account | `Yes` | String | 25 | | | PDOACC  |
| 7 | Credit to account name | credit_to_account_name | No | String | 250 | | | CACNM   |
| 8 | Currency of debit account | currency_of_debit_account | No | String | 3 | | trường ẩn | CCCR    |
| 9 | Currency of credit account | currency_of_credit_account | No | String | 3 | | trường ẩn | CCCR1   |
| 10 | Customer code | customer_code | No | String | 15 | | | CCTMCD  |
| 11 | Customer address | customer_address | No | String | 250 | | | CCTMA   |
| 12 | Customer description | customer_description | No | JSON Object | | | | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 13 | Total fee | total_fee | No | `Number` | | | trường ẩn | TXAMT   |
| 14 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 15 | Description | description | No | String | 250 | | | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Master FD account | master_fd_account | String | | |
| 3 | Link FD account | link_fd_account | String | | |
| 4 | Link FD account name | link_fd_account_name | String | | |
| 5 | Current balance | current_balance | `Number` | | |
| 6 | Amount | amount | `Number` | | |
| 7 | Credit to account  | credit_to_account | String | | |
| 8 | Credit to account name | credit_to_account_name | String | | |
| 9 | Currency of debit account | currency_of_debit_account | String | | trường ẩn |
| 10 | Currency of credit account | currency_of_credit_account | String | | trường ẩn |
| 11 | Customer code | customer_code | String | | |
| 12 | Customer address | customer_address | String | | |
| 13 | Customer description | customer_description | JSON Object | | |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 14 | Total fee | total_fee | `Number` | | trường ẩn |
| 15 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 16 | Description | description | String | | |
| 17 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | |
| 18 | Transaction status | status | String |  |  |
| 19 | Transaction date | transaction_date | `Date time` |  |  |
| 20 | User id | user_id | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
-	"Master Fixed Deposit account": exists with status {"Active"}. Account type is Master FD account.
-	"Link Fixed Deposit account": exists with status {"Active", "Maturity", "Normal"}. Account type is Link FD account and belongs to Master FD account.
-	"Credit account": exists with status is "Normal"
    + Deposit type: FD 
	  + MultipleDepositAllow = Y
	+ Deposit type: Saving 
	  + PassbookOrReceiptStatus = N/ U
-	Accounts must be same currency.
-	"Link FD account" has enough amount to transfer (not include amount is keeping).

**Flow of events:**
-	If first time deposit. Amount of credit account must be larger than "Initial deposit amount".
-	If setup parameter: allow deposit one-time. System will not allow to continue deposit amount.
-	Change status of credit account if first time deposit:
	+	Account type is current, change status to "Normal".
	+	Account type is saving, change status to "Normal".
	+	Account type is fixed deposit, change status to "Actived" (if allow deposit: only one time).
	+	Account type is fixed deposit, change status to "Normal" (if allow deposit: multi-time).
-	Allow collection fees for this transaction (if any).
-	Credit account will increase amount.
-	Debit account will decrease amount.

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT (LFA) | Amount deposit | Currency of account |
| 1 | C | DEPOSIT (receive) | Amount deposit | Currency of account |

**Cập nhật thông tin deposit account cho tài khoản gửi tiền:**
[Tham khảo thông tin chi tiết tại mục 5](Specification/Common/06 Transaction Flow Deposit)<br>

**Cập nhật thông tin deposit account cho tài khoản nhận tiền:**
[Tham khảo thông tin chi tiết tại mục 4](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Deposit type is invalid | Deposit type không hợp lệ |
|  | Account type is invalid | Account type không hợp lệ |
|  | Balance has not enough | Balance của tài khoản gửi không đủ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
- System will change:
    - Transaction status to "Reversed".
	
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDMACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_ACLINK | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_BCY_ACNAME | DPT_GET_INFO_CUSTOMER_TIMEDPT | [DPT_GET_INFO_CUSTOMER_TIMEDPT](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_SIG_PDOACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 5 | GET_INFO_OTHER_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 6 | LKP_DATA_PDACC | DPT_LKP_DATA_PDACC | [DPT_LKP_DATA_PDACC](Specification/Common/15 Deposit Rulefuncs) |
| 7 | TRAN_FEE_DPT_TRF\GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 8 | TRAN_FEE\GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 9 | TRAN_FEE_DPT_TRF\LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>