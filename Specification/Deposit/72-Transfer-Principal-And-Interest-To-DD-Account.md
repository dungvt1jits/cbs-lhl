# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Interest due > 0.00
- Type roll-over: "No roll-over".
- Exists linkage between 2 accounts:  transfer account and receive account.
- Accounts must be currency.
- Receive account exists with status {"Normal", "Dormant"}. Type is current or saving.

**Flow of events:**
- Receive account will increase balance.
- Interest due of transfer account = 0.00
- Balance of transfer account = 0.00
- Transfer account status is “Closed”.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT (transfer) | Balance | Currency of account |
| 1 | C | DEPOSIT (receive) | Balance | Currency of account |
| 2 | D | INTEREST | [Interest payable or receivable] | Currency of account |
| 2 | C | REVERT_INTEREST | [Interest payable or receivable] | Currency of account |
| 3 | D | WITHHOLDING_TAX | [Withholding tax accrual] - [Withholding tax amount] | Currency of account |
| 3 | C | PAID_WHT | [Withholding tax accrual] - [Withholding tax amount] | Currency of account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |