# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_MWR`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | PDACC   |
| 2 | Current balance | current_balance | `Yes` | `Number` |  | 0 |  | CAMT2   |
| 3 | Available balance | available_balance | `Yes` | `Number` |  | 0 |  | CAMT3   |
| 4 | Interest prepaid | interest_prepaid | No | `Number` | | 0 | trường ẩn | CIPRE   |
| 5 | Interest earlywdr | interest_earlywdr | No | `Number` | | 0 | trường ẩn | CTINT   |
| 6 | Withdraw amount | withdraw_amount | `Yes` | `Number` |  | >0 |  | PGAMT   |
| 7 | Credit accounting | credit_accounting | `Yes` | String | 25 |  |  | PGACC   |
| 8 | Accounting currency | accounting_currency | No | String | 3  |  | trường ẩn | CCCR1   |
| 9 | Cross rate | cross_rate | No | `Number` | | 0 | trường ẩn | CCRRATE |
| 10 | Accounting amount | accounting_amount | No | `Number` | | 0 | trường ẩn | CPGCVT  |
| 11 | Exchange rate of accounting/BCY | exchange_rate_of_accounting_bcy | No | `Number` | | 0 | trường ẩn | PGEXR   |
| 12 | Amount equivalent in BCY | amount_equivalent_in_bcy | No | `Number` | | 0 | trường ẩn | PGCVT   |
| 13 | Withdrawer name | withdrawer_name | `Yes` | String | 250 |  |  | CACNM   |
| 14 | Withdrawer code | withdrawer_code | No | String | 15 |  |  | CCTMCD  |
| 15 | Withdrawer address | withdrawer_address | No | String | 250 |  |  | CCTMA   |
| 16 | Withdrawer description | withdrawer_description | No | JSON Object |  |  |  | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 17 | Values date | values_date | No | `Date time` |  | Working date |  | CVLDT   |
| 18 | Currency of deposit account | currency_of_deposit_account | No | String | 3 |  | trường ẩn | CCCR    |
| 19 | Exchange rate (Debit account/BCY) | exchange_rate_debit_account_bcy | No | `Number` | | 0 | trường ẩn | CBKEXR  |
| 20 | Amount (Debit account/BCY) | amount_debit_account_bcy | No | `Number` | | 0 | trường ẩn | CCVT    |
| 21 | Account linkage | account_linkage | No | String | 25 |  | trường ẩn | PDOACC  |
| 22 | Amount linkage | amount_linkage | No | `Number` | | 0 | trường ẩn | CAMT4   |
| 23 | Branch id | branch_id | No | `Number` | | branch id user login | trường ẩn | CBRCD   |
| 24 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 25 | Description | description | No | String |  |  |  | DESCS   |
| 26 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | | String | | |
| 2 | Account number | account_number | String |  |  |
| 3 | Current balance | current_balance | `Number` |  |  |
| 4 | Available balance | available_balance | `Number` |  |  |
| 5 | Interest prepaid | interest_prepaid | `Number` |  | trường ẩn |
| 6 | Interest earlywdr | interest_earlywdr | `Number` |  | trường ẩn |
| 7 | Withdraw amount | withdraw_amount | `Number` |  |  |
| 8 | Credit accounting | credit_accounting | String |  |  |
| 9 | Accounting currency | accounting_currency | String |  | trường ẩn |
| 10 | Cross rate | cross_rate | `Number` |  | trường ẩn |
| 11 | Accounting amount | accounting_amount | `Number` |  | trường ẩn |
| 12 | Exchange rate of accounting/BCY | exchange_rate_of_accounting_bcy | `Number` |  | trường ẩn |
| 13 | Amount equivalent in BCY | amount_equivalent_in_bcy | `Number` |  | trường ẩn |
| 14 | Withdrawer name | withdrawer_name | String |  |  |
| 15 | Withdrawer code | withdrawer_code | String |  |  |
| 16 | Withdrawer address | withdrawer_address | String |  |  |
| 17 | Withdrawer description | withdrawer_description | JSON Object |  |  |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 18 | Values date | values_date | String |  |  |
| 19 | Currency of deposit account | currency_of_deposit_account | String |  | trường ẩn |
| 20 | Exchange rate (Debit account/BCY) | exchange_rate_debit_account_bcy | `Number` |  | trường ẩn |
| 21 | Amount (Debit account/BCY) | amount_debit_account_bcy | `Number` |  | trường ẩn |
| 22 | Account linkage | account_linkage | String |  | trường ẩn |
| 23 | Amount linkage | amount_linkage | `Number` |  | trường ẩn |
| 24 | Branch id | branch_id | `Number` |  | trường ẩn |
| 25 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 26 | Description | description | String |  |  |
| 27 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | |
| 28 | Transaction status | status | String |  |  |
| 29 | Transaction date | transaction_date | `Date time` |  |  |
| 30 | User id | user_id | `Number` |  |  |
| 31 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Account number: exists with status {"Normal", ~~"Dormant"~~}
-	Account must have balance to withdraw (not include amount is keeping).
-	GL account: exists and same branch with user. 
-	GL allows credit side. (KTB không xét điều kiện này)
-	Currency of GL must be same with currency of deposit account.

**Flow of events:**
-	Don't allow withdraw amount that different currency with account.
-	Allow to collect fee if withdrawal different branch and another fee.
-	Change status to Normal if account is "Dormant". (KTB thay đổi nghiệp vụ, deposit account có status là Dormant không cho phép thực hiện giao dịch)
-	Balance will decrease.
-   If transaction has collect fee, system will collect fee from withdraw amount
<br>Ex: Withdraw amount = 100, Fee = 10 => GL will receive: 100, system collect fee from deposit: 10

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Amount withdraw | Currency of account |
| 1 | C | GL | Amount withdraw | Currency of account |
| Case collect fee (fee amount > 0) | | | | |
| 2 | D | DEPOSIT | Fee amount | Currency of account |
| 2 | C | IFCC (income) | Fee amount | Currency of account |

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 5](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | {} is not allowed to be credit side | Posting side của GL account không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_CUSTOMER | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_AVALABLE_BAL | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_INFO_ACLINK | DPT_GET_INFO_ACLINK | [DPT_GET_INFO_ACLINK](Specification/Common/15 Deposit Rulefuncs) |
| 6 | GET_INFO_PGEXR | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 7 | LKP_DATA_BACNO | ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY | [ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY](Specification/Common/11 Accounting Rulefuncs) |
| 8 | TRAN_FEE\GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 9 | TRAN_FEE\LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>