# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_EMK`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC  |
| 2 | Hold amount | hold_amount | `Yes` | `Number` | | > 0 | | CAMT4  |
| 3 | Reference code | reference_code | `Yes` | String | 35 | | | CRMDL  |
| 4 | Reference number | reference_number | `Yes` | String | 100 | | | CRNUM  |
| 5 | Fee amount | fee_amount | No | `Number` | | 0 | trường ẩn | CFAMT  |
| 6 | Fee VAT | fee_vat | No | `Number` | | 0 | trường ẩn | CVAT   |
| 7 | Depositor name | depositor_name | `Yes` | String | 250 | | | CACNM  |
| 8 | Depositor id | depositor_id | No | String | 15 | | | CCTMCD |
| 9 | Depositor address | depositor_address | No | String | 250 | | | CCTMA  |
| 10 | Depositor description | depositor_description | No | JSON Object | | | | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 11 | Values date | values_date | No | `Date time` | | Working date | trường ẩn | CVLDT  |
| 12 | Amount currency | amount_currency | No | String | 3 | | | PCSCCR |
| 13 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  | 0 | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  | 0 | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  | 0 | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  | 0 | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 14 | Description | description | No | String | 250 | | | DESCS  |
| 15 | Expired date | expired_date | `Yes` | `Date time` |  |  |  | `gửi thêm` |
| 16 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | | |
| 3 | Hold amount | hold_amount | `Number` | | |
| 4 | Reference code | reference_code | String | | |
| 5 | Reference number | reference_number | String | | |
| 6 | Fee amount | fee_amount | `Number` | | trường ẩn |
| 7 | Fee VAT | fee_vat | `Number` | | trường ẩn |
| 8 | Depositor name | depositor_name | String | | |
| 9 | Depositor id | depositor_id | String | | |
| 10 | Depositor address | depositor_address | String | | |
| 11 | Depositor description | depositor_description | JSON Object | | |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
| 12 | Values date | values_date | `Date time` | | trường ẩn |
| 13 | Amount currency | amount_currency | String | | |
| 14 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 15 | Description | description | String | | |
| 16 | Transaction status | status | String |  |  |
| 17 | Transaction date | transaction_date | `Date time` |  |  |
| 18 | User id | user_id | `Number` |  |  |
| 19 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | |
| 20 | Expired date | expired_date | `Date time` | | `trả thêm` |
| 21 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Account number: exists with status <> "Closed"/"Block". Type is saving or current.
-	Balance of account has enough to keep amount.
-	Hold Amount must be larger than zero.
-   If account has status is "Dormant", system will show popup approve

**Flow of events:**
-	Don't allow withdraw amount is hold. 
-	Can withdraw amount smaller or equal {Balance - Hold amount}.
-	Don't allow debit side that amount larger than {Balance - Hold amount}.
-	Support storing ID information relation to keep balance.
-	Allow collection fees for this transaction.

**Database:**
- DepositAccount
- DepositEarmark
- DepositStatement
- DepositHistory 

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| Case collect fee (fee amount > 0) | | | | |
| 1 | D | CASH | Fee amount | Currency of deposit account |
| 1 | C | IFCC (income) | Fee amount | Currency of deposit account |

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Hold amount > 0 | Hold amount khác 0 |
|  | Hold amount must be less than available balance | Hold amount < Available balance - Initial deposit amount |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- This transaction is not allowed to reverse.

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_CUSTOMER | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 4 | TRAN_FEE/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |
| 5 | TRAN_FEE/GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>