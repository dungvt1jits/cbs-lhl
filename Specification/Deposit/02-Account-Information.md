# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/DepositAccount/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_SEARCH_DEPOSIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account id | id | `Number` |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Currency code | currency_code | String | 3 |  |
| 6 | Customer code | customer_code | String | 8 |  |
| 7 | Customer type | customer_type | String |  |  |
| 8 | Catalogue code | catalog_code | String | 8 |  |
| 9 | Status | deposit_status | String |  |  |
| 10 | Deposit type | deposit_type | String |  |  |
| 11 | MFA | mfa | String |  |  |
| 12 | Old a/c no | reference_number | String |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/DepositAccount/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_ADSEARCH_DEPOSIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |
| 2 | Account name | account_name | No | String |  |  |  |
| 3 | Currency code | currency_code | No | String | 3 |  |  |
| 4 | Customer code | customer_code | No | String | 8 |  |  |
| 5 | Customer type | customer_type | No | String |  |  |  |
| 6 | Catalogue code | catalog_code | No | String | 8 |  |  |
| 7 | Status | deposit_status | No | String |  |  |  |
| 8 | Deposit type | deposit_type | No | String |  |  |  |
| 9 | MFA | mfa | No | String |  |  |  |
| 10 | Old a/c no | reference_number | No | String |  |  |  |
| 11 | Page index | page_index | No | `Number` |  |  |  |
| 12 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{

}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account id | id | `Number` |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Currency code | currency_code | String | 3 |  |
| 6 | Customer code | customer_code | String | 8 |  |
| 7 | Customer type | customer_type | String |  |  |
| 8 | Catalogue code | catalog_code | String | 8 |  |
| 9 | Status | deposit_status | String |  |  |
| 10 | Deposit type | deposit_type | String |  |  |
| 11 | MFA | mfa | String |  |  |
| 12 | Old a/c no | reference_number | String |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add 
[Tham khảo thông tin chi tiết tại đây](Specification/Deposit/04 DPT_OPN Open New Deposit Account)<br>

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/DepositAccount/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_VIEW_DEPOSIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Deposit id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 1
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Deposit id | id | `Number` |  |  |
| 2 | Account number def | account_number_def | String | 36 | tự gen sau khi giao dịch open thành công |
| 3 | Account number | account_number | String |  | tự gen sau khi giao dịch open thành công |
| 4 | Passbook or receipt number | passbook_or_receipt_number | String |  | cập nhật sau khi làm giao dịch issue stock |
| 5 | Account name | account_name | String |  | lấy từ giao dịch |
| 6 | Currency code | currency_code | String |  | lấy từ catalog |
| 7 | Secure currency | secure_currency | String |  | không dùng, xem xét xóa đi |
| 8 | Account holder type | customer_type | String |  | lấy từ giao dịch |
| 9 | Account holder | customer_id | `Number` |  | lấy từ giao dịch |
| 10 | Branch id | branch_id | `Number` |  | lấy từ branch làm giao dịch |
| 11 | Account status | deposit_status | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 12 | Passbook or receipt status | passbook_or_receipt_status | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 13 | Opening resident status | acrsd | String |  | lấy từ customer |
| 14 | Customer resident status |  | String |  | lấy từ customer |
| 15 | Catalogue id | catalog_id | `Number` |  | lấy từ giao dịch |
| 16 | Deposit type | deposit_type | String |  | lấy từ catalog |
| 17 | Account type | deposit_group | `Number` |  | lấy từ giao dịch |
| 18 | Deposit purpose | deposit_purpose | String |  | lấy từ catalog |
| 19 | Deposit classification | deposit_classification | String |  | lấy từ catalog |
| 20 | Passbook or statement (P/S) | passbook_or_statement | String |  | lấy từ catalog |
| 21 | Interest accrual rate | intacrrt | `Number` |  | lấy từ catalog |
| 22 | Tenor 1 | tenor | `Number` |  | lấy từ catalog |
| 23 | Tenor unit 1 | tenor_unit | String |  | lấy từ catalog |
| 24 | Tenor 2 | tenor_2 | `Number` |  | lấy từ catalog |
| 25 | Tenor unit 2 | tenor_unit_2 | String |  | lấy từ catalog |
| 26 | Begin of tenor | begin_of_tenor | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 27 | End of tenor | end_of_tenor | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 28 | Open date | open_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 29 | Close date | close_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 30 | Last transaction date | last_transaction_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 31 | Last date system transfer interest to due | last_transfer_interest_to_due | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 32 | Dormant date | dormant_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 33 | Last change dormant to normal date | last_change_dormant_to_normal_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 34 | Created by | user_created | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 35 | Approved by | user_approved | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 36 | Account manager staff id | account_manager_staff_id | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 37 | Business purpose code | business_purpose_code | String |  | lấy từ giao dịch |
| 38 | Current balance | current_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 39 | Available balance  | available_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công, current_balance - minimum_deposit_amount - earmark_book_amount + CRLIMIT, `cần check lại` |
| 40 | Minimum deposit amount | minimum_deposit_amount | `Number` |  | lấy từ catalog |
| 41 | Minimum amount to dormant | minimum_dormant_amount | `Number` |  | lấy từ catalog |
| 42 | Earmark (block) amount | earmark_book_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 43 | Initial deposit amount | initial_deposit_amount | `Number` |  | lấy từ catalog |
| 44 | Periodic deposit amount | periodic_deposit_amount | `Number` |  | lấy từ catalog |
| 45 | Month average balance | month_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 46 | Quarter average balance | quarter_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 47 | Semi-annual average balance | semi_annual_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 48 | Year average balance | year_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 49 | Amount for interest calculation | icbal | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 50 | Float amount | float_amount | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu, `cần check lại` |
| 51 | With holding tax accrual | tax_accrual | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu, `cần check lại` |
| 52 | Interest accrual | interest_accrual | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 53 | Interest due | interest_due | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 54 | Interest not paid | interest_not_paid | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công, `INTNYPD` |
| 55 | Interest paid | interest_paid | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 56 | Deposit amount | deposit_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 57 | Withdraw amount | withdraw_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 58 | Week debit | week_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 59 | Week credit | week_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 60 | Month debit | month_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 61 | Month credit | month_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 62 | Quarter debit | quarter_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 63 | Quarter credit | quarter_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 64 | Semi-annual debit | semi_annual_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 65 | Semi-annual credit | semi_annual_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 66 | Year debit | year_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 67 | Year credit | year_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 68 | Accumulate dormant fee | dormant_accumulate | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu, `cần check lại` |
| 69 | Last dormant fee collection date | dormant_date | `Date time` |  | kiểm tra code để lấy đúng thông tin từ đâu, `cần check lại` |
| 70 | Accumulate maintenance fee | maintenance_accumulate | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu, `cần check lại` |
| 71 | Last maintenance fee collection date | maintenance_date | `Date time` |  | kiểm tra code để lấy đúng thông tin từ đâu, `cần check lại` |
| 72 | Deposit tenor | deposit_tenor | `Number` |  | lấy từ catalog |
| 73 | Deposit tenor unit | deposit_tenor_unit | String |  | lấy từ catalog |
| 74 | Interest tenor | interest_tenor | `Number` |  | lấy từ catalog |
| 75 | Interest tenor unit | interest_tenor_unit | String |  | lấy từ catalog |
| 76 | Minimum tenor | minimum_tenor | `Number` |  | lấy từ catalog |
| 77 | Minimum tenor unit | minimum_tenor_unit | String |  | lấy từ catalog |
| 78 | Multiple deposit allow | multiple_deposit_allow | String |  | lấy từ catalog |
| 79 | Multiple withdrawal allow | multiple_withdrawal_allow | String |  | lấy từ catalog |
| 80 | Early withdrawal (Y/N) | early_withdrawal | String |  | lấy từ catalog |
| 81 | Minimum tenor allow early withdrawal | minimum_tenor_allow_early_withdrawal | `Number` |  | lấy từ catalog |
| 82 | Minimum tenor allow early withdrawal unit | minimum_tenor_allow_early_withdrawal_unit | String |  | lấy từ catalog |
| 83 | Credit interest (Y/N) | credit_interest | String |  | lấy từ catalog |
| 84 | Credit interest tenor | credit_interest_tenor | `Number` |  | lấy từ catalog |
| 85 | Credit interest tenor unit | credit_interest_tenor_unit | String |  | lấy từ catalog |
| 86 | The day of tenor for crediting interest | crediting_interest | `Number` |  | lấy từ catalog |
| 87 | Dormant period | dormant_period | `Number` |  | lấy từ catalog |
| 88 | Dormant period unit | dormant_period_unit | String |  | lấy từ catalog |
| 89 | Rollover option | rollover | String |  | lấy từ giao dịch |
| 90 | Rollover to catalogue | rollover_to_catalog | `Number` |  | lấy từ catalog |
| 91 | Interest due on holiday | interest_due_on_holiday | `Number` |  | lấy từ catalog |
| 92 | Principal due on holiday | principal_due_on_holiday | `Number` |  | lấy từ catalog |
| 93 | Statement tenor | statement_tenor | `Number` |  | lấy từ catalog |
| 94 | Statement tenor unit | statement_tenor_unit | String |  | lấy từ catalog |
| 95 | Statement format | statement_format | String |  | lấy từ catalog |
| 96 | Interest paid type | intpdcd | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 97 | Interest paid account | intac | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 98 | Remark | remark | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 99 | Reference number | reference_number | String |  | use for conversion old account id,… |
|  | Tariff code | tariff_code | `Number` |  | lấy từ catalog |
|  | Master FD account | macno | String |  | lấy từ giao dịch |
|  | Deposit sub type | deposit_sub_type | String |  | lấy từ giao dịch |
|  | Interest prepaid | intpre | `Number` |  |  |
|  | Interest payable/receiveble | intpbl | `Number` |  |  |
|  | Interest overdue, not paid | intovd | `Number` |  |  |
|  | Interest suspense | intspamt | `Number` |  |  |
|  | Total income amount | inamt | `Number` |  | use for fund investment account |
|  | Total expense amount | examt | `Number` |  | use for fund investment account |
|  | Reverse balance | rvbalance | `Number` |  |  (for OD,…) |
|  | Reverse month average balance | rvmavgamt | `Number` |  |  |
|  | Reverse quarter average balance | rvqavgamt | `Number` |  |  |
|  | Reverse semi-annual average balance | rvhavgamt | `Number` |  |  |
|  | Reverse year average balance | rvyavgamt | `Number` |  |  |
|  | Reverse interest accrual amount | rvintamt | `Number` |  | rounding 5 digit |
|  | Reverse interest paid | rintpaid | `Number` |  |  |
|  | Reverse interest prepaid | rintpre | `Number` |  |  |
|  | Reverse interest payable/receiveble | rvintpbl | `Number` |  |  |
|  | Reverse interest due | rvintdue | `Number` |  |  |
|  | Reverse interest overdue, not paid | rvintovd | `Number` |  |  |
|  | Reverse interest suspense | rvintspamt | `Number` |  |  |
|  | Reverse interest not paid | rvintnypd | `Number` |  | previous tenor, not rollover |
|  | Previous status | psts | String |  |  |
|  | Ties to tariff? | trftied | String |  | không dùng, mặc định: Y, xem xét xóa đi |
|  | User define field | udfield_1 | String |  | reserve, json structure |
|  | Periodic deposit tenor | periodic | `Number` |  | không dùng, xem xét xóa đi |
|  | Periodic deposit tenor unit | periodicun | String |  | không dùng, xem xét xóa đi |
|  | Block by | blkby | String |  |  |
|  | Customer code | customer_code | String |  | `trả thêm` |
|  | Customer name | customer_name | String |  | `trả thêm` |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Account manager staff code | account_manager_staff_code | String |  | `trả thêm` |
|  | Account manager staff name | account_manager_staff_name | String |  | `trả thêm` |
|  | Catalogue code | catalog_code | String |  | `trả thêm` |
|  | Business purpose name | business_purpose_name | String |  | `trả thêm` |
|  | Rollover to catalogue code | rollover_to_catalog_code | String |  | `trả thêm`, nếu `rollover_to_catalog` null thì trả null |
|  | Rollover to catalogue name | rollover_to_catalog_name | String |  | `trả thêm`, nếu `rollover_to_catalog` null thì trả null |
|  | Agent hub referral | agent_hub_referral | String |  | `trả thêm` |
|  | Relation customers | relation_customers | Array object |  | `trả thêm` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/DepositAccount/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_UPDATE_DEPOSIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Deposit id | id | `Yes` | `Number` |  |  | không được sửa |
| 2 | Account name | account_name | `Yes` | String |  |  |  |
| 3 | Business purpose code | business_purpose_code | `Yes` | String |  |  |  |
| 4 | Minimum deposit amount | minimum_deposit_amount | No | `Number` |  |  | số có hai số thập phân |
| 5 | Initial deposit amount | initial_deposit_amount | `Yes` | `Number` |  |  | số có hai số thập phân |
| 6 | Interest tenor | interest_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 7 | Interest tenor unit | interest_tenor_unit | `Yes` | String |  |  |  |
| 8 | Minimum tenor | minimum_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 9 | Minimum tenor unit | minimum_tenor_unit | `Yes` | String |  |  |  |
| 10 | Multiple deposit allow | multiple_deposit_allow | `Yes` | String |  |  |  |
| 11 | Multiple withdrawal allow | multiple_withdrawal_allow | `Yes` | String |  |  |  |
| 12 | Early withdrawal (Y/N) | early_withdrawal | `Yes` | String |  |  |  |
| 13 | Minimum tenor allow early withdrawal | minimum_tenor_allow_early_withdrawal | `Yes` | `Number` |  |  | số nguyên dương |
| 14 | Minimum tenor allow early withdrawal unit | minimum_tenor_allow_early_withdrawal_unit | `Yes` | String |  |  |  |
| 15 | Credit interest (Y/N) | credit_interest | `Yes` | String |  |  |  |
| 16 | Credit interest tenor | credit_interest_tenor | `Yes` | `Number` |  |  | số nguyên dương |
| 17 | Credit interest tenor unit | credit_interest_tenor_unit | `Yes` | String |  |  |  |
| 18 | The day of tenor for crediting interest | crediting_interest | No | `Number` |  |  | số nguyên dương |
| 19 | Dormant period | dormant_period | `Yes` | `Number` |  |  | số nguyên dương |
| 20 | Dormant period unit | dormant_period_unit | `Yes` | String |  |  |  |
| 21 | Rollover option | rollover | `Yes` | String |  |  |  |
| 22 | Rollover to catalogue | rollover_to_catalog | No | `Number` |  | `Null` |  |
| 23 | Interest due on holiday | interest_due_on_holiday | No | `Number` |  |  |  |
| 24 | Principal due on holiday | principal_due_on_holiday | No | `Number` |  |  |  |
| 25 | Statement tenor | statement_tenor | No | String |  |  |  |
| 26 | Statement tenor unit | statement_tenor_unit | No | `Number` |  |  |  |
| 27 | Statement format | statement_format | No | String |  |  |  |
| 28 | Account number def | account_number_def | `Yes` | String |  |  | Không được sửa |
| 29 | Module code | module_code | `Yes` | String |  |  | `DPT` |
| 30 | Approve modify | approve_modify | `Yes` | `Boolean` |  |  |  |
| 31 | IFC data | list_ifc_balance | No |  Array Object |  |  |  |
|  | Module code | module_code | No | String |  |  | `DPT` |
|  | IFC code | ifc_code | No | `Number` |  |  | Không được sửa |
|  | Value base | value_base | No | String |  |  | Không được sửa |
|  | IFC value | ifc_value | No | `Number` |  |  | Không được sửa, số có 5 số thập phân |
|  | Margin value | margin_value | No | `Number` |  |  | Được phép sửa, số có 5 số thập phân |
|  | Amount | amount | No | `Number` |  |  | Không được sửa, số có 5 số thập phân |
|  | Paid | paid | No | `Number` |  |  | Không được sửa, số có 5 số thập phân |
|  | Status | ifc_status | No | String |  |  | Được phép sửa |
|  | Last date time | last_datetime | No | String |  |  | lấy ngày working date |
|  | Amount payable | amtpbl | No | `Number` |  |  | Không được sửa, số có 5 số thập phân |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Deposit id | id | `Number` |  |  |
| 2 | Account number def | account_number_def | String | 36 | tự gen sau khi giao dịch open thành công |
| 3 | Account number | account_number | String |  | tự gen sau khi giao dịch open thành công |
| 4 | Passbook or receipt number | passbook_or_receipt_number | String |  | cập nhật sau khi làm giao dịch issue stock |
| 5 | Account name | account_name | String |  | lấy từ giao dịch |
| 6 | Currency code | currency_code | String |  | lấy từ catalog |
| 7 | Secure currency | secure_currency | String |  | không dùng, xem xét xóa đi |
| 8 | Account holder type | customer_type | String |  | lấy từ giao dịch |
| 9 | Account holder | customer_code | String |  | lấy từ giao dịch |
| 10 | Branch id | branch_id | String |  | lấy từ branch làm giao dịch |
| 11 | Account status | deposit_status | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 12 | Passbook or receipt status | passbook_or_receipt_status | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 13 | Opening resident status | acrsd | String |  | lấy từ customer |
| 14 | Customer resident status |  | String |  | lấy từ customer |
| 15 | Catalogue code | catalog_code | String |  | lấy từ giao dịch |
| 16 | Deposit type | deposit_type | String |  | lấy từ catalog |
| 17 | Account type | deposit_group | `Number` |  | lấy từ giao dịch |
| 18 | Deposit purpose | deposit_purpose | String |  | lấy từ catalog |
| 19 | Deposit classification | deposit_classification | String |  | lấy từ catalog |
| 20 | Passbook or statement (P/S) | passbook_or_statement | String |  | lấy từ catalog |
| 21 | Interest accrual rate | intacrrt | `Number` |  | lấy từ catalog |
| 22 | Tenor 1 | tenor | `Number` |  | lấy từ catalog |
| 23 | Tenor unit 1 | tenor_unit | String |  | lấy từ catalog |
| 24 | Tenor 2 | tenor_2 | `Number` |  | lấy từ catalog |
| 25 | Tenor unit 2 | tenor_unit_2 | String |  | lấy từ catalog |
| 26 | Begin of tenor | begin_of_tenor | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 27 | End of tenor | end_of_tenor | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 28 | Open date | open_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 29 | Close date | close_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 30 | Last transaction date | last_transaction_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 31 | Last date system transfer interest to due | last_transfer_interest_to_due | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 32 | Dormant date | dormant_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 33 | Last change dormant to normal date | last_change_dormant_to_normal_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 34 | Created by | user_created | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 35 | Approved by | user_approved | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 36 | Account manager staff id | account_manager_staff_id | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 37 | Business purpose code | business_purpose_code | String |  | lấy từ giao dịch |
| 38 | Current balance | current_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 39 | Available balance  | available_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công, current_balance - minimum_deposit_amount - earmark_book_amount + CRLIMIT |
| 40 | Minimum deposit amount | minimum_deposit_amount | `Number` |  | lấy từ catalog |
| 41 | Minimum amount to dormant | minimum_dormant_amount | `Number` |  | lấy từ catalog |
| 42 | Earmark (block) amount | earmark_book_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 43 | Initial deposit amount | initial_deposit_amount | `Number` |  | lấy từ catalog |
| 44 | Periodic deposit amount | periodic_deposit_amount | `Number` |  | lấy từ catalog |
| 45 | Month average balance | month_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 46 | Quarter average balance | quarter_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 47 | Semi-annual average balance | semi_annual_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 48 | Year average balance | year_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 49 | Amount for interest calculation | icbal | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 50 | Float amount |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 51 | With holding tax accrual |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 52 | Interest accrual | interest_accrual | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 53 | Interest due | interest_due | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 54 | Interest not paid | interest_not_paid | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 55 | Interest paid | interest_paid | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 56 | Deposit amount | deposit_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 57 | Withdraw amount | withdraw_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 58 | Week debit | week_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 59 | Week credit | week_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 60 | Month debit | month_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 61 | Month credit | month_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 62 | Quarter debit | quarter_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 63 | Quarter credit | quarter_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 64 | Semi-annual debit | semi_annual_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 65 | Semi-annual credit | semi_annual_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 66 | Year debit | year_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 67 | Year credit | year_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 68 | Accumulate dormant fee |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 69 | Last dormant fee collection date |  | `Date time` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 70 | Accumulate maintenance fee |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 71 | Last maintenance fee collection date |  | `Date time` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 72 | Deposit tenor | deposit_tenor | `Number` |  | lấy từ catalog |
| 73 | Deposit tenor unit | deposit_tenor_unit | String |  | lấy từ catalog |
| 74 | Interest tenor | interest_tenor | `Number` |  | lấy từ catalog |
| 75 | Interest tenor unit | interest_tenor_unit | String |  | lấy từ catalog |
| 76 | Minimum tenor | minimum_tenor | `Number` |  | lấy từ catalog |
| 77 | Minimum tenor unit | minimum_tenor_unit | String |  | lấy từ catalog |
| 78 | Multiple deposit allow | multiple_deposit_allow | String |  | lấy từ catalog |
| 79 | Multiple withdrawal allow | multiple_withdrawal_allow | String |  | lấy từ catalog |
| 80 | Early withdrawal (Y/N) | early_withdrawal | String |  | lấy từ catalog |
| 81 | Minimum tenor allow early withdrawal | minimum_tenor_allow_early_withdrawal | `Number` |  | lấy từ catalog |
| 82 | Minimum tenor allow early withdrawal unit | minimum_tenor_allow_early_withdrawal_unit | String |  | lấy từ catalog |
| 83 | Credit interest (Y/N) | credit_interest | String |  | lấy từ catalog |
| 84 | Credit interest tenor | credit_interest_tenor | `Number` |  | lấy từ catalog |
| 85 | Credit interest tenor unit | credit_interest_tenor_unit | String |  | lấy từ catalog |
| 86 | The day of tenor for crediting interest | crediting_interest | `Number` |  | lấy từ catalog |
| 87 | Dormant period | dormant_period | `Number` |  | lấy từ catalog |
| 88 | Dormant period unit | dormant_period_unit | String |  | lấy từ catalog |
| 89 | Rollover option | rollover | String |  | lấy từ giao dịch |
| 90 | Rollover to catalogue | rollover_to_catalog | `Number` |  | lấy từ catalog |
| 91 | Interest due on holiday | interest_due_on_holiday | `Number` |  | lấy từ catalog |
| 92 | Principal due on holiday | principal_due_on_holiday | `Number` |  | lấy từ catalog |
| 93 | Statement tenor | statement_tenor | `Number` |  | lấy từ catalog |
| 94 | Statement tenor unit | statement_tenor_unit | String |  | lấy từ catalog |
| 95 | Statement format | statement_format | String |  | lấy từ catalog |
| 96 | Interest paid type | intpdcd | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 97 | Interest paid account | intac | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 98 | Remark | remark | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 99 | Reference number | reference_number | String |  | use for conversion old account id,… |
|  | Tariff code | tariff_code | `Number` |  | lấy từ catalog |
|  | Master FD account | macno | String |  | lấy từ giao dịch |
|  | Deposit sub type | deposit_sub_type | String |  | lấy từ giao dịch |
|  | Interest prepaid | intpre | `Number` |  |  |
|  | Interest payable/receiveble | intpbl | `Number` |  |  |
|  | Interest overdue, not paid | intovd | `Number` |  |  |
|  | Interest suspense | intspamt | `Number` |  |  |
|  | Total income amount | inamt | `Number` |  | use for fund investment account |
|  | Total expense amount | examt | `Number` |  | use for fund investment account |
|  | Reverse balance | rvbalance | `Number` |  |  (for OD,…) |
|  | Reverse month average balance | rvmavgamt | `Number` |  |  |
|  | Reverse quarter average balance | rvqavgamt | `Number` |  |  |
|  | Reverse semi-annual average balance | rvhavgamt | `Number` |  |  |
|  | Reverse year average balance | rvyavgamt | `Number` |  |  |
|  | Reverse interest accrual amount | rvintamt | `Number` |  | rounding 5 digit |
|  | Reverse interest paid | rintpaid | `Number` |  |  |
|  | Reverse interest prepaid | rintpre | `Number` |  |  |
|  | Reverse interest payable/receiveble | rvintpbl | `Number` |  |  |
|  | Reverse interest due | rvintdue | `Number` |  |  |
|  | Reverse interest overdue, not paid | rvintovd | `Number` |  |  |
|  | Reverse interest suspense | rvintspamt | `Number` |  |  |
|  | Reverse interest not paid | rvintnypd | `Number` |  | previous tenor, not rollover |
|  | Previous status | psts | String |  |  |
|  | Ties to tariff? | trftied | String |  | không dùng, mặc định: Y, xem xét xóa đi |
|  | User define field | udfield_1 | String |  | reserve, json structure |
|  | Periodic deposit tenor | periodic | `Number` |  | không dùng, xem xét xóa đi |
|  | Periodic deposit tenor unit | periodicun | String |  | không dùng, xem xét xóa đi |
|  | Block by | blkby | String |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/DepositAccount/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_DELETE_DEPOSIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Deposit id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 1
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Deposit id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 1
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list signature by customer code
- [Get information của "Customer Media Files](Specification/Customer/04 Customer Media Files)

# 8. Get list IFC by deposit account number def
## 8.1 Field description
### Request message
**HTTP Method:** ``

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `IFC_LISTIFCBYDEFACCNO_IFCBALANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |
| 2 | Module code | module_code | `Yes` | String |  |  | `DPT` |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |
| 4 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC id | ifc_id | `Number` |  |  |
| 2 | IFC code | ifc_code | `Number` |  |  |
| 3 | IFC name | ifc_name | String |  |  |
| 4 | Base value | base_value | String |  |  |
| 5 | Is linked | is_linked | String |  |  |  |
| 6 | IFC value | ifc_value | `Number` |  | số có 5 số thập phân |
| 7 | Margin value | margin_value | `Number` |  | số có 5 số thập phân |
| 8 | Status | ifc_status | String |  |  |
| 9 | Outstanding | ifc_outstanding | `Number` |  | số có 5 số thập phân |
| 10 | Paid | ifc_paid | `Number` |  | số có 5 số thập phân |

**Example:**
```json
{
    
}
```

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |

# 9. View by account number def
## 9.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_VIEWBYDEFACNO_DEPOSIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |

**Example:**
```json
{
    "account_number_def": ""
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Deposit id | id | `Number` |  |  |
| 2 | Account number def | account_number_def | String | 36 | tự gen sau khi giao dịch open thành công |
| 3 | Account number | account_number | String |  | tự gen sau khi giao dịch open thành công |
| 4 | Passbook or receipt number | passbook_or_receipt_number | String |  | cập nhật sau khi làm giao dịch issue stock |
| 5 | Account name | account_name | String |  | lấy từ giao dịch |
| 6 | Currency code | currency_code | String |  | lấy từ catalog |
| 7 | Secure currency | secure_currency | String |  | không dùng, xem xét xóa đi |
| 8 | Account holder type | customer_type | String |  | lấy từ giao dịch |
| 9 | Account holder | customer_id | `Number` |  | lấy từ giao dịch |
| 10 | Branch id | branch_id | `Number` |  | lấy từ branch làm giao dịch |
| 11 | Account status | deposit_status | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 12 | Passbook or receipt status | passbook_or_receipt_status | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 13 | Opening resident status | acrsd | String |  | lấy từ customer |
| 14 | Customer resident status |  | String |  | lấy từ customer |
| 15 | Catalogue id | catalog_id | `Number` |  | lấy từ giao dịch |
| 16 | Deposit type | deposit_type | String |  | lấy từ catalog |
| 17 | Account type | deposit_group | `Number` |  | lấy từ giao dịch |
| 18 | Deposit purpose | deposit_purpose | String |  | lấy từ catalog |
| 19 | Deposit classification | deposit_classification | String |  | lấy từ catalog |
| 20 | Passbook or statement (P/S) | passbook_or_statement | String |  | lấy từ catalog |
| 21 | Interest accrual rate | intacrrt | `Number` |  | lấy từ catalog |
| 22 | Tenor 1 | tenor | `Number` |  | lấy từ catalog |
| 23 | Tenor unit 1 | tenor_unit | String |  | lấy từ catalog |
| 24 | Tenor 2 | tenor_2 | `Number` |  | lấy từ catalog |
| 25 | Tenor unit 2 | tenor_unit_2 | String |  | lấy từ catalog |
| 26 | Begin of tenor | begin_of_tenor | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 27 | End of tenor | end_of_tenor | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 28 | Open date | open_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 29 | Close date | close_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 30 | Last transaction date | last_transaction_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 31 | Last date system transfer interest to due | last_transfer_interest_to_due | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 32 | Dormant date | dormant_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 33 | Last change dormant to normal date | last_change_dormant_to_normal_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 34 | Created by | user_created | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 35 | Approved by | user_approved | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 36 | Account manager staff id | account_manager_staff_id | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 37 | Business purpose code | business_purpose_code | String |  | lấy từ giao dịch |
| 38 | Current balance | current_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 39 | Available balance  | available_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công, current_balance - minimum_deposit_amount - earmark_book_amount + CRLIMIT |
| 40 | Minimum deposit amount | minimum_deposit_amount | `Number` |  | lấy từ catalog |
| 41 | Minimum amount to dormant | minimum_dormant_amount | `Number` |  | lấy từ catalog |
| 42 | Earmark (block) amount | earmark_book_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 43 | Initial deposit amount | initial_deposit_amount | `Number` |  | lấy từ catalog |
| 44 | Periodic deposit amount | periodic_deposit_amount | `Number` |  | lấy từ catalog |
| 45 | Month average balance | month_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 46 | Quarter average balance | quarter_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 47 | Semi-annual average balance | semi_annual_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 48 | Year average balance | year_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 49 | Amount for interest calculation | icbal | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 50 | Float amount |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 51 | With holding tax accrual |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 52 | Interest accrual | interest_accrual | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 53 | Interest due | interest_due | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 54 | Interest not paid | interest_not_paid | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 55 | Interest paid | interest_paid | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 56 | Deposit amount | deposit_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 57 | Withdraw amount | withdraw_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 58 | Week debit | week_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 59 | Week credit | week_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 60 | Month debit | month_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 61 | Month credit | month_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 62 | Quarter debit | quarter_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 63 | Quarter credit | quarter_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 64 | Semi-annual debit | semi_annual_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 65 | Semi-annual credit | semi_annual_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 66 | Year debit | year_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 67 | Year credit | year_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 68 | Accumulate dormant fee |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 69 | Last dormant fee collection date |  | `Date time` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 70 | Accumulate maintenance fee |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 71 | Last maintenance fee collection date |  | `Date time` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 72 | Deposit tenor | deposit_tenor | `Number` |  | lấy từ catalog |
| 73 | Deposit tenor unit | deposit_tenor_unit | String |  | lấy từ catalog |
| 74 | Interest tenor | interest_tenor | `Number` |  | lấy từ catalog |
| 75 | Interest tenor unit | interest_tenor_unit | String |  | lấy từ catalog |
| 76 | Minimum tenor | minimum_tenor | `Number` |  | lấy từ catalog |
| 77 | Minimum tenor unit | minimum_tenor_unit | String |  | lấy từ catalog |
| 78 | Multiple deposit allow | multiple_deposit_allow | String |  | lấy từ catalog |
| 79 | Multiple withdrawal allow | multiple_withdrawal_allow | String |  | lấy từ catalog |
| 80 | Early withdrawal (Y/N) | early_withdrawal | String |  | lấy từ catalog |
| 81 | Minimum tenor allow early withdrawal | minimum_tenor_allow_early_withdrawal | `Number` |  | lấy từ catalog |
| 82 | Minimum tenor allow early withdrawal unit | minimum_tenor_allow_early_withdrawal_unit | String |  | lấy từ catalog |
| 83 | Credit interest (Y/N) | credit_interest | String |  | lấy từ catalog |
| 84 | Credit interest tenor | credit_interest_tenor | `Number` |  | lấy từ catalog |
| 85 | Credit interest tenor unit | credit_interest_tenor_unit | String |  | lấy từ catalog |
| 86 | The day of tenor for crediting interest | crediting_interest | `Number` |  | lấy từ catalog |
| 87 | Dormant period | dormant_period | `Number` |  | lấy từ catalog |
| 88 | Dormant period unit | dormant_period_unit | String |  | lấy từ catalog |
| 89 | Rollover option | rollover | String |  | lấy từ giao dịch |
| 90 | Rollover to catalogue | rollover_to_catalog | `Number` |  | lấy từ catalog |
| 91 | Interest due on holiday | interest_due_on_holiday | `Number` |  | lấy từ catalog |
| 92 | Principal due on holiday | principal_due_on_holiday | `Number` |  | lấy từ catalog |
| 93 | Statement tenor | statement_tenor | `Number` |  | lấy từ catalog |
| 94 | Statement tenor unit | statement_tenor_unit | String |  | lấy từ catalog |
| 95 | Statement format | statement_format | String |  | lấy từ catalog |
| 96 | Interest paid type | intpdcd | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 97 | Interest paid account | intac | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 98 | Remark | remark | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 99 | Reference number | reference_number | String |  | use for conversion old account id,… |
|  | Tariff code | tariff_code | `Number` |  | lấy từ catalog |
|  | Master FD account | macno | String |  | lấy từ giao dịch |
|  | Deposit sub type | deposit_sub_type | String |  | lấy từ giao dịch |
|  | Interest prepaid | intpre | `Number` |  |  |
|  | Interest payable/receiveble | intpbl | `Number` |  |  |
|  | Interest overdue, not paid | intovd | `Number` |  |  |
|  | Interest suspense | intspamt | `Number` |  |  |
|  | Total income amount | inamt | `Number` |  | use for fund investment account |
|  | Total expense amount | examt | `Number` |  | use for fund investment account |
|  | Reverse balance | rvbalance | `Number` |  |  (for OD,…) |
|  | Reverse month average balance | rvmavgamt | `Number` |  |  |
|  | Reverse quarter average balance | rvqavgamt | `Number` |  |  |
|  | Reverse semi-annual average balance | rvhavgamt | `Number` |  |  |
|  | Reverse year average balance | rvyavgamt | `Number` |  |  |
|  | Reverse interest accrual amount | rvintamt | `Number` |  | rounding 5 digit |
|  | Reverse interest paid | rintpaid | `Number` |  |  |
|  | Reverse interest prepaid | rintpre | `Number` |  |  |
|  | Reverse interest payable/receiveble | rvintpbl | `Number` |  |  |
|  | Reverse interest due | rvintdue | `Number` |  |  |
|  | Reverse interest overdue, not paid | rvintovd | `Number` |  |  |
|  | Reverse interest suspense | rvintspamt | `Number` |  |  |
|  | Reverse interest not paid | rvintnypd | `Number` |  | previous tenor, not rollover |
|  | Previous status | psts | String |  |  |
|  | Ties to tariff? | trftied | String |  | không dùng, mặc định: Y, xem xét xóa đi |
|  | User define field | udfield_1 | String |  | reserve, json structure |
|  | Periodic deposit tenor | periodic | `Number` |  | không dùng, xem xét xóa đi |
|  | Periodic deposit tenor unit | periodicun | String |  | không dùng, xem xét xóa đi |
|  | Block by | blkby | String |  |  |
|  | Customer code | customer_code | String |  | `trả thêm` |
|  | Customer name | customer_name | String |  | `trả thêm` |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Account manager staff code | account_manager_staff_code | String |  | `trả thêm` |
|  | Account manager staff name | account_manager_staff_name | String |  | `trả thêm` |
|  | Catalogue code | catalog_code | String |  | `trả thêm` |
|  | Business purpose name | business_purpose_name | String |  | `trả thêm` |
|  | Agent hub referral | agent_hub_referral | String |  | `trả thêm` |
|  | Relation customers | relation_customers | Array object |  | `trả thêm` |

**Example:**
```json
{
    
}
```

## 9.2 Transaction flow

## 9.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 10. Print
## 10.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_PRINTPASSBOOK`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number def | account_number_def | `Yes` | String |  |  |  |
| 2 | From date | from_date | `Yes` | `Date Time` |  | Working date |  |
| 3 | From line | from_line | `Yes` | `Number` |  | 1 |  |

**Example:**
```json
{
    "account_number_def": "00999USDDPT00000084",
    "from_date": "2022-04-12T09:55:42.926Z",
    "from_line": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Account name | account_name | String |  |  |
| 4 | Print | print | String |  |  |
| 5 | Passbook or receipt number | passbook_or_receipt_number | String |  |  |
| 6 | Open date | open_date | `Date Time` |  |  |
| 7 | Rep id | rep_id | String |  |  |
| 8 | Full name | full_name | String |  |  |
| 9 | Address | address | String |  |  |
| 10 | Deposit type | deposit_type | String |  |  |
| 11 | From date | from_date | `Date Time` |  |  |
| 12 | From line | from_line | `Number` |  |  |
| 13 | Currency code | currency_code | String |  |  |
| 14 | Managing branch | managing_branch | String |  |  |
| 15 | Open teller | open_teller | String |  |  |
| 16 | Print page | pg_print | `Number` |  |  |
| 17 | Total lines of a detail page | pgbt_from | `Number` |  |  |
| 18 | Add a page break at the cursor line position | pb_line | `Number` |  |  |
| 19 | Dataset | dataset | Array Object |  |  |
|  | No | no | `Number` |  |  |
|  | Date | date | `Date` |  |  |
|  | Time | time | `Time` |  |  |
|  | Tran reference | trans_reference | String |  |  |
|  | Code | code | String |  |  |
|  | Account infor | account_infor | String |  |  |
|  | Withdrawal | withdrawal | `Number` |  | có 2 số thập phân |
|  | Deposit | deposit | `Number` |  | có 2 số thập phân |
|  | Balance | balance | `Number` |  | có 2 số thập phân |
|  | Teller no | teller_no | String |  |  |
|  | Description | description | String |  |  |

**Example:**
```json
{
    
}
```

## 10.2 Transaction flow

## 10.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# Business rule and constrains
- Customer of the bank can have many accounts includes current, demand saving, term saving, investment, installment saving, capital, etc.
    - Current account: Non-interest bearing (or interest bearing depend on bank policy), use check and statement. 
    - Demand saving: Interest bearing (or not depend on bank policy), use passbook or receipt of deposit.  
    - Term saving: saving for 1 month, 3 months, etc. Interest paid on monthly, quarterly, half-yearly or yearly. Rollover when maturity, depends on customer demand and bank policy.
    - Investment: investment money and has minimum profit rate (interest rate). Management fee included. Can transfer fund to other customer.
    - Installment saving: frequently deposit money to the account, interest calculate on average balance. 
    - Capital: Customer capital deposit in the bank.
- Account holder can be single customer or group of customers, or customer linkage with appropriate privilege to account.
- Account can link to customer, to others account. The account linkages are usefully for automation feature e.g. sweeping, automatic fund transfer, interest automatic payment, etc.
- Account status can be defined by the bank, e.g. status and allowance action as list below:

| Code | Status | Description | Allowance action |
| ---- | ------ | ----------- | ---------------- |
| W | New | Customer open new current or saving account | Deposit action |
| P | Pending to approve | Customer open new LFA account | Approve action  |
| N | Normal | Normal status | Many actions: e.g. deposit, withdraw, credit interest, transfer, etc. |
| A | Actived | This status used for fixed deposit account with information deposit money only 1 time. | accrued interest action |
| M | Maturity | When account comes to maturity | Withdraw, rollover action |
| D | Dormant | When account has no transaction conducted exceed dormant period | First action operates require officer overriding |
| B | Block | Teller block account for some seasons | Only un-block action. Interest is calculated normally |
| C | Closed | Customer close account | No action. For some bank, might be re-activate action can operate with the closed account |

- The system must display a warning to the authorized about changed deposit account information. Modified data of deposit account must be approved or reject:
    - Approved: Modified data will apply in Deposit account.
    - Reject: Modified data will not apply in Deposit account.
- How to calculate deposit interest:
    - Balance = the amount customer deposited
    - Interest rate = the percent of balance
    - Interest daily = Balance * Interest rate a day
    - Interest amount = ∑ interest daily

- For fixed deposit account

| Balance | Interest rate (year) | Day of year | Term (month) | Interest daily | Interest amount |
| ------- | -------------------- | ----------- | ------------ | -------------- | --------------- |
|  |  |  | Assume, | = Balance * Interest rate/Day of year | =Interest daily * Total days in term |
|  |  |  | 3 months | = 1,000,000 * 2.5% / 365 |  |
|  |  |  | = 92 days |  |  |
| $1,000,000.00 | 2.50% | 365 | 3 | $68.49  | $6,301.37  |

- For saving account

| Balance | Interest rate (year) | Day of year | Total days | Interest daily | Interest amount |
| ------- | -------------------- | ----------- | ---------- | -------------- | --------------- |
|  |  |  |  | = Balance * Interest rate / Day of year | =Interest daily * Total days |
|  |  |  |  | =1,000,000*0.25%/365 |  |
| $1,000,000.00 | 0.25% | 365 | 20 | $6.85  | $1,369.86  |