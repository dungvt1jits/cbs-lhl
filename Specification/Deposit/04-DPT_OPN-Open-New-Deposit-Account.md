# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_OPN`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | No | String | 25 | | | PDACC |
| 2 | Customer type | customer_type | `Yes` | String | 1 | | | CCTMT |
| 3 | Customer code | customer_code | `Yes` | String | 15 | | | CCTMCD |
| 4 | Catalogue code | catalog_code | `Yes` | String | 25 | | | CNCAT |
| 5 | Catalogue name | catalog_name | `Yes` | String | 100 | | | CCATNM |
| 6 | Deposit type | deposit_type | `Yes` | String | 25 | | Current (C), Saving (S), Fixed Deposit (T) | DPTTYPE |
| 7 | Master FD account | master_fd_account | No | String | 25 | | `deposit_type=T` => `master_fd_account` is optional, `deposit_type=T` => `master_fd_account` is empty | PDMACC |
| 8 | Deposit purpose | deposit_purpose | `Yes` | String | 1 | | Time deposit (T), Saving (S), Payment (P) | DPTPRP |
| 9 | Customer segmentation | account_type | `Yes` | String | 2 | | Individual (1), Joint resident (2), Partnership - Foreigner (14), Master FD account (23), Link FD account (24), ... When `deposit_purpose=T` then `account_type=23` | CACTYPE |
| 10 | Seq number | seq_number | No | `Number` | 6 | 0 | cho phép `null` | CNSEQ |
| 11 | Account holder name | account_name | `Yes` | String | 250 | | | CACNM |
| 12 | Business purpose code | business_purpose_code | ~~`Yes`~~ No | String | 15 | | | ISICCD |
| 13 | Rollover option | rollover | `Yes` | String | 1 | | No rollover (N), Principal plus interest rollover (A), Principal rollover only (P). `deposit_type<>T` => `rollover=N` | RLLOVR |
| 14 | Auto transfer option | auto_transfer_option | No | String | 1 | | Auto collection(transfer) for both P and I (A), Auto collection(transfer) for Interest (I), Select one (N). `deposit_type<>T` => `auto_transfer_option=N` | LKCLASS |
| 15 | To account number | to_account_number | No | String | 25 | | `rollover=N and auto_transfer_option=A` OR `rollover=P and auto_transfer_option=I` => to_account_number is require. `rollover=A and auto_transfer_option=N` OR `rollover=N and auto_transfer_option=N` => to_account_number is empty. | PDOACC |
| 16 | Description | description | No | String | 250 | | | DESCS |
| 17 | Deposit sub type | deposit_sub_type | `Yes` | String | 2 | | | `gửi thêm` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | 15 | |
| 3 | Customer type | customer_type | String | | |
| 4 | Customer code | customer_code | String | 8 | |
| 5 | Catalogue code | catalog_code | String | 8 | |
| 6 | Catalogue name | catalog_name | String | | |
| 7 | Deposit type | deposit_type | String | | |
| 8 | Master FD account | master_fd_account | String | | |
| 9 | Deposit purpose | deposit_purpose | String | | |
| 10 | Customer segmentation | account_type | String | | |
| 11 | Seq number | seq_number | `Number` | | |
| 12 | Account holder name | account_name | String | | |
| 13 | Business purpose code | business_purpose_code | String | | |
| 14 | Rollover option | rollover | String | | |
| 15 | Auto transfer option | auto_transfer_option | String | | |
| 16 | To account number | to_account_number | String | | |
| 17 | Description | description | String | | |
| 18 | Transaction status | status | String | | |
| 19 | Transaction date |  | `Date time` |  |  |
| 20 | User id |  | `Number` |  |  |
| 21 | Deposit sub type | deposit_sub_type | String | | `trả thêm` |

### Deposit account information
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Deposit account id | id | `Number` |  |  |
| 2 | Account number def | account_number_def | String | 36 | tự gen sau khi giao dịch open thành công |
| 3 | Account number | account_number | String |  | tự gen sau khi giao dịch open thành công |
| 4 | Passbook or receipt number | passbook_or_receipt_number | String |  | cập nhật sau khi làm giao dịch issue stock |
| 5 | Account name | account_name | String |  | lấy từ giao dịch |
| 6 | Currency code | currency_code | String |  | lấy từ catalog |
| 7 | Secure currency | secure_currency | String |  | không dùng, xem xét xóa đi |
| 8 | Account holder type | customer_type | String |  | lấy từ giao dịch |
| 9 | Account holder | customer_id | String |  | lấy từ giao dịch |
| 10 | Branch id | branch_id | String |  | lấy từ branch làm giao dịch |
| 11 | Account status | deposit_status | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 12 | Passbook or receipt status | passbook_or_receipt_status | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 13 | Opening resident status | acrsd | String |  | lấy từ customer |
| 14 | Customer resident status |  | String |  | lấy từ customer |
| 15 | Catalogue code | catalog_id | `Number` |  | lấy từ giao dịch |
| 16 | Deposit type | deposit_type | String |  | lấy từ catalog |
| 17 | Account type | deposit_group | `Number` |  | lấy từ giao dịch |
| 18 | Deposit purpose | deposit_purpose | String |  | lấy từ catalog |
| 19 | Deposit classification | deposit_classification | String |  | lấy từ catalog |
| 20 | Passbook or statement (P/S) | passbook_or_statement | String |  | lấy từ catalog |
| 21 | Interest accrual rate | intacrrt | `Number` |  | lấy từ catalog |
| 22 | Tenor 1 | tenor | `Number` |  | lấy từ catalog |
| 23 | Tenor unit 1 | tenor_unit | String |  | lấy từ catalog |
| 24 | Tenor 2 | tenor_2 | `Number` |  | lấy từ catalog |
| 25 | Tenor unit 2 | tenor_unit_2 | String |  | lấy từ catalog |
| 26 | Begin of tenor | begin_of_tenor | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 27 | End of tenor | end_of_tenor | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 28 | Open date | open_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 29 | Close date | close_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 30 | Last transaction date | last_transaction_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 31 | Last date system transfer interest to due | last_transfer_interest_to_due | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 32 | Dormant date | dormant_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 33 | Last change dormant to normal date | last_change_dormant_to_normal_date | `Date time` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 34 | Created by | user_created | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 35 | Approved by | user_approved | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 36 | Account manager staff id | account_manager_staff_id | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 37 | Business purpose code | business_purpose_code | String |  | lấy từ giao dịch |
| 38 | Current balance | current_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 39 | Available balance  |  | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công, current_balance - minimum_deposit_amount - earmark_book_amount + CRLIMIT |
| 40 | Minimum deposit amount | minimum_deposit_amount | `Number` |  | lấy từ catalog |
| 41 | Minimum amount to dormant | minimum_dormant_amount | `Number` |  | lấy từ catalog |
| 42 | Earmark (book) amount | earmark_book_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 43 | Initial deposit amount | initial_deposit_amount | `Number` |  | lấy từ catalog |
| 44 | Periodic deposit amount | periodic_deposit_amount | `Number` |  | lấy từ catalog |
| 45 | Month average balance | month_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 46 | Quarter average balance | quarter_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 47 | Semi-annual average balance | semi_annual_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 48 | Year average balance | year_average_balance | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 49 | Amount for interest calculation | icbal | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 50 | Float amount |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 51 | With holding tax accrual |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 52 | Interest accrual | interest_accrual | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 53 | Interest due | interest_due | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 54 | Interest not paid | interest_not_paid | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 55 | Interest paid | interest_paid | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 56 | Deposit amount | deposit_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 57 | Withdraw amount | withdraw_amount | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 58 | Week debit | week_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 59 | Week credit | week_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 60 | Month debit | month_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 61 | Month credit | month_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 62 | Quarter debit | quarter_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 63 | Quarter credit | quarter_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 64 | Semi-annual debit | semi_annual_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 65 | Semi-annual credit | semi_annual_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 66 | Year debit | year_debit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 67 | Year credit | year_credit | `Number` |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 68 | Accumulate dormant fee |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 69 | Last dormant fee collection date |  | `Date time` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 70 | Accumulate maintenance fee |  | `Number` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 71 | Last maintenance fee collection date |  | `Date time` |  | kiểm tra code để lấy đúng thông tin từ đâu |
| 72 | Deposit tenor | deposit_tenor | `Number` |  | lấy từ catalog |
| 73 | Deposit tenor unit | deposit_tenor_unit | String |  | lấy từ catalog |
| 74 | Interest tenor | interest_tenor | `Number` |  | lấy từ catalog |
| 75 | Interest tenor unit | interest_tenor_unit | String |  | lấy từ catalog |
| 76 | Minimum tenor | minimum_tenor | `Number` |  | lấy từ catalog |
| 77 | Minimum tenor unit | minimum_tenor_unit | String |  | lấy từ catalog |
| 78 | Multiple deposit allow | multiple_deposit_allow | String |  | lấy từ catalog |
| 79 | Multiple withdrawal allow | multiple_withdrawal_allow | String |  | lấy từ catalog |
| 80 | Early withdrawal (Y/N) | early_withdrawal | String |  | lấy từ catalog |
| 81 | Minimum tenor allow early withdrawal | minimum_tenor_allow_early_withdrawal | `Number` |  | lấy từ catalog |
| 82 | Minimum tenor allow early withdrawal unit | minimum_tenor_allow_early_withdrawal_unit | String |  | lấy từ catalog |
| 83 | Credit interest (Y/N) | credit_interest | String |  | lấy từ catalog |
| 84 | Credit interest tenor | credit_interest_tenor | `Number` |  | lấy từ catalog |
| 85 | Credit interest tenor unit | credit_interest_tenor_unit | String |  | lấy từ catalog |
| 86 | The day of tenor for crediting interest | crediting_interest | `Number` |  | lấy từ catalog |
| 87 | Dormant period | dormant_period | `Number` |  | lấy từ catalog |
| 88 | Dormant period unit | dormant_period_unit | String |  | lấy từ catalog |
| 89 | Rollover option | rollover | String |  | lấy từ giao dịch |
| 90 | Rollover to catalogue | rollover_to_catalog | `Number` |  | lấy từ catalog |
| 91 | Interest due on holiday | interest_due_on_holiday | `Number` |  | lấy từ catalog |
| 92 | Principal due on holiday | principal_due_on_holiday | `Number` |  | lấy từ catalog |
| 93 | Statement tenor | statement_tenor | `Number` |  | lấy từ catalog |
| 94 | Statement tenor unit | statement_tenor_unit | String |  | lấy từ catalog |
| 95 | Statement format | statement_format | String |  | lấy từ catalog |
| 96 | Interest paid type | intpdcd | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 97 | Interest paid account | intac | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 98 | Remark | remark | String |  | lấy giá trị đúng theo nghiệp vụ sau khi giao dịch open thành công |
| 99 | Reference id | reference_id | String |  | use for conversion old account id,… |
|  | Tariff code | tariff_code | `Number` |  | lấy từ catalog |
|  | Master FD account | macno | String |  | lấy từ giao dịch |
|  | Deposit sub type | deposit_sub_type | String |  | lấy từ giao dịch |
|  | Interest prepaid | intpre | `Number` |  |  |
|  | Interest payable/receiveble | intpbl | `Number` |  |  |
|  | Amount for interest calculation | icbal | `Number` |  |  |
|  | Interest overdue, not paid | intovd | `Number` |  |  |
|  | Interest suspense | intspamt | `Number` |  |  |
|  | Total income amount | inamt | `Number` |  | use for fund investment account |
|  | Total expense amount | examt | `Number` |  | use for fund investment account |
|  | Reverse balance | rvbalance | `Number` |  |  (for OD,…) |
|  | Reverse month average balance | rvmavgamt | `Number` |  |  |
|  | Reverse quarter average balance | rvqavgamt | `Number` |  |  |
|  | Reverse semi-annual average balance | rvhavgamt | `Number` |  |  |
|  | Reverse year average balance | rvyavgamt | `Number` |  |  |
|  | Reverse interest accrual amount | rvintamt | `Number` |  | rounding 5 digit |
|  | Reverse interest paid | rintpaid | `Number` |  |  |
|  | Reverse interest prepaid | rintpre | `Number` |  |  |
|  | Reverse interest payable/receiveble | rvintpbl | `Number` |  |  |
|  | Reverse interest due | rvintdue | `Number` |  |  |
|  | Reverse interest overdue, not paid | rvintovd | `Number` |  |  |
|  | Reverse interest suspense | rvintspamt | `Number` |  |  |
|  | Reverse interest not paid | rvintnypd | `Number` |  | previous tenor, not rollover |
|  | Preview status | psts | String |  |  |
|  | Ties to tariff? | trftied | String |  | không dùng, mặc định: Y, xem xét xóa đi |
|  | User define field | udfield_1 | String |  | reserve, json structure |
|  | Periodic deposit tenor | periodic | `Number` |  | không dùng, xem xét xóa đi |
|  | Periodic deposit tenor unit | periodicun | String |  | không dùng, xem xét xóa đi |
|  | Block by | blkby | String |  |  |

## 1.2 Transaction flow
**Conditions:**

-	Customer code is existing in the system with status is Normal.
-	"Catalogue code" is defined with status is Normal.

**Flow of events:**
-	Allow open account: current, saving, fixed deposit.
-	A customer can open many accounts with multi-currencies. No limit quantity.
-	Account code will auto create.
-	Allow creation account code by number sequence define.
-	Content of accounting will copy from definition "Catalogue code".
-	Allow deleting account if it has just opened.
-	Allow to modify information contract after opening contract.
-	Allow adjustment interest rate on account.
-	When complete transaction: deposit account status is "New".
-   With deposit type is Fixed deposit:
    -  Master FD is null: automatically generate new MFA
    -  Master FD <> null: automatically get MFA was input for LFA
    -  Master FD is invalid: automatically get MFA was input for LFA

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 1](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A11`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | |
| 2 | Transaction status |  | String | | |
| 3 | User approve |  | String | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | 15 | |
| 3 | Customer type | customer_type | String | | |
| 4 | Customer code | customer_code | String | 8 | |
| 5 | Catalogue code | catalog_code | String | 8 | |
| 6 | Catalogue name | catalog_name | String | | |
| 7 | Deposit type | deposit_type | String | | |
| 8 | Master FD account | master_fd_account | String | | |
| 9 | Deposit purpose | deposit_purpose | String | | |
| 10 | Account type | deposit_group | String | | |
| 11 | Seq number | seq_number | String | | |
| 12 | Account holder name | account_name | String | | |
| 13 | Business purpose code | business_purpose_code | String | | |
| 14 | Rollover option | rollover | String | | |
| 15 | Auto transfer option | auto_transfer_option | String | | |
| 16 | To account number | to_account_number | String | | |
| 17 | Description | description | String | | |
| 18 | Transaction status | status | String | | |
| 19 | Transaction date |  | `Date time` |  |  |
| 20 | User id |  | `Number` |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_CACNM | CTM_GET_INFO_FULLNAME | [CTM_GET_INFO_FULLNAME](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_CCATNM | GET_INFO_DPTCAT | [GET_INFO_DPTCAT](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_PDMACC | DPT_OPN_GET_INFO_PDMACC | [DPT_OPN_GET_INFO_PDMACC](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_INFO_RLLOVR | GET_INFO_DPTCAT | [GET_INFO_DPTCAT](Specification/Common/15 Deposit Rulefuncs) |
| 6 | GET_INFO_CACTYPE | Thực hiện trên JWEB | Khi DPTPRP = 'T' thì CACTYPE=23 |
| 7 | GET_INFO_WISICCD | GET_INFO_WISICCD | [GET_INFO_WISICCD](Specification/Common/17 Admin Rulefuncs) |
| 8 | GET_INFO_PDOACC |  | Không dùng |
| 9 | LKP_DATA_CCTMCD | CTM_LOOKUP_CTM_BY_CTMTYPE | [CTM_LOOKUP_CTM_BY_CTMTYPE](Specification/Common/12 Customer Rulefuncs) |
| 10 | LKP_DATA_CNCAT | DPT_OPN_LKP_DATA_CNCAT | [DPT_OPN_LKP_DATA_CNCAT](Specification/Common/15 Deposit Rulefuncs) |
| 11 | LKP_DATA_WISICCD | ADM_LOOKUP_CODE_LIST | [ADM_LOOKUP_CODE_LIST](Specification/Common/01 Rulefunc) |
| 12 | LKP_DATA_PDMACC | DPT_OPN_LKP_DATA_PDMACC | [DPT_OPN_LKP_DATA_PDMACC](Specification/Common/15 Deposit Rulefuncs) |
| 13 | LKP_DATA_PDOACC | DPT_OPN_LKP_DATA_PDOACC | [DPT_OPN_LKP_DATA_PDOACC](Specification/Common/15 Deposit Rulefuncs) |