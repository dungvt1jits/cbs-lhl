# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CIP`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | Interest accrual | interest_accrual | No | `Number` | | 0 | trường ẩn | CINT |
| 3 | Interest prepaid | interest_prepaid | No | `Number` | | 0 | trường ẩn | CIPRE |
| 4 | Interest payable/receivable | interest_payable_receivable | No | `Number` | | 0 | | CIPBL |
| 5 | Interest due | interest_due | No | `Number` | | 0 | | CIDUE |
| 6 | Interest overdue | interest_overdue | No | `Number` | | 0 | | CIOVD |
| 7 | Interest suspense | interest_suspense | No | `Number` | | 0 | trường ẩn | CISPS |
| 8 | Interest not paid | interest_not_paid | No | `Number` | | 0 | | CINOTPAID |
| 9 | Interest amount | interest_amount | No | `Number` | | 0 | | CTCVT |
| 10 | Gross paid interest amount | gross_paid_interest_amount | No | `Number` | | <> 0 | | PCSAMT    |
| 11 | Withholding tax accrual actural | withholding_tax_accrual_actural | No | `Number` | | 0 | trường ẩn | WHTAMT |
| 12 | Withholding tax amount calc | withholding_tax_amount_calc | No | `Number` | | 0 | | CWHTA |
| 13 | Withholding tax acrual | withholding_tax_accrual | No | `Number` | | 0 | | CFAMT |
| 14 | Receiver name | receiver_name | No | String | 250 | | | CACNM |
| 15 | Receiver id | receiver_id | No | String | 15 | | | CCTMCD |
| 16 | Receiver address | receiver_address | No | String | 250 | | | CCTMA |
| 17 | Receiver description | receiver_description | No | JSON Object | | | | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 18 | Deposit currency | deposit_currency | No | String | 3 | | trường ẩn | CCCR |
| 19 | Description | description | No | String | 250 | | | DESCS |
| 20 | Branch name | branch_name | No | String | | | | `gửi thêm` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | | |
| 3 | Account number | account_number | String | | | |
| 4 | Interest accrual | interest_accrual | `Number` | | trường ẩn |
| 5 | Interest prepaid | interest_prepaid | `Number` | | trường ẩn |
| 6 | Interest payable/receivable | interest_payable_receivable | `Number` | | số có hai số thập phân |
| 7 | Interest due | interest_due | `Number` | | số có hai số thập phân |
| 8 | Interest overdue | interest_overdue | `Number` | | số có hai số thập phân |
| 9 | Interest suspense | interest_suspense | `Number` | | trường ẩn |
| 10 | Interest not paid | interest_not_paid | `Number` | | số có hai số thập phân |
| 11 | Interest amount | interest_amount | `Number` | | số có hai số thập phân |
| 12 | Gross paid interest amount | gross_paid_interest_amount | `Number` | | số có hai số thập phân |
| 13 | Withholding tax acrual actural | withholding_tax_accrual_actural | `Number` | | trường ẩn |
| 14 | Withholding tax amount calc | withholding_tax_amount_calc | `Number` | | số có hai số thập phân |
| 15 | Withholding tax acrual | withholding_tax_accrual | `Number` | | số có hai số thập phân |
| 16 | Receiver name | receiver_name | String | | |
| 17 | Receiver id | receiver_id | String | | |
| 18 | Receiver address | receiver_address | String | | |
| 19 | Receiver description | receiver_description | JSON Object | | |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 20 | Deposit currency | deposit_currency | String | | trường ẩn |
| 21 | Description | description | String | | |
| 28 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | |
| 29 | Transaction status | status | String | | |
| 30 | Transaction date | transaction_date | `Date time` |  |  |
| 31 | User id | user_id | `Number` |  |  |
| 32 | Branch name | branch_name | No | String | | | `gửi thêm` | |

## 1.2 Transaction flow
**Conditions:**
-	Account number: exists with status {"Normal", ~~"Dormant"~~, "Maturity"}.
    + 	Account has interest amount.
    +  | Gross paid interest amount | <= Interest payable/receivable + Interest due + Interest not paid + Interest overdue
- Teller do this transaction:
  - User must have enough cash to withdraw.
  - User has right to do transaction related to cash.
  - User must have enough cash to give customer.
-   ~~If account has status is "Dormant", system will show popup approve~~ (KTB thay đổi nghiệp vụ không cho phép thực hiện giao dịch với tài khoản Dormant)

**Flow of events:**
-	Don't allows to withdrawal money with other currency.
-	If account is type "Fixed deposit", just allows repayment interest due. If withdrawal before maturity:
    +	If a customer withdraws before 3 months, no interest received or calculate interest based on defined rate, reverse accrued interest (not paid), reverse withholding tax.
    +	If term is over 3 months and 3 months < deposit period < term: Calculate based on saving interest rate and deduct withholding tax.
-	Record repayment interest:  Decrease interest amount.
-	~~If before transaction completed, account has status "Dormant". System will auto change status "Normal".~~ (KTB thay đổi nghiệp vụ không cho phép thực hiện giao dịch với tài khoản Dormant)

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST | Amount repay | Currency of account |
| 1 | C | GL | Amount repay | Currency of account |

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 6](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A204`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Teller has not enough cash | Teller không đủ tiền mặt |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_INT | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_DEF_INT_PAID_TOCUS | DPT_GET_REDEMPTION_INTEREST | [DPT_GET_REDEMPTION_INTEREST](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_INFO_WHTAMT | DPT_GET_INFO_WHTAMT | [DPT_GET_INFO_WHTAMT](Specification/Common/15 Deposit Rulefuncs) |
| 6 | CCL_NUMB_CWHTA | DPT_GET_INTEREST_WITHOUT_WHTAX | [DPT_GET_INTEREST_WITHOUT_WHTAX](Specification/Common/15 Deposit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>