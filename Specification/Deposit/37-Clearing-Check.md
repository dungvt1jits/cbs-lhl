# 1. Refresh
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_REFRESH_CLEARING`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | From date |  | `Yes` | Date |  |  |  |
| 2 | To date |  | `Yes` | Date |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Search results | dataset | Array Object |  |  |
| 1 | Date | date | Date |  |  |  |
| 2 | Tran reference | transaction_reference_id | String |  |  |  |
| 3 | Cheque no | cheque_no | String |  |  |  |
| 4 | Issue bank | issue_bank | String |  |  |  |
| 5 | Amount | amount | `Number` |  |  | số có hai số thập phân |
| 6 | GL account | clearing_account | String |  |  |  |
| 7 | Drawer name | drawer_name | String |  |  |  |
| 8 | Customer code | customer_code | String | 8 |  |  |
| 9 | Account number | account_number | String | 15 |  |  |
| 10 | Cheque currency | currency_code | String | 3 |  |  |
| 11 | Ben amount | beneficiary_amount | `Number` |  |  | số có hai số thập phân |
| 12 | Ben currency | beneficiary_currency | String | 3 |  |  |
| 13 | Base amount | `vatamt` thành `vat_amount` | `Number` |  |  | số có hai số thập phân |
| 14 | | Clearing id | id | `Number` |  |  |  |

## 1.2 Transaction flow
- Clearing status=N and fltamt <> 0
- Account number def is existed in deposit account

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Process
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_OCC`

**Body:**
| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Lấy giá trị từ response của DPT_REFRESH_CLEARING |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Cheque no | cheque_no | `Yes` | String | 20 | | | cheque_no | 
| 2 | Cheque currency | cheque_currency | No | String | 3 | | | currency_code |
| 3 | Issue bank | clearing_bank | `Yes` | String | 25 | | | issue_bank |
| 4 | Amount | cheque_amount | `Yes` | `Number` | | 0 | | amount |
| 5 | GL account | clearing_bank_nostro_acc | `Yes` | String | 25 | | | clearing_account |
| 6 | Account number | beneficiary_account | `Yes` | String | 25 | | | account_number |
| 7 | Beneficiary currency | beneficiary_currency | No | String | 3 | | | beneficiary_currency |
| 8 | Ben amount | beneficiary_amount | No | `Number` | | 0 | | beneficiary_amount |
| 9 | Drawer name | drawer_name | No | String | 250 | | | drawer_name |
| 10 | Customer code | drawer_paper_number | No | String | 25 | | | customer_code |
| 11 | Description | description | No | String | 250 | Auto Clearing cheque | trường ẩn | |
| 12 | Holding days | holding_days | No | `Number` | | 0 | trường ẩn | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | | |
| 2 | Cheque no | cheque_no | String | 20 | |
| 3 | Cheque currency | cheque_currency | String | 3 | |
| 4 | Issue bank | clearing_bank | String | 25 | |
| 5 | Amount | cheque_amount | `Number` |  | |
| 6 | GL account | clearing_bank_nostro_acc | String | 25 | |
| 7 | Account number | beneficiary_account | String | 25 | |
| 8 | Beneficiary currency | beneficiary_currency | String | 3 | |
| 9 | Ben amount | beneficiary_amount | `Number` |  | |
| 10 | Drawer name | drawer_name | String | 250 | |
| 11 | Customer code | drawer_paper_number | String | 25 | |
| 12 | Description | description | String | 250 | |
| 13 | Holding days | holding_days | `Number` | | trường ẩn |
| 14 | Transaction status | status | String |  |  |
| 15 | Transaction date | transaction_date | `Date time` |  |  |
| 16 | User id | user_id | `Number` |  | 

## 2.2 Transaction flow
[Tham khảo giao dịch DPT_OCC](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/Specification/Deposit/51%20DPT_OCC%20Outward%20Cheque%20Cleared)

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |

# 3. Reject
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `DPT_OCR`

**Body:**
| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Lấy giá trị từ response của DPT_REFRESH_CLEARING |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Cheque no | cheque_no | `Yes` | String | 20 | | | cheque_no |
| 2 | Cheque currency | cheque_currency | No | String | 3 | | | currency_code |
| 3 | Issue bank | clearing_bank | `Yes` | String | 25 | | | issue_bank |
| 4 | Amount | cheque_amount | `Yes` | `Number` | | 0 | | amount |
| 5 | GL account | clearing_bank_nostro | `Yes` | String | 25 | | | clearing_account |
| 6 | Account number | beneficiary_account | `Yes` | String | 25 | | | account_number |
| 7 | Beneficiary currency | beneficiary_currency | No | String | 3 | | | beneficiary_currency |
| 8 | Ben amount | beneficiary_amount | No | `Number` | | 0 | | beneficiary_amount |
| 9 | Drawer name | drawer_name | No | String | 250 | | | drawer_name |
| 10 | Customer code | drawer_paper_number | No | String | 25 | | | customer_code |
| 11 | Base amount | base_amount | No | `Number` | | 0 | | vat_amount |
| 12 | Description | description | No | String | 250 | Auto Clearing cheque | trường ẩn | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Cheque no | cheque_no | String | 20 | |
| 3 | Cheque currency | cheque_currency | String | 3 | |
| 4 | Issue bank | clearing_bank | String | 25 | |
| 5 | Amount | cheque_amount | `Number` |  | |
| 6 | GL account | clearing_bank_nostro_acc | String | 25 | |
| 7 | Account number | beneficiary_account | String | 25 | |
| 8 | Beneficiary currency | beneficiary_currency | String | 3 | |
| 9 | Ben amount | beneficiary_amount | `Number` |  | |
| 10 | Drawer name | drawer_name | String | 250 | |
| 11 | Customer code | drawer_paper_number | String | 25 | |
| 12 | Base amount | base_amount | `Number` | 0 | |
| 13 | Description | description | String | 250 | |
| 14 | Transaction status | status | String | | |
| 15 | Transaction date | transaction_date | `Date time` |  |  |
| 16 | User id | user_id | String |  |  |

## 3.2 Transaction flow
[Tham khảo giao dịch DPT_OCR](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/Specification/Deposit/52%20DPT_OCR%20Outward%20Cheque%20Return)

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |