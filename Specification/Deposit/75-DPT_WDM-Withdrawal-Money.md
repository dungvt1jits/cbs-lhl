# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_WDM`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | PDACC |
| 2 | Account name | account_name | `Yes` | String | 250 |  |  | CACNM |
| 3 | Account status | account_status | `Yes` | String | 1 |  |  | DPTSTS |
| 4 | Withdraw amount | withdraw_amount | `Yes` | `Number` |  | > 0 |  | PCSAMT |
| 5 | Withdrawal method | withdrawal_method | `Yes` | String | 3 |  |  | PAYBY |
| 6 | Credit account | credit_account | No | String | 25 |  |  | SACNO |
| 7 | Credit account name | credit_account_name | No | String | 250 |  |  | CCTMA |
| 8 | Credit account currency | credit_account_currency | No | String | 3 |  |  | CCCR1 |
| 9 | Total fee amount | total_fee_amount | No | `Number` |  | 0 |  | TXAMT |
| 10 | Description | description | No | String | 250 |  |  | DESCS |
| 11 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 12 | Currency of deposit account | currency_of_deposit_account | No | String | 3 |  | trường ẩn | CCCR |
| 13 | Branch id | branch_id | No | `Number` |  | branch id user login | trường ẩn | CBRCD |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String | | |
| 3 | Account name | account_name | String | | |
| 4 | Account status | account_status | String | | |
| 5 | Withdraw amount | withdraw_amount | `Number` | | |
| 6 | Withdrawal method | withdrawal_method | String | | |
| 7 | Credit account | credit_account | String | | |
| 8 | Credit account name | credit_account_name | String | | |
| 9 | Credit account currency | credit_account_currency | String | | |
| 10 | Total fee amount | total_fee_amount | `Number` | | |
| 11 | Description | description | String | | |
| 12 | Fee data | fee_data | JSON Object | | | |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 13 | Currency of deposit account | currency_of_deposit_account | String | | |
| 14 | Branch id | branch_id | String | | |
| 15 | Transaction status | status | String |  |  |
| 16 | Transaction date | transaction_date | `Date time` |  |  |
| 17 | User id | user_id | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- Withdraw by Cash: Tham khảo giao dịch [DPT_CWR: 1120: Cash withdrawal
](Specification/Deposit/16-DPT_CWR-Cash-Withdrawal)
- Withdraw by GL: Tham khảo giao dịch [DPT_MWR: 1122: Miscellaneous withdrawal
](Specification/Deposit/17-DPT_MWR-Miscellaneous-Withdrawal)

**Withdraw by Deposit:**
-	"Debit account": exists with status is "Normal". Type is current or saving
	+ Deposit type: Saving 
	  + PassbookOrReceiptStatus = N/ U
-	"Credit account": exists with status is "Normal"
-	Accounts must be same currency.
-	"Debit account" has enough amount to transfer (not include amount is keeping).
-   "Debit account" <> "Credit account"

**Flow of events:**
- Withdraw by Cash: Tham khảo giao dịch [DPT_CWR: 1120: Cash withdrawal
](Specification/Deposit/16-DPT_CWR-Cash-Withdrawal)
- Withdraw by GL: Tham khảo giao dịch [DPT_MWR: 1122: Miscellaneous withdrawal
](Specification/Deposit/17-DPT_MWR-Miscellaneous-Withdrawal)
- Withdraw by Deposit: Tham khảo giao dịch [DPT_TRF: 1130: Transfer from deposit account to deposit account](Specification/Deposit/14-DPT_TRF-Transfer-From-Deposit-Account-To-Deposit-Account)

-   If transaction has collect fee, deposit account must pay more for fee
<br>Ex: Withdraw amount = 100, Fee = 10 => Deposit account will withdraw: 110

Deposit type of debit account <> FD

**Database:**


**Voucher:**
- [Voucher](Specification/Common/03 Voucher)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Account number |  | String |  |  |  |
| 3 | From date |  | `Date time` |  |  |  |
| 4 | To date |  | `Date time` |  |  |  |
| 5 | Transaction date |  | `Date time` |  |  |  |
| 6 | User id |  | String |  |  |  |
| 7 | Transaction status |  | String |  |  |  |
| 8 | Transaction list |  | JSON Object |  |  |  |
|  | Transaction code |  | String |  |  |  |
|  | Transaction number |  | `Date time` |  |  |  |
|  | Transaction date |  | String |  |  |  |
|  | Created by |  | String |  |  |  |
|  | DORC |  | `Number` |  |  | số có hai số thập phân |
|  | Amount |  | String |  |  |  |
|  | Description |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_BCY_CACNM | DPT_GET_INFO_DPTACC_CASA | [DPT_GET_INFO_DPTACC_CASA](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_PAYBY_CSH | `SELECT null,null,null FROM DUAL` | `SACNO,CCTMA,CCCR1` Làm trên JWEB |
| 4 | GET_INFO_DESCS | `select '1120: Withdrawal money by ' CASE '@PAYBY' WHEN 'CSH' THEN 'cash' WHEN 'DPT' then 'deposit' WHEN 'ACT' THEN 'miscellaneous' ELSE '' end  from dual` | Làm trên JWEB |
| 5 | GET_INFO_DPT_CCCR | DPT_GET_INFO_SACNO | [DPT_GET_INFO_SACNO](Specification/Common/15 Deposit Rulefuncs) |
| 6 | GET_INFO_ACT_SACNO | DPT_GET_INFO_SACNO | [DPT_GET_INFO_SACNO](Specification/Common/15 Deposit Rulefuncs) |
| 7 | TRAN_FEE_DPT_TRF/GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 8 | TRAN_FEE/GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 9 | TRAN_FEE_DPT_TRF/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) |
| 10 | LKP_DATA_SACNO | DPT_LKP_DATA_SACNO | [DPT_LKP_DATA_SACNO](Specification/Common/15 Deposit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>