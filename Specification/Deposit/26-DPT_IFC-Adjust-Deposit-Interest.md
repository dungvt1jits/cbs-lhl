# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_IFC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | IFC code | ifc_code | `Yes` | `Number` | | | | CIFCCD |
| 3 | Customer code | customer_code | `Yes` | String | 15 | | | CCTMCD |
| 4 | Account name | account_name | `Yes` | String | 250 | | | ACNM |
| 5 | Accrual interest amount | accrual_interest_amount | `Yes` | `Number` | | 0 | số có hai số thập phân | IACR |
| 6 | Interest due amount | interest_due_amount | `Yes` | `Number` | | 0 | số có hai số thập phân | INTDUE |
| 7 | Interest repayable amount | interest_repayable_amount | `Yes` | `Number` | | 0 | số có hai số thập phân | IPBLE |
| 8 | Current IFC amount | current_ifc_amount | `Yes` | `Number` | | 0 | số có hai số thập phân | IFCAMT |
| 9 | Adjustment amount | adjustment_amount | `Yes` | `Number` | | <> 0 | số có hai số thập phân, cho phép nhập < 0.00 và > 0.00 | TXAMT |
| 10 | IFC Type | ifc_type | `Yes` | String | 1 | | | CTYPE |
| 11 | New ifc amount | new_ifc_amount | `Yes` | `Number` | | 0 | số có hai số thập phân | CAMT |
| 12 | Adjusted accrual interest | adjusted_accrual_interest | `Yes` | `Number` | | 0 | số có hai số thập phân | CINTAMT |
| 13 | Adjusted due interest | adjusted_due_interest | `Yes` | `Number` | | 0 | số có hai số thập phân | CINTDUE |
| 14 | Adjusted payable interest | adjusted_payable_interest | `Yes` | `Number` | | 0 | số có hai số thập phân | CINTPBL |
| 15 | Description | description | `Yes` | String | 250 | | | DESCS |
| 16 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | | |
| 3 | IFC code | ifc_code | `Number` | | |
| 4 | Customer code | customer_code | String | | |
| 5 | Account name | account_name | String | | |
| 6 | Accrual interest amount | accrual_interest_amount | `Number` | | |
| 7 | Interest due amount | interest_due_amount | `Number` | | |
| 8 | Interest repayable amount | interest_repayable_amount | `Number` | | |
| 9 | Current IFC amount | current_ifc_amount | `Number` | | |
| 10 | Adjustment amount | adjustment_amount | `Number` | | |
| 11 | IFC Type | ifc_type | String | | |
| 12 | New ifc amount | new_ifc_amount | `Number` | | |
| 13 | Adjusted accrual interest | adjusted_accrual_interest | `Number` | | |
| 14 | Adjusted due interest | adjusted_due_interest | `Number` | | |
| 15 | Adjusted payable interest | adjusted_payable_interest | `Number` | | |
| 16 | Description | description | String | | |
| 17 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | |
| 18 | Transaction status | status | String |  |  |
| 19 | Transaction date | transaction_date | `Date time` |  |  |
| 20 | User id | user_id | `Number` |  |  |
| 21 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**

-	Deposit account: exists with status is "Normal".
-   Adjustment amount <> 0
-   In case user ajdust decrease, |Adjustment amount| <= Interest accrual amount + Interest due

**Flow of events:**
-	Allow adjustment interest amount or withholding tax amount.
-	Adjust interest amount, system don't auto calculate {Withholding tax}. 
-	Adjust {Withholding tax} amount. Daily, system will calculate interest a day and {Withholding tax} based on interest daily.
    +	{Withholding tax} = {Withholding tax} before + {Withholding tax} daily.
It means, system will don't calculate again {Withholding tax}. 
Actually, it continues calculation.
-	Don't allow adjust interest amount and {withholding tax} same time. Must adjust one by one.
-	Allow to adjust increase or decrease

**Posting:**
-	Adjust interest increase (Adjustment amount > 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | PAID_INTEREST | Adjustment amount | Currency of account |
| 1 | C | INTEREST | Adjustment amount | Currency of account |

-	Adjust interest decrease (Adjustment amount < 0)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST | Adjustment amount | Currency of account |
| 1 | C | REVERT_INTEREST | Adjustment amount | Currency of account |

-	Adjust withholding tax (increasing/ decreasing)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | PAID_WHT | Adjustment amount | Currency of account |
| 1 | C | WITHHOLDING_TAX | Adjustment amount | Currency of account |

> Cần kiểm tra lại với môi trường hiện thực

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_AMOUNT | DPT_IFC_GET_INFO_IFCAMT | [DPT_IFC_GET_INFO_IFCAMT](Specification/Common/21 IFC Rulefuncs) |
| 3 | GET_INFO_BCY_CACNM | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_ADJUSTMENT | DPT_IFC_GET_INFO_ADJUSTMENT | [DPT_IFC_GET_INFO_ADJUSTMENT](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_IFC_TYPE | GET_IFC_TYPE | [GET_IFC_TYPE](Specification/Common/21 IFC Rulefuncs) |
| 6 | LKP_DATA_CIFCCD | IFC_LOOKUP_IFCTYPE_IT_BY_DPTACNO | [IFC_LOOKUP_IFCTYPE_IT_BY_DPTACNO](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>