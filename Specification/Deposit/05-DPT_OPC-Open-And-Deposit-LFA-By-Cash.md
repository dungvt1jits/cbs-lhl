# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_OPC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | No | String | 25 | | | PDACC |
| 2 | Master account number | master_fd_account | `Yes` | String | 25 | | | PDMACC |
| 3 | Customer type | customer_type | `Yes` | String | 1 | | | CCTMT |
| 4 | Customer code | customer_code | `Yes` | String | 15 | | | CCTMCD |
| 5 | Catalogue code | catalog_code | `Yes` | String | 25 | | | CNCAT |
| 6 | Catalogue name | catalog_name | `Yes` | String | 100 | | | CCATNM |
| 7 | Deposit type | deposit_type | `Yes` | String | 1 | | | DPTTYPE |
| 8 | Deposit purpose | deposit_purpose | `Yes` | String | 1 | | | DPTPRP |
| 9 | Account type | account_type | `Yes` | String | 2 | | | CACTYPE |
| 10 | Account sub type | account_sub_type | `Yes` | String | 1 | | | CTYPE |
| 11 | Amount deposit | amount_deposit | `Yes` | `Number` | | > 0 | số có hai số thập phân | PCSAMT |
| 12 | Seq number | seq_number | No | `Number` | 6 | 0 | cho phép `null` | CNSEQ |
| 13 | Description | description | No | String | 250 | | | DESCS |
| 14 | Account holder name | account_holder_name | `Yes` | String | 250 | | | CACNM |
### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references | reference_id | String | | | |
| 2 | Account number def | DEFACNO | String | 36 | | |
| 3 | Account number | account_number | String | | | |
| 4 | Master account number | master_fd_account | String | | | |
| 5 | Customer type | customer_type | String | | | |
| 6 | Customer code | customer_code | String | | | |
| 7 | Catalogue code | catalog_code | String | | | |
| 8 | Catalogue name | catalog_name | String | | | |
| 9 | Deposit type | deposit_type | String | | | |
| 10 | Deposit purpose | deposit_purpose | String | | | |
| 11 | Account type | account_type | String | | | |
| 12 | Account sub type | account_sub_type | String | | | |
| 13 | Amount deposit | amount_deposit | `Number` | | | |
| 14 | Seq number | seq_number | `Number` | | | |
| 15 | Description | description | String | | | |
| 16 | Account holder name | account_holder_name | String | | | |
| 17 | Account status | | String | | | |
| 18 | Open date | | `Date` | | | |
| 19 | Created by | | String | | | User login |
| 20 | Transaction status | status | String | | |
| 21 | Transaction date | transaction_date | `Date time` |  |  |
| 22 | User id | user_id | String |  |  |

## 1.2 Transaction flow
**Conditions:**
-	"Master Fixed Deposit account": exists with status "Actived", "Normal".
-	"Amount deposit": must be larger than zero.

**Flow of events:**
-	"Link Fixed Deposit account" status is "Pending to approve".
-	Balance link fixed deposit account will increase.

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 1](Specification/Common/06 Transaction Flow Deposit)<br>

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |