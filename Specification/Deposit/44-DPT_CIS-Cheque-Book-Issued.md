# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CIS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 2 | Stock number | stock_number | No | String | 15 | | | STK_NO |
| 3 | From serial | from_serial | `Yes` | String | 20 | | | CFRSER |
| 4 | To serial | to_serial | `Yes` | String | 20 | | | CTOSER |
| 5 | Number of leaves | number_of_leaves | `Yes` | `Number` | | >0 | | CAMT1 |
| 6 | Fee amount | fee_amount | `Yes` | `Number` |  | 0 |  | CFAMT |
| 7 | Description | description | No | String | 250 |  |  | DESCS |
| 8 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 9 | Stock type | stock_type | `Yes` | String | 1 | | trường ẩn | CSTKTP |
| 10 | Total Fee for Cash | total_fee_for_cash | No | `Number` | | 0 | trường ẩn | TFCSH |
| 11 | Total VAT for Cash | total_vat_for_cash | No | `Number` | | 0 | trường ẩn | TVATCSH |
| 12 | Fee VAT | fee_vat | No | `Number` | | 0 | trường ẩn | CVAT |
| 13 | Stock prefix | stock_prefix | No | String | 5 | | trường ẩn | CSTKPRE |
| 14 | Currency | currency_code | No | String | 3 | | trường ẩn | CCCR |
| 15 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | 15 | |
| 3 | Stock number | stock_number | String | | |
| 4 | From serial | from_serial | String | | |
| 5 | To serial | to_serial | String | | |
| 6 | Number of leaves | number_of_leaves | `Number` | | |
| 7 | Fee amount | fee_amount | `Number` | | |
| 8 | Description | description | String | | |
| 9 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 10 | Stock type | stock_type | String | | |
| 11 | Total Fee for Cash | total_fee_for_cash | `Number` | | |
| 12 | Total VAT for Cash | total_vat_for_cash | `Number` | | |
| 13 | Fee VAT | fee_vat | `Number` | | |
| 14 | Stock prefix | stock_prefix | String | | |
| 15 | Currency | currency_code | String | | |
| 16 | Transaction status | status | String |  |  |
| 17 | Transaction date | transaction_date | `Date time` |  |  |
| 18 | User id | user_id | `Number` |  | 
| 19 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Books exist in Teller (user do transaction is who is managing these books).
    +	Stock type: Cheque
    +	Book status: Assigned to teller
    +	Confirm status: Confirm
    +	Status of stock leaves: Normal
-   Deposit account:
    +	Deposit type: Current
    +	Exists with status is "Normal"
    +   Same branch with stock

**Flow of events:**
-   Allow issue many books at the same time (KTB không xét được assign teller)
-	Allow collect fee for this transaction: fee issue cheque or other fees.
-	Don't allow issued cheque for other account. Except account is closed.
-	Transaction complete:
    +	System will show pages of cheque to print out.
    +	All leaves of books have status "Unpaid".

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| Case collect fee (fee amount > 0) | | | | |
| 1 | D | CASH | Fee amount | Currency of deposit account |
| 1 | C | IFCC (income) | Fee amount | Currency of deposit account |

**Cập nhật thông tin stock:**
[Tham khảo thông tin chi tiết tại mục 8](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A2`, `A1`, n*`A7`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_NO_LEAF | DPT_CIS_GET_INFO_NO_LEAF | [DPT_CIS_GET_INFO_NO_LEAF](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_NO_FR | DPT_CIS_GET_INFO_NO_LEAF | [DPT_CIS_GET_INFO_NO_LEAF](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX | [DPT_GET_STOCK_PREFIX](Specification/Common/15 Deposit Rulefuncs) |
| 6 | TRAN_FEE_DPT_CIS/GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 7 | TRAN_FEE/GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 8 | TRAN_FEE_DPT_CIS/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>