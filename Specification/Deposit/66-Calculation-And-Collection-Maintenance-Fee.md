# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Account number: exists with status <> {"Closed"}.
- Deposit account type is “Current”.

**Flow of events:**
- Calculation maintenance fee:
    - Maintenance fee generate recurring (recurring is year, each 12 months from date of open account).
    - When open account, maintenance fee is zero (0) (because the Bank do not collect maintenance fee in year open account).
    - This fee is generated in beginning of recurring and fee amounts have not paid that will plus into [outstanding] of IFC list tag of deposit account.
    - Example, open account 02/03/2016, maintenance fee is generated following:
        - 02/03/2016 => fee is zero (0)
        - 02/03/2017 => generating fee
        - 02/03/2018 => generating fee
- Collection maintenance fee:
    - Collection the fee from itself deposit account.
    - Time of charging: Collection fee on each December 25th year.
    - Collection from amount of [Available balance] of deposit account.
    - Only collection maintenance fee when [Available balance] greater or equal maintenance fee (that is meaning the system do not allow partial. 
    - Example: 
        - Maintenance fee is 10,000 KHR, [available Balance] is 50 KHR
        - 25/12/2016, the system does NOT collection 50 KHR

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Amount maintenance fee is collected | Currency of account |
| 1 | C | IFCC (income) | Amount maintenance fee is collected | Currency of account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |