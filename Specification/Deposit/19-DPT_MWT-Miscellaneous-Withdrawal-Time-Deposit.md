# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_MWT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Master FD account | master_fd_account | `Yes` | String | 25 | | | PDMACC |
| 2 | Link FD account | link_fd_account | `Yes` | String | 25 | | | PDACC  |
| 3 | Current balance | current_balance | `Yes` | `Number` | | 0 | | CAMT2  |
| 4 | Withdraw amount | withdraw_amount | `Yes` | `Number` | | >0 | | PGAMT  |
| 5 | GL account | gl_account | `Yes` | String | 25 | | | PGACC  |
| 6 | Accounting currency | accounting_currency | No | String | 3 | | trường ẩn | CCCR1  |
| 7 | Account currency | account_currency | No | String | 3 | | trường ẩn | CCCR   |
| 8 | Withdrawer name | withdrawer_name | `Yes` | String | 250 | | | CACNM  |
| 9 | Withdrawer code | withdrawer_code | No | String | 15 | | | CCTMCD |
| 10 | Withdrawer address | withdrawer_address | No | String | 250 | | | CCTMA  |
| 11 | Withdrawer description | withdrawer_description | No | JSON Object | | | | MDESC  |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 12 | Total fee | total_fee | No | `Number` | | 0 | trường ẩn | TXAMT  |
| 13 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 14 | Description | description | No | String | 250 | | | DESCS  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Master FD account | master_fd_account | String | | |
| 3 | Link FD account | link_fd_account | String | | |
| 4 | Current balance | current_balance | `Number` | | |
| 5 | Withdraw amount | withdraw_amount | `Number` | | |
| 6 | GL account | gl_account | String | | |
| 7 | Accounting currency | accounting_currency | String | | trường ẩn |
| 8 | Account currency | account_currency | String | | trường ẩn |
| 9 | Withdrawer name | withdrawer_name | String | | |
| 10 | Withdrawer code | withdrawer_code | String | | |
| 11 | Withdrawer address | withdrawer_address | String | | |
| 12 | Withdrawer description | withdrawer_description | JSON Object | | |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 13 | Total fee | total_fee | `Number` | | trường ẩn |
| 14 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 15 | Description | description | String | | |
| 16 | Transaction status | status | String |  |  |
| 17 | Transaction date |  | `Date time` |  |  |
| 18 | User id |  | `Number` |  |  |
| 19 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | |

## 1.2 Transaction flow
**Conditions:**
-	"Master Fixed Deposit account": exists with status {"Active"}. Account type is Master FD account.
-	"Link Fixed Deposit account": exists with status {"Active", "Maturity", "Normal"}. Account type is Link FD account and belongs to Master FD account.
-	GL account: exists and same branch with user. 
-	GL allows credit side.
-	Currency of GL must be same with currency of deposit account.

**Flow of events:**
-	Don't allow withdraw amount that different currency with account.
-	Allow to collect fee if withdrawal different branch and another fee.
-	Change status to Normal if account is "Dormant". (KTB thay đổi nghiệp vụ, deposit account có status là Dormant không cho phép thực hiện giao dịch)
-	Balance will decrease.
-	If transaction has collect fee, system will collect fee from GL
<br>Ex: Withdraw amount = 100, Fee = 10 => GL will receive: 100, system collect fee from deposit: 10
-	Update LFA follows:
<br>![Withdraw-time-deposit](uploads/e164e84139708eba81535794c1e4d8c0/Withdraw-time-deposit.png)

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Amount withdraw | Currency of account |
| 1 | C | GL | Amount withdraw | Currency of account |
| 2 | D | DEPOSIT | Fee amount | Currency of account |
| 2 | C | INCOME | Fee amount | Currency of account |

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 5](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | {} is not allowed to be credit side | Posting side của GL account không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDMACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_CUSTOMER | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_INFO_PGEXR | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 6 | LKP_DATA_BACNO | ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY | [ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY](Specification/Common/11 Accounting Rulefuncs) |
| 7 | LKP_DATA_PDACC | DPT_LOOKUP_DEPOSIT_BY_MACNO | [DPT_LOOKUP_DEPOSIT_BY_MACNO](Specification/Common/15 Deposit Rulefuncs) |
| 8 | TRAN_FEE_CRD_FCG\GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 9 | TRAN_FEE\GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 10 | TRAN_FEE_CRD_FCG\LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>