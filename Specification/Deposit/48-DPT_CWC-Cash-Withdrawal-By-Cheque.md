# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CWC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Stock prefix | stock_prefix | `Yes` | String | 5 | | trường ẩn | CSTKPRE  |
| 2 | Cheque no | cheque_no | `Yes` | String | 20 | | | CFRSER |
| 3 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 4 | Current balance | current_balance | No | `Number` | | 0 | | CAMT2 |
| 5 | Available balance | available_balance | No | `Number` | | 0 | | CAMT3 |
| 6 | Cheque amount | cheque_amount | `Yes` | `Number` | | 0 | | PCSAMT   |
| 7 | Cash currency | cash_currency | `Yes` | String | 3 | | trường ẩn | PCSCCR |
| 8 | Cross rate | cross_rate | `Yes` | `Number` | | 0 | trường ẩn | CCRRATE |
| 9 | Cash amount | cash_amount | No | `Number` | | 0 | trường ẩn | CTCVT |
| 10 | Exchange rate | exchange_rate | `Yes` | `Number` | | 0 | trường ẩn | PCSEXR |
| 11 | Cash amount /BCY | cash_amount_bcy | No | `Number` | | 0 | trường ẩn | PCSCVT |
| 12 | Withdrawer name | withdrawer_name | `Yes` | String | 250 | | | CACNM |
| 13 | Withdrawer id | withdrawer_id | No | String | 15 | | | CCTMCD |
| 14 | Withdrawer address | withdrawer_address | No | String | 250 | | | CCTMA |
| 15 | Withdrawer description | withdrawer_description | No | JSON Object | | | | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 16 | ID issue date | id_issue_date | No | `Date time` | | Working date | trường ẩn | CIDDT |
| 17 | ID place | id_place | No | String | 90 | | trường ẩn | CIDPLACE |
| 18 | Identification number | identification_number | No | String | 100 | | trường ẩn | CREPID |
| 19 | Value date | value_date | No | `Date time` | | Working date | trường ẩn | CVLDT |
| 20 | Currency of deposit account | currency_of_deposit_account | No | String | 3 | | trường ẩn | CCCR |
| 21 | Exchange rate (Debit account/BCY) | exchange_rate_debit_account_bcy | No | `Number` | | 0 | trường ẩn | CBKEXR |
| 22 | Amount (Debit account/BCY) | amount_debit_account_bcy | No | `Number` | | 0 | trường ẩn | CCVT     |
| 23 | Commission | commission | No | `Number` | | 0 | trường ẩn | CFAMT    |
| 24 | Interest from Earlywdr | interest_from_earlywdr | No | `Number` | | 0 | trường ẩn | CTINT |
| 25 | Due date | due_date | No | `Date time` | | Working date | trường ẩn | DUEDT    |
| 26 | Interest prepaid | interest_prepaid | No | `Number` | | 0 | trường ẩn | CIPRE |
| 27 | Total VAT for Cash | total_vat_for_cash | No | `Number` | | 0 | trường ẩn | CSVAT |
| 28 | Account Linkage | account_linkage | No | String | 25 | | trường ẩn | PDOACC |
| 29 | Amount linkage | amount_linkage | No | `Number` | | 0 | trường ẩn | CAMT4 |
| 30 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  | 0 | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  | 0 | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  | 0 | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  | 0 | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 31 | Description | description | No | String | 250 | | | DESCS |
| 32 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2	| Stock prefix | stock_prefix | String | | trường ẩn |
| 3 | Cheque no | cheque_no | String | | |
| 4 | Account number | account_number | String | | |
| 5 | Current balance | current_balance | `Number` | | |
| 6 | Available balance | available_balance | `Number` | | |
| 7 | Cheque amount | cheque_amount | `Number` | | |
| 8 | Cash currency | cash_currency | String | | trường ẩn |
| 9 | Cross rate | cross_rate | `Number` | | trường ẩn |
| 10 | Cash amount | cash_amount | `Number` | | trường ẩn |
| 11 | Exchange rate | exchange_rate | `Number` | | trường ẩn |
| 12 | Cash amount /BCY | cash_amount_bcy | `Number` | | trường ẩn |
| 13 | Withdrawer name | withdrawer_name | String | | |
| 14 | Withdrawer id | withdrawer_id | String | | |
| 15 | Withdrawer address | withdrawer_address | String | | |
| 16 | Withdrawer description | withdrawer_description | JSON Object | | |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 17 | ID issue date | id_issue_date | `Date time` | | trường ẩn |
| 18 | ID place | id_place | String | | trường ẩn |
| 19 | Identification number | identification_number | String | | trường ẩn |
| 20 | Value date | value_date | String | | trường ẩn |
| 21 | Currency of deposit account | currency_of_deposit_account | String | | trường ẩn |
| 22 | Exchange rate (Debit account/BCY) | exchange_rate_debit_account_bcy | `Number` | | trường ẩn |
| 23 | Amount (Debit account/BCY) | amount_debit_account_bcy | `Number` | | trường ẩn |
| 24 | Commission | commission | `Number` | | trường ẩn |
| 25 | Interest from Earlywdr | interest_from_earlywdr | `Number` | | trường ẩn |
| 26 | Due date | due_date | String | | trường ẩn |
| 27 | Interest prepaid | interest_prepaid | `Number` | | trường ẩn |
| 28 | Total VAT for Cash | total_vat_for_cash | `Number` | | trường ẩn |
| 29 | Account Linkage | account_linkage | String | | trường ẩn |
| 30 | Amount linkage | amount_linkage | `Number` | | trường ẩn |
| 31 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 32 | Description | description | String | | |
| 33 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String | 3 | | |
| 34 | Transaction status | status | String | | |
| 35 | Transaction date | transaction_date | `Date time` |  |  |
| 36 | User id | user_id | `Number` |  |  |
| 37 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Cheque no.: exists with status "Unpaid".
-	Current account must have balance to withdraw (not include amount is keeping).
- Deposit account exists with status <> "Closed"/"Block"
- Teller do this transaction:
  - User must have enough cash to withdraw.
  - User has right to do transaction related to cash.
  - User must have enough cash to give customer.

**Flow of events:**
-	Don't allow withdraw amount that different currency with current account. 
-	Allow to collect fee as: fee withdrawal by cheque and other fees.
-	Transaction complete:
    +	Change status to Normal if current account is "Dormant".
    +	Balance will decrease.
    +	Stock leaves status: Paid.

**Posting**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Amount withdrawal | Currency of deposit account |
| 1 | C | CASH | Amount withdrawal | Currency of deposit account |
| Case collect fee (fee amount > 0) | | | | |
| 2 | D | CASH | Fee amount | Currency of deposit account |
| 2 | C | IFCC (income) | Fee amount | Currency of deposit account |

**Cập nhật thông tin stock:**
[Tham khảo thông tin chi tiết tại mục 9](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_EMK | DPT_GET_INFO_EMK_BY_SERIAL | [DPT_GET_INFO_EMK_BY_SERIAL](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX_C | [DPT_GET_STOCK_PREFIX_C](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_AC | DPT_GET_INFO_PDACC_BY_CHEQUE_SERIAL | [DPT_GET_INFO_PDACC_BY_CHEQUE_SERIAL](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_BCY_CACNM | GET_INFO_ACCOUNT_BY_ACNO_DPTTYPE | [GET_INFO_ACCOUNT_BY_ACNO_DPTTYPE](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_INFO_ACLINK | DPT_GET_INFO_ACLINK | [DPT_GET_INFO_ACLINK](Specification/Common/15 Deposit Rulefuncs) |
| 6 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 7 | TRAN_FEE\GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 8 | TRAN_FEE\LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>