# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Accounts has balance. And status <> "Closed".

**Flow of events:**
- Interest amount daily = Balance * interest rate of day.
    - If interest rate of a year, system will convert to interest rate of a day.
    - Decimal maximum is 5 digits. Can define less.
        - Withholding tax amount daily = Interest amount daily * Withholding tax rate
    - Withholding tax rate based on resident status of account:
        - With FD accounts: if change resident status. It will apply for next tenor.
        - With DD accounts: If change resident status. It applies immediately.
    - Decimal maximum is 5 digits. Can define less.
- On each account allows to setup parameters:
    - Interest rate and conditions
    - Withholding tax rate and conditions
- System will calculate Interest amount daily and Withholding tax amount daily on end of date.
- If accounts don’t setup interest rate or withholding tax rate, system will skip calculation.
    - Example: current account, don’t need calculation interest rate and withholding tax rate.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | PAID_INTEREST | Interest amount daily | Currency of deposit account |
| 1 | C | INTEREST | Interest amount daily | Currency of deposit account |
| 2 | D | PAID_WHT | Withholding tax amount daily | Currency of deposit account |
| 2 | C | WITHHOLDING_TAX | Withholding tax amount daily | Currency of deposit account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |