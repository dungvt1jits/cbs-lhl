# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_OCC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Cheque no | cheque_no | `Yes` | String | 20 | | | CCHQNO |
| 2 | Cheque currency | cheque_currency | No | String | 3 | | | CCCR |
| 3 | issuing bank | issuing_bank | No | String | 25 | | | SBNAME |
| 4 | Clearing bank | clearing_bank | `Yes` | String | 25 | | | CIBNK |
| 5 | Cheque amount | cheque_amount | `Yes` | `Number` | | 0 | | CAMT |
| 6 | Clearing bank Nostro acc | clearing_bank_nostro_acc | `Yes` | String | 25 | | | PGACC |
| 7 | Beneficiary account | beneficiary_account | `Yes` | String | 25 | | | PDACC |
| 8 | Beneficiary currency | beneficiary_currency | No | String | 3 | | | RCCR |
| 9 | Beneficiary amount | beneficiary_amount | No | `Number` | | 0 | | PCSAMT |
| 10 | CBM account | cbm_account | No | String | 25 | | trường ẩn | PDOACC |
| 11 | Holding days | holding_days | No | `Number` | | 0 | trường ẩn | CAMT1 |
| 12 | Drawer name | drawer_name | No | String | 250 | | | CACNM2  |
| 13 | Drawer paper number | drawer_paper_number | No | String | 25 | | | CREPID2 |
| 14 | Drawer address | drawer_address | No | String | 250 | | | SBADDR |
| 15 | Description | description | No | String | 250 | | | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | | |
| 3	| Cheque no	| cheque_no | String	| | |
| 4 | Cheque currency | cheque_currency | String | | |
| 5 | issuing bank | issuing_bank | String | | |
| 6 | Clearing bank | clearing_bank | String | | |
| 7 | Cheque amount | cheque_amount | `Number` | | |
| 8 | Clearing bank Nostro acc | clearing_bank_nostro_acc | String | | |
| 9 | Beneficiary account | beneficiary_account | String | | |
| 10 | Beneficiary currency | beneficiary_currency | String | | |
| 11 | Beneficiary amount | beneficiary_amount | `Number` | | |
| 12 | CBM account | cbm_account | String | | trường ẩn |
| 13 | Holding days | holding_days | `Number` | | trường ẩn |
| 14 | Drawer name | drawer_name | String | | |
| 15 | Drawer paper number | drawer_paper_number | String | | |
| 16 | Drawer address | drawer_address | String | | |
| 17 | Description | description | String | | |
| 18 | Transaction status | status | String |  |  |
| 19 | Transaction date | transaction_date | `Date time` |  |  |
| 20 | User id | user_id | `Number` |  | 

## 1.2 Transaction flow
**Conditions:**
-	Outward cheque exists with status <> "C"/ "S" in queue.
-	GL allows debit side. 
-   Debit GL account and beneficiary account must same branch with branch does this tranaction
-   All information map with cheque no

**Flow of events:**
-	Select it in queue and click button "Accept".
-	System will auto create transaction to accept payment and information is taken from Outward cheque.
-	Transaction complete: 
    +	Outward cheque doesn't show in queue.
    +	Customer can use beneficiary amount. (Not block beneficiary amount in account).

**Database:**
ClearingCheck
DepositTransaction

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CHEQUE | DPT_GET_INFO_CHEQUE | [DPT_GET_INFO_CHEQUE](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_INFO_CUST | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_CIBNK | GET_INFO_CIBNK | [GET_INFO_CIBNK](Specification/Common/16 Payment Rulefuncs) |
| 4 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 5 | LKP_DATA_VOSTRO | ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY | [ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY](Specification/Common/11 Accounting Rulefuncs) |
| 6 | LKP_DATA_CIBNK | PMT_LOOKUP_AGENTBANK | [PMT_LOOKUP_AGENTBANK](Specification/Common/16 Payment Rulefuncs) |
| 7 | LKP_DATA_PDOACC | ACT_ACCHRT_LOOKUP_BY_BRANCHID | [ACT_ACCHRT_LOOKUP_BY_BRANCHID](Specification/Common/11 Accounting Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>