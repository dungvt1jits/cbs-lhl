# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_MDP`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 | | | PDACC   |
| 2 | Amount Deposit | amount_deposit | No | `Number` | | >0 | | CTCVT   |
| 3 | Debit accounting | debit_accounting | `Yes` | String | 25 | | | PGACC   |
| 4 | Accounting currency | accounting_currency | No | String | 3 | | trường ẩn | PGCCR   |
| 5 | Cross rate | cross_rate | `Yes` | `Number` | | 0 | trường ẩn | CCRRATE |
| 6 | Accounting amount | acounting_amount | `Yes` | `Number` | | 0 | trường ẩn | PGAMT   |
| 7 | Exchange rate of accounting/BCY | exchange_rate_of_accounting_bcy | No | `Number` | | 0 | trường ẩn | PGEXR   |
| 8 | Accounting amount/BCY | accounting_amount_bcy | No | `Number` | | 0 | trường ẩn | PGCVT   |
| 9 | Depositor name | depositor_name | `Yes` | String | 250 | | | CACNM   |
| 10 | Depositor code | depositor_code | No | String | 15 | | | CCTMCD  |
| 11 | Depositor address | depositor_address | No | String | 250 | | | CCTMA   |
| 12 | Depositor description | depositor_description | No | JSON Object | | | | MDESC   |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 13 | Values date | values_date | No | `Date time` | | Working date | trường ẩn | CVLDT   |
| 14 | Deposit currency | deposit_currency | No | String | 3 | | trường ẩn | CCCR    |
| 15 | Exchange rate (Debit account/BCY) | exchange_rate_debit_account_bcy | No | `Number` | | 0 | trường ẩn | CBKEXR  |
| 16 | Amount (Debit account/BCY) | amount_debit_account_bcy | No | `Number` | | 0 | trường ẩn | CCVT    |
| 17 | Accounting currency | accounting_currency1 | No | String | 3 | | trường ẩn | CCCR1   |
| 18 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  | 0 | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  | 0 | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  | 0 | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  | 0 | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 19 | Description | description | No | String | 250 | | | DESCS   |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | |
| 2 | Account number | account_number | String | | |
| 3 | Amount Deposit | amount_deposit | `Number` | | |
| 4 | Debit accounting | debit_accounting | String | | |
| 5 | Accounting currency | accounting_currency | String | | trường ẩn |
| 6 | Cross rate | cross_rate | `Number` | | trường ẩn |
| 7 | Acounting amount | acounting_amount | `Number` | | trường ẩn |
| 8 | Exchange rate of accounting/BCY | exchange_rate_of_accounting_bcy | `Number` | | trường ẩn |
| 9 | Accounting amount/BCY | accounting_amount_bcy | `Number` | | trường ẩn |
| 10 | Depositor name | depositor_name | String | | |
| 11 | Depositor code | depositor_code | String | | |
| 12 | Depositor address | depositor_address | String | | |
| 13 | Depositor description | depositor_description | JSON Object | | |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 14 | Values date | values_date | `Date time` | | trường ẩn |
| 15 | Deposit currency | deposit_currency | String | | trường ẩn |
| 16 | Exchange rate (Debit account/BCY) | exchange_rate_debit_account_bcy | `Number` | | trường ẩn |
| 17 | Amount (Debit account/BCY) | amount_debit_account_bcy | `Number` | | trường ẩn |
| 18 | Accounting currency | accounting_currency1 | String | | trường ẩn |
| 19 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 20 | Description | description | String | | |
| 21 | Posting data | | JSON Object | | | |
| | Group | | `Number` | | | |
| | Index in group | | `Number` | | | |
| | Posting side | | String | | | |
| | System account name | | String | | | |
| | GL account number | | String | | | |
| | Amount | | `Number` | | số có hai số thập phân | |
| | Currency | | String | 3 | |
| 22 | Transaction status | status | String | | |
| 23 | Transaction date | transaction_date | `Date time` |  |  |
| 24 | User id | user_id | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
-	"Deposit account": exists with status is "New"/ "Normal"/ ~~"Dormant"~~.
-	GL account: exists and same branch with user. 
-	GL allows debit side. (KTB không xét điều kiện này)
-	Currency of GL must be same with currency of deposit account.
- Teller does this transaction:
  - User has right to do transaction related to cash.

**Flow of events:**
-	If first time deposit. Amount of credit account must be larger than "Initial deposit amount".
-	If setup parameter: allow deposit one-time. System will not allow to continue deposit amount.
-	Change status of credit account if first time deposit:
	+	Account type is current, change status to "Normal".
	+	Account type is saving, change status to "Normal".
	+	Account type is fixed deposit, change status to "Actived" (if allow deposit: only one time).
	+	Account type is fixed deposit, change status to "Normal" (if allow deposit: multi-time).
-	Allow collection fees for this transaction (if any).
-	Debit GL.
-   If transaction has collect fee, system will collect fee from deposit amount
<br>Ex: Deposit amount = 100, Fee = 10 => Deposit account will receive: 90, system collect fee from GL: 10 

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | GL | Amount deposit | Currency of account |
| 1 | C | DEPOSIT | Amount deposit | Currency of account |
| Case collect fee (fee amount > 0) | | | | |
| 2 | D | CASH | Fee amount | Currency of account |
| 2 | C | IFCC (income) | Fee amount | Currency of account |

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 4](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | {} is not allowed to be debit side | Posting side của GL account không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PGCCR | accounting_currency, accounting_currency1 lấy chung 1 giá trị của currency_code từ rule func ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 2 | LKP_DATA_BACNO | ACT_ACCHRT_LOOKUP_BY_CURRENCY, gửi đi deposit_currency | [ACT_ACCHRT_LOOKUP_BY_CURRENCY](Specification/Common/11 Accounting Rulefuncs) |
| 3 | GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 4 | LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |
| 5 | GET_INFO_PGCCR | lấy giá trị rate_currency01 từ rule func FX_RULEFUNC_GET_INFO_PCSEXR, gửi đi accounting_currency và branch_id_fx của user login | [FX_RULEFUNC_GET_INFO_PCSEXR](Specification/Common/20 FX Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>