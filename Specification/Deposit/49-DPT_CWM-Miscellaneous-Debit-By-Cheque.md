# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CWM`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Stock prefix | stock_prefix | `Yes` | String | 5 | | trường ẩn | CSTKPRE |
| 2 | Cheque no | cheque_no | `Yes` | String | 20 | | | CFRSER |
| 3 | Account number | account_number | `Yes` | String | 25 | | | PDACC |
| 4 | Current balance | current_balance | `Yes` | `Number` | | > 0 | | CAMT2 |
| 5 | Available balance | available_balance | `Yes` | `Number` | | > 0 | | CAMT3 |
| 6 | Interest prepaid | interest_prepaid | No | `Number` | | 0 | trường ẩn | CIPRE |
| 7 | Interest earlywdr | interest_earlywdr | No | `Number` | | 0 | trường ẩn | CTINT |
| 8 | Cheque amount | cheque_amount | `Yes` | `Number` | | > 0 | | PGAMT |
| 9 | Credit accounting | credit_accounting | `Yes` | String | 25 | | | PGACC |
| 10 | Accounting currency | accounting_currency | No | String | 3 | | | CCCR1 |
| 11 | Cross rate | cross_rate | `Yes` | `Number` | | > 0 | trường ẩn | CCRRATE |
| 12 | Accounting amount | accounting_amount | No | `Number` | | 0 | | CPGCVT |
| 13 | Exchange rate of accounting/BCY | exchange_rate_of_accounting | No | `Number` | | 0 | trường ẩn | PGEXR |
| 14 | Amount equivalent in BCY | amount_equivalent_in_bcy | No | `Number` | | 0 | trường ẩn | PGCVT |
| 15 | Withdrawer name | withdrawer_name | `Yes` | String | 250 | | | CACNM |
| 16 | Withdrawer code | withdrawer_code | No | String | 15 | | | CCTMCD |
| 17 | Withdrawer address | withdrawer_address | No | String | 250 | | | CCTMA |
| 18 | Withdrawer description | withdrawer_description | No | JSON Object | | | | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 19 | Values date | values_date | No | `Date time` | | | trường ẩn | CVLDT |
| 20 | Currency of deposit account | currency_of_deposit_account | No | String | 3 | | trường ẩn | CCCR |
| 21 | Exchange rate (Debit account/BCY) | exchange_rate_of_debit_account | No | `Number` | | 0 | trường ẩn | CBKEXR |
| 22 | Amount (Debit account/BCY) | amount_of_debit_account | No | `Number` | | 0 | trường ẩn | CCVT |
| 23 | Account Linkage | account_linkage | No | String | 25 | | | PDOACC |
| 24 | Amount linkage | amount_linkage | No | `Number` | | 0 | | CAMT4 |
| 25 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  | 0 | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  | 0 | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  | 0 | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  | 0 | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 26 | Description | description | No | String | 250 | | | DESCS |
| 27 | Branch name | branch_name | No | String | | | `gửi thêm` | |


SELECT O9UTIL.GET_CROSS_RATE ('@CCCR','@CCCR1','TB',CASE WHEN '@CCCR' = '@CCCR1' THEN 'TB' ELSE 'TA' END, '@CRRBRID' ,NULL,'@CCRRATE'),'@CCCR'||'/'||'@CCCR1' FROM DUAL	

CCRRATE|CFRSER

currency_code01 = currency_of_deposit_account
currency_code02 = accounting_currency
branch_id_fx = 
cross_rate = cross_rate


| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency_code01 | No | String |  |  |  |
| 2 | Currency 02 | currency_code02 | No | String |  |  |  |
| 3 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên |
| 4 | Cross rate | cross_rate | No | `Number` |  |  | số có 9 số thập phân |

- **Response message:**

CCRRATE|TXB_CCRRATE


| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân, `crate` |
| 2 | txb_ccrrate | txb_ccrrate | String |  | `txb_ccrrate` |




### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | | |
| 2 | Stock prefix | stock_prefix | String | | trường ẩn |
| 3 | Cheque no | cheque_no | String | | |
| 4 | Account number | account_number | String | | |
| 5 | Current balance | current_balance | `Number` | | |
| 6 | Available balance | available_balance | `Number` | | |
| 7 | Interest prepaid | interest_prepaid | `Number` | | trường ẩn |
| 8 | Interest earlywdr | interest_earlywdr | `Number` | | trường ẩn |
| 9 | Cheque amount | cheque_amount | `Number` | | |
| 10 | Credit accounting | credit_accounting | String | | |
| 11 | Accounting currency | accounting_currency | String | | |
| 12 | Cross rate | cross_rate | `Number` | | trường ẩn |
| 13 | Accounting amount | accounting_amount | `Number` | | |
| 14 | Exchange rate of accounting/BCY | exchange_rate_of_accounting | `Number` | | trường ẩn |
| 15 | Amount equivalent in BCY | amount_equivalent_in_bcy | `Number` | | trường ẩn |
| 16 | Withdrawer name | withdrawer_name | String | | |
| 17 | Withdrawer code | withdrawer_code | String | | |
| 18 | Withdrawer address | withdrawer_address | String | | |
| 19 | Withdrawer description | withdrawer_description | JSON Object | | |
| 20 | Values date | values_date | `Date time` | | trường ẩn |
| 21 | Currency of deposit account | currency_of_deposit_account | String | | trường ẩn |
| 22 | Exchange rate (Debit account/BCY) | exchange_rate_of_debit_account | `Number` | | trường ẩn |
| 23 | Amount (Debit account/BCY) | amount_of_debit_account | `Number` | | trường ẩn |
| 24 | Account Linkage | account_linkage | String | | |
| 25 | Amount linkage | amount_linkage | `Number` | | |
| 26 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 27 | Description | description | String | | |
| 28 | Transaction status | status | String |  |  |
| 29 | Transaction date | transaction_date | `Date time` |  |  |
| 30 | User id | user_id | `Number` |  |  |
|  | Step code: `ACT_EXECUTE_POSTING` |  |  |  |  |
| 31 | Posting data | | JSON Object | | |
|  | Group | accounting_entry_group | `Number` |  | số nguyên dương |
|  | Index in group | accounting_entry_index | `Number` |  | số nguyên dương |
|  | Posting side | debit_or_credit | String |  | D or C |
|  | Account number | bank_account_number | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Currency | currency_code | String |  |  |
| 32 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Cheque no.: exists with status "Unpaid".
-	Current account must have balance to withdraw (not include amount is keeping).
-	Amount withdrawal must be > 0.
-	GL account: exists and same branch with user. 
-	GL allows credit side.
-	Currency of GL must be same with currency of deposit account.

**Flow of events:**
-	Allow to collect fee as: fee withdrawal by cheque and other fees.
-	Transaction complete:
    +	Change status to Normal if current account is "Dormant".
    +	Debit GL.
    +	Stock leaves status: Paid

**Posting**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Amount withdrawal | Currency of deposit account |
| 1 | C | GL | Amount withdrawal | Currency of deposit account |
| Case collect fee (fee amount > 0) | | | | |
| 2 | D | CASH | Fee amount | Currency of deposit account |
| 2 | C | IFCC (income) | Fee amount | Currency of deposit account |

**Cập nhật thông tin stock:**
[Tham khảo thông tin chi tiết tại mục 9](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_INFO_EMK | DPT_GET_INFO_EMK_BY_SERIAL | [DPT_GET_INFO_EMK_BY_SERIAL](Specification/Common/15 Deposit Rulefuncs) | `serial` = `cheque_no` | `amount` = `cheque_amount` |
| 2 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX_C | [DPT_GET_STOCK_PREFIX_C](Specification/Common/15 Deposit Rulefuncs) | `serial` = `cheque_no` | `stock_prefix` = `stock_prefix` |
| 3 | GET_INFO_AC | DPT_GET_INFO_PDACC_BY_CHEQUE_SERIAL | [DPT_GET_INFO_PDACC_BY_CHEQUE_SERIAL](Specification/Common/15 Deposit Rulefuncs) | `serial` = `cheque_no` | `account_number` = `account_number` |
| 4 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) | `account_number` = `account_number` | `account_name` = `withdrawer_name`, `currency_code` = `currency_of_deposit_account`, `intpre` = `interest_prepaid`, `current_balance` = `current_balance` |
| 5 | GET_INFO_CUSTOMER | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) | `account_number` = `account_number` | `customer_code` = `withdrawer_code`, `address` = `withdrawer_address` |
| 6 | GET_INFO_AVALABLE_BAL | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) | `account_number` = `account_number` | `available_balance` = `available_balance` |
| 7 | GET_INFO_ACLINK | DPT_GET_INFO_ACLINK | [DPT_GET_INFO_ACLINK](Specification/Common/15 Deposit Rulefuncs) | `account_number` = `account_number` | `account_number` = `account_linkage` |
| 8 | GET_INFO_PCSEXR | FX_RULEFUNC_GET_INFO_PCSEXR | [FX_RULEFUNC_GET_INFO_PCSEXR](Specification/Common/20 FX Rulefuncs) | `currency_code` = `currency_of_deposit_account`, `branch_id_fx` = `branch_id` của user làm giao dịch | `rate_currency01` = `exchange_rate_of_accounting` |
| 9 | GET_INFO_CCRRATE | FX_RULEFUNC_GET_INFO_CCRRATE_TB_TA | [FX_RULEFUNC_GET_INFO_CCRRATE_TB_TA](Specification/Common/20 FX Rulefuncs) | `currency_code01` = `currency_of_deposit_account`, `currency_code02` = `accounting_currency`, `branch_id_fx` = `branch_id` của user làm giao dịch, `cross_rate` = `cross_rate` | `cross_rate` = `cross_rate` |
| 10 | GET_INFO_CBKEXR | FX_RULEFUNC_GET_INFO_CBKEXR | [FX_RULEFUNC_GET_INFO_CBKEXR](Specification/Common/20 FX Rulefuncs) | `currency_code` = `currency_of_deposit_account`, `branch_id_fx` = `branch_id` của user làm giao dịch | `bk_rate_currency` = `exchange_rate_of_debit_account` |
| 11 | GET_INFO_CCRRATE1 | FX_RULEFUNC_GET_INFO_CCRRATE_TB_TA | [FX_RULEFUNC_GET_INFO_CCRRATE_TB_TA](Specification/Common/20 FX Rulefuncs) | `currency_code01` = `currency_of_deposit_account`, `currency_code02` = `accounting_currency`, `branch_id_fx` = `branch_id` của user làm giao dịch, `cross_rate` = `cross_rate` | `cross_rate` = `cross_rate` |
| 12 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) | `account_number` = `account_number`, `module` = `DPT` |  |
| 13 | GET_INFO_PGEXR | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) | `bank_account_number` = `credit_accounting` | `currency_code` = `accounting_currency` |
| 14 | LKP_DATA_BACNO | ACT_ACCHRT_LOOKUP_BY_BRANCHID | [ACT_ACCHRT_LOOKUP_BY_BRANCHID](Specification/Common/11 Accounting Rulefuncs) | `account_branch_id` = `branch_id` của user làm giao dịch |  |
| 15 | TRAN_FEE/GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |  |  |
| 16 | TRAN_FEE/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |  |  |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>