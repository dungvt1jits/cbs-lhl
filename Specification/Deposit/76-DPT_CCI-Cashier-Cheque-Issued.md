# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_CCI`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Stock number  | stock_number | No | String | 20 | | | STK_NO |
| 2 | Serial no | serial_no | `Yes` | String | 20 | | | CFRSER |
| 3 | Issuing name | issuing_name | No | String | | | | CACNM |
| 4 | Amount | amount | `Yes` | `Number` |  | > 0 | | PCSAMT |
| 5 | Currency | currency_code | No | String | 3 | | | CCCR |
| 6 | Beneficiary name | beneficiary_name | No | String | 250 | | | CCTMA |
| 7 | Beneficiary contact number | contact | No | String | 35 | | | CONTACT |
| 8 | Purpose | purpose | No | String | 35 | | | PURPOSE |
| 9 | Debit method | debit_method | `Yes` | String | 3 | | | PAYBY |
| 10 | Debit account | debit_account | No | String | 25 | | | SACNO |
| 11 | Description | description | No | String | 250 | | | DESCS |
| 12 | Stock prefix | stock_prefix | No | String | 5 | | trường ẩn | CSTKPRE |
| 13 | Leaf no | leaf_no | No | `Number` | | 0 | trường ẩn | CAMT3 |
| 14 | Branch id | branch_id | No | `Number` | | branch id của user login | trường ẩn | CBRCD |
| 15 | Beneficiary ID number | beneficiary_id_number | No | String | 50 |  | `gửi thêm` |  |
| 16 | Beneficiary address | beneficiary_address | No | String | 50 |  | `gửi thêm` |  |
| 17 | Issued date | issued_date | No | `Date time` |  |  | `gửi thêm` |  |
| 18 | Expired date | expired_date | No | `Date time` |  |  | `gửi thêm` |  |
| 19 | Issuer ID number | issuer_id_number | No | String |  |  | `gửi thêm` 12/01/2023 |
| 20 | Issuer contact number | issuer_contact_number | No | String |  |  | `gửi thêm` 12/01/2023 |
| 21 | Fee data | fee_data | No | Array Object |  |  | `gửi thêm` |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  |  |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân |  |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân |  |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân |  |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân |  |
|  | Currency | currency_fee_code | No | String | 3 |  |  |  |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn |  |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn |  |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT or ACT | trường ẩn |  |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn |  |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn |  |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn |  |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn |  |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Stock number  | stock_number | String | | | |
| 3 | Serial no | serial_no | String | | |
| 4 | Issuing name | issuing_name | String | | |
| 5 | Amount | amount | `Number` | > 0 | |
| 6 | Currency | currency_code | String | | |
| 7 | Beneficiary name | beneficiary_name | String | | |
| 8 | Beneficiary contact number | contact | String | | |
| 9 | Purpose | purpose | String | | |
| 10 | Debit method | debit_method | String | | |
| 11 | Debit account | debit_account | String | | |
| 12 | Description | description | String | | |
| 13 | Stock prefix | stock_prefix | String | | trường ẩn |
| 14 | Leaf no | leaf_no | `Number` | 0 | trường ẩn |
| 15 | Branch id | branch_id | `Number` | branch id của user login | trường ẩn |
| 16 | Transaction status | status | String |  |  |
| 17 | Transaction date | transaction_date | `Date time` |  |  |
| 18 | User id | user_id | `Number` |  |  |
| 19 | Beneficiary ID number | beneficiary_id_number | String |  | `trả thêm` |
| 20 | Beneficiary address | beneficiary_address | String |  | `trả thêm` |
| 21 | Issued date | issued_date | `Date time` |  | `trả thêm` |
| 22 | Expired date | expired_date | `Date time` |  | `trả thêm` |
| 23 | Issuer ID number | issuer_id_number | String |  | `trả thêm` 12/01/2023 |
| 24 | Issuer contact number | issuer_contact_number | String |  | `trả thêm` 12/01/2023 |
| 25 | Fee data | fee_data | JSON Object | | | |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String | 3 |  |
|  | Currency account code | currency_account_code | String | 3 | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |

## 1.2 Transaction flow
**Conditions:**
- With cashier cheque
  - Book status: Assigned to teller
  - Confirm status: Confirmed
  - Status of stock leaves: Normal
  - Stock is used in transaction belongs to teller do this transaction
- Amount > 0 
- Debit by **Cash**:
  - User has right to do transaction related to cash.
- Debit by **Deposit**:
  - Deposit type is Current/ Saving 
  - Status is Normal (KTB chưa xét trường hợp khi người dùng nhập tay deposit account là Fixed deposit và có status là Normal thay vì chọn dữ liệu trong lookup)
  - Deposit account and stock must be same branch
  - Deposit's currency and stock's currency must be the same
  - Available balance >= Amount
- Debit by **Accounting**:
  - GL account is active
  - GL's currency and stock's currency must be the same
  - Balance side is Both/ Debit
  - GL must belongs to stock's branch

**Flow of events:**
- When complete transaction: 
  - Update information of stock:
    - Stock number
    - Issuing name
    - Beneficiary name
    - Contact
    - Purpose
    - Beneficiary Id Number
    - Beneficiary Address 
    - Issued Date
    - Expired Date
    - Issuer ID Number
    - Issuer Contact Number
    - Book status: Normal
    - Stock leaves status: Unpaid
    - Stock amount = Stock balance = Amount (in transaction)
    - Currency = Currency (in transaction)
  - Cash will increase (if debit method is Cash) 
  - Deposit balance will decrease (if debit method is Deposit)

**Database:**

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASH/DEPOSIT/GL | Amount | Currency of cheque |
| 1 | C | CASHIER CHEQUE | Amount | Currency of cheque |

**Voucher:**
- `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CCR | DPT_GET_INFO_STOCK_BY_CASHIER_CHEQUE | [DPT_GET_INFO_STOCK_BY_CASHIER_CHEQUE](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX_G | [DPT_GET_STOCK_PREFIX_G](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_PAYBY_CSH | `SELECT null,null FROM DUAL` | Làm trên JWEB, tương ứng `SACNO`,`TXB_SACNO` |
| 4 | GET_INFO_DESCS | `select '11806: Cashier cheque issued by ' CASE '@PAYBY' WHEN 'CSH' THEN 'cash' WHEN 'DPT' then 'deposit' WHEN 'ACT' THEN 'miscellaneous' ELSE '' end  from dual` | Làm trên JWEB |
| 5 | GET_INFO_DPT_CCCR | DPT_GET_INFO_SACNO | [DPT_GET_INFO_SACNO](Specification/Common/15 Deposit Rulefuncs) |
| 6 | GET_INFO_ACT_SACNO | DPT_GET_INFO_SACNO | [DPT_GET_INFO_SACNO](Specification/Common/15 Deposit Rulefuncs) |
| 7 | LKP_DATA_SACNO | DPT_LKP_DATA_SACNO | [DPT_LKP_DATA_SACNO](Specification/Common/15 Deposit Rulefuncs) |
| 8 | Get thông tin IFC theo `ifc_code` và `currency_code` | IFC_GET_INFO_IFCCD_BY_CURRENCY_AND_TRANSCODE | [IFC_GET_INFO_IFCCD_BY_CURRENCY_AND_TRANSCODE](Specification/Common/21 IFC Rulefuncs) |
| 9 | Get list IFC theo `currency_code` | IFC_LOOKUP_BY_CURRENCY_AND_TRANSCODE | [IFC_LOOKUP_BY_CURRENCY_AND_TRANSCODE](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>