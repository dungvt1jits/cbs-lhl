# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_REC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Stock prefix | stock_prefix | No | String | 5 | | trường ẩn | CSTKPRE |
| 2 | Cheque no | cheque_no | No | String | 20 | | | CFRSER |
| 3 | Account number | account_number | No | String | 25 | | | PDACC |
| 4 | Cheque amount | cheque_amount | No | `Number` |  | >0 | | CTXAMT1 |
| 5 | Total Fee for Cash | total_fee_for_cash | No | `Number` |  | 0 | trường ẩn | TFCSH |
| 6 | Total VAT for Cash | total_vat_for_cash | No | `Number` |  | 0 | trường ẩn | TVATCSH |
| 7 | Fee amount | fee_amount | No | `Number` | | 0 | trường ẩn | CFAMT |
| 8 | VAT amount | vat_amount | No | `Number` | | 0 | trường ẩn | CVAT |
| 9 | Purpose of hold amount | purpose_of_hold_amount | No | String | 35 | | | CTXTEMK |
| 10 | Amount currency | amount_currency | No | String | 3 | | trường ẩn | PCSCCR |
| 11 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |
| 12 | Description | description | No | String | 250 | | | DESCS |
| 13 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String | | | |
| 3 | Stock prefix | stock_prefix | String |  | trường ẩn |
| 4 | Cheque no | cheque_no | String |  | |
| 5 | Account number | account_number | String | | |
| 6 | Cheque amount | cheque_amount | `Number` |  | |
| 7 | Total Fee for Cash | total_fee_for_cash | `Number` |  | trường ẩn |
| 8 | Total VAT for Cash | total_vat_for_cash | `Number` |  | trường ẩn |
| 9 | Fee amount | fee_amount | `Number` | | trường ẩn |
| 10 | VAT amount | vat_amount | `Number` | | trường ẩn |
| 11 | Purpose of hold amount | purpose_of_hold_amount | String |  | |
| 12 | Description | amount_currency | String |  | |
| 13 | Amount currency | description | String | | trường ẩn |
| 14 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  | số có 5 số thập phân |
|  | Fee | ifc_amount | `Number` |  | số có hai số thập phân |
|  | Floor | floor_value | `Number` |  | số có hai số thập phân |
|  | Ceiling | ceiling_value | `Number` |  | số có hai số thập phân |
|  | Currency | currency_fee_code | String |  |  |
|  | Currency account code | currency_account_code | String |  | trường ẩn |
|  | Payable rate | payrate | `Number` |  | trường ẩn |
|  | Payable source | pay_source | String |  | trường ẩn |
|  | Round amount | round_amount | `Number` |  | trường ẩn |
|  | Round rate | round_rate | `Number` |  | trường ẩn |
|  | Share amount | share_amount | `Number` |  | trường ẩn |
|  | Share fee apply | share_fee | `Number` |  | trường ẩn |
|  | Share rate | share_rate | `Number` |  | trường ẩn |
| 19 | Posting data | | JSON Object | | |
| | Group | | `Number` | | |
| | Index in group | | `Number` | | |
| | Posting side | | String | | |
| | System account name | | String | | |
| | GL account number | | String | | |
| | Amount | | `Number` | | số có hai số thập phân |
| | Currency | | String |  | |
| 20 | Transaction status | status | String |  |  |
| 21 | Transaction date | transaction_date | `Date time` |  |  |
| 22 | User id | user_id | `Number` |  | 
| 23 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
-	Cheque no: exists with status “Earmark”. 
- Deposit account exists with status is "Normal"/"Dormant"
-	Amount was hold.
-	Release Amount = Holding amount.
- If user wants to collect fee:
  - User has right to do transaction related to cash.

**Flow of events:**
-	Don't allow release amount larger than hold amount.
-	Allow collection fees for this transaction (if any).
-	Transaction complete:
    +	Available amount increase.
    +	Stock leaves status: Unpaid.

**Database:**
- StockInventory
- DepositAccount
- DepositEarmark
- DepositTransaction
- DepositStatement

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| Case collect fee (fee amount > 0) | | | | |
| 1 | D | CASH | Fee amount | Currency of deposit account |
| 1 | C | IFCC (income) | Fee amount | Currency of deposit account |

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | Teller/ Branch {} is invalid | Teller/ Branch không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PRIX | DPT_GET_STOCK_PREFIX_C | [DPT_GET_STOCK_PREFIX_C](Specification/Common/15 Deposit Rulefuncs) |
| 2 | GET_INFO_ACNO | DPT_GET_INFO_EMK_BY_SERIAL | [DPT_GET_INFO_EMK_BY_SERIAL](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 5 | GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |
| 6 | LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO | [IFC_LOOKUP_IFCTYPE_CO](Specification/Common/21 IFC Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>