# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountCommon​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCOM_SER_SIMPLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    "search_text": "",
    "page_size": 5,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | References account number | ref_account_number | String |  |  |
| 4 | References account number 2 | ref_account_number2 | String |  |  |
| 5 | Common account id | id | `Number` |  |  |

**Example:**
```json
{
    "total_count": 13,
    "total_pages": 3,
    "has_previous_page": false,
    "has_next_page": true,
    "items": [
        {
            "id": 1,
            "account_number": "----**11511?110000000",
            "account_name": "ATMCSS",
            "ref_account_number": "",
            "ref_account_number2": ""
        },
        {
            "id": 2,
            "account_number": "----**11110?100000000",
            "account_name": "CASH",
            "ref_account_number": "",
            "ref_account_number2": ""
        },
        {
            "id": 3,
            "account_number": "----**32115?000000000",
            "account_name": "CASHIERCHEQUE",
            "ref_account_number": "",
            "ref_account_number2": ""
        },
        {
            "id": 4,
            "account_number": "----**11220?000000000",
            "account_name": "DPTCLR",
            "ref_account_number": "",
            "ref_account_number2": ""
        },
        {
            "id": 5,
            "account_number": "----0257441?000000000",
            "account_name": "FXGAIN",
            "ref_account_number": "",
            "ref_account_number2": ""
        }
    ],
    "page_index": 0,
    "page_size": 5
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountCommon​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCOM_SER_ADVANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account name | account_name | No | String |  |  |  |
| 2 | Account number | account_number | No | String |  |  |  |
| 3 | References account number | ref_account_number | No | String |  |  |  |
| 4 | References account number 2 | ref_account_number2 | No | String |  |  |  |
| 5 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 6 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    "account_number": "",
    "account_name": "",
    "ref_account_number": "",
    "ref_account_number2": "",
    "page_size": 5,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | References account number | ref_account_number | String |  |  |
| 4 | References account number 2 | ref_account_number2 | String |  |  |
| 5 | Common account id | id | `Number` |  |  |

**Example:**
```json
{
    "total_count": 13,
    "total_pages": 3,
    "has_previous_page": false,
    "has_next_page": true,
    "items": [
        {
            "id": 1,
            "account_number": "----**11511?110000000",
            "account_name": "ATMCSS",
            "ref_account_number": "",
            "ref_account_number2": ""
        },
        {
            "id": 2,
            "account_number": "----**11110?100000000",
            "account_name": "CASH",
            "ref_account_number": "",
            "ref_account_number2": ""
        },
        {
            "id": 3,
            "account_number": "----**32115?000000000",
            "account_name": "CASHIERCHEQUE",
            "ref_account_number": "",
            "ref_account_number2": ""
        },
        {
            "id": 4,
            "account_number": "----**11220?000000000",
            "account_name": "DPTCLR",
            "ref_account_number": "",
            "ref_account_number2": ""
        },
        {
            "id": 5,
            "account_number": "----0257441?000000000",
            "account_name": "FXGAIN",
            "ref_account_number": "",
            "ref_account_number2": ""
        }
    ],
    "page_index": 0,
    "page_size": 5
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountCommon​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCOM_INS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |
| 2 | Account name | account_name | `Yes` | String |  |  |  |
| 3 | References account number | ref_account_number | No | String |  |  |  |
| 4 | References account number 2 | ref_account_number2 | No | String |  |  |  |

**Example:**
```json
{
    "account_number": "----**32115?000000045",
    "account_name": "NAME",
    "ref_account_number": "",
    "ref_account_number2": ""
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | References account number | ref_account_number | String |  |  |
| 4 | References account number 2 | ref_account_number2 | String |  |  |
| 5 | Common account id | id | `Number` |  |  |

**Example:**
```json
{
    "account_number": "----**32115?000000045",
    "account_name": "NAME",
    "ref_account_number": "",
    "ref_account_number2": "",
    "id": 17
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountCommon​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCOM_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Common account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 17
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | References account number | ref_account_number | String |  |  |
| 4 | References account number 2 | ref_account_number2 | String |  |  |
| 5 | Common account id | id | `Number` |  |  |

**Example:**
```json
{
    "account_number": "----**32115?000000045",
    "account_name": "NAME",
    "ref_account_number": "",
    "ref_account_number2": "",
    "id": 17
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountCommon​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCOM_UPD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number |  | No | String |  |  |  |
| 2 | Account name |  | `Yes` | String |  |  |  |
| 3 | References account number |  | No | String |  |  |  |
| 4 | References account number 2 |  | No | String |  |  |  |
| 5 | Common account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 17,
    "account_number": "----**32115?000000055",
    "account_name": "NAME_U",
    "ref_account_number": "",
    "ref_account_number2": ""
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | References account number | ref_account_number | String |  |  |
| 4 | References account number 2 | ref_account_number2 | String |  |  |
| 5 | Common account id | id | `Number` |  |  |

**Example:**
```json
{
    "account_number": "----**32115?000000055",
    "account_name": "NAME_U",
    "ref_account_number": "",
    "ref_account_number2": "",
    "id": 17
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountCommon​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCOM_DEL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Common account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 17
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Common account id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 17
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |