# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/ExtensionAccountOfGroupDefinition​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGLDEF_SER_SIMPLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Extension account id | id | `Number` |  |  |
| 2 | Accounting group | accounting_group_id | `Number` |  |  |
| 3 | Group name | group_name | String |  | lấy từ `03-Group-Definition` |
| 4 | Replace by | replace_by | String |  |  |
| 5 | Account name | account_name | String |  |  |
| 6 | Description | desc | String |  |  |
| 7 | Customer sector | sector | String |  |  |
| 8 | Customer resident status | resident_status | String |  |  |
| 9 | Customer categories | categories | String |  |  |
| 10 | Account resident status | account_resident | String |  |  |

**Example:**
```json

```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/ExtensionAccountOfGroupDefinition​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGLDEF_SER_ADVANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Extension account id | id | No | `Number` |  |  |  |
| 2 | Accounting group | accounting_group_id | No | `Number` |  |  |  |
| 3 | Group name | group_name | No | String |  |  |  |
| 4 | Replace by | replace_by | No | String |  |  |  |
| 5 | Account name | account_name | No | String |  |  |  |
| 6 | Description | desc | No | String |  |  |  |
| 7 | Customer sector | sector | No | String |  |  |  |
| 8 | Customer resident status |resident_status  | No | String |  |  |  |
| 9 | Customer categories | categories | No | String |  |  |  |
| 10 | Account resident status | account_resident | No | String |  |  |  |
| 11 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 12 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Extension account id | id | `Number` |  |  |
| 2 | Accounting group | accounting_group_id | `Number` |  |  |
| 3 | Group name | group_name | String |  | lấy từ `03-Group-Definition` |
| 4 | Replace by | replace_by | String |  |  |
| 5 | Account name | account_name | String |  |  |
| 6 | Description | desc | String |  |  |
| 7 | Customer sector | sector | String |  |  |
| 8 | Customer resident status | resident_status | String |  |  |
| 9 | Customer categories | categories | String |  |  |
| 10 | Account resident status | account_resident | String |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/ExtensionAccountOfGroupDefinition​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGLDEF_INS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Accounting group | accounting_group_id | `Yes` | `Number` |  |  |  |
| 2 | Replace code | replace_by_code | `Yes` | String |  |  |  |
| 3 | Replace by | replace_by | `Yes` | String |  |  |  |
| 4 | System account name | account_name | `Yes` | String |  |  |  |
| 5 | Condition account of group | condition | No | JSON Object |  |  |  |
|  | Customer sector | sector | No | String |  |  |  |
|  | Customer resident status | resident_status | No | String |  |  |  |
|  | Customer categories | categories | No | String |  |  |  |
|  | Account resident status | account_resident | No | String |  |  |  |
|  | Sub product | subproduct | No | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Extension account id | id | `Number` |  |  |
| 2 | Accounting group | accounting_group_id | `Number` |  |  |
| 3 | Replace code | replace_by_code | String |  |  |
| 4 | Replace by | replace_by | String |  |  |
| 5 | System account name | account_name | String |  |  |
| 6 | Condition account of group | condition | JSON Object |  |  |
|  | Customer sector | sector | String |  |  |
|  | Customer resident status | resident_status | String |  |  |
|  | Customer categories | categories | String |  |  |
|  | Account resident status | account_resident | String |  |  |
|  | Sub product | subproduct | String |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/ExtensionAccountOfGroupDefinition​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGLDEF_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Extension account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 1795
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Extension account id | id | `Number` |  |  |
| 2 | Accounting group | accounting_group_id | `Number` |  |  |
| 3 | Accounting group module | accounting_group_module | String |  | `trả thêm`, lấy giá trị `module` của `group_id` trong "Group Definition" tương ứng `accounting_group_id` |
| 4 | Group name | group_name | String |  | `trả thêm`, lấy giá trị `account_group_def` của `group_id` trong "Group Definition" tương ứng `accounting_group_id` |
| 5 | Replace code | replace_by_code | String |  |  |
| 6 | Replace by | replace_by | String |  |  |
| 7 | System account name | account_name | String |  |  |
| 8 | Bank define account name | bank_define_account_name | String |  | `trả thêm`, lấy giá trị `bank_define_account_name` của `system_account_name` trong "Module Account List" tương ứng `account_name` |
| 9 | Condition account of group | condition | JSON Object |  |  |
|  | Customer sector | sector | String |  |  |
|  | Customer resident status | resident_status | String |  |  |
|  | Customer categories | categories | String |  |  |
|  | Account resident status | account_resident | String |  |  |
|  | Sub product | subproduct | String |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/ExtensionAccountOfGroupDefinition​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGLDEF_UPD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Extension account id | id | `Yes` | `Number` |  |  |  |
| 2 | Replace code | replace_by_code | `Yes` | String |  |  |  |
| 3 | Replace by | replace_by | `Yes` | String |  |  |  |
| 4 | System account name | account_name | `Yes` | String |  |  |  |
| 5 | Condition account of group | condition | No | JSON Object |  |  |  |
|  | Customer sector | sector | No | String |  |  |  |
|  | Customer resident status | resident_status | No | String |  |  |  |
|  | Customer categories | categories | No | String |  |  |  |
|  | Account resident status | account_resident | No | String |  |  |  |
|  | Sub product | subproduct | No | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Extension account id | id | `Number` |  |  |
| 2 | Accounting group | accounting_group_id | `Number` |  | không được sửa |
| 3 | Replace code | replace_by_code | String |  |  |
| 4 | Replace by | replace_by | String |  |  |
| 5 | System account name | account_name | String |  |  |
| 6 | Condition account of group | condition | JSON Object |  |  |
|  | Customer sector | sector | String |  |  |
|  | Customer resident status | resident_status | String |  |  |
|  | Customer categories | categories | String |  |  |
|  | Account resident status | account_resident | String |  |  |
|  | Sub product | subproduct | String |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/ExtensionAccountOfGroupDefinition​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGLDEF_DEL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Extension account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 1795
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Extension account id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 1795
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |