# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinition​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEF_SER_SIMPLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    "search_text": "",
    "page_size": 5,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Accounting group | group_id | `Number` |  |  |
| 2 | Module name | module | String |  |  |
| 3 | Group name | account_group_def | String |  |  |
| 4 | Group id | id | `Number` |  |  |

**Example:**
```json
{
    "total_count": 78,
    "total_pages": 16,
    "has_previous_page": false,
    "has_next_page": true,
    "items": [
        {
            "id": 2,
            "group_id": 2,
            "module": "DPT",
            "account_group_def": "Current account in THB"
        },
        {
            "id": 3,
            "group_id": 3,
            "module": "DPT",
            "account_group_def": "Current account in USD"
        },
        {
            "id": 4,
            "group_id": 4,
            "module": "DPT",
            "account_group_def": "Saving account in KHR"
        },
        {
            "id": 5,
            "group_id": 5,
            "module": "DPT",
            "account_group_def": "Saving account in THB"
        },
        {
            "id": 6,
            "group_id": 6,
            "module": "DPT",
            "account_group_def": "Saving account in USD"
        }
    ],
    "page_index": 0,
    "page_size": 5
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinition​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEF_SER_ADVANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Accounting group | group_id | No | `Number` |  |  |  |
| 2 | Module name | module | No | String |  |  |  |
| 3 | Group name | account_group_def | No | String |  |  |  |
| 4 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 5 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    "group_id": null,
    "module": "",
    "account_group_def": "",
    "page_index": 0,
    "page_size": 5
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Accounting group | group_id | `Number` |  |  |
| 2 | Module name | module | String |  |  |
| 3 | Group name | account_group_def | String |  |  |
| 4 | Group id | id | `Number` |  |  |

**Example:**
```json
{
    "total_count": 78,
    "total_pages": 16,
    "has_previous_page": false,
    "has_next_page": true,
    "items": [
        {
            "id": 2,
            "group_id": 2,
            "module": "DPT",
            "account_group_def": "Current account in THB"
        },
        {
            "id": 3,
            "group_id": 3,
            "module": "DPT",
            "account_group_def": "Current account in USD"
        },
        {
            "id": 4,
            "group_id": 4,
            "module": "DPT",
            "account_group_def": "Saving account in KHR"
        },
        {
            "id": 5,
            "group_id": 5,
            "module": "DPT",
            "account_group_def": "Saving account in THB"
        },
        {
            "id": 6,
            "group_id": 6,
            "module": "DPT",
            "account_group_def": "Saving account in USD"
        }
    ],
    "page_index": 0,
    "page_size": 5
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinition​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEF_INS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Accounting group | group_id | `Yes` | `Number` |  |  |  |
| 2 | Module | module | `Yes` | String |  |  |  |
| 3 | Group name | account_group_def | `Yes` | String |  |  |  |

**Example:**
```json
{
    "group_id": 1220,
    "module": "ACT",
    "account_group_def": "saving account test"
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group id | id | `Number` |  |  |  |
| 2 | Accounting group | group_id | `Number` |  |  |  |
| 3 | Module | module | String |  |  |  |
| 4 | Group name | account_group_def | String |  |  |  |

**Example:**
```json
{
    "group_id": 1220,
    "module": "ACT",
    "account_group_def": "saving account test",
    "id": 81
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinition​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEF_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 81
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group id | id | `Number` |  |  |  |
| 2 | Accounting group | group_id | `Number` |  |  |  |
| 3 | Module | module | String |  |  |  |
| 4 | Group name | account_group_def | String |  |  |  |

**Example:**
```json
{
    "group_id": 1220,
    "module": "ACT",
    "account_group_def": "saving account test",
    "id": 81
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinition​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEF_UPD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | id | `Yes` | `Number` |  |  |  |
| 1 | Module | module | `Yes` | String |  |  |  |
| 2 | Group name | account_group_def | `Yes` | String |  |  |  |

**Example:**
```json
{
    "id": 81,
    "module": "ACT",
    "account_group_def": "up saving account test"
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group id | id | `Number` |  |  |  |
| 2 | Accounting group | group_id | `Number` |  |  |  |
| 3 | Module | module | String |  |  |  |
| 4 | Group name | account_group_def | String |  |  |  |

**Example:**
```json
{
    "group_id": 1220,
    "module": "ACT",
    "account_group_def": "up saving account test",
    "id": 81
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinition​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEF_DEL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 81
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group id | id | `Number` |  |  |  |

**Example:**
```json
{
    "id": 81
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |