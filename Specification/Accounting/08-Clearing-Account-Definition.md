# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountClearing​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCLR_SER_SIMPLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Branch name | branch_name | String |  | lấy từ bảng branch |
| 2 | Currency code | currency_id | String | 3 |  |
| 3 | Clear branch name | clear_branch_name | String |  | lấy từ bảng branch |
| 4 | Clear type | clearing_type | String |  |  |
| 5 | Account number | account_number | String |  |  |
| 6 | Clearing account id | id | `Number` |  |  |
| 7 | Bank account number | bank_account_number | String |  | `trả thêm`, lấy giá trị `bank_account_number` của `account_number` trong "Bank Account Definition" tương ứng `account_number` |

**Example:**
```json
{

}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountClearing​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCLR_SER_ADVANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch name | branch_name | No | String |  |  |  |
| 2 | Currency code | currency_id | No | String | 3 |  |  |
| 3 | Clear branch name | clear_branch_name | No | String |  |  |  |
| 4 | Clear type | clearing_type | No | String |  |  |  |
| 5 | Bank account number | account_number | No | String |  |  |  |
| 6 | Page index | page_size | No | `Number` |  |  | số nguyên dương |
| 7 | Page size | page_index | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Branch name | branch_name | String |  | lấy từ bảng branch |
| 2 | Currency code | currency_code | String | 3 |  |
| 3 | Clear branch name | clear_branch_name | String |  | lấy từ bảng branch |
| 4 | Clear type | clearing_type | String |  |  |
| 5 | Account number | account_number | String |  |  |
| 6 | Clearing account id | id | `Number` |  |  |
| 7 | Bank account number | bank_account_number | String |  | `trả thêm`, lấy giá trị `bank_account_number` của `account_number` trong "Bank Account Definition" tương ứng `account_number` |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountClearing​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCLR_INS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch code | branch_code | `Yes` | String |  |  |  |
| 2 | Currency code | currency_id | `Yes` | String |  |  |  |
| 3 | Clearing branch code | clearing_branch_code | `Yes` | String |  |  |  |
| 4 | Clearing type | clearing_type | `Yes` | String |  |  |  |
| 5 | Bank account number | account_number | `Yes` | String |  |  | hiển thị `bank_account_number`, gửi đi `account_number` |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Clearing account id | id | `Number` |  |  |
| 2 | Branch code | branch_code | String |  |  |
| 3 | Branch name | branch_name | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Clearing branch code | clearing_branch_code | String |  |  |
| 6 | Clearing branch name | clearing_branch_name | String |  |  |
| 7 | Clearing type | clearing_type | String |  |  |
| 8 | Bank account number | account_number | String |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountClearing​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCLR_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Clearing account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 21
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Clearing account id | id | `Number` |  |  |
| 2 | Branch code | branch_code | String |  |  |
| 3 | Branch name | branch_name | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Clearing branch code | clearing_branch_code | String |  |  |
| 6 | Clearing branch name | clearing_branch_name | String |  |  |
| 7 | Clearing type | clearing_type | String |  |  |
| 8 | Bank account number | account_number | String |  |  |
| 9 | Account name | account_name | String |  | `trả thêm`, lấy giá trị `account_name` của `account_number` trong "Bank Account Definition" tương ứng `account_number` |
| 10 | Bank account number | bank_account_number | String |  | `trả thêm`, lấy giá trị `bank_account_number` của `account_number` trong "Bank Account Definition" tương ứng `account_number` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does `Yes`t exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountClearing​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCLR_UPD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch code | branch_code | `Yes` | String |  |  |  |
| 2 | Currency code | currency_id | `Yes` | String |  |  |  |
| 3 | Clearing branch code | clearing_branch_code | `Yes` | String |  |  |  |
| 4 | Clearing type | clearing_type | `Yes` | String |  |  |  |
| 5 | Bank account number | account_number | `Yes` | String |  |  | hiển thị `bank_account_number`, gửi đi `account_number` |
| 6 | Clearing account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Clearing account id | id | `Number` |  |  |
| 2 | Branch code | branch_code | String |  |  |
| 3 | Branch name | branch_name | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Clearing branch code | clearing_branch_code | String |  |  |
| 6 | Clearing branch name | clearing_branch_name | String |  |  |
| 7 | Clearing type | clearing_type | String |  |  |
| 8 | Bank account number | account_number | String |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does `Yes`t exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountClearing​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCLR_DEL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Clearing account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 21
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Clearing account id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 21
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |