# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `ACT_FEE`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Fee currency | fee_currency | `Yes` | String | 3 |  |  | CCCR |
| 2 | Customer name | customer_name | `Yes` | String | 250 |  |  | ACNM |
| 3 | Total fee amount | total_fee_amount | `Yes` | `Number` |  | > 0 | số có hai số thập phân | TXAMT |
| 4 | Amount for fee calculation | amount_for_fee_calculation | No | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 5 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 6 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Fee currency | fee_currency | String | 3 |  |
| 3 | Customer name | customer_name | String |  |  |
| 4 | Total fee amount | total_fee_amount | `Number` |  | số có hai số thập phân |
| 5 | Amount for fee calculation | amount_for_fee_calculation | `Number` |  | số có hai số thập phân |
| 6 | Description | description | String |  |  |
| 7 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  |  |
|  | Fee | ifc_amount | `Number` |  |  |
|  | Floor | floor_value | `Number` |  |  |
|  | Ceiling | ceiling_value | `Number` |  |  |
|  | Currency | currency_fee_code | String |  |  |
|  | Currency account code | currency_account_code | String |  |  |
|  | Payable rate | payrate | `Number` |  |  |
|  | Payable source | pay_source | String |  |  |
|  | Round amount | round_amount | `Number` |  |  |
|  | Round rate | round_rate | `Number` |  |  |
|  | Share amount | share_amount | `Number` |  |  |
|  | Share fee apply | share_fee | `Number` |  |  |
|  | Share rate | share_rate | `Number` |  |  |
| 8 | Transaction status | status | String |  |  |
| 9 | Transaction date | transaction_date | `Date time` |  |  |
| 10 | User id | user_id | `Number` |  |  |
|  | Step code: `ACT_EXECUTE_POSTING` |  |  |  |  |
| 11 | Posting data | entry_journals | JSON Object |  |  |
|  | Group | accounting_entry_group | `Number` |  | số nguyên dương |
|  | Index in group | accounting_entry_index | `Number` |  | số nguyên dương |
|  | Posting side | debit_or_credit | String |  | D or C |
|  | Account number | bank_account_number | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Currency | currency_code | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Total fee amount > 0.00

**Flow of events:**
- Have two types fee as: flat amount and percentage.
    - Case fee is flat amount, allow user enter fee amount.
    - Case fee is percentage, allow user enter amount and percentage. 
        - System calculation fee amount = Amount * percentage.
- Transaction complete: Cash at counter will increase.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASH | Total fee amount | Currency of fee |
| 1 | C | IFCC (income) | Total fee amount | Currency of fee |

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | TRAN_FEE_ACT_FEE/GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |  |  |
| 2 | TRAN_FEE/GET_INFO_IFCCD | TRAN_FEE_GET_INFO_IFCCD | [TRAN_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) |  |  |
| 3 | TRAN_FEE_ACT_FEE/LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_C | [IFC_LOOKUP_IFCTYPE_C](Specification/Common/21 IFC Rulefuncs) | `currency_code` = `fee_currency` |  |