# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountChart​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCHRT_SER_SIMPLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_count | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account level | account_level | `Number` | 2 |  |
| 2 | Account number | bank_account_number | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Classification | account_classification | String |  |  |
| 6 | Balance side | balance_side | String |  |  |
| 7 | Group | account_group | String |  |  |
| 8 | Account id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountChart​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCHRT_SER_ADVANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account level from | account_level_from | No | `Number` | 2 |  |  |
| 2 | Account level to | account_level_to | No | `Number` | 2 |  |  |
| 3 | Account number | bank_account_number | No | String |  |  |  |
| 4 | Currency | currency_code | No | String | 3 |  |  |
| 5 | Account name | account_name | No | String |  |  |  |
| 6 | Classification | account_classification | No | String |  |  |  |
| 7 | Balance side | balance_side | No | String |  |  |  |
| 8 | Group | account_group | No | String |  |  |  |
| 9 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 10 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account level | account_level | `Number` | 2 |  |
| 2 | Account number | bank_account_number | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Classification | account_classification | String |  |  |
| 6 | Balance side | balance_side | String |  |  |
| 7 | Group | account_group | String |  |  |
| 8 | Account id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountChart​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCHRT_INS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account level | account_level | `Yes` | `Number` | 2 |  |  |
| 2 | Currency | currency_code | No | String |  |  |  |
| 3 | Bank account number | bank_account_number | `Yes` | String |  |  |  |
| 4 | Account name | account_name | `Yes` | String |  |  |  |
| 5 | Short account name | short_account_name | `Yes` | String |  |  |  |
| 6 | Other name | multi_value_name | No | JSON Object |  |  |  |
|  | Laos name | laos_name | No | String |  |  |  |
|  | Thai name | thai_name | No | String |  |  |  |
|  | Khmer name | khmer_name | No | String |  |  |  |
|  | Vietnamese name | vietnamese_name | No | String |  |  |  |
| 7 | Account classification | account_classification | `Yes` | String |  |  |  |
| 8 | Reverse balance | reverse_balance | `Yes` | String |  |  |  |
| 9 | Balance side | balance_side | `Yes` | String |  |  |  |
| 10 | Posting side | posting_side | `Yes` | String |  |  |  |
| 11 | Account group | account_group | `Yes` | String |  |  |  |
| 12 | Account categories | account_categories | `Yes` | String |  |  |  |
| 13 | Direct posting | direct_posting | `Yes` | String |  |  |  |
| 14 | Is Inactive | is_visible | `Yes` | String |  |  |  |
| 15 | Job process option | job_process_option | `Yes` | String |  |  |  |
| 16 | Reference accno 1 | ref_account_number | No | String |  |  |  |
| 17 | Reference accno 2 | references_number | No | String |  |  |  |
| 18 | Branch code | branch_code | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  |  |
| 2 | Account level | account_level | `Number` | 2 |  |
| 3 | Currency | currency_code | String |  |  |
| 4 | Bank account number | bank_account_number | String |  |  |
| 5 | Account name | account_name | String |  |  |
| 6 | Short account name | short_account_name | String |  |  |
| 7 | Other name | multi_value_name | JSON Object |  |  |
|  | Laos name | laos_name | String |  |  |
|  | Thai name | thai_name | String |  |  |
|  | Khmer name | khmer_name | String |  |  |
|  | Vietnamese name | vietnamese_name | String |  |  |
| 8 | Account classification | account_classification | String |  |  |
| 9 | Reverse balance | reverse_balance | String |  |  |
| 10 | Balance side | balance_side | String |  |  |
| 11 | Posting side | posting_side | String |  |  |
| 12 | Account group | account_group | String |  |  |
| 13 | Account categories | account_categories | String |  |  |
| 14 | Direct posting | direct_posting | String |  |  |
| 15 | Is inactive | is_visible | String |  |  |
| 16 | Job process option | job_process_option | String |  |  |
| 17 | Reference accno 1 | ref_account_number | String |  |  |
| 18 | Reference accno 2 | references_number | String |  |  |
| 19 | Branch code | branch_code | String |  |  |
| 20 | Account id | id | `Number` |  |  |
|  | Parent account id | parent_account_id | String |  |  |
|  | Is account leave | is_account_leave | `Boolean` |  |  |
|  | Is multi currency | is_multi_currency | String |  |  |
|  | Is cash account | is_cash_account | `Boolean` |  |  |
|  | Is master account | is_master_account | `Boolean` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow
- Reference accno 1, Reference accno 2 hiển thị theo Job process option
- Balance, Account group hiển thị theo Account classification

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountChart​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCHRT_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 33075
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  |  |
| 2 | Account level | account_level | `Number` | 2 |  |
| 3 | Currency | currency_code | String |  |  |
| 4 | Bank account number | bank_account_number | String |  |  |
| 5 | Account name | account_name | String |  |  |
| 6 | Short account name | short_account_name | String |  |  |
| 7 | Other name | multi_value_name | JSON Object |  |  |
|  | Laos name | laos_name | String |  |  |
|  | Thai name | thai_name | String |  |  |
|  | Khmer name | khmer_name | String |  |  |
|  | Vietnamese name | vietnamese_name | String |  |  |
| 8 | Account classification | account_classification | String |  |  |
| 9 | Reverse balance | reverse_balance | String |  |  |
| 10 | Balance side | balance_side | String |  |  |
| 11 | Posting side | posting_side | String |  |  |
| 12 | Account group | account_group | String |  |  |
| 13 | Account categories | account_categories | String |  |  |
| 14 | Direct posting | direct_posting | String |  |  |
| 15 | Is inactive | is_visible | String |  |  |
| 16 | Job process option | job_process_option | String |  |  |
| 17 | Reference accno 1 | ref_account_number | String |  |  |
| 18 | Reference accno 2 | references_number | String |  |  |
| 19 | Branch code | branch_code | String |  |  |
| 20 | Account id | id | `Number` |  |  |
|  | Parent account id | parent_account_id | String |  |  |
|  | Is account leave | is_account_leave | `Boolean` |  |  |
|  | Is multi currency | is_multi_currency | String |  |  |
|  | Is cash account | is_cash_account | `Boolean` |  |  |
|  | Is master account | is_master_account | `Boolean` |  |  |

**Example:**
```json
{

}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountChart​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCHRT_UPD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account name | account_name | `Yes` | String |  |  |  |
| 2 | Short account name | short_account_name | `Yes` | String |  |  |  |
| 3 | Other name | multi_value_name | No | JSON Object |  |  |  |
|  | Laos name | laos_name | No | String |  |  |  |
|  | Thai name | thai_name | No | String |  |  |  |
|  | Khmer name | khmer_name | No | String |  |  |  |
|  | Vietnamese name | vietnamese_name | No | String |  |  |  |
| 4 | Account classification | account_classification | `Yes` | String |  |  |  |
| 5 | Reverse balance | reverse_balance | `Yes` | String |  |  |  |
| 6 | Balance side | balance_side | `Yes` | String |  |  |  |
| 7 | Posting side | posting_side | `Yes` | String |  |  |  |
| 8 | Account group | account_group | `Yes` | String |  |  |  |
| 9 | Account categories | account_categories | `Yes` | String |  |  |  |
| 10 | Direct posting | direct_posting | `Yes` | String |  |  |  |
| 11 | Is inactive | is_visible | `Yes` | String |  |  |  |
| 12 | Job process option | job_process_option | `Yes` | String |  |  |  |
| 13 | Reference accno 1 | ref_account_number | No | String |  |  |  |
| 14 | Reference accno 2 | references_number | No | String |  |  |  |
| 15 | Account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  |  |
| 2 | Account level | account_level | `Number` | 2 |  |
| 3 | Currency | currency_code | String |  |  |
| 4 | Bank account number | bank_account_number | String |  |  |
| 5 | Account name | account_name | String |  |  |
| 6 | Short account name | short_account_name | String |  |  |
| 7 | Other name | multi_value_name | JSON Object |  |  |
|  | Laos name | laos_name | String |  |  |
|  | Thai name | thai_name | String |  |  |
|  | Khmer name | khmer_name | String |  |  |
|  | Vietnamese name | vietnamese_name | String |  |  |
| 8 | Account classification | account_classification | String |  |  |
| 9 | Reverse balance | reverse_balance | String |  |  |
| 10 | Balance side | balance_side | String |  |  |
| 11 | Posting side | posting_side | String |  |  |
| 12 | Account group | account_group | String |  |  |
| 13 | Account categories | account_categories | String |  |  |
| 14 | Direct posting | direct_posting | String |  |  |
| 15 | Is inactive | is_visible | String |  |  |
| 16 | Job process option | job_process_option | String |  |  |
| 17 | Reference accno 1 | ref_account_number | String |  |  |
| 18 | Reference accno 2 | references_number | String |  |  |
| 19 | Branch code | branch_code | String |  |  |
| 20 | Account id | id | `Number` |  |  |
|  | Parent account id | parent_account_id | String |  |  |
|  | Is account leave | is_account_leave | `Boolean` |  |  |
|  | Is multi currency | is_multi_currency | String |  |  |
|  | Is cash account | is_cash_account | `Boolean` |  |  |
|  | Is master account | is_master_account | `Boolean` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow
- Reference accno 1, Reference accno 2 hiển thị theo Job process option
- Balance, Account group hiển thị theo Account classification

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountChart​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCHRT_DEL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 33075
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 33075
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get currency from account number
## 7.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_BANK_ACCOUNT_DEFINITION_GET_INFO_CCRCD`

**Query:** SELECT `CurrencyId` FROM `Currency` WHERE `ShortCurrencyId` = substr('099902647102000000000',5,2)

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency id | currency_id | String |  |  |

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 8. Get branch id from account number
## 8.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_BANK_ACCOUNT_DEFINITION_GET_INFO_BRANCHID`

**Query:** SELECT id FROM `Branch` WHERE `BranchCode` = SUBSTR('099902647102000000000',1,4);

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch id | branch_id | `Number` |  | Số nguyên |

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 9. Get branch code (4 digits) from account number
## 9.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_BANK_ACCOUNT_DEFINITION_GET_INFO_BRANCHCODE`

**Query:** SELECT `BranchCode` FROM `Branch` WHERE `BranchCode` = SUBSTR('099902647102000000000',1,4);

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch code | branch_code | String |  |  |

## 9.2 Transaction flow

## 9.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 10. Get information from bank account number
## 10.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCHRT_GET_INFO_ACNO`

**Query:** SELECT `AccountName`, `AccountNumber`, `BranchCode`, `CurrencyCode` FROM `AccountChart` WHERE `BankAccountNumber`='084801296511010000000';

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Bank account number | bank_account_number | No | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Branch code | branch_code | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Bank account number | bank_account_number | String |  |  |
| 6 | Id | id | `Number` |  |  |

# 11. Get information from bank account number and branch code
## 11.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACCHRT_GET_INFO_BACNO`

**Query in O9:** SELECT BACNO,ACNAME,CCRCD FROM D_ACCHRT WHERE  ISDIRECT ='Y' AND ISLEAVE = 'Y' AND BRANCHID = '@CRRBRID' AND BACNO='@PGOACC'.

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Bank account number | bank_account_number | No | String |  |  |  |
| 2 | Branch code | branch_code | No | String |  |  |  |

**Response message:**
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Branch code | branch_code | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Bank account number | bank_account_number | String |  |  |
