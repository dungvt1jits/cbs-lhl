# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinitionDetail​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEFDTL_SER_SIMPLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    "search_text": "",
    "page_size": 5,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Module name | module | String |  |  |
| 2 | System account name | system_account_name | String |  |  |
| 3 | Description | bank_define_account_name | String |  |  |
| 4 | Module id | id | `Number` |  |  |

**Example:**
```json
{
    "total_count": 61,
    "total_pages": 13,
    "has_previous_page": false,
    "has_next_page": true,
    "items": [
        {
            "id": 1,
            "module": "CRD",
            "system_account_name": "COMMITMENT_LIMIT",
            "bank_define_account_name": "Un-utilized limit account"
        },
        {
            "id": 2,
            "module": "CRD",
            "system_account_name": "COMMITMENT_REL",
            "bank_define_account_name": "Utilized limit account"
        },
        {
            "id": 3,
            "module": "CRD",
            "system_account_name": "CREDIT0",
            "bank_define_account_name": "Principal - STD"
        },
        {
            "id": 4,
            "module": "CRD",
            "system_account_name": "CREDIT1",
            "bank_define_account_name": "Principal - SPM"
        },
        {
            "id": 5,
            "module": "CRD",
            "system_account_name": "CREDIT2",
            "bank_define_account_name": "Principal - SubSTD"
        }
    ],
    "page_index": 0,
    "page_size": 5
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinitionDetail​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEFDTL_SER_ADVANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Module name | module | No | String |  |  |  |
| 2 | System account name | system_account_name | No | String |  |  |  |
| 3 | Description | bank_define_account_name | No | String |  |  |  |
| 4 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 5 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    "module": "",
    "system_account_name": "",
    "bank_define_account_name": "",
    "page_size": 5,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Module name | module | String |  |  |
| 2 | System account name | system_account_name | String |  |  |
| 3 | Description | bank_define_account_name | String |  |  |
| 4 | Module id | id | `Number` |  |  |

**Example:**
```json
{
    "total_count": 61,
    "total_pages": 13,
    "has_previous_page": false,
    "has_next_page": true,
    "items": [
        {
            "id": 1,
            "module": "CRD",
            "system_account_name": "COMMITMENT_LIMIT",
            "bank_define_account_name": "Un-utilized limit account"
        },
        {
            "id": 2,
            "module": "CRD",
            "system_account_name": "COMMITMENT_REL",
            "bank_define_account_name": "Utilized limit account"
        },
        {
            "id": 3,
            "module": "CRD",
            "system_account_name": "CREDIT0",
            "bank_define_account_name": "Principal - STD"
        },
        {
            "id": 4,
            "module": "CRD",
            "system_account_name": "CREDIT1",
            "bank_define_account_name": "Principal - SPM"
        },
        {
            "id": 5,
            "module": "CRD",
            "system_account_name": "CREDIT2",
            "bank_define_account_name": "Principal - SubSTD"
        }
    ],
    "page_index": 0,
    "page_size": 5
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinitionDetail​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEFDTL_INS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Module name | module | `Yes` | String |  |  |  |
| 2 | System account name | system_account_name | `Yes` | String |  |  |  |
| 3 | Description | bank_define_account_name | `Yes` | String |  |  |  |

**Example:**
```json
{
    "module": "DPT",
    "system_account_name": "SYSTEM_ACCOUNT_NAME",
    "bank_define_account_name": "account name"
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Module id | id | `Number` |  |  |
| 2 | Module name | module | String |  |  |
| 3 | System account name | system_account_name | String | 25 |  |
| 4 | Description | bank_define_account_name | String |  |  |

**Example:**
```json
{
    "module": "DPT",
    "system_account_name": "SYSTEM_ACCOUNT_NAME",
    "bank_define_account_name": "account name",
    "id": 64
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinitionDetail​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEFDTL_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Module id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 64
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Module id | id | `Number` |  |  |
| 2 | Module name | module | String |  |  |
| 3 | System account name | system_account_name | String | 25 |  |
| 4 | Description | bank_define_account_name | String |  |  |

**Example:**
```json
{
    "module": "DPT",
    "system_account_name": "SYSTEM_ACCOUNT_NAME",
    "bank_define_account_name": "account name",
    "id": 64
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinitionDetail​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEFDTL_UPD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Module id | id | `Yes` | `Number` |  |  |  |
| 2 | Description | bank_define_account_name | `Yes` | String |  |  |  |

**Example:**
```json
{
    "id": 64,
    "bank_define_account_name": "account name update"
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Module id | id | `Number` |  |  |
| 2 | Module name | module | String |  |  |
| 3 | System account name | system_account_name | String | 25 |  |
| 4 | Description | bank_define_account_name | String |  |  |

**Example:**
```json
{
    "module": "DPT",
    "system_account_name": "SYSTEM_ACCOUNT_NAME",
    "bank_define_account_name": "account name update",
    "id": 64
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountGroupDefinitionDetail​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRPDEFDTL_DEL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Module id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 64
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Module id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 64
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |