# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/GroupOfAccount​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRP_SER_SIMPLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    "search_text": "",
    "page_size": 5,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Accounting group | group_id | `Number` |  |  |
| 2 | Group name | account_group_def | String |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Description | desc | String |  |  |
| 6 | Account of group id | id | `Number` |  |  |

**Example:**
```json
[
    {
        "id": 2,
        "group_id": 2,
        "account_group_def": "Current account in THB",
        "account_number": "####**{REF}?000000000",
        "account_name": "DEPOSIT",
        "desc": "Principal - Deposit from"
    },
    {
        "id": 3,
        "group_id": 3,
        "account_group_def": "Current account in USD",
        "account_number": "####**{REF}?000000000",
        "account_name": "DEPOSIT",
        "desc": "Principal - Deposit from"
    },
    {
        "id": 4,
        "group_id": 4,
        "account_group_def": "Saving account in KHR",
        "account_number": "####**{REF}",
        "account_name": "WITHHOLDING_TAX",
        "desc": "Saving and Time Deposits Interest WHT"
    },
    {
        "id": 8,
        "group_id": 4,
        "account_group_def": "Saving account in KHR",
        "account_number": "####**{REF}?000000000",
        "account_name": "REVERT_INTEREST",
        "desc": "Interest Expense Reversal (cross period)"
    },
    {
        "id": 9,
        "group_id": 4,
        "account_group_def": "Saving account in KHR",
        "account_number": "####**{REF}?000000000",
        "account_name": "PAID_WHT",
        "desc": "Interest Payable deducted by Withholding Taxes"
    }
]
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/GroupOfAccount​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRP_SER_ADVANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Accounting group | group_id | No | `Number` |  |  |  |
| 2 | Group name | account_group_def | No | String |  |  |  |
| 3 | Account number | account_number | No | String |  |  |  |
| 4 | Account name | account_name | No | String |  |  |  |
| 5 | Description | desc | No | String |  |  |  |
| 6 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 7 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    "group_id": null,
    "account_group_def": "",
    "account_number": "",
    "account_name": "",
    "desc": "",
    "page_size": 5,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Accounting group | group_id | `Number` |  |  |
| 2 | Group name | account_group_def | String |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | Description | desc | String |  |  |
| 6 | Account of group id | id | `Number` |  |  |

**Example:**
```json
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/GroupOfAccount​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRP_INS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Accounting group | group_id | `Yes` | `Number` |  |  |  |
| 2 | Account number | account_number | No | String |  |  |  |
| 3 | System account name | account_name | `Yes` | String | 50 |  |  |
| 4 | References account number | ref_account_number | No | String |  |  |  |
| 5 | References account number 2 | ref_account_number2 | No | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account of group id | id | `Number` |  |  |
| 2 | Accounting group | group_id |`Number` |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | System account name | account_name |String |  |  |
| 5 | References account number | ref_account_number | String |  |  |
| 6 | References account number 2 | ref_account_number2 | String |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/GroupOfAccount​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRP_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account of group id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 444
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account of group id | id | `Number` |  |  |
| 2 | Accounting group | group_id |`Number` |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | System account name | account_name |String |  |  |
| 5 | References account number | ref_account_number | String |  |  |
| 6 | References account number 2 | ref_account_number2 | String |  |  |
| 7 | Accounting group module | group_module | String |  | `trả thêm`, lấy giá trị `module` của `group_id` trong "Group Definition" tương ứng `group_id` |
| 8 | Group name | group_name | String |  | `trả thêm`, lấy giá trị `account_group_def` của `group_id` trong "Group Definition" tương ứng `group_id` |
| 9 | Bank define account name | bank_define_account_name | String |  | `trả thêm`, lấy giá trị `bank_define_account_name` của `system_account_name` trong "Module Account List" tương ứng `account_name` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/GroupOfAccount​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRP_UPD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account of group id | id | `Yes` | `Number` |  |  |  |
| 2 | Account number |  | No | String |  |  |  |
| 3 | System account name |  | `Yes` | String | 50 |  |  |
| 4 | References account number |  | No | String |  |  |  |
| 5 | References account number 2 |  | No | String |  |  |  |

**Example:**
```json
{
    "id": 444,
    "account_number": "####**{REF}?000000001",
    "account_name": "name update",
    "ref_account_number": "update",
    "ref_account_number2": "update"
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account of group id | id | `Number` |  |  |
| 2 | Accounting group | group_id |`Number` |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | System account name | account_name |String |  |  |
| 5 | References account number | ref_account_number | String |  |  |
| 6 | References account number 2 | ref_account_number2 | String |  |  |

**Example:**
```json
{
    "group_id": 3,
    "account_number": "####**{REF}?000000001",
    "account_name": "name update",
    "ref_account_number": "update",
    "ref_account_number2": "update",
    "id": 444
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/GroupOfAccount​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACGRP_DEL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account of group id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 444
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account of group id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 444
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |