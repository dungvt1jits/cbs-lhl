# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountMapping​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACMAP_SER_SIMPLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Optimal9 GL account | mapping_id | String |  |  |
| 2 | Mapping type | mapping_type | String |  |  |
| 3 | Head-office GL account | mapping_table_name | String |  |  |
| 4 | Account mapping id | id | `Number` |  |  |

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Add
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountMapping​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACMAP_INS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Optimal9 GL account | mapping_id | `Yes` | String |  |  |  |
| 2 | Mapping type | mapping_type | `Yes` | String |  |  | `CodeName='MAPTYPE' and CodeGroup='ACT'` |
| 3 | Head-office GL account | mapping_table_name | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account mapping id | id | `Number` |  |  |  |
| 2 | Optimal9 GL account | mapping_id | String |  |  |  |
| 3 | Mapping type | mapping_type | String |  |  |
| 4 | Head-office GL account | mapping_table_name | String |  |  |

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 3. View
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountMapping​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACMAP_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account mapping id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account mapping id | id | `Number` |  |  |  |
| 2 | Optimal9 GL account | mapping_id | String |  |  |  |
| 3 | Mapping type | mapping_type | String |  |  |
| 4 | Head-office GL account | mapping_table_name | String |  |  |

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Modify
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountMapping​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACMAP_UPD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account mapping id | id | `Yes` | `Number` |  |  | không được sửa |
| 2 | Mapping type | mapping_type | `Yes` | String |  |  | `CodeName='MAPTYPE' and CodeGroup='ACT'` |
| 3 | Head-office GL account | mapping_table_name | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account mapping id | id | `Number` |  |  |  |
| 2 | Optimal9 GL account | mapping_id | String |  |  |  |
| 3 | Mapping type | mapping_type | String |  |  |
| 4 | Head-office GL account | mapping_table_name | String |  |  |

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 5. Delete
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountMapping​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACMAP_DEL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account mapping id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account mapping id | id | `Number` |  |  |  |

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 6. Search advanced
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/AccountMapping​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ACT_ACMAP_SER_ADVANCE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Optimal9 GL account | mapping_id | No | String |  |  |  |
| 2 | Mapping type | mapping_type | `Yes` | String |  |  |  |
| 3 | Head-office GL account | mapping_table_name | `Yes` | String |  |  |  |
| 4 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 5 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Optimal9 GL account | mapping_id | String |  |  |
| 2 | Mapping type | mapping_type | String |  |  |
| 3 | Head-office GL account | mapping_table_name | String |  |  |
| 4 | Account mapping id | id | `Number` |  |  |

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |