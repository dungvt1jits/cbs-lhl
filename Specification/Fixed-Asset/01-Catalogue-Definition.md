# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetCatalog​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_SEARCH_FACCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String | 8 |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Fixed asset type | fixed_asset_type | String |  |  |
| 4 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 5 | Depreciation method | depreciation_method | String |  |  |
| 6 | Catalogue status | catalog_status | String |  |  |
| 7 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetCatalog​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_ADSEARCH_FACCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String | 8 |  |  |
| 2 | Catalogue name | catalog_name | No | String |  |  |  |
| 3 | Fixed asset type | fixed_asset_type | No | String |  |  |  |
| 4 | Fixed asset classification | fixed_asset_classification | No | String |  |  |  |
| 5 | Depreciation method | depreciation_method | No | String |  |  |  |
| 6 | Catalogue status | catalog_status | No | String |  |  |  |
| 7 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 8 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String | 8 |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Fixed asset type | fixed_asset_type | String |  |  |
| 4 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 5 | Depreciation method | depreciation_method | String |  |  |
| 6 | Catalogue status | catalog_status | String |  |  |
| 7 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetCatalog​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_INSERT_FACCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | `Yes` | String | 8 |  |  |
| 2 | Catalogue name | catalog_name | `Yes` | String |  |  |  |
| 3 | Fixed asset type | fixed_asset_type | `Yes` | String |  |  |  |
| 4 | Fixed asset classification | fixed_asset_classification | No | String |  |  |  |
| 5 | Depreciation method | depreciation_method | `Yes` | String |  |  |  |
| 6 | Catalogue status | catalog_status | `Yes` | String |  |  |  |
| 7 | Group ID | group_id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Catalogue name | catalog_name | String | 25 |  |
| 4 | Fixed asset type | fixed_asset_type | String |  |  |
| 5 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 6 | Depreciation method | depreciation_method | String |  |  |
| 7 | Catalogue status | catalog_status | String |  |  |
| 8 | Created by | user_id | `Number` |  |  |
| 9 | Approve by | approve_user | `Number` |  |  |
| 10 | Group ID | group_id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetCatalog​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_VIEW_FACCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 1
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Catalogue name | catalog_name | String | 25 |  |
| 4 | Fixed asset type | fixed_asset_type | String |  |  |
| 5 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 6 | Depreciation method | depreciation_method | String |  |  |
| 7 | Catalogue status | catalog_status | String |  |  |
| 8 | Created by | user_id | `Number` |  |  |
| 9 | Approve by | approve_user | `Number` |  |  |
| 10 | Group ID | group_id | `Number` |  |  |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetCatalog​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_UPDATE_FACCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |
| 2 | Catalogue name | catalog_name | `Yes` | String |  |  |  |
| 3 | Fixed asset type | fixed_asset_type | `Yes` | String |  |  |  |
| 4 | Fixed asset classification | fixed_asset_classification | No | String |  |  |  |
| 5 | Depreciation method | depreciation_method | `Yes` | String |  |  |  |
| 6 | Catalogue status | catalog_status | `Yes` | String |  |  |  |
| 7 | Group ID | group_id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Catalogue name | catalog_name | String | 25 |  |
| 4 | Fixed asset type | fixed_asset_type | String |  |  |
| 5 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 6 | Depreciation method | depreciation_method | String |  |  |
| 7 | Catalogue status | catalog_status | String |  |  |
| 8 | Created by | user_id | `Number` |  |  |
| 9 | Approve by | approve_user | `Number` |  |  |
| 10 | Group ID | group_id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetCatalog​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_DELETE_FACCAT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 1
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 1
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list accounting group for fixed asset
- "module": "FAC"
- [Accounting group trong "Rulefunc"](Specification/Common/01 Rulefunc)