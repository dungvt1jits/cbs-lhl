# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `FAC_OPN`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | No | String | 25 |  |  | pdacc |
| 2 | Owner of this fixed asset | owner_of_this_fixed_asset | No | String | 250 |  |  | cspur |
| 3 | Catalogue code | catalog_code | `Yes` | String | 25 |  | truyền cat code |  | cncat |
| 4 | Catalogue name | catalog_name | No | String | 100 |  |  | ccatnm |
| 5 | Sequence number | sequence_number | No | `Number` | 6 | 0 | cho phép `null` | cnseq |
| 6 | Fixed asset name | fixed_asset_name | `Yes` | String | 250 |  |  | cacnm2 |
| 7 | Fixed asset type | fixed_asset_type | No | String | 1 |  |  | cactp |
| 8 | Fixed asset classification | fixed_asset_classification | No | String | 1 |  |  | sbtype |
| 9 | Depreciation method | depreciation_method | No | String | 1 |  |  | crtype |
| 10 | Fixed asset group | fixed_asset_group | `Yes` | String |  |  |  | cstype |
| 11 | Fixed asset life time | fixed_asset_life_time | No | `Number` |  | 1 |  | c_ukn_amt2 |
| 12 | Life time unit | life_time_unit | No | String | 1 |  |  | recby |
| 13 | Depreciation rate | depreciation_rate | No | `Number` |  | 0 | số có hai số thập phân | crate |
| 14 | Reference number | reference_number | No | String |  |  |  | crepid |
| 15 | Provider name | provider_name | `Yes` | String | 100 |  |  | cacnm |
| 16 | Original currency | original_currency | No | String | 3 |  |  | cccr |
| 17 | Original price | original_price | `Yes` | `Number` |  | > 0 | số có hai số thập phân | camt |
| 18 | Booking currency | booking_currency | No | String | 3 |  |  | cccr2 |
| 19 | Booking value | booking_value | `Yes` | `Number` |  | > 0 | số có hai số thập phân | camt2 |
| 20 | Cross rate | cross_rate | `Yes` | `Number` |  | 0 | số có 9 số thập phân | ccrrate |
| 21 | Salvage value of the asset | salvage_value_of_the_asset | `Yes` | `Number` |  | > 0 | số có hai số thập phân | camt3 |
| 22 | Description | description | No | String | 250 |  |  | descs |
| 23 | Value date | value_date | No | `Date time` |  | Working date | trường ẩn | cvldt |
| 24 | Customer type | customer_type | No | String | 1 |  | trường ẩn | CCTMT |
| 25 | Customer code | customer_code | No | String | 15 |  | trường ẩn | CCTMCD |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Owner of this fixed asset | owner_of_this_fixed_asset | String |  |  |
| 4 | Catalogue code | catalog_code | String |  |  |
| 5 | Catalogue name | catalog_name | String |  |  |
| 6 | Sequence number | sequence_number | String |  |  |
| 7 | Fixed asset name | fixed_asset_name | String |  |  |
| 8 | Fixed asset type | fixed_asset_type | String |  |  |
| 9 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 10 | Depreciation method | depreciation_method | String |  |  |
| 11 | Fixed asset group | fixed_asset_group | String |  |  |
| 12 | Fixed asset life time | fixed_asset_life_time | `Number` |  |  |
| 13 | Life time unit | life_time_unit | String |  |  |
| 14 | Depreciation rate | depreciation_rate | `Number` |  | số có hai số thập phân |
| 15 | Reference number | reference_number | String |  |  |
| 16 | Provider name | provider_name | String |  |  |
| 17 | Original currency | original_currency | String |  |  |
| 18 | Original price | original_price | `Number` |  | số có hai số thập phân |
| 19 | Booking currency | booking_currency | String |  |  |
| 20 | Booking value | booking_value | `Number` |  | số có hai số thập phân |
| 21 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân |
| 22 | Salvage value of the asset | salvage_value_of_the_asset | `Number` |  | số có hai số thập phân |
| 23 | Description | description | String |  |  |
| 24 | Value date | value_date | `Date time` |  | Working date |
| 25 | Customer type | customer_type | String |  |  |
| 26 | Customer code | customer_code | String |  |  |
| 27 | User id |  | `Number` |  |  |
| 28 | Transaction status | status | String |  |  |
| 29 | Transaction date |  | `Date time` |  |  |

## 1.2 Transaction flow
**Conditions:**
-	"Catalogue code": exists with status "Normal"
-   Original price > 0.00
-   Booking value > 0.00
-   Salvage value of the asset > 0.00

**Flow of events:**
-	Account code will auto create.
-	Allow creation account code by number sequence define.
    +	Price original can be different with book value.
    +	Currency between price original and book value can be different.
-	When complete transaction: status is "New".

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- | 
| 1 | Tran reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Owner of this fixed asset | owner_of_this_fixed_asset | String |  |  |
| 4 | Catalogue code | catalog_code | String | 8 |  |
| 5 | Catalogue name | catalog_name | String |  |  |
| 6 | Sequence number | sequence_number | String |  |  |
| 7 | Fixed asset name | fixed_asset_name | String |  |  |
| 8 | Fixed asset type | fixed_asset_type | String |  |  |
| 9 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 10 | Depreciation method | depreciation_method | String |  |  |
| 11 | Fixed asset group | fixed_asset_group | String |  |  |
| 12 | Fixed asset life time | fixed_asset_life_time | `Number` |  |  |
| 13 | Life time unit | life_time_unit | String |  |  |
| 14 | Depreciation rate | depreciation_rate | `Number` |  | số có hai số thập phân |
| 15 | Reference number | reference_number | String |  |  |
| 16 | Provider name | provider_name | String |  |  |
| 17 | Original currency | original_currency | String |  |  |
| 18 | Original price | original_price | `Number` |  | số có hai số thập phân |
| 19 | Booking currency | booking_currency | String |  |  |
| 20 | Booking value | booking_value | `Number` |  | số có hai số thập phân |
| 21 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân |
| 22 | Salvage value of the asset | salvage_value_of_the_asset | `Number` |  | số có hai số thập phân |
| 23 | Description | description | String |  |  |
| 24 | Value date | value_date | `Date time` |  | Working date |
| 25 | Customer type | customer_type | String |  |  |
| 26 | Customer code | customer_code | String |  |  |
| 27 | User id |  | `Number` |  |  |
| 28 | Transaction status | status | String |  |  |
| 29 | Transaction date |  | `Date time` |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC |  | Không dùng |
| 2 | GET_INFO_CATNAME | FAC_RF_SETINFO_CATALOG | [FAC_RF_SETINFO_CATALOG](Specification/Common/19 Fixed Asset Rulefuncs) |
| 3 | LKP_DATA_CATEGORY | SQL_LOOKUP_FACCAT | [SQL_LOOKUP_FACCAT](Specification/Common/01 Rulefunc) |

# 5. Signature
- None: Phân hệ FAC không có customer nên sẽ không có chức năng signature