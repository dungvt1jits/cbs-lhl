# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `FAC_SBC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | pdacc |
| 2 | Fixed asset name | fixed_asset_name | No | String | 250 |  |  | cacnm2 |
| 3 | Net book value | net_book_value | `Yes` | `Number` |  | 0 | số có hai số thập phân | camt3 |
| 4 | Accumulate not posted | acummulate_not_posted | No | `Number` |  | < 0, = 0 or > 0 | số có hai số thập phân | camt5 |
| 5 | Accumulate posted | acummulate_posted | No | `Number` |  | 0 | số có hai số thập phân | iacr |
| 6 | Book value | book_value | No | `Number` |  | 0 | số có hai số thập phân | camt4 |
| 7 | Book currency | book_currency | `Yes` | String | 3 |  |  | cccr2 |
| 8 | Selling amount | selling_amount | No | `Number` |  | 0 | số có hai số thập phân | camt |
| 9 | Cash currency | cash_currency | `Yes` | String | 3 |  |  | cccr |
| 10 | Cross rate | cross_rate | `Yes` | `Number` |  | 0 | số có 9 số thập phân | crate |
| 11 | Amount in cash | amount_in_cash | `Yes` | `Number` |  | 0 | số có hai số thập phân | camt2 |
| 12 | Purchaser id | purchaser_id | No | String | 15 |  |  | cctmcd |
| 13 | Purchaser name | purchaser_name | No | String | 250 |  |  | cctma |
| 14 | Purchaser address | purchaser_address | No | String | 250 |  |  | cidplace |
| 15 | Purchaser description | purchaser_description | No | String | 250 |  |  | mdesc |
| 16 | Description | description | No | String | 250 |  |  | descs |
| 17 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | cvldt |
| 18 | Exchange rate | exchange_rate | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | pcsexr |
| 19 | Cash amount /BCY | cash_amount_bcy | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | pcscvt |
| 20 | Amount (Debit account/BCY) 1 | amount_cbkexr | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | cbkexr |
| 21 | Amount (Debit account/BCY) 2 | amount_ccvt | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | ccvt |
| 22 | Income or expend | income_or_expend | `Yes` | `Number` |  | < 0, = 0 or > 0 | số có hai số thập phân, trường ẩn | pcsamt |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String | 16 |  |
| 3 | Fixed asset name | fixed_asset_name | String |  |  |
| 4 | Net book value | net_book_value | `Number` |  | số có hai số thập phân |
| 5 | Accumulate not posted | acummulate_not_posted | `Number` |  | số có hai số thập phân |
| 6 | Accumulate posted | acummulate_posted | `Number` |  | số có hai số thập phân |
| 7 | Book value | book_value | `Number` |  | số có hai số thập phân |
| 8 | Book currency | book_currency | String | 3 |  |
| 9 | Selling amount | selling_amount | `Number` |  | số có hai số thập phân |
| 10 | Cash currency | cash_currency | String | 3 |  |
| 11 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân |
| 12 | Amount in cash | amount_in_cash | `Number` |  | số có hai số thập phân |
| 13 | Purchaser id | purchaser_id | String | 8 |  |
| 14 | Purchaser name | purchaser_name | String |  |  |
| 15 | Purchaser address | purchaser_address | String |  |  |
| 16 | Purchaser description | purchaser_description | String |  |  |
| 17 | Description | description | String |  |  |
| 18 | Value date | value_date | `Date time` |  |  |
| 19 | Exchange rate | exchange_rate | `Number` |  |  |
| 20 | Cash amount /BCY | cash_amount_bcy | `Number` |  |  |
| 21 | Amount (Debit account/BCY) 1 | amount_cbkexr | `Number` |  |  |
| 22 | Amount (Debit account/BCY) 2 | amount_ccvt | `Number` |  |  |
| 23 | Income or expend | income_or_expend | `Number` |  |  |
| 24 | Transaction status | status | String |  |  |
| 25 | Transaction date | transaction_date | `Date time` |  |  |
| 26 | User id | user_id | `Number` |  |  |
| 27 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
-	Asset code: exists with status "Normal" and same branch with user.
-	Amount sell must be same currency of book value.

**Flow of events:**
-	Allow to sell gain or loss.
    + Sell gain: (net_book_value - selling_amount) <= 0.00
      + Cập nhật các trường credit trong DB là `net_book_value`
    + Sell loss: (net_book_value - selling_amount) > 0.00
      + Cập nhật các trường credit trong DB là `selling_amount`
-	Transaction complete: 
    +	System will change status to "Sold".
    +	Stop calculation depreciation (Net book value = 0).
    +	Cash at counter will increase.

**Posting:**
-	Case: Selling amount > Net book value (Gain)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | ACCUMULATE | Book value - Net book value | Book currency code of fixed asset |
| 1 | C | FASSET | Book value - Net book value | Book currency code of fixed asset |
| 2 | D | CASH | Net book value | Book currency code of fixed asset |
| 2 | C | FASSET | Net book value | Book currency code of fixed asset |
| 3 | D | CASH | Selling amount - Net book value | Book currency code of fixed asset |
| 3 | C | INCOME | Selling amount - Net book value | Book currency code of fixed asset |
| 4 | D | DEPRECIATION | Accumulate not posted | Book currency code of fixed asset |
| 4 | C | ACCUMULATE | Accumulate not posted | Book currency code of fixed asset |

-	Case: Selling amount < Net book value (Loss)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | ACCUMULATE | Book value - Net book value | Book currency code of fixed asset |
| 1 | C | FASSET | Book value - Net book value | Book currency code of fixed asset |
| 2 | D | CASH | Selling amount | Book currency code of fixed asset |
| 2 | C | FASSET | Selling amount | Book currency code of fixed asset |
| 3 | D | EXPENSE | Net book value - Selling amount | Book currency code of fixed asset |
| 3 | C | FASSET | Net book value - Selling amount | Book currency code of fixed asset |
| 4 | D | DEPRECIATION | Accumulate not posted | Book currency code of fixed asset |
| 4 | C | ACCUMULATE | Accumulate not posted | Book currency code of fixed asset |

-	Case: Selling amount = Net book value

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | ACCUMULATE | Accumulate posted | Book currency code of fixed asset |
| 1 | C | FASSET | Accumulate posted | Book currency code of fixed asset |
| 2 | D | CASH | Net book value | Book currency code of fixed asset |
| 2 | C | FASSET | Net book value | Book currency code of fixed asset |

**Cập nhật thông tin fixed asset account:**
[Tham khảo thông tin chi tiết tại mục 2](Specification/Common/06 Transaction Flow Fixed Asset)<br>

**Voucher:**
- `A203`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_SIG_PDACC |  | Không dùng |
| 2 | GET_INFO_PDACC | FAC_RF_SETINFO_ACCOUNT | [FAC_RF_SETINFO_ACCOUNT](Specification/Common/19 Fixed Asset Rulefuncs) |  | `accummulate_not_posted` = `acummulate_not_posted`, `expense_amount` = `acummulate_posted` |
| 3 | GET_INFO_CRATE | FX_RULEFUNC_GET_INFO_CRATE_TB_TA | [FX_RULEFUNC_GET_INFO_CRATE_TB_TA](Specification/Common/20 FX Rulefuncs) |
| 4 | GET_INFO_PCSEXR | FAC_GET_INFO_PCSEXR_TB | [FAC_GET_INFO_PCSEXR_TB](Specification/Common/19 Fixed Asset Rulefuncs) |
| 5 | GET_INFO_CBKEXR | FAC_GET_INFO_PCSEXR | [FAC_GET_INFO_PCSEXR](Specification/Common/19 Fixed Asset Rulefuncs) |

# 5. Signature
- None: Phân hệ FAC không có customer nên sẽ không có chức năng signature