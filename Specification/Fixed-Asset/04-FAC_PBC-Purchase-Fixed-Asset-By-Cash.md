# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `FAC_PBC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | pdacc |
| 2 | Fixed asset name | fixed_asset_name | No | String | 250 |  |  | cacnm2 |
| 3 | Original currency | original_currency | `Yes` | String | 3 |  |  | cccr2 |
| 4 | Original amount | original_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | camt |
| 5 | Cash currency (book cccy) | cash_currency | `Yes` | String | 3 |  |  | cccr |
| 6 | Cross rate (At time open FA) | cross_rate | `Yes` | `Number` |  | 0 | số có 9 số thập phân | crate |
| 7 | Amount in cash | amount_in_cash | `Yes` | `Number` |  | 0 | số có hai số thập phân | camt2 |
| 8 | Vendor id | vendor_id | No | String | 15 |  |  | cctmcd |
| 9 | Vendor name | vendor_name | No | String | 250 |  |  | cctma |
| 10 | Vendor address | vendor_address | No | String | 250 |  |  | cidplace |
| 11 | Vendor description | vendor_description | No | String | 250 |  |  | mdesc |
| 12 | Description | description | No | String | 250 |  |  | descs |
| 13 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | cvldt |
| 14 | Original price | original_price | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | camt3 |
| 15 | Booked amount | booked_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | camt4 |
| 16 | Amount (Debit account/BCY) 1 | amount_cbkexr | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | cbkexr |
| 17 | Amount (Debit account/BCY) 2 | amount_ccvt | No | `Number` |  | 0 | số có hai số thập phân, trường ẩn, `CAMT*CBKEXR` | ccvt |
| 18 | Exchange rate | exchange_rate | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | pcsexr |
| 19 | Cash amount /BCY | cash_amount_bcy | No | `Number` |  | 0 | số có hai số thập phân, trường ẩn, `CAMT*PCSEXR` | pcscvt |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String | 16 |  |
| 3 | Fixed asset name | fixed_asset_name | String |  |  |
| 4 | Original currency | original_currency | String |  |  |
| 5 | Original amount | original_amount | `Number` |  |  |
| 6 | Cash currency (book cccy) | cash_currency | String |  |  |
| 7 | Cross rate (At time open FA) | cross_rate | `Number` |  |  |
| 8 | Amount in cash | amount_in_cash | `Number` |  |  |
| 9 | Vendor id | vendor_id | String | 8 |  |  |
| 10 | Vendor name | vendor_name | String |  |  |
| 11 | Vendor address | vendor_address | String |  |  |
| 12 | Vendor description | vendor_description | String |  |  |
| 13 | Description | description | String |  |  |
| 14 | Value date | value_date | `Date time` |  |  |
| 15 | Original price | original_price | `Number` |  |  |
| 16 | Booked amount | booked_amount | `Number` |  |  |
| 17 | Amount (Debit account/BCY) 1 | amount_cbkexr | `Number` |  |  |
| 18 | Amount (Debit account/BCY) 2 | amount_ccvt | `Number` |  |  |
| 19 | Exchange rate | exchange_rate | `Number` |  |  |
| 20 | Cash amount /BCY | cash_amount_bcy | `Number` |  |  |
| 21 | User id |  | `Number` |  |  |
| 22 | Transaction status | status | String |  |  |
| 23 | Transaction date |  | `Date time` |  |  |
| 24 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Asset code: exists with status "New" and same branch with user.
- User has position with cash.
- User must have enough cash to purchase fixed asset.
- original_amount < or = (original_price - booked_amount)

**Flow of events:**
-	Transaction complete: 
    - System will change status to "Normal". 
    - Booking amount increase (DB: BookingAmount = NetBookingValue).
    - Cash will decrease.
-	Allow starting depreciation from date bought and next time until net book value = 0.

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | FASSET | Book value (Amount in cash) | Book currency code of fixed asset |
| 1 | C | CASH | Book value (Amount in cash) | Book currency code of fixed asset |

`Note: Book value = Amount in cash (cash_amount)`

**Cập nhật thông tin fixed asset account:**
[Tham khảo thông tin chi tiết tại mục 1](Specification/Common/06 Transaction Flow Fixed Asset)<br>

**Cập nhật thông tin deposit account:**
[Tham khảo thông tin chi tiết tại mục 4](Specification/Common/06 Transaction Flow Deposit)<br>

**Voucher:**
- `A203`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_INFO_PDACC | FAC_RF_SETINFO_ACCOUNT | [FAC_RF_SETINFO_ACCOUNT](Specification/Common/19 Fixed Asset Rulefuncs) |  | `booking_currency_code` = `original_currency`, `currency_code` = `cash_currency` |
| 2 | GET_SIG_PDACC |  | Không dùng |
| 3 | GET_INFO_ORGAMT | FAC_RF_SETINFO_ACCOUNT | [FAC_RF_SETINFO_ACCOUNT](Specification/Common/19 Fixed Asset Rulefuncs) |
| 4 | GET_INFO_CBKEXR | FAC_GET_INFO_PCSEXR | [FAC_GET_INFO_PCSEXR](Specification/Common/19 Fixed Asset Rulefuncs) | `currency01` = `original_currency` | `rate_currency01` = `amount_cbkexr` |

# 5. Signature
- None: Phân hệ FAC không có customer nên sẽ không có chức năng signature