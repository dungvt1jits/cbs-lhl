# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetAccount​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_SEARCH_FACACT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | fixed_asset_account_name | String |  |  |
| 3 | Book currency | booking_currency_code | String | 3 |  |
| 4 | Catalogue code | catalog_code | String | 8 |  |
| 5 | Fixed asset type | fixed_asset_type | String |  |  |
| 6 | Fixed asset status | fixed_asset_status | String |  |  |
| 7 | Fixed asset account id | id | `Number` |  |  |

**Example:**
```json
{

}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetAccount​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_ADSEARCH_FACACT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |
| 2 | Account name | fixed_asset_account_name | No | String |  |  |  |
| 3 | Book currency | booking_currency_code | No | String | 3 |  |  |
| 4 | Catalogue code | catalog_code | No | String | 8 |  |  |
| 5 | Fixed asset type | fixed_asset_type | No | String |  |  |  |
| 6 | Fixed asset status | fixed_asset_status | No | String |  |  |  |
| 7 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 8 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | fixed_asset_account_name | String |  |  |
| 3 | Book currency | booking_currency_code | String | 3 |  |
| 4 | Catalogue code | catalog_code | String | 8 |  |
| 5 | Fixed asset type | fixed_asset_type | String |  |  |
| 6 | Fixed asset status | fixed_asset_status | String |  |  |
| 7 | Fixed asset account id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
[Tham khảo thông tin chi tiết tại đây](Specification/Fixed Asset/03 FAC_OPN Open New Fixed Asset)<br>

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetAccount​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_VIEW_FACACT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Fixed asset account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 3
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String | 36 |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Fixed asset name | fixed_asset_account_name | String |  |  |
| 4 | Booking currency code | booking_currency_code | String |  |  |
| 5 | Branch id | branchid | `Number` |  |  |
| 6 | Reference number | reference_number | String |  |  |  |
| 7 | Catalogue id | catalog_id | `Number` |  |  |
| 8 | Fixed asset type | fixed_asset_type | String |  |  |
| 9 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 10 | Depreciation method | depreciation_method | String |  |  |
| 11 | Fixed asset life time | fixed_asset_life_time | `Number` |  |  |
| 12 | Provider name | provider_name | String |  |  |
| 13 | Owner fixed asset | owner | String |  |  |
| 14 | Currency code | currency_code | String |  |  |
| 15 | Status | fixed_asset_status | String |  |  |
| 16 | Created by | user_id | String |  |  |
| 17 | Approved by | approve_user | String |  |  |
| 18 | Depreciation rate | depreciation_rate | `Number` |  | số có hai số thập phân |
| 19 | Original price | original_price | `Number` |  | số có hai số thập phân |
| 20 | Book amount | booking_amount | `Number` |  | số có hai số thập phân |
| 21 | Net book value | net_booking_value | `Number` |  | số có hai số thập phân |
| 22 | Accummulate amount | accummulate_amount | `Number` |  | số có hai số thập phân |
| 23 | Expense amount | expense_amount | `Number` |  | số có hai số thập phân |
| 24 | Insurrance value | insurrance_value | `Number` |  | số có hai số thập phân |
| 25 | Insurrance fee value | insurrance_fee | `Number` |  | số có hai số thập phân |
| 26 | Salvage value of the asset | salvage_amount | `Number` |  | số có hai số thập phân |
| 27 | Income amount for this asset | income_amount | `Number` |  | số có hai số thập phân |
| 28 | Week debit | week_debit | `Number` |  | số có hai số thập phân |
| 29 | Week credit | week_credit | `Number` |  | số có hai số thập phân |
| 30 | Month debit | month_debit | `Number` |  | số có hai số thập phân |
| 31 | Month credit | month_credit | `Number` |  | số có hai số thập phân |
| 32 | Quater debit | quater_debit | `Number` |  | số có hai số thập phân |
| 33 | Quater credit | quater_credit | `Number` |  | số có hai số thập phân |
| 34 | Semi-annual debit | semiannual_debit | `Number` |  | số có hai số thập phân |
| 35 | Semi-annual credit | semiannual_credit | `Number` |  | số có hai số thập phân |
| 36 | Year debit | year_debit | `Number` |  | số có hai số thập phân |
| 37 | Year credit | year_credit | `Number` |  | số có hai số thập phân |
| 38 | Fixed asset account id | id | `Number` |  |
| 39 | Customer id | customer_id | `Number` | không dùng, xem xét xóa đi |
| 40 | Customer type | customer_type | String | không dùng, xem xét xóa đi |
| 41 | Fixed asset group | fixed_asset_group | String |  |
| 42 | Life time unit | fixed_asset_life_time_unit | String |  |
| 43 | Purchase date | buydt | `Date time` |  |
| 44 | Depreciation date from | dprdt | `Date time` |  |
| 45 | Warrantee from | wrfrdt | `Date time` |  |
| 46 | Warrantee expire date | wrtodt | `Date time` |  |
| 47 | Warrantee certificate serial number | wrsrn | String |  |
| 48 | Account manager staff id | staff_id | `Number` | không dùng, xem xét xóa đi |
| 49 | Remark | remark | String |  |
| 50 | Reference id | old_id_of_customer | String | (use for conversion old account id,…) |
| 51 | User define field | udfield1 | String | (Reserve), json structure |
| 52 | Cross rate | cross_rate | `Number` |  |
| 53 | Expire date | expire_date | `Date time` |  | `trả thêm` |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Account manager staff code | staff_code | String |  | `trả thêm` |
|  | Account manager staff name | staff_name | String |  | `trả thêm` |
|  | Catalogue code | catalog_code | String |  | `trả thêm` |
|  | Branch name | branch_name | String |  | `trả thêm` |
|  | Catalogue name | catalog_name | String |  | `trả thêm` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetAccount​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_UPDATE_FACACT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Fixed asset account id | id | `Yes` | `Number` |  |  |  |
| 2 | Fixed asset name | fixed_asset_account_name | `Yes` | String |  |  |  |
| 3 | Reference number | reference_number | No | String |  |  |  |
| 4 | Fixed asset classification | fixed_asset_classification | No | String |  |  |  |
| 5 | Depreciation method | depreciation_method | `Yes` | String |  |  |  |
| 6 | Fixed asset life time | fixed_asset_life_time | No | `Number` |  |  |  |
| 7 | Provider name | provider_name | `Yes` | String |  |  |  |
| 8 | Owner fixed asset | owner | No | String |  |  |  |
| 7 | Life time unit | fixed_asset_life_time_unit | No | String |  |  | `gửi thêm`, cho phép sửa |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String | 36 |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Fixed asset name | fixed_asset_account_name | String |  |  |
| 4 | Booking currency code | booking_currency_code | String |  |  |
| 5 | Branch code | branch_code | String |  |  |
| 6 | Reference number | reference_number | String |  |  |  |
| 7 | Catalogue code | catalog_code | String |  |  |
| 8 | Fixed asset type | fixed_asset_type | String |  |  |
| 9 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 10 | Depreciation method | depreciation_method | String |  |  |
| 11 | Fixed asset life time | fixed_asset_life_time | `Number` |  |  |
| 12 | Provider name | provider_name | String |  |  |
| 13 | Owner fixed asset | owner | String |  |  |
| 14 | Currency code | currency_code | String |  |  |
| 15 | Status | fixed_asset_status | String |  |  |
| 16 | Created by | user_id | String |  |  |
| 17 | Approved by | approve_user | String |  |  |
| 18 | Depreciation rate | depreciation_rate | `Number` |  | số có hai số thập phân |
| 19 | Original price | original_price | `Number` |  | số có hai số thập phân |
| 20 | Book amount | booking_amount | `Number` |  | số có hai số thập phân |
| 21 | Net book value | net_booking_value | `Number` |  | số có hai số thập phân |
| 22 | Accummulate amount | accummulate_amount | `Number` |  | số có hai số thập phân |
| 23 | Expense amount | expense_amount | `Number` |  | số có hai số thập phân |
| 24 | Insurrance value | insurrance_value | `Number` |  | số có hai số thập phân |
| 25 | Insurrance fee value | insurrance_fee | `Number` |  | số có hai số thập phân |
| 26 | Salvage value of the asset | salvage_amount | `Number` |  | số có hai số thập phân |
| 27 | Income amount for this asset | income_amount | `Number` |  | số có hai số thập phân |
| 28 | Week debit | week_debit | `Number` |  | số có hai số thập phân |
| 29 | Week credit | week_credit | `Number` |  | số có hai số thập phân |
| 30 | Month debit | month_debit | `Number` |  | số có hai số thập phân |
| 31 | Month credit | month_credit | `Number` |  | số có hai số thập phân |
| 32 | Quater debit | quater_debit | `Number` |  | số có hai số thập phân |
| 33 | Quater credit | quater_credit | `Number` |  | số có hai số thập phân |
| 34 | Semi-annual debit | semiannual_debit | `Number` |  | số có hai số thập phân |
| 35 | Semi-annual credit | semiannual_credit | `Number` |  | số có hai số thập phân |
| 36 | Year debit | year_debit | `Number` |  | số có hai số thập phân |
| 37 | Year credit | year_credit | `Number` |  | số có hai số thập phân |
| 38 | Fixed asset account id | id | `Number` |  |
| 39 | Customer id | customer_id | `Number` | không dùng, xem xét xóa đi |
| 40 | Customer type | customer_type | String | không dùng, xem xét xóa đi |
| 41 | Fixed asset group | fixed_asset_group | String |  |
| 42 | Life time unit | fixed_asset_life_time_unit | String |  |
| 43 | Purchase date | buydt | `Date time` |  |
| 44 | Depreciation date from | dprdt | `Date time` |  |
| 45 | Warrantee from | wrfrdt | `Date time` |  |
| 46 | Warrantee expire date | wrtodt | `Date time` |  |
| 47 | Warrantee certificate serial number | wrsrn | String |  |
| 48 | Account manager staff id | staff_id | `Number` | không dùng, xem xét xóa đi |
| 49 | Remark | remark | String |  |
| 50 | Reference id | old_id_of_customer | String | (use for conversion old account id,…) |
| 51 | User define field | udfield1 | String | (Reserve), json structure |
| 52 | Cross rate | cross_rate | `Number` |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/FixedAssetAccount​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_DELETE_FACACT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Fixed asset account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 3
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Fixed asset account id | id | `Number` |  |

**Example:**
```json
{
    "id": 3
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |