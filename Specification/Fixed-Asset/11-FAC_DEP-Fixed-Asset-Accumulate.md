# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `FAC_DEP`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Fixasset number | fixasset_number | `Yes` | String | 25 |  |  | PDACC |
| 2 | Accumulate amount | accumulate_amount | `Yes` | `Number` |  | >0 | số có hai số thập phân | CAMT |
| 3 | Transaction description | transaction_description | No | String | 250 |  |  | MDESC |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Fixasset number | fixasset_number | String | 16 |  |
| 3 | Accumulate amount | accumulate_amount | `Number` |  | số có hai số thập phân |
| 4 | Transaction description | transaction_description | String |  |  |
| 5 | Transaction status | status | String |  |  |
| 6 | Transaction date | transaction_date | `Date time` |  |  |
| 7 | User id | user_id | `Number` |  |  |
| 8 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
-	Asset code: exists with status "Normal".
-	Net book value larger than zero. 
-	Accumulate amount > 0

**Flow of events:**
-	Transaction complete:
    - If Depreciation Method <> Straight forward (F)
        - Expense amount increase with `accumulate_amount`.
    - If Depreciation Method = Straight forward (F)
        - Net book value decrease with `accumulate_amount`.
        - Accummulate amount increase with `accumulate_amount`.
        - Expense amount increase with `accumulate_amount`.

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPRECIATION | Accumulate amount | Book currency code of fixed asset |
| 1 | C | ACCUMULATE | Accumulate amount | Book currency code of fixed asset |

**Voucher:**
- None

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC |  | Không dùng |
| 2 | GET_INFO_PDACC | FAC_RF_SETINFO_ACCOUNT | [FAC_RF_SETINFO_ACCOUNT](Specification/Common/19 Fixed Asset Rulefuncs) |
| 3 | GET_INFO_CRATE | FX_RULEFUNC_GET_INFO_CRATE_TB_TA | [FX_RULEFUNC_GET_INFO_CRATE_TB_TA](Specification/Common/20 FX Rulefuncs) |

# 5. Signature
- None: Phân hệ FAC không có customer nên sẽ không có chức năng signature