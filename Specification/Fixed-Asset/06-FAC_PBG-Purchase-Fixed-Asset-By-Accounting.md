# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `FAC_PBG`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | pdacc |
| 2 | Fixed asset name | fixed_asset_name | No | String | 250 |  |  | cacnm2 |
| 3 | Original currency | original_currency | `Yes` | String | 3 |  |  | cccr2 |
| 4 | Original amount | original_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | camt |
| 5 | GL account | gl_account | `Yes` | String | 25 |  |  | pgacc |
| 6 | GL currency | gl_currency | `Yes` | String | 3 |  |  | cccr |
| 7 | Cross rate | cross_rate | `Yes` | `Number` |  | 0 | số có 9 số thập phân | crate |
| 8 | Book amount | book_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | camt2 |
| 9 | Vendor id | vendor_id | No | String | 15 |  |  | cctmcd |
| 10 | Vendor name | vendor_name | No | String | 250 |  |  | cctma |
| 11 | Vendor address | vendor_address | No | String | 250 |  |  | cidplace |
| 12 | Vendor description | vendor_description | No | String | 250 |  |  | mdesc |
| 13 | Description | description | No | String | 250 |  |  | descs |
| 14 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | cvldt |
| 15 | Original price | original_price | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | camt3 |
| 16 | Booked amount | booked_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | camt4 |
| 17 | Amount (Debit account/BCY) 1 | amount_cbkexr | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | cbkexr |
| 18 | Amount (Debit account/BCY) 2 | amount_ccvt | No | `Number` |  | 0 | số có hai số thập phân, trường ẩn | ccvt |
| 19 | Exchange rate | exchange_rate | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | pcsexr |
| 20 | Cash amount /BCY | cash_amount_bcy | No | `Number` |  | 0 | số có hai số thập phân, trường ẩn | pcscvt |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String | 16 |  |
| 3 | Fixed asset name | fixed_asset_name | String |  |  |
| 4 | Original currency | original_currency | String | 3 |  |
| 5 | Original amount | original_amount | `Number` |  | số có hai số thập phân |
| 6 | GL account | gl_account | String |  |  |
| 7 | GL currency | gl_currency | String | 3 |  |
| 8 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân |
| 9 | Book amount | book_amount | `Number` |  | số có hai số thập phân |
| 10 | Vendor id | vendor_id | String | 8 |  |
| 11 | Vendor name | vendor_name | String |  |  |
| 12 | Vendor address | vendor_address | String |  |  |
| 13 | Vendor description | vendor_description | String |  |  |
| 14 | Description | description | String |  |  |
| 15 | Value date | value_date | `Date time` |  |  |
| 16 | Original price | original_price | `Number` |  |  |
| 17 | Booked amount | booked_amount | `Number` |  |  |
| 18 | Amount (Debit account/BCY) 1 | amount_cbkexr | `Number` |  |  |
| 19 | Amount (Debit account/BCY) 2 | amount_ccvt | `Number` |  |  |
| 20 | Exchange rate | exchange_rate | `Number` |  |  |
| 21 | Cash amount /BCY | cash_amount_bcy | `Number` |  |  |
| 22 | Transaction status | status | String |  |  |
| 23 | Transaction date | transaction_date | `Date time` |  |  |
| 24 | User id | user_id | `Number` |  |  |
| 25 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
- Asset code: exists with status "New" and same branch with user.
- GL account: exists and same branch with user. 
- GL allows credit side.
- Currency of GL must be same with currency of book value.
- Branch of GL and fixed asset must be same.
- original_amount < or = (original_price - booked_amount)

**Flow of events:**
-	Transaction complete: 
    +	System will change status to "Normal".
    +	Credit GL.
-	Allow starting depreciation from date bought and next time until net book value = 0.

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | FASSET | Book value (Book amount) | Book currency code of fixed asset |
| 1 | C | GL | Book value (Book amount) | Book currency code of fixed asset |

`Note: Book value = Book amount (book_amount)`

**Cập nhật thông tin fixed asset account:**
[Tham khảo thông tin chi tiết tại mục 1](Specification/Common/06 Transaction Flow Fixed Asset)<br>

**Voucher:**
- `A203`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_PDACC | FAC_RF_SETINFO_ACCOUNT | [FAC_RF_SETINFO_ACCOUNT](Specification/Common/19 Fixed Asset Rulefuncs) |  | `booking_currency_code` = `original_currency`, `currency_code` = `cash_currency` |
| 2 | GET_SIG_PDACC |  | Không dùng |
| 3 | GET_INFO_ORGAMT | FAC_RF_SETINFO_ACCOUNT | [FAC_RF_SETINFO_ACCOUNT](Specification/Common/19 Fixed Asset Rulefuncs) |
| 4 | GET_INFO_CCRCD | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) |
| 5 | LKP_DATA_PGACC | ACT_ACCHRT_LOOKUP_BY_BRANCHID | [ACT_ACCHRT_LOOKUP_BY_BRANCHID](Specification/Common/11 Accounting Rulefuncs) |

# 5. Signature
- None: Phân hệ FAC không có customer nên sẽ không có chức năng signature