# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `FAC_TFB`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | pdacc |
| 2 | Fixed asset name | fixed_asset_name | No | String | 250 |  |  | cacnm2 |
| 3 | Book currency | book_currency | `Yes` | String | 3 |  |  | cccr2 |
| 4 | Net book amount | net_book_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | camt |
| 5 | Acummulate not posted | acummulate_not_posted | No | `Number` |  | < 0, = 0 or > 0 | số có hai số thập phân | camt5 |
| 6 | Book amount | book_amount | No | `Number` |  | 0 | số có hai số thập phân | camt2 |
| 7 | New branch | new_branch | `Yes` | `Number` |  | branch của user login | số có hai số thập phân | cbrid |
| 8 | New account | new_account | No | String | 25 |  |  | PDOACC |
| 9 | Customer id | customer_id | No | String | 15 |  |  | cctmcd |
| 10 | Customer name | customer_name | No | String | 250 |  |  | cctma |
| 11 | Customer address | customer_address | No | String | 250 |  |  | cidplace |
| 12 | Customer description | customer_description | No | String | 250 |  |  | mdesc |
| 13 | Description | description | No | String | 250 |  |  | descs |
| 14 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | cvldt |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | New account | new_account | String |  |  |
| 3 | Account number | account_number | String | 16 |  |
| 4 | Fixed asset name | fixed_asset_name | String |  |  |
| 5 | Book currency | book_currency | String | 3 |  |
| 6 | Net book amount | net_book_amount | `Number` |  | số có hai số thập phân |
| 7 | Acummulate not posted | acummulate_not_posted | `Number` |  | số có hai số thập phân |
| 8 | Book amount | book_amount | `Number` |  | số có hai số thập phân |
| 9 | New branch | new_branch | String |  | số có hai số thập phân |
| 10 | Customer id | customer_id | String | 8 |  |
| 11 | Customer name | customer_name | String |  |  |
| 12 | Customer address | customer_address | String |  |  |
| 13 | Customer description | customer_description | String |  |  |
| 14 | Description | description | String |  |  |
| 15 | Value date | value_date | `Date time` |  |  |
| 16 | Transaction status | status | String |  |  |
| 17 | Transaction date | transaction_date | `Date time` |  |  |
| 18 | User id | user_id | `Number` |  |  |
| 19 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
-	Asset code: exists with status "Normal".

**Flow of events:**
-	Transaction complete when transfer fixed asset from Branch A to Branch B
-	Update information of fixed asset at Branch A: 
    +	Status: Transfer
    +	Book amount = Book value
    +	Net book amount = Net book value
    +	Accumulate amount = Expense amount = Accumulate not posted 
-	Insert new fixed asset at Branch B: 
    +	Status: Normal
    +	Book amount = Accumulate posted
    +	Net book amount = 0
    +	Accumulate amount = Expense amount = Accumulate not posted

**Posting:**
-	Transfer fixed asset from Branch (BR.A) to Branch (BR.B)

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| FASSET |  |  |  |  |
| 1 | D | B.FASSET | Book amount | Book currency code of fixed asset |
| 1 | C | B.IBT.HO | Book amount | Book currency code of fixed asset |
| 2 | D | A.IBT.HO | Book amount | Book currency code of fixed asset |
| 2 | C | A.FASSET | Book amount | Book currency code of fixed asset |
| 3 | D | HO.IBT.B | Book amount | Book currency code of fixed asset |
| 3 | C | HO.IBT.A | Book amount | Book currency code of fixed asset |
| ACCUMULATE |  |  |  |  |
| 4 | D | A.ACCUMULATE | Book amount - Net book amount | Book currency code of fixed asset |
| 4 | C | A.IBT.HO | Book amount - Net book amount | Book currency code of fixed asset |
| 5 | D | B.IBT.HO | Book amount - Net book amount | Book currency code of fixed asset |
| 5 | C | B.ACCUMULATE | Book amount - Net book amount | Book currency code of fixed asset |
| 6 | D | HO.IBT.A | Book amount - Net book amount | Book currency code of fixed asset |
| 6 | C | HO.IBT.B | Book amount - Net book amount | Book currency code of fixed asset |
| DEPRECIATION |  |  |  |  |
| 7 | D | A. DEPRECIATION | Acummulate not posted | Book currency code of fixed asset |
| 7 | C | A. ACCUMULATE | Acummulate not posted | Book currency code of fixed asset |

**Voucher:**
- `A203`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_SIG_PDACC |  | Không dùng |
| 2 | GET_INFO_PDACC | FAC_RF_SETINFO_ACCOUNT | [FAC_RF_SETINFO_ACCOUNT](Specification/Common/19 Fixed Asset Rulefuncs) |  | `accummulate_not_posted` = `acummulate_not_posted` |
| 3 | LKP_DATA_CBRID | ADM_LOOKUP_BRANCH | [ADM_LOOKUP_BRANCH](Specification/Common/01 Rulefunc) |

# 5. Signature
- None: Phân hệ FAC không có customer nên sẽ không có chức năng signature