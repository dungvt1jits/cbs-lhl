# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `FAC_CLS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | pdacc |
| 2 | Fixed asset name | fixed_asset_name | No | String | 250 |  |  | cacnm2 |
| 3 | Net book value | net_book_value | `Yes` | `Number` |  | 0 | số có hai số thập phân | camt3 |
| 4 | Book currency | book_currency | `Yes` | String | 3 |  |  | cccr2 |
| 5 | Accumulate posted | acummulate_posted | No | `Number` |  | 0 | số có hai số thập phân | camt |
| 6 | Book value | book_value | No | `Number` |  | 0 | số có hai số thập phân | camt4 |
| 7 | Customer id | customer_id | No | String | 15 |  |  | cctmcd |
| 8 | Customer name | customer_name | No | String | 250 |  |  | cctma |
| 9 | Customer address | customer_address | No | String | 250 |  |  | cidplace |
| 10 | Customer description | customer_description | No | String | 250 |  |  | mdesc |
| 11 | Description | description | No | String | 250 |  |  | descs |
| 12 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | cvldt |
| 13 | Exchange rate | exchange_rate | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | pcsexr |
| 14 | Cash amount /BCY | cash_amount_bcy | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | pcscvt |
| 15 | Amount (Debit account/BCY) 1 | amount_cbkexr | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | cbkexr |
| 16 | Amount (Debit account/BCY) 2 | amount_ccvt | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | ccvt |
| 17 | Income or expend | income_or_expend | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | pcsamt |
| 18 | Cross rate | cross_rate | `Yes` | `Number` |  | 0 | số có 9 số thập phân, trường ẩn | crate |
| 19 | Amount in cash | amount_in_cash | `Yes` | `Number` |  | 0 | số có hai số thập phân, trường ẩn | camt2 |
| 20 | Cash currency | cash_currency | `Yes` | String | 3 |  | trường ẩn | cccr |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String | 16 |  |
| 3 | Fixed asset name | fixed_asset_name | String |  |  |
| 4 | Net book value | net_book_value | `Number` |  | số có hai số thập phân |
| 5 | Book currency | book_currency | String | 3 |  |
| 6 | Accumulate posted | acummulate_posted | `Number` |  | số có hai số thập phân |
| 7 | Book value | book_value | `Number` |  | số có hai số thập phân |
| 8 | Customer id | customer_id | String | 8 |  |
| 9 | Customer name | customer_name | String |  |  |
| 10 | Customer address | customer_address | String |  |  |
| 11 | Customer description | customer_description | String |  |  |
| 12 | Description | description | String |  |  |
| 13 | Value date | value_date | `Date time` |  |  |
| 14 | Exchange rate | exchange_rate | `Number` |  |  |
| 15 | Cash amount /BCY | cash_amount_bcy | `Number` |  |  |
| 16 | Amount (Debit account/BCY) 1 | amount_cbkexr | `Number` |  |  |
| 17 | Amount (Debit account/BCY) 2 | amount_ccvt | `Number` |  |  |
| 18 | Income or expend | income_or_expend | `Number` |  |  |
| 19 | Cross rate | cross_rate | `Number` |  |  |
| 20 | Amount in cash | amount_in_cash | `Number` |  |  |
| 21 | Cash currency | cash_currency | String | 3 |  |
| 22 | Transaction status | status | String |  |  |
| 23 | Transaction date | transaction_date | `Date time` |  |  |
| 24 | User id | user_id | `Number` |  |  |
| 25 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |

## 1.2 Transaction flow
**Conditions:**
-	Asset code: exists with status = "Normal".

**Flow of events:**
-	Transaction complete:
    +	Status: Close
    +	Book amount = 0
    +	Net book amount = 0

**Posting:**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | EXPENSE | Net book value | Book currency code of fixed asset |
| 1 | D | ACCUMULATE | Accumulate posted | Book currency code of fixed asset |
| 1 | C | FASSET | Book value | Book currency code of fixed asset |

**Voucher:**
- `A203`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_SIG_PDACC |  | Không dùng |
| 2 | GET_INFO_PDACC | FAC_RF_SETINFO_ACCOUNT | [FAC_RF_SETINFO_ACCOUNT](Specification/Common/19 Fixed Asset Rulefuncs) |  | `expense_amount` = `acummulate_posted` |
| 3 | GET_INFO_CRATE | FX_RULEFUNC_GET_INFO_CRATE_TB_TA | [FX_RULEFUNC_GET_INFO_CRATE_TB_TA](Specification/Common/20 FX Rulefuncs) |
| 4 | GET_INFO_PCSEXR | FAC_GET_INFO_PCSEXR_TB | [FAC_GET_INFO_PCSEXR_TB](Specification/Common/19 Fixed Asset Rulefuncs) |
| 5 | GET_INFO_CBKEXR | FAC_GET_INFO_PCSEXR | [FAC_GET_INFO_PCSEXR](Specification/Common/19 Fixed Asset Rulefuncs) |

# 5. Signature
- None: Phân hệ FAC không có customer nên sẽ không có chức năng signature