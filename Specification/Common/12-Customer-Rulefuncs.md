# 1. Customer module
## Trả về full_name khi nhận customer_code và customer_type
- **Workflow id:** `CTM_GET_INFO_FULLNAME`

- **Câu query của O9:** SELECT O9SYS.O9COM.GET_ALL_FULLNAME(O9SYS.O9CTM.SET_CUSTOMERCD_CUSTOMERID('@CCTMCD','@CCTMT'),'@CCTMT') FROM DUAL

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | No | String |  |  |  |
| 2 | Customer type | customer_type | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Customer code | customer_code | String |  |  |
| 2 | Full name | fullname | String |  |  |
| 3 | Customer id | id | `Number` |  | `thêm mới` |
| 4 | Type | customer_private | String |  | `trả thêm` |
| 5 | Date of birth | date_of_birth | `Date time` |  | `trả thêm` |
| 6 | Paper type | paper_type | String |  | `trả thêm` |
| 7 | Paper number | paper_number | String |  | `trả thêm` |
| 8 | Phone mobile | phone_mobile | String |  | `trả thêm` |
| 9 | Address | address | String |  | Nối chuỗi các sub string: `address_local, village, sub_district, district, province` của `contact_local_address` |
| 10 | Country of income | country_of_income | String |  | `trả thêm` |
| 11 | Nationality | nation | String |  | `trả thêm` |

## Trả về danh sách Customer khi nhận "Customer type"
- **Workflow id:** `CTM_LOOKUP_CTM_BY_CTMTYPE`

- **Query in O9:** 
```sql
select CUSTOMERCD,FULLNAME 
from o9sys.v_customeracib 
where CTMTYPE='@CCTMT'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer type | customer_type | No | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 2 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 3 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String |  |  |
| 2 | Full name | fullname | String |  |  |
| 3 | Customer id | id | `Number` |  |  |

## Trả về thông tin Customer single khi nhận "Customer code"
- **Workflow id:** `CTM_GET_INFO_RULEFUNC`

- **Query in O9:** 
```sql
SELECT A.CCRCD ||'-'|| A.FULLNAME,
A.CTMSTS,
fullname,
ad.maddress,
SECTOR,
CATEGORIES 
FROM o9data.D_CUSTOMER A 
inner join o9sys.v_ctmaddr ad on a.customerid=ad.customerid
WHERE A.CUSTOMERCD = '@CCTMCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | No | String |  |  | `CCTMCD` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency full name | currency_code_and_name | String |  | `A.CCRCD-A.FULLNAME` tương ứng `currency_code-full_name` |
| 2 | Customer status | customer_status | String |  |  |
| 3 | Customer code | customer_code | String |  |  |
| 4 | Full name | fullname | String |  |  |
| 5 | Currency code | currency_code | String | 3 |  |
| 6 | Type | customer_private | String |  |  |
| 7 | Date of birth | date_of_birth | `Date time` |  |  |
| 8 | Paper type | paper_type | String |  |  |
| 9 | Paper number | paper_number | String |  |  |
| 10 | Address | address | String |  | Nối chuỗi các sub string: `address_local, village, sub_district, district, province` của `contact_local_address` |
| 11 | Economic sector | sector | String |  |  |
| 12 | Customer category | categories | String |  |  |
| 13 | Create user | staff_code | String |  |  |

## Trả về thông tin media khi nhận "Account number" và "Module"
- **Workflow id:** `GET_MEDIA_SIG_ACNO`

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |
| 2 | Module | module | No | String | 3 |  | DPT, CRD, MTG, `CTM` |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |
| 4 | Page index | page_index | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results media | items | Array Object |  |  |
| 1 | Media file id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Media name | media_name | String |  |  |
| 4 | Media type | media_type | String |  |  |
| 5 | Media data | media_data | String |  |  |
| 6 | Expire date | expire_date | `Date time` |  |  |
| 7 | Other | other | String |  |  |
| 8 | Media status | media_status | String |  |  |
| 9 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |