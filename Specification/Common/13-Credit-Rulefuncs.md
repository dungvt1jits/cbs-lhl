# 1. Credit module
## Trả về thông tin "Sub product limit" và Customer khi nhận "Sub product code"
- **Workflow id:** `CRD_GET_INFO_SPL`

- **Query in O9:** 
```sql
SELECT A.SPLNM,
       A.CCRCD||' - '||A.SPLNM as TXB_SPLCD,
       A.CTMTYPE CCTMT,
       A.CUSTOMERID,
       O9CTM.SET_CUSTOMERID_CUSTOMERCD (A.CUSTOMERID, A.CTMTYPE) CUSTOMERCD,
       O9SYS.O9COM.GET_ALL_FULLNAME (A.CUSTOMERID, A.CTMTYPE) FULLNAME TXB_CCTMCD,
       A.REFID CRNUM,
       A.CCRCD CACCCR,
       A.LMAMT TXAMT,
       A.ALAMT,
       A.SPLSTS CSTS,
       A.PLCD PLCD
  FROM O9DATA.D_CRDSPL A
 WHERE A.SPLCD = '@SPLCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Sub product limit code | sub_product_limit_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency sub product limit code | currency_sub_product_limit_code | String |  | `A.CCRCD-A.SPLNM` |
| 2 | Sub product name | sub_product_limit_name | String |  | `SPLNM` |
| 3 | Customer type | customer_type | String |  | `CTMTYPE` |
| 4 | Customer id | customer_id | `Number` |  | `CUSTOMERID` |
| 5 | Product code | product_limit_code | String |  | `PLCD` |
| 6 | Reference code | reference_number | String |  | `REFID` |
| 7 | Currency | currency_code | String |  | `CCRCD` |
| 8 | Amount | limit_amount | `Number` |  | số có hai số thập phân, `LMAMT` |
| 9 | Available amount | available_amount | `Number` |  | số có hai số thập phân, `ALAMT` |
| 10 | Status | sub_product_status | String |  | SPLSTS |
| 11 | Customer code | customer_code | String |  | `CUSTOMERCD` |
| 12 | Full name | full_name | String |  | `FULLNAME` |

## Trả về thông tin product limit và Customer khi nhận "Product limit code"
- **Workflow id:** `CRD_GET_INFO_PL`

- **Query in O9:** 
```sql
SELECT A.PLNM,
A.CCRCD||' - '||A.PLNM as TXB_PLCD,
A.CCRCD,
A.LMTP, 
A.REFID,
A.LMAMT,
A.ALAMT,
A.PLSTS,
A.CTMTYPE,
A.CUSTOMERID,
O9SYS.O9COM.GET_ALL_FULLNAME(A.CUSTOMERID,A.CTMTYPE) FULLNAME,
O9CTM.SET_CUSTOMERID_CUSTOMERCD(A.CUSTOMERID,A.CTMTYPE) CUSTOMERCD 
FROM O9DATA.D_CRDPL A WHERE A.PLCD='@PLCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Product limit code | product_limit_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Product name | product_limit_name | String |  | `PLNM` |
| 2 | Reference code | reference_number | String |  | `REFID` |
| 3 | Currency | currency_code | String |  | `CCRCD` |
| 4 | Amount | limit_amount | `Number` |  | số có hai số thập phân, `LMAMT` |
| 5 | Available amount | available_amount | `Number` |  | số có hai số thập phân, `ALAMT` |
| 6 | Limit type | limit_type | String |  | `LMTP` |
| 7 | Status | product_status | String |  | `PLSTS` |
| 8 | Currency product limit name | currency_product_limit_name | String |  | `TXB_PLCD` |
| 9 | Customer type | customer_type | String |  | `CTMTYPE` |
| 10 | Customer id | customer_id | `Number` |  | `CUSTOMERID` |
| 11 | Customer code | customer_code | String |  | `CUSTOMERCD` |
| 12 | Full name | full_name | String |  | `O9SYS.O9COM.GET_ALL_FULLNAME(A.CUSTOMERID,A.CTMTYPE) FULLNAME` |

## Trả về danh sách "Credit catalog" với CRDFACILITY ='OD' khi nhận "Currency code"
- **Workflow id:** `CRD_LOOKUP_CRDCAT_OD`

- **Query in O9:** 
```sql
SELECT CATCD,CATNAME 
FROM O9DATA.D_CRDCAT  
WHERE CCRCD = '@CCCR' 
AND CATSTS = 'N' 
AND CRDFACILITY ='OD' ORDER BY CATCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency code | currency_code | No | String |  |  |  |
| 2 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 3 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String |  |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Catalogue id | id | `Number` |  |  |

## Trả về danh sách "Product limit" khi nhận "Customer code" và "Customer type"
- **Workflow id:** `CRD_LOOKUP_CRDPL_BY_CTM`

- **Query in O9:** 
```sql
SELECT A.PLCD,A.PLNM 
FROM O9DATA.D_CRDPL A 
WHERE A.CTMTYPE = '@CCTMT' 
AND A.CUSTOMERID=O9SYS.O9CTM.SET_CUSTOMERCD_CUSTOMERID('@CCTMCD','@CCTMT') 
AND PLSTS ='N'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | No | String |  |  |  |
| 2 | Customer type | customer_type | No | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer 
| 3 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 4 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Product limit code | product_limit_code | String |  |  |
| 2 | Product name | product_limit_name | String |  |  |

## Trả về danh sách "Sub product limit" với faci='OD' khi nhận "Customer code" và "Customer type"
- **Workflow id:** `CRD_LOOKUP_CRDSPL_OD_BY_CTM`

- **Query in O9:** 
```sql
SELECT a.splcd, 
a.splnm    
FROM o9data.d_crdspl a         
INNER JOIN o9data.d_customer b         
ON a.customerid = b.customerid 
where a.ctmtype='@CCTMT' 
and b.customercd='@CCTMCD' 
and a.faci='OD' 
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | No | String |  |  |  |
| 2 | Customer type | customer_type | No | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer 
| 3 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 4 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Sub product limit code | sub_product_limit_code | String |  |  |
| 2 | Sub product name | sub_product_limit_name | String |  |  |

## Trả về thông tin adjustment khi nhận "IFC code", "Adjustment amount" và "Account number" của Credit
- **Workflow id:** `CRD_IFC_GET_INFO_ADJUSTMENT`

- **Query in O9:** 
```sql
SELECT CASE WHEN amt > 0 THEN amt ELSE - (ABS (amt) - LEAST (ABS (amt), ROUND (intdue, 2))) END
           intamt,
       amt intpbl,
       CASE WHEN amt > 0 THEN 0 ELSE -LEAST (ABS (amt), ROUND (intdue, 2)) END intdue,
       CASE
           WHEN amt + on_bal_int < 0 THEN -on_bal_int
           ELSE CASE WHEN idx IN (1, 2) THEN amt ELSE 0 END
       END
           pay_on_int,
       CASE
           WHEN amt + on_bal_int < 0 THEN amt + on_bal_int
           ELSE CASE WHEN idx NOT IN (1, 2) THEN amt ELSE 0 END
       END
           pay_off_int
  FROM (SELECT '@TXAMT' amt,
               intdue,
               intamt,
               intpre,
               intpaid,
               intpbl,
               intspamt,
               b.acno,
               o9sys.o9crd.get_on_bal_interest (b.acno) on_bal_int,
               o9sys.o9crd.get_off_bal_interest (b.acno) off_bal_int,
               1+o9sys.o9crd.get_npl_index (o9sys.o9crd.set_acno_defacno (b.acno)) idx
          FROM o9data.d_ifcbal a INNER JOIN o9data.d_credit b ON a.defacno = b.defacno
         WHERE b.acno = '@PCACC' AND a.ifccd = '@CIFCCD')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Adjustment amount | adjustment_amount | No | `Number` |  |  |  |
| 2 | Account number | account_number | No | String |  |  | account number của credit |
| 3 | IFC code | ifc_code | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Adjusted accrual interest | adjusted_accrual_interest | `Number` |  | số có 2 số thập phân, `cintamt` |
| 2 | Adjusted payable interest | adjusted_payable_interest | `Number` |  | số có 2 số thập phân, `cintpbl` |
| 3 | Adjusted due interest | adjusted_due_interest | `Number` |  | số có 2 số thập phân, `cintdue` |
| 4 | Adjusted on-balance sheet interest | adjusted_on_balance_sheet_interest | `Number` |  | số có 2 số thập phân, `conint` |
| 5 | Adjusted off-balance sheet interest | adjusted_off_balance_sheet_interest | `Number` |  | số có 2 số thập phân, `coffint` |

## Trả về thông tin Credit account khác Overdraft (OD) và Customer khi nhận "Account number" của Credit
- **Workflow id:** `CRD_CLS_GET_INFO_CACNM`

- **Query in O9:** 
```sql
SELECT 
O9SYS.O9COM.GET_ALL_FULLNAME(D_CREDIT.CUSTOMERID,D_CREDIT.CTMTYPE) ACNAME,
V_CUSTOMERINFO.CUSTOMERCD,
V_CUSTOMERINFO.ADDRESS,
O9SYS.O9COM.GET_ALL_FULLNAME(D_CREDIT.CUSTOMERID,D_CREDIT.CTMTYPE)  
FROM D_CREDIT, O9SYS.V_CUSTOMERINFO, D_CRDCAT  
WHERE D_CREDIT.CUSTOMERID= V_CUSTOMERINFO.CUSTOMERID 
AND V_CUSTOMERINFO.CTMTYPE = D_CREDIT.CTMTYPE 
AND D_CREDIT.CATID = D_CRDCAT.CATID 
AND D_CREDIT.ACNO = '@PCACC' 
AND D_CREDIT.CRDFACILITY <> 'OD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Customer code | customer_code | String |  | `customercd` |
| 4 | Full name | full_name | String |  | `O9SYS.O9COM.GET_ALL_FULLNAME(a.CUSTOMERID,a.CTMTYPE) FULLNAME` |
| 5 | Address | address | String |  |  |

## Trả về thông tin "Catalogue" khi nhận "Catalogue code" của Credit
- **Workflow id:** `GET_INFO_CRDCAT`

- **Query in O9:** 
```sql
SELECT CATCD,CATNAME,CCRCD,CRDFACILITY,CRDCLS,TNTYPE 
FROM D_CRDCAT WHERE CATCD = '@CNCAT' ORDER BY CATCD 
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue code | catalog_code | String |  |  |
| 2 | Catalogue name | catalog_name | String |  | `CATNAME` |
| 3 | Currency code | currency_code | String |  | `CCRCD` |
| 4 | Credit classification | credit_classification | String |  | `CRDCLS` |
| 5 | Credit facility | credit_facility | String |  | `CRDFACILITY` |
| 6 | Tenor type | tenor_type | String |  | `TNTYPE` |

## Trả về thông tin "Sub product limit"trong giao dịch CRD_OPN khi nhận "Sub product code" và "Catalogue code"
- **Workflow id:** `CRD_OPN_GET_INFO_SPL`

- **Query in O9:** 
```sql
select 
a.ALAMT*o9sys.o9util.get_bcyrate (a.ccrcd, 'BK', branchid) / o9sys.o9util.get_bcyrate (b.ccrcd, 'BK', branchid), a.splnm, 
a.ccrcd   
from o9data.d_crdspl a, o9data.d_crdcat b 
where a.SPLSTS='N' 
AND a.splcd='@SPL' 
and a.faci<>'OD' 
and b.catcd='@CNCAT'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Sub product limit code | sub_product_limit_code | No | String |  |  |  |
| 2 | Catalogue code | catalog_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Sub product name | sub_product_limit_name | String |  | `SPLNM` |
| 2 | Currency | currency_code | String |  | `CCRCD` |
| 3 | Amount in currency catalog | amount_in_currency_catalog | `Number` |  | `a.ALAMT*o9sys.o9util.get_bcyrate (a.ccrcd, 'BK', branchid) / o9sys.o9util.get_bcyrate (b.ccrcd, 'BK', branchid)` |

## Trả về thông tin "Sub product limit"trong giao dịch DPT_ODO khi nhận "Sub product code" và "Catalogue code"
- **Workflow id:** `DPT_ODO_GET_INFO_SPL`

- **Query in O9:** 
```sql
select 
a.lmamt*o9sys.o9util.get_bcyrate (a.ccrcd, 'BK', branchid) / o9sys.o9util.get_bcyrate (b.ccrcd, 'BK', branchid), a.splnm, 
a.ccrcd,
b.tntype
from o9data.d_crdspl a, o9data.d_crdcat b 
where a.splcd='@SPL' 
and  b.catcd='@CNCAT'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Sub product limit code | sub_product_limit_code | No | String |  |  |  |
| 2 | Catalogue code | catalog_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Sub product name | sub_product_limit_name | String |  | `splnm` |
| 2 | Currency | currency_code | String |  | `ccrcd` |
| 3 | Amount in currency catalog | amount_in_currency_catalog | `Number` |  | `a.lmamt*o9sys.o9util.get_bcyrate (a.ccrcd, 'BK', branchid) / o9sys.o9util.get_bcyrate (b.ccrcd, 'BK', branchid)` |
| 4 | Tenor type | tenor_type | String |  | `tntype` |

## Trả về thông tin "Credit account" và "Provision" khi nhận "Account number" của Credit
- **Workflow id:** `CRD_GET_INFO_BCY_CACNM`

- **Query in O9:** 
```sql
SELECT a.ccrcd, 
       o9sys.o9com.get_all_fullname (a.customerid, a.ctmtype) fullname, 
       a.balance prin, 
       a.intpbl cipbl, 
       c.cdidx g1, 
       PPAMT prov_p1, 
       IPAMT prov_i1,
	   ROUND ( 
            o9sys.o9crd.get_provision_principal_rate(PACNO=>a.acno, PGROUP=>TO_CHAR (c.cdidx-1)) 
           * a.balance 
           / 100, 
           2) 
           prov_p2,
       ROUND ( 
             o9sys.o9crd.get_provision_interest_rate(PACNO=>a.acno, PGROUP=>TO_CHAR (c.cdidx-1)) 
           * a.intspamt 
           / 100, 
           2) 
           prov_i2,
        ROUND ( 
            o9sys.o9crd.get_provision_interest_rate(PACNO=>a.acno, PGROUP=>TO_CHAR (c.cdidx-1))                     
           * a.intspamt 
           / 100, 
           2)  -  IPAMT as CAMT, 
           a.intspamt
  FROM o9data.d_credit a 
       INNER JOIN o9sys.v_customerinfo b 
           ON (a.customerid = b.customerid AND b.ctmtype = a.ctmtype) 
       INNER JOIN (SELECT * 
                     FROM o9code.c_cdlist 
                    WHERE cdgrp = 'CRD' AND cdname = 'CLSTS') c 
           ON a.clsts = c.cdid 
 WHERE a.acno = '@PCACC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency code | currency_code | String |  | `ccrcd` |
| 2 | Full name | full_name | String |  | `fullname` |
| 3 | Outstanding balance | balance | `Number` |  | số thập phân, `prin` |
| 4 | Interest receivable | interest_receivable | `Number` |  | số thập phân, `cipbl` |
| 5 | Group classification01 | group_classification01 | `Number` |  | số nguyên dương, `g1` |
| 6 | Provision of principal01 | provision_principal01 | `Number` |  | số thập phân, `prov_p1` |
| 7 | Provision of interest01 | provision_interest01 | `Number` |  | số thập phân, `prov_i1` |
| 8 | Provision of principal02 | provision_principal02 | `Number` |  | số thập phân, `prov_p2` |
| 9 | Provision of interest02 | provision_interest02 | `Number` |  | số thập phân, `prov_i2` |
| 10 | Adjust provision of interest | adjust_provision_interest | `Number` |  | số thập phân, `CAMT` |
| 11 | On-balance sheet interest | on_balance_sheet_interest | `Number` |  | số thập phân, `intspamt` |

## Trả về thông tin "Provision" khi nhận "Account number" của Credit và Group
- **Workflow id:** `CRD_NPL_GET_INFO_G2`

- **Query in O9:** 
```sql
SELECT PPAMT prov_p1, 
       ROUND ( 
          o9sys.o9crd.get_provision_principal_rate(PACNO=>a.acno, PGROUP=>TO_CHAR ('@G2'-1)) 
           * a.balance 
           / 100, 
           2) 
           prov_p2, 
      IPAMT prov_i1,                   
       ROUND ( 
             o9sys.o9crd.get_provision_interest_rate(PACNO=>a.acno, PGROUP=>TO_CHAR ('@G2'-1)) 
           * a.intspamt 
           / 100, 
           2) 
           prov_i2            
  FROM o9data.d_credit a 
 WHERE a.acno = '@PCACC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |
| 2 | Group classification02 | group_classification02 | No | `Number` |  |  | `G2` |


- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Provision of principal01 | provision_principal01 | `Number` |  | số thập phân, `prov_p1` |
| 2 | Provision of interest01 | provision_interest01 | `Number` |  | số thập phân, `prov_i1` |
| 3 | Provision of principal02 | provision_principal02 | `Number` |  | số thập phân, `prov_p2` |
| 4 | Provision of interest02 | provision_interest02 | `Number` |  | số thập phân, `prov_i2` |

## Trả về danh sách "Credit catalog" khi nhận "Sub product limit code"
- **Workflow id:** `CRD_LOOKUP_CRDCAT_BY_SPL`

- **Query in O9:** 
```sql
SELECT a.catcd, a.catname      
FROM o9data.d_crdcat a           
INNER JOIN o9data.d_crdspl b 
ON a.crdfacility = b.faci           
INNER JOIN o9data.d_crdpl c 
ON b.plcd = c.plcd     
WHERE     a.catsts <> 'C'           
AND a.crdfacility <> 'OD'           
AND b.splsts = 'N'           
AND ( (c.lmtp = 'M' AND a.crdtype = 'B') OR (c.lmtp = 'F' AND a.crdtype = 'F'))  ORDER BY catcd
AND b.splcd = '@SPL'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Sub product limit code | sub_product_limit_code | No | String |  |  |  |
| 2 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 3 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String |  |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Catalogue id | id | `Number` |  |  |

## Trả về danh sách "Sub product limit" khi nhận "Customer code" và "Customer type"
- **Workflow id:** `CRD_LOOKUP_CRDSPL_BY_CTM`

- **Query in O9:** 
```sql
SELECT a.splcd, a.splnm 
FROM o9data.d_crdspl a 
INNER JOIN o9sys.v_customerinfo b 
ON a.customerid = b.customerid 
AND a.ctmtype = b.ctmtype 
WHERE a.SPLSTS='N'
AND a.faci <> 'OD'
AND a.ctmtype = '@CCTMT' 
AND b.customercd = '@CCTMCD' 
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | No | String |  |  |  |
| 2 | Customer type | customer_type | No | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer 
| 3 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 4 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Sub product limit code | sub_product_limit_code | String |  |  |
| 2 | Sub product name | sub_product_limit_name | String |  |  |

## Trả về thông tin "Amount" và "Customer" khi nhận "Account number" của Credit
- **Workflow id:** `CRD_GET_INFO_CRDACC`

- **Query in O9:** 
```sql
SELECT a.ccrcd,
       a.acname,
       a.ccrcd ||'-'|| a.acname,
       o9sys.o9com.get_all_fullname (a.customerid, a.ctmtype) fullname,
       b.address,
       b.customercd,
       b.repidtype,
       a.crdsts,
       a.ctmtype,
       a.crlimit,
       a.balance,
       ROUND (a.intpbl, 2) intpbl, 
       ROUND (a.intamt, 2) intamt,
       ROUND (a.intdue, 2) intdue,
       ROUND (a.intspamt, 2) on_bal_int,
       ROUND (a.intamt + a.intdue, 2) totalint,
       ROUND (a.intamt + a.intdue, 2) totalpay,
       ROUND (a.intamt + a.intdue, 2) - ROUND (a.intspamt, 2) off_bal_int, 
       (ROUND (a.intamt + a.intdue, 2) + a.balance) AMT, 
       o9sys.o9crd.cal_due_principal(a.acno)
FROM d_credit a, o9sys.v_customerinfo b
WHERE a.customerid = b.customerid
       AND b.ctmtype = a.ctmtype
       AND a.acno = '@PCACC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Account name | account_name | String |  | `a.acname` |
| 3 | Currency account name | currency_account_name | String |  | `a.ccrcd-a.acname` |
| 4 | Currency code | currency_code | String |  | `a.ccrcd` |
| 5 | Credit status | credit_status | `Number` |  | `a.crdsts` |
| 6 | Account holder type | customer_type | String |  | `a.ctmtype` |
| 7 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân, `a.crlimit` |
| 8 | Outstanding balance | balance | `Number` |  | số có hai số thập phân, `a.balance` |
| 9 | Interest receivable | interest_receivable | `Number` |  | số có hai số thập phân, `ROUND (a.intpbl, 2) intpbl` |
| 10 | Interest accrual amount | interest_amount | `Number` |  | số có hai số thập phân, `ROUND (a.intamt, 2) intamt` |
| 11 | Interest due | interest_due | `Number` |  | số có hai số thập phân, `ROUND (a.intdue, 2) intdue` |
| 12 | On-balance sheet interest | on_balance_sheet_interest | `Number` |  | số có hai số thập phân, `ROUND (a.intspamt, 2) on_bal_int` |
| 13 | Total interest | total_interest | `Number` |  | số có hai số thập phân, `ROUND (a.intamt + a.intdue, 2) totalint` |
| 14 | Total pay | total_pay | `Number` |  | số có hai số thập phân, `ROUND (a.intamt + a.intdue, 2) totalpay` |
| 15 | Off-balance sheet interest | off_balance_sheet_interest | `Number` |  | số có hai số thập phân, `ROUND (a.intamt + a.intdue, 2) - ROUND (a.intspamt, 2) off_bal_int` |
| 16 | Amount | amount | `Number` |  | số có hai số thập phân, `(ROUND (a.intamt + a.intdue, 2) + a.balance) AMT` |
| 17 | Due principal | due_principal | `Number` |  | số có hai số thập phân, `o9sys.o9crd.cal_due_principal(a.acno)` |
| 18 | Customer code | customer_code | String |  | `b.customercd` |
| 19 | Full name | full_name | String |  | `fullname` |
| 20 | Address | address | String |  | `b.address` |
| 21 | Paper type | paper_type | String |  | `b.repidtype` |
| 22 | Paper number | paper_number | String |  |  |

## Trả về thông tin "Interest" khi nhận "Account number" và "Interest amount" của Credit
- **Workflow id:** `CRD_GET_INFO_INTEREST`

- **Query in O9:** 
```sql
SELECT intamt,
       intpbl,
       intovd,
       intdue,
       (amt - intdue - intamt) intpre,
       pay_on_bal,
       pay_off_bal
  FROM (SELECT amt,
               LEAST (amt - intdue, intamt) intamt,
               intpbl,
               intovd,
               intdue,
               pay_on_bal,
               GREATEST(0, amt - pay_on_bal) pay_off_bal
          FROM (SELECT amt,
                       intamt,
                       intpbl,
                       intovd,
                       LEAST (amt - intovd, intdue) intdue,
                       totalint,
                       LEAST (amt, on_bal_int) pay_on_bal,
                       off_bal_int
                  FROM (SELECT amt,
                               LEAST (intamt, amt) intamt,
                               LEAST (intpbl, amt) intpbl,
                               LEAST (amt, intovd) intovd,
                               intdue,
                               intamt totalint,
                               on_bal_int,
                               (all_int - on_bal_int) off_bal_int
                          FROM (SELECT '@INT' amt,
                                       ROUND (intamt, 2) intamt,
                                       ROUND (intpbl, 2) intpbl,
                                       ROUND (intovd, 2) intovd,
                                       ROUND (intdue, 2) intdue,
                                       ROUND (intspamt, 2) on_bal_int,
                                       ROUND (intdue + intamt, 2) all_int
                                  FROM o9data.d_credit
                                 WHERE acno = '@PCACC'))))
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |
| 2 | Interest amount | interest_amount | No | `Number` |  |  | interest amount của Credit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Interest accrual amount | interest_amount | `Number` |  | số có hai số thập phân, `intamt` |
| 2 | Interest receivable | interest_receivable | `Number` |  | số có hai số thập phân, `intpbl` |
| 3 | Interest overdue | interest_overdue | `Number` |  | số thập phân, `intovd` |
| 4 | Interest due | interest_due | `Number` |  | số có hai số thập phân, `intdue` |
| 5 | Interest prepaid | interest_prepaid | `Number` |  | số có hai số thập phân, `intpre` |
| 6 | Pay on-balance sheet interest | pay_on_balance_sheet_interest | `Number` |  | số có hai số thập phân, `pay_on_bal` |
| 7 | Pay off-balance sheet interest | pay_off_balance_sheet_interest | `Number` |  | số có hai số thập phân, `pay_off_bal` |

## Trả về thông tin Credit account, Product limit code, Sub product limit code và customer khi nhận "Account number" của Credit
- **Workflow id:** `CRD_CLA_GET_INFO_CACNM`

- **Query in O9:** 
```sql
SELECT 
a.CCRCD,
a.CCRCD ||'-'|| O9SYS.O9COM.GET_ALL_FULLNAME(a.CUSTOMERID,a.CTMTYPE) FULLNAME,
a.CRLIMIT,
a.LFRDT,
a.LTODT,
a.SPLCD,
b.PLCD,
decode(a.ctmtype, 'C', cus.customercd, 'L', cus_lkg.customercd, cus_grp.customercd)
FROM O9DATA.D_CREDIT a inner join o9data.d_crdspl b on a.splcd=b.splcd
    left join o9data.d_customer cus on a.ctmtype='C' and a.customerid=cus.customerid
    left join o9data.d_ctmlkg lkg on a.customerid=cus.customerid
    left join o9data.d_customer cus_lkg on lkg.mcustomerid= cus_lkg.customerid 
    left join o9data.d_ctmgrp grp on a.customerid=grp.grpid
    left join o9data.d_customer cus_grp on lkg.mcustomerid= cus_grp.customerid 
WHERE ACNO = '@PCACC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account name | account_name | String |  |  |
| 2 | Currency code | currency_code | String |  | `CCRCD` |
| 3 | Currency full name | currency_full_name | String |  | `a.CCRCD'-O9SYS.O9COM.GET_ALL_FULLNAME(a.CUSTOMERID,a.CTMTYPE) FULLNAME` |
| 4 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân, `CRLIMIT` |
| 5 | Limit effective from | limit_effective_from | `Date time` |  | `LFRDT` |
| 6 | Limit effective to | limit_effective_to | `Date time` |  | `LTODT` |
| 7 | Sub product limit code | sub_product_limit_code | String |  | `SPLCD` |
| 8 | Product limit code | product_limit_code | String |  | `PLCD` |
| 9 | Customer code | customer_code | String |  | `customercd` |
| 10 | Customer type | customer_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer, `ctmtype` |

## Trả về thông tin "Catalog", "Account" và "Customer" khi nhận "Account number" của Credit
- **Workflow id:** `CRD_GET_INFO_CUSTOMER`

- **Query in O9:** 
```sql
SELECT O9SYS.O9COM.GET_ALL_FULLNAME(D_CREDIT.CUSTOMERID,D_CREDIT.CTMTYPE) FULLNAME,
V_CUSTOMERINFO.CUSTOMERCD,
V_CUSTOMERINFO.ADDRESS,
V_CUSTOMERINFO.REPIDTYPE,
O9SYS.O9COM.GET_NRC('@PCACC',V_CUSTOMERINFO.REPIDTYPE) REPID,
D_CREDIT.CCRCD||' - '||D_CREDIT.ACNAME,
D_CREDIT.ACNAME,
D_CREDIT.TODT,
D_CREDIT.CRLIMIT,
D_CREDIT.BALANCE,
D_CREDIT.PPAMT,
D_CREDIT.INTPBL,
D_CREDIT.INTAMT,
D_CREDIT.INTDUE,
D_CREDIT.INTOVD,
D_CREDIT.CCRCD CCRCD,
D_CREDIT.CTMTYPE,
D_CREDIT.SPLCD SPLCD,
D_CREDIT.CRDSTS,
D_CRDCAT.CATCD CATCD,
D_CRDCAT.CATNAME CATNAME
FROM D_CREDIT, O9SYS.V_CUSTOMERINFO, D_CRDCAT
WHERE D_CREDIT.CUSTOMERID= V_CUSTOMERINFO.CUSTOMERID 
AND V_CUSTOMERINFO.CTMTYPE = D_CREDIT.CTMTYPE 
AND D_CREDIT.CATID = D_CRDCAT.CATID 
AND D_CREDIT.ACNO = '@PCACC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account name | account_name | String |  |  |
| 2 | Currency account name | currency_account_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |
| 4 | End of tenor | to_date | `Date time` |  |  |
| 5 | Credit limit | credit_limit | `Number` |  | số thập phân |
| 6 | Outstanding balance | balance | `Number` |  | số thập phân |
| 7 | Provision of principal | provision_principal | `Number` |  | số thập phân, `PPAMT` |
| 8 | Interest receivable | interest_receivable | `Number` |  | số thập phân, `INTPBL` |
| 9 | Interest accrual amount | interest_amount | `Number` |  | số thập phân, `INTAMT` |
| 10 | Interest due | interest_due | `Number` |  | số thập phân, `INTDUE` |
| 11 | Interest overdue | interest_overdue | `Number` |  | số thập phân, `INTOVD` |
| 12 | Sub product limit code | sub_product_limit_code | String |  |  |
| 13 | Account holder type | customer_type | String |  |  |
| 14 | Credit status | credit_status | `Number` |  | số nguyên dương, `CRDSTS` |
| 15 | Catalogue code | catalog_code | String |  |  |
| 16 | Catalogue name | catalog_name | String |  |  |
| 17 | Customer code | customer_code | String |  |  |
| 18 | Full name | full_name | String |  |  |
| 19 | Address | address | String |  |  |
| 20 | Paper type | paper_type | String |  |  |
| 21 | Paper number | paper_number | String |  |  |

## Trả về thông tin Customer và branch khi nhận "Customer code", "Customer type" và "Branch id"
- **Workflow id:** `CRD_OPN_GET_INFO_CACNM`

- **Query in O9:** 
```sql
SELECT o9sys.o9com.get_all_fullname (o9sys.o9ctm.set_customercd_customerid ('@CCTMCD', '@CCTMT'), '@CCTMT'),
       CASE
           WHEN '@CRRBRID' = to_number(o9sys.o9util.getsparam ('BANK', 'HOST_BRANCH'))
           THEN
               o9sys.o9com.get_managing_branch_code (o9sys.o9ctm.set_customercd_customerid ('@CCTMCD', '@CCTMT'), '@CCTMT')
           ELSE
               to_char(BRANCHCD)
       END 
       mngbrnch
  FROM o9sys.s_branch where branchid = '@CRRBRID'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | No | String |  |  |  |
| 2 | Customer type | customer_type | No | String |  |  |  |
| 3 | Branch id | branch_id | No | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Full name | fullname | String |  |  |
| 2 | Branch code | branch_code | String |  |  |

## Trả về thông tin "Principal" khi nhận "Account number" và "Principal amount" của Credit
- **Workflow id:** `CRD_GET_INFO_PRINCIPAL`

- **Query in O9:** 
```sql
SELECT LEAST (amt - (p2 + p3 + p4 + p5), p1) p1,
       p2,
       p3,
       p4,
       p5
  FROM (SELECT amt,
               p1,
               LEAST (amt - (p3 + p4 + p5), p2) p2,
               p3,
               p4,
               p5
          FROM (SELECT amt,
                       p1,
                       p2,
                       LEAST (amt - (p4 + p5), p3) p3,
                       p4,
                       p5
                  FROM (SELECT amt,
                               p1,
                               p2,
                               p3,
                               LEAST (amt - p5, p4) p4,
                               p5
                          FROM (SELECT amt,
                                       p1,
                                       p2,
                                       p3,
                                       p4,
                                       LEAST (amt, p5) p5
                                  FROM (SELECT '@PRIN' amt,
                                               o9crd.get_nor_amount (acno) p1,
                                               o9crd.get_ovd_amount (acno) p2,
                                               o9crd.get_npl_amount (acno) p3,
                                               o9crd.get_doubtful_amount (
                                                   acno)
                                                   p4,
                                               o9crd.get_loss_amount (acno)
                                                   p5
                                          FROM o9data.d_credit
                                         WHERE acno = '@PCACC')))))
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |
| 2 | Principal amount | principal_amount | No | `Number` |  |  | Principal amount của Credit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Normal amount | normal_amount | `Number` |  | số có hai số thập phân, `CAMT`, p1 |
| 2 | NPL amount | npl_amount | `Number` |  | số có hai số thập phân, `CAMT1`, p2 |
| 3 | Doubtful amount | doubtful_amount | `Number` |  | số có hai số thập phân, `CAMT2`, p3 |
| 4 | Overdue amount | overdue_amount | `Number` |  | số có hai số thập phân, `CAMT3`, p4 |
| 5 | Loss amount | loss_amount | `Number` |  | số có hai số thập phân, `CAMT4`, p5 |