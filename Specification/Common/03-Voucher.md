# Voucher A1
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |
| 1 | Code | code | String |  |  |  |
| 2 | Order | order | `Number` |  |  |  |
| 3 | Tags | tags | JSON Object |  |  |  |
| 3.1 | Transaction body | transaction_body | JSON Object |  |  |  |
| 3.1.1 | Branch name long | system_branch_name_long | String |  |  |  |  |
| 3.1.2 | Branch name | system_branch_name | String |  |  |  |  |
| 3.1.3 | Branch address | system_branch_address | String |  |  |  |  |
| 3.1.4 | Account customer | customer_code | String |  |  |  | sysaccc |
| 3.1.5 | Account number | account_number | String |  |  |  | sysaacc |
| 3.1.6 | Account name | system_account_name | String |  |  |  |  |
| 3.1.7 | Description | description | String |  |  |  |  |
| 3.1.8 | Teller name | system_teller_name | String |  |  |  |  |
| 3.1.9 | Approve name | system_approve_name | String |  |  |  |  |
| 3.2 | Transaction date | transaction_date | `Date time` |  |  |  |  |
| 3.3 | Transaction ref id | transaction_refid | String |  |  |  |  |
| 3.4 | Postings | postings | JSON Array Object |  |  |  |  |
| 3.4.1 | Bank account number | bank_account_number | String |  |  |  |  |
| 3.4.2 | Account name | account_name | String |  |  |  |  |
| 3.4.3 | Currency code | currency_code | String |  |  |  |  |
| 3.4.4 | Amount | amount | `Number` |  |  |  |  |
| 3.4.5 | Action | action | String |  |  |  |  |

## Example
```json
{
    "code": "A1",
    "order": 2,
    "tags": {
    "transaction_body": {
        "system_branch_name_long": "ວຽງຈັນ",
        "system_branch_name": "VIENTIANE",
        "system_branch_address": "80 Lanexang Road, B.Xiengngeuanthong M.Chanthabouly, Vientiane, Lao PDR",
        "customer_code": "11000006",
        "account_number": "140100000000022",
        "system_account_name": "TranggHT",
        "description": "1110: Cash deposit",
        "system_teller_name": "Optimal 9",
        "system_approve_name": "Optimal 9"
    },
    "transaction_date": "15/10/2021 10:52:11",
    "transaction_refid": "211015000000003355",
    "postings": [
        {
        "bank_account_number": "140100220110082100000",
        "account_name": "Demand Deposit",
        "currency_code": "USD",
        "amount": 1,
        "action": "D"
        },
        {
        "bank_account_number": "140100220110082100000",
        "account_name": "Demand Deposit",
        "currency_code": "USD",
        "amount": 1,
        "action": "C"
        }
    ]
    }
}
```

# Voucher A2
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |
| 1 | Code | code | String |  |  |  |
| 2 | Order | order | `Number` |  |  |  |
| 3 | Tags | tags | JSON Object |  |  |  |
| 3.1 | Transaction body | transaction_body | JSON Object |  |  |  |
| 3.1.1 | Account number | account_number | String |  |  |  |  |
| 3.1.2 | Account name | account_name | String |  |  |  |  |
| 3.1.3 | Customer name | customer_name | String |  |  |  |  |
| 3.1.4 | User code | system_user_code | String |  |  |  |  |
| 3.1.5 | Other currency | other_currency | String |  |  |  |  |
| 3.1.6 | Currency code | currency_code | String |  |  |  |  |
| 3.1.7 | Account amount | account_amount | `Number` |  |  |  |  |
| 3.2 | Transaction date | transaction_date | `Date time` |  |  |  |  |
| 3.3 | Transaction ref id | transaction_refid | String |  |  |  |  |
| 3.4 | Transaction code | transaction_code | String |  |  |  |  |
| 3.5 | Branch name | branch_name | String |  |  |  |  |

## Example
```json
{
    "code": "A2",
    "order": 2,
    "tags": {
    "transaction_body": {
        "account_number": "140100000000022",
        "account_name": "Trang HT",
        "customer_name": "Trang HT",
        "system_user_code": "00911",
        "other_currency": "USD",
        "currency_code": "USD",
        "account_amount": 1
    },
    "transaction_date": "15/10/2021 10:52:11",
    "transaction_refid": "211015000000003355",
    "transaction_code": "DPT_CDP",
    "branch_name": "VIENTIANE"
    }
}
```

# Voucher A17
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |
| 1 | Code | code | String |  |  |  |
| 2 | Order | order | `Number` |  |  |  |
| 3 | Tags | tags | JSON Object |  |  |  |
| 3.1 | Transaction body | transaction_body | JSON Object |  |  |  |
| 3.1.1 | Account number | account_number | String |  |  |  |  |
| 3.1.2 | Account name | account_name | String |  |  |  |  |
| 3.1.3 | Address | address | String |  |  |  |  |
| 3.1.4 | Fee amounts | fee_amounts | String |  |  | cho phép trả về nhiều fee amount, cách nhau dấu “|” |  |
| 3.1.5 | Fee names | fee_names | String |  |  | cho phép trả về nhiều fee names, cách nhau dấu “|” |  |
| 3.2 | Transaction date | transaction_date | `Date time` |  |  |  |  |
| 3.5 | Branch name | branch_name | String |  |  |  |  |

## Example
```json
{
    "code": "A17",
    "order": 2,
    "tags": {
    "transaction_body": {
        "account_number": "140100000000022",
        "account_name": "Trang HT",
        "address": "B. Nongping, Chanthabuly District, Vientiane Capital",
        "fee_amounts": "0|",
        "fee_names": "Fee for International Outward Remittance (%) (THB) (>=THB400,000)|"
    },
    "transaction_date": "15/10/2021 10:52:11",
    "branch_name": "VIENTIANE"
    }
}
```

# Voucher A11
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |
| 1 | Code | code | String |  |  |  |
| 2 | Order | order | `Number` |  |  |  |
| 3 | Tags | tags | JSON Object |  |  |  |
| 3.1 | Transaction body | transaction_body | JSON Object |  |  |  |
| 3.1.1 | Branch name long | system_branch_name_long | String |  |  |  |  |
| 3.1.2 | Branch name | system_branch_name | String |  |  |  |  |
| 3.1.3 | Branch address | system_branch_address | String |  |  |  |  |
| 3.1.4 | Account number | account_number | String |  |  |  |  |
| 3.1.5 | Customer type | customer_type | String |  |  |  |  |
| 3.1.6 | Customer name | customer_name | String |  |  |  |  |
| 3.1.7 | Customer code | customer_code | String |  |  |  |  |
| 3.1.8 | Catalogue code | catalog_code | String |  |  |  |  |
| 3.1.9 | Catalogue name | catalog_name | String |  |  |  |  |
| 3.1.10 | Deposit type | deposit_type | String |  |  |  |  |
| 3.1.11 | Account type | account_type | String |  |  |  |  |
| 3.1.12 | Description | description | String |  |  |  |  |
| 3.1.13 | Teller name | system_teller_name | String |  |  |  |  |
| 3.1.14 | Approve name | system_approve_name | String |  |  |  |  |
| 3.2 | Transaction date | transaction_date | `Date time` |  |  |  |  |

## Example
```json
{
    "code": "A11",
    "order": 2,
    "tags": {
    "transaction_body": {
        "system_branch_name_long": "ວຽງຈັນ",
        "system_branch_name": "VIENTIANE",
        "system_branch_address": "80 Lanexang Road, B.Xiengngeuanthong M.Chanthabouly, Vientiane, Lao PDR",
        "account_number": "099900010000153",
        "customer_type": "C",
        "customer_name": "NHUNG  TT",
        "customer_code": "11000013",
        "catalog_code": "CAKHR000",
        "catalog_name": "Current account in KHR",
        "deposit_type": "Current",
        "account_type": "1",
        "description": "1100: Open new deposit account",
        "system_teller_name": "Optimal 9",
        "system_approve_name": "Optimal 9"
    },
    "transaction_date": "15/10/2021 10:52:11"
    }
}
```

# Voucher A171
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |

## Example
```json
{
    "code": "A171",
    "order": 2,
    "tags": {
    "transaction_body": {
        "account_number": "140100000000022",
        "account_name": "Trang HT",
        "address": "B. Nongping, Chanthabuly District, Vientiane Capital",
        "fee_amounts": "0|",
        "fee_names": "Fee for International Outward Remittance (%) (THB) (>=THB400,000)|"

    },
    "transaction_date": "15/10/2021 10:52:11",
    "branch_name": "VIENTIANE"
    }
}
```

# Voucher A172
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |

## Example
```json
{
    "code": "A172",
    "order": 2,
    "tags": {
    "transaction_body": {
        "account_number": "140100000000022",
        "account_name": "Trang HT",
        "address": "B. Nongping, Chanthabuly District, Vientiane Capital",
        "fee_amounts": "0|",
        "fee_names": "Fee for International Outward Remittance (%) (THB) (>=THB400,000)|"

    },
    "transaction_date": "15/10/2021 10:52:11",
    "branch_name": "VIENTIANE"
    }
}

```

# Voucher A18
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |

## Example
```json
{
    "code": "A18",
    "order": 2,
    "tags": {
    "transaction_body": {
        "system_branch_name_long": "ວຽງຈັນ",
        "system_branch_name": "VIENTIANE",
        "system_branch_address": "80 Lanexang Road, B.Xiengngeuanthong M.Chanthabouly, Vientiane, Lao PDR",
        "system_branch_phone": "",
        "customer_name": "NHUNG  TT",
        "paper_number": "",
        "address": "",
        "telephone": "",
        "debit_accounting_type": "Deposit",
    "debit_account_number": "099900050000027",
        "debit_currency": "USD",
        "debit_amount": 0.01,
        "credit_accounting_type": "Accounting",
    "credit_account_number": "099901112201000000000",
        "credit_currency": "LAK",
        "cross_rate": "9,281.00000000",
        "credit_amount": 110,
        "fee_amount": 0,
        "receive_amount": 0,
        "description": "1100: Open new deposit account",
        "system_teller_name": "Optimal 9",
        "system_approve_name": "Optimal 9"
    },
    "transaction_refid": "211015000000003355",
    "transaction_date": "15/10/2021 10:52:11"
    }
}

```

# Voucher A203
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |

## Example
```json
{
  "code": "A203",
  "order": 2,
  "tags": {
    "transaction_body": {
      "account_number": "140100000000022",
      "customer_name": "Trang HT",
      "account_name": "Signature'proof by Violet \"BLB6\"",
      "book_currency": "LAK",
      "account_amount": 1
    },
    "transaction_date": "15/10/2021 10:52:11",
    "transaction_refid": "211015000000003355",
    "transaction_code": "DPT_CDP",
    "branch_name": "VIENTIANE"
  }
}
```

# Voucher A204
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |

## Example
```json
{
    "code": "A204",
    "order": 2,
    "tags": {
        "transaction_body": {
        "account_number": "140100000000022",
        "account_name": "Trang HT",
        "customer_name": "Signature'proof by Violet \"BLB6\"",
        "user_code":"00922",
        "other_currency":"USD",
        "deposit_currency": "LAK",
        "account_amount": 1,
        "interest_amount":19.17,
        "withholding_tax_amount":0
        },
        "transaction_date": "15/10/2021 10:52:11",
        "transaction_refid": "211015000000003355",
        "transaction_code": "DPT_CDP",
        "branch_name": "VIENTIANE"
    }
}

```

# Voucher A3
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |

## Example
```json
{
    "code": "A3",
    "order": 2,
    "tags": {
        "transaction_body": {
        "system_teller_name": "LA Oper 002",
        "system_approve_name": "LA Oper 002",
        "customer_code": "11000014",
        "account_name": "Demand Deposit",
        "account_number": "1100001410003",
        "description": "Approve product limit"
        },
        "transaction_date": "15/10/2021 10:52:11",
        "transaction_code": "2110150055",
        "transaction_refid": "211015000000003355",
        "postings": [
        {
            "bank_account_number": "140100220110082100000",
            "account_name": "Demand Deposit",
            "currency_code": "USD",
            "amount": 1,
            "action": "D"
        },
        {
            "bank_account_number": "140100220110082100000",
            "account_name": "Demand Deposit",
            "currency_code": "USD",
            "amount": 1,
            "action": "C"
        }
        ]
    }
}

```

# Voucher A7
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |

## Example
```json
{
  "code": "A7",
  "order": 2,
  "tags": {
    "transaction_body": {
      "from_serial": "AB0000000001",
      "to_serial": "AB0000000010",
      "stock_prefix": "AB",
      "account_number": "140100000000033"
    },
    "branch_name": "VIET NAM"
    
  }
}

```

# Voucher C101
## Response message
| No | Field name | Parameter (NEW) | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---- | ------ | ----------- | --------------- |

## Example
```json
{
    "code": "C101",
    "order": 2,
    "tags": {
        "transaction_body": {
        "system_branch_name_long": "ວຽງຈັນ",
        "system_branch_name": "VIENTIANE",
        "system_branch_address": "80 Lanexang Road, B.Xiengngeuanthong M.Chanthabouly, Vientiane, Lao PDR",
        "user_name": "KH Oper 003",
        "currency_code": "USD",
        "closing_cash_balance": 234,
        "denom_cash_balance": 3,
        "remaining_cash_balance": 231,
        "system_teller_name": "Optimal 9",
        "system_approve_name": "Optimal 9",
        "cash_denom": [
            {
            "cash_denom": {
                "currency_code": "USD",
                "face_value": 100,
                "face_type": "N",
                "sheet": 0,
                "amount": 0
            }
            }
        ]
        },
        "transaction_refid": "211015000000003355",
        "transaction_date": "15/10/2021 10:52:11"
    }
}
```