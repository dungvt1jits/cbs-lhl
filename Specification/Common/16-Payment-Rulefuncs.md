# 1. Payment module
## Trả về thông tin Agent bank khi nhận "Agent bank id"
- **Workflow id:** `GET_INFO_CIBNK`

- **Query in O9:** 
```sql
SELECT 
BNKID || ' [' || BICCD || '] ' || BNKNAME || ' . ' || BRANCHCD 
FROM O9DATA.D_AGENTBANK 
WHERE BNKID='@CIBNK' AND BNKTYPE <> 'I' AND BNKSTS <> 'I'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Agent bank id | id | No | `Number` |  |  | `CIBNK` tương ứng `id` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Agent bank info | id_bic_code_bank_name_branch_code | String |  | `BNKID [BICCD] BNKNAME . BRANCHCD` tương ứng `id [bic_code] bank_name . branch_code` |
| 2 | Bank id | id | `Number` |  |  |
| 3 | BIC code/ SWIFT code | bic_code | String |  |  |
| 4 | Bank name | bank_name | String |  |  |
| 5 | Branch code | branch_code | String |  |  |

## Trả về danh sách "Correspondent Bank" với BNKTYPE <> 'I' và BNKSTS <> 'I'
- **Workflow id:** `PMT_LOOKUP_AGENTBANK`

- **Query in O9:** 
```sql
SELECT BNKID, BNKNAME 
FROM (
    SELECT BNKID , BNKID || ' [' || BICCD || '] ' || BNKNAME || ' . ' || BRANCHCD BNKNAME, NVL(BRANCHCD,'0000') BRANCHCD 
    FROM O9DATA.D_AGENTBANK 
    WHERE BNKTYPE <> 'I' 
    AND BNKSTS <> 'I') 
ORDER BY BNKID
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 2 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Bank id | id | `Number` |  |  |
| 2 | BIC code + bank name | bic_code_bank_name | String |  |  |
| 3 | BIC code/ SWIFT code | bic_code | String |  |  |
| 4 | Bank name | bank_name | String |  |  |

## Trả về thông tin Agent bank khi nhận "Agent bank id" và "Currency code"
- **Workflow id:** `GET_INFO_CIBNK_AND_NOSTRO_GL`

- **Query in O9:** 
```sql
SELECT NVL(O9SYS.O9PMT.get_agent_account_nostro_byid(BNKID,'@CCCR') ,''),  
BNKID || ' [' || BICCD || '] ' || BNKNAME || ' . ' || BRANCHCD 
FROM O9DATA.D_AGENTBANK 
WHERE BNKID='@CIBNK' AND BNKTYPE <> 'I' AND BNKSTS <> 'I'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Agent bank id | id | No | `Number` |  |  | `CIBNK` tương ứng `id` |
| 2 | Currency code | currency_code | Yes | String |  |  | `CCCR` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Agent bank info | id_bic_code_bank_name_branch_code | String |  | `BNKID [BICCD] BNKNAME . BRANCHCD` tương ứng `id [bic_code] bank_name . branch_code` |
| 2 | Nostro account | nostro_account | String |  | lấy GL account tương ứng với `currency_code` |
| 3 | Bank id | id | `Number` |  |  |
| 4 | BIC code/ SWIFT code | bic_code | String |  |  |
| 5 | Bank name | bank_name | String |  |  |
| 6 | Branch code | branch_code | String |  |  |

## Trả về thông tin "Message" khi nhận "Message code", "Branch id" của Payment
- **Workflow id:** `PMT_GET_INFO_MESSAGE_INWARD_INTERNAL`

- **Query in O9:** 
```sql
SELECT SCUSTNAME, SLICENSE, SADDRESS, STEL, STYPE,RCUSTNAME, RLICENSE, RADDRESS, RTEL, RTYPE,COUNTRY,PURPOSE,DRCCR, CRCCR, CRAMT, DRAMT, BSAMT, EXRATE, VAT,SBANK, IBANK,CBANK
FROM D_PMTTRAN WHERE REFNO='@MSGCODE' AND TXCODE='PMT_OIT' AND MSGSTS='S' AND BBANKBR = '@CRRBRID'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Message code | message_code | No | String |  |  |  | 
| 2 | Branch id | branch_id | No | `Number` |  |  |  | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Sender name | send_customer_name | String | | `ccatnm` |
| 2 | Sender id | sender_license | String | | `c_license` |
| 3 | Sender address | sender_address | String | | `cctma` |
| 4 | Sender phone | sender_phone | String | | `cstel` |
| 5 | Sender type | sender_type | String | | `cstype` |
| 6 | Receiver name | receive_customer_name | String | | `crname` |
| 7 | Receiver id | receiver_license | String | | `crid` |
| 8 | Receiver address | receiver_address | String | | `craddr` |
| 9 | Receiver phone | receiver_phone | String | | `crtel` |
| 10 | Receiver type | receiver_type | String | | `crtype` |
| 11 | Country | country | String | | `ccountry` |
| 12 | Purpose | purpose | String | | `cspur` |
| 13 | Receive currency | credit_currency | String | | `rccr` |
| 14 | Send currency | debit_currency | String | | `cccr` |
| 15 | Receive amount | credit_amount | `Number` | | `pramt` |
| 16 | Send amount | debit_amount | `Number` | | `psamt` |
| 17 | Amount convert to BCY | base_amount | `Number` | | `cbamt` |
| 18 | Cross rate | exchange_rate| `Number` | | `ccrrate` |
| 19 | Fee | fee | `Number` | | `cvat` |
| 20 | Remitting Branch | beneficary_bank | String | | `crbank` |
| 21 | Sender to receiver information | is_inform_bank | String | | `ctxg1` |
| 22 | TEST-Key | collecting_bank | String | | `ctxg2` |

## Trả về thông tin "Bank account number" khi nhận "Account number", "Branch id"
- **Workflow id:** `PMT_GET_INFO_ACT_SACNO`

- **Query in O9:** 
```sql
SELECT BACNO,ACNAME,CCRCD FROM D_ACCHRT WHERE ISLEAVE = 'Y' AND ISDIRECT='Y' AND BRANCHID = '@CRRBRID' AND BACNO='@SACNO'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  | 
| 2 | Branch id | branch_id | No | String |  |  |  | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Bank account number | bank_account_number | String |  | `sacno` |
| 2 | Account name | account_name | String |  | `txb_sacno` |
| 3 | Currency code | currency_code | String |  | `cccr` |

## Trả về thông tin "Bank account number" khi nhận "Account number", "Branch id"
- **Workflow id:** `PMT_GET_INFO_ACT_RACNO`

- **Query in O9:** 
```sql
SELECT BACNO,ACNAME,CCRCD FROM D_ACCHRT WHERE BACNO='@RACNO' AND ISLEAVE = 'Y' AND BRANCHID = '@CRRBRID' ORDER BY BACNO
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  | 
| 2 | Branch id | branch_id | No | String |  |  |  | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Bank account number | bank_account_number | String |  | `racno` |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |

## Trả về thông tin "Correspondent Bank" khi nhận "Beneficiary branch", "Branch id"
- **Workflow id:** `PMT_OIT_GET_INFO_AGENT_BANK`

- **Query in O9:** 
```sql
 SELECT BNKID || ' [' || BICCD || '] ' || BNKNAME || ' . ' || BRANCHCD BNKNAME,BRANCHCD,'',BNKNAME 
FROM O9DATA.D_AGENTBANK where BNKSTS='A' AND PARTNER='Y' AND BNKTYPE  ='I'  AND BNKID='@CRBANK' AND BRANCHCD <> '@CRRBRID'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Beneficiary branch | beneficiary_branch | `Number` | String |  |  |  | 
| 2 | Branch id | branch_id | No | `Number` |  |  |  | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | BIC code + bank name | bic_code_bank_name | String | | |
| 2 | Branch id | branch_id | `Number` | | `CBRCD`, branch_id lấy từ branch_id của branch_code trong agent bank và khác với branch_id truyền vào |
| 3 | Product Code | product_code | String | | `CNCAT` |
| 4 | Sender information | sender_information | String | | `CTXG3` |

## Trả về danh sách account "Correspondent bank" khi nhận "Branch code"
- **Workflow id:** `PMT_LOOKUP_AGENTBANK_INTERNAL`

- **Query in O9:** 
```sql
SELECT BNKID, BANKNAME FROM (
SELECT BNKID, BNKID || ' [' || BICCD || '] ' || BNKNAME || ' . ' || BRANCHCD BANKNAME, BRANCHCD 
FROM O9DATA.D_AGENTBANK  WHERE  BNKSTS='A' AND PARTNER='Y' AND BNKTYPE  ='I' AND BRANCHCD <> '@CRRBRID' ) ORDER BY BRANCHCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | branch_id | No | `Number` |  |  | `CRRBRID` |
| 2 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 3 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Bank id | id | `Number` |  | `CRBANK` |
| 2 | BIC code + bank name | bic_code_bank_name | String |  |  |
| 3 | Branch code | branch_code | String |  |  |

## Trả về danh sách "Payment catalogue"
- **Workflow id:** `PMT_LOOKUP_PAYMENT_CATALOG`

- **Query in O9:** 
```sql
SELECT CATCD,CATNAME,DIRECTION, OUTPUTFMT FROM O9DATA.D_PMTCAT WHERE CATSTS='N' AND OUTPUTFMT  ='@OUTPUTFMT' AND DIRECTION='@DIRECTION' ORDER BY CATCD 
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Direction | direction | No | String |  |  |  |
| 2 | OutputFormat | output_format | No | String |  |  |  |
| 3 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 4 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String |  | `cncat` |
| 2 | Catalogue name | catalog_name | String |  | `catname` |
| 3 | Direction | direction | String |  | `cdirec` |
| 4 | OutputFormat | output_format | String |  | `ctx1` |

Trans code | Trans name | output_format | direction |
| -------- | ---------- | ------------- | --------- |
PMT_OIT | 6600: Outward internal bank | I | O |
PMT_IIT | 6610: Inward internal bank | I | I |
PMT_ODT | 6601: Outward domestic bank | D | O |
PMT_OITT | 6602: Outward international bank | S | O |
PMT_IITT | 6612: Inward International bank | S | I |

## Trả về thông tin "Correspondent Bank" khi nhận "Remitting Branch"
- **Workflow id:** `PMT_IIT_GET_INFO_AGENT_BANK`

- **Query in O9:** 
```sql
SELECT BNKNAME , BRANCHCD, BNKNAME FROM O9DATA.D_AGENTBANK WHERE BNKTYPE='I' AND BNKID='@CRBANK' 
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Remitting Branch | remitting_branch | String | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Bank name | bank_name | String | | `ctxg3` |
| 2 | Receive branch id | receive_branch_id | `Number` | | `cbrcd` |

## Trả về danh sách "Correspondent bank" khi nhận "Branch code"
- **Workflow id:** `PMT_LOOKUP_AGENTBANK_INTERNAL`

- **Query in O9:** 
```sql
SELECT BNKID, BANKNAME FROM (
SELECT BNKID, BNKID || ' [' || BICCD || '] ' || BNKNAME || ' . ' || BRANCHCD BANKNAME, BRANCHCD 
FROM O9DATA.D_AGENTBANK  WHERE  BNKSTS='A' AND PARTNER='Y' AND BNKTYPE  ='I' AND BRANCHCD <> '@CRRBRID' ) ORDER BY BRANCHCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch code | branch_code | No | `Number` |  |  | `CRRBRID` |
| 2 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 3 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Bank id | id | `Number` |  | `CRBANK` |
| 2 | BIC code + bank name | bic_code_bank_name | String |  |  |
| 3 | Branch code | branch_code | String |  |  |

## Trả về danh sách account (Deposit or Accounting) khi nhận "Branch ID", "Method"

- **Workflow id:** `PMT_LKP_DATA_SACNO`

- **Query in O9:** 
```sql
SELECT bacno, acname, ccrcd
  FROM v_acchrt_deposit
 WHERE (   (    isleave = 'Y'
            AND isdirect = 'Y'
            AND dptsts IS NULL
            AND dpttype IS NULL
            AND branchid = '@CRRBRID'
            AND recby = '@PAYBY'
           )
        OR (    isleave IS NULL
            AND isdirect IS NULL
            AND dptsts IN ('A', 'N', 'W')
            AND dpttype IN ('C', 'S')
            AND branchid = '@CRRBRID'
            AND recby = '@PAYBY')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | branch_id | String | `Number` |  |  | `CRRBRID` |
| 2 | Method | method | No | String |  | `DPT` or `ACT` | `PAYBY` |
| 3 | Page index | page_index | No | `Number` |  |  |  |
| 4 | Page size | page_size | No | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |

## Trả về thông tin "Deposit account" khi nhận "Account number"
- **Workflow id:** `PMT_GET_INFO_DPT_CCCR`

- **Query in O9:** 
```sql
SELECT CCRCD,O9SYS.O9DPT.get_fullname(DEFACNO,'NAME'),
O9SYS.O9DPT.get_fullname(DEFACNO,'NRC'),
O9SYS.O9DPT.get_fullname(DEFACNO,'ADDRESS'),
O9SYS.O9DPT.get_fullname(DEFACNO,'PHONE') 
FROM O9DATA.D_DEPOSIT     WHERE  ACNO='@SACNO'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Send currency | sender_currency | String | | `CCCR` |
| 2 | Sender name | sender_name | String | | `CCATNM` |
| 3 | Sender id | sender_id | String | | `C_LICENSE` |
| 4 | Sender address | sender_address | String | | `CCTMA` |
| 5 | Sender phone | sender_phone | String | | `CSTEL` |

## Trả về thông tin "Correspondent Bank" khi nhận "Product code", "Nostro bank"
- **Workflow id:** `PMT_KIIT_GET_INFO_AGENT_BANK`

- **Query in O9:** 
```sql
SELECT A.BNKID || ' [' || A.BICCD || '] ' || A.BNKNAME || ' . ' || A.BRANCHCD BNKNAME ,A.BRANCHCD, A.BNKNAME,A.COUNTRY,A.ADDRESS 
FROM O9DATA.D_AGENTBANK A, O9DATA.D_PMTCAT B 
WHERE B.CATCD = '@CNCAT' AND BNKID='@CRBANK' AND A.BNKTYPE = B.OUTPUTFMT AND A.BNKSTS='A' AND  A.BNKTYPE  IN ('S','D') AND A.PARTNER='Y' ORDER BY BNKID
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Product code | product_code | No | String |  |  | cncat | 
| 2 | Nostro bank | nostro_bank | No | String |  |  | crbank | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | BIC code + bank name | bic_code_bank_name | String |  | txb_crbank |
| 2 | Receive Branch id | receive_branch_id | `Number` |  | cbrcd | branch_id lấy từ branch_id của branch_code trong agent bank |
| 3 | Bank name | bank_name | String |  | ctxg3 |
| 4 | Bank country | bank_country | String |  | cntrbank |
| 5 | Bank address | bank_address | String |  | addbank |

## Trả về thông tin "Nostro account" khi nhận "Nostro bank", "Remitting currency"
- **Workflow id:** `PMT_GET_INFO_NOSTRO`

- **Query in O9:** 
```sql
SELECT NVL(O9SYS.O9PMT.GET_INFO_ACCOUNT('@CRBANK','@CCCR','N'),'*') FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Nostro bank | nostro_bank | No | String |  |  | crbank | 
| 2 | Remitting currency | remitting_currency | No | String |  |  | cccr | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Nostro account | nostro_account | String |  | cnostro |

## Trả về thông tin rate khi nhận "Remitting currency", "Receive currency", "Branch id", "Catalogue code"
- **Workflow id:** `PMT_GET_INFO_CCRRATE`

- **Query in O9:** 
```sql
SELECT o9util.get_cross_rate (
           '@CCCR',
           '@RCCR',
           CASE WHEN outputfmt in( 'S','D') THEN 'TB' ELSE 'CB' END,
           CASE WHEN outputfmt in( 'S','D') THEN 'TA' ELSE 'CA' END,
           '@CRRBRID',
           NULL,
           NULL)  AS CCRRATE,
           o9util.get_cross_rate (
           '@CCCR',
           '@RCCR',
           CASE WHEN outputfmt in( 'S','D') THEN 'TA' ELSE 'CB' END,
           CASE WHEN outputfmt in( 'S','D') THEN 'TB' ELSE 'CA' END,
           '@CRRBRID',
           NULL,
           NULL) AS ORGRATE,
		   o9util.get_cross_rate (
           '@RCCR',
           '@CCCR',
           CASE WHEN outputfmt in( 'S','D') THEN 'TA' ELSE 'CB' END,
           CASE WHEN outputfmt in( 'S','D') THEN 'TB' ELSE 'CA' END,
           '@CRRBRID',
           NULL,
           NULL) AS SWRATE,
           1 AS CHKRATE,
       '@CCCR' || '/' || '@RCCR'
  FROM o9data.d_pmtcat
 WHERE catcd = '@CNCAT'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Remitting currency | remitting_currency | No | String |  |  | cccr | 
| 2 | Receive currency | receive_currency | No | String |  |  | rccr | 
| 3 | Branch id | branch_id  | No | `Number` |  |  |  | 
| 4 | Catalogue code | catalog_code | No | String |  |  |  | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` | | ccrrate |
| 2 | Rate RCCR | rate_rccr | `Number` | | orgrate | |
| 3 | Reverse rate | reverse_rate | `Number` | | swrate |
| 4 | Check rate | check_rate | `Number` | | chkrate |
| 5 | Bank address | bank_address | String | | addbank |

## Trả về danh sách "Payment catalogue" cho giao dịch PMT_KITT

- **Workflow id:** `PMT_KITT_LOOKUP_PAYMENT_CATALOG`

- **Query in O9:** 
```sql
SELECT CATCD,CATNAME,DIRECTION, OUTPUTFMT, '' CRBANK FROM O9DATA.D_PMTCAT WHERE CATSTS = 'N' AND  OUTPUTFMT  IN ('D', 'S') AND DIRECTION='I' ORDER BY CATCD 
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 2 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String |  | `cncat` |
| 2 | Catalogue name | catalog_name | String |  | `catname` |
| 3 | Direction | direction | String |  | `cdirec` |
| 4 | OutputFormat | output_format | String |  | `ctx1` |
| 5 | Nostro bank | nostro_bank | String |  | `crbank` |

## Trả về danh sách "Correspondent Bank" khi nhận "Catalogue code"

- **Workflow id:** `PMT_KITT_LOOKUP_AGENTBANK`

- **Query in O9:** 
```sql
SELECT BNKID, BNKNAME FROM (SELECT BNKID, BNKID || ' [' || BICCD || '] ' || BNKNAME || ' . ' || BRANCHCD BNKNAME 
FROM O9DATA.D_AGENTBANK A, O9DATA.D_PMTCAT B WHERE B.CATCD = '@CNCAT' AND A.BNKTYPE = B.OUTPUTFMT AND A.BNKSTS='A' AND  A.BNKTYPE  IN ('S','D') AND A.PARTNER='Y') ORDER BY BNKID
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  | cncat | 
| 2 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 3 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Nostro bank | nostro_bank | String |  | `crbank` |
| 2 | BIC code + bank name | bic_code_bank_name | String |  | `txb_crbank` |

## Trả về thông tin "Nostro account" khi nhận "Nostro bank", "Receive currency", "Remitting amount"
- **Workflow id:** `PMT_GET_INFO_NOSTRO_BY_ID`

- **Query in O9:** 
```sql
SELECT NVL(O9SYS.O9PMT.get_agent_account_nostro_byid('@CRBANK','@RCCR'),'*') FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Nostro bank | nostro_bank | No | String |  |  | crbank | 
| 2 | Receive currency | receive_currency | No | String |  |  | rccr | 
| 3 | Remitting amount | amount_before_fees | No | `Number` |  |  | psamt | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Nostro account | nostro_account | String |  | `cnostro` |

## Trả về thông tin "Correspondent Bank" khi nhận "Nostro bank"
- **Workflow id:** `PMT_OITT_GET_INFO_AGENT_BANK`

- **Query in O9:** 
```sql
SELECT BNKID || ' [' || BICCD || '] ' || BNKNAME || ' . ' || BRANCHCD BNKNAME,BRANCHCD,BNKNAME,COUNTRY,ADDRESS, BNKNAME FROM O9DATA.D_AGENTBANK where BNKSTS='A' AND  BNKTYPE  ='S' AND PARTNER='Y' AND BNKID='@CRBANK'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Nostro bank | nostro_bank | No | String |  |  | `crbank` | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | BIC code + bank name | bic_code_bank_name | String |  | `txb_crbank` |
| 2 | Branch cd | branch_cd | `Number` |  | `cbrcd` | branch_id lấy từ branch_id của branch_code trong agent bank |
| 3 | Bank name | bank_name | String |  | `ctxg3`, `ctxg8` |
| 4 | Bank country | bank_country | String |  | `cntrbank` |
| 5 | Bank address | bank_address | String |  | `addbank` |

## Trả về thông tin "Message" khi nhận "Message code" cho giao dịch PMT_IIT
- **Workflow id:** `PMT_GET_INFO_MESSAGE_INWARD_INTERNATIONAL`

- **Query in O9:** 
```sql
SELECT scustname ccatnm,  slicense c_license,  saddress cctma,  stel cstel,  stype cstype,  dramt PSAMT,  
   ROUND (  dramt  * ROUND (  o9util.get_bcyrate (drccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TB' END,  '@CRRBRID'),  9)  / ROUND (  o9util.get_bcyrate (crccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TA' END,  '@CRRBRID'),  9),  2)  PRAMT,  
   sbank crbank,  ibank ctxg1,  cbank ctxg2,  recby recby,  country ccountry,  purpose cspur,  rtype crtype,  pmtnum pmtnum,  refno ctxg5,  nostro cnostro,  sbankbr cbrcdc,  
   ROUND (  o9util.get_bcyrate (drccr, CASE WHEN crccr = drccr THEN 'TA' ELSE 'TB' END, '@CRRBRID'),  9)  pgexr,  
   ROUND (  o9util.get_bcyrate (crccr, CASE WHEN crccr = drccr THEN 'TA' ELSE 'TA' END, '@CRRBRID'),  9)  ctxexr,  
   drccr cccr,  crccr rccr,   
   ROUND (dramt *  ROUND (  o9util.get_bcyrate (drccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TB' END,  '@CRRBRID'),  9)  / ROUND (  o9util.get_bcyrate (crccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TA' END,  '@CRRBRID'),  9),  2)  - '@CFAMT'  pcsamt,  
   ROUND (  ROUND (  o9util.get_bcyrate (drccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TB' END,  '@CRRBRID'),  9)  / ROUND (  o9util.get_bcyrate (crccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TA' END,  '@CRRBRID'),  9),  9)  ccrrate,  
   ROUND (  ROUND (  o9util.get_bcyrate (drccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TB' END,  '@CRRBRID'),  9)  / ROUND (  o9util.get_bcyrate (crccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TA' END,  '@CRRBRID'),  9),  9)  orgrate,  
   ROUND (  ROUND (  o9util.get_bcyrate (crccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TA' END,  '@CRRBRID'),  9)  / ROUND (  o9util.get_bcyrate (drccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TB' END,  '@CRRBRID'),  9),  9)  swrate,  
   ROUND (  ROUND (  o9util.get_bcyrate (drccr,  CASE WHEN crccr = drccr THEN 'TA' ELSE 'TB' END,  '@CRRBRID'),  9)  * dramt,  9)  cbamt,ORDINSTITUTION  
   FROM o9data.d_pmttran  WHERE refno = '@MSGCODE' AND txrefid = '@PMREF'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Message code | message_code | No | String |  |  |  | 
| 2 | Reference number | reference_number | No | String |  |  |  | 
| 3 | Commission | commission | No | `Number` |  |  | `cfamt` | 
| 4 | Branch id | branch_id  | No | `Number` |  |  |  |   

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Sender name | send_customer_name | String | | `ccatnm` |
| 2 | Sender id | sender_license | String | | `c_license` |
| 3 | Sender address | sender_address | String | | `cctma` |
| 4 | Sender phone | sender_phone | String | | `cstel` |
| 5 | Sender type | sender_type | String | | `cstype` |
| 6 | Remitting amount | debit_amount | String | | `psamt` |
| 7 | Amount before fees | amount_before_fees | String | | `pramt` |
| 8 | Nostro Bank | send_bank | String | | `crbank` |
| 9 | Sender to receiver information | is_inform_bank | String | | `ctxg1` |
| 10 | TEST-Key | collecting_bank | String | | `ctxg2` |
| 11 | Receive by | receive_by | String | | `recby` |
| 12 | Country | country | String | | `ccountry` |
| 13 | Purpose | purpose | String | | `cspur` |
| 14 | Receiver type | receiver_type | String | | `crtype` |
| 15 | Transaction number | payment_number | String | | `pmtnum` |
| 16 | Other information | reference_number | String | | `ctxg5` |
| 17 | Nostro Account | nostro | String | | `cnostro` |
| 18 | Nostro bank branchcd | send_bank_branch | String | | `cbrcdc` |
| 19 | Remitting exchange rate/BCY | remitting_exchange_rate_bcy | `Number` | | `pgexr` |
| 20 | Receive exchange rate/BCY | receive_exchange_rate_bcy | `Number` | | `ctxexr` |
| 21 | Remitting currency | debit_currency | String | | `cccr` |
| 22 | Receive currency | credit_currency | String | | `rccr` |
| 23 | Amount customer receives | amount_customer_receives | `Number` | | `pcscvt` |
| 24 | Original cross rate | original_rate | `Number` | | `orgrate` |
| 25 | Cross rate | cross_rate | `Number` | | `ccrrate` |
| 26 | Reverse rate | reverse_rate | `Number` | | `swrate` |
| 27 | Remitting amount in BCY | remitting_amount_in_bcy | `Number` | | `cbamt` |
| 28 | Ordering Institution | ordering_institution | String | | `crbank2` |
| 29 | Account number | account_number2 | String | | `racno` |
| 30 | Receiver name | receive_customer_name | String | | `crname` |
| 31 | Receiver id | receiver_license | String | | `crid` |
| 32 | Receiver phone | receiver_phone | String | | `crtel` |
| 33 | Receiver address | receive_address | String | | `craddr` |
| 34 | Receive currency | credit_currency | String | | `rccr` |

## Trả về thông tin "Reference number" khi nhận "Message code"
- **Workflow id:** `PMT_GET_INFO_RMREF`

- **Query in O9:** 
```sql
SELECT TXREFID FROM O9DATA.D_PMTQUEUE WHERE MSGCODE='@MSGCODE' 
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Message code | message_code | No | String |  |  |  |  

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Reference number | reference_number | String |  | `pmref` |

## Trả về thông tin "Customer" khi nhận "Message code", "Reference number"
- **Workflow id:** `PMT_GET_INFO_CSH_RCUSTOMER`

- **Query in O9:** 
```sql
select rcustname,rlicense,rtel,raddress,crccr  from o9data.d_pmttran  where refno='@MSGCODE' and txrefid='@PMREF' 
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Message code | message_code | No | String |  |  |  | 
| 2 | Reference number | reference_number | No | String |  |  |  | 
- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Receiver name | receiver_name | String | | `crname` |
| 2 | Receiver id | receiver_id | String | | `crid` |
| 3 | Receiver phone | receiver_phone | String | | `crtel` |
| 4 | Receiver address | receiver_address | String | | `craddr` |
| 5 | Receive currency | receive_currency | String | | `rccr` |

## Trả về thông tin "Account number" khi nhận "Message code", "Reference number"
- **Workflow id:** `PMT_GET_INFO_RACNO`

- **Query in O9:** 
```sql
SELECT ACNO2 FROM D_PMTTRAN WHERE refno='@MSGCODE' and txrefid='@PMREF'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Message code | message_code | No | String |  |  |  | 
| 2 | Reference number | reference_number | No | String |  |  |  |   

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String | | `racno` |

## Trả về thông tin "Correspondent Bank" khi nhận "Nostro bank", "Message code"
- **Workflow id:** `PMT_IITT_GET_INFO_AGENT_BANK`

- **Query in O9:** 
```sql
SELECT BNKNAME,BNKNAME FROM O9DATA.D_AGENTBANK where BNKSTS='A' AND  BNKTYPE  ='D' AND PARTNER='Y' AND BICCD='@CRBANK'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Nostro bank | nostro_bank | No | String |  |  | `crbank` | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | BIC code + bank name | bic_code_bank_name | String |  | `txb_crbank` |
| 2 | Nostro bank name | bank_name | String |  | `ctxg8` |

## Trả về thông tin rate khi nhận "Remitting amount", "Commission", "Remitting currency", "Receive currency", "Branch id"
- **Workflow id:** `PMT_GET_INFO_ON_RECBY`

- **Query in O9:** 
```sql
SELECT ctxexr,
ROUND (pgexr / ctxexr, 9) ccrrate,
ROUND (ctxexr / pgexr, 9) swrate,
'@PSAMT' * ROUND (pgexr / ctxexr, 9) PRAMT,
('@PSAMT' * ROUND (pgexr / ctxexr, 9) - '@CFAMT') pcscvt,
'' RACNO
FROM (SELECT ROUND (
o9util.get_bcyrate ('@CCCR',CASE WHEN '@CCCR' = '@RCCR' THEN 'TA' ELSE 'TB' END,'@CRRBRID'),9)pgexr,
ROUND (o9util.get_bcyrate ('@RCCR',CASE WHEN '@CCCR' = '@RCCR' THEN 'TA' ELSE 'TA' END,'@CRRBRID'),9)ctxexr
FROM DUAL)
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Commission | commission | No | `Number` |  |  | `cfamt` | 
| 2 | Remitting currency | remitting_currency | No | String |  |  | cccr | 
| 3 | Receive currency | receive_currency | No | String |  |  | rccr | 
| 4 | Branch id | branch_id | No | `Number` |  |  | | 
| 5 | Remitting amount | remitting_amount | No | `Number` |  |  | `psamt` | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Receive exchange rate/BCY | receive_exchange_rate_bcy | `Number` | | `ctxexr` |
| 2 | Cross rate | cross_rate | `Number` | | `ccrrate`, `orgrate` |
| 3 | Reverse rate | reverse_rate | `Number` | | `swrate` |
| 4 | Amount before fees | amount_before_fees | `Number` | | `pramt` |
| 5 | Amount customer receives | amount_customer_receives | String | | `pcscvt` |
| 6 | Account number | account_number | String | | `racno` |

## Trả về thông tin "Amount customer receive" khi nhận "Amount before fees", "Remitting amount", "Commission"
- **Workflow id:** `GET_INFO_ON_PRAMT`

- **Query in O9:** 
```sql
select ROUND('@PRAMT'/'@PSAMT',9) CCRRATE, ROUND('@PSAMT'/'@PRAMT',9) SWRATE, '@PRAMT'-'@CFAMT' from dual
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Amount before fees | amount_before_fees | No | `Number` |  |  | `pramt` | 
| 2 | Remitting amount | remitting_amount | No | `Number` |  |  | `psamt` | 
| 3 | Commission | commission | No | `Number` |  |  | `cfamt` | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` | | `CCRRATE` = `PRAMT`/`PSAMT` (làm tròn 9) |
| 2 | Reverse rate | reverse_rate | `Number` | | `SWRATE` = `PSAMT`/`PRAMT` (làm tròn 9) |
| 3 | Amount customer receives | amount_customer_receives | String | | `PCSCVT` = `PRAMT` - `CFAMT` |

## Trả về danh sách "Message code" khi nhận "Branch id"
- **Workflow id:** `PMT_LOOKUP_MESSAGE_INWARD_INTERNATIONAL`

- **Query in O9:** 
```sql
SELECT B.MSGCODE, B.TXREFID, ACNO2  FROM       O9DATA.D_PMTTRAN A         INNER JOIN             O9DATA.D_PMTQUEUE B         ON A.REFNO = B.MSGCODE AND A.TXREFID = B.TXREFID  WHERE      BRAPRSTS = 'A'         AND BRPROSTS = 'N'         AND B.MSGDRT = 'I'         AND B.PMTTYPE = 'S'         AND A.MSGSTS <>'P'         AND PMTNUM IS NOT NULL         AND A.STATUS <> 'R' AND B.RBRANCH = '@CRRBRID'  ORDER BY A.TXREFID
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | branch_id | No | `Number` |  |  |  | 
| 2 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 3 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Message code | message_code | String | | `msgcode` |
| 2 | Reference number | reference_number | String | | `pmref` |
| 3 | Account number | account_number | String | | `racno` |

## Trả về thông tin "Payment catalogue" khi nhận "Catalogue code"
- **Workflow id:** `GET_INFO_PMTCAT`

- **Query in O9:** 
```sql
SELECT CATNAME FROM O9DATA.D_PMTCAT where CATSTS='N' AND  OUTPUTFMT  ='D' AND DIRECTION='I' AND CATCD='@PMTCAT'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue name | catalog_name | String | | |

## Trả về danh sách "Correspondent bank" Outward International
- **Workflow id:** `PMT_LOOKUP_AGENTBANK_INTERNATIONAL`

- **Query in O9:** 
```sql
SELECT BNKID,BNKNAME,BRANCHCD,BNKNAME1 FROM ( SELECT BNKID,BNKID || ' [' || BICCD || '] ' || BNKNAME || ' . ' || BRANCHCD BNKNAME, BRANCHCD, BNKNAME BNKNAME1 FROM O9DATA.D_AGENTBANK  WHERE  BNKSTS='A' AND  BNKTYPE  ='S' AND PARTNER='Y') ORDER BY BNKID
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 2 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Nostro bank | nostro_bank | String |  | `crbank` |
| 2 | BIC code + bank name | bic_code_bank_name | String |  | `txb_crbank` |
| 3 | Branch cd | branch_cd | `Number` |  | `cbrcd` | branch_id lấy từ branch_id của branch_code trong agent bank |
| 4 | Nostro bank name | nostro_bank_name | String |  | `ctxg8` |

## Trả về thông tin rate khi nhận "Remitting currency", "Receive currency", "Remitting amount"
- **Workflow id:** `PMT_GET_INFO_ON_CCCR`

- **Query in O9:** 
```sql
SELECT o9util.get_bcyrate ('@CCCR', 'TB', '@CRRBRID') AS payment_rate,  o9util.get_bcyrate ('@RCCR', 'TA', '@CRRBRID') AS remitting_rate,  o9sys.o9util.get_cross_rate ('@CCCR',  '@RCCR',  'TB',  'TA',  '@CRRBRID')  AS ccrrate,  o9sys.o9util.get_cross_rate ('@RCCR',  '@CCCR',  'TA',  'TB',  '@CRRBRID')  AS swrate,  ROUND ('@PSAMT' * o9util.get_bcyrate ('@CCCR', 'TB', '@CRRBRID'), 2) AS cbamt,  ROUND (  '@PSAMT'  * o9sys.o9util.get_cross_rate ('@CCCR',  '@RCCR',  'TB',  'TA',  '@CRRBRID'),  2)  AS PRAMT,  o9sys.o9util.get_cross_rate ('@CCCR',  '@RCCR',  'TB',  'TA',  '@CRRBRID')  AS orgrate  FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Remitting currency | remitting_currency | No | String |  |  | cccr | 
| 2 | Receive currency | receive_currency | No | String |  |  | rccr | 
| 3 | Remitting amount | remitting_amount | No | `Number` |  |  | psamt | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Payment exchange rate | payment_exchange_rate | `Number` |  | `pgexr` |
| 2 | Remitting exchange rate | remitting_exchange_rate | `Number` | | `ctxexr` |
| 3 | Cross rate | cross_rate | `Number` | | `ccrrate` |
| 4 | Reverse rate | reverse_rate | `Number` | | `swrate` |
| 5 | Remitting amount in base currency | remitting_amount_in_base_currency | `Number` | | `cbamt` |
| 6 | Remitting amount | remitting_amount | `Number` | | `pramt` |
| 7 | Rate RCCR | rate_rccr | `Number` | | `orgrate` |

## Trả về thông tin "Reverse rate" khi nhận "Cross rate"
- **Workflow id:** `PMT_GET_INFO_ON_CCRRATE`

- **Query in O9:** 
```sql
SELECT ROUND (1 / '@CCRRATE', 9) as SWRATE FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Cross rate | cross_rate | No | `Number` |  |  | `ccrrate` | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Reverse rate | reverse_rate | `Number` | | `swrate` |

## Trả về thông tin "Cross rate" khi nhận "Reverse rate"
- **Workflow id:** `PMT_GET_INFO_ON_SWRATE`

- **Query in O9:** 
```sql
SELECT ROUND (1 / '@SWRATE', 9) AS swrate FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Reverse rate | reverse_rate | No | `Number` |  |  | `swrate` | 

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` | | `ccrrate` |