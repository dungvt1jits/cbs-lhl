# 1. Admin module
## Trả về danh sách các user khi nhận "Branch id"
- **Workflow id:** `ADM_LOOKUP_USER_ACCOUNT_BY_BRANCHID`

- **Query in O9:** 
```sql
SELECT USRCD,USRNAME 
FROM O9SYS.S_USRAC 
WHERE STATUS = 'N' 
AND BRANCHID = '@CRRBRID' ORDER BY USRCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | branch_id | No | `Number` |  |  | `CRRBRID` |
| 2 | Page index | page_index | No | `Number` |  |  |  |
| 3 | Page size | page_size | No | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  |  |
| 3 | User name | user_name | String |  |  |
| 4 | Login name | login_name | String |  |  |

## Trả về thông tin user là "Cashier (C)" or "Chief Cashier (I)" khi nhận "User code" và "Branch id"
- **Workflow id:** `GET_INFO_USER_ACCOUNT_CASHIER`

- **Query in O9:** 
```sql
SELECT USRNAME,USRID,LGNAME 
FROM S_USRAC 
WHERE (O9UTIL.GET_FLAT_JSON_VALUE_NUMBER(POSITION,'C')=1 OR O9UTIL.GET_FLAT_JSON_VALUE_NUMBER(POSITION,'I')=1) 
AND USRCD = '@USRCD' 
AND BRANCHID = '@CRRBRID'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User code | user_code | No | String |  |  | `USRCD` |
| 2 | Branch id | branch_id | No | `Number` |  |  | `CRRBRID` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  |  |
| 3 | User name | user_name | String |  |  |
| 4 | Login name | login_name | String |  |  |

## Trả về danh sách các user là "Cashier (C)" or "Chief Cashier (I)" khi nhận "Branch id"
- **Workflow id:** `ADM_LOOKUP_USER_ACCOUNT_CASHIER`

- **Query in O9:** 
```sql
SELECT USRCD,USRNAME,USRID,LGNAME FROM S_USRAC WHERE (O9UTIL.GET_FLAT_JSON_VALUE_NUMBER(POSITION,'C')=1 OR O9UTIL.GET_FLAT_JSON_VALUE_NUMBER(POSITION,'I')=1) 
AND BRANCHID = '@CRRBRID' ORDER BY USRCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | branch_id | No | `Number` |  |  | `CRRBRID` |
| 2 | Page index | page_index | No | `Number` |  |  |  |
| 3 | Page size | page_size | No | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  |  |
| 3 | User name | user_name | String |  |  |
| 4 | Login name | login_name | String |  |  |

## Trả về thông tin Branch khi nhận "Branch code"
- **Workflow id:** `GET_INFO_BRNAME`

- **Query in O9:** 
```sql
SELECT BRANCHID,BRANCHCD,BRNAME,ADDRESS FROM O9SYS.S_BRANCH WHERE BRANCHCD ='@CIBNK'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch code | branch_code | No | String |  |  | `CIBNK` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch id | id | `Number` | `BRANCHID` |
| 2 | Branch code | branch_code | String | `BRANCHCD` |
| 3 | Branch name | branch_name | String | `BRNAME` |
| 4 | Branch address | branch_address | String | `ADDRESS` |

## Trả về thông tin caption khi nhận "Code id" của Business Purpose Code
- **Workflow id:** `GET_INFO_WISICCD`

- **Query in O9:** 
```sql
select CAPTION from o9code.c_banklst where cdname='ISICCD' and CDGRP='CTM' and cdid='@ISICCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Caption | caption | String |  |  |

## Trả về information khi nhận "User code"
- **Workflow id:** `GET_INFO_USER_ACCOUNT`

- **Query in O9:** 
```sql
SELECT USRCD, USRID, LGNAME,USRNAME FROM O9SYS.S_USRAC WHERE USRCD = '@USRCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User code | user_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  |  |
| 3 | User name | user_name | String |  |  |
| 4 | Login name | login_name | String |  |  |