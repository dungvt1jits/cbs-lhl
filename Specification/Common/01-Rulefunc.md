# 1. Accounting module
## Accounting group lookup
- [Search advanced của "Group Definition"](Specification/Accounting/03 Group Definition)
- **Workflow id:** `ACT_ACGRPDEF_LOOKUP`
- Lookup:
    - Code: group_id
    - Name: account_group_def

## System account name lookup
- [Search advanced của "Module Account List"](Specification/Accounting/04 Module Account List)
- **Workflow id:** `ACT_ACGRPDEFDTL_LOOKUP`
- Lookup:
    - Code: system_account_name
    - Name: bank_define_account_name

## Accounting Chart lookup
- [Search advanced của "Bank Account Definition"](Specification/Accounting/01 Bank Account Definition)
- **Workflow id:** `ACT_ACCHRT_LOOKUP`
- Lookup:
    - Code: bank_account_number
    - Name: account_name

# 2. Admin module
## Currency lookup
- [Search advanced của "Currency"](Specification/Administration/04 Currency)
- **Workflow id:** `ADM_LOOKUP_CURRENCY`
- **Điều kiện:** "status_of_currency": "A"
- Select:
    - Code: currency_id

## Country lookup
- [Search advanced của "Country"](Specification/Administration/03 Country)
- **Workflow id:** `ADM_LOOKUP_COUNTRY`
- Lookup:
    - Code: iso2_alpha
    - Name: country_name

- Select: 
    - Code: iso2_alpha
    - Name: country_name

## Branch lookup
- [Search advanced của "Branch Profile"](Specification/Administration/05 Branch Profile)
- **Workflow id:** `ADM_LOOKUP_BRANCH`
- Lookup:
    - Code: branch_code
    - Name: branch_name

## Department lookup

- **Workflow id:** `ADM_LOOKUP_DEPARTMENT`

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Department code | department_code | No | String |  |  |  |
| 2 | Department name | department_name | No | String |  |  |  |
| 3 | Branch name | branch_name | No | String |  |  |  |
| 4 | Page index | page_index | No | `Number` |  |  |  |
| 5 | Page size | page_size | No | `Number` |  |  |  |
| 6 | Branch id | branch_id | No | `Number` |  |  | `gửi thêm` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Department code | department_code | String |  |  |
| 2 | Branch name | branch_name | String |  |  |
| 3 | Department name | department_name | String |  |  |
| 4 | Department id | id | `Number` |  |  |

- **Workflow id:** `ADM_LOOKUP_DEPARTMENT`
- Lookup:
    - Code: department_code
    - Name: department_name

## User profile lookup
- [Search advanced của "User Profile"](Specification/Administration/07 User Profile)
- **Workflow id:** `ADM_LOOKUP_USER_ACCOUNT`
- **Điều kiện:** "user_account_status": "N"
- Lookup:
    - Code: user_code
    - Name: user_name

## User policy lookup
- [Search advanced của "System-Policy"](Specification/Administration/08 System Policy)
- **Workflow id:** `ADM_LOOKUP_USER_POLICY`
- Lookup:
    - Code: id
    - Name: descr

## Code list lookup
- [Search advanced của "Code Definition List"](Specification/Administration/16 Code Definition List)
- **Workflow id:** `ADM_LOOKUP_CODE_LIST`
- Lookup:
    - Code: code_id
    - Name: caption
- Mô tả ví dụ:
    - Trong module Accounting fields "Resident": "code_name": "ACRSD" và "code_group": "ACT"
    - Trong module Payment, tab Payment instruction group information: "code_name": "PIGCD" và "code_group": "PMT"
    - Occupation (HO): code_name='PROFESSION' and code_group='CTM
    - Business Type (HO): code_name='BUCD' and code_group='CTM'
    - Customer Type (HO): 
        - "customer_private"="I": code_name='CTYPEP' and code_group='CTM'
        - "customer_private"="E": code_name='CTYPEO' and code_group='CTM'
    - ISIC code: code_name='ISICCD' and code_group='CTM'
    - Business purpose code: code_name='ISICCD' and code_group='CTM'

## Code list sub lookup
- [Search advanced của "Code Definition List"](Specification/Administration/16 Code Definition List)
- **Workflow id:** `ADM_LOOKUP_CODE_LIST_SUB`
- Lookup:
    - Code: code_id
    - Name: caption
- Mô tả thêm phần address:
    - Province: gửi "code_name": "LCPROVINE" và "code_group": "CTM"
    - District: gửi "code_name": "LCDISTRICT", "code_group": "CTM" và "code_id" của `Province` đã chọn
    - Sub district: gửi "code_name": "LCSDISTRICT", "code_group": "CTM" và "code_id" của `District` đã chọn
    - Village: gửi "code_name": "LCVILLAGE", "code_group": "CTM" và "code_id" của `Sub district` đã chọn

## Get branch_id

- **Workflow id:** `ACT_BANK_ACCOUNT_DEFINITION_GET_INFO_BRANCHID`

- **Query:** SELECT id FROM `Branch` WHERE `BranchCode` = SUBSTR('099902647102000000000',1,4);

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch id | branch_id | `Number` |  | Số nguyên |

## Get currency_id

- **Workflow id:** `ACT_BANK_ACCOUNT_DEFINITION_GET_INFO_CCRCD`

- **Query:** SELECT `CurrencyId` FROM `Currency` WHERE `ShortCurrencyId` = substr('099902647102000000000',5,2)  

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency id | currency_id | String |  |  |

# 3. IFC module
## IFC lookup
- [Search advanced của "IFC Item Definition"](Specification/IFC/01 IFC Item Definition)
- **Workflow id:** `IFC_LOOKUP_IFC`
- **Điều kiện:** "ifc_status": "N"
- Lookup:
    - Code: ifc_code
    - Name: ifc_name

## Tariff lookup
- [Search advanced của "Tariff Configuration"](Specification/IFC/02 Tariff Configuration)
- **Workflow id:** `IFC_LOOKUP_TARIFF`
- **Điều kiện:** "tariff_status": "N"
- Lookup:
    - Code: tariff_code
    - Name: tariff_name

# 4. Deposit module
## Catalog lookup
- [Search advanced của "Catalogue Definition"](Specification/Deposit/01 Catalogue Definition)
- **Workflow id:** `DPT_LOOKUP_CATALOG`
- **Điều kiện:** "catalog_status": "N"
- Lookup:
    - Code: catalog_code
    - Name: catalog_name

## Deposit account lookup
- [Search advanced của "Account Information"](Specification/Deposit/02 Account Information)
- **Workflow id:** `DPT_LOOKUP_DEPOSIT`
- Lookup:
    - Code: account_number
    - Name: account_name

# 5. Credit module
## Catalog lookup
- [Search advanced của "Catalogue Definition"](Specification/Credit/01 Catalogue Definition)
- **Workflow id:** `CRD_LOOKUP_CRDCAT`
- **Điều kiện:** "catalog_status": "N"
- Lookup:
    - Code: catalog_code
    - Name: catalog_name

## Group limit lookup
- [Search advanced của "Group Limit"](Specification/Credit/03 Group Limit)
- **Workflow id:** `CRD_LOOKUP_CRDGRPLM`
- Lookup:
    - Code: group_limit_code
    - Name: group_limit_name

## Product limit lookup
- [Search advanced của "Product Limit"](Specification/Credit/04 Product Limit)
- **Workflow id:** `CRD_LOOKUP_CRDPL`
- Lookup:
    - Code: product_limit_code
    - Name: product_limit_name

## Sub product limit lookup
- [Search advanced của "Sub Product Limit"](Specification/Credit/05 Sub Product Limit)
- **Workflow id:** `CRD_LOOKUP_CRDSPL`
- Lookup:
    - Code: sub_product_limit_code
    - Name: sub_product_limit_name

## Credit account lookup
- [Search advanced của "Account Information"](Specification/Credit/02 Account Information)
- **Workflow id:** `CRD_LOOKUP_CREDIT`
- Lookup:
    - Code: account_number
    - Name: account_name

# 9. Mortgage module
## Catalog lookup
- [Search advanced của "Catalogue Definition"](Specification/Collateral/01 Catalogue Definition)
- **Workflow id:** `MTG_LOOKUP_MORTGAGE_CATALOG`
- **Điều kiện:** "catalog_status": "N"
- Lookup:
    - Code: catalog_code
    - Name: catalog_name

# 10. Fixed asset module
## Catalog lookup
- [Search advanced của "Catalogue Definition"](Specification/Fixed Asset/01 Catalogue Definition)
- **Workflow id:** `SQL_LOOKUP_FACCAT`
- **Điều kiện:** "catalog_status": "N"
- Lookup:
    - Code: catalog_code
    - Name: catalog_name

# 11. Customer lookup
## Customer single
- [Search advanced của "Customer Profile"](Specification/Customer/01 Customer Profile)
- **Workflow id:** `SQL_LOOKUP_CTM`
- Lookup:
    - Code: customer_code
    - Name: fullname

## Customer linkage
- [Search advanced của "Customer Linkage"](Specification/Customer/03 Customer Linkage)
- **Workflow id:** `SQL_LOOKUP_CTMLKG`
- Lookup:
    - Code: linkage_code
    - Name: master_customer_name

# 19. Check sanction
## 19.1 Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Module code | module_code | `Yes` | String |  |  |  |
| 2 | Module account number | module_account_number | `Yes` | String |  |  |  |
| 3 | Module type | module_type | No | String |  |  |  |

## 19.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Sanction number | sanction_number | `Number` |  |  |

# 20. Get info address of customer
## 20.1 Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- | 
| 1 | Customer type | | `Yes` | String | | | |
| 2 | Customer code | | `Yes` | String | | | |

## 20.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Customer id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Customer address | contact_local_address| String |  |  |

# 21. Get info deposit account
## 21.1 Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- | 
| 1 | Deposit account number | | `Yes` | String | | | |

## 21.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Customer code | | String | | | |
| 2 | Customer name | | String | | | |
| 3 | Customer address | | String | | | |

# 22. Get info credit account
## 22.1 Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- | 
| 1 | Credit account number | | `Yes` | String | | | |

## 22.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Customer code | | String | | | |
| 2 | Customer name | | String | | | |
| 3 | Customer address | | String | | | |

# 23. Get info GL account
## 23.1 Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- | 
| 1 | GL account | | `Yes` | String | | | |

## 23.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | GL account number | | String | | | |
| 2 | GL name | | String | | | |