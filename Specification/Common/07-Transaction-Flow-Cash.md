# 1. Cash decrease
**Transaction:**
- CRD_CDR: Cash Disbursement
- CRD_RFC: Fee Refund By Cash
- DPT_CWR: Cash Withdrawal
- DPT_CWT: Cash Withdrawal Time Deposit
- DPT_CWC: Cash Withdrawal By Cheque
- DPT_CIP: Interest Payment By Cash
- DPT_CLS: Close Deposit Account
- FAC_PBC: Purchase Fixed Asset By Cash

**Flow of events:**
- Decrease cash at counter.
- Transaction complete:
    - Cash of teller decrease "Customer withdrawal".

**Database:**

# 1. Cash increase
**Transaction:**
- CRD_CIPM: Principal And Interest Collection By Cash
- CRD_FOC: Fee Collection By Cash
- DPT_CDP: Cash Deposit
- DPT_AOPC: Approve Open And Deposit LFA By Cash
- DPT_COD: Close OD Facility By Cash
- DPT_FOC: Fee Collection By Cash For DD
- FAC_SBC: Selling Fixed Asset By Cash
- ACT_FEE: Fee collection by cash

**Flow of events:**
- Increase cash at counter.
- Transaction complete:
    - Cash of teller increase "Customer deposit".

**Database:**