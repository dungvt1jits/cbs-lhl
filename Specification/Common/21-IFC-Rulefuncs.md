# 1. IFC module
## Trả về danh sách các IFC có IFCTYPE là C khi nhận "Currency"
- **Workflow id:** `IFC_LOOKUP_IFCTYPE_C`

- **Query in O9:** 
```sql
SELECT DISTINCT IFCCD,IFCNAME,PAYRATE,VALTYPE,IFCVAL 
FROM V_TXIFCLST 
WHERE IFCSTS = 'N' 
AND IFCTYPE IN ('C') 
AND GRPID IS NOT NULL 
AND CCRCD='@CCCR' ORDER BY IFCCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  | 0 |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Currency code | currency_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC code | ifc_code | `Number` |  | `IFCCD` |
| 2 | IFC name | ifc_name | String |  | `IFCNAME` |
| 3 | Pay rate | payrate | `Number` |  | `PAYRATE` |
| 4 | Value type | value_type | String |  | `VALTYPE` |
| 5 | IFC value | ifc_value | `Number` |  | số thập phân, `IFCVAL` |

## Trả về danh sách các IFC có IFCTYPE là C và O khi nhận "Currency"
- **Workflow id:** `IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY`

- **Query in O9:** 
```sql
SELECT DISTINCT IFCCD,IFCNAME,PAYRATE,VALTYPE,IFCVAL 
FROM V_TXIFCLST 
WHERE IFCSTS = 'N' 
AND IFCTYPE IN ('C','O') 
AND CCRCD='@CCCR'
AND GRPID IS NOT NULL ORDER BY IFCCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  | 0 |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Currency code | currency_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC code | ifc_code | `Number` |  | `IFCCD` |
| 2 | IFC name | ifc_name | String |  | `IFCNAME` |
| 3 | Pay rate | payrate | `Number` |  | `PAYRATE` |
| 4 | Value type | value_type | String |  | `VALTYPE` |
| 5 | IFC value | ifc_value | `Number` |  | số thập phân, `IFCVAL` |

## Trả về thông tin IFC trong D_IFCLST khi nhận "IFC code"
- **Workflow id:** `GET_IFC_TYPE`

- **Query in O9:** 
```sql
SELECT IFCTYPE FROM O9DATA.D_IFCLST WHERE IFCCD = '@CIFCCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | IFC code | ifc_code | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | IFC id | id | `Number` |  |  |
| 2 | IFC code | ifc_code | `Number` |  |  |
| 3 | IFC name | ifc_name | String |  |  |
| 4 | IFC type | ifc_type | String |  |  |
| 5 | IFC sub type | ifc_sub_type | String |  |  |
| 6 | Value base | value_base | String |  |  |
| 7 | Is linked | is_linked | String |  |  |
| 8 | Value | ifc_value | `Number` |  | số có 5 số thập phân |
| 9 | Margin value | margin_value | `Number` |  | số có 5 số thập phân |
| 10 | Value type | value_type | String |  |  |
| 11 | Currency code | currency_code | String | 3 |  |

## Trả về thông tin IFC trong D_IFCBAL khi nhận "IFC code" và "Account number" của Deposit
- **Workflow id:** `DPT_IFC_GET_INFO_IFCAMT`

- **Query in O9:** 
```sql
SELECT AMT FROM D_IFCBAL WHERE DEFACNO = O9SYS.O9DPT.SET_ACNO_DEFACNO('@PDACC')AND IFCCD = '@CIFCCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của deposit |
| 2 | IFC code | ifc_code | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Amount | amount | `Number` |  | số có 5 số thập phân, `D_IFCBAL` |
| 2 | Paid | paid | `Number` |  | số có 5 số thập phân, `D_IFCBAL` |

## Trả về thông tin IFC trong D_IFCBAL khi nhận "IFC code" và "Account number" của Credit
- **Workflow id:** `CRD_IFC_GET_INFO_IFCAMT`

- **Query in O9:** 
```sql
SELECT amt FROM o9data.d_ifcbal WHERE defacno = o9sys.o9crd.set_acno_defacno ('@PCACC') AND ifccd = '@CIFCCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của credit |
| 2 | IFC code | ifc_code | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Amount | amount | `Number` |  | số có 5 số thập phân |
| 2 | Paid | paid | `Number` |  | số có 5 số thập phân |

## Trả về danh sách IFC có IFCTYPE in ('I', 'T') khi nhận "Account number" của Deposit
- **Workflow id:** `IFC_LOOKUP_IFCTYPE_IT_BY_DPTACNO`

- **Query in O9:** 
```sql
SELECT IFCCD, IFCNAME, IFCVAL FROM O9DATA.D_IFCLST 
where IFCTYPE in ('I', 'T') 
and IFCCD IN (
    select ifccd 
    from o9data.d_ifcbal 
    where defacno=o9sys.o9dpt.set_acno_defacno('@PDACC')
) ORDER BY IFCCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Deposit |
| 2 | Page index | page_index | No | `Number` |  | 0 |  |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC code | ifc_code | `Number` |  | `IFCCD` |
| 2 | IFC name | ifc_name | String |  | `IFCNAME` |
| 3 | Pay rate | payrate | `Number` |  | `PAYRATE` |
| 4 | Value type | value_type | String |  | `VALTYPE` |
| 5 | IFC value | ifc_value | `Number` |  | số thập phân, `IFCVAL` |

## Trả về danh sách IFC có IFCTYPE='I' khi nhận "Account number" của Credit
- **Workflow id:** `IFC_LOOKUP_IFCTYPE_I_BY_CRDACNO`

- **Query in O9:** 
```sql
SELECT IFCCD, IFCNAME, IFCVAL FROM O9DATA.D_IFCLST 
where IFCTYPE='I' 
and IFCCD IN (
select ifccd 
from o9data.d_ifcbal 
where defacno=o9sys.o9crd.set_acno_defacno('@PCACC')
) ORDER BY IFCCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |
| 2 | Page index | page_index | No | `Number` |  | 0 |  |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC code | ifc_code | `Number` |  | `IFCCD` |
| 2 | IFC name | ifc_name | String |  | `IFCNAME` |
| 3 | Pay rate | payrate | `Number` |  | `PAYRATE` |
| 4 | Value type | value_type | String |  | `VALTYPE` |
| 5 | IFC value | ifc_value | `Number` |  | số thập phân, `IFCVAL` |

## Trả về danh sách các IFC có IFCTYPE là C và O
- **Workflow id:** `IFC_LOOKUP_IFCTYPE_CO`

- **Query in O9:** 
```sql
SELECT DISTINCT IFCCD,IFCNAME,PAYRATE,VALTYPE,IFCVAL FROM V_TXIFCLST
WHERE IFCSTS = 'N' AND IFCTYPE IN ('C','O') AND GRPID IS NOT NULL ORDER BY IFCCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  | 0 |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC code | ifc_code | `Number` |  |  |
| 2 | IFC name | ifc_name | String |  |  |
| 3 | Pay rate | payrate | `Number` |  |  |
| 4 | Value type | value_type | String |  |  |
| 5 | IFC value | ifc_value | `Number` |  | số có 5 số thập phân |

## Trả về thông tin IFC trong V_TXIFCLST khi nhận "IFC code" và "Currency"
- **Workflow id:** `TRAN_FEE_ACT_FEE_GET_INFO_IFCCD`

- **Query in O9:**
```sql
SELECT DISTINCT IFCCD,IFCNAME,PAYRATE,VALTYPE,IFCVAL,IFCAMT,IFCBAMT,VATAMT,SENDAMT,RECEIAMT,EQUIAMT,FLRVAL,CEIVAL,CCRCD,IFCLINK,RNDRULE,RNDNUM,SFAPPL,SRATE,SAMT,RRATE,RAMT,GRPID 
FROM V_TXIFCLST 
WHERE IFCSTS = 'N' AND IFCTYPE IN ('C', 'O') AND IFCCD = '@IFCCD' AND CCRCD='@CCCR' AND GRPID IS NOT NULL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | IFC code | ifc_code | No | `Number` |  |  | số nguyên |
| 2 | Currency code | currency_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | IFC code | ifc_code | `Number` |  |  |
| 2 | IFC name | ifc_name | String |  |  |
| 3 | Pay rate | payrate | `Number` |  |  |
| 4 | Value type | value_type | String |  |  |
| 5 | IFC value | ifc_value | `Number` |  | số có 5 số thập phân |
| 6 | IFC amount | ifc_amount | `Number` |  | số có 2 số thập phân |
| 7 | IFC base amount | ifc_base_amount | `Number` |  | số có 2 số thập phân, Amount in base currency |
| 8 | VAT amount | vat_amount | `Number` |  | số có 2 số thập phân |
| 9 | Send amount | send_amount | `Number` |  | số có 2 số thập phân |
| 10 | Received amount | received_amount | `Number` |  | số có 2 số thập phân |
| 11 | Equivalent amount | equivalent_amount | `Number` |  | số có 2 số thập phân |
| 12 | Floor value | floor_value | `Number` |  | số có 5 số thập phân |
| 13 | Ceiling value | ceiling_value | `Number` |  | số có 5 số thập phân |
| 14 | Currency code | currency_code | String | 3 |  |
| 15 | IFC linkage | ifc_linkage | `Number` |  |  |
| 16 | Rounding rule | rounding_rule | String |  |  |
| 17 | Rounding num | rounding_num | `Number` |  |  |
| 18 | Share fee | share_fee | `Number` |  |  |
| 19 | Share rate | share_rate | `Number` |  |  |
| 20 | Share amount | share_amount | `Number` |  |  |
| 21 | Round rate | round_rate | `Number` |  |  |
| 22 | Round amount | round_amount | `Number` |  |  |
| 23 | Group id | group_id | `Number` |  | số nguyên dương |

## Trả về thông tin IFC trong V_TXIFCLST khi nhận "IFC code"
- **Workflow id:** `TRAN_FEE_GET_INFO_IFCCD`

- **Query in O9:**
```sql
SELECT DISTINCT IFCCD,IFCNAME,PAYRATE,VALTYPE,IFCVAL,IFCAMT,IFCBAMT,VATAMT,SENDAMT,RECEIAMT,EQUIAMT,FLRVAL,CEIVAL,CCRCD,IFCLINK,RNDRULE,RNDNUM,SFAPPL,SRATE,SAMT,RRATE,RAMT,GRPID 
FROM V_TXIFCLST 
WHERE IFCSTS = 'N' AND IFCTYPE IN ('C','O') AND IFCCD = '@IFCCD' AND GRPID IS NOT NULL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | IFC code | ifc_code | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description | Parameter (OLD) |
| -- | ---------- | --------- | ---- | ------ | ----------- | --------------- |
| 1 | IFC code | ifc_code | `Number` |  |  | IFCCD |
| 2 | IFC name | ifc_name | String |  |  | IFCNAME |
| 3 | Pay rate | payrate | `Number` |  |  | PAYRATE |
| 4 | Value type | value_type | String |  |  | VALTYPE |
| 5 | IFC value | ifc_value | `Number` |  | số có 5 số thập phân | IFCVAL |
| 6 | IFC amount | ifc_amount | `Number` |  | số có 2 số thập phân | IFCAMT |
| 7 | IFC base amount | ifc_base_amount | `Number` |  | số có 2 số thập phân, Amount in base currency | IFCBAMT |
| 8 | VAT amount | vat_amount | `Number` |  | số có 2 số thập phân | VATAMT |
| 9 | Send amount | send_amount | `Number` |  | số có 2 số thập phân | SENDAMT |
| 10 | Received amount | received_amount | `Number` |  | số có 2 số thập phân | RECEIAMT |
| 11 | Equivalent amount | equivalent_amount | `Number` |  | số có 2 số thập phân | EQUIAMT |
| 12 | Floor value | floor_value | `Number` |  | số có 5 số thập phân | FLRVAL |
| 13 | Ceiling value | ceiling_value | `Number` |  | số có 5 số thập phân | CEIVAL |
| 14 | Currency code | currency_code | String | 3 |  | CCRCD |
| 15 | IFC linkage | ifc_linkage | `Number` |  |  | IFCLINK |
| 16 | Rounding rule | rounding_rule | String |  |  | RNDRULE |
| 17 | Rounding num | rounding_num | `Number` |  |  | RNDNUM |
| 18 | Share fee apply | share_fee | `Number` |  |  | SFAPPL |
| 19 | Share rate | share_rate | `Number` |  |  | SRATE |
| 20 | Share amount | share_amount | `Number` |  |  | SAMT |
| 21 | Round rate | round_rate | `Number` |  |  | RRATE |
| 22 | Round amount | round_amount | `Number` |  |  | RAMT |
| 23 | Group id | group_id | `Number` |  | số nguyên dương | GRPID |

## Trả về thông tin IFC khi nhận "IFC code" và "Currency" và "Transaction code"
- **Workflow id:** `IFC_GET_INFO_IFCCD_BY_CURRENCY_AND_TRANSCODE`

- **Query:** Clone từ workflow id: TRAN_FEE_ACT_FEE_GET_INFO_IFCCD

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | IFC code | ifc_code | No | `Number` |  |  | số nguyên |
| 2 | Currency code | currency_code | No | String |  |  |  |
| 3 | Transaction code | tran_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | IFC code | ifc_code | `Number` |  |  |
| 2 | IFC name | ifc_name | String |  |  |
| 3 | Pay rate | payrate | `Number` |  |  |
| 4 | Value type | value_type | String |  |  |
| 5 | IFC value | ifc_value | `Number` |  | số có 5 số thập phân |
| 6 | IFC amount | ifc_amount | `Number` |  | số có 2 số thập phân |
| 7 | IFC base amount | ifc_base_amount | `Number` |  | số có 2 số thập phân, Amount in base currency |
| 8 | VAT amount | vat_amount | `Number` |  | số có 2 số thập phân |
| 9 | Send amount | send_amount | `Number` |  | số có 2 số thập phân |
| 10 | Received amount | received_amount | `Number` |  | số có 2 số thập phân |
| 11 | Equivalent amount | equivalent_amount | `Number` |  | số có 2 số thập phân |
| 12 | Floor value | floor_value | `Number` |  | số có 5 số thập phân |
| 13 | Ceiling value | ceiling_value | `Number` |  | số có 5 số thập phân |
| 14 | Currency code | currency_code | String | 3 |  |
| 15 | IFC linkage | ifc_linkage | `Number` |  |  |
| 16 | Rounding rule | rounding_rule | String |  |  |
| 17 | Rounding num | rounding_num | `Number` |  |  |
| 18 | Share fee | share_fee | `Number` |  |  |
| 19 | Share rate | share_rate | `Number` |  |  |
| 20 | Share amount | share_amount | `Number` |  |  |
| 21 | Round rate | round_rate | `Number` |  |  |
| 22 | Round amount | round_amount | `Number` |  |  |
| 23 | Group id | group_id | `Number` |  | số nguyên dương |

## Trả về danh sách các IFC có IFCTYPE là C và O khi nhận "Currency"  và "Transaction code"
- **Workflow id:** `IFC_LOOKUP_BY_CURRENCY_AND_TRANSCODE`

- **Query:** Clone từ workflow id: IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  | 0 |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Currency code | currency_code | No | String |  |  |  |
| 4 | Transaction code | tran_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC code | ifc_code | `Number` |  | `IFCCD` |
| 2 | IFC name | ifc_name | String |  | `IFCNAME` |
| 3 | Pay rate | payrate | `Number` |  | `PAYRATE` |
| 4 | Value type | value_type | String |  | `VALTYPE` |
| 5 | IFC value | ifc_value | `Number` |  | số thập phân, `IFCVAL` |

## Trả về danh sách các IFC có IFCTYPE KHÁC C (Custom fee)
- **Workflow id:** `IFC_LOOKUP_IFCTYPE_NOT_C`

- **Điều kiện:** 
- IFC type <> C (Custom fee)
- IFC status = N (Normal)

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  | 0 |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | IFC id | id | `Number` |  |  |
| 2 | IFC code | ifc_code | `Number` |  |  |
| 3 | IFC name | ifc_name | String |  |  |
| 4 | IFC type | ifc_type | String |  |  |
| 5 | IFC sub type | ifc_sub_type | String |  |  |
| 6 | Value base | value_base | String |  |  |
| 7 | Is linked | is_linked | String |  |  |
| 8 | Value | ifc_value | `Number` |  | số có 5 số thập phân |
| 9 | IFC linkage | ifc_linkage | `Number` |  |  |
| 10 | IFC operator | ifc_operator | String |  |  |
| 11 | Margin value | margin_value | `Number` |  | số có 5 số thập phân |
| 12 | Value type | value_type | String |  |  |
| 13 | Value basic | value_basic | String |  |  |
| 14 | Active condition | ifc_condition | String |  |  |