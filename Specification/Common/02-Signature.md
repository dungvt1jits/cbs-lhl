# 1. Get list signature by customer code
## Request message

**Workflow id:** `SQL_GETINFO_MEDIA`

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | `Yes` | String | 8 |  |  |
| 2 | Media references type | reference_type | `Yes` | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |
| 4 | Page index | page_index | No | `Number` |  | 0 |  |

## Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results media | items | Array Object |  |  |
| 1 | Media file id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Media name | media_name | String |  |  |
| 4 | Media type | media_type | String |  |  |
| 5 | Media data | media_data | String |  |  |
| 6 | Expire date | expire_date | `Date time` |  |  |
| 7 | Other | other | String |  |  |
| 8 | Media status | media_status | String |  |  |
| 9 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |

# 2. Get list signature by customer id
## Request message

**Workflow id:** `SQL_GET_SIGNATURE`

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer id | id | `Yes` | `Number` |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

## Response message
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_count | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Media file name | media_name | String |  |  |  |
| 2 | Image | media_data | String |  |  |  |
| 3 | Expire date | expire_date | Date |  |  |  |
| 4 | Other | other | String |  |  |  |


# 3. Trả về thông tin media khi nhận "Account number" và "Module"
- **Workflow id:** `GET_MEDIA_SIG_ACNO`

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |
| 2 | Module | module | No | String | 3 |  | DPT, CRD, MTG, `CTM` |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |
| 4 | Page index | page_index | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results media | items | Array Object |  |  |
| 1 | Media file id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Media name | media_name | String |  |  |
| 4 | Media type | media_type | String |  |  |
| 5 | Media data | media_data | String |  |  |
| 6 | Expire date | expire_date | `Date time` |  |  |
| 7 | Other | other | String |  |  |
| 8 | Media status | media_status | String |  |  |
| 9 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |