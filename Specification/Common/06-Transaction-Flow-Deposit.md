# 1. Open account/ Open and deposit LFA
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status |  | Pending to approve |
| 2 | Open date |  | Working date |
| 3 | Created by |  | User login |

# 2. Approve account
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status | | New | |

# 3. Approve open and deposit LFA
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status |  | Active | |
| 2 | Begin of tenor |  | Working date | |
| 3 | End of tenor |  | [begin of tenor] + kỳ hạn catalogue | |
| 4 | Current balance |  | Current balance (old) + Deposit amount | số có hai số thập phân |
| 5 | Available balance |  | Available balance (old) + Deposit amount | số có hai số thập phân |
| 6 | Deposit amount |  | Deposit amount (old) + Deposit amount | số có hai số thập phân |
| 7 | Week credit |  | Week credit (old) + Deposit amount | số có hai số thập phân |
| 8 | Month credit |  | Month credit (old) + Deposit amount | số có hai số thập phân |
| 9 | Quarter credit |  | Quarter credit (old) + Deposit amount | số có hai số thập phân |
| 10 | Semi-annual credit |  | Semi-annual credit (old) + Deposit amount | số có hai số thập phân |
| 11 | Year credit |  | Year credit (old) + Deposit amount | số có hai số thập phân |

# 4. Deposit trans
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status |  | Normal | |
| 2 | Current balance |  | Current balance (old) + Deposit amount | số có hai số thập phân |
| 3 | Available balance |  | Available balance (old) + Deposit amount | số có hai số thập phân |
| 4 | Deposit amount |  | Deposit amount (old) + Deposit amount | số có hai số thập phân |
| 5 | Week credit |  | Week credit (old) + Deposit amount | số có hai số thập phân |
| 6 | Month credit |  | Month credit (old) + Deposit amount | số có hai số thập phân |
| 7 | Quarter credit |  | Quarter credit (old) + Deposit amount | số có hai số thập phân |
| 8 | Semi-annual credit |  | Semi-annual credit (old) + Deposit amount | số có hai số thập phân |
| 9 | Year credit |  | Year credit (old) + Deposit amount | số có hai số thập phân |

# 5. Withdraw trans
## 5.1 Current/ Saving
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Current balance | | | Current balance (old) - Withdraw amount | số có hai số thập phân | |
| 2 | Available balance | | | Available balance (old) - Withdraw amount | số có hai số thập phân | |
| 3 | Withdraw amount | | | Withdraw amount (old) + Withdraw amount | số có hai số thập phân | |
| 4 | Week debit | | | Week debit (old) + Withdraw amount | số có hai số thập phân | |
| 5 | Month debit | | | Month debit (old) + Withdraw amount | số có hai số thập phân | |
| 6 | Quarter debit | | | Quarter debit (old) + Withdraw amount | số có hai số thập phân | |
| 7 | Semi-annual debit | | | Semi-annual debit (old) + Withdraw amount | số có hai số thập phân | |
| 8 | Year debit | | | Year debit (old) + Withdraw amount | số có hai số thập phân |

## 5.2 Fixed deposit (Withdrawal amount = Available balance)
###  5.2.1 Status: Maturity
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status | | Closed | |
| 2 | Close date | | Working date| | |
| 3 | Current balance | | 0 | số có hai số thập phân |
| 4 | Available balance | | 0 | số có hai số thập phân |
| 5 | Interest accrual | | 0 | số có hai số thập phân |
| 6 | Interest due | | 0 | số có hai số thập phân |
| 7 | Withdraw amount | | Withdraw amount (old) + Withdraw amount | số có hai số thập phân |
| 8 | Week debit | | Week debit (old) + Withdraw amount | số có hai số thập phân |
| 9 | Month debit | | Month debit (old) + Withdraw amount | số có hai số thập phân |
| 10 | Quarter debit | | Quarter debit (old) + Withdraw amount | số có hai số thập phân |
| 11 | Semi-annual debit | | Semi-annual debit (old) + Withdraw amount | số có hai số thập phân |
| 12 | Year debit | | Year debit (old) + Withdraw amount | số có hai số thập phân |

###  5.2.2 Status: Active/ Normal
**Working date < Open date + 3 months < Maturity date**<br>
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status | | Closed | |
| 2 | Close date | | Working date| | |
| 3 | Current balance | | 0 | số có hai số thập phân |
| 4 | Available balance | | 0 | số có hai số thập phân |
| 5 | Interest accrual | | 0 | số có hai số thập phân |
| 6 | Withholding tax accrual | | 0 | số có hai số thập phân |
| 7 | Interest due | | 0 | số có hai số thập phân |
| 8 | Withdraw amount | | Withdraw amount (old) + Withdraw amount | số có hai số thập phân |
| 9 | Week debit | | Week debit (old) + Withdraw amount | số có hai số thập phân |
| 10 | Month debit | | Month debit (old) + Withdraw amount | số có hai số thập phân |
| 11 | Quarter debit | | Quarter debit (old) + Withdraw amount | số có hai số thập phân |
| 12 | Semi-annual debit | | Semi-annual debit (old) + Withdraw amount | số có hai số thập phân |
| 13 | Year debit | | Year debit (old) + Withdraw amount | số có hai số thập phân |

**Working date > Open date + 3 months < Maturity date**<br>
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status | | Closed | |
| 2 | Close date | | Working date| | |
| 3 | Current balance | | 0 | số có hai số thập phân |
| 4 | Available balance | | 0 | số có hai số thập phân |
| 5 | Interest accrual | | 0 | số có hai số thập phân |
| 6 | Interest due | | 0 | số có hai số thập phân |
| 7 | Interest not paid | | (Balance * Saving rate)/365 * tổng số ngày tính từ begin of tenor tới working date | số có hai số thập phân |
| 8 | Withdraw amount | | Withdraw amount (old) + Withdraw amount | số có hai số thập phân |
| 9 | Week debit | | Week debit (old) + Withdraw amount | số có hai số thập phân |
| 10 | Month debit | | Month debit (old) + Withdraw amount | số có hai số thập phân |
| 11 | Quarter debit | | Quarter debit (old) + Withdraw amount | số có hai số thập phân |
| 12 | Semi-annual debit | | Semi-annual debit (old) + Withdraw amount | số có hai số thập phân |
| 13 | Year debit | | Year debit (old) + Withdraw amount | số có hai số thập phân |

# 6. Interest collection
## 6.1 Saving
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Interest accrual | | Interest accrual (old) - Interest collect | số có hai số thập phân |
| 2 | Interest paid | | Interest paid (old) + Interest collectsố có hai số thập phân | |
| 3 | IFC data | | | |
| | IFC code | | | |
| | IFC name | | | |
| | Base value | | | |
| | Is linked | | | |
| | IFC value | | | số có 5 số thập phân |
| | Margin value | | | số có 5 số thập phân |
| | Status | | | |
| | Oustanding | | | số có 5 số thập phân |
| | Paid | | Paid (old) + Interest collect | số có 5 số thập phân |

## 6.2 Fixed deposit
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Interest due | | Interest due (old) - Interest collect | số có hai số thập phân |
| 2 | Interest paid | | Interest paid (old) + Interest collectsố có hai số thập phân | |
| 3 | IFC data | | | |
| | IFC code | | | |
| | IFC name | | | |
| | Base value | | | |
| | Is linked | | | |
| | IFC value | | | số có 5 số thập phân |
| | Margin value | | | số có 5 số thập phân |
| | Status | | | |
| | Oustanding | | | số có 5 số thập phân |
| | Paid | | Paid (old) + Interest collect | số có 5 số thập phân |

# 7. Close account
D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status | | Closed | |
| 2 | Close date | | Working date| | |
| 3 | Current balance | | 0 | số có hai số thập phân |
| 4 | Available balance | | 0 | số có hai số thập phân |
| 5 | Interest accrual | | 0 | số có hai số thập phân |
| 6 | Interest due | | 0 | số có hai số thập phân |
| 7 | Withdraw amount | | Withdraw amount (old) + Withdraw amount | số có hai số thập phân |
| 8 | Week debit | | Week debit (old) + Withdraw amount | số có hai số thập phân |
| 9 | Month debit | | Month debit (old) + Withdraw amount | số có hai số thập phân |
| 10 | Quarter debit | | Quarter debit (old) + Withdraw amount | số có hai số thập phân |
| 11 | Semi-annual debit | | Semi-annual debit (old) + Withdraw amount | số có hai số thập phân |
| 12 | Year debit | | Year debit (old) + Withdraw amount | số có hai số thập phân |

# 8. Stock issue for customer
## 8.1 Cheque
D_STKINV
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Book status | | Normal | |
| 2 | Ref.account No. | | Deposit account number | |


## 8.2 Passbook/ Receipt
D_STKINV
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Book status | | Normal | |
| 2 | Ref.account No. | | Deposit account number | |

D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Passbook or receipt number | | String | | | |
| 2 | Passbook or receipt status | | String | | | |

# 9. Withdraw by Cheque
D_STKINV
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Book status | | Paid | |

D_DEPOSIT
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Current balance | | | Current balance (old) - Withdraw amount | số có hai số thập phân | |
| 2 | Available balance | | | Available balance (old) - Withdraw amount | số có hai số thập phân | |
| 3 | Withdraw amount | | | Withdraw amount (old) + Withdraw amount | số có hai số thập phân | |
| 4 | Week debit | | | Week debit (old) + Withdraw amount | số có hai số thập phân | |
| 5 | Month debit | | | Month debit (old) + Withdraw amount | số có hai số thập phân | |
| 6 | Quarter debit | | | Quarter debit (old) + Withdraw amount | số có hai số thập phân | |
| 7 | Semi-annual debit | | | Semi-annual debit (old) + Withdraw amount | số có hai số thập phân | |
| 8 | Year debit | | | Year debit (old) + Withdraw amount | số có hai số thập phân |