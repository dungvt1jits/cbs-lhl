# 1. Deposit module
## Trả về thông tin Stock prefix của "Stock type=C" khi nhận "Serial"
- **Workflow id:** `DPT_GET_STOCK_PREFIX_C`

- **Query in O9:** 
```sql
SELECT SUBSTR('@CFRSER',1,O9SYS.O9STK.RETURN_PRF('C')) FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock prefix | stock_prefix | String |  |  |

## Trả về thông tin Stock prefix khi nhận "Serial" và "Stock type"
- **Workflow id:** `DPT_GET_STOCK_PREFIX`

- **Query in O9:** 
```sql
SELECT SUBSTR('@CFRSER',1,O9SYS.O9STK.RETURN_PRF('@CSTKTP')) FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |
| 2 | Stock type | stock_type | No | String |  |  | `CSTKTP` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock prefix | stock_prefix | String |  |  |

## Trả về thông tin Stock prefix của "Stock type=P" khi nhận "Serial"
- **Workflow id:** `DPT_GET_STOCK_PREFIX_P`

- **Query in O9:** 
```sql
SELECT SUBSTR('@CFRSER',1,O9SYS.O9STK.RETURN_PRF('P')) FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock prefix | stock_prefix | String |  |  |

## Trả về thông tin Stock prefix của "Stock type=F" khi nhận "Serial"
- **Workflow id:** `DPT_GET_STOCK_PREFIX_F`

- **Query in O9:** 
```sql
SELECT SUBSTR('@CFRSER',1,O9SYS.O9STK.RETURN_PRF('F')) FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock prefix | stock_prefix | String |  |  |

## Trả về thông tin Stock prefix của "Stock type=R" khi nhận "Serial"
- **Workflow id:** `DPT_GET_STOCK_PREFIX_R`

- **Query:** 
Tương tự `DPT_GET_STOCK_PREFIX_F` nhưng STKTYPE = 'R'

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock prefix | stock_prefix | String |  |  |

## Trả về thông tin Stock prefix của "Stock type=G" khi nhận "Serial"
- **Workflow id:** `DPT_GET_STOCK_PREFIX_G`

- **Query in O9:** 
```sql
SELECT SUBSTR('@CFRSER',1,O9SYS.O9STK.RETURN_PRF('G')) FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock prefix | stock_prefix | String |  |  |

## Trả về thông tin Stock prefix của "Stock type=O" khi nhận "Serial"
- **Workflow id:** `DPT_GET_STOCK_PREFIX_O`

- **Query in O9:** 
```sql
SELECT SUBSTR('@CFRSER',1,O9SYS.O9STK.RETURN_PRF('O')) FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock prefix | stock_prefix | String |  |  |

## Trả về thông tin Account number khi nhận "Serial" của Stock type là Cheque
- **Workflow id:** `DPT_GET_INFO_PDACC_BY_CHEQUE_SERIAL`

- **Query in O9:** 
```sql
SELECT REFAC FROM O9DATA.D_STKINV WHERE '@CFRSER' BETWEEN STKPRF||STKFR AND STKPRF||STKTO AND STKTYPE = 'C'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  | `REFAC` |

## Trả về thông tin Account number khi nhận "Serial" và "Stock type"
- **Workflow id:** `DPT_GET_INFO_PDACC_BY_SERIAL`

- **Query in O9:** 
```sql
SELECT REFAC FROM O9DATA.D_STKINV WHERE STKPRF||STKFR = '@CFRSER' AND STKTYPE = '@CSTKTP'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |
| 2 | Stock type | stock_type | No | String |  |  | `CSTKTP` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  | `REFAC` |

## Trả về thông tin Stock khi nhận "Serial" của Stock type là "Cashier Cheque"
- **Workflow id:** `DPT_GET_INFO_STOCK_BY_CASHIER_CHEQUE`

- **Query in O9:** 
```sql
SELECT INICCR,branchid,intamt,STKBAL,stklvl 
FROM O9DATA.D_STKINV 
WHERE '@CFRSER' BETWEEN STKPRF||STKFR AND STKPRF||STKTO AND STKTYPE = 'G'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency code | currency_code | String | 3 | `INICCR` |
| 2 | Branch id | branch_id | `Number` |  | số nguyên, `branchid` |
| 3 | Stock amount | stock_amount | `Number` |  | số thập phân, `intamt` |
| 4 | Stock balance | stock_balance | `Number` |  | số thập phân, `STKBAL` |
| 5 | No of leaves | no_of_leaves | `Number` |  | số nguyên, `stklvl` |
| 6 | Beneficiary ID number | beneficiary_id_number | String |  | `trả thêm` |
| 7 | Beneficiary address | beneficiary_address | String |  | `trả thêm` |
| 8 | Issued date | issued_date | `Date time` |  | `trả thêm` |
| 9 | Expired date | expired_date | `Date time` |  | `trả thêm` |
| 10 | Beneficiary Name | beneficiary_name | String |  | `trả thêm` |
| 11 | Beneficiary Contact Number | beneficiary_contact_number | String |  | `trả thêm` |

## Trả về thông tin Stock khi nhận "Serial" của Stock type là "Payment Order"
- **Workflow id:** `DPT_GET_INFO_STOCK_BY_PAYMENT_ORDER`

- **Query:** 

Tương tự `DPT_GET_INFO_STOCK_BY_CASHIER_CHEQUE` nhưng STKTYPE = 'O'

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency code | currency_code | String | 3 |  |
| 2 | Branch id | branch_id | `Number` |  | số nguyên |
| 3 | Stock amount | stock_amount | `Number` |  | số thập phân |
| 4 | Stock balance | stock_balance | `Number` |  | số thập phân |
| 5 | No of leaves | no_of_leaves | `Number` |  | số nguyên |
| 6 | Beneficiary ID number | beneficiary_id_number | String |  |  |
| 7 | Beneficiary address | beneficiary_address | String |  |  |
| 8 | Issued date | issued_date | `Date time` |  |  |
| 9 | Expired date | expired_date | `Date time` |  |  |
| 10 | Beneficiary Name | beneficiary_name | String |  | `trả thêm` |
| 11 | Beneficiary Contact Number | beneficiary_contact_number | String |  | `trả thêm` |

## Trả về thông tin adjustment khi nhận "IFC code", "Adjustment amount" và "Account number" của Deposit
- **Workflow id:** `DPT_IFC_GET_INFO_ADJUSTMENT`

- **Query in O9:** 
```sql
SELECT CASE WHEN amt > 0 THEN amt ELSE -LEAST (ABS (amt), intamt) END intamt2, 
       CASE WHEN amt > 0 THEN amt ELSE -LEAST (ABS (amt), intpbl) END intpbl2, 
       CASE WHEN amt > 0 THEN 0 ELSE -GREATEST (ABS (amt) - intamt, 0) END 
           intdue2
  FROM (SELECT '@TXAMT' amt, 
               intdue, 
               intamt, 
               intpre, 
               intpaid, 
               intpbl 
          FROM     o9data.d_ifcbal a 
               INNER JOIN 
                   o9data.d_deposit b 
               ON a.defacno = b.defacno 
         WHERE b.acno = '@PDACC' and a.ifccd='@CIFCCD')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Adjustment amount | adjustment_amount | No | `Number` |  |  |  |
| 2 | Account number | account_number | No | String |  |  | account number của deposit |
| 3 | IFC code | ifc_code | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Adjusted accrual interest | adjusted_accrual_interest | `Number` |  | số có 2 số thập phân, `cintamt` |
| 2 | Adjusted payable interest | adjusted_payable_interest | `Number` |  | số có 2 số thập phân, `cintpbl` |
| 3 | Adjusted due interest | adjusted_due_interest | `Number` |  | số có 2 số thập phân, `cintdue` |

## Trả về danh sách Reference number khi nhận "Reference code" và "Account number" của Deposit
- **Workflow id:** `DPT_ERL_LKP_DATA_REF`

- **Query in O9:** 
```sql
SELECT A.REFNO,A.DPTREF 
FROM O9DATA.D_DPTEMK A 
WHERE A.DEFACNO = O9DPT.SET_ACNO_DEFACNO('@PDACC') AND A.DPTREF = '@CRMDL'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |
| 2 | Reference code | reference_code | No | String |  |  | `CRMDL` |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |
| 4 | Page index | page_index | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Reference number | reference_number | String |  |  |
| 2 | Reference code | reference_code | String |  |  |

## Trả về thông tin catalog khi nhận "Catalogue code" của Deposit
- **Workflow id:** `GET_INFO_DPTCAT`

- **Query in O9:** 
```sql
SELECT A.CATNAME CATNAME, C_CDLIST.CAPTION DPTTYPE, A.DPTGRP, A.DPTPRP,A.RLLOVR,'' 
FROM O9DATA.D_DPTCAT A, C_CDLIST  
WHERE A.DPTTYPE=C_CDLIST.CDID AND C_CDLIST.CDNAME='DPTTYPE' AND A.CATCD = '@CNCAT'  and A.CATCD <> 'FDKTBAAA'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Name | catalog_name | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Deposit type | deposit_type | String |  |  |
| 6 | Deposit type caption | deposit_type_caption | String |  |  |
| 7 | Deposit group | deposit_group | String |  |  |
| 8 | Deposit purpose | deposit_purpose | String |  |  |
| 9 | Rollover option | rollover | String |  |  |
| 10 | Deposit sub type | deposit_sub_type | String |  |  |

## Trả về danh sách DPT Catalogue với "Catalogue code" <> 'FDKTBAAA'
- **Workflow id:** `DPT_OPN_LKP_DATA_CNCAT`

- **Query in O9:** 
```sql
SELECT CATCD, CATNAME FROM O9DATA.D_DPTCAT WHERE CATSTS = 'N' and CATCD <> 'FDKTBAAA' ORDER BY CATCD
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page size | page_size | No | `Number` |  | 0 |  |
| 2 | Page index | page_index | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String |  |  |
| 3 | Name | catalog_name | String |  |  |

## Trả về thông tin Cheque clearing khi nhận "Cheque no" và "Clearing bank"
- **Workflow id:** `DPT_GET_INFO_CHEQUE`

- **Query in O9:** 
```sql
SELECT AMOUNT,CLRAC,O9SYS.O9DPT.SET_DEFACNO_ACNO(DEFACNO) ACNO,OPENFLT, CCRCD, BENCCRCD, BENAMT, VATAMT FROM O9DATA.D_DPTCLR 
WHERE TRIM(CHQNO) = TRIM(UPPER('@CCHQNO')) AND TRIM(CBNK) = TRIM(UPPER('@CIBNK'))
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Cheque no | cheque_no | No | String |  |  | `CCHQNO` |
| 2 | Clearing bank | clearing_bank | No | String |  |  | `CIBNK` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Amount | amount | `Number` |  | `AMOUNT` |
| 2 | Clearing account | clearing_account | String |  | `CLRAC` |
| 3 | Beneficiary account | account_number | String |  | `ACNO` |
| 4 | Open float | open_float | `Number` |  | `OPENFLT` |
| 5 | Currency code | currency_code | String |  | `CCRCD` |
| 6 | Beneficiary currency | beneficiary_currency | String |  | `BENCCRCD` |
| 7 | Beneficiary amount | beneficiary_amount | `Number` |  | `BENAMT` |
| 8 | VAT amount | vat_amount | `Number` |  | `VATAMT` |

## Trả về thông tin Branch ID cần return khi nhận "From serial", "To serial", "Stock type" và "Stock prefix"
- **Workflow id:** `GET_BRANCH_ID_CRT`

- **Query in O9:** 
```sql
SELECT O9SYS.O9STK.GET_BRANCHID_CRT(
SUBSTR('@CFRSER',O9SYS.O9STK.RETURN_PRF('@CSTKTP')+1,TO_NUMBER(LENGTH('@CFRSER')-O9SYS.O9STK.RETURN_PRF('@CSTKTP'))),
SUBSTR('@CTOSER',O9SYS.O9STK.RETURN_PRF('@CSTKTP')+1,TO_NUMBER(LENGTH('@CTOSER')-O9SYS.O9STK.RETURN_PRF('@CSTKTP'))),
'@CSTKPRE',
'@CSTKTP'
) BRANCHID FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | From serial | from_serial | No | String |  |  | `CFRSER` |
| 2 | To serial | to_serial | No | String |  |  | `CTOSER` |
| 3 | Stock type | stock_type | No | String |  |  | `CSTKTP` |
| 4 | Stock prefix | stock_prefix | No | String |  |  | `CSTKPRE` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch id | branch_id | `Number` |  | `BRANCHID` |

## Trả về thông tin Earmark amount khi nhận "Serial"
- **Workflow id:** `DPT_GET_INFO_EMK_BY_SERIAL`

- **Query in O9:** 
```sql
SELECT DEFACNO,AMT,NVL(AMT,0) - NVL(CLRAMT,0) FROM O9DATA.D_DPTEMK WHERE DPTREF = 'C'||'@CFRSER'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  | `DEFACNO` |
| 2 | Amount | amount | `Number` |  | `AMT` |
| 3 | Amount - Clear amount | amount_clear_amount | `Number` |  | `NVL(AMT,0) - NVL(CLRAMT,0)` |

## Trả về thông tin Number of leaf trong Cheque book issued  khi nhận "From serial", "To serial" và "Stock type"
- **Workflow id:** `DPT_CIS_GET_INFO_NO_LEAF`

- **Query in O9:** 
```sql
SELECT SUBSTR('@CTOSER',O9SYS.O9STK.RETURN_PRF('@CSTKTP') + 1,TO_NUMBER(LENGTH('@CTOSER')-O9SYS.O9STK.RETURN_PRF('@CSTKTP')))
-SUBSTR('@CFRSER',O9SYS.O9STK.RETURN_PRF('@CSTKTP') + 1,TO_NUMBER(LENGTH('@CFRSER')-O9SYS.O9STK.RETURN_PRF('@CSTKTP')))
+1 NOLEAF FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | From serial | from_serial | No | String |  |  | `CFRSER` |
| 2 | To serial | to_serial | No | String |  |  | `CTOSER` |
| 3 | Stock type | stock_type | No | String |  |  | `CSTKTP` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | No leaf | no_leaf | `Number` |  |  |

## Trả về thông tin Number of leaf trong Stock registration khi nhận "From serial", "To serial", "Stock type" và "Number of book"
- **Workflow id:** `DPT_SRG_GET_INFO_NO_LEAF`

- **Query in O9:** 
```sql
SELECT (
SUBSTR('@CTOSER',O9SYS.O9STK.RETURN_PRF('@CSTKTP') + 1,TO_NUMBER(LENGTH('@CTOSER')-O9SYS.O9STK.RETURN_PRF('@CSTKTP')))
- SUBSTR('@CFRSER',O9SYS.O9STK.RETURN_PRF('@CSTKTP') + 1,TO_NUMBER(LENGTH('@CTOSER')-O9SYS.O9STK.RETURN_PRF('@CSTKTP')))
 + 1 )
/'@CAMT1' C_UKN_AMT2 FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | From serial | from_serial | No | String |  |  | `CFRSER` |
| 2 | To serial | to_serial | No | String |  |  | `CTOSER` |
| 3 | Stock type | stock_type | No | String |  |  | `CSTKTP` |
| 4 | Number of leaves | `no_book` đổi thành `no_of_leaves` | No | `Number` |  |  | `CAMT1` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Number of book | `no_leaf` đổi thành `no_of_books` | `Number` |  |  |

## Trả về thông tin Number of book trong Stock registration khi nhận "From serial", "To serial", "Stock type" và "Number of leaf"
- **Workflow id:** `DPT_SRG_GET_INFO_NO_BOOK`

- **Query in O9:** 
```sql
SELECT (
SUBSTR('@CTOSER',O9SYS.O9STK.RETURN_PRF('@CSTKTP') + 1,TO_NUMBER(LENGTH('@CTOSER')-O9SYS.O9STK.RETURN_PRF('@CSTKTP')))- SUBSTR('@CFRSER',O9SYS.O9STK.RETURN_PRF('@CSTKTP') + 1,TO_NUMBER(LENGTH('@CFRSER')-O9SYS.O9STK.RETURN_PRF('@CSTKTP')))
 + 1 )
 /'@C_UKN_AMT2' CAMT1 FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | From serial | from_serial | No | String |  |  | `CFRSER` |
| 2 | To serial | to_serial | No | String |  |  | `CTOSER` |
| 3 | Stock type | stock_type | No | String |  |  | `CSTKTP` |
| 4 | Number of book | `no_leaf` đổi thành `no_of_books` | No | String |  |  | `C_UKN_AMT2` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Number of leaves | `no_book` đổi thành `no_of_leaves` | `Number` |  |  |

## Trả về thông tin Holding amount khi nhận "Reference code", "Reference number" và "Account number" của Deposit
- **Workflow id:** `DPT_GET_INFO_HOLDING_AMT`

- **Query in O9:** 
```sql
SELECT AMT-CLRAMT,AMT-CLRAMT FROM O9DATA.D_DPTEMK 
WHERE DEFACNO = O9DPT.SET_ACNO_DEFACNO('@PDACC') AND DPTREF ='@CRMDL' AND REFNO='@CRNUM'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |
| 2 | Reference code | reference_code | No | String |  |  | `CRMDL` |
| 3 | Reference number | reference_number | No | String |  |  | `CRNUM` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Amount - Clear amount 01 | amount_clear_amount01 | `Number` |  | `AMT-CLRAMT` |
| 2 | Amount - Clear amount 02 | amount_clear_amount02 | `Number` |  | `AMT-CLRAMT` |
| 3 | Expired date | expired_date | `Date time` | | `trả thêm` |

## Trả về thông tin Stock no khi nhận "Account number" của Deposit
- **Workflow id:** `GET_STOCK_NO`

- **Query in O9:** 
```sql
SELECT O9SYS.O9STK.GET_STKNO('@PDACC',SUBSTR('@CFRSER',TO_NUMBER(LENGTH('@CSTKPRE')+1), TO_NUMBER(LENGTH('@CFRSER') - LENGTH('@CSTKPRE'))),'@CSTKPRE','@CSTKTP') FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Serial | serial | No | String |  |  | `CFRSER` |
| 2 | Stock prefix | stock_prefix | String |  |  | `CSTKPRE` |
| 3 | Stock type | stock_type | No | String |  |  | `CSTKTP` |
|  | Account number |  | No | String |  |  | `PDACC` không sử dụng nên không cần gửi đi |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Stock no | stock_no | String |  | `stk_no` |

## Trả về danh sách Deposit account (CASA) khi nhận "Branch id" và "Account number" của Deposit
- **Workflow id:** `DPT_LKP_DATA_PDOACC`

- **Query in O9:** 
```sql
SELECT ACNO, ACNAME 
FROM O9DATA.D_DEPOSIT 
WHERE DPTTYPE IN ('S','C') 
AND DPTSTS IN ('N','D') 
AND BRANCHID = '@CRRBRID'    
AND CCRCD = (SELECT CCRCD FROM O9DATA.D_DEPOSIT WHERE ACNO = '@PDMACC')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | branch_id | No | `Number` |  |  |  |
| 2 | Account number | account_number | No | String |  |  | Account number của Deposit |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |
| 4 | Page size | page_size | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |

## Trả về thông tin account linkage với LKTYPE='D' khi nhận "Account number" của Deposit
- **Workflow id:** `DPT_GET_INFO_ACLINK`

- **Query in O9:** 
```sql
SELECT ACNO,
O9SYS.O9DPT.GET_AVAILABLE_BALANCE('@SACNO') AMT,
CCRCD 
FROM D_DEPOSIT 
WHERE DEFACNO = (
SELECT DISTINCT LACNO 
FROM O9DATA.d_aclinkage 
WHERE LKTYPE='D' 
AND  MACNO = o9dpt.set_acno_defacno('@SACNO')
)
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  | `Account number linkage` |
| 2 | Available balance  | available_balance | `Number` |  |  |
| 3 | Currency code | currency_code | String |  |  |

## Trả về danh sách Deposit account khi nhận "Mater FD account number" của Deposit
- **Workflow id:** `DPT_LKP_DATA_PDACC`

- **Query in O9:** 
```sql
SELECT D_DEPOSIT.ACNO, D_DEPOSIT.ACNAME TXBACNAME  
FROM D_DEPOSIT  
WHERE D_DEPOSIT.DPTGRP = '24' 
AND D_DEPOSIT.MACNO = '@PDMACC' 
AND D_DEPOSIT.DPTSTS NOT IN ('C','P')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Master FD account number | master_fd_account_number | No | String |  |  | Master FD account number của Deposit |
| 2 | Page index | page_index | No | `Number` |  | 0 |  |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |

## Tạo API trả về thông tin master FD account khi nhận "Account number" của Deposit và "Branch id"
- **Workflow id:** `DPT_GET_INFO_PDMACC`

- **Query in O9:** 
```sql
SELECT A.ACNAME, B.CATCD, B.CATNAME, B.DPTTYPE, A.CTMTYPE, O9SYS.O9CTM.SET_CUSTOMERID_CUSTOMERCD(A.CUSTOMERID,A.CTMTYPE), O9SYS.O9COM.GET_ALL_FULLNAME(A.CUSTOMERID,A.CTMTYPE), B.DPTGRP
FROM O9DATA.D_DEPOSIT A
INNER JOIN 
(SELECT A.CATID CATID, A.CATCD CATCD,A.CATNAME CATNAME, C_CDLIST.CAPTION DPTTYPE, '24' AS DPTGRP, A.DPTPRP FROM O9DATA.D_DPTCAT A, C_CDLIST  
WHERE A.DPTTYPE=C_CDLIST.CDID AND C_CDLIST.CDNAME='DPTTYPE' ) B
ON A.CATID = B.CATID
WHERE A.ACNO = '@PDMACC' AND A.DPTGRP = '23' AND A.DPTPRP = 'T' AND A.DPTTYPE = 'T' AND A.DPTSTS NOT IN ('P','C')   AND A.BRANCHID = '@CRRBRID'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  |  |
| 2 | Bracnh id | bracnh_id | No | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Account name | account_name | String |  |  |
| 4 | Catalogue code | catalog_code | String |  |  |
| 5 | Catalogue name | catalog_name | String |  |  |
| 6 | Deposit type | deposit_type | String |  |  |
| 7 | Deposit type caption | deposit_type_caption | String |  |  |
| 8 | Customer type | customer_type | String |  |  |
| 9 | Customer code | customer_code | String |  |  |
| 10 | Fullname | full_name | String |  |  |
| 11 | Account type | deposit_group | `Number` |  |  |

## Trả về thông tin redemption interest khi nhận "Account number" của Deposit
- **Workflow id:** `DPT_GET_REDEMPTION_INTEREST`

- **Query in O9:** 
```sql
SELECT NVL(O9DPT.REDEMPTION_INTEREST(PACNO=>'@PDACC'),0) FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Redemption interest | redemption_interest | `Number` |  |  |

## Trả về thông tin account linkage với LKTYPE='D' AND LKCLASS ='F' khi nhận "Account number" và "Master FD account" của Deposit
- **Workflow id:** `DPT_CWT_GET_INFO_ACLINK`

- **Query in O9:** 
```sql
SELECT ACNO 
FROM D_DEPOSIT 
WHERE DEFACNO = (
SELECT LACNO 
FROM O9DATA.D_ACLINKAGE 
WHERE LKTYPE='D' 
AND LKCLASS ='F' 
AND  MACNO = (
SELECT DEFACNO 
FROM D_DEPOSIT 
WHERE ACNO='@PDACC' 
AND MACNO = '@PDMACC' 
AND DPTSTS NOT IN ('C','P')
)
)
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |
| 2 | Master FD account number | master_account_number | No | String |  |  | `PDMACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number linkage | account_number_linkage | String |  |  |

## Trả về thông tin Master FD account khi nhận "Catalogue code" của Deposit, "Customer code" và "Customer type"
- **Workflow id:** `DPT_OPN_GET_INFO_PDMACC`

- **Query in O9:** 
```sql
SELECT NVL(MAX(acno),'') FROM (SELECT acno, acname FROM     o9data.d_deposit d 
INNER JOIN o9sys.v_customeracib v 
ON     v.customerid = d.customerid AND v.ctmtype = d.ctmtype AND dptgrp = '23' AND dpttype = 'T' 
AND ccrcd in (select ccrcd from o9data.d_dptcat where CATCD='@CNCAT' ) 
AND v.CUSTOMERCD='@CCTMCD' AND v.ctmtype ='@CCTMT' order by d.opndt desc) where rownum=1
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  |  |
| 2 | Customer code | customer_code | No | String |  |  |  |
| 3 | Customer type | customer_type | No | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Account number | account_number | String |  |  |

## Trả về danh sách DPT master khi nhận "Catalogue code" của Deposit, "Customer code" và "Customer type"
- **Workflow id:** `DPT_OPN_LKP_DATA_PDMACC`

- **Query in O9:** 
```sql
SELECT acno, acname 
FROM     o9data.d_deposit d 
INNER JOIN o9sys.v_customeracib v 
ON     v.customerid = d.customerid AND v.ctmtype = d.ctmtype AND dptgrp = '23' AND dpttype = 'T' AND dptsts <> 'C' 
AND ccrcd in (select ccrcd from o9data.d_dptcat where CATCD='@CNCAT' AND DPTTYPE NOT IN ('C','S')) 
AND v.CUSTOMERCD='@CCTMCD' AND v.ctmtype ='@CCTMT' order by d.opndt desc
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  |  |
| 2 | Customer code | customer_code | No | String |  |  |  |
| 3 | Customer type | customer_type | No | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 4 | Page size | page_size | No | `Number` |  | 0 |  |
| 5 | Page index | page_index | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Account name | account_name | String |  |  |

## Trả về danh sách DPT CASA khi nhận "Catalogue code" của Deposit, "Customer code", "Customer type" và "Linkage class"
- **Workflow id:** `DPT_OPN_LKP_DATA_PDOACC`

- **Query in O9:** 
```sql
SELECT acno, acname 
FROM     o9data.d_deposit d 
INNER JOIN o9sys.v_customeracib v 
ON     v.customerid = d.customerid AND v.ctmtype = d.ctmtype AND dpttype IN ('C','S') AND dptsts <> 'C' 
AND ccrcd in (select ccrcd from o9data.d_dptcat where CATCD='@CNCAT'  AND '@LKCLASS' <> 'N' ) 
AND v.CUSTOMERCD='@CCTMCD' AND v.ctmtype ='@CCTMT' order by d.opndt desc
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  |  |
| 2 | Customer code | customer_code | No | String |  |  |  |
| 3 | Customer type | customer_type | No | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 4 | Linkage class | linkage_class | No | String |  |  |  |
| 5 | Page size | page_size | No | `Number` |  | 0 |  |
| 6 | Page index | page_index | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Account name | account_name | String |  |  |

## Trả về thông tin account và customer khi nhận "Account number" của Deposit
- **Workflow id:** `DPT_GET_INFO_CUSTOMER`

- **Query in O9:** 
```sql
SELECT D_DEPOSIT.CCRCD ||'-'|| D_DEPOSIT.ACNAME,D_DEPOSIT.BALANCE,D_DEPOSIT.CCRCD,D_DEPOSIT.INTAMT,D_DEPOSIT.INTDUE,D_DEPOSIT.INTPBL,D_DEPOSIT.CTMTYPE,
D_DPTCAT.CATCD,NVL(D_DPTCAT.MINAMT,0),
O9SYS.O9COM.GET_ALL_FULLNAME(D_DEPOSIT.CUSTOMERID,D_DEPOSIT.CTMTYPE) FULLNAME,V_CUSTOMERINFO.ADDRESS,V_CUSTOMERINFO.REPID,V_CUSTOMERINFO.CUSTOMERCD CUSTOMERCD,
D_DEPOSIT.CCRCD||' - '||O9SYS.O9COM.GET_ALL_FULLNAME(D_DEPOSIT.CUSTOMERID,D_DEPOSIT.CTMTYPE),
O9SYS.O9DPT.GET_AVAILABLE_BALANCE('@PDACC')
FROM D_DEPOSIT, O9SYS.V_CUSTOMERINFO, D_DPTCAT  
WHERE D_DEPOSIT.CUSTOMERID= V_CUSTOMERINFO.CUSTOMERID 
AND D_DEPOSIT.CATID = D_DPTCAT.CATID AND D_DEPOSIT.CTMTYPE = V_CUSTOMERINFO.CTMTYPE AND D_DEPOSIT.ACNO = '@PDACC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency account name | currency_account_name | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |
| 4 | Current balance | current_balance | `Number` |  |  |
| 5 | Available balance  | available_balance | `Number` |  |  |
| 6 | Interest payable/receiveble | intpbl | `Number` |  |  |
| 7 | Interest accrual | interest_accrual | `Number` |  |  |
| 8 | Interest due | interest_due | `Number` |  |  |
| 9 | Customer type | customer_type | String |  |  |
| 10 | Customer code | customer_code | String |  |  |
| 11 | Full name | full_name | String |  |  |
| 12 | Address | address | String |  |  |
| 13 | Paper number | paper_number | String |  |  |
| 14 | Currency full name | currency_full_name | String |  |  |
| 15 | Minimum deposit amount | minimum_deposit_amount | `Number` |  |  |
| 16 | Catalogue code | catalog_code | String |  |  |

## Trả về account và customer khi nhận "Account number" của Deposit có DPTTYPE IN ('C','S')
- **Workflow id:** `GET_INFO_ACCOUNT_BY_ACNO_DPTTYPE`

- **Query in O9:** 
```sql
SELECT D_DEPOSIT.BALANCE,O9SYS.O9DPT.GET_AVAILABLE_BALANCE(ACNO) AVAILABLE_BALANCE,
D_DEPOSIT.CCRCD || '-' || O9SYS.O9COM.GET_ALL_FULLNAME(V_CUSTOMERINFO.CUSTOMERID,V_CUSTOMERINFO.CTMTYPE) TXBACNAME,
V_CUSTOMERINFO.CUSTOMERCD,O9SYS.O9COM.GET_ALL_FULLNAME(V_CUSTOMERINFO.CUSTOMERID,V_CUSTOMERINFO.CTMTYPE) ACNAME,
V_CUSTOMERINFO.ADDRESS,
D_DEPOSIT.CCRCD 
FROM D_DEPOSIT, O9SYS.V_CUSTOMERINFO, D_DPTCAT 
WHERE  D_DEPOSIT.CUSTOMERID = V_CUSTOMERINFO.CUSTOMERID AND V_CUSTOMERINFO.CTMTYPE = D_DEPOSIT.CTMTYPE  AND D_DEPOSIT.CATID = D_DPTCAT.CATID AND D_DEPOSIT.ACNO = '@PDACC' AND D_DEPOSIT.DPTTYPE IN ('C','S')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency account name | currency_account_name | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Current balance | current_balance | `Number` |  |  |
| 4 | Available balance  | available_balance | `Number` |  |  |
| 5 | Currency code | currency_code | String |  |  |
| 6 | Customer type | customer_type | String |  |  |
| 7 | Customer code | customer_code | String |  |  |
| 8 | Full name | full_name | String |  |  |
| 9 | Address | address | String |  |  |
| 10 | Paper number | paper_number | String |  |  |
| 11 | Currency full name | currency_full_name | String |  |  |

## Trả về account và customer khi nhận "Account number" và "Master FD account number" của Deposit
- **Workflow id:** `DPT_GET_INFO_CUSTOMER_TIMEDPT`

- **Query in O9:** 
```sql
SELECT D_DEPOSIT.CCRCD || '-' || D_DEPOSIT.ACNAME TXBACNAME,
D_DEPOSIT.BALANCE,
O9SYS.O9DPT.GET_AVAILABLE_BALANCE(ACNO) AVAILABLE_BALANCE,
D_DEPOSIT.ACNAME,
V_CUSTOMERINFO.CUSTOMERCD,
D_DEPOSIT.ACNAME ACNAME,
V_CUSTOMERINFO.ADDRESS,
D_DEPOSIT.CCRCD 
FROM D_DEPOSIT, O9SYS.V_CUSTOMERINFO 
WHERE  D_DEPOSIT.CUSTOMERID = V_CUSTOMERINFO.CUSTOMERID 
AND V_CUSTOMERINFO.CTMTYPE = D_DEPOSIT.CTMTYPE  
AND D_DEPOSIT.ACNO = '@PDACC'
AND D_DEPOSIT.DPTGRP = '24' 
AND D_DEPOSIT.MACNO = '@PDMACC' 
AND D_DEPOSIT.DPTSTS NOT IN ('C','P')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |
| 2 | Master account number | master_account_number | No | String |  |  | `PDMACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency account name | currency_account_name | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Current balance | current_balance | `Number` |  |  |
| 4 | Available balance  | available_balance | `Number` |  |  |
| 5 | Currency code | currency_code | String |  |  |
| 6 | Customer type | customer_type | String |  |  |
| 7 | Customer code | customer_code | String |  |  |
| 8 | Full name | full_name | String |  |  |
| 9 | Address | address | String |  |  |
| 10 | Paper number | paper_number | String |  |  |

## Trả về account name và các amount khi nhận "Account number" của Deposit
- **Workflow id:** `GET_INFO_DPTACC`

- **Query in O9:** 
```sql
SELECT CCRCD ||'-'|| ACNAME,ACNAME,
BALANCE,
O9SYS.O9DPT.GET_AVAILABLE_BALANCE('@PDACC'),
CCRCD,DPTSTS,CLSDT,CATID,BLKBY,
CUSTOMERID,BRANCHID,
INTPBL,
INTAMT,
INTPRE,
INTDUE,
INTPBL+INTDUE,
INTOVD,
INTSPAMT,
INTNYPD,
INTPBL+INTNYPD+INTOVD+INTDUE,
INTAMT-INTPBL
FROM D_DEPOSIT WHERE ACNO = '@PDACC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency account name | currency_account_name | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Current balance | current_balance | `Number` |  |  |
| 4 | Available balance  | available_balance | `Number` |  |  |
| 5 | Currency code | currency_code | String |  |  |
| 6 | Account status | deposit_status | String |  |  |
| 7 | Account status caption | deposit_status_caption | String |  |  |
| 8 | Catalogue id | catalog_id | `Number` |  |  |
| 9 | Close date | close_date | `Date time` |  |  |
| 10 | Block by | blkby | String |  |  |
| 11 | Account holder | customer_id | `Number` |  |  |
| 12 | Branch id | branch_id | `Number` |  |  |
| 13 | Interest accrual | interest_accrual | `Number` |  |  |
| 14 | Interest payable/receiveble | intpbl | `Number` |  |  |
| 15 | Interest prepaid | intpre | `Number` |  |  |
| 16 | Interest payable + Interest due | intpbl_interest_due | `Number` |  | `INTPBL+INTDUE` |
| 17 | Interest due | interest_due | `Number` |  |  |
| 18 | Interest overdue, not paid | intovd | `Number` |  |  |
| 19 | Interest suspense | intspamt | `Number` |  |  |
| 20 | Interest not paid | interest_not_paid | `Number` |  | `INTNYPD` |
| 21 | Total interest amount | total_interest_amount | `Number` |  | `INTPBL+INTNYPD+INTOVD+INTDUE` |
| 22 | Interest accrual - Interest payable | interest_accrual_intpbl | `Number` |  | `INTAMT-INTPBL` |
| 23 | Catalogue code | catalog_code | String |  | `trả thêm` |
| 24 | Customer segmentation | account_type | String |  | `trả thêm` |
| 25 | User created | user_created | String |  | `trả thêm` |

## Trả về danh sách "Deposit account" khi nhận "Master FD Account"
- **Workflow id:** `DPT_LOOKUP_DEPOSIT_BY_MACNO`

- **Query in O9:** 
```sql
SELECT D_DEPOSIT.ACNO, D_DEPOSIT.ACNAME TXBACNAME
FROM D_DEPOSIT  
WHERE D_DEPOSIT.DPTGRP = '24' 
AND D_DEPOSIT.DPTSTS NOT IN ('C','P')
AND D_DEPOSIT.MACNO = '@PDMACC' 
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 2 | Page size | page_size | No | `Number` |  |  | số nguyên dương |
| 3 | Master FD account number | master_fd_account_number | No | String |  |  | `PDMACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | account_name | String |  |  |

## Trả về danh sách Master FD account khi nhận "Branch id"
- **Workflow id:** `DPT_LKP_DATA_PDMACC`

- **Query in O9:** 
```sql
SELECT ACNO, ACNAME 
FROM O9DATA.D_DEPOSIT 
WHERE 
DPTGRP = '23' 
AND DPTPRP = 'T' 
AND DPTTYPE = 'T' 
AND DPTSTS NOT IN ('P','C')  
AND BRANCHID = '@CRRBRID'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Bracnh id | branch_id | No | `Number` |  |  |  | 
| 2 | Page index | page_index | No | `Number` |  | 0 |  |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | account_name | String |  |  |

## Trả về account name khi nhận "Account number" của Deposit, "Branch id" và "Master FD Account number" của Deposit
- **Workflow id:** `DPT_OPT_GET_INFO_PDOACC`

- **Query in O9:** 
```sql
SELECT ACNAME AS TXB, ACNAME AS TXBODY 
FROM O9DATA.D_DEPOSIT 
WHERE DPTTYPE IN ('S','C') AND DPTSTS IN ('N','D') AND BRANCHID = '@CRRBRID' AND ACNO = '@PDOACC' 
AND CCRCD = (SELECT CCRCD FROM O9DATA.D_DEPOSIT WHERE ACNO = '@PDMACC')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDOACC` |
| 2 | Bracnh id | bracnh_id | No | `Number` |  |  |  |
| 3 | Master account number | master_account_number | No | String |  |  | `PDMACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account name 01 | account_name01 | String |  |  |
| 2 | Account name 02 | account_name02 | String |  |  |

## Trả về thông tin withholding tax khi nhận "Account number" của Deposit
- **Workflow id:** `DPT_GET_INFO_WHTAMT`

- **Query in O9:** 
```sql
select NVL(sum(AMT),0) from o9data.d_ifcbal where ifccd in(select ifccd from o9data.d_ifclst where ifcsubtype='TW') and defacno=(select defacno from o9data.d_deposit where acno= '@PDACC')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Withholding tax | withholding_tax | `Number` |  |  |

## Trả về thông tin Interest without whtax khi nhận "Account number" của Deposit và "Interest amount"
- **Workflow id:** `DPT_GET_INTEREST_WITHOUT_WHTAX`

- **Query in O9:** 
```sql
SELECT NVL(O9SYS.O9DPT.CAL_INTEREST_WITHOUT_WHTAX('@CTINT','@PDACC'),0) FROM DUAL

```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |
| 2 | Interest amount | interest_amount | No | `Number` |  |  | `CTINT` or `PCSAMT` or `TXAMT` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Interest without whtax | interest_without_whtax | `Number` |  |  |

## Trả về account name, catalog và customer time deposit cần approve by cash khi nhận "Account number" của Deposit
- **Workflow id:** `DPT_AOPC_GET_INFO_PDACC`

- **Query in O9:** 
```sql
SELECT CCRCD||'-'||ACNAME,
       d.ctmtype,
       o9ctm.set_customerid_customercd (d.customerid, d.ctmtype) customercd,
       o9dpt.set_catid_catcd ('D_DPTCAT', catid) catcd,
       d.macno,
       (SELECT e.catname
          FROM o9data.d_dptcat e
         WHERE e.catid = d.catid)
           catname,
       d.dpttype,
       dptgrp,
       acname,
       d.dptsubtype,
       d.balance,
       d.CCRCD,
       TO_NUMBER(SUBSTR(acno,9,6) )seq
  FROM o9data.d_deposit d
  LEFT JOIN O9CODE.C_CDLIST CD1 ON D.dpttype = CD1.CDID AND CD1.cdname='DPTTYPE' AND CD1.CDGRP ='DPT'
  INNER JOIN O9DATA.D_TXINV TX ON TX.TXCODE='DPT_OPC' and TX.str1=d.acno
 WHERE acno = '@PDACC' and dptgrp='24' and dptsts='W'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDOACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency account name | currency_account_name | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Current balance | current_balance | `Number` |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Deposit type | deposit_type | String |  |  |
| 6 | Account type | deposit_group | `Number` |  |  |
| 7 | Deposit sub type | deposit_sub_type | String |  |  |
| 8 | Master FD account | macno | String |  |  |
| 9 | Customer type | customer_type | String |  |  |
| 10 | Customer code | customer_code | String |  |  |
| 11 | Full name | full_name | String |  |  |
| 12 | Address | address | String |  |  |
| 13 | Paper number | paper_number | String |  |  |
| 14 | Catalogue code | catalog_code | String |  |  |
| 15 | Catalogue name | catalog_name | String |  |  |
| 16 | Sequence number | sequence_number | `Number` |  |  |

## Trả về account name, catalog và customer time deposit cần approve by miscellaneous khi nhận "Account number" của Deposit
- **Workflow id:** `DPT_AOPM_GET_INFO_PDACC`

- **Query in O9:** 
```sql
SELECT CCRCD||'-'||ACNAME,
       d.ctmtype,
       o9ctm.set_customerid_customercd (d.customerid, d.ctmtype) customercd,
       o9dpt.set_catid_catcd ('D_DPTCAT', catid) catcd,
       d.macno,
       (SELECT e.catname
          FROM o9data.d_dptcat e
         WHERE e.catid = d.catid)
           catname,
       d.dpttype,
       dptgrp,
       acname,
       d.dptsubtype,
       d.balance,
       d.CCRCD,
       TO_NUMBER(SUBSTR(acno,9,6) )seq
  FROM o9data.d_deposit d
  LEFT JOIN O9CODE.C_CDLIST CD1 ON D.dpttype = CD1.CDID AND CD1.cdname='DPTTYPE' AND CD1.CDGRP ='DPT'
  INNER JOIN O9DATA.D_TXINV TX ON TX.TXCODE='DPT_OPM' and TX.str1=d.acno
 WHERE acno = '@PDACC' and dptgrp='24' and dptsts='W'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDOACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency account name | currency_account_name | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Current balance | current_balance | `Number` |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Deposit type | deposit_type | String |  |  |
| 6 | Account type | deposit_group | `Number` |  |  |
| 7 | Deposit sub type | deposit_sub_type | String |  |  |
| 8 | Master FD account | macno | String |  |  |
| 9 | Customer type | customer_type | String |  |  |
| 10 | Customer code | customer_code | String |  |  |
| 11 | Full name | full_name | String |  |  |
| 12 | Address | address | String |  |  |
| 13 | Paper number | paper_number | String |  |  |
| 14 | Catalogue code | catalog_code | String |  |  |
| 15 | Catalogue name | catalog_name | String |  |  |
| 16 | Sequence number | sequence_number | `Number` |  |  |

## Trả về account name, catalog và customer time deposit cần approve by transfer khi nhận "Account number" của Deposit
- **Workflow id:** `DPT_AOPT_GET_INFO_PDACC`

- **Query in O9:** 
```sql
SELECT CCRCD||'-'||ACNAME,
       d.ctmtype,
       o9ctm.set_customerid_customercd (d.customerid, d.ctmtype) customercd,
       o9dpt.set_catid_catcd ('D_DPTCAT', catid) catcd,
       d.macno,
       (SELECT e.catname
          FROM o9data.d_dptcat e
         WHERE e.catid = d.catid)
           catname,
       d.dpttype,
       dptgrp,
       acname,
       d.dptsubtype,
       d.balance,
       d.CCRCD,
       TO_NUMBER(SUBSTR(acno,9,6) )seq
  FROM o9data.d_deposit d
  LEFT JOIN O9CODE.C_CDLIST CD1 ON D.dpttype = CD1.CDID AND CD1.cdname='DPTTYPE' AND CD1.CDGRP ='DPT'
  INNER JOIN O9DATA.D_TXINV TX ON TX.TXCODE='DPT_OPT' and TX.str1=d.acno
 WHERE acno = '@PDACC' and dptgrp='24' and dptsts='W'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDOACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency account name | currency_account_name | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Current balance | current_balance | `Number` |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Deposit type | deposit_type | String |  |  |
| 6 | Account type | deposit_group | `Number` |  |  |
| 7 | Deposit sub type | deposit_sub_type | String |  |  |
| 8 | Master FD account | macno | String |  |  |
| 9 | Customer type | customer_type | String |  |  |
| 10 | Customer code | customer_code | String |  |  |
| 11 | Full name | full_name | String |  |  |
| 12 | Address | address | String |  |  |
| 13 | Paper number | paper_number | String |  |  |
| 14 | Catalogue code | catalog_code | String |  |  |
| 15 | Catalogue name | catalog_name | String |  |  |
| 16 | Sequence number | sequence_number | `Number` |  |  |

## Trả về account name và các amount khi nhận "Account number" của Deposit loại là CASA
- **Workflow id:** `DPT_GET_INFO_DPTACC_CASA`

- **Query in O9:** 
```sql
SELECT CCRCD ||'-'|| ACNAME,ACNAME,CCRCD,branchid FROM D_DEPOSIT WHERE ACNO = '@PDACC'  AND DPTTYPE IN ('C','S')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | `PDACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency account name | currency_account_name | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |
| 4 | Branch id | branch_id | `Number` |  |  |

## Trả về danh sách account (Deposit or Accounting) khi nhận "Branch ID", "Method" và "Currency code"
- **Workflow id:** `DPT_LKP_DATA_SACNO`

- **Query in O9:** 
```sql
SELECT bacno, acname, ccrcd
  FROM v_acchrt_deposit
 WHERE (   (    isleave = 'Y'
            AND isdirect = 'Y'
            AND dptsts IS NULL
            AND dpttype IS NULL
            AND branchid = '@CRRBRID'
            AND recby = '@PAYBY'
             AND ccrcd = '@CCCR'
           )
        OR (    isleave IS NULL
            AND isdirect IS NULL
            AND dptsts IN ('A', 'N', 'W')
            AND dpttype IN ('C', 'S')
            AND branchid = '@CRRBRID'
            AND recby = '@PAYBY'
            AND ccrcd = '@CCCR'))
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | branch_id | No | `Number` |  |  | `CRRBRID` |
| 2 | Method | method | No | String |  | `DPT` or `ACT` | `PAYBY` |
| 3 | Currency code | currency_code | No | String |  |  | `CCCR` |
| 4 | Page index | page_index | No | `Number` |  |  |  |
| 5 | Page size | page_size | No | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |

## Trả về thông tin account linkage với LKTYPE='C' và LKCLASS='O' khi nhận "Account number" của Deposit
- **Workflow id:** `DPT_GET_INFO_CREDITAC`

- **Query in O9:** 
```sql
SELECT C.ACNO, 
C.CCRCD ||'-'|| C.ACNAME, 
C.BALANCE,
C.INTPBL AS CIPBL, 
C.INTDUE, 
C.INTAMT, 
C.INTPAID, 
C.BALANCE, 
C.INTPBL 
FROM O9DATA.D_ACLINKAGE L 
INNER JOIN O9DATA.D_DEPOSIT D 
ON L.MACNO = D.DEFACNO 
INNER JOIN (SELECT * FROM O9DATA.D_CREDIT WHERE CRDSTS <> 'C') C 
ON L.LACNO = C.DEFACNO 
WHERE L.LKTYPE = 'C' 
AND L.LKCLASS = 'O' 
AND D.ACNO = '@PDACC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Deposit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  | `C.ACNO` |
| 2 | Account name | account_name | String |  | `C.ACNAME` |
| 3 | Currency account name | currency_account_name | String |  | `C.CCRCD - C.ACNAME` |
| 4 | Outstanding balance | balance | `Number` |  | số thập phân, `C.BALANCE` |
| 5 | Interest receivable | interest_receivable | `Number` |  | số thập phân, `C.INTPBL` |
| 6 | Interest accrual amount | interest_amount | `Number` |  | số thập phân, `C.INTAMT` |
| 7 | Interest due | interest_due | `Number` |  | số thập phân, `C.INTDUE` |
| 8 | Interest paid | interest_paid | `Number` |  | số thập phân, `C.INTPAID` |


## Trả về thông tin account (Deposit or Accounting) khi nhận "Branch ID", "Method", "Currency code" và "Account number"
- **Workflow id:** `DPT_GET_INFO_SACNO`

- **Query in O9:** 
```sql
SELECT acname, ccrcd FROM o9sys.v_acchrt_deposit 
WHERE (
   (
       isleave = 'Y' 
       AND isdirect = 'Y' 
       AND dptsts IS NULL 
       AND dpttype IS NULL 
       AND branchid = '@CRRBRID' 
       AND recby = '@PAYBY') 
       OR (    
       isleave IS NULL 
       AND isdirect IS NULL 
       AND dptsts IN ('A', 'N', 'W') 
       AND dpttype IN ('C', 'S') 
       AND recby = '@PAYBY')
       ) 
       AND BACNO='@PGACC' AND CCRCD='@PCSCCR'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | branch_id | No | `Number` |  |  | `CRRBRID` |
| 2 | Method | method | No | String |  | `DPT` or `ACT` | `PAYBY` |
| 3 | Currency code | currency_code | No | String |  |  | `PCSCCR` |
| 4 | Account number | account_number | No | String |  |  | `PGACC` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account name | account_name | String |  |  |
| 2 | Currency code | currency_code | String |  |  |

## Trả về danh sách deposit account có deposit loại là CASA
- **Workflow id:** `DPT_LOOKUP_DEPOSIT_CASA`

- **Query in O9:** 
```sql
select acno, acname, ccrcd from o9data.d_deposit where dptsts not in ('C','S','B')    and dpttype IN ('C','S')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 4 | Page index | page_index | No | `Number` |  |  |  |
| 5 | Page size | page_size | No | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String |  |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency code | currency_code | String |  |  |