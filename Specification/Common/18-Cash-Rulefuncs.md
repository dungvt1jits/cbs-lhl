# 1. Cash module
## Trả về Description trong giao dịch "Internal Cash Movement"
- **Workflow id:** `CSH_RULEFUNC_MOV_GET_INFO_DESCS`

- **Query in O9:**
```sql
select '@FR_USER_NAME'||' move ' || '@TXAMT' ||' '|| '@CACCCR' || ' cash to ' || '@TO_USER_NAME' from dual
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | From user name | from_user_name | No | String |  |  |  |
| 2 | Amount | move_amount | No | `Number` |  |  | số có 2 số thập phân |
| 3 | Currency | currency | No | String |  |  |  |
| 4 | To user name | to_user_name | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Description | description | String |  |  |

from_user_name move amount currency cash to to_user_name
Ví dụ: KH Oper 015 move 100.56 USD cash to KH Oper 014

## Trả về thông tin Current balance của user đang login khi nhận vào currency code
- **Workflow id:** `CSH_RULEFUNC_GET_CURRENT_BALANCE`

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency | currency_code | No | String | 3 |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Current balance | current_balance | `Number` |  | số thập phân |

## Trả về danh sách Denom (có balance và không có balance) của user đang login khi nhận vào currency code
- **Workflow id:** `CSH_RULEFUNC_GET_LIST_DENOM_BALANCE`

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency | currency_code | No | String | 3 |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Search results | items | Array Object |  |  |
| 1 | Face value | face_value | `Number` |  | số nguyên dương |
| 2 | Face type | face_type | String |  |  |
| 3 | Face type caption | face_type_caption | String |  |  |
| 4 | Last sheet | last_sheet | `Number` |  | số nguyên dương |
| 5 | Last amount | last_amount | `Number` |  | số có hai số thập phân |
| 6 | Amount in safe | amount_in_safe | `Number` |  | số nguyên dương |
| 7 | Sheet | sheet | `Number` |  | số nguyên dương |
| 8 | Amount | amount | `Number` |  | số có hai số thập phân |