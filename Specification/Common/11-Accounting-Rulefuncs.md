# 1. Accounting module
## Accounting group lookup
- [Search advanced của "Group Definition"](Specification/Accounting/03 Group Definition)
- **Workflow id:** `ACT_ACGRPDEF_LOOKUP`
- Lookup:
    - Code: group_id
    - Name: account_group_def

## System account name lookup
- [Search advanced của "Module Account List"](Specification/Accounting/04 Module Account List)
- **Workflow id:** `ACT_ACGRPDEFDTL_LOOKUP`
- Lookup:
    - Code: system_account_name
    - Name: bank_define_account_name

## Accounting Chart lookup
- [Search advanced của "Bank Account Definition"](Specification/Accounting/01 Bank Account Definition)
- **Workflow id:** `ACT_ACCHRT_LOOKUP`
- Lookup:
    - Code: bank_account_number
    - Name: account_name

## Accounting Chart lookup by branch id
- **Workflow id:** `ACT_ACCHRT_LOOKUP_BY_BRANCHID`

- **Query in O9:** 
```sql
SELECT BACNO,ACNAME,CCRCD FROM D_ACCHRT WHERE ISDIRECT ='Y' AND ISLEAVE = 'Y' AND ACLVL=7 AND BRANCHID = '@CRRBRID' ORDER BY BACNO
```
- **Query in Neptune:** 
```sql
SELECT * FROM `AccountChart` WHERE `DirectPosting`='Y' and `IsAccountLeave`=1 and `AccountLevel`=7 and `BranchCode`='0999';
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | account_branch_id | No | `Number` |  |  |  |
| 2 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 3 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Branch code | branch_code | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Bank account number | bank_account_number | String |  |  |

## Accounting Chart lookup by Currency code
- **Workflow id:** `ACT_ACCHRT_LOOKUP_BY_CURRENCY`

- **Query in O9:** 
```sql
SELECT BACNO,ACNAME,CCRCD FROM D_ACCHRT WHERE ISDIRECT ='Y' AND ISLEAVE = 'Y' AND ACLVL=7 AND CCRCD = '@CCRCD' ORDER BY BACNO
```
- **Query in Neptune:** 
```sql
SELECT * FROM `AccountChart` WHERE `AccountLevel`=7 and `CurrencyCode`='USD';
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency code | currency_code | No | String |  |  |  |
| 2 | Page index | page_index | No | `Number` |  |  | số nguyên dương |
| 3 | Page size | page_size | No | `Number` |  |  | số nguyên dương |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Branch code | branch_code | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Bank account number | bank_account_number | String |  |  |

## Trả về danh sách GL cấp 7 khi nhận "Branch id" and "Account number" của Deposit
- **Workflow id:** `ACT_ACCHRT_LOOKUP_BY_BRANCHID_DPTACC`

- **Query in O9:** 
```sql
SELECT BACNO,ACNAME 
FROM O9DATA.D_ACCHRT 
WHERE ACLVL = 7 
AND BRANCHID = '@CRRBRID'    
AND CCRCD = (SELECT CCRCD FROM O9DATA.D_DEPOSIT WHERE ACNO = '@PDMACC')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | account_branch_id | No | `Number` |  |  |  |
| 2 | Account number | account_number | No | String |  |  | Account number của Deposit |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |
| 4 | Page size | page_size | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account name | account_name | String |  |  |
| 2 | Bank account number | bank_account_number | String |  |  |

## Trả về danh sách GL cấp 7 khi nhận "Branch id" and "Currency code"
- **Workflow id:** `ACT_ACCHRT_LOOKUP_BY_BRANCHID_CURRENCY`

- **Query in O9:** 
```sql
SELECT BACNO,ACNAME,CCRCD 
FROM D_ACCHRT 
WHERE ISLEAVE = 'Y' 
AND ISDIRECT='Y' 
AND ACLVL=7 
AND BRANCHID = '@CRRBRID' 
and CCRCD='@CCCR' ORDER BY BACNO
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | account_branch_id | No | `Number` |  |  |  |
| 2 | Currency code | currency_code | No | String |  |  |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |
| 4 | Page size | page_size | No | `Number` |  | 0 |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Branch code | branch_code | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Bank account number | bank_account_number | String |  |  |

## Trả về information khi nhận "Bank account number"
- **Workflow id:** `ACT_ACCHRT_GET_INFO_ACNO`

- **Query:** 
```sql
SELECT `AccountName`, `AccountNumber`, `BranchCode`, `CurrencyCode`, `currency_code ||'-'|| account_name` FROM `AccountChart` WHERE `BankAccountNumber`='084801296511010000000';
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Bank account number | bank_account_number | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account name | account_name | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Branch code | branch_code | String |  |  |
| 4 | Currency code | currency_code | String |  |  |
| 5 | Bank account number | bank_account_number | String |  |  |
| 6 | Id | id | `Number` |  |  |
| 7 | Currency account name | currency_account_name | String |  | `trả thêm`, `currency_code-account_name` |

## Trả về thông tin "Group definition" khi nhận "Group id"
- **Workflow id:** `ACT_ACGRPDEF_GET_INFO_ACTGRP`

- **Query in O9:** 
```sql
SELECT A.GRPID,A.O9MODULE,A.ACGRPDEF FROM O9SYS.S_ACGRPDEF A WHERE A.GRPID='@ACTGRP'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | group_id | No | `Number` |  |  | `ACTGRP` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Accounting group | group_id | `Number` |  | `GRPID` |
| 2 | Module | module | String |  | `O9MODULE` |
| 3 | Group name | account_group_def | String |  | `ACGRPDEF` |

