# 1. Fixed Asset module
## Trả về thông tin catalog khi nhận "catalogue code" của Fixed asset
- **Workflow id:** `FAC_RF_SETINFO_CATALOG`

- **Query in O9:** 
```sql
SELECT CATNAME,FATYPE,FACLASS,DEPMODE FROM O9DATA.D_FACCAT WHERE CATCD = '@CNCAT'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  | catalogue code của Fixed asset |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String |  |  |
| 3 | Catalogue name | catalog_name | String |  |  |
| 4 | Fixed asset type | fixed_asset_type | String |  |  |
| 5 | Fixed asset classification | fixed_asset_classification | String |  |  |
| 6 | Depreciation method | depreciation_method | String |  |  |

## Trả về thông tin account khi nhận "account number" của Fixed asset
- **Workflow id:** `FAC_RF_SETINFO_ACCOUNT`

- **Query in O9:** 
```sql
SELECT ACNAME,BKCCRCD,CCRCD,ACNO,ORGPRICE,BKAMT,NBV,CCRRATE,ACMAMT,ACMAMT - EXPAMT as ACMMR, EXPAMT,EXDATE,FADRATE FROM O9DATA.D_FIXEDASSET WHERE ACNO = '@PDACC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Fixed asset |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Account number | account_number | String |  | `ACNO` |
| 3 | Fixed asset name | fixed_asset_account_name | String |  | `ACNAME` |
| 4 | Booking currency code | booking_currency_code | String |  | `BKCCRCD` |
| 5 | Currency code | currency_code | String |  | `CCRCD` |
| 6 | Original price | original_price | `Number` |  | số có hai số thập phân, `ORGPRICE` |
| 7 | Book amount | booking_amount | `Number` |  | số có hai số thập phân, `BKAMT` |
| 8 | Net book value | net_booking_value | `Number` |  | số có hai số thập phân, `NBV` |
| 9 | Cross rate | cross_rate | `Number` | `CCRRATE` |
| 10 | Acummulate amount | accummulate_amount | `Number` |  | số có hai số thập phân |
| 11 | Acummulate not posted | accummulate_not_posted | `Number` |  | số có hai số thập phân, `accummulate_amount - expense_amount tương ứng (ACMAMT - EXPAMT)` |
| 12 | Expense amount | expense_amount | `Number` |  | số có hai số thập phân, `Acummulate posted`, `EXPAMT` |
| 13 | Depreciation rate | depreciation_rate | `Number` |  | số có hai số thập phân |
| 14 | Expire date | expire_date | `Date time` |  |  |

## Trả về thông tin BK rate (GET_BCYRATE) khi nhận "Currency" 01, "Currency" 02, "Branch id"
- **Workflow id:** `FAC_GET_INFO_PCSEXR`

- **Query in O9:** 
```sql
SELECT ROUND(O9UTIL.GET_BCYRATE('@CCCR2', 'BK','@CRRBRID'),9), '@CCCR'||'/'||'@CRRBCY' FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency01 | No | String |  |  | currency của Fixed asset, `CCCR2` |
| 2 | Currency 02 | currency02 | No | String |  |  | currency của purchases, 'CCCR' |
| 3 | Branch id | branch_id | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | BK rate currency01 | rate_currency01 | `Number` |  | số có 9 số thập phân, `cbkexr` |
| 2 | txb_pcsexr | txb_pcsexr | String |  | `txb_cbkexr` |

## Trả về thông tin CB rate (GET_BCYRATE) khi nhận "Currency" 01, "Currency" 02, "Branch id"
- **Workflow id:** `FAC_GET_INFO_PCSEXR_CB`

- **Query in O9:** 
```sql
SELECT ROUND(O9UTIL.GET_BCYRATE('@CCCR2', 'CB','@CRRBRID'),9), '@CCCR'||'/'||'@CRRBCY' FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency01 | No | String |  |  | currency của Fixed asset, `CCCR2` |
| 2 | Currency 02 | currency02 | No | String |  |  | currency của purchases, 'CCCR' |
| 3 | Branch id | branch_id | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | CB rate currency01 | rate_currency01 | `Number` |  | số có 9 số thập phân, `pcsexr` |
| 2 | txb_pcsexr | txb_pcsexr | String |  | `txb_pcsexr` |

## Trả về thông tin TB rate (GET_BCYRATE) khi nhận "Currency" 01, "Currency" 02, "Branch id"
- **Workflow id:** `FAC_GET_INFO_PCSEXR_TB`

- **Query in O9:** 
```sql
SELECT ROUND(O9UTIL.GET_BCYRATE('@CCCR2', 'TB','@CRRBRID'),9), '@CCCR'||'/'||'@CRRBCY' FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency01 | No | String |  |  | currency của Fixed asset, `CCCR2` |
| 2 | Currency 02 | currency02 | No | String |  |  | currency của purchases, 'CCCR' |
| 3 | Branch id | branch_id | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | TB rate currency01 | rate_currency01 | `Number` |  | số có 9 số thập phân, `pcsexr` |
| 2 | txb_pcsexr | txb_pcsexr | String |  | `txb_pcsexr` |