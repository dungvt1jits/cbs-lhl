# 1. FX module
## Trả về thông tin BK rate (GET_BCYRATE) khi nhận "Currency" và "Branch id"
- **Workflow id:** `FX_RULEFUNC_GET_INFO_CBKEXR`

- **Query in O9:** 
```sql
SELECT ROUND(O9UTIL.GET_BCYRATE('@CCCR', 'BK','@CRRBRID'),9) FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency code | currency_code | No | String |  |  | `CCCR` |
| 2 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên, `CRRBRID` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | BK rate currency | bk_rate_currency | `Number` |  | số có 9 số thập phân |

## Trả về thông tin TB or CA rate (GET_CROSS_RATE) khi nhận "Currency" 01, "Currency" 02, "Branch id" và "Cross rate"
- **Workflow id:** `FX_RULEFUNC_GET_INFO_CCRRATE_TB_CA`

- **Query in O9:** 
```sql
SELECT O9UTIL.GET_CROSS_RATE ('@CCCR','@PCSCCR','TB',CASE WHEN '@PCSCCR' = '@CCCR' THEN 'TB' ELSE 'CA' END,'@CRRBRID',NULL,'@CCRRATE'),'@PCSCCR'||'/'||'@CCCR' FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency_code01 | No | String |  |  |  |
| 2 | Currency 02 | currency_code02 | No | String |  |  |  |
| 3 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên |
| 4 | Cross rate | cross_rate | No | `Number` |  |  | số có 9 số thập phân |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân, `crate` |
| 2 | txb_ccrrate | txb_ccrrate | String |  | `txb_ccrrate` |

## Trả về thông tin TB rate (GET_BCYRATE) khi nhận "Currency" và "Branch id"
- **Workflow id:** `FX_RULEFUNC_GET_INFO_PCSEXR`

- **Query in O9:** 
```sql
SELECT ROUND(O9UTIL.GET_BCYRATE('@CCCR', 'TB','@CRRBRID'),9), '@CCCR'||'/'||'@CRRBCY' FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency | currency_code | No | String |  |  | `CCCR` |
| 3 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên, `CRRBRID` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | TB rate currency01 | rate_currency01 | `Number` |  | số có 9 số thập phân |
| 2 | txb_pcsexr | txb_pcsexr | String |  | `txb_pcsexr` |

## Trả về thông tin CB or CA rate (GET_CROSS_RATE) khi nhận "Currency" 01, "Currency" 02, "Branch id"
- **Workflow id:** `FX_RULEFUNC_GET_INFO_CRATE_CB_CA`

- **Query in O9:** 
```sql
SELECT O9UTIL.GET_CROSS_RATE ('@CCCR2','@CCCR','CB',CASE WHEN '@CCCR' = '@CCCR2' THEN 'CB' ELSE 'CA' END, '@CRRBRID' ,NULL,NULL),'@CCCR2'||'/'||'@CCCR' FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency_code01 | No | String |  |  | currency của Fixed asset, `CCCR2` |
| 2 | Currency 02 | currency_code02 | No | String |  |  | `CCCR` |
| 3 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân, `crate` |
| 2 | txb_crate | txb_crate | String |  | `txb_crate` |

## Trả về thông tin TB or TA rate (GET_CROSS_RATE) khi nhận "Currency" 01, "Currency" 02, "Branch id" và "Cross rate"
- **Workflow id:** `FX_RULEFUNC_GET_INFO_CCRRATE_TB_TA`

- **Query in O9:** 
```sql
SELECT O9UTIL.GET_CROSS_RATE ('@CCCR','@CCCR1','TB',
CASE WHEN '@CCCR' = '@CCCR1' THEN 'TB' ELSE 'TA' END, '@CRRBRID' ,NULL,'@CCRRATE'),'@CCCR'||'/'||'@CCCR1' FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency_code01 | No | String |  |  |  |
| 2 | Currency 02 | currency_code02 | No | String |  |  |  |
| 3 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên |
| 4 | Cross rate | cross_rate | No | `Number` |  |  | số có 9 số thập phân |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân, `crate` |
| 2 | txb_ccrrate | txb_ccrrate | String |  | `txb_ccrrate` |

## Trả về thông tin TB or TA rate (GET_CROSS_RATE) khi nhận "Currency" 01, "Currency" 02, "Branch id"
- **Workflow id:** `FX_RULEFUNC_GET_INFO_CRATE_TB_TA`

- **Query in O9:** 
```sql
SELECT O9UTIL.GET_CROSS_RATE ('@CCCR2','@CCCR','TB',CASE WHEN '@CCCR' = '@CCCR2' THEN 'TB' ELSE 'TA' END, '@CRRBRID' ,NULL,NULL),'@CCCR2'||'/'||'@CCCR' FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency_code01 | No | String |  |  | currency của Fixed asset, `CCCR2` |
| 2 | Currency 02 | currency_code02 | No | String |  |  | `CCCR` |
| 3 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân, `crate` |
| 2 | txb_crate | txb_crate | String |  | `txb_crate` |

## Trả về thông tin TB or CA rate (GET_CROSS_RATE) khi nhận "Currency" 01, "Currency" 02, "Branch id"
- **Workflow id:** `FX_RULEFUNC_GET_INFO_CRATE_TB_CA`

- **Query in O9:** 
```sql
SELECT O9UTIL.GET_CROSS_RATE ('@CCCR','@PCSCCR','TB',CASE WHEN '@PCSCCR' = '@CCCR' THEN 'TB' ELSE 'CA' END, '@CRRBRID' ,NULL,NULL),'@PCSCCR'||'/'||'@CCCR' FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency_code01 | No | String |  |  | currency của Fixed asset, `CCCR` |
| 2 | Currency 02 | currency_code02 | No | String |  |  | `PCSCCR` |
| 3 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên, `CRRBRID` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân, `crate` |
| 2 | txb_crate | txb_crate | String |  | `txb_crate` |

## Trả về thông tin rate và amount khi nhận "Currency" 01, "Currency" 02, "Branch id" và "Amount"
- **Workflow id:** `FX_RULEFUNC_GET_INFO_CCRRATE`

- **Query in O9:** 
```sql
SELECT O9UTIL.GET_CROSS_RATE ('@CCCR','@RCCR','CB','CA', '@CRRBRID' ,NULL,NULL), 
O9UTIL.GET_BCYRATE('@CCCR', 'CB', '@CRRBRID', NULL),
O9UTIL.GET_BCYRATE('@RCCR', 'CA', '@CRRBRID', NULL) ,
O9UTIL.GET_BCYRATE('@CCCR', 'CB', '@CRRBRID', NULL) *('@CAMT') , 
O9UTIL.GET_CROSS_RATE ('@CCCR','@RCCR','CB','CA', '@CRRBRID' ,NULL,NULL) * ('@CAMT') FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency01 | No | String |  |  | currency của Mortgage |
| 2 | Currency 02 | currency02 | No | String |  |  | currency của Credit |
| 3 | Branch id | branch_id | No | `Number` |  |  | số nguyên |
| 4 | Amount in currency 01 | amount_in_currency01 | No | `Number` |  |  | số có hai số thập phân |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate CB CA | cross_rate_cb_ca | `Number` |  | số có 9 số thập phân, `ccrrate` |
| 2 | CB rate currency01 | cb_rate_currency01 | `Number` |  | số có 9 số thập phân, `crate` |
| 3 | CA rate currency02 | ca_rate_currency02 | `Number` |  | số có 9 số thập phân, `ORGRATE` |
| 4 | Amount in base currency | amount_in_base_currency | `Number` |  | số có 2 số thập phân, `cbamt` |
| 5 | Amount in currency 02 | amount_in_currency02 | `Number` |  | số có 2 số thập phân, `pcsamt` |

## Trả về thông tin BK rate (GET_CROSS_RATE) khi nhận "Currency" 01 và "Currency" 02
- **Workflow id:** `FX_GET_CROSS_RATE`

- **Query in O9:** 
```sql
SELECT ROUND(O9UTIL.GET_CROSS_RATE ('@CCCR','@CCCR2','BK', 'BK',NULL,NULL,NULL),9) FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency_code01 | No | String |  |  | `CCCR` |
| 2 | Currency 02 | currency_code02 | No | String |  |  | `CCCR2` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân |

## Trả về thông tin FX rate (GET_CROSS_RATE) trong 9 giao dịch FX khi nhận "Debit Currency", "Credit Currency", "Debit rate type", "Credit rate type" và Branch id
- **Workflow id:** `FX_GET_FX_RATE`

- **Query in O9:** 
```sql
select CCRRATE,ORGRATE,round(1/CCRRATE,9) SWRATE 
from (
SELECT ROUND(O9UTIL.GET_CROSS_RATE('@PCSCCR','@PGCCR', '@DRTYPE', '@CRTYPE','@CRRBRID'),9) CCRRATE,
ROUND(O9UTIL.GET_CROSS_RATE('@PCSCCR','@PGCCR', '@DRTYPE', '@CRTYPE','@CRRBRID'),9) ORGRATE FROM DUAL
)
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Debit currency code | debit_currency_code | No | String | 3 |  | `PCSCCR` |
| 2 | Credit currency code | credit_currency_code | No | String | 3 |  | `PGCCR` |
| 3 | Debit rate type | debit_rate_type | No | String | 2 | `CB` |  `DRTYPE` |
| 4 | Credit rate type | credit_rate_type | No | String | 2 | `CA` | `CRTYPE` |
| 5 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên, `CRRBRID` |

`debit_rate_type` có thể là `CB`, `credit_rate_type` có thể là `CA`

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân, `CCRRATE` |
| 2 | Original cross rate | original_cross_rate | `Number` |  | số có 9 số thập phân, `ORGRATE` |
| 3 | Reverse rate | reverse_rate | `Number` |  | số có 9 số thập phân, `SWRATE` |

## Trả về thông tin FX BCY rate (GET_BCYRATE) trong 9 giao dịch FX khi nhận "Currency" , "Rate type" và Branch Id
- **Workflow id:** `FX_RULEFUNC_GET_FX_BCYRATE`

- **Query in O9:** 
```sql
SELECT ROUND(O9UTIL.GET_BCYRATE('@PCSCCR', '@DRTYPE','@CRRBRID'),9)   FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency code | currency_code | No | String | 3 |  | `PCSCCR`, `PGCCR` |
| 2 | Rate type | rate_type | No | String | 2 | `CB` hoặc `CA` |  `DRTYPE`=CB, `CRTYPE`=CA |
| 3 | Branch id | branch_id_fx | No | `Number` |  |  | số nguyên, `CRRBRID` |

`rate_type` có thể là `CB` hoặc `CA`

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | BCY rate | bcy_rate | `Number` |  | số có 9 số thập phân, `PGEXR`, `CTXEXR` |