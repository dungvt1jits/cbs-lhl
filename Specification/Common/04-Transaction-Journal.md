# 1. Search common
## 1.1 Request message

**Workflow id:** `SQL_SIMPLE_SEARCH_TRANSACTION_JOURNAL`

**Body:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

## 1.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Status | tran_status | String |  |  |
| 2 | Money |  | Boolean |  |  |
| 3 | Tran Reference | tran_number | String |  |  |
| 4 | Tran Name | tran_name | String |  |  |
| 5 | Account Number | account_number | String |  |  |
| 6 | Tran Date | transaction_date | Date |  |  |
| 7 | Tran Time | transaction_date | Date |  |  |
| 8 | Amount | amount | Number |  |  |
| 9 | Status | tran_status_caption | String |  | CodeName=`TXSTS` |
| 10 | Tran Type | tran_type_caption | String |  | CodeName=`TXTYPE` |
| 11 | User Name | user_name | String |  |  |
| 12 | User Approve | user_approve | String |  |  |
| 13 | Approve Date | approve_date | Date |  |  |
| 14 | Description | description | String |  |  |
| 15 | Branch Code | branch_code | String |  |  |
| 16 | Approval Reason | approval_reason | String |  |  |
| 17 | Execution id | tran_reference | String |  |  |
| 18 | Channel | channel | String |  |  |
| 19 | Channel | channel_caption | String |  | CodeName=`CHANNEL`, `trả thêm` |
| 20 | User Reject | user_reject | String |  | `trả thêm` |


## 1.3 Flow of events:
User có position:
- Cashier ("Cashier":1): chỉ được xem giao dịch của chính mình.
- Officer ("Officer":1): xem được giao dịch của chính mình và các user khác cùng branch.
- Chief cashier ("ChiefCashier":1): xem được giao dịch của chính mình và các user khác cùng branch.
- Inter-branch user ("InterBranchUser":1): xem được giao dịch của tất cả user (cùng branch và khác branch)

# 2. Search advanced
## 2.1 Request message

**Workflow id:** `SQL_ADVANCED_SEARCH_TRANSACTION_JOURNAL`

**Body:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Execution id | tran_reference | No | String |  |  |  |
| 2 | Tran Name | tran_name | No | String |  |  |  |
| 3 | Account Number | account_number | No | String |  |  |  |
| 4 | Tran Date | tran_date | No | Date |  |  |  |
| 5 | Tran Time | tran_time | No | Date |  |  |  |
| 6 | Amount from | amount_from | No | `Number` |  |  |  |
| 7 | Amount to | amount_to | No | `Number` |  |  |  |
| 8 | Status | tran_status | No | String |  |  | Select box |
| 9 | Tran Type | tran_type | No | String |  |  |  |
| 10 | User Name | user_name | No | String |  |  |  |
| 11 | User Approve | tran_user_approve | No | String |  |  |  |
| 12 | Branch Code | branch_code | No | String |  |  |  |
| 13 | Approval Reason | approval_reason | No | String |  |  |  |
| 14 | Page index | page_index | No | `Number` |  |  |  |
| 15 | Page size | page_size | No | `Number` |  |  |  |
| 16 | Channel | channel | String |  | `gửi thêm` |
| 17 | User Reject | user_reject | String |  | `gửi thêm` |
| 18 | Tran Reference | tran_number | String |  | `gửi thêm` |

## 2.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Status | tran_status | String |  |  |
| 2 | Money | money | Boolean |  |  |
| 3 | Tran Reference | tran_number | String |  |  |
| 4 | Tran Name | tran_name | String |  |  |
| 5 | Account Number | account_number | String |  |  |
| 6 | Tran Date | transaction_date | Date |  |  |
| 7 | Tran Time | transaction_date | Date |  |  |
| 8 | Amount | amount | Number |  |  |
| 9 | Status | tran_status_caption | String |  | CodeName=`TXSTS` |
| 10 | Tran Type | tran_type_caption | String |  | CodeName=`TXTYPE` |
| 11 | User Name | user_name | String |  |  |
| 12 | User Approve | user_approve | String |  |  |
| 13 | Approve Date | approve_date | Date |  |  |
| 14 | Description | description | String |  |  |
| 15 | Branch Code | branch_code | String |  |  |
| 16 | Approval Reason | approval_reason | String |  |  |
| 17 | Execution id | tran_reference | String |  |  |
| 18 | Channel | channel | String |  |  |
| 19 | Channel | channel_caption | String |  | CodeName=`CHANNEL`, `trả thêm` |
| 20 | User Reject | user_reject | String |  | `trả thêm` |

## 2.3 Flow of events:
User có position:
- Cashier ("Cashier":1): chỉ được xem giao dịch của chính mình.
- Officer ("Officer":1): xem được giao dịch của chính mình và các user khác cùng branch.
- Chief cashier ("ChiefCashier":1): xem được giao dịch của chính mình và các user khác cùng branch.
- Inter-branch user ("InterBranchUser":1): xem được giao dịch của tất cả user (cùng branch và khác branch)

# 3. Approve reverse
## 3.1 Request message

**Workflow id:** `UMG_LOGIN_APPROVE`

**Body:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Username | username | `Yes` | String |  |  |  |
| 2 | Password | password | `Yes` | String |  |  |  |

## 3.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |

## 3.3 Flow of events:
- User approve tồn tại vào nhập đúng password.
- User approve có đang chung branch với user thực hiện giao dịch không:
  - Chung branch: trả về id của user approve như luồng hiện tại
  - Khác branch: Check position `InterBranchUser` của user approve
    - InterBranchUser = 1: trả về id của user approve như luồng hiện tại
    - Thông báo lỗi với mã lỗi là `SYS_APPROVE_TRANSACTION`
- User approve có trùng với user thực hiện giao dịch không:
  - User approve khác user thực hiện giao dịch: trả về id của user approve như luồng hiện tại
  - User approve = user thực hiện giao dịch: Thông báo lỗi với mã lỗi là `SYS_USR_APROVE_RULE`

# 4. Approve print
## 4.1 Request message

**Workflow id:** ``

**Body:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Username | username | `Yes` | String |  |  |  |
| 2 | Password | password | `Yes` | String |  |  |  |

## 4.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |