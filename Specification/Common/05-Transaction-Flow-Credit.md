# 1. Credit's outstanding increase
**Transaction:**
- CRD_CDR: Cash Disbursement
- CRD_MDR: Miscellaneous Disbursement
- CRD_TDR: Disbursement By Transfer

**Flow of events:**
- Record amount disbursed.
- Record credit limit was used.
- Provision principal based on amount disbursement.
- Record history credit.
- Schedule information:
    - Credit type is "Blanket" (B):
        - Interest computation mode is "Fixed" (F):
            - Principal payment:
                - Will generate schedule based on default information. In which: `amount` = `disbursement amount`, only one row, due date = end of tenor.
    - Credit type is "Fixed" (F): Unchanged.

**Database:**
- Table name: **d_credit**

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |
| 1 | Disbursement amount | + Disbursement amount |  |
| 2 | Outstanding balance | + Disbursement amount |  |
| 3 | Normal principal amount | + Disbursement amount |  |
| 4 | Provision of principal | + (Disbursement amount * provision rate) |  |
| 5 | Week credit | + Disbursement amount |  |
| 6 | Month credit | + Disbursement amount |  |
| 7 | Quarter credit | + Disbursement amount |  |
| 8 | Semi-annual credit | + Disbursement amount |  |
| 9 | Year credit | + Disbursement amount |  |
| 10 | Last transaction date | = Working date |  |

- Table name: **d_crdbschd**

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |
| 1 | Unutilized amount | - Disbursement amount |  |
| 2 | Disbursement amount | Unchanged |  |

# 2. Collect principal and interest
**Transaction:**
- CRD_MIPM: Miscellaneous Interest And Principal Collection
- CRD_CIPM: Principal And Interest Collection By Cash
- CRD_IPCT: Principal And Interest Collection By Transfer

**Flow of events:**
- Don't allow prepaid interest.
- Don't allow collect principal amount larger than outstanding.
- Amount collection can be interest amount or principal amount or both of them.
- Case collect interest: repay for "Off balance sheet" first, and rest for "On balance sheet" and update paid amount on schedule interest.
- Case collect principal: account have outstanding and don't allow collection principal amount larger than outstanding and update paid amount on schedule principal.
- Reverse provision-based collection principal amount.
- Record history credit side of principal: week, month, quarter, semi-annual/year credit
- Repayment advance principal for credit account:
    - Outstanding balance > 0.00
    - Principal collect > Due amount.
    - Principal collect <= Outstanding balance
    - Interest computation mode is "Principal equally for Fixed" (S) or "Compound basic of installment" (C) or "Payment + Interest equally for compound" (P).
    - Outstanding balance = Outstanding balance (old) - Principal collect
    - Due amount = 0.00
    - Principal paid amount = Principal paid amount (old) + Principal collect
    - Interest paid = Interest receivable – Interest collect
- Schedule information:
    - Credit type is "Fixed" (F):
        - Advance repayment principal mode is "Decrease repayment amount" (A):
            - Interest computation mode is "Principal equally for Fixed" (S):
                - Principal payment: 
                    - Add more 1 tenor for repayment advance principal.
                    - Will generate schedule based on default information.
            - Interest computation mode is "Compound basic of installment" (C):
                - Principal payment:
                    - Add more 1 tenor for repayment advance principal.
                    - Will generate schedule based on default information.
                - Interest payment:
                    - Will generate schedule based on default information.
            - Interest computation mode is "Payment + Interest equally for compound" (P):
                - Principal payment:
                    - Add more 1 tenor for repayment advance principal
                    - Will generate schedule based on default information.
                - Interest payment: 
                    - Will generate schedule based on default information.
        - Advance repayment principal mode is "Shortern duration" (C):
            - Interest computation mode is "Principal equally for Fixed" (S):
                - Principal payment: 
                    - Add more 1 tenor for repayment advance principal.
                    - Will generate schedule based on `payment schedule` information.
            - Interest computation mode is "Payment + Interest equally for compound" (P):
                - Principal payment:
                    - Add more 1 tenor for repayment advance principal
                    - Will generate schedule based on `payment schedule` information.
                - Interest payment: 
                    - Will generate schedule based on `payment schedule` information.
        - Interest computation mode is "Fixed" (F):
            - Principal payment: 
                - Will NOT generate schedule.
    - Credit type is "Blanket" (B):
        - Interest computation mode is "Fixed" (F):
            - Principal payment:
                - Amount = Unchanged, Paid = Paid (old) + Principal collect, only one row, due date = end of tenor.

**Database:**
- Table name: **d_credit**

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |
| 1 | Outstanding balance | - Principal collect |  |
| 2 | Normal principal amount | - Principal collect |  |
| 3 | Provision of principal | - (Principal collect * provision rate) |  |
| 4 | Due amount | - Principal collect | Không được nhỏ hơn 0.00 |
| 5 | Interest receivable | - Interest collect | Không được nhỏ hơn 0.00 |
| 6 | Interest due | - Interest collect | Không được nhỏ hơn 0.00 |
| 7 | Interest paid | + Interest collect |  |
| 8 | Week debit | + Principal collect |  |
| 9 | Month debit | + Principal collect |  |
| 10 | Quarter debit | + Principal collect |  |
| 11 | Semi-annual debit | + Principal collect |  |
| 12 | Year debit | + Principal collect |  |
| 13 | Last transaction date | = Working date |  |

- d_credit
- d_crdhst
- d_crdtran
- d_txinv
- d_credit_sp
- d_crdbschd
- d_crdbschd_sp
- d_crschd
- d_crschd_sp

# Cập nhật thông tin giải ngân bằng tiền mặt
Cập nhật thông tin giảm tiền mặt của teller:
- [Transaction Flow Cash](Specification/Common/07 Transaction Flow Cash)<br>

# Cập nhật thông tin giải ngân bằng chuyển khoản đến tài khoản tiền gửi
Cập nhật thông tin tăng balance tiền gửi của customer:
- [Transaction Flow Deposit](Specification/Common/06 Transaction Flow Deposit#4-deposit-trans)<br>

# Cập nhật thông tin giải ngân bằng tài khoản GL
Cập nhật thông tin credit vào tài khoản GL:
- [Transaction Flow GL](Specification/Common/08 Transaction Flow GL)<br>