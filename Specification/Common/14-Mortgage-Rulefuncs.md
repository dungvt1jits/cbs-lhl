# 1. Mortgage module
## Trả về thông tin product limit trong các FO MTG khi nhận "Product limit code"
- **Workflow id:** `MTG_GET_INFO_PL`

- **Query in O9:** 
```sql
select plnm, ccrcd, lmamt from o9data.d_crdpl where plcd='@PLCD' and plsts not in ('C','J')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Product limit code | product_limit_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Product name | product_limit_name | String |  |  |
| 2 | Currency | currency_code | String | 3 |  |
| 3 | Limit amount | limit_amount | `Number` |  | số có hai số thập phân |

## Trả về thông tin các amount default và in use khi nhận "Account number" của Mortgage và "Product limit code"
- **Workflow id:** `MTG_RPL_GET_INFO_CAMT_PCSAMT`

- **Query in O9:** 
```sql
SELECT ROUND(mtg_in_use * GREATEST (pl_in_use - pl_req, 0) / pl_in_use, 2) mtg_default,
       mtg_in_use,
       GREATEST (pl_in_use - pl_req, 0) pl_default,
       pl_in_use
  FROM (SELECT b.amt - b.clramt mtg_in_use,
               b.mdlamt - b.mdlcrlamt pl_in_use,
               o9sys.o9crdlimit.get_required_secured_amount (a.plcd) pl_req
          FROM o9data.d_crdpl a
               INNER JOIN o9data.d_mtgbal b
                   ON     a.plcd = b.mtgref
                      AND b.defacno = o9sys.o9mtg.set_acno_defacno ('@CMRGAC')
         WHERE a.plcd = '@PLCD')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Collateral account number | collateral_account_number | No | String |  |  | account number của Mortgage |
| 2 | Product limit code | product_limit_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Collateral amount default | collateral_amount_default | `Number` |  | số có hai số thập phân, `CAMT` |
| 2 | Collateral amount in use | collateral_amount_in_use | `Number` |  | số có hai số thập phân, `CRATE` |
| 3 | Product limit amount default | product_limit_amount_default | `Number` |  | số có hai số thập phân, `PCSAMT` |
| 4 | Product limit amount in use | product_limit_amount_in_use | `Number` |  | số có hai số thập phân, `ORGRATE` |

## Trả về thông tin amount trong D_MTGBAL khi nhận "Account number" của Mortgage và "Product limit code"
- **Workflow id:** `MTG_SPL_GET_INFO_SCRAMT`

- **Query in O9:** 
```sql
SELECT ROUND(0.01 * a.secrt * a.lmamt, 2) - NVL (SUM (b.mdlamt - b.mdlcrlamt), 0) amt
  FROM o9data.d_crdpl a LEFT JOIN o9data.d_mtgbal b ON a.plcd = b.mtgref and b.defacno = o9sys.o9mtg.set_acno_defacno ('@CMRGAC')
 WHERE a.plcd = '@PLCD'
 group by a.secrt, a.lmamt
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Collateral account number | collateral_account_number | No | String |  |  | account number của Mortgage |
| 2 | Product limit code | product_limit_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Amount | amount | `Number` |  | số có hai số thập phân |

## Trả về thông tin secure amount trong D_MTGBAL khi nhận "Account number" của Mortgage và "Product limit code"
- **Workflow id:** `MTG_RPL_GET_INFO_SCRAMT`

- **Query in O9:** 
```sql
    SELECT ROUND (0.01 * a.secrt * a.lmamt, 2)
    FROM o9data.d_crdpl a
         INNER JOIN o9data.d_mtgbal b
             ON a.plcd = b.mtgref AND b.defacno = o9sys.o9mtg.set_acno_defacno ('@CMRGAC')
   WHERE a.plcd = '@PLCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Collateral account number | collateral_account_number | No | String |  |  | account number của Mortgage |
| 2 | Product limit code | product_limit_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Secured amount | secured_amount | `Number` |  | số có hai số thập phân |

## Trả về thông tin secure amount trong D_MTGBAL khi nhận "Account number" của Mortgage và "Account number" của Credit
- **Workflow id:** `MTG_RLS_GET_INFO_SCRAMT`

- **Query in O9:** 
```sql
SELECT NVL(SUM(MDLAMT),0) - NVL(SUM(MDLCRLAMT),0) AMT 
FROM O9DATA.D_MTGBAL 
WHERE MTGREF = O9SYS.O9CRD.set_acno_defacno('@PCACC') AND DEFACNO = O9SYS.O9MTG.set_acno_defacno('@CMRGAC')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Collateral account number | collateral_account_number | No | String |  |  | account number của Mortgage |
| 2 | Credit account number | credit_account_number | No | String |  |  | account number của Credit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Secured amount | secured_amount | `Number` |  | số có hai số thập phân |

## Trả về thông tin account khi nhận "Account number" của Mortgage
- **Workflow id:** `GET_INFO_MTGACC`

- **Query in O9:** 
```sql
SELECT A.ACNAME, A.CCRCD, A.MAVALUE,A.MABV,A.MAMT - A.RLAMT SCRAMT,A.KPAMT,A.KPRL, B.CUSTOMERCD, O9SYS.O9COM.GET_ALL_FULLNAME(O9SYS.O9CTM.SET_CUSTOMERCD_CUSTOMERID(B.CUSTOMERCD,B.CTMTYPE),B.CTMTYPE) FULLNAME, B.ADDRESS
FROM O9DATA.D_MORTGAGE A 
INNER JOIN O9SYS.V_CUSTOMERINFO B 
ON A.CUSTOMERID = B.CUSTOMERID AND A.CTMTYPE = B.CTMTYPE WHERE A.ACNO = '@CMRGAC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Mortgage |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number def | account_number_def | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Account name | account_name | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Collateral asset value | collateral_asset_value | `Number` |  | số có hai số thập phân, `MAVALUE` |
| 6 | Book value | book_value | `Number` |  | số có hai số thập phân, `MABV` |
| 7 | Amount | amount | `Number` |  | số có hai số thập phân, `MortgageAmount`- `ReleasedCollateralAmount` tương ứng (A.MAMT - A.RLAMT AMT) |
| 8 | Keeping amount | keeping_amount | `Number` |  | số có hai số thập phân |
| 9 | Keeping release amount | keeping_release_amount | `Number` |  | số có hai số thập phân |
| 10 | Customer code | customer_code | String |  |  |
| 11 | Customer name | customer_name | String |  |  |
| 12 | Address | address | String |  |  |

## Trả về thông tin module amount trong D_MTGBAL khi nhận "Account number" của Mortgage
- **Workflow id:** `MTG_SCR_GET_INFO_CAMT5`

- **Query in O9:** 
```sql
SELECT NVL(SUM(MDLAMT),0) - NVL(SUM(MDLCRLAMT),0) AMT 
FROM O9DATA.D_MTGBAL 
WHERE MTGREF = O9SYS.O9CRD.set_acno_defacno('@PCACC')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Credit |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Amount | amount | `Number` |  | số có hai số thập phân |

## T
- **Workflow id:** `MTG_RTN_GET_INFO_SCRAMT`

- **Query in O9:** 
```sql
SELECT NVL(SUM(AMT-CLRAMT),0) FROM O9DATA.D_MTGBAL WHERE DEFACNO = o9sys.o9mtg.set_acno_defacno('@CMRGAC')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Mortgage |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Secured amount | amount | `Number` |  | số có hai số thập phân, (`Amount` - `ClearAmount`) tuong ứng (AMT-CLRAMT) |

## T
- **Workflow id:** `MTG_KPT_GET_INFO_SCRAMT`

- **Query in O9:** 
```sql
SELECT NVL(SUM(AMT),0) FROM O9DATA.D_MTGBAL WHERE DEFACNO = o9sys.o9mtg.set_acno_defacno('@CMRGAC')
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Mortgage |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Amount | amount | `Number` |  | số có hai số thập phân |

## Trả về thông tin module amount trong D_MTGBAL khi nhận "Product limit code"
- **Workflow id:** `MTG_PL_GET_INFO_CAMT5`

- **Query in O9:** 
```sql
SELECT NVL (SUM (mdlamt), 0) - NVL (SUM (mdlcrlamt), 0) amt
  FROM o9data.d_mtgbal
 WHERE mtgref = '@PLCD'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Product limit code | product_limit_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Amount | amount | `Number` |  | số có hai số thập phân |

## Trả về thông tin catalog trong khi nhận "Catalogue code" của Mortgage
- **Workflow id:** `GET_INFO_MTGCAT`

- **Query in O9:** SELECT CATNAME,MARATE,RKRATE,CCRCD FROM O9DATA.D_MTGCAT WHERE CATCD = '@CNCAT' ORDER BY CATCD

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  |  |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Catalogue name | catalog_name | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 6 | Risk allocation rate | risk_rate | `Number` |  | số có hai số thập phân |

## Trả về danh sách các Product Limit đã secure khi nhận "Account number" của Mortgage
- **Workflow id:** `MTG_RPL_LOOKUP_CRDPL`

- **Query in O9:** 
```sql
SELECT a.plcd,         a.plnm,         a.ccrcd,         a.lmamt    FROM o9data.d_crdpl a         
INNER JOIN o9data.d_mortgage b ON a.customerid = b.customerid AND a.ctmtype = b.ctmtype         
INNER JOIN O9DATA.d_mtgbal T ON T.DEFACNO=B.DEFACNO AND T.MTGREF=A.PLCD   
WHERE a.plsts NOT IN ('C','J') and a.sectype IN ('F', 'P') AND b.acno = '@CMRGAC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Mortgage |
| 2 | Page index | page_index | No | `Number` |  |  |  |
| 3 | Page size | page_size | No | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Product limit code | product_limit_code | String |  |  |
| 2 | Product name | product_limit_name | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Limit amount | limit_amount | `Number` |  | số có hai số thập phân |

## Trả về danh sách các Product Limit có thể secure khi nhận "Account number" của Mortgage
- **Workflow id:** `MTG_SPL_LOOKUP_CRDPL`

- **Query in O9:** 
```sql
SELECT a.plcd,       a.plnm,       a.ccrcd,       a.lmamt
  FROM o9data.d_crdpl a
       INNER JOIN o9data.d_mortgage b ON a.customerid = b.customerid AND a.ctmtype = b.ctmtype
 WHERE a.plsts NOT IN ('C','J') and a.sectype IN ('F', 'P') AND b.acno = '@CMRGAC'
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String |  |  | account number của Mortgage |
| 2 | Page index | page_index | No | `Number` |  |  |  |
| 3 | Page size | page_size | No | `Number` |  |  |  |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Product limit code | product_limit_code | String |  |  |
| 2 | Product name | product_limit_name | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Limit amount | limit_amount | `Number` |  | số có hai số thập phân |

## Trả về thông tin rate, amount và flag khi nhận "Currency" 01, "Currency" 02, "Branch id" và "Amount"
- **Workflow id:** `MTG_PL_GET_INFO_CCRRATE`

- **Query in O9:** 
```sql
SELECT CASE 
           WHEN o9util.get_cross_rate ('@CCCR', 
                                       '@RCCR', 
                                       'BK', 
                                       'BK', 
                                       '@CRRBRID', 
                                       NULL, 
                                       NULL) > 1 
           THEN 
               (o9util.get_cross_rate ('@CCCR', 
                                       '@RCCR', 
                                       'BK', 
                                       'BK', 
                                       '@CRRBRID', 
                                       NULL, 
                                       NULL)) 
           ELSE 
               o9util.get_cross_rate ('@RCCR', 
                                      '@CCCR', 
                                      'BK', 
                                      'BK', 
                                      '@CRRBRID', 
                                      NULL, 
                                      NULL) 
       END 
           AS CCRRATE,
                              
       o9util.get_bcyrate ('@CCCR',
                           'BK',
                           '@CRRBRID',
                           NULL),
       o9util.get_bcyrate ('@RCCR',
                           'BK',
                           '@CRRBRID',
                           NULL),
         o9util.get_bcyrate ('@CCCR',
                             'BK',
                             '@CRRBRID',
                             NULL)
       * ('@CAMT'),
          CASE 
           WHEN o9util.get_cross_rate ('@CCCR', 
                                       '@RCCR', 
                                       'BK', 
                                       'BK', 
                                       '@CRRBRID', 
                                       NULL, 
                                       NULL) > 1 
           THEN 
               (o9util.get_cross_rate ('@CCCR', 
                                       '@RCCR', 
                                       'BK', 
                                       'BK', 
                                       '@CRRBRID', 
                                       NULL, 
                                       NULL)) *'@CAMT'
           ELSE 
               '@CAMT'/o9util.get_cross_rate ('@RCCR', 
                                      '@CCCR', 
                                      'BK', 
                                      'BK', 
                                      '@CRRBRID', 
                                      NULL, 
                                      NULL) 
       END PCSAMT,
       CASE 
           WHEN o9util.get_cross_rate ('@CCCR', 
                                       '@RCCR', 
                                       'BK', 
                                       'BK', 
                                       '@CRRBRID', 
                                       NULL, 
                                       NULL) > 1
           THEN 1 ELSE 0 END AS CHKFLAG
  FROM DUAL
```

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency01 | No | String |  |  | currency của Mortgage |
| 2 | Currency 02 | currency02 | No | String |  |  | currency của Credit |
| 3 | Branch id | branch_id | No | `Number` |  |  | số nguyên |
| 4 | Amount in currency 01 | amount_in_currency01 | No | `Number` |  |  | số có hai số thập phân |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate BK BK | cross_rate_bk_bk | `Number` |  | số có 9 số thập phân, `CCRRATE` |
| 2 | BK rate currency01 | bk_rate_currency01 | `Number` |  | số có 9 số thập phân, `CRATE` |
| 3 | BK rate currency02 | bk_rate_currency02 | `Number` |  | số có 9 số thập phân, `ORGRATE` |
| 4 | Amount in base currency | amount_in_base_currency | `Number` |  | số có 2 số thập phân, `CBAMT` |
| 5 | Amount in currency 02 | amount_in_currency02 | `Number` |  | số có 2 số thập phân, `PCSAMT` |
| 6 | Check flag | check_flag | `Number` |  | số nguyên, 0 or 1, `CHKRATE` |