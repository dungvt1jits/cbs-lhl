# 1. Purchase fixed asset
D_FIXEDASSET
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status |  | Normal | |
| 2 | Book amount |  | Book amount (old) + Purchase amount | số có hai số thập phân |

# 2. Selling fixed asset
D_FIXEDASSET
| No | Field name | Parameter | Update value | Description |
| -- | ---------- | --------- | ------------ | ----------- |
| 1 | Account status |  | Sold | |
| 2 | Net book value |  | 0 | số có hai số thập phân |
| 3 | Book amount |  | Book amount (old) - Selling amount | số có hai số thập phân |