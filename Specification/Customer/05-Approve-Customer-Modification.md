# 1. Search common
## 1.1 Field description
### Request message
**Workflow id:** `SQL_SEARCH_APR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String | 8 |  |
| 2 | Full name | fullname | String |  |  |
| 3 | Name in local | shortname | String |  |  |
| 4 | Paper number | paper_number | String |  |  |
| 5 | Date of birth | date_of_birth | `Date time` |  |  |
| 6 | Gender | gender | String |  |  |
| 7 | Status | customer_status | String |  |  |
| 8 | Nationality | nation | String |  |  |
| 9 | Resident status | resident | String |  |  |
| 10 | Address | contact_local_address | String |  |  |
| 11 | Old id of customer | old_id_of_customer | String |  |  |
| 12 | Group limit code | group_id | String | 10 |  |
| 13 | Transaction id | id | `Number` |  |  |
| 14 | Pending approve | approve_status | String |  |  |
| 15 | Approver | approver | String |  | `trả thêm` trả về giá trị `User name` theo ApproveUserId trong bảng SingleCustomer |


## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message

**Workflow id:** `SQL_ADSEARCH_APR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Customer code | customer_code | No | String | 8 |  |  |
| 2 | Full name | fullname | No | String |  |  |  |
| 3 | Name in local | shortname | No | String |  |  |  |
| 4 | Paper number | paper_number | No | String |  |  |  |
| 5 | Date of birth from | dob_from | No | `Date time` |  |  |  |
| 6 | Date of birth to | dob_to | No | `Date time` |  |  |  |
| 7 | Gender | gender | No | String |  |  |  |
| 8 | Status | customer_status | No | String |  |  |  |
| 9 | Nationality | nation | No | String |  |  |  |
| 10 | Resident status | resident | No | String |  |  |  |
| 11 | Address | address | No | String |  |  |  |
| 12 | Old id of customer | old_id_of_customer | No | String |  |  |  |
| 13 | Group limit code | group_id | No | String | 10 |  |  |
| 14 | Page index | page_index | No | `Number` |  |  |  |
| 15 | Page size | page_size | No | `Number` |  |  |  |
| 16 | Approver | approver | No | String |  |  | `gửi thêm` tìm by text với giá trị `User name` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String | 8 |  |
| 2 | Full name | fullname | String |  |  |
| 3 | Name in local | shortname | String |  |  |
| 4 | Paper number | paper_number | String |  |  |
| 5 | Date of birth | date_of_birth | `Date time` |  |  |
| 6 | Gender | gender | String |  |  |
| 7 | Status | customer_status | String |  |  |
| 8 | Nationality | nation | String |  |  |
| 9 | Resident status | resident | String |  |  |
| 10 | Address | contact_local_address | String |  |  |
| 11 | Old id of customer | old_id_of_customer | String |  |  |
| 12 | Group limit code | group_id | String | 10 |  |
| 13 | Transaction id | id | `Number` |  |  |
| 14 | Pending approve | approve_status | String |  |  |
| 15 | Approver | approver | String |  | `trả thêm` trả về giá trị `User name` theo ApproveUserId trong bảng SingleCustomer |

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  | No data found | Không tìm thấy dữ liệu |

# 3. View by customer code
- [View by customer code của "Customer Profile"](Specification/Customer/01 Customer Profile)<br>


# 4. View user modify
## 4.1 Field description
### Request message
**Workflow id:** `CTM_GET_INFO_USER_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | `Yes` | String | 8 |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User name | user_name | String |  |  |
| 2 | Transaction reference id | tx_reference_id | String |  |  |
| 3 | Notification | notification | String |  |  |
| 4 | Approve status | is_approve | String |  |  |
| 5 | Transaction date | trans_date | `Date time` |  |  |

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. View modify

## 5.1 Field description

### Request message

**Workflow id:** `SQL_VIEW_APR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Transaction id | transaction_id | `Yes` | String |  |  |  |
| 2 | Customer code | customer_code | `Yes` | String | 8 |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | HO CIF ID | hocifcd | String |  |  |
| 2 | Title | title | String |  |  |
| 3 | Title of organization | title_of_organization | String |  |  |
| 4 | Suffix | suffix | String |  |  |
| 5 | First name (en) | firstname | String |  |  |
| 6 | Last name (en) | lastname | String |  |  |
| 7 | Middle name (en) | midname | String |  |  |
| 8 | Full name | fullname | String |  |  |
| 9 | First name (local) | firstname_local | String |  |  |
| 10 | Last name (local) | lastname_local | String |  |  |
| 11 | Middle name (local) | midname_local | String |  |  |
| 12 | Name in Cambodia | shortname | String |  |  |
| 13 | Gender | gender | String |  |  |
| 14 | Date of birth/ Starting date of company | date_of_birth | `Date time` |  |  |
| 15 | Place of birth | place_of_birth | String |  |  |
| 16 | Nationality | nation | String |  | VN |
| 17 | Country of company | country | String |  | VN |
| 18 | Paper type | paper_type | String |  | I |
| 19 | Paper number | paper_number | String |  |  |
| 20 | Issue date of paper | issue_date_of_paper | `Date time` |  | Working date |
| 21 | Issue place of paper | issue_place_of_paper | String |  |  |
| 22 | Expire date of paper | expire_date_of_paper | `Date time` |  |  |
| 23 | Sub paper type | paper_type_sub | String |  |  |
| 24 | Sub paper number | paper_number_sub | String |  |  |
| 25 | Issue date of sub paper | issue_date_of_sub_paper | `Date time` |  |  |
| 26 | Issue place of sub paper | issue_place_of_sub_paper | String |  |  |
| 27 | Group type | customer_group_type | String |  |  |
| 28 | Group sub type | customer_sub_group_type | String |  |  |
| 29 | Economic sector | sector | String |  | I0 |
| 30 | Sub economic sector | subsector | String |  | I01 |
| 31 | Resident status | resident | String |  | R |
| 32 | Legal local address | legal_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Village | village | String |  |  |
|  | Sub district | sub_district | String |  |  |
|  | District | district | String |  |  |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 33 | Contact local address | contact_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Village | village | String |  |  |
|  | Sub district | sub_district | String |  |  |
|  | District | district | String |  |  |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 34 | Home phone | phone_home | String |  |  |
| 35 | Mobile phone | phone_mobile | String |  |  |
| 36 | Email address | email | String |  |  |
| 37 | Education | education | String |  |  |
| 38 | Marital status | marital_status | String |  |  |
| 39 | Occupation (HO) | profession | String |  |  |
| 40 | Business type (HO) | business_type | String |  |  |
| 41 | Financial institute | financial | String |  |  |
| 42 | ISIC code | isic_code | String |  |  |
| 43 | Managing branch code | managing_branch_code | String |  |  |
| 44 | Classification | classify | String |  |  |
| 45 | Is involving politicians TH | polists | String |  |  |
| 46 | Is involving politicians FR | repolists | String |  |  |
| 47 | Country of income | country_of_income | String |  |  |
| 48 | FATCA status | fatca_status | String |  |  |
| 49 | Government organization id | government_id | String |  |  |
| 50 | International organization and oversea government id | international_id | String |  |  |
| 51 | Oversea juristic id | oversea_juristic_id | String |  |  |
| 52 | GFMIS code | gfmis_code | String |  |  |
| 53 | Last date | last_update_date | `Date time` |  | Working date |
| 54 | Group limit code | group_id | String | 10 |  |
| 55 | Credit line | customer_credit_line | `Number` |  |  |
| 56 | Currency | currency_code | String | 3 |  |
| 57 | Customer type (HO) | customer_type | String |  | O030 |
| 58 | Primary CIF | primary_cif | String |  |  |
| 59 | MDM AML status | mdm_amount_status | String |  |  |
| 60 | MDM request status | mdm_request_status | String |  |  |
| 61 | MDM list sub type code | mdm_list_sub_type_code | String |  |  |
| 62 | MDM list sub type desc | mdm_list_sub_type_desc | String |  |  |
| 63 | MDM matching by | mdm_matching_by | String |  |  |
| 64 | MDM final KYC status | mdm_final_kyc_status | String |  |  |
| 65 | MDM KYC comment code | mdm_kyc_comment_code | String |  |  |
| 66 | MDM KYC comment desc | mdm_kyc_comment_desc | String |  |  |
| 67 | MDM KYC scoring (KYC) | kyc_level | String |  |  |
| 68 | MDM KYC last update | kyc_update_date | `Date time` |  |  |
| 69 | KYC override | kyc_override | String |  |  |
| 70 | KYC override description | kyc_override_descr | String |  |  |
| 71 | Transaction id | reference_id | String |  |  |
| 72 | Transaction date | transaction_date | `Date time` |  |  |
| 73 | Approve status | approve_status | String |  |  |
| 74 | Customer code | customer_code | String |  |  |
| 75 | Normal address | normal_local_address | JSON Object |  | `thêm mới` |
|  | Address in Cambodia | address_local | String |  | `thêm mới` |
|  | Province | province | String |  | `thêm mới` |
|  | Village | village | String |  | `thêm mới` |
|  | Sub district | sub_district | String |  | `thêm mới` |
|  | District | district | String |  | `thêm mới` |
|  | Address | address | String |  | `thêm mới` |
|  | Zipcode | zipcode | String |  | `thêm mới` |
| 76 | Introducer 1 | introducer_1 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 77 | Introducer 2 | introducer_2 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 78 | Father name | father_name | String |  | `thêm mới` |
| 79 | Customer sub type | customer_sub_type | String |  | `thêm mới` |

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 6. Approve modify
## 6.1 Field description
### Request message
**Workflow id:** `SQL_CTM_APR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Transaction id | transaction_id | `Yes` | String |  |  |  |
| 2 | Customer code | customer_code | `Yes` | String | 8 |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | Transaction id | transaction_id | String |  |  |
| 2 | Customer code | customer_code | String |  |  |

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 7. Reject modify

## 7.1 Field description

### Request message

**Workflow id:** `SQL_CTM_REJ`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Transaction id | transaction_id | `Yes` | String |  |  |  |
| 2 | Customer code | customer_code | `Yes` | String | 8 |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | Transaction id | transaction_id | String |  |  |
| 2 | Customer code | customer_code | String |  |  |

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |