# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CTM_CAC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Customer type | customer_type | `Yes` | String | 1 |  |  | CCTMT |
| 2 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 3 | New customer type | new_customer_type | `Yes` | String | 1 |  |  | CCTMT1 |
| 4 | New customer code | new_customer_code | `Yes` | String | 15 |  |  | CCTMCD1 |
| 5 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 6 | Customer id | customer_id | No | `Number` |  | 0 | trường ẩn | CCTMID, lấy `id` của `new_customer_code` |
| 7 | New status | new_status | No | String | 1 |  | trường ẩn |  |
| 8 | Limit amount | limit_amount | No | `Number` |  | 0 | trường ẩn | AMT |
| 9 | Limit currency | limit_currency | No | String | 3 |  | trường ẩn | CCCR |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |
| 2 | Customer type | customer_type | String |  |  |
| 3 | Customer code | customer_code | String |  |  |
| 4 | New customer type | new_customer_type | String |  |  |
| 5 | New customer code | new_customer_code | String |  |  |
| 6 | Description | description | String |  |  |
| 7 | Customer id | customer_id | `Number` |  |  |
| 8 | New status | new_status | String | 1 |  |
| 9 | Limit amount | limit_amount | `Number` |  |  |
| 10 | Limit currency | limit_currency | String | 3 |  |
| 11 | Transaction date |  | `Date time` |  |  |
| 12 | User id |  | `Number` |  |  |
| 13 | Transaction status |  | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Customer code is existing in the system.
- New customer code is existing in the system.
- New customer code has status "Normal".

**Flow of events:**
- Transaction complete:
    - All account of "Customer code" move to "New customer code".
    - Customer code don’t have any account.
    - Accounts include deposit accounts, credit (credit accounts, sub product limit, product limit), and mortgage accounts.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Note |
| -- | ------------------- | ------------------- | ----------- | ---- |
| 1 | GET_INFO_CTMNAME | CTM_GET_INFO_FULLNAME | [CTM_GET_INFO_FULLNAME](Specification/Common/12 Customer Rulefuncs) | giá trị là `Customer code` và `Customer type` |
| 2 | GET_INFO_CTMNAME1 | CTM_GET_INFO_FULLNAME | [CTM_GET_INFO_FULLNAME](Specification/Common/12 Customer Rulefuncs) | giá trị là `New customer code` và `New customer type` |