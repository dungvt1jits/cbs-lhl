# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CTM_SEC`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 2 | Depositor name | depositor_name | `Yes` | String | 250 |  |  | CACNM |
| 3 | Depositor address | depositor_address | No | String | 250 |  |  | CCTMA |
| 4 | Current sector | current_sector | `Yes` | String | 2 |  |  | SECTOR |
| 5 | Current category | current_category | `Yes` | String | 2 |  |  | CATEGORY |
| 6 | New sector | new_sector | `Yes` | String | 2 |  |  | NEWSECTOR |
| 7 | New category | new_category | `Yes` | String | 2 |  |  | NEWCATEGORY |
| 8 | Description | description | No | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Depositor name | depositor_name | String |  |  |
| 4 | Depositor address | depositor_address | String |  |  |
| 5 | Current sector | current_sector | String |  |  |
| 6 | Current category | current_category | String |  |  |
| 7 | New sector | new_sector | String |  |  |
| 8 | New category | new_category | String |  |  |
| 9 | Description | description | String |  |  |
| 10 | Transaction date |  | `Date time` |  |  |
| 11 | User id |  | `Number` |  |  |
| 12 | Transaction status |  | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Customer code is existing in the system.

**Flow of events:**
- Transaction complete:
    - Economic sector: Update field information to new sector.
    - Customer category: Update field information to new category.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Customer code |  | String |  |  |  |
| 3 | Depositor name |  | String |  |  |  |
| 4 | Depositor address |  | String |  |  |  |
| 5 | Current sector |  | String |  |  |  |
| 6 | Current category |  | String |  |  |  |
| 7 | New sector |  | String |  |  |  |
| 8 | New category |  | String |  |  |  |
| 9 | Description |  | String |  |  |  |
| 10 | Transaction date |  | `Date` |  |  |  |
| 11 | User id |  | String |  |  |  |
| 12 | Transaction status |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_CUS_NAME | CTM_GET_INFO_RULEFUNC | [CTM_GET_INFO_RULEFUNC](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_SECTOR | CTM_GET_INFO_RULEFUNC | [CTM_GET_INFO_RULEFUNC](Specification/Common/12 Customer Rulefuncs) |