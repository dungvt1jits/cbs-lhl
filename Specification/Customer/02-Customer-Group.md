# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CustomerGroup​/SimpleSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_SEARCH_CTMGRP`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Group id | id | `Number` |  |  |
| 2 | Mater customer code | group_code | String |  |  |
| 3 | Full name | group_name | String |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/CustomerGroup​/AdvanceSearch`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_ADSEARCH_CTMGRP`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Mater customer code | group_code | No | String |  |  |  |
| 2 | Full name | group_name | No | String |  |  |  |
| 3 | Page index | page_index | No | `Number` |  |  |  |
| 4 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Group id | id | `Number` |  |  |
| 2 | Mater customer code | group_code | String |  |  |
| 3 | Full name | group_name | String |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
[Tham khảo thông tin chi tiết tại phần "Add list "Relation Customer Information" đối với customer `Type = Enterprise`"](Specification/Customer/01 Customer Profile)<br>

## 3.2 Transaction flow
**Conditions:**
- At least two customer profiles in a group.
- Must has a member is leader.
- Don't allow exists member duplicate in a group.

**Flow of events:**
- A leader has many members.
- Allow managing credit line in linkage.
- Allow managing media files in linkage.
- Auto show media files of all members in group
- Each group has a unique group code, identifying customer group.
- Don't allow to delete out system if it is using in other modules.

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/CustomerGroup​/View`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_VIEW_CTMGRP`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 2
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group id | id | `Number` |  |  |
| 2 | Group code | group_code | String | 8 |  |
| 3 | Group name | group_name | String |  |  |
| 4 | List member information | list_members | Array Object |  |  |
|  | Customer id | customer_id | `Number` |  |  |
|  | Customer code | customer_code | String | 8 |  |
|  | Full name | fullname | String |  |  |
|  | Position | position | String |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/CustomerGroup​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_UPDATE_CTMGRP`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | id | `Yes` | `Number` |  |  | không được sửa |
| 2 | List member information | list_members | `Yes` | Array Object |  |  |  |
|  | Customer id | customer_id | `Yes` | `Number` |  |  |  |
|  | Position | position | No | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group id | id | `Number` |  |  |
| 2 | Group code | group_code | String | 8 |  |
| 3 | Group name | group_name | String |  |  |
| 4 | List member information | list_members | Array Object |  |  |
|  | Customer id | customer_id | `Number` |  |  |
|  | Customer code | customer_code | String | 8 |  |
|  | Full name | fullname | String |  |  |
|  | Position | position | String |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/CustomerGroup​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_DELETE_CTMGRP`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 2
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Group id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 2
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list detail customer code
- [Get list detail customer code của "Customer Profile"](Specification/Customer/01 Customer Profile)