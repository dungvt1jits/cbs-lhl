# 1. Search common

## 1.1 Field description

### Request message

**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_SEARCH_MEDIA`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**

```json
{
    "search_text": "",
    "page_size": 10,
    "page_index": 0
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String | 8 |  |
| 2 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 3 | Media name | media_file_name | String |  |  |
| 4 | Status | media_status | String |  |  |
| 5 | Media type | media_type | String |  |  |
| 6 | Create date | open_date | `Date time` |  |  |
| 7 | Modify date | last_update_date | `Date time` |  |  |
| 8 | Expire date | expire_date | `Date time` |  |  |
| 9 | Other | other | String |  |  |
| 10 | Media file id | id | `Number` |  |  |

**Example:**

```json
{
    "total_count": 1,
    "total_pages": 1,
    "has_previous_page": false,
    "has_next_page": false,
    "items": [
      {
        "id": 1,
        "media_name": "test01",
        "media_status": "P",
        "media_type": "S",
        "customer_code": "11000001",
        "reference_type": "C",
        "open_date": "2022-04-05T00:00:00",
        "last_update_date": "2022-04-05T00:00:00",
        "expire_date": "2052-04-05T00:00:00",
        "other": "update test 01"
      }
    ],
    "page_index": 0,
    "page_size": 10
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  | No data found | Không tìm thấy dữ liệu |

# 2. Get information

## 2.1 Field description

### Request message

**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_GETINFO_MEDIA`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Customer code | customer_code | `Yes` | String | 8 |  |  |
| 2 | Media references type | reference_type | `Yes` | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |
| 4 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**

```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results media | items | Array Object |  |  |
| 1 | Media file id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Media name | media_name | String |  |  |
| 4 | Media type | media_type | String |  |  |
| 5 | Media data | media_data | String |  |  |
| 6 | Expire date | expire_date | `Date time` |  |  |
| 7 | Other | other | String |  |  |
| 8 | Media status | media_status | String |  |  |
| 9 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |

**Example:**

```json
{
    
}
```

## 2.2 Transaction flow
- Trả về danh sách các media theo reference_type tương ứng:
    - `reference_type = C`: trả về các media theo customer single tương ứng
    - `reference_type = L`: trả về các media theo customer linkage và các media của customer single liên quan (master, detail)
    - `reference_type = G`: trả về các media theo customer group và các media của customer single liên quan (master, detail)  

- Chỉ lấy danh sách các media có `media_status = A`.

## 2.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  | No data found | Không tìm thấy dữ liệu |

# 3. Get information detail

## 3.1 Field description

### Request message

**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_GETINFO2_MEDIA`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Customer code | customer_code | `Yes` | String | 8 |  |  |
| 2 | Media references type | reference_type | `Yes` | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer |

**Example:**

```json
{
    "customer_code": "",
    "reference_type": ""
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | Data 1 | data1 | String |  |  |
| 2 | Data 2 | data2 | String |  |  |
| 3 | Data 3 | data3 | String |  |  |
| 4 | Data 4 | data4 | String |  |  |

## 3.2 Transaction flow
- `reference_type = C`: 
	- 1. Customer code: customer_code
	- 2. Full name: full_name
	- 3. Date of birth: date_of_birth
	- 4. Identification: paper_number
- `reference_type = L`:
	- 1. Linkage code: linkage_code
	- 2. Master name: master_name
	- 3. Master code: master_code
	- 4. Description: description
- `reference_type = G`:
	- 1. Group code: group_code
	- 2. Group name: group_name
	- 3. Group type: group_type
	- 4. Description: description

## 3.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  | No data found | Không tìm thấy dữ liệu |

# 4. Import

## 4.1 Field description

### Request message

**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_INSERT_MEDIA`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Media data | media_data | `Yes` | String |  |  |  |
| 2 | Media name | media_name | `Yes` | String |  |  |  |
| 3 | Customer code | customer_code | `Yes` | String |  |  |  |
| 4 | Media references type | reference_type | `Yes` | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 5 | Expire date | expire_date | No | `Date time` |  |  |  |
| 6 | Other | other | No | String |  |  |  |
| 7 | Status | media_status | `Yes` | String |  |  |  |
| 8 | Media type | media_type | `Yes` | String |  |  |  |
| 9 | Create date | open_date | `Yes` | `Date time` |  |  |  |
| 10 | Modify date | last_update_date | `Yes` | `Date time` |  |  |  |

**Example:**

```json
{

}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | Media file id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Media name | media_name | String |  |  |
| 4 | Media type | media_type | String |  |  |
| 5 | Media data | media_data | String |  |  |
| 6 | Create date | open_date | `Date time` |  |  |
| 7 | Modify date | last_update_date | `Date time` |  |  |
| 8 | Other | other | String |  |  |
| 9 | Media status | media_status | String |  |  |
| 10 | Expire date | expire_date | `Date time` |  |  |
| 11 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 12 | Media thumbnail | tmbmedia | String |  |  |

**Example:**

```json
{
    
}
```

## 4.2 Transaction flow

**Conditions:**

- Customer code exists in system.

**Flow of events:**

- Import files from local computer:
  - Allow imports many files as same times.
  - File extension can be: doc, excel, photo, mp3, pdf, …
  - Don't limit size.
  - Manage files expire date.
- Allows delete file out system.
- Approve files after importing:
  - Allows approval many records as same times.

## 4.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  |  |  |

# 5. View list approve

## 5.1 Field description

### Request message

**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_LISTAPP_MEDIA`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Page size | page_size | No | `Number` |  | 0 |  |
| 2 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:**

```json
{
    "page_index": 0,
    "page_size": 10
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results | items | Array Object |  |  |
| 1 | Media file id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Media name | media_name | String |  |  |
| 4 | Media type | media_type | String |  |  |
| 5 | Media data | media_data | String |  |  |
| 6 | Other | other | String |  |  |
| 7 | Media status | media_status | String |  |  |
| 8 | Expire date | expire_date | `Date time` |  |  |
| 9 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |

**Example:**

```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  |  |  |

# 6. Approve

## 6.1 Field description

### Request message

**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_APPROVE_MEDIA`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Media file id | id | `Yes` | `Number` |  |  |  |

**Example:**

```json
{
    "id": 1
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | Media file id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Media name | media_name | String |  |  |
| 4 | Media type | media_type | String |  |  |
| 5 | Media data | media_data | String |  |  |
| 6 | Create date | open_date | `Date time` |  |  |
| 7 | Modify date | last_update_date | `Date time` |  |  |
| 8 | Other | other | String |  |  |
| 9 | Media status | media_status | String |  |  |
| 10 | Expire date | expire_date | `Date time` |  |  |
| 11 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 12 | Media thumbnail | tmbmedia | String |  |  |

**Example:**

```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  |  |  |

# 7. View

## 7.1 Field description

### Request message

**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_VIEW_MEDIA`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Media file id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 1
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | Media file id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Media name | media_name | String |  |  |
| 4 | Media type | media_type | String |  |  |
| 5 | Media data | media_data | String |  |  |
| 6 | Create date | open_date | `Date time` |  |  |
| 7 | Modify date | last_update_date | `Date time` |  |  |
| 8 | Other | other | String |  |  |
| 9 | Media status | media_status | String |  |  |
| 10 | Expire date | expire_date | `Date time` |  |  |
| 11 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 12 | Media thumbnail | tmbmedia | String |  |  |

**Example:**

```json
{
    
}
```

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  |  |  |

# 8. Modify

## 8.1 Field description

### Request message

**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_UPDATE_MEDIA`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Media file id | id | `Yes` | `Number` |  |  |  |
| 2 | Expire date | expire_date | No | `Date time` |  |  |  |
| 3 | Other | other | No | String |  |  |  |

**Example:**

```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | Media file id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Media name | media_name | String |  |  |
| 4 | Media type | media_type | String |  |  |
| 5 | Media data | media_data | String |  |  |
| 6 | Create date | open_date | `Date time` |  |  |
| 7 | Modify date | last_update_date | `Date time` |  |  |
| 8 | Other | other | String |  |  |
| 9 | Media status | media_status | String |  |  |
| 10 | Expire date | expire_date | `Date time` |  |  |
| 11 | Media references type | reference_type | String |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 12 | Media thumbnail | tmbmedia | String |  |  |

**Example:**

```json
{
    
}
```

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  |  |  |

# 9. Delete

## 9.1 Field description

### Request message

**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `SQL_DELETE_MEDIA`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
|----|------------|-----------|------------|------|--------|---------------|-------------|
| 1 | Media file id | id | `Yes` | `Number` |  |  |  |

**Example:**

```json
{
    "id": 2
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | Media file id | id | `Number` |  |  |

**Example:**

```json
{
    "id": 2
}
```

## 9.2 Transaction flow

## 9.3 List of error code
| Error code | Error description | Error explanation |
|------------|-------------------|-------------------|
|  |  |  |

