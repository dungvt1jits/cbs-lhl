# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CTM_CAS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 2 | Current status | current_status | No | String | 1 |  |  | CTXT1 |
| 3 | New status | new_status | `Yes` | String | 1 |  |  | CNSTS |
| 4 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 5 | Limit amount | limit_amount | No | `Number` |  | 0 | trường ẩn | AMT |
| 6 | Limit currency | limit_currency | No | String | 3 |  | trường ẩn | CCCR |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Customer code | customer_code | String |  |  |
| 3 | Current status | current_status | String |  |  |
| 4 | New status | new_status | String |  |  |
| 5 | Description | description | String |  |  |
| 6 | Limit amount | limit_amount | `Number` |  |  |
| 7 | Limit currency | limit_currency | String |  |  |
| 8 | Transaction date |  | `Date time` |  |  |
| 9 | User id |  | String |  |  |
| 10 | Transaction status |  | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Customer code is existing in the system.
- Customer code has different status {"Pending to approve": (P), "Disease claim": (D), "Closed": (C)}.
- New status: {"Normal": (N), "Block all activities": (S), "Disease claim": (D), "Closed": (C)}
- {New status} <> {Current status}


**Flow of events:**
- Transaction complete:
    - Status: update field information to new status.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Customer code |  | String |  |  |  |
| 3 | Current status |  | String |  |  |  |
| 4 | New status |  | String |  |  |  |
| 5 | Reason for block |  | String |  |  |  |
| 6 | Description |  | String |  |  |  |
| 7 | Transaction date |  | `Date time` |  |  |  |
| 8 | User id |  | String |  |  |  |
| 9 | Transaction status |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_CCTMCD | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_CTMNAME | CTM_GET_INFO_RULEFUNC | [CTM_GET_INFO_RULEFUNC](Specification/Common/12 Customer Rulefuncs) |