# 1. Search common
## 1.1 Field description
### Request message

**Workflow id:** `SQL_SEARCH_CTMLKG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Linkage id | id | `Number` |  |  |
| 2 | Linkage code | linkage_code | String | 8 |  |
| 3 | Master customer code | master_customer_code | String |  |  |
| 4 | Master customer name | master_customer_name | String |  |  |
| 5 | Description | linkage_description | String |  |  |
| 6 | Credit line | linkage_credit_line | `Number` |  | số có hai số thập phân |
| 7 | Group limit code | group_limit_code | String | 10 |  |

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**Workflow id:** `SQL_ADSEARCH_CTMLKG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Linkage code | linkage_code | No | String | 8 |  |  |
| 2 | Master customer code | master_customer_code | No | String | 8 |  |  |
| 3 | Master customer name | master_customer_name | No | String |  |  |  |
| 4 | Description | linkage_description | No | String |  |  |  |
| 5 | Credit line from | linkage_credit_line_from | No | `Number` |  |  | số có hai số thập phân |
| 6 | Credit line to | linkage_credit_line_to | No | `Number` |  |  | số có hai số thập phân |
| 7 | Group limit code | group_limit_code | No | String | 10 |  |  |
| 8 | Page index | page_index | No | `Number` |  |  |  |
| 9 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Linkage id | id | `Number` |  |  |
| 2 | Linkage code | linkage_code | String | 8 |  |
| 3 | Master customer code | master_customer_code | String |  |  |
| 4 | Master customer name | master_customer_name | String |  |  |
| 5 | Description | linkage_description | String |  |  |
| 6 | Credit line | linkage_credit_line | `Number` |  | số có hai số thập phân |
| 7 | Group limit code | group_limit_code | String | 10 |  |

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message

**Workflow id:** `SQL_INSERT_CTMLKG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Master customer id | master_customer_id | `Yes` | `Number` |  |  |  |
| 2 | Linkage description | linkage_description | No | String |  |  |  |
| 3 | Group limit code | group_limit_code | No | String | 15 |  |  |
| 4 | Credit line | linkage_credit_line | No | `Number` |  |  | số có hai số thập phân |
| 5 | Currency | currency_code | No | String | 3 |  |  |
| 6 | Linkage detail list | linkage_detail_list | `Yes` | Array Object |  |  |  |  |
|  | Detail customer id | detail_customer_id | `Yes` | `Number` |  |  | d_ctmlkgrl |
|  | Linkage type | linkage_type | `Yes` | String |  |  | d_ctmlkgrl |
|  | Linkage status | linkage_status | `Yes` | String |  |  | d_ctmlkgrl |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Linkage id | id | `Number` |  |  |
| 2 | Linkage code | linkage_code | String | 15 |  |
| 3 | Master customer id | master_customer_id | `Number` |  |  |
| 4 | Master customer code | master_customer_code | String | 15 |  |
| 5 | Master customer name | master_customer_name | String |  |  |
| 6 | Linkage description | linkage_description | String |  |  |
| 7 | Group limit code | group_limit_code | String | 15 |  |
| 8 | Credit line | linkage_credit_line | `Number` |  | số có hai số thập phân |
| 9 | Currency | currency_code | String | 3 |  |
| 10 | Linkage detail list | linkage_detail_list | Array Object |  |  |
|  | Detail customer id | detail_customer_id | `Number` |  |  |
|  | Detail customer code | customer_code | String | 15 |  |
|  | Detail customer name | fullname | String |  |  |
|  | Linkage type | linkage_type | String |  |  |
|  | Linkage status | linkage_status | String |  |  |

## 3.2 Transaction flow
**Conditions:**
- At least two customer profiles in a linkage.
- Don't allow linkage with it selves.

**Flow of events:**
- A "Master customer" has many "Detail customer".
- Allow managing credit line in linkage.
- Allow managing media files in linkage.
- Auto show media files of all members in linkage
- Each customer linkage has a unique linkage code, identifying customer linkage.
- Don't allow to delete out system if it is using in other modules.

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message

**Workflow id:** `SQL_VIEW_CTMLKG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Linkage id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Linkage id | id | `Number` |  |  |
| 2 | Linkage code | linkage_code | String | 8 |  |
| 3 | Master customer id | master_customer_id | `Number` |  |  |
| 4 | Master customer code | master_customer_code | String | 8 |  |
| 5 | Master customer name | master_customer_name | String |  |  |
| 6 | Linkage description | linkage_description | String |  |  |
| 7 | Group limit code | group_limit_code | String | 10 |  |
| 8 | Credit line | linkage_credit_line | `Number` |  | số có hai số thập phân |
| 9 | Currency | currency_code | String | 3 |  |
| 10 | Linkage detail list | linkage_detail_list | Array Object |  |  |
|  | Detail customer id | detail_customer_id | `Number` |  |  |
|  | Detail customer code | customer_code | String | 15 |  |
|  | Detail customer name | fullname | String |  |  |
|  | Linkage type | linkage_type | String |  |  |
|  | Linkage status | linkage_status | String |  |  |

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**Workflow id:** `SQL_UPDATE_CTMLKG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Linkage id | id | `Yes` | `Number` |  |  |  |
| 2 | Linkage description | linkage_description | No | String |  |  |  |
| 3 | Group limit code | group_limit_code | No | String | 15 |  |  |
| 4 | Credit line | linkage_credit_line | No | `Number` |  |  | số có hai số thập phân |
| 5 | Currency | currency_code | No | String | 3 |  |  |
| 6 | Linkage detail list | linkage_detail_list | `Yes` | Array Object |  |  |  |  |
|  | Detail customer id | detail_customer_id | `Yes` | `Number` |  |  | d_ctmlkgrl |
|  | Linkage type | linkage_type | `Yes` | String |  |  | d_ctmlkgrl |
|  | Linkage status | linkage_status | `Yes` | String |  |  | d_ctmlkgrl |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Linkage id | id | `Number` |  |  |
| 2 | Linkage code | linkage_code | String | 8 |  |
| 3 | Master customer id | master_customer_id | `Number` |  |  |
| 4 | Master customer code | master_customer_code | String | 8 |  |
| 5 | Master customer name | master_customer_name | String |  |  |
| 6 | Linkage description | linkage_description | String |  |  |
| 7 | Group limit code | group_limit_code | String | 10 |  |
| 8 | Credit line | linkage_credit_line | `Number` |  | số có hai số thập phân |
| 9 | Currency | currency_code | String | 3 |  |
| 10 | Linkage detail list | linkage_detail_list | Array Object |  |  |
|  | Detail customer id | detail_customer_id | `Number` |  |  |
|  | Detail customer code | customer_code | String | 15 |  |
|  | Detail customer name | fullname | String |  |  |
|  | Linkage type | linkage_type | String |  |  |
|  | Linkage status | linkage_status | String |  |  |

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**Workflow id:** `SQL_DELETE_CTMLKG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Linkage id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Linkage id | id | `Number` |  |  |

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list master customer code
## 7.1 Field description
### Request message
**Workflow id:** `SQL_LOOKUP_CTM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | No | String | 8 |  |  |
| 2 | Full name | fullname | No | String |  |  |  |
| 3 | Name in local | shortname | No | String |  |  |  |
| 4 | Paper number | paper_number | No | String |  |  |  |
| 5 | Date of birth from | dob_from | No | `Date time` |  |  |  |
| 6 | Date of birth to | dob_to | No | `Date time` |  |  |  |
| 7 | Gender | gender | No | String |  |  |  |
| 8 | Customer status | customer_status | No | String |  |  |  |
| 9 | Nationality | nation | No | String |  |  |  |
| 10 | Resident status | resident | No | String |  |  |  |
| 11 | Address | address | No | String |  |  |  |
| 12 | Old id of customer | old_id_of_customer | No | String |  |  |  |
| 13 | Group limit code | group_id | No | String | 10 |  |  |
| 14 | Page index | page_index | No | `Number` |  |  |  |
| 15 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String | 8 |  |
| 2 | Full name | fullname | String |  |  |
| 3 | Name in local | shortname | String |  |  |
| 4 | Paper number | paper_number | String |  |  |
| 5 | Date of birth | date_of_birth | `Date time` |  |  |
| 6 | Gender | gender | String |  |  |
| 7 | Customer status | customer_status | String |  |  |
| 8 | Nationality | nation | String |  |  |
| 9 | Resident status | resident | String |  |  |
| 10 | Address | contact_local_address | String |  | Nối chuỗi các sub string: address_local, village, sub_district, district, province |
| 11 | Old id of customer | old_id_of_customer | String |  |  |
| 12 | Group limit code | group_id | String | 10 |  |
| 13 | Customer id | id | `Number` |  |  |

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |

# 8. Get list signature by linkage code
- [Get information của "Customer Media Files"](Specification/Customer/04 Customer Media Files)

# 9. Get list detail customer code
- [Get list detail customer code của "Customer Profile"](Specification/Customer/01 Customer Profile)