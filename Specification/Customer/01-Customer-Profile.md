# 1. Search common
## 1.1 Field description
### Request message

**Workflow id:** `SQL_SEARCH_CTM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String | 8 |  | 
| 2 | Full name | fullname | String |  |  |
| 3 | Name in local | shortname | String |  |  |
| 4 | Paper number | paper_number | String |  |  |
| 5 | Date of birth | date_of_birth | `Date time` |  |  |
| 6 | Gender | gender | String |  |  |
| 7 | Customer status | customer_status | String |  |  |
| 8 | Nationality | nation | String |  |  |
| 9 | Resident status | resident | String |  |  |
| 10 | Address | contact_local_address | String |  | Nối chuỗi các sub string: address_local, village, sub_district, district, province |
| 11 | Old id of customer | old_id_of_customer | String |  |  |
| 12 | Group limit code | group_id | String | 10 |  |
| 13 | Customer id | id | `Number` |  |  |
| 14 | Approver | approver | String |  | `trả thêm` trả về giá trị `User name` theo ApproveUserId trong bảng SingleCustomer |

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message

**Workflow id:** `SQL_ADSEARCH_CTM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | No | String | 8 |  |  |
| 2 | Full name | fullname | No | String |  |  |  |
| 3 | Name in local | shortname | No | String |  |  |  |
| 4 | Paper number | paper_number | No | String |  |  |  |
| 5 | Date of birth from | dob_from | No | `Date time` |  |  |  |
| 6 | Date of birth to | dob_to | No | `Date time` |  |  |  |
| 7 | Gender | gender | No | String |  |  |  |
| 8 | Customer status | customer_status | No | String |  |  |  |
| 9 | Nationality | nation | No | String |  |  |  |
| 10 | Resident status | resident | No | String |  |  | `RESIDENT` |
| 11 | Address | address | No | String |  |  |  |
| 12 | Old id of customer | old_id_of_customer | No | String |  |  |  |
| 13 | Group limit code | group_id | No | String | 10 |  |  |
| 14 | Page index | page_index | No | `Number` |  |  |  |
| 15 | Page size | page_size | No | `Number` |  |  |  |
| 16 | Approver | approver | No | String |  |  | `gửi thêm` tìm by text với giá trị `User name` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String | 8 |  |
| 2 | Full name | fullname | String |  |  |
| 3 | Name in local | shortname | String |  |  |
| 4 | Paper number | paper_number | String |  |  |
| 5 | Date of birth | date_of_birth | `Date time` |  |  |
| 6 | Gender | gender | String |  |  |
| 7 | Customer status | customer_status | String |  |  |
| 8 | Nationality | nation | String |  |  |
| 9 | Resident status | resident | String |  |  |
| 10 | Address | contact_local_address | String |  | Nối chuỗi các sub string: address_local, village, sub_district, district, province |
| 11 | Old id of customer | old_id_of_customer | String |  |  |
| 12 | Group limit code | group_id | String | 10 |  |
| 13 | Customer id | id | `Number` |  |  |
| 15 | Approver | approver | String |  | `trả thêm` trả về giá trị `User name` theo ApproveUserId trong bảng SingleCustomer |

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message

**Workflow id:** `SQL_INSERT_CTM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | HO CIF ID | hocifcd | No | String |  |  |  |
| 2 | `Type` | customer_private | `Yes` | String |  | I | E = Enterprise, I = Individual |
| 3 | Title | title | No | String |  |  | `Type`=I |
| 4 | Title of organization | title_of_organization | No | String |  |  | `Type`=E |
| 5 | Suffix | suffix | No | String |  |  | `Type`=E |
| 6 | First name (en) | firstname | `Yes` | String |  |  |  |
| 7 | Last name (en) | lastname | `Yes` | String |  |  |  |
| 8 | Middle name (en) | midname | No | String |  |  |  |
| 9 | Full name | fullname | `Yes` | String |  |  |  |
| 10 | First name (local) | firstname_local | No | String |  |  |  |
| 11 | Last name (local) | lastname_local | No | String |  |  |  |
| 12 | Middle name (local) | midname_local | No | String |  |  |  |
| 13 | Name in local | shortname | No | String |  |  |  |
| 14 | Gender | gender | No | String |  |  |  |
| 15 | Date of birth/ Starting date of company | date_of_birth | `Yes` | `Date time` |  |  | dob |
| 16 | Place of birth | place_of_birth | No | String |  |  | pob |
| 17 | Nationality | nation | `Yes` | String |  | VN |  |
| 18 | Country of company | country | No | String |  | VN | `Type`=E, require |
| 19 | Paper type | paper_type | `Yes` | String |  | I |  |
| 20 | Paper number | paper_number | `Yes` | String |  |  | is unique |
| 21 | Issue date of paper | issue_date_of_paper | No | `Date time` |  | Working date |  |
| 22 | Issue place of paper | issue_place_of_paper | No | String |  |  |  |
| 23 | Expire date of paper | expire_date_of_paper | No | `Date time` |  |  |  |
| 24 | Sub paper type | paper_type_sub | No | String |  |  |  |
| 25 | Sub paper number | paper_number_sub | No | String |  |  |  |
| 26 | Issue date of sub paper | issue_date_of_sub_paper | No | `Date time` |  |  |  |
| 27 | Issue place of sub paper | issue_place_of_sub_paper | No | String |  |  |  |
| 28 | Group type | customer_group_type | No | String |  |  |  |
| 29 | Group sub type | customer_sub_group_type | No | String |  |  |  |
| 30 | Customer category | categories | `Yes` | String |  | SE |  |
| 31 | Economic sector | sector | No | String |  | I0 | `Type`=E, require |
| 32 | Sub economic sector | subsector | No | String |  | I01 | `Type`=E, require |
| 33 | Resident status | resident | `Yes` | String |  | R |  |
| 34 | Legal local address | legal_local_address | No | JSON Object |  |  |  |
|  | Address in Cambodia | address_local | No | String |  |  |  |
|  | Province | province | No | String |  |  |  |
|  | Village | village | No | String |  |  |  |
|  | Sub district | sub_district | No | String |  |  |  |
|  | District | district | No | String |  |  |  |
|  | Address | address | No | String |  |  |  |
|  | Zipcode | zipcode | No | String |  |  |  |
| 35 | Contact local address | contact_local_address | No | JSON Object |  |  |  |
|  | Address in Cambodia | address_local | No | String |  |  |  |
|  | Province | province | No | String |  |  |  |
|  | Village | village | No | String |  |  |  |
|  | Sub district | sub_district | No | String |  |  |  |
|  | District | district | No | String |  |  |  |
|  | Address | address | No | String |  |  |  |
|  | Zipcode | zipcode | No | String |  |  |  |
| 36 | Home phone | phone_home | No | String |  |  |  |
| 37 | Mobile phone | phone_mobile | No | String |  |  |  |
| 38 | Email address | email | No | String |  |  |  |
| 39 | Education | education | No | String |  |  | `Type`=I |
| 40 | Marital status | marital_status | No | String |  |  | `Type`=I |
| 41 | Occupation (HO) | profession | ~~`Yes`~~ No | String |  |  | `Type`=I |
| 42 | Business type (HO) | business_type | ~~`Yes`~~ No | String |  |  | `Type`=I |
| 43 | Financial Institute | financial | No | String |  |  | `Type`=E |
| 44 | ISIC Code | isic_code | No | String |  |  | `Type`=E |
| 45 | Managing branch code | managing_branch_code | `Yes` | String |  |  |  |
| 46 | Customer status | customer_status | `Yes` | String |  | P |  |
| 47 | Classification | classify | `Yes` | String |  |  |  |
| 48 | Is involving politicians TH | polists | ~~`Yes`~~ No | String |  |  |  |
| 49 | Is involving politicians FR | repolists | ~~`Yes`~~ No | String |  |  |  |
| 50 | Country of income | country_of_income | `Yes` | String |  |  |  |
| 51 | FATCA status | fatca_status | ~~`Yes`~~ No | String |  |  | `Type`=I |
| 52 | Government organization id | government_id | No | String |  |  | `Type`=E |
| 53 | International organization and oversea government id | international_id | No | String |  |  | `Type`=E |
| 54 | Oversea juristic id | oversea_juristic_id | No | String |  |  | `Type`=E |
| 55 | GFMIS code | gfmis_code | No | String |  |  | `Type`=E |
| 56 | Branch id | branchid | No | `Number` |  |  |  |
| 57 | Group limit code | group_id | No | String | 10 |  |  |
| 58 | Credit line | customer_credit_line | No | `Number` |  |  | số có hai số thập phân |
| 59 | Currency | currency_code | No | String | 3 |  |  |
| 60 | Customer type (HO) | customer_type | No | String |  | O030 |  |
| 61 | Primary CIF | primary_cif | No | String |  |  |  |
| 62 | AML status | mdm_amount_status | No | String |  |  |  |
| 63 | Request status | mdm_request_status | No | String |  |  |  |
| 64 | List sub type code | mdm_list_sub_type_code | No | String |  |  |  |
| 65 | List Sub type desc | mdm_list_sub_type_desc | No | String |  |  |  |
| 66 | Matching By | mdm_matching_by | No | String |  |  |  |
| 67 | KYC status | mdm_final_kyc_status | No | String |  |  |  |
| 68 | KYC comment code | mdm_kyc_comment_code | No | String |  |  |  |
| 69 | KYC comment description | mdm_kyc_comment_desc | No | String |  |  |  |
| 70 | KYC scoring (KYC) | kyc_level | No | String |  |  |  |
| 71 | KYC update date | kyc_update_date | No | `Date time` |  |  | lấy working date |
| 72 | Relation CIF 1 | relation_customer_id1 | No | `Number` |  |  | `Type`=E, `thêm mới` |
| 73 | Relation CIF 2 | relation_customer_id2 | No | `Number` |  |  | `Type`=E, `thêm mới` |
| 74 | Relation CIF 3 | relation_customer_id3 | No | `Number` |  |  | `Type`=E, `thêm mới` |
| 75 | Relation CIF 4 | relation_customer_id4 | No | `Number` |  |  | `Type`=E, `thêm mới` |
| 76 | Relation CIF 5 | relation_customer_id5 | No | `Number` |  |  | `Type`=E, `thêm mới` |
| 77 | Customer size | customer_size | No | String |  |  | `thêm mới` |
| 78 | Staff id | staff_id | `Yes` | `Number` |  |  | `thêm mới` |
| 79 | Normal address | normal_local_address | No | JSON Object |  |  | `thêm mới` |
|  | Address in Cambodia | address_local | No | String |  |  | `thêm mới` |
|  | Province | province | No | String |  |  | `thêm mới` |
|  | Village | village | No | String |  |  | `thêm mới` |
|  | Sub district | sub_district | No | String |  |  | `thêm mới` |
|  | District | district | No | String |  |  | `thêm mới` |
|  | Address | address | No | String |  |  | `thêm mới` |
|  | Zipcode | zipcode | No | String |  |  | `thêm mới` |
| 80 | Introducer 1 | introducer_1 | No | JSON Object |  |  | `thêm mới` |
|  | Name | name | No | String | 100 |  | `thêm mới` |
|  | Account no | account_no | No | String | 25 |  | `thêm mới` |
| 81 | Introducer 2 | introducer_2 | No | JSON Object |  |  | `thêm mới` |
|  | Name | name | No | String | 100 |  | `thêm mới` |
|  | Account no | account_no | No | String | 25 |  | `thêm mới` |
| 82 | Father name | father_name | `Yes` | String |  |  | `thêm mới` |
| 83 | Customer sub type | customer_sub_type | `Yes` | String |  |  | `thêm mới` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Customer id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String | 8 |  |
| 3 | HO CIF ID | hocifcd | String |  |  |
| 4 | `Type` | customer_private | String |  |  |
| 5 | Title | title | String |  | `Type`=I |
| 6 | Title of organization | title_of_organization | String |  | `Type`=E |
| 7 | Suffix | suffix | String |  | `Type`=E |
| 8 | First name (en) | firstname | String |  |  |
| 9 | Last name (en) | lastname | String |  |  |
| 10 | Middle name (en) | midname | String |  |  |
| 11 | Full name | fullname | String |  |  |
| 12 | First name (local) | firstname_local | String |  |  |
| 13 | Last name (local) | lastname_local | String |  |  |
| 14 | Middle name (local) | midname_local | String |  |  |
| 15 | Name in Cambodia | shortname | String |  |  |
| 16 | Gender | gender | String |  |  |
| 17 | Date of birth/ Starting date of company | date_of_birth | `Date time` |  |  |
| 18 | Place of birth | place_of_birth | String |  |  |
| 19 | Nationality | nation | String |  |  |
| 20 | Country of company | country | String |  | `Type`=E |
| 21 | Paper type | paper_type | String |  |  |
| 22 | Paper number | paper_number | String |  |  |
| 23 | Issue date of paper | issue_date_of_paper | `Date time` |  |  |
| 24 | Issue place of paper | issue_place_of_paper | String |  |  |
| 25 | Expire date of paper | expire_date_of_paper | `Date time` |  |  |
| 26 | Sub paper type | paper_type_sub | String |  |  |
| 27 | Sub paper number | paper_number_sub | String |  |  |
| 28 | Issue date of sub paper | issue_date_of_sub_paper | `Date time` |  |  |
| 29 | Issue place of sub paper | issue_place_of_sub_paper | String |  |  |
| 30 | Group type | customer_group_type | String |  |  |
| 31 | Group sub type | customer_sub_group_type | String |  |  |
| 32 | Customer category | categories | String |  |  |
| 33 | Economic sector | sector | String |  | `Type`=E |
| 34 | Sub economic sector | subsector | String |  | `Type`=E |
| 35 | Resident status | resident | String |  |  |
| 36 | Legal local address | legal_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Village | village | String |  |  |
|  | Sub district | sub_district | String |  |  |
|  | District | district | String |  |  |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 37 | Contact local address | contact_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Village | village | String |  |  |
|  | Sub district | sub_district | String |  |  |
|  | District | district | String |  |  |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 38 | Home phone | phone_home | String |  |  |
| 39 | Mobile phone | phone_mobile | String |  |  |
| 40 | Email address | email | String |  |  |
| 41 | Education | education | String |  | `Type`=I |
| 42 | Marital status | marital_status | String |  | `Type`=I |
| 43 | Occupation (HO) | profession | String |  | `Type`=I |
| 44 | Business type (HO) | business_type | String |  | `Type`=I |
| 45 | Financial institute | financial | String |  | `Type`=E |
| 46 | ISIC code | isic_code | String |  | `Type`=E |
| 47 | Managing branch code | managing_branch_code | String |  |  |
| 48 | Status | customer_status | String |  |  |
| 49 | Classification | classify | String |  |  |
| 50 | Is involving politicians TH | polists | String |  |  |
| 51 | Is involving politicians FR | repolists | String |  |  |
| 52 | Country of income | country_of_income | String |  |  |
| 53 | FATCA status | fatca_status | String |  | `Type`=I |
| 54 | Government organization id | government_id | String |  | `Type`=E |
| 55 | International organization and oversea government id | international_id | String |  | `Type`=E |
| 56 | Oversea juristic id | oversea_juristic_id | String |  | `Type`=E |
| 57 | GFMIS code | gfmis_code | String |  | `Type`=E |
| 58 | Open date | opendate | `Date time` |  | Working date |
| 59 | Approve date | approve_date | `Date time` |  |  |
| 60 | Last date | last_update_date | `Date time` |  | Working date |
| 61 | Branch id | branchid | `Number` |  |  |
| 62 | Staff care | staff_id | `Number` |  | User login |
| 63 | Approved by | approve_user_id | `Number` |  |  |
| 64 | Group limit code | group_id | String | 10 |  |
| 65 | Credit line | customer_credit_line | `Number` |  | số có hai số thập phân |
| 66 | Currency | currency_code | String | 3 |  |
| 67 | Customer type (HO) | customer_type | String |  |  |
| 68 | Primary CIF | primary_cif | String |  |  |
| 69 | AML status | mdm_amount_status | String |  |  |
| 70 | Request status | mdm_request_status | String |  |  |
| 71 | List sub type code | mdm_list_sub_type_code | String |  |  |
| 72 | List Sub type desc | mdm_list_sub_type_desc | String |  |  |
| 73 | Matching By | mdm_matching_by | String |  |  |
| 74 | KYC status | mdm_final_kyc_status | String |  |  |
| 75 | KYC comment code | mdm_kyc_comment_code | String |  |  |
| 76 | KYC comment description | mdm_kyc_comment_desc | String |  |  |
| 77 | KYC scoring (KYC) | kyc_level | String |  |  |
| 78 | KYC last update | kyc_update_date | `Date time` |  |  |
| 79 | KYC override | kyc_override | String |  |  |
| 80 | KYC override description | kyc_override_descr | String |  |  |
| 81 | Relation CIF 1 | relation_customer_id1 | `Number` |  | `Type`=E, `thêm mới` |
| 82 | Relation CIF 2 | relation_customer_id2 | `Number` |  | `Type`=E, `thêm mới` |
| 83 | Relation CIF 3 | relation_customer_id3 | `Number` |  | `Type`=E, `thêm mới` |
| 84 | Relation CIF 4 | relation_customer_id4 | `Number` |  | `Type`=E, `thêm mới` |
| 85 | Relation CIF 5 | relation_customer_id5 | `Number` |  | `Type`=E, `thêm mới` |
| 86 | Customer size | customer_size | String |  |  |
| 87 | Normal address | normal_local_address | JSON Object |  | `thêm mới` |
|  | Address in Cambodia | address_local | String |  | `thêm mới` |
|  | Province | province | String |  | `thêm mới` |
|  | Village | village | String |  | `thêm mới` |
|  | Sub district | sub_district | String |  | `thêm mới` |
|  | District | district | String |  | `thêm mới` |
|  | Address | address | String |  | `thêm mới` |
|  | Zipcode | zipcode | String |  | `thêm mới` |
| 88 | Introducer 1 | introducer_1 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 89 | Introducer 2 | introducer_2 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 90 | Father name | father_name | String |  | `thêm mới` |
| 91 | Customer sub type | customer_sub_type | String |  | `thêm mới` |

## 3.2 Transaction flow
**Conditions:**
- Unique:
    - Customer type is Individual: Paper type, Paper number, Date of birth, Place of birth, Full name.
    - Customer type is Enterprise: Paper type, Paper number, Start date of company, Full name.

**Flow of events:**
- Each profile has a unique. It's customer code. 
- Customer profile allow deleting when this customer not use anywhere.
- With the search customer information, allow finding the exact or a part of the information. 
- If customer uses for purpose loan, then provides to manage credit limit.
- Some important information that user need notice when input data:
    - Resident status: It involves to calculate withholding tax.
    - Resident status, Customer category: It involves posting GL in deposit module and credit module.
- Allow managing media files in profile.
- Don't allow to delete out system if it is using in other modules.
- Add new customer status is “Pending to approve”.
- The system must display a warning to the authorized about changed customer information.
- Modified data of customer must be approved or reject.
    - Approved: Modified data will apply in Customer profile.
    - Reject: Modified data will not apply in Customer profile.
  
**KYC verify:**
Tùy vào từng nghiệp vụ:
- KYC level = 0: tạo tài khoản
- KYC level = 1: có xác thực email
- KYC level = 2: có xác thực ID và face ID
- KYC level = 3: đã giao dịch thành công

**KYC conditions:**
- Không được phép hạ KYC level
- KYC level (new) > = KYC level (old)
- KYC override không để trống khi KYC level <> 0

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message

**Workflow id:** `SQL_VIEW_CTM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Customer id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String | 8 |  |
| 3 | HO CIF ID | hocifcd | String |  |  |
| 4 | `Type` | customer_private | String |  |  |
| 5 | Title | title | String |  | `Type`=I |
| 6 | Title of organization | title_of_organization | String |  | `Type`=E |
| 7 | Suffix | suffix | String |  | `Type`=E |
| 8 | First name (en) | firstname | String |  |  |
| 9 | Last name (en) | lastname | String |  |  |
| 10 | Middle name (en) | midname | String |  |  |
| 11 | Full name | fullname | String |  |  |
| 12 | First name (local) | firstname_local | String |  |  |
| 13 | Last name (local) | lastname_local | String |  |  |
| 14 | Middle name (local) | midname_local | String |  |  |
| 15 | Name in Cambodia | shortname | String |  |  |
| 16 | Gender | gender | String |  |  |
| 17 | Date of birth/ Starting date of company | date_of_birth | `Date time` |  |  |
| 18 | Place of birth | place_of_birth | String |  |  |
| 19 | Nationality | nation | String |  |  |
| 20 | Country of company | country | String |  | `Type`=E |
| 21 | Paper type | paper_type | String |  |  |
| 22 | Paper number | paper_number | String |  |  |
| 23 | Issue date of paper | issue_date_of_paper | `Date time` |  |  |
| 24 | Issue place of paper | issue_place_of_paper | String |  |  |
| 25 | Expire date of paper | expire_date_of_paper | `Date time` |  |  |
| 26 | Sub paper type | paper_type_sub | String |  |  |
| 27 | Sub paper number | paper_number_sub | String |  |  |
| 28 | Issue date of sub paper | issue_date_of_sub_paper | `Date time` |  |  |
| 29 | Issue place of sub paper | issue_place_of_sub_paper | String |  |  |
| 30 | Group type | customer_group_type | String |  |  |
| 31 | Group sub type | customer_sub_group_type | String |  |  |
| 32 | Customer category | categories | String |  |  |
| 33 | Economic sector | sector | String |  | `Type`=E |
| 34 | Sub economic sector | subsector | String |  | `Type`=E |
| 35 | Resident status | resident | String |  |  |
| 36 | Legal local address | legal_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Province name | province_name | String |  | `trả thêm` |
|  | Village | village | String |  |  |
|  | Village name | village_name | String |  | `trả thêm` |
|  | Sub district | sub_district | String |  |  |
|  | Sub district name | sub_district_name | String |  | `trả thêm` |
|  | District | district | String |  |  |
|  | District name | district_name | String |  | `trả thêm` |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 37 | Contact local address | contact_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Province name | province_name | String |  | `trả thêm` |
|  | Village | village | String |  |  |
|  | Village name | village_name | String |  | `trả thêm` |
|  | Sub district | sub_district | String |  |  |
|  | Sub district name | sub_district_name | String |  | `trả thêm` |
|  | District | district | String |  |  |
|  | District name | district_name | String |  | `trả thêm` |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 38 | Home phone | phone_home | String |  |  |
| 39 | Mobile phone | phone_mobile | String |  |  |
| 40 | Email address | email | String |  |  |
| 41 | Education | education | String |  | `Type`=I |
| 42 | Marital status | marital_status | String |  | `Type`=I |
| 43 | Occupation (HO) | profession | String |  | `Type`=I |
| 44 | Business type (HO) | business_type | String |  | `Type`=I |
| 45 | Financial institute | financial | String |  | `Type`=E |
| 46 | ISIC code | isic_code | String |  | `Type`=E |
| 47 | Managing branch code | managing_branch_code | String |  |  |
| 48 | Status | customer_status | String |  |  |
| 49 | Classification | classify | String |  |  |
| 50 | Is involving politicians TH | polists | String |  |  |
| 51 | Is involving politicians FR | repolists | String |  |  |
| 52 | Country of income | country_of_income | String |  |  |
| 53 | FATCA status | fatca_status | String |  | `Type`=I |
| 54 | Government organization id | government_id | String |  | `Type`=E |
| 55 | International organization and oversea government id | international_id | String |  | `Type`=E |
| 56 | Oversea juristic id | oversea_juristic_id | String |  | `Type`=E |
| 57 | GFMIS code | gfmis_code | String |  | `Type`=E |
| 58 | Open date | opendate | `Date time` |  |  |
| 59 | Approve date | approve_date | `Date time` |  |  |
| 60 | Last date | last_update_date | `Date time` |  |  |
| 61 | Branch id | branchid | `Number` |  |  |
| 62 | Staff care | staff_id | `Number` |  |  |
| 63 | Approved by | approve_user_id | `Number` |  |  |
| 64 | Group limit code | group_id | String | 10 |  |
| 65 | Credit line | customer_credit_line | `Number` |  | số có hai số thập phân |
| 66 | Currency | currency_code | String | 3 |  |
| 67 | Customer type (HO) | customer_type | String |  |  |
| 68 | Primary CIF | primary_cif | String |  |  |
| 69 | AML status | mdm_amount_status | String |  |  |
| 70 | Request status | mdm_request_status | String |  |  |
| 71 | List sub type code | mdm_list_sub_type_code | String |  |  |
| 72 | List Sub type desc | mdm_list_sub_type_desc | String |  |  |
| 73 | Matching By | mdm_matching_by | String |  |  |
| 74 | KYC status | mdm_final_kyc_status | String |  |  |
| 75 | KYC comment code | mdm_kyc_comment_code | String |  |  |
| 76 | KYC comment description | mdm_kyc_comment_desc | String |  |  |
| 77 | KYC scoring (KYC) | kyc_level | String |  |  |
| 78 | KYC last update | kyc_update_date | `Date time` |  |  |
| 79 | KYC override | kyc_override | String |  |  |
| 80 | KYC override description | kyc_override_descr | String |  |  |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Created by code | user_created_code | String |  | `trả thêm` |
|  | Created by name | user_created_name | String |  | `trả thêm` |
|  | Approved by code | user_approved_code | String |  | `trả thêm` |
|  | Approved by name | user_approved_name | String |  | `trả thêm` |
|  | Occupation (HO) name | profession_name | String |  | `trả thêm` |
|  | Business type (HO) name | business_type_name | String |  | `trả thêm` |
|  | Customer type (HO) name | customer_type_name | String |  | `trả thêm` |
|  | ISIC code name | isic_code_name | String |  | `trả thêm` |
|  | Managing branch name | managing_branch_name | String |  | `trả thêm` |
|  | Group limit name | group_name | String |  | `trả thêm` |
| 81 | Customer size | customer_size | String |  |  |
| 82 | Normal address | normal_local_address | JSON Object |  | `thêm mới` |
|  | Address in Cambodia | address_local | String |  | `thêm mới` |
|  | Province | province | String |  | `thêm mới` |
|  | Village | village | String |  | `thêm mới` |
|  | Sub district | sub_district | String |  | `thêm mới` |
|  | District | district | String |  | `thêm mới` |
|  | Address | address | String |  | `thêm mới` |
|  | Zipcode | zipcode | String |  | `thêm mới` |
| 83 | Introducer 1 | introducer_1 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 84 | Introducer 2 | introducer_2 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 85 | Father name | father_name | String |  | `thêm mới` |
| 86 | Customer sub type | customer_sub_type | String |  | `thêm mới` |

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message

**Workflow id:** `SQL_UPDATE_CTM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer id | id | `Yes` | `Number` |  |  | không được sửa |
| 2 | HO CIF ID | hocifcd | No | String |  |  |  |
| 3 | Title | title | `Yes` | String |  |  | `Type`=I |
| 4 | Title of organization | title_of_organization | `Yes` | String |  |  | `Type`=E |
| 5 | Suffix | suffix | No | String |  |  | `Type`=E |
| 6 | First name (en) | firstname | `Yes` | String |  |  |  |
| 7 | Last name (en) | lastname | `Yes` | String |  |  |  |
| 8 | Middle name (en) | midname | No | String |  |  |  |
| 9 | Full name | fullname | `Yes` | String |  |  |  |
| 10 | First name (local) | firstname_local | No | String |  |  |  |
| 11 | Last name (local) | lastname_local | No | String |  |  |  |
| 12 | Middle name (local) | midname_local | No | String |  |  |  |
| 13 | Name in Cambodia | shortname | No | String |  |  |  |
| 14 | Gender | gender | `Yes` | String |  |  |  |
| 15 | Date of birth/ Starting date of company | date_of_birth | `Yes` | `Date time` |  |  |  |
| 16 | Place of birth | place_of_birth | No | String |  |  |  |
| 17 | Nationality | nation | `Yes` | String |  | VN |  |
| 18 | Country of company | country | No | String |  | VN | `Type`=E, require |
| 19 | Paper type | paper_type | `Yes` | String |  | I |  |
| 20 | Paper number | paper_number | `Yes` | String |  |  |  |
| 21 | Issue date of paper | issue_date_of_paper | No | `Date time` |  | Working date |  |
| 22 | Issue place of paper | issue_place_of_paper | No | String |  |  |  |
| 23 | Expire date of paper | expire_date_of_paper | No | `Date time` |  |  |  |
| 24 | Sub paper type | paper_type_sub | No | String |  |  |  |
| 25 | Sub paper number | paper_number_sub | No | String |  |  |  |
| 26 | Issue date of sub paper | issue_date_of_sub_paper | No | `Date time` |  |  |  |
| 27 | Issue place of sub paper | issue_place_of_sub_paper | No | String |  |  |  |
| 28 | Group type | customer_group_type | No | String |  |  |  |
| 29 | Group sub type | customer_sub_group_type | No | String |  |  |  |
| 30 | Economic sector | sector | No | String |  | I0 | `Type`=E, require |
| 31 | Sub economic sector | subsector | No | String |  | I01 | `Type`=E, require |
| 32 | Resident status | resident | `Yes` | String |  | R |  |
| 33 | Legal local address | legal_local_address | No | JSON Object |  |  |  |
|  | Address in Cambodia | address_local | No | String |  |  |  |
|  | Province | province | No | String |  |  |  |
|  | Village | village | No | String |  |  |  |
|  | Sub district | sub_district | No | String |  |  |  |
|  | District | district | No | String |  |  |  |
|  | Address | address | No | String |  |  |  |
|  | Zipcode | zipcode | No | String |  |  |  |
| 34 | Contact local address | contact_local_address | No | JSON Object |  |  |  |
|  | Address in Cambodia | address_local | No | String |  |  |  |
|  | Province | province | No | String |  |  |  |
|  | Village | village | No | String |  |  |  |
|  | Sub district | sub_district | No | String |  |  |  |
|  | District | district | No | String |  |  |  |
|  | Address | address | No | String |  |  |  |
|  | Zipcode | zipcode | No | String |  |  |  |
| 35 | Home phone | phone_home | No | String |  |  |  |
| 36 | Mobile phone | phone_mobile | No | String |  |  |  |
| 37 | Email address | email | No | String |  |  |  |
| 38 | Education | education | No | String |  |  | `Type`=I |
| 39 | Marital status | marital_status | No | String |  |  | `Type`=I |
| 40 | Occupation (HO) | profession | ~~`Yes`~~ No | String |  |  | `Type`=I |
| 41 | Business type (HO) | business_type | ~~`Yes`~~ No | String |  |  | `Type`=I |
| 42 | Financial institute | financial | No | String |  |  | `Type`=E |
| 43 | ISIC code | isic_code | No | String |  |  | `Type`=E |
| 44 | Managing branch code | managing_branch_code | `Yes` | String |  |  |  |
| 45 | Classification | classify | `Yes` | String |  |  |  |
| 46 | Is involving politicians TH | polists | ~~`Yes`~~ No | String |  |  |  |
| 47 | Is involving politicians FR | repolists | ~~`Yes`~~ No | String |  |  |  |
| 48 | Country of income | country_of_income | `Yes` | String |  |  |  |
| 49 | FATCA status | fatca_status | ~~`Yes`~~ No | String |  |  | `Type`=I |
| 50 | Government organization id | government_id | No | String |  |  | `Type`=E |
| 51 | International organization and oversea government id | international_id | No | String |  |  | `Type`=E |
| 52 | Oversea juristic id | oversea_juristic_id | No | String |  |  | `Type`=E |
| 53 | GFMIS code | gfmis_code | No | String |  |  | `Type`=E |
| 54 | Last date | last_update_date | `Yes` | `Date time` |  | Working date |  |
| 55 | Group limit code | group_id | No | String | 10 |  |  |
| 56 | Credit line | customer_credit_line | No | `Number` |  |  | số có hai số thập phân |
| 57 | Currency | currency_code | No | String | 3 |  |  |
| 58 | Customer type (HO) | customer_type | No | String |  | O030 |  |
| 59 | Primary CIF | primary_cif | No | String |  |  |  |
| 60 | AML status | mdm_amount_status | No | String |  |  |  |
| 61 | Request status | mdm_request_status | No | String |  |  |  |
| 62 | List sub type code | mdm_list_sub_type_code | No | String |  |  |  |
| 63 | List Sub type desc | mdm_list_sub_type_desc | No | String |  |  |  |
| 64 | Matching By | mdm_matching_by | No | String |  |  |  |
| 65 | KYC status | mdm_final_kyc_status | No | String |  |  |  |
| 66 | KYC comment code | mdm_kyc_comment_code | No | String |  |  |  |
| 67 | KYC comment description | mdm_kyc_comment_desc | No | String |  |  |  |
| 68 | KYC scoring (KYC) | kyc_level | No | String |  |  |  |
| 69 | KYC last update | kyc_update_date | No | `Date time` |  |  |  |
| 70 | KYC override | kyc_override | No | String |  |  |  |
| 71 | KYC override description | kyc_override_descr | No | String |  |  |  |
| 72 | Customer size | customer_size | No | String |  |  | `thêm mới` |
| 73 | Normal address | normal_local_address | No | JSON Object |  |  | `thêm mới` |
|  | Address in Cambodia | address_local | No | String |  |  | `thêm mới` |
|  | Province | province | No | String |  |  | `thêm mới` |
|  | Village | village | No | String |  |  | `thêm mới` |
|  | Sub district | sub_district | No | String |  |  | `thêm mới` |
|  | District | district | No | String |  |  | `thêm mới` |
|  | Address | address | No | String |  |  | `thêm mới` |
|  | Zipcode | zipcode | No | String |  |  | `thêm mới` |
| 74 | Introducer 1 | introducer_1 | No | JSON Object |  |  | `thêm mới` |
|  | Name | name | No | String | 100 |  | `thêm mới` |
|  | Account no | account_no | No | String | 25 |  | `thêm mới` |
| 75 | Introducer 2 | introducer_2 | No | JSON Object |  |  | `thêm mới` |
|  | Name | name | No | String | 100 |  | `thêm mới` |
|  | Account no | account_no | No | String | 25 |  | `thêm mới` |
| 76 | Father name | father_name | `Yes` | String |  |  | `thêm mới` |
| 78 | Customer sub type | customer_sub_type | `Yes` | String |  |  | `thêm mới` |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Customer id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String | 8 |  |
| 3 | HO CIF ID | hocifcd | String |  |  |
| 4 | `Type` | customer_private | String |  |  |
| 5 | Title | title | String |  | `Type`=I |
| 6 | Title of organization | title_of_organization | String |  | `Type`=E |
| 7 | Suffix | suffix | String |  | `Type`=E |
| 8 | First name (en) | firstname | String |  |  |
| 9 | Last name (en) | lastname | String |  |  |
| 10 | Middle name (en) | midname | String |  |  |
| 11 | Full name | fullname | String |  |  |
| 12 | First name (local) | firstname_local | String |  |  |
| 13 | Last name (local) | lastname_local | String |  |  |
| 14 | Middle name (local) | midname_local | String |  |  |
| 15 | Name in Cambodia | shortname | String |  |  |
| 16 | Gender | gender | String |  |  |
| 17 | Date of birth/ Starting date of company | date_of_birth | `Date time` |  |  |
| 18 | Place of birth | place_of_birth | String |  |  |
| 19 | Nationality | nation | String |  |  |
| 20 | Country of company | country | String |  | `Type`=E |
| 21 | Paper type | paper_type | String |  |  |
| 22 | Paper number | paper_number | String |  |  |
| 23 | Issue date of paper | issue_date_of_paper | `Date time` |  |  |
| 24 | Issue place of paper | issue_place_of_paper | String |  |  |
| 25 | Expire date of paper | expire_date_of_paper | `Date time` |  |  |
| 26 | Sub paper type | paper_type_sub | String |  |  |
| 27 | Sub paper number | paper_number_sub | String |  |  |
| 28 | Issue date of sub paper | issue_date_of_sub_paper | `Date time` |  |  |
| 29 | Issue place of sub paper | issue_place_of_sub_paper | String |  |  |
| 30 | Group type | customer_group_type | String |  |  |
| 31 | Group sub type | customer_sub_group_type | String |  |  |
| 32 | Customer category | categories | String |  |  |
| 33 | Economic sector | sector | String |  | `Type`=E |
| 34 | Sub economic sector | subsector | String |  | `Type`=E |
| 35 | Resident status | resident | String |  |  |
| 36 | Legal local address | legal_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Village | village | String |  |  |
|  | Sub district | sub_district | String |  |  |
|  | District | district | String |  |  |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 37 | Contact local address | contact_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Village | village | String |  |  |
|  | Sub district | sub_district | String |  |  |
|  | District | district | String |  |  |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 38 | Home phone | phone_home | String |  |  |
| 39 | Mobile phone | phone_mobile | String |  |  |
| 40 | Email address | email | String |  |  |
| 41 | Education | education | String |  | `Type`=I |
| 42 | Marital status | marital_status | String |  | `Type`=I |
| 43 | Occupation (HO) | profession | String |  | `Type`=I |
| 44 | Business type (HO) | business_type | String |  | `Type`=I |
| 45 | Financial institute | financial | String |  | `Type`=E |
| 46 | ISIC code | isic_code | String |  | `Type`=E |
| 47 | Managing branch code | managing_branch_code | String |  |  |
| 48 | Status | customer_status | String |  |  |
| 49 | Classification | classify | String |  |  |
| 50 | Is involving politicians TH | polists | String |  |  |
| 51 | Is involving politicians FR | repolists | String |  |  |
| 52 | Country of income | country_of_income | String |  |  |
| 53 | FATCA status | fatca_status | String |  | `Type`=I |
| 54 | Government organization id | government_id | String |  | `Type`=E |
| 55 | International organization and oversea government id | international_id | String |  | `Type`=E |
| 56 | Oversea juristic id | oversea_juristic_id | String |  | `Type`=E |
| 57 | GFMIS code | gfmis_code | String |  | `Type`=E |
| 58 | Open date | opendate | `Date time` |  |  |
| 59 | Approve date | approve_date | `Date time` |  |  |
| 60 | Last date | last_update_date | `Date time` |  |  |
| 61 | Branch id | branchid | `Number` |  |  |
| 62 | Staff care | staff_id | `Number` |  |  |
| 63 | Approved by | approve_user_id | `Number` |  |  |
| 64 | Group limit code | group_id | String | 10 |  |
| 65 | Credit line | customer_credit_line | `Number` |  | số có hai số thập phân |
| 66 | Currency | currency_code | String | 3 |  |
| 67 | Customer type (HO) | customer_type | String |  |  |
| 68 | Primary CIF | primary_cif | String |  |  |
| 69 | AML status | mdm_amount_status | String |  |  |
| 70 | Request status | mdm_request_status | String |  |  |
| 71 | List sub type code | mdm_list_sub_type_code | String |  |  |
| 72 | List Sub type desc | mdm_list_sub_type_desc | String |  |  |
| 73 | Matching By | mdm_matching_by | String |  |  |
| 74 | KYC status | mdm_final_kyc_status | String |  |  |
| 75 | KYC comment code | mdm_kyc_comment_code | String |  |  |
| 76 | KYC comment description | mdm_kyc_comment_desc | String |  |  |
| 77 | KYC scoring (KYC) | kyc_level | String |  |  |
| 78 | KYC last update | kyc_update_date | `Date time` |  |  |
| 79 | KYC override | kyc_override | String |  |  |
| 80 | KYC override description | kyc_override_descr | String |  |  |
| 81 | Customer size | customer_size | String |  |  |
| 82 | Normal address | normal_local_address | JSON Object |  | `thêm mới` |
|  | Address in Cambodia | address_local | String |  | `thêm mới` |
|  | Province | province | String |  | `thêm mới` |
|  | Village | village | String |  | `thêm mới` |
|  | Sub district | sub_district | String |  | `thêm mới` |
|  | District | district | String |  | `thêm mới` |
|  | Address | address | String |  | `thêm mới` |
|  | Zipcode | zipcode | String |  | `thêm mới` |
| 83 | Introducer 1 | introducer_1 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 84 | Introducer 2 | introducer_2 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 85 | Father name | father_name | String |  | `thêm mới` |
| 86 | Customer sub type | customer_sub_type | String |  | `thêm mới` |

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message

**Workflow id:** `SQL_DELETE_CTM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Customer id | id | `Number` |  |  |

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list signature by customer id
- [Get signature](Specification/Common/02 Signature)

# 8. Get list relation customer by customer code
## 8.1 Field description
### Request message

**Workflow id:** `SQL_RELATION_CTMGRP`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Group code | group_code | `Yes` | String |  |  | là `customer code` liên quan |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String |  |  |
| 2 | Customer name | fullname | String |  |  |
| 3 | Type | customer_private | String |  | `trả thêm` |
| 4 | Date of birth | date_of_birth | `Date time` |  |  |
| 5 | Paper type | paper_type | String |  |  |
| 6 | Paper number | paper_number | String |  |  |
| 7 | Customer type | customer_type | String |  |  |

## 8.2 Transaction flow
- Lấy danh sách customer member trong customer group theo group code tương ướng customer code.

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 9. Get list deposit by customer id
## 9.1 Field description
### Request message

**Workflow id:** `DPT_GETLISTDPTBYCUSTID`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer id | customer_id | `Yes` | `Number` |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results | items | Array Object |  |  |
| 1 | Branch code | branch_code | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Account type | account_type | String |  |  |
| 5 | Balance | current_balance | `Number` |  | số có hai số thập phân |
| 6 | MFA | mfa | String |  |  |

## 9.2 Transaction flow

## 9.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 10. Get list credit by customer id
## 10.1 Field description
### Request message

**Workflow id:** `CRD_LIST_CREDIT_ACCOUNT_BY_CUSTOMER_ID`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer id | customer_id | `Yes` | `Number` |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results | items | Array Object |  |  |
| 1 | Branch code | branch_code | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Account type | account_type | String |  |  |
| 5 | Balance | balance | `Number` |  | số có hai số thập phân |

## 10.2 Transaction flow

## 10.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 11. View by customer code
## 11.1 Field description
### Request message

**Workflow id:** `SQL_VIEWBYCODE_CTM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Customer id | id | `Number` |  |  |
| 2 | Customer code | customer_code | String | 8 |  |
| 3 | HO CIF ID | hocifcd | String |  |  |
| 4 | `Type` | customer_private | String |  |  |
| 5 | Title | title | String |  | `Type`=I |
| 6 | Title of organization | title_of_organization | String |  | `Type`=E |
| 7 | Suffix | suffix | String |  | `Type`=E |
| 8 | First name (en) | firstname | String |  |  |
| 9 | Last name (en) | lastname | String |  |  |
| 10 | Middle name (en) | midname | String |  |  |
| 11 | Full name | fullname | String |  |  |
| 12 | First name (local) | firstname_local | String |  |  |
| 13 | Last name (local) | lastname_local | String |  |  |
| 14 | Middle name (local) | midname_local | String |  |  |
| 15 | Name in Cambodia | shortname | String |  |  |
| 16 | Gender | gender | String |  |  |
| 17 | Date of birth/ Starting date of company | date_of_birth | `Date time` |  |  |
| 18 | Place of birth | place_of_birth | String |  |  |
| 19 | Nationality | nation | String |  |  |
| 20 | Country of company | country | String |  | `Type`=E |
| 21 | Paper type | paper_type | String |  |  |
| 22 | Paper number | paper_number | String |  |  |
| 23 | Issue date of paper | issue_date_of_paper | `Date time` |  |  |
| 24 | Issue place of paper | issue_place_of_paper | String |  |  |
| 25 | Expire date of paper | expire_date_of_paper | `Date time` |  |  |
| 26 | Sub paper type | paper_type_sub | String |  |  |
| 27 | Sub paper number | paper_number_sub | String |  |  |
| 28 | Issue date of sub paper | issue_date_of_sub_paper | `Date time` |  |  |
| 29 | Issue place of sub paper | issue_place_of_sub_paper | String |  |  |
| 30 | Group type | customer_group_type | String |  |  |
| 31 | Group sub type | customer_sub_group_type | String |  |  |
| 32 | Customer category | categories | String |  |  |
| 33 | Economic sector | sector | String |  | `Type`=E |
| 34 | Sub economic sector | subsector | String |  | `Type`=E |
| 35 | Resident status | resident | String |  |  |
| 36 | Legal local address | legal_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Province name | province_name | String |  | `trả thêm` |
|  | Village | village | String |  |  |
|  | Village name | village_name | String |  | `trả thêm` |
|  | Sub district | sub_district | String |  |  |
|  | Sub district name | sub_district_name | String |  | `trả thêm` |
|  | District | district | String |  |  |
|  | District name | district_name | String |  | `trả thêm` |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 37 | Contact local address | contact_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_local | String |  |  |
|  | Province | province | String |  |  |
|  | Province name | province_name | String |  | `trả thêm` |
|  | Village | village | String |  |  |
|  | Village name | village_name | String |  | `trả thêm` |
|  | Sub district | sub_district | String |  |  |
|  | Sub district name | sub_district_name | String |  | `trả thêm` |
|  | District | district | String |  |  |
|  | District name | district_name | String |  | `trả thêm` |
|  | Address | address | String |  |  |
|  | Zipcode | zipcode | String |  |  |
| 38 | Home phone | phone_home | String |  |  |
| 39 | Mobile phone | phone_mobile | String |  |  |
| 40 | Email address | email | String |  |  |
| 41 | Education | education | String |  | `Type`=I |
| 42 | Marital status | marital_status | String |  | `Type`=I |
| 43 | Occupation (HO) | profession | String |  | `Type`=I |
| 44 | Business type (HO) | business_type | String |  | `Type`=I |
| 45 | Financial institute | financial | String |  | `Type`=E |
| 46 | ISIC code | isic_code | String |  | `Type`=E |
| 47 | Managing branch code | managing_branch_code | String |  |  |
| 48 | Status | customer_status | String |  |  |
| 49 | Classification | classify | String |  |  |
| 50 | Is involving politicians TH | polists | String |  |  |
| 51 | Is involving politicians FR | repolists | String |  |  |
| 52 | Country of income | country_of_income | String |  |  |
| 53 | FATCA status | fatca_status | String |  | `Type`=I |
| 54 | Government organization id | government_id | String |  | `Type`=E |
| 55 | International organization and oversea government id | international_id | String |  | `Type`=E |
| 56 | Oversea juristic id | oversea_juristic_id | String |  | `Type`=E |
| 57 | GFMIS code | gfmis_code | String |  | `Type`=E |
| 58 | Open date | opendate | `Date time` |  |  |
| 59 | Approve date | approve_date | `Date time` |  |  |
| 60 | Last date | last_update_date | `Date time` |  |  |
| 61 | Branch id | branchid | `Number` |  |  |
| 62 | Staff care | staff_id | `Number` |  |  |
| 63 | Approved by | approve_user_id | `Number` |  |  |
| 64 | Group limit code | group_id | String | 10 |  |
| 65 | Credit line | customer_credit_line | `Number` |  | số có hai số thập phân |
| 66 | Currency | currency_code | String | 3 |  |
| 67 | Customer type (HO) | customer_type | String |  |  |
| 68 | Primary CIF | primary_cif | String |  |  |
| 69 | AML status | mdm_amount_status | String |  |  |
| 70 | Request status | mdm_request_status | String |  |  |
| 71 | List sub type code | mdm_list_sub_type_code | String |  |  |
| 72 | List Sub type desc | mdm_list_sub_type_desc | String |  |  |
| 73 | Matching By | mdm_matching_by | String |  |  |
| 74 | KYC status | mdm_final_kyc_status | String |  |  |
| 75 | KYC comment code | mdm_kyc_comment_code | String |  |  |
| 76 | KYC comment description | mdm_kyc_comment_desc | String |  |  |
| 77 | KYC scoring (KYC) | kyc_level | String |  |  |
| 78 | KYC last update | kyc_update_date | `Date time` |  |  |
| 79 | KYC override | kyc_override | String |  |  |
| 80 | KYC override description | kyc_override_descr | String |  |  |
| 81 | Branch code | branch_code | String |  | `trả thêm` |
| 82 | Created by code | user_created_code | String |  | `trả thêm` |
| 83 | Created by name | user_created_name | String |  | `trả thêm` |
| 84 | Approved by code | user_approved_code | String |  | `trả thêm` |
| 85 | Approved by name | user_approved_name | String |  | `trả thêm` |
| 86 | Occupation (HO) name | profession_name | String |  | `trả thêm` |
| 87 | Business type (HO) name | business_type_name | String |  | `trả thêm` |
| 88 | Customer type (HO) name | customer_type_name | String |  | `trả thêm` |
| 89 | ISIC code name | isic_code_name | String |  | `trả thêm` |
| 90 | Managing branch name | managing_branch_name | String |  | `trả thêm` |
| 91 | Group limit name | group_name | String |  | `trả thêm` |
| 92 | Normal address | normal_local_address | JSON Object |  | `thêm mới` |
|  | Address in Cambodia | address_local | String |  | `thêm mới` |
|  | Province | province | String |  | `thêm mới` |
|  | Village | village | String |  | `thêm mới` |
|  | Sub district | sub_district | String |  | `thêm mới` |
|  | District | district | String |  | `thêm mới` |
|  | Address | address | String |  | `thêm mới` |
|  | Zipcode | zipcode | String |  | `thêm mới` |
| 93 | Introducer 1 | introducer_1 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 94 | Introducer 2 | introducer_2 | JSON Object |  | `thêm mới` |
|  | Name | name | String |  | `thêm mới` |
|  | Account no | account_no | String |  | `thêm mới` |
| 95 | Father name | father_name | String |  | `thêm mới` |
| 96 | Customer sub type | customer_sub_type | String |  | `thêm mới` |

## 11.2 Transaction flow

## 11.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 12. Get list detail customer code (lấy ra danh sách các customer, trừ customer code được gửi đi)
## 12.1 Field description
### Request message

**Workflow id:** `SQL_LOOKUP_LISTNOT_CTM`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | `Yes` | String | 8 |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String | 8 |  | 
| 2 | Full name | fullname | String |  |  |
| 3 | Customer id | id | `Number` |  |  |

## 12.2 Transaction flow

## 12.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |

# 13. Get lookup list Address
## Get lookup list Province
### Request message
- **Workflow id:** `SQL_SUBSEARCH_PROVINCE`
- Mô tả thêm phần address:
    - Province: gửi "code_name": "LCPROVINE" và "code_group": "CTM"

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | No | String |  |  |  |
| 2 | Code name | code_name | No | String |  | `LCPROVINE` |  |
| 3 | Caption | caption | No | String |  |  |  |
| 4 | Code group | code_group | No | String |  | `CTM` |  |
| 5 | Code index | code_index | No | `Number` |  |  | số nguyên |
| 6 | Code index from | code_index_from | No | `Number` |  |  | số nguyên |
| 7 | Code index to | code_index_to | No | `Number` |  |  | số nguyên |
| 8 | Field link | ftag | No | String |  |  |  |
| 9 | Page index | page_index | No | `Number` |  |  |  |
| 10 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption | caption | String |  |  |
| 5 | Code of group | code_group | String |  |  |
| 6 | Code index | code_index | `Number` |  | số nguyên |
| 7 | Field link | ftag | String |  |  |
| 8 | Is visible | visible | `Number` |  | 1: true, 0: false |

## Get lookup list District
### Request message
- **Workflow id:** `SQL_SUBSEARCH_DISTRICT`
- Mô tả thêm phần address:
    - Province: gửi "code_name": "LCPROVINE" và "code_group": "CTM"
    - District: gửi "code_name": "LCDISTRICT", "code_group": "CTM" và "code_id" của `Province` đã chọn

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | No | String |  | "code_id" của `Province` đã chọn |  |
| 2 | Code name | code_name | No | String |  | `LCDISTRICT` |  |
| 3 | Caption | caption | No | String |  |  |  |
| 4 | Code group | code_group | No | String |  | `CTM` |  |
| 5 | Code index | code_index | No | `Number` |  |  | số nguyên |
| 6 | Code index from | code_index_from | No | `Number` |  |  | số nguyên |
| 7 | Code index to | code_index_to | No | `Number` |  |  | số nguyên |
| 8 | Field link | ftag | No | String |  |  |  |
| 9 | Page index | page_index | No | `Number` |  |  |  |
| 10 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption | caption | String |  |  |
| 5 | Code of group | code_group | String |  |  |
| 6 | Code index | code_index | `Number` |  | số nguyên |
| 7 | Field link | ftag | String |  |  |
| 8 | Is visible | visible | `Number` |  | 1: true, 0: false |

## Get lookup list Sub district
### Request message
- **Workflow id:** `SQL_SUBSEARCH_SDISTRICT`
- Mô tả thêm phần address:
    - Province: gửi "code_name": "LCPROVINE" và "code_group": "CTM"
    - District: gửi "code_name": "LCDISTRICT", "code_group": "CTM" và "code_id" của `Province` đã chọn
    - Sub district: gửi "code_name": "LCSDISTRICT", "code_group": "CTM" và "code_id" của `District` đã chọn

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | No | String |  | "code_id" của `District` đã chọn |  |
| 2 | Code name | code_name | No | String |  | `LCSDISTRICT` |  |
| 3 | Caption | caption | No | String |  |  |  |
| 4 | Code group | code_group | No | String |  | `CTM` |  |
| 5 | Code index | code_index | No | `Number` |  |  | số nguyên |
| 6 | Code index from | code_index_from | No | `Number` |  |  | số nguyên |
| 7 | Code index to | code_index_to | No | `Number` |  |  | số nguyên |
| 8 | Field link | ftag | No | String |  |  |  |
| 9 | Page index | page_index | No | `Number` |  |  |  |
| 10 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption | caption | String |  |  |
| 5 | Code of group | code_group | String |  |  |
| 6 | Code index | code_index | `Number` |  | số nguyên |
| 7 | Field link | ftag | String |  |  |
| 8 | Is visible | visible | `Number` |  | 1: true, 0: false |

## Get lookup list Village
### Request message
- **Workflow id:** `SQL_SUBSEARCH_VILLAGE`
- Mô tả thêm phần address:
    - Province: gửi "code_name": "LCPROVINE" và "code_group": "CTM"
    - District: gửi "code_name": "LCDISTRICT", "code_group": "CTM" và "code_id" của `Province` đã chọn
    - Sub district: gửi "code_name": "LCSDISTRICT", "code_group": "CTM" và "code_id" của `District` đã chọn
    - Village: gửi "code_name": "LCVILLAGE", "code_group": "CTM" và "code_id" của `Sub district` đã chọn

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | No | String |  | "code_id" của `Sub district` đã chọn |  |
| 2 | Code name | code_name | No | String |  | `LCVILLAGE` |  |
| 3 | Caption | caption | No | String |  |  |  |
| 4 | Code group | code_group | No | String |  | `CTM` |  |
| 5 | Code index | code_index | No | `Number` |  |  | số nguyên |
| 6 | Code index from | code_index_from | No | `Number` |  |  | số nguyên |
| 7 | Code index to | code_index_to | No | `Number` |  |  | số nguyên |
| 8 | Field link | ftag | No | String |  |  |  |
| 9 | Page index | page_index | No | `Number` |  |  |  |
| 10 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption | caption | String |  |  |
| 5 | Code of group | code_group | String |  |  |
| 6 | Code index | code_index | `Number` |  | số nguyên |
| 7 | Field link | ftag | String |  |  |
| 8 | Is visible | visible | `Number` |  | 1: true, 0: false |

# 14. Get list relation customer by customer code and customer type
## 14.1 Field description
### Request message

**Workflow id:** `CTM_GET_LIST_RELATION`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Customer code | customer_code | `Yes` | String |  |  | là `Master customer code` của linkage hoặc group |
| 2 | Customer type | customer_type | `Yes` | String |  |  | C: Single customer, L: Customer linkage, G: Group of customer |
| 3 | Page size | page_size | No | `Number` |  | 0 |  |
| 4 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Results | items | Array Object |  |  |
| 1 | Customer code | customer_code | String |  |  |
| 2 | Customer name | fullname | String |  |  |
| 3 | Type | customer_private | String |  | `trả thêm` |
| 4 | Date of birth | date_of_birth | `Date time` |  |  |
| 5 | Paper type | paper_type | String |  |  |
| 6 | Paper number | paper_number | String |  |  |

## 14.2 Transaction flow
- customer_type = L: Lấy danh sách customer member trong Customer linkage theo Master customer code tương ứng.
- customer_type = G: Lấy danh sách customer member trong customer group theo Master customer code tương ứng.
- customer_type = C: Trả về danh sách rỗng.
