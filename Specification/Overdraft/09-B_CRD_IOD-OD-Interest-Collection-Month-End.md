# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Credit account number |  | `Yes` | String | 25 |  |  |
| 2 | Deposit account number |  | `Yes` | String | 25 |  |  |
| 3 | Interest amount |  | `Yes` | `Number` |  |  | số có hai số thập phân |
| 4 | Description |  | `Yes` | String | 250 |  |  |
| 5 | Transaction date |  | `Yes` | `Date` |  | Working date |  |
| 6 | User id |  | `Yes` | String |  | User login |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Credit account number |  | String |  |  |  |
| 3 | Deposit account number |  | String |  |  |  |
| 4 | Interest amount |  | `Number` |  |  | số có hai số thập phân |
| 5 | Description |  | String |  |  |  |
| 6 | Transaction date |  | `Date` |  |  |  |
| 7 | User id |  | String |  |  |  |
| 8 | Transaction status |  | String |  |  |  |
| 9 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | `Number` |  |  |  |
|  | Index in group |  | `Number` |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | `Number` |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 1.2 Transaction flow
**Conditions:**
- Working date is end of month.
- Credit account has interest due > 0.00
- Deposit account: exists with status “Normal”.
- Credit account: exists with status “Normal”.
- Deposit has balance:
    - Deposit balance < 0.00
        - Credit limit - Absolute value of [Deposit balance] >= Interest due
    - Deposit balance >= 0.00

**Flow of events:**
- Decrease amount of deposit account. Amount is balance but not include amount is blocked (hold).
- If deposit account doesn’t have balance, system will skip collection auto.
- If deposit account has tiny amount and not enough. System will collect all amount that it has.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

- Credit account status <> "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Interest collect | Currency of credit account |
| 1 | C | INTEREST {K=0,1,2,3,4} | Interest collect | Currency of credit account |
| 2 | D | PAID_INTEREST_S | Interest collect | Currency of credit account |
| 2 | C | PAID_INTEREST {K=2,3,4} | Interest collect | Currency of credit account |

{K=0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss}

- Credit account status is "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Interest collect | Currency of credit account |
| 1 | C | BAD_DEBT_INTEREST_WO | Interest collect | Currency of credit account |
| 2 | D | WOFF_PAID_INTEREST | Interest collect | Currency of credit account |
| 2 | C | WRITEOFF_INTEREST | Interest collect | Currency of credit account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |