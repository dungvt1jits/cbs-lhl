# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_COD`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | DD account number | dd_account_number | `Yes` | String | 25 |  |  | PDACC |
| 2 | Deposit balance | deposit_balance | No | `Number` |  | 0 | số có hai số thập phân, có thể < or = or > 0.00 | CPRIN |
| 3 | Interest accrual | interest_accrual | No | `Number` |  | 0 | số có hai số thập phân | CINT |
| 4 | Interest paid | interest_paid | No | `Number` |  | 0 | số có hai số thập phân | CIPRE |
| 5 | Interest receivable | interest_receivable | No | `Number` |  | 0 | số có hai số thập phân | CIPBL |
| 6 | Interest due | interest_due | No | `Number` |  | 0 | số có hai số thập phân | CIDUE |
| 7 | Credit balance | credit_balance | No | `Number` |  | 0 | số có hai số thập phân | PCSAMT |
| 8 | Credit account | credit_account | `Yes` | String | 25 |  |  | PDOACC |
| 9 | Depositor name | depositor_name | No | String | 250 |  |  | CACNM |
| 10 | Depositor id | depositor_id | No | String | 15 |  |  | CCTMCD |
| 11 | Depositor address | depositor_address | No | String | 250 |  |  | CCTMA |
| 12 | Depositor description | depositor_description | No | JSON Object | | | | MDESC |
|  | Home | home | No | String |  |  |  | h |
|  | Office | office | No | String |  |  |  | o |
| 13 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 14 | Currency | currency_code | No | String |  |  | trường ẩn | CCCR |
| 15 | Sum amount | sum_amount | No | `Number` |  | 0 | trường ẩn | AMT |
| 16 | Total amount | total_amount | No | `Number` |  | 0 | trường ẩn | CFAMT |
| 17 | Interest not paid | interest_not_paid | No | `Number` |  | 0 | trường ẩn | CINOTPAID |
| 18 | IFCCD | ifc_code | No | `Number` |  | 0 | trường ẩn | CVAT |
| 19 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | DD account number | dd_account_number | String | | |
| 3 | Deposit balance | deposit_balance | `Number` | | |
| 4 | Interest accrual | interest_accrual | `Number` | | |
| 5 | Interest paid | interest_paid | `Number` | | |
| 6 | Interest receivable | interest_receivable | `Number` | | |
| 7 | Interest due | interest_due | `Number` | | |
| 8 | Credit balance | credit_balance | `Number` | | |
| 9 | Credit account | credit_account | String | | |
| 10 | Depositor name | depositor_name | String | | |
| 11 | Depositor id | depositor_id | String | | |
| 12 | Depositor address | depositor_address | String | | |
| 13 | Depositor description | depositor_description | JSON Object | | |
|  | Home | home | String | | |
|  | Office | office | String | | |
| 14 | Description | description | String | | |
| 15 | Currency | currency_code | String | | |
| 16 | Sum amount | sum_amount | `Number` | | |
| 17 | Total amount | total_amount | `Number` | | |
| 18 | Interest not paid | interest_not_paid | `Number` | | |
| 19 | IFCCD | ifc_code | `Number` | | |
| 20 | Transaction status | status | String |  |  |
| 21 | Transaction date | transaction_date | `Date time` |  |  |
| 22 | User id | user_id | `Number` |  |  |
| 23 | Posting data |  | JSON Object |  |  |
|  | Group |  | `Number` |  |  |
|  | Index in group |  | `Number` |  |  |
|  | Posting side |  | String |  |  |
|  | System account name |  | String |  |  |
|  | GL account number |  | String |  |  |
|  | Amount |  | `Number` |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |
| 24 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
- Deposit account's status must be different from "Closed / Block"
- Credit account's status must be different from "Close"

**Flow of events:**
- When complete transaction: 
    - Credit account status is "Close".
    - Credit outstanding balance = 0.00
    - Credit interest accrual amount = 0.00
    - Update IFC List
- Increase cash at counter.

**Database:**
- Credit account
- Credit schedule (principal, disbursement)
- Credit IFC

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>
- Credit account status <> “Write-off”

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| Deposit balance < 0.00 |  |  |  |  |
|  | Absolute value of [Deposit balance] = Credit balance > 0.00 |  |  |  |
| 1 | D | CASH | Credit balance | Currency of credit account |
| 1 | C | CREDIT {K=0,1,2,3,4} | Credit balance | Currency of credit account |
|  | Absolute value of [Deposit balance] < Credit balance > 0.00 |  |  |  |
| 1 | D | CASH | Absolute value of [Deposit balance] | Currency of credit account |
| 1 | D | DEPOSIT | Credit balance - Absolute value of [Deposit balance] | Currency of credit account |
| 1 | C | CREDIT {K=0,1,2,3,4} | Credit balance | Currency of credit account |
|  | Absolute value of [Deposit balance] > Credit balance > 0.00 |  |  |  |
| 1 | D | CASH | Absolute value of [Deposit balance] | Currency of credit account |
| 1 | C | CREDIT {K=0,1,2,3,4} | Credit balance | Currency of credit account |
| 1 | C | DEPOSIT | Absolute value of [Deposit balance] - Credit balance | Currency of credit account |
|  | Interest receivable > 0.00 |  |  |  |
| 2 | D | CASH | Interest collect | Currency of credit account |
| 2 | C | INTEREST {K=0,1,2,3,4} | Interest collect | Currency of credit account |
| Deposit balance > 0.00 |  |  |  |  |
|  | Credit balance > 0.00 |  |  |  |
| 1 | D | DEPOSIT | Credit balance | Currency of credit account |
| 1 | C | CREDIT {K=0,1,2,3,4} | Credit balance | Currency of credit account |
|  | Deposit balance < Interest receivable > 0.00 |  |  |  |
| 2 | D | CASH | Interest receivable - Deposit balance | Currency of credit account |
| 2 | D | DEPOSIT | Deposit balance | Currency of credit account |
| 2 | C | INTEREST {K=0,1,2,3,4} | Interest receivable | Currency of credit account |
|  | Deposit balance >= Interest receivable > 0.00 |  |  |  |
| 2 | D | DEPOSIT | Interest receivable | Currency of credit account |
| 2 | C | INTEREST {K=0,1,2,3,4} | Interest receivable | Currency of credit account |
| Deposit balance = 0.00 |  |  |  |  |
|  | Credit balance > 0.00 |  |  |  |
| 1 | D | DEPOSIT | Credit balance | Currency of credit account |
| 1 | C | CREDIT {K=0,1,2,3,4} | Credit balance | Currency of credit account |
| 2 | D | PROV_P_DR_RVS {K=0,1,2,3,4} | Reverse provision = Principal collect * Provision rate | Currency of credit account |
| 2 | C | PROV_P_CR_RVS {K=0,1,2,3,4} | Reverse provision = Principal collect * Provision rate | Currency of credit account |
| 3 | D | COMMITMENT_LIMIT | Principal collect * FX rate (Book rate) | Product limit's currency |
| 3 | C | COMMITMENT_REL | Principal collect * FX rate (Book rate) | Product limit's currency |
|  | Interest receivable > 0.00 |  |  |  |
| 4 | D | CASH | Interest receivable | Currency of credit account |
| 4 | C | INTEREST {K=0,1,2,3,4} | Interest receivable | Currency of credit account |
| 5 | D | PAID_INTEREST_S | Interest collect | Currency of credit account |
| 5 | C | PAID_INTEREST {K=2,3,4} | Interest collect | Currency of credit account |

*{K=0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss}*

- Credit account status is “Write-off”

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CASH | Principal collect | Currency of credit account |
| 1 | C | BAD_DEBT_PRINCIPLE | Principal collect | Currency of credit account |
| 2 | D | CASH | Interest collect | Currency of credit account |
| 2 | C | BAD_DEBT_INTEREST_WO | Interest collect | Currency of credit account |
| 3 | D | WOFF_PAID_LOAN | Principal collect | Currency of credit account |
| 3 | C | WRITEOFF_LOAN | Principal collect | Currency of credit account |
| 4 | D | WOFF_PAID_INTEREST | Interest collect | Currency of credit account |
| 4 | C | WRITEOFF_INTEREST | Interest collect | Currency of credit account |

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_BCY_CACNM | GET_INFO_DPTACC | [GET_INFO_DPTACC](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_CREDITAC | DPT_GET_INFO_CREDITAC | [DPT_GET_INFO_CREDITAC](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_CUSTOMER | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 5 | GET_SIG_PDOACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>