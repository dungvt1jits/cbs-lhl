# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_ODO`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | PDACC |
| 2 | Sub product limit code | sub_product_limit_code | `Yes` | String | 25 |  |  | SPL |
| 3 | Product code | product_code | `Yes` | String | 25 |  |  | CNCAT |
| 4 | Credit classification | credit_classification | `Yes` | String | 2 |  |  | CNSTSO |
| 5 | Sub-product | sub_product | `Yes` | String | 2 |  |  | SUBPRODUCT |
| 6 | Customer type | customer_type | `Yes` | String | 1 |  |  | CCTMT |
| 7 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 8 | Catalogue code | catalog_code | `Yes` | String | 25 |  |  | CNCAT1 |
| 9 | Minimum balance | minimum_balance | No | `Number` |  | 0 | số có hai số thập phân | PGAMT |
| 10 | Account credit | account_credit | No | String | 25 |  |  | PCACC |
| 11 | OD contract number | od_contract_number | No | String | 25 |  |  | PDOACC |
| 12 | Account holder name | account_holder_name | `Yes` | String | 250 |  |  | CACNM |
| 13 | OD from minimum balance | od_from_minimum_balance | `Yes` | String | 1 |  |  | CNSTS1 |
| 14 | OD limit | od_limit | `Yes` | `Number` |  | > 0 | số có hai số thập phân | PCSAMT |
| 15 | Margin | margin | `Yes` | `Number` |  | 0 | số có hai số thập phân, có thể < 0.00, = 0.00 và > 0.00 | MARGIN |
| 16 | Effective date | effective_date | `Yes` | `Date time` |  |  | Working date | CDT1 |
| 17 | Expire date | expire_date | `Yes` | `Date time` |  |  | Working date | CDT2 |
| 18 | First int. repayment date | first_int_repayment_date | `Yes` | `Date time` |  |  | Working date | DUEDT |
| 19 | Description | description | No | String | 250 |  |  | DESCS |
| 20 | Currency of deposit account | currency_of_deposit_account | No | String | 3 |  | trường ẩn | CCCR |
| 21 | Currency of product code | currency_of_product_code | No | String | 3 |  | trường ẩn | CCCR1 |
| 22 | Deposit type | deposit_type | No | String | 1 |  | trường ẩn | CNSTS |
| 23 | Branch code | branch_code | `Yes` | String | 4 | branch_code của branch đang làm giao dịch | trường ẩn | CBRCD |
| 24 | Product tenor type | product_tenor_type | `Yes` | String | 1 |  | trường ẩn | CTYPE |
| 25 | Branch name | branch_name | No | String | | | `gửi thêm` | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account number | account_number | String |  |  |
| 2 | Sub product limit code | sub_product_limit_code | String |  |  |
| 3 | Product code | product_code | String | 25 |  |  | CNCAT |
| 4 | Credit classification | credit_classification | String |  |  |
| 5 | Sub-product | sub_product | String |  |  |  |
| 6 | Customer type | customer_type | String |  |  |
| 7 | Customer code | customer_code | String |  |  |
| 8 | Catalogue code | catalog_code | String |  |  |
| 9 | Minimum balance | minimum_balance | `Number` |  |  |
| 10 | Account credit | account_credit | String |  |  |
| 11 | OD contract number | od_contract_number | String |  |  |
| 12 | Account holder name | account_holder_name | String |  |  |
| 13 | OD from minimum balance | od_from_minimum_balance | String |  |  |
| 14 | OD limit | od_limit | `Number` |  |  |
| 15 | Margin | margin | `Number` |  |  |
| 16 | Effective date | effective_date | `Date time` |  |  |
| 17 | Expire date | expire_date | `Date time` |  |  |
| 18 | First int. repayment date | first_int_repayment_date | `Date time` |  |  |
| 19 | Description | description | String |  |  |
| 20 | Currency of deposit account | currency_of_deposit_account | String |  |  |
| 21 | Currency of product code | currency_of_product_code | String |  |  |
| 22 | Deposit type | deposit_type | String |  |  |
| 23 | Branch code | branch_code | String |  |  |
| 24 | Product tenor type | product_tenor_type | String |  |  |
| 25 | Transaction references | reference_id | String |  |  |
| 26 | Transaction date |  | `Date time` |  |  |
| 27 | User id |  | `Number` |  |  |
| 28 | Transaction status |  | String |  |  |
| 29 | Branch name | branch_name | String | | `trả thêm` |

## 1.2 Transaction flow
**Conditions:**
- Exists deposit account number: 
    - Status is {New, Normal, Pending, Dormant}
    - Deposit type is "Current"
- Catalogue code (Credit) is defined:
    - Credit account type is "Blanket"
    - Facility: Overdraft
    - Don't allow different currency with deposit account.
    - Don't allow different branch with deposit account.
- Exists customer code.
- Have period of contract.
- Exists sub product limit.

**Flow of events:**
- OD account code will auto create.
- OD limit smaller or equal sub product limit.
- System auto converts credit line of sub product to suit with loan's currency. It is easy to compare limit. Use exchange rate is book rate.
- Prevent entering contract date is in past.
- Content of contract will copy from definition "Catalogue code".
- Allow to enter plus/minus "Margin rate".
- Allow deleting account if it has just opened.
- Allow to modify information contract after opening contract.
- When complete transaction: 
    - Status is "Pending to approve".
    - Auto creates linkage between deposit account and credit account.
        - Master module name: Deposit
        - Master account code: Deposit account number
        - Linkage module name: Credit
        - Linkage account code: Credit account number
        - Linkage type: Linkage Credit - Deposit
        - Linkage class: Over draft linkage

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_INFORMATION | DPT_GET_INFO_CUSTOMER | [DPT_GET_INFO_CUSTOMER](Specification/Common/15 Deposit Rulefuncs) |
| 3 | GET_INFO_CCATNM | GET_INFO_DPTCAT | [GET_INFO_DPTCAT](Specification/Common/15 Deposit Rulefuncs) |
| 4 | GET_INFO_SPL | DPT_ODO_GET_INFO_SPL | [DPT_ODO_GET_INFO_SPL](Specification/Common/13 Credit Rulefuncs), truyền giá trị của `product_code` ở message request vào `catalog_code` của rule func |
| 5 | GET_INFO_CNCAT1 | GET_INFO_CRDCAT | [GET_INFO_CRDCAT](Specification/Common/13 Credit Rulefuncs) |
| 6 | LKP_DATA_CNCAT1 | CRD_LOOKUP_CRDCAT_OD | [CRD_LOOKUP_CRDCAT_OD](Specification/Common/13 Credit Rulefuncs) |
| 7 | LKP_DATA_SPLCD | CRD_LOOKUP_CRDSPL_OD_BY_CTM | [CRD_LOOKUP_CRDSPL_OD_BY_CTM](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>