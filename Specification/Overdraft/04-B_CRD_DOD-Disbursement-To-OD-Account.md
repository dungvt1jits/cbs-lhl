# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_DOD`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Deposit account number | deposit_account | `Yes` | String | 25 |  |  | PDOACC |
| 3 | Total disbursement amount | total_disbursement_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | TXAMT |
| 4 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 5 | Remaining provision amount | remaining_provision_amount | No | `Number` |  | 0 |  | PPAMT |
| 6 | Total principal amount | total_principal_amount | No | `Number` |  | 0 |  | TT_P |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Credit account number | credit_account | String |  |  |
| 3 | Deposit account number | deposit_account | String |  |  |
| 4 | Total disbursement amount | total_disbursement_amount | `Number` |  | số có hai số thập phân |
| 5 | Description | description | String |  |  |
| 6 | Remaining provision amount | remaining_provision_amount | `Number` |  |  |
| 7 | Total principal amount | total_principal_amount | `Number` |  |  |
| 8 | Transaction status | status | String |  |  |
| 9 | Transaction date | transaction_date | `Date time` |  |  |
| 10 | User id | user_id | `Number` |  |  |
|  | Step code: `ACT_EXECUTE_POSTING` |  |  |  |  |
| 11 | Posting data | entry_journals | JSON Object |  |  |
|  | Group | accounting_entry_group | `Number` |  | số nguyên dương |
|  | Index in group | accounting_entry_index | `Number` |  | số nguyên dương |
|  | Posting side | debit_or_credit | String |  | D or C |
|  | Account number | bank_account_number | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Currency | currency_code | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Deposit account: exists with status "Normal".
- Credit account: exists with status "Normal".
- Absolute value of [Deposit balance] > Credit outstanding.
- {Total amount was disbursed of account} + {amount is going disburse} < or = credit limit.
- {Total amount was disbursed of all accounts belong sub product} + {amount is going disburse} < or = sub product limit.
- Date disbursement is not smaller than begin contract date or not larger than ending contract date.

**Flow of events:**
- Disburse amount = Absolute value of [Deposit balance] - Credit outstanding
- Credit outstanding will increase.
- Record amount disbursed.
- Record credit limit was used.
- Provision principal based on amount disbursement.
- Record product limit was used based on product limit's currency. (Exchange rate is book rate).

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

- Credit account status <> "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | CREDIT {K=0,1,2,3,4} | Disburse amount | Currency of credit account |
| 1 | C | DEPOSIT | Disburse amount | Currency of credit account |
| 2 | D | COMMITMENT_REL | Disburse amount * FX rate (Book rate) | Product limit's currency |
| 2 | C | COMMITMENT_LIMIT | Disburse amount * FX rate (Book rate) | Product limit's currency |
| 3 | D | PROV_P_DR {K=0,1,2,3,4} | Disburse amount * Provision rate | Currency of credit account |
| 3 | C | PROV_P_CR {K=0,1,2,3,4} | Disburse amount * Provision rate | Currency of credit account |

- Note: {K=0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss}

- Credit account status is "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | BAD_DEBT_LOSS | Disburse amount | Currency of credit account |
| 1 | C | DEPOSIT | Disburse amount | Currency of credit account |
| 2 | D | WRITEOFF_LOAN | Disburse amount | Currency of credit account |
| 2 | C | WOFF_PAID_LOAN | Disburse amount | Currency of credit account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |