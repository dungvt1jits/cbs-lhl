# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Debit account number |  | `Yes` | String | 25 |  |  |
| 2 | Credit account number |  | `Yes` | String | 25 |  |  |
| 3 | Amount |  | `Yes` | `Number` |  | 0 | số có hai số thập phân |
| 4 | Credit off-bal account |  | `Yes` | String | 25 |  |  |
| 5 | Debit off-bal account |  | `Yes` | String | 25 |  |  |
| 6 | NPL index |  | `Yes` | `Number` |  |  | số nguyên dương |
| 7 | Catalogue id |  | `Yes` | `Number` |  |  | số nguyên dương |
| 8 | Description |  | `Yes` | String | 250 |  |  |
| 9 | Thread id |  | `Yes` | `Number` |  |  | số nguyên dương |
| 10 | Transaction date |  | `Yes` | `Date` |  | Working date |  |
| 11 | User id |  | `Yes` | String |  | User login |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Debit account number |  | String |  |  |  |
| 3 | Credit account number |  | String |  |  |  |
| 4 | Amount |  | `Number` |  |  | số có hai số thập phân |
| 5 | Credit off-bal account |  | String |  |  |  |
| 6 | Debit off-bal account |  | String |  |  |  |
| 7 | NPL index |  | `Number` |  |  | số nguyên dương |
| 8 | Catalogue id |  | `Number` |  |  | số nguyên dương |
| 9 | Description |  | String |  |  |  |
| 10 | Thread id |  | `Number` |  |  | số nguyên dương |
| 11 | Transaction date |  | `Date` |  |  |  |
| 12 | User id |  | String |  |  |  |
| 13 | Transaction status |  | String |  |  |  |
| 14 | Posting data |  | JSON Object |  |  |  |
|  | Group |  | Number |  |  |  |
|  | Index in group |  | Number |  |  |  |
|  | Posting side |  | String |  |  |  |
|  | System account name |  | String |  |  |  |
|  | GL account number |  | String |  |  |  |
|  | Amount |  | Number |  |  | số có hai số thập phân |
|  | Currency |  | String | 3 |  |  |

## 1.2 Transaction flow
**Conditions:**
- Credit accounts have outstanding > 0.00
- Interest rate > 0.00
- Apply calculation for all classification status.

**Flow of events:**
- [Interest daily amount] = Outstanding * interest rate of day.
- Calculate interest on end of day.
- Support calculation interest normal.
- Allow definition tenor of interest rate: year, month, week, days. 
    - Example: 
        - Interest rate = 4% / year (Interest rate of day = 4% / 360)
        - Interest rate = 2% / month (Interest rate of day = 2% / 30)
        - Interest rate = 1.5% / week (Interest rate of day = 1.5% / 7)
- Allow definition number decimal of Interest accrual.
- Flexibility in setup interest rate: Float rate + margin rate.
- Interest amount divide 2 type: "On balance sheet interest" and "Off balance sheet interest". So, Interest accrual will add into:
    - "On balance sheet interest" when account is normal, special mention.
    - "Off balance sheet interest" when account is substandard, doubtful, loss.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

- Case classification of account is {Normal: 0, Special mention: 1}

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST {K=0,1} | Interest daily amount | Currency of credit account |
| 1 | C | PAID_INTEREST {K=0,1} | Interest daily amount | Currency of credit account |

- Case classification of account is {Substandard: 2, Doubtful: 3, Loss: 4}

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | INTEREST {K=2,3,4} | Interest daily amount | Currency of credit account |
| 1 | C | PAID_INTEREST_S | Interest daily amount | Currency of credit account |

- Case status account is “Write-off”

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | WRITEOFF_INTEREST | Interest daily amount | Currency of credit account |
| 1 | C | WOFF_PAID_INTEREST | Interest daily amount | Currency of credit account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |