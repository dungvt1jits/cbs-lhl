# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction date |  | `Yes` | `Date` |  | Working date |  |
| 2 | User id |  | `Yes` | String |  | User login |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Transaction date |  | `Date` |  |  |  |
| 3 | User id |  | String |  |  |  |
| 4 | Transaction status |  | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Working date is end of month, is due date of Interest.
- Credit facility is "Overdraft".
- Credit account: exists with status "Normal".

**Flow of events:**
- Transfer interest accrual to interest due:
    - Insert one row interest due on schedule interest.
- Transaction handles auto by system:
    - Interest due increase.

**Database:**
- Table name: **d_credit**
- Method: `Update`

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |
| 1 | Disbursement amount | + Disbursement amount |  |
| 2 | Outstanding balance | + Disbursement amount |  |

- Table name: **d_crschd**
- Method: `Add`

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |
| 1 | Interest payment data |  |  |
|  | No |  |  |
|  | Due date |  |  |
|  | Amount |  |  |
|  | Paid amount |  |  |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |