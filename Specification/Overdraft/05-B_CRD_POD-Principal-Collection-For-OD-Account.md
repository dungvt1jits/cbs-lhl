# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `CRD_POD`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Credit account number | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 2 | Deposit account number | deposit_account | `Yes` | String | 25 |  |  | PDOACC |
| 3 | Total principal amount | collection_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | TXAMT |
| 4 | Description |  | `Yes` | String | 250 |  |  | DESCS |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Credit account number |  | String |  |  |  |
| 3 | Deposit account number |  | String |  |  |  |
| 4 | Total principal amount |  | `Number` |  |  | số có hai số thập phân |
| 5 | Description |  | String |  |  |  |
| 8 | Transaction status | status | String |  |  |
| 9 | Transaction date | transaction_date | `Date time` |  |  |
| 10 | User id | user_id | `Number` |  |  |
|  | Step code: `ACT_EXECUTE_POSTING` |  |  |  |  |
| 11 | Posting data | entry_journals | JSON Object |  |  |
|  | Group | accounting_entry_group | `Number` |  | số nguyên dương |
|  | Index in group | accounting_entry_index | `Number` |  | số nguyên dương |
|  | Posting side | debit_or_credit | String |  | D or C |
|  | Account number | bank_account_number | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Currency | currency_code | String |  |  |

## 1.2 Transaction flow
**Conditions:**
- Deposit account: exists with status "Normal".
- Credit account: exists with status "Normal".
- Credit outstanding > 0.00
    - Absolute value of [Deposit balance] < Credit outstanding
    - OR Deposit balance > Credit outstanding

**Flow of events:**
- Principal collect will be
    - If Deposit balance < 0.00
        - Principal collect = Credit outstanding - Absolute value of [Deposit balance]
    - If Deposit balance >= 0.00
        - Principal collect = Credit outstanding
- Reverse provision-based collection principal amount.
- Credit outstanding will decrease.
- Record credit limit wasn’t used.
- Record product limit wasn’t used based on product limit's currency. (Exchange rate is book rate).

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

**Posting:**
- Cập nhật thông tin vào tài khoản GL: [refer transaction flow: GL](Specification/Common/08 Transaction Flow GL)<br>

- Credit account status <> "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Principal collect | Currency of credit account |
| 1 | C | CREDIT {K=0,1,2,3,4} | Principal collect | Currency of credit account |
| 2 | D | PROV_P_DR_RVS {K=0,1,2,3,4} | Reverse provision = Principal collect * Provision rate | Currency of credit account |
| 2 | C | PROV_P_CR_RVS {K=0,1,2,3,4} | Reverse provision = Principal collect * Provision rate | Currency of credit account |
| 3 | D | COMMITMENT_LIMIT | Principal collect * FX rate (Book rate) | Product limit's currency |
| 3 | C | COMMITMENT_REL | Principal collect * FX rate (Book rate) | Product limit's currency |

{K=0: Normal, 1: Special mention, 2: Substandard, 3: Doubtful, 4: Loss}

- Credit account status is "Write-off"

| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT | Principal collect | Currency of credit account |
| 1 | C | BAD_DEBT_PRINCIPLE | Principal collect | Currency of credit account |
| 2 | D | WOFF_PAID_LOAN | Principal collect | Currency of credit account |
| 2 | C | WRITEOFF_LOAN | Principal collect | Currency of credit account |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |