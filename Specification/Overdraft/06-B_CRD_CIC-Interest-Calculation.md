# 1. Accept
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number |  | `Yes` | String | 25 |  |  |
| 2 | Interest mode |  | `Yes` | String | 25 |  |  |
| 3 | Balance for interest calculation |  | `Yes` | `Number` |  | 0 | số có hai số thập phân |
| 4 | Interest rate |  | `Yes` | `Number` |  | 0 | số có hai số thập phân |
| 5 | Currency rounding rule |  | `Yes` | `Number` |  | 0 | số có hai số thập phân |
| 6 | Transaction date |  | `Yes` | `Date` |  | Working date |  |
| 7 | User id |  | `Yes` | String |  | User login |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Account number |  | String |  |  |  |
| 3 | Interest mode |  | String |  |  |  |
| 4 | Balance for interest calculation |  | `Number` |  |  | số có hai số thập phân |
| 5 | Interest rate |  | `Number` |  |  | số có hai số thập phân |
| 6 | Currency rounding rule |  | `Number` |  |  | số có hai số thập phân |
| 7 | Transaction date |  | `Date` |  |  |  |
| 8 | User id |  | String |  |  |  |
| 9 | Transaction status |  | String |  |  |  |

## 1.2 Transaction flow
**Conditions:**
- Credit accounts have outstanding > 0.00
- Interest rate > 0.00
- Apply calculation for all classification status.

**Flow of events:**
- [Interest daily amount] = Outstanding * interest rate of day.
- Calculate interest on end of day.
- Support calculation interest normal.
- Allow definition tenor of interest rate: year, month, week, days. 
    - Example: 
        - Interest rate = 4% / year (Interest rate of day = 4% / 360)
        - Interest rate = 2% / month (Interest rate of day = 2% / 30)
        - Interest rate = 1.5% / week (Interest rate of day = 1.5% / 7)
- Allow definition number decimal of Interest accrual.
- Flexibility in setup interest rate: Float rate + margin rate.
- Interest amount divide 2 type: "On balance sheet interest" and "Off balance sheet interest". So, Interest accrual will add into:
    - "On balance sheet interest" when account is normal, special mention.
    - "Off balance sheet interest" when account is substandard, doubtful, loss.

**Database:**
- Table name: 

| No | Field name | Value | Note |
| -- | ---------- | ----- | ---- |

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |