# 1. Refresh
## 1.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | From date |  | `Yes` | Date |  |  |  |
| 2 | To date |  | `Yes` | Date |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
|  | Data |  | JSON Object |  |  |  |
| 1 | File id |  | String |  |  |  |
| 2 | Date |  | `Date` |  |  |  |
| 3 | User id |  | String |  |  |  |
| 4 | File name |  | String |  |  |  |
| 5 | Status |  | String |  |  |  |
| 6 | Processed |  | String |  |  |  |
| 7 | Size (B) |  | String |  |  |  |
| 8 | Lines |  | String |  |  |  |
| 9 | Messages |  | String |  |  |  |
| 10 | File type |  | String |  |  |  |
|  | Paging |  | JSON Object |  |  |  |
| 1 | Page index |  | Number |  |  |  |
| 2 | Total page |  | Number |  |  |  |
| 3 | Next page |  | Boolean |  |  |  |
| 4 | Previous page |  | Boolean |  |  |  |

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Save import
## 2.1 Field description
### Request message
URL:

HTTP Method:

Header:

Body:
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |  |

### Response message
Header:

Body:
| No | Field name | Parameter | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---- | ------ | ------------- | ----------- |
| 1 |  |  |  |  |  |  |

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |