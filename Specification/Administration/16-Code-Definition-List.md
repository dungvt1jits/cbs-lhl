# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** ​`/api​/CodeList​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_CODE_LIST`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  |  |  |
| 3 | Page index | page_index | No | `Number` |  |  |  |

**Example:** `/api/CodeList/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption | caption | String |  |  |
| 5 | Code of group | code_group | String |  |  |
| 6 | Code index | code_index | `Number` |  | số nguyên |
| 7 | Field link | ftag | String |  |  |
| 8 | Is visible | visible | `Number` |  | 1: true, 0: false |

**Example:**
```json
{

}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation | 
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/CodeList​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_CODE_LIST`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | No | String |  |  |  |
| 2 | Code name | code_name | No | String |  |  |  |
| 3 | Caption | caption | No | String |  |  |  |
| 4 | Code group | code_group | No | String |  |  |  |
| 5 | Code index | code_index | No | `Number` |  |  | số nguyên |
| 6 | Code index from | code_index_from | No | `Number` |  |  | số nguyên |
| 7 | Code index to | code_index_to | No | `Number` |  |  | số nguyên |
| 8 | Field link | ftag | No | String |  |  |  |
| 9 | Page index | page_index | No | `Number` |  |  |  |
| 10 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{

}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption | caption | String |  |  |
| 5 | Code of group | code_group | String |  |  |
| 6 | Code index | code_index | `Number` |  | số nguyên |
| 7 | Field link | ftag | String |  |  |
| 8 | Is visible | visible | `Number` |  | 1: true, 0: false |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/CodeList​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_CODE_LIST`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | `Yes` | String |  |  |  |
| 2 | Code name | code_name | `Yes` | String |  |  |  |
| 3 | Caption of code | caption | `Yes` | String |  |  |  |
| 4 | Multi caption of code | mcaption | No | JSON Object |  |  |  |
|  | English | english | No | String |  |  |  |
|  | Vietnamese | vietnamese | No | String |  |  |  |
|  | Laothian | laothian | No | String |  |  |  |
|  | Khmer | khmer | No | String |  |  |  |
|  | Myanmar | myanmar | No | String |  |  |  |
| 5 | Code of group | code_group | `Yes` | String |  |  |  |
| 6 | Code index | code_index | `Yes` | `Number` |  |  | số nguyên |
| 7 | Code value | code_value | No | String |  |  |  |
| 8 | Field link | ftag | No | String |  |  |  |
| 9 | Is visible | visible | No | `Number` |  |  | 1: true, 0: false |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption of code | caption | String |  |  |
| 5 | Multi caption of code | mcaption | JSON Object |  |  |
|  | English | english | String |  |  |
|  | Vietnamese | vietnamese | String |  |  |
|  | Laothian | laothian | String |  |  |
|  | Khmer | khmer | String |  |  |
|  | Myanmar | myanmar | String |  |  |
| 6 | Code of group | code_group | String |  |  |
| 7 | Code index | code_index | `Number` |  | số nguyên |
| 8 | Code value | code_value | String |  |  |
| 9 | Field link | ftag | String |  |  |
| 10 | Is visible | visible | `Number` |  | 1: true, 0: false |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View by id
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api​/CodeList​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_CODE_LIST`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code number | id | `Yes` | `Number` |  |  |  |

**Example:** `/api/CodeList/View/2937`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption of code | caption | String |  |  |
| 5 | Multi caption of code | mcaption | JSON Object |  |  |
|  | English | english | String |  |  |
|  | Vietnamese | vietnamese | String |  |  |
|  | Laothian | laothian | String |  |  |
|  | Khmer | khmer | String |  |  |
|  | Myanmar | myanmar | String |  |  |
| 6 | Code of group | code_group | String |  |  |
| 7 | Code index | code_index | `Number` |  | số nguyên |
| 8 | Code value | code_value | String |  |  |
| 9 | Field link | ftag | String |  |  |
| 10 | Is visible | visible | `Number` |  | 1: true, 0: false |

**Example:**
```json
{

}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify by id
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `/api​/CodeList​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_CODE_LIST`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code number | id | `Yes` | `Number` |  |  |  |
| 2 | Code id | code_id | `Yes` | String |  |  |  |
| 3 | Caption of code | caption | `Yes` | String |  |  |  |
| 4 | Multi caption of code | mcaption | No | JSON Object |  |  |  |
|  | English | english | No | String |  |  |  |
|  | Vietnamese | vietnamese | No | String |  |  |  |
|  | Laothian | laothian | No | String |  |  |  |
|  | Khmer | khmer | No | String |  |  |  |
|  | Myanmar | myanmar | No | String |  |  |  |
| 5 | Code index | code_index | `Yes` | `Number` |  |  | số nguyên |
| 6 | Code value | code_value | No | String |  |  |  |
| 7 | Field link | ftag | No | String |  |  |  |
| 8 | Is visible | visible | No | `Number` |  |  | 1: true, 0: false |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption of code | caption | String |  |  |
| 5 | Multi caption of code | mcaption | JSON Object |  |  |
|  | English | english | String |  |  |
|  | Vietnamese | vietnamese | String |  |  |
|  | Laothian | laothian | String |  |  |
|  | Khmer | khmer | String |  |  |
|  | Myanmar | myanmar | String |  |  |
| 6 | Code of group | code_group | String |  |  |
| 7 | Code index | code_index | `Number` |  | số nguyên |
| 8 | Code value | code_value | String |  |  |
| 9 | Field link | ftag | String |  |  |
| 10 | Is visible | visible | `Number` |  | 1: true, 0: false |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete by id
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** ​`/api​/CodeList​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_CODE_LIST`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code number | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 2937
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Code number | id | `Number` |  |  |

**Example:**
```json
{
    "id": 2937
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. View by "code_id and code_name and code_group"
## 7.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_CODE_LIST_BY_PRIMARY_KEY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | `Yes` | String |  |  |  |
| 2 | Code name | code_name | `Yes` | String |  |  |  |
| 3 | Code of group | code_group | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption of code | caption | String |  |  |
| 5 | Multi caption of code | mcaption | JSON Object |  |  |
|  | English | english | String |  |  |
|  | Vietnamese | vietnamese | String |  |  |
|  | Laothian | laothian | String |  |  |
|  | Khmer | khmer | String |  |  |
|  | Myanmar | myanmar | String |  |  |
| 6 | Code of group | code_group | String |  |  |
| 7 | Code index | code_index | `Number` |  | số nguyên |
| 8 | Code value | code_value | String |  |  |
| 9 | Field link | ftag | String |  |  |
| 10 | Is visible | visible | `Number` |  | 1: true, 0: false |

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 8. Delete by "code_id and code_name and code_group"
## 8.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ​``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_CODE_LIST_BY_PRIMARY_KEY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | `Yes` | String |  |  |  |
| 2 | Code name | code_name | `Yes` | String |  |  |  |
| 3 | Code of group | code_group | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Code id | code_id | String |  |  |
| 2 | Code name | code_name | String |  |  |
| 3 | Code of group | code_group | String |  |  |

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 9. Modify by "code_id and code_name and code_group"
## 9.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `/api​/CodeList​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_CODE_LIST_BY_PRIMARY_KEY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Code id | code_id | `Yes` | String |  |  | Không được sửa |
| 2 | Code name | code_name | `Yes` | String |  |  | Không được sửa |
| 3 | Caption of code | caption | `Yes` | String |  |  |  |
| 4 | Multi caption of code | mcaption | No | JSON Object |  |  |  |
|  | English | english | No | String |  |  |  |
|  | Vietnamese | vietnamese | No | String |  |  |  |
|  | Laothian | laothian | No | String |  |  |  |
|  | Khmer | khmer | No | String |  |  |  |
|  | Myanmar | myanmar | No | String |  |  |  |
| 5 | Code of group | code_group | `Yes` | String |  |  | Không được sửa |
| 6 | Code index | code_index | `Yes` | `Number` |  |  | số nguyên |
| 7 | Code value | code_value | No | String |  |  |  |
| 8 | Field link | ftag | No | String |  |  |  |
| 9 | Is visible | visible | No | `Number` |  |  | 1: true, 0: false |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Code number | id | `Number` |  |  |
| 2 | Code id | code_id | String |  |  |
| 3 | Code name | code_name | String |  |  |
| 4 | Caption of code | caption | String |  |  |
| 5 | Multi caption of code | mcaption | JSON Object |  |  |
|  | English | english | String |  |  |
|  | Vietnamese | vietnamese | String |  |  |
|  | Laothian | laothian | String |  |  |
|  | Khmer | khmer | String |  |  |
|  | Myanmar | myanmar | String |  |  |
| 6 | Code of group | code_group | String |  |  |
| 7 | Code index | code_index | `Number` |  | số nguyên |
| 8 | Code value | code_value | String |  |  |
| 9 | Field link | ftag | String |  |  |
| 10 | Is visible | visible | `Number` |  | 1: true, 0: false |

## 9.2 Transaction flow

## 9.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 10. Sync code list
## 10.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SYNC_CODE_LIST`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  |  |  |
| 2 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Code id | code_id | String |  |  |
| 2 | Code name | code_name | String |  |  |
| 3 | Caption of code | caption | String |  |  |
| 4 | Multi caption of code | mcaption | JSON Object |  |  |
|  | English | english | String |  |  |
|  | Vietnamese | vietnamese | String |  |  |
|  | Laothian | laothian | String |  |  |
|  | Khmer | khmer | String |  |  |
|  | Myanmar | myanmar | String |  |  |
| 5 | Code of group | code_group | String |  |  |
| 6 | Code index | code_index | `Number` |  | số nguyên |
| 7 | Code value | code_value | String |  |  |
| 8 | Field link | ftag | String |  |  |
| 9 | Is visible | visible | `Number` |  | 1: true, 0: false |

**Example:**
```json
{
    
}
```

## 10.2 Transaction flow

## 10.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |