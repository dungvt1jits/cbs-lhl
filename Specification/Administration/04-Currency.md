# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api/Currency/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_CURRENCY`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `/api/Currency/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Currency code | currency_id | String | 3 |  |
| 2 | Short currency code | short_currency_id | String | 2 |  |
| 3 | Currency number | currency_number | `Number` |  |  |
| 4 | Status | status_of_currency | String |  |  |
| 5 | Order | order | `Number` |  |  |
| 6 | Currency id | id | `Number` |  |  |

**Example:**
```json
{

}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/Currency/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_CURRENCY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency code | currency_id | No | String | 3 |  |  |
| 2 | Short currency code | short_currency_id | No | String | 2 |  |  |
| 3 | Currency number | currency_number | No | `Number` |  |  | số nguyên dương |
| 4 | Currency number from | currency_number_from | No | `Number` |  |  | số nguyên dương |
| 5 | Currency number to | currency_number_to | No | `Number` |  |  | số nguyên dương |
| 6 | Status | status_of_currency | No | String |  |  |  |
| 7 | Order | order | No | `Number` |  |  | số nguyên dương |
| 8 | Order from | order_from | No | `Number` | 3 |  |  |
| 9 | Order to | order_to | No | `Number` |  |  |  |
| 10 | Page index | page_index | No | `Number` |  |  |  |
| 11 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Currency code | currency_id | String | 3 |  |
| 2 | Short currency code | short_currency_id | String | 2 |  |
| 3 | Currency number | currency_number | `Number` |  |  |
| 4 | Status | status_of_currency | String |  |  |
| 5 | Order | order | `Number` |  |  |
| 6 | Currency id | id | `Number` |  |  |

**Example:**
```json
{

}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/Currency/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_CURRENCY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency code | currency_id | `Yes` | String | 3 |  | is unique |
| 2 | Short currency code | short_currency_id | `Yes` | String | 2 |  | is unique |
| 3 | Currency number | currency_number | No | `Number` |  |  |  
| 4 | Currency name | currency_name | No | JSON Object |  |  |  |
|  | Currency name 1 | currency_name1 | No | String |  |  |  |
|  | Currency name 2 | currency_name2 | No | String |  |  |  |
|  | Currency name 3 | currency_name3 | No | String |  |  |  |
| 5 | Master name | master_name | No | JSON Object |  |  |  |
|  | Master name 1 | master_name1 | No | String |  |  |  |
|  | Master name 2 | master_name2 | No | String |  |  |  |
|  | Master name 3 | master_name3 | No | String |  |  |  |
| 6 | Decimal name | decimal_name | No | JSON Object |  |  |  |
|  | Decimal name 1 | decimal_name1 | No | String |  |  |  |
|  | Decimal name 2 | decimal_name2 | No | String |  |  |  |
|  | Decimal name 3 | decimal_name3 | No | String |  |  |  |
| 7 | Decimal digits | decimal_digits | `Yes` | `Number` |  |  |  |
| 8 | Rounding digits | rounding_digits | `Yes` | `Number` |  |  |  |
| 9 | Status of currency | status_of_currency | `Yes` | String |  |  |  |
| 10 | Order | order | No | `Number` |  |  |  |

**Example:**
```json
{

}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency code | currency_id | String | 3 |  |
| 2 | Short currency code | short_currency_id | String | 2 |  |
| 3 | Currency number | currency_number | `Number` |  |  |
| 4 | Currency name | currency_name | JSON Object |  |  |
|  | Currency name 1 | currency_name1 | String |  |  |
|  | Currency name 2 | currency_name2 | String |  |  |
|  | Currency name 3 | currency_name3 | String |  |  |
| 5 | Master name | master_name | JSON Object |  |  |
|  | Master name 1 | master_name1 | String |  |  |
|  | Master name 2 | master_name2 | String |  |  |
|  | Master name 3 | master_name3 | String |  |  |
| 6 | Decimal name | decimal_name | JSON Object |  |  |
|  | Decimal name 1 | decimal_name1 | String |  |  |
|  | Decimal name 2 | decimal_name2 | String |  |  |
|  | Decimal name 3 | decimal_name3 | String |  |  |
| 6 | Decimal name | decimal_name | String |  |  |
| 7 | Decimal digits | decimal_digits | `Number` |  |  |
| 8 | Rounding digits | rounding_digits | `Number` |  |  |
| 9 | Status of currency | status_of_currency | String |  |  |
| 10 | Order | order | `Number` |  |  |
| 11 | Currency id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api/Currency/View/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_CURRENCY`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency code | currency_id | String | 3 |  |
| 2 | Short currency code | short_currency_id | String | 2 |  |
| 3 | Currency number | currency_number | `Number` |  |  |
| 4 | Currency name | currency_name | JSON Object |  |  |
|  | Currency name 1 | currency_name1 | String |  |  |
|  | Currency name 2 | currency_name2 | String |  |  |
|  | Currency name 3 | currency_name3 | String |  |  |
| 5 | Master name | master_name | JSON Object |  |  |
|  | Master name 1 | master_name1 | String |  |  |
|  | Master name 2 | master_name2 | String |  |  |
|  | Master name 3 | master_name3 | String |  |  |
| 6 | Decimal name | decimal_name | JSON Object |  |  |
|  | Decimal name 1 | decimal_name1 | String |  |  |
|  | Decimal name 2 | decimal_name2 | String |  |  |
|  | Decimal name 3 | decimal_name3 | String |  |  |
| 7 | Decimal digits | decimal_digits | `Number` |  |  |
| 8 | Rounding digits | rounding_digits | `Number` |  |  |
| 9 | Status of currency | status_of_currency | String |  |  |
| 10 | Order | order | `Number` |  |  |
| 11 | Currency id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `/api/Currency/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_CURRENCY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency id | id | `Yes` | `Number` |  |  | is existed |
| 2 | Short currency code | short_currency_id | `Yes` | String | 2 |  |  |
| 3 | Currency number | currency_number | No | `Number` |  |  |  
| 4 | Currency name | currency_name | No | JSON Object |  |  |  |
|  | Currency name 1 | currency_name1 | No | String |  |  |  |
|  | Currency name 2 | currency_name2 | No | String |  |  |  |
|  | Currency name 3 | currency_name3 | No | String |  |  |  |
| 5 | Master name | master_name | No | JSON Object |  |  |  |
|  | Master name 1 | master_name1 | No | String |  |  |  |
|  | Master name 2 | master_name2 | No | String |  |  |  |
|  | Master name 3 | master_name3 | No | String |  |  |  |
| 6 | Decimal name | decimal_name | No | JSON Object |  |  |  |
|  | Decimal name 1 | decimal_name1 | No | String |  |  |  |
|  | Decimal name 2 | decimal_name2 | No | String |  |  |  |
|  | Decimal name 3 | decimal_name3 | No | String |  |  |  |
| 7 | Decimal digits | decimal_digits | `Yes` | `Number` |  |  |  |
| 8 | Rounding digits | rounding_digits | `Yes` | `Number` |  |  |  |
| 9 | Status of currency | status_of_currency | `Yes` | String |  |  |  |
| 10 | Order | order | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency code | currency_id | String | 3 |  |
| 2 | Short currency code | short_currency_id | String | 2 |  |
| 3 | Currency number | currency_number | `Number` |  |  |
| 4 | Currency name | currency_name | JSON Object |  |  |
|  | Currency name 1 | currency_name1 | String |  |  |
|  | Currency name 2 | currency_name2 | String |  |  |
|  | Currency name 3 | currency_name3 | String |  |  |
| 5 | Master name | master_name | JSON Object |  |  |
|  | Master name 1 | master_name1 | String |  |  |
|  | Master name 2 | master_name2 | String |  |  |
|  | Master name 3 | master_name3 | String |  |  |
| 6 | Decimal name | decimal_name | JSON Object |  |  |
|  | Decimal name 1 | decimal_name1 | String |  |  |
|  | Decimal name 2 | decimal_name2 | String |  |  |
|  | Decimal name 3 | decimal_name3 | String |  |  |
| 7 | Decimal digits | decimal_digits | `Number` |  |  |
| 8 | Rounding digits | rounding_digits | `Number` |  |  |
| 9 | Status of currency | status_of_currency | String |  |  |
| 10 | Order | order | `Number` |  |  |
| 11 | Currency id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `/api/Currency/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_CURRENCY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 375
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Currency id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 375
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |