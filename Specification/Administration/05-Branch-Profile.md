# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api​/Branch​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_BRANCH`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `/api​/Branch​/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Branch code | branch_code | String | 4 |
| 2 | Branch name | branch_name | String |  |
| 3 | Address | branch_address | String |  |
| 4 | Base currency code | base_currency_code | String | 3 |
| 5 | Online | online | String |  |
| 6 | Branch id | id | `Number` |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/Branch​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_BRANCH`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | ISO 2 alpha | branch_code | No | String |  |  |  |
| 2 | ISO 3 alpha | branch_name | No | String |  |  |  |
| 3 | Country name | branch_address | No | String |  |  |  |
| 4 | ISO 3 alpha | base_currency_code | No | String |  |  |  |
| 5 | Country name | online | No | String |  |  |  |
| 6 | Page index | page_index | No | `Number` |  |  |  |
| 7 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Branch code | branch_code | String | 4 |
| 2 | Branch name | branch_name | String |  |
| 3 | Address | branch_address | String |  |
| 4 | Base currency code | base_currency_code | String | 3 |
| 5 | Online | online | String |  |
| 6 | Branch id | id | `Number` |  |

**Example:**
```json
{

}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Branch​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_BRANCH`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Old branch id | old_branch_id | No | String |  |  |
| 2 | Branch name | branch_name | `Yes` | String |  |  |
| 3 | Branch address | branch_address | No | String |  |  |
| 4 | Branch phone | branch_phone | No | String |  |  |
| 5 | Phone number | phone_number | No | JSON Object |  |  |
|  | Home | home | No | String |  |  |
|  | Office | office | No | String |  |  |
|  | Cell | cell | No | String |  |  |
|  | Facsimile | facsimile | No | String |  |  |
|  | Telex | telex | No | String |  |  |
| 6 | Tax code | tax_code | No | String |  |  |
| 7 | Base currency code | base_currency_code | `Yes` | String | 3 |  |
| 8 | Local currency code | local_currency_code | `Yes` | String | 3 |  |
| 9 | Reference code | reference_code | No | JSON Object |  |  |
|  | Bic | bic | No | String |  |  |
|  | Domestic bank code | domestic_bank_code | No | String |  |  |
|  | Internal code | internal_code | No | String |  |  |
| 10 | Country | country | `Yes` | String |  |  |
| 11 | Main language | main_language | `Yes` | String |  | EN |
| 12 | Time zone of branch | time_zone_of_branch | `Yes` | `Number` |  | số có 1 số thập phân |
| 13 | Thousand separate character | thousand_separate_character | `Yes` | String |  | , |
| 14 | Decimal separate character | decimal_separate_character | `Yes` | String |  | . |
| 15 | Date format for short | date_format_for_short | `Yes` | String |  | dd/MM/yyyy |
| 16 | Long date format | long_date_format | No | String |  |  |
| 17 | Time format | time_format | `Yes` | String |  | hh:mm:ss |
| 18 | Online | online | `Yes` | String |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch code | branch_code | String | 4 |
| 2 | Old branch id | old_branch_id | String |  |
| 3 | Branch name | branch_name | String |  |
| 4 | Branch address | branch_address | String |  |
| 5 | Branch phone | branch_phone | String |  |
| 6 | Phone number | phone_number | JSON Object |  |
|  | Home | home | String |  |
|  | Office | office | String |  |
|  | Cell | cell | String |  |
|  | Facsimile | facsimile | String |  |
|  | Telex | telex | String |  |
| 7 | Tax code | tax_code | String |  |
| 8 | Base currency code | base_currency_code | String | 3 |
| 9 | Local currency code | local_currency_code | String | 3 |
| 10 | Reference code | reference_code | JSON Object |  |
|  | Bic | bic | String |  |
|  | Domestic bank code | domestic_bank_code | String |  |
|  | Internal code | internal_code | String |  |
| 11 | Country | country | String |  |
| 12 | Main language | main_language | String |  |
| 13 | Time zone of branch | time_zone_of_branch | `Number` | số có 1 số thập phân |
| 14 | Thousand separate character | thousand_separate_character | String |  |
| 15 | Decimal separate character | decimal_separate_character | String |  |
| 16 | Date format for short | date_format_for_short | String |  |
| 17 | Long date format | long_date_format | String |  |
| 18 | Time format | time_format | String |  |
| 19 | Online | online | String |  |
| 20 | Branch id | id | `Number` |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** ​`/api​/Branch​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_BRANCH`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch code | branch_code | String | 4 |
| 2 | Old branch id | old_branch_id | String |  |
| 3 | Branch name | branch_name | String |  |
| 4 | Branch address | branch_address | String |  |
| 5 | Branch phone | branch_phone | String |  |
| 6 | Phone number | phone_number | JSON Object |  |
|  | Home | home | String |  |
|  | Office | office | String |  |
|  | Cell | cell | String |  |
|  | Facsimile | facsimile | String |  |
|  | Telex | telex | String |  |
| 7 | Tax code | tax_code | String |  |
| 8 | Base currency code | base_currency_code | String | 3 |
| 9 | Local currency code | local_currency_code | String | 3 |
| 10 | Reference code | reference_code | JSON Object |  |
|  | Bic | bic | String |  |
|  | Domestic bank code | domestic_bank_code | String |  |
|  | Internal code | internal_code | String |  |
| 11 | Country | country | String |  |
| 12 | Main language | main_language | String |  |
| 13 | Time zone of branch | time_zone_of_branch | `Number` | số có 1 số thập phân |
| 14 | Thousand separate character | thousand_separate_character | String |  |
| 15 | Decimal separate character | decimal_separate_character | String |  |
| 16 | Date format for short | date_format_for_short | String |  |
| 17 | Long date format | long_date_format | String |  |
| 18 | Time format | time_format | String |  |
| 19 | Online | online | String |  |
| 20 | Branch id | id | `Number` |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** ​`/api​/Branch​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_BRANCH`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | id | `Yes` | `Number` |  |  |
| 2 | Old branch id | old_branch_id | No | String |  |  |
| 3 | Branch name | branch_name | `Yes` | String |  |  |
| 4 | Branch address | branch_address | No | String |  |  |
| 5 | Branch phone | branch_phone | No | String |  |  |
| 6 | Phone number | phone_number | No | JSON Object |  |  |
|  | Home | home | No | String |  |  |
|  | Office | office | No | String |  |  |
|  | Cell | cell | No | String |  |  |
|  | Facsimile | facsimile | No | String |  |  |
|  | Telex | telex | No | String |  |  |
| 7 | Tax code | tax_code | No | String |  |  |
| 8 | Base currency code | base_currency_code | `Yes` | String | 3 |  |
| 9 | Local currency code | local_currency_code | `Yes` | String | 3 |  |
| 10 | Reference code | reference_code | No | JSON Object |  |  |
|  | Bic | bic | No | String |  |  |
|  | Domestic bank code | domestic_bank_code | No | String |  |  |
|  | Internal code | internal_code | No | String |  |  |
| 11 | Country | country | `Yes` | String |  |  |
| 12 | Main language | main_language | `Yes` | String |  | EN |
| 13 | Time zone of branch | time_zone_of_branch | `Yes` | `Number` |  | số có 1 số thập phân |
| 14 | Thousand separate character | thousand_separate_character | `Yes` | String |  | , |
| 15 | Decimal separate character | decimal_separate_character | `Yes` | String |  | . |
| 16 | Date format for short | date_format_for_short | `Yes` | String |  | dd/MM/yyyy |
| 17 | Long date format | long_date_format | No | String |  |  |
| 18 | Time format | time_format | `Yes` | String |  | hh:mm:ss |
| 19 | Online | online | `Yes` | String |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch code | branch_code | String | 4 |
| 2 | Old branch id | old_branch_id | String |  |
| 3 | Branch name | branch_name | String |  |
| 4 | Branch address | branch_address | String |  |
| 5 | Branch phone | branch_phone | String |  |
| 6 | Phone number | phone_number | JSON Object |  |
|  | Home | home | String |  |
|  | Office | office | String |  |
|  | Cell | cell | String |  |
|  | Facsimile | facsimile | String |  |
|  | Telex | telex | String |  |
| 7 | Tax code | tax_code | String |  |
| 8 | Base currency code | base_currency_code | String | 3 |
| 9 | Local currency code | local_currency_code | String | 3 |
| 10 | Reference code | reference_code | JSON Object |  |
|  | Bic | bic | String |  |
|  | Domestic bank code | domestic_bank_code | String |  |
|  | Internal code | internal_code | String |  |
| 11 | Country | country | String |  |
| 12 | Main language | main_language | String |  |
| 13 | Time zone of branch | time_zone_of_branch | `Number` | số có 1 số thập phân |
| 14 | Thousand separate character | thousand_separate_character | String |  |
| 15 | Decimal separate character | decimal_separate_character | String |  |
| 16 | Date format for short | date_format_for_short | String |  |
| 17 | Long date format | long_date_format | String |  |
| 18 | Time format | time_format | String |  |
| 19 | Online | online | String |  |
| 20 | Branch id | id | `Number` |  |

**Example:**
```json
{

}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `/api​/Branch​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_BRANCH`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 7
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch id | id | `Number` |  |

**Example:**
```json
{
    "id": 7
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. List user in branch
## 7.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_GET_LIST_USERS_AT_BRANCH`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id | id | `Yes` | `Number` |  |  |  |
| 2 | Page size | page_size | No | `Number` |  |  |  |
| 3 | Page index | page_index | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | User name | user_name | String |  |  |
| 2 | Login name | login_name | String |  |  |
| 3 | Branch name | branch_name | String |  |  |
| 4 | Department name | department_name | String |  |  |
| 5 | Status | user_account_status | String |  |  |
| 6 | Is online | is_online | String |  |  |
| 7 | Email | email | String |  |  |
| 8 | User code | user_code | String |  |  |
| 9 | User id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |