# 1. View
## 1.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_LIMIT_POSITION_VIEWALL`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |


### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Limit amount in base currency data |  | Array Object |  |  | s_lmtpo, s_usrrole |
| 1 | Role id | role_id | `Number` |  |  |  |
| 2 | Role name | role_name | String |  |  |  |
| 3 | Amount | amount | `Number` |  |  | số có hai số thập phân |

## 1.2 Transaction flow
**Conditions:**
- Cash that users is keeping.

**Flow of events:**
- When user make transaction have: Cash deposit + Current cash position > Limit cash position
    - Warning by over limit cash position in desk.
- Approval only transaction over limit (cover cash in pending transaction f8).
- Not allow doing next transaction only if user return to main cashier.

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 2. Modify
## 2.1 Field description
### Request message
**HTTP Method:** ``

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CSH_LIMIT_POSITION_MODIFY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
|  | Limit amount in base currency data | cash_limits | `Yes` | Array Object |  |  | s_lmtpo, s_usrrole |
| 1 | Role id | role_id | `Yes` | `Number` |  |  | is unique, không được sửa |
| 2 | Amount | amount | No | `Number` |  |  | số có hai số thập phân |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Limit amount in base currency data |  | Array Object |  |  | s_lmtpo, s_usrrole |
| 1 | Role id | role_id | `Number` |  |  |  |
| 2 | Role name | role_name | String |  |  |  |
| 3 | Amount | amount | `Number` |  |  | số có hai số thập phân |

## 2.2 Transaction flow
- Bảng KTB trên UI đang dùng không nhập số thập phân.

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |