# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api/Country/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_COUNTRY`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `/api/Country/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | ISO 2 alpha | iso2_alpha | String | 3 | c_country |
| 2 | ISO 3 alpha | iso3_alpha | String | 2 |  |
| 3 | Country name | country_name | String |  |  |
| 4 | Country id | id | `Number` |  |  |

**Example:**
```json
{

}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/Country/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_COUNTRY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | ISO 2 alpha | iso2_alpha | No | String |  |  |  |
| 2 | ISO 3 alpha | iso3_alpha | No | String |  |  |  |
| 3 | Country name | country_name | No | String |  |  |  |
| 4 | Page index | page_index | No | `Number` |  |  |  |
| 5 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | ISO 2 alpha | iso2_alpha | String | 3 | c_country |
| 2 | ISO 3 alpha | iso3_alpha | String | 2 |  |
| 3 | Country name | country_name | String |  |  |
| 4 | Country id | id | `Number` |  |  |

**Example:**
```json
{

}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/Country/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_COUNTRY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | ISO 2 alpha | iso2_alpha | `Yes` | String | 2 |  | is unique |
| 2 | ISO 3 alpha | iso3_alpha | `Yes` | String | 3 |  | is unique |
| 3 | Country name | country_name | No | String |  |  |  |
| 4 | Multi lingual country name | multi_lingual_country_name | No | JSON Object |  |  |  |
|  | Country name 1 | country_name1 | No | String |  |  |  |
|  | Country name 2 | country_name2 | No | String |  |  |  |
|  | Country name 3 | country_name3 | No | String |  |  |  |
| 5 | Country short name | country_short_name | No | String |  |  |  |
| 6 | Multi lingual country short name | multi_lingual_country_short_name | No | JSON Object |  |  |  |
|  | Short name 1 | short_name1 | No | String |  |  |  |
|  | Short name 2 | short_name2 | No | String |  |  |  |
|  | Short name 3 | short_name3 | No | String |  |  |  |
| 7 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 8 | Main language | main_language | `Yes` | String |  |  |  |
| 9 | Status of country | status_of_country | `Yes` | String |  |  |  |
| 10 | Order | order | No | `Number` |  |  |  |
| 11 | Region of country | region_of_country | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | ISO 2 alpha | iso2_alpha | String | 2 |  |
| 2 | ISO 3 alpha | iso3_alpha | String | 3 |  |
| 3 | Country name | country_name | String |  |  |
| 4 | Multi-lingual country name | multi_lingual_country_name | JSON Object |  |  |
|  | Country name 1 | country_name1 | String |  |  |
|  | Country name 2 | country_name2 | String |  |  |
|  | Country name 3 | country_name3 | String |  |  |
| 5 | Country short name | country_short_name | String |  |  |
| 6 | Multi-lingual country short name | multi_lingual_country_short_name | JSON Object |  |  |
|  | Short name 1 | short_name1 | String |  |  |
|  | Short name 2 | short_name2 | String |  |  |
|  | Short name 3 | short_name3 | String |  |  |
| 7 | Currency code | currency_code | String | 3 |  |
| 8 | Main language | main_language | String |  |  |
| 9 | Status of country | status_of_country | String |  |  |
| 10 | Order | order | `Number` |  |  |
| 11 | Region of country | region_of_country | String |  |  |
| 12 | Country id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api/Country/View/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_COUNTRY`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Country id | id | `Yes` | `Number` |  |  |  |

**Example:** `/api/Country/View/224`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | ISO 2 alpha | iso2_alpha | String | 2 |  |
| 2 | ISO 3 alpha | iso3_alpha | String | 3 |  |
| 3 | Country name | country_name | String |  |  |
| 4 | Multi-lingual country name | multi_lingual_country_name | JSON Object |  |  |
|  | Country name 1 | country_name1 | String |  |  |
|  | Country name 2 | country_name2 | String |  |  |
|  | Country name 3 | country_name3 | String |  |  |
| 5 | Country short name | country_short_name | String |  |  |
| 6 | Multi-lingual country short name | multi_lingual_country_short_name | JSON Object |  |  |
|  | Short name 1 | short_name1 | String |  |  |
|  | Short name 2 | short_name2 | String |  |  |
|  | Short name 3 | short_name3 | String |  |  |
| 7 | Currency code | currency_code | String | 3 |  |
| 8 | Main language | main_language | String |  |  |
| 9 | Status of country | status_of_country | String |  |  |
| 10 | Order | order | `Number` |  |  |
| 11 | Region of country | region_of_country | String |  |  |
| 12 | Country id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `/api/Country/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_COUNTRY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Country id | id | `Yes` | `Number` |  |  | is existed |
| 2 | ISO 2 alpha | iso2_alpha | `Yes` | String | 2 |  | is unique |
| 3 | ISO 3 alpha | iso3_alpha | `Yes` | String | 3 |  | is unique |
| 4 | Country name | country_name | No | String |  |  |  |
| 5 | Multi lingual country name | multi_lingual_country_name | No | JSON Object |  |  |  |
|  | Country name 1 | country_name1 | No | String |  |  |  |
|  | Country name 2 | country_name2 | No | String |  |  |  |
|  | Country name 3 | country_name3 | No | String |  |  |  |
| 6 | Country short name | country_short_name | No | String |  |  |  |
| 7 | Multi lingual country short name | multi_lingual_country_short_name | No | JSON Object |  |  |  |
|  | Short name 1 | short_name1 | No | String |  |  |  |
|  | Short name 2 | short_name2 | No | String |  |  |  |
|  | Short name 3 | short_name3 | No | String |  |  |  |
| 8 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 9 | Main language | main_language | `Yes` | String |  |  |  |
| 10 | Status of country | status_of_country | `Yes` | String |  |  |  |
| 11 | Order | order | No | `Number` |  |  |  |
| 12 | Region of country | region_of_country | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | ISO 2 alpha | iso2_alpha | String | 2 |  |
| 2 | ISO 3 alpha | iso3_alpha | String | 3 |  |
| 3 | Country name | country_name | String |  |  |
| 4 | Multi-lingual country name | multi_lingual_country_name | JSON Object |  |  |
|  | Country name 1 | country_name1 | String |  |  |
|  | Country name 2 | country_name2 | String |  |  |
|  | Country name 3 | country_name3 | String |  |  |
| 5 | Country short name | country_short_name | String |  |  |
| 6 | Multi-lingual country short name | multi_lingual_country_short_name | JSON Object |  |  |
|  | Short name 1 | short_name1 | String |  |  |
|  | Short name 2 | short_name2 | String |  |  |
|  | Short name 3 | short_name3 | String |  |  |
| 7 | Currency code | currency_code | String | 3 |  |
| 8 | Main language | main_language | String |  |  |
| 9 | Status of country | status_of_country | String |  |  |
| 10 | Order | order | `Number` |  |  |
| 11 | Region of country | region_of_country | String |  |  |
| 12 | Country id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `/api/Country/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_COUNTRY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Country id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 224
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Country id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 224
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |