# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/Company​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_COMPANY`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Company code | company_code | String |  |  |
| 2 | Company name | company_name | String |  |  |
| 3 | Company type | company_type | String |  |  |
| 4 | Company status | company_status | String |  |  |
| 5 | Description | descr | String |  |  |
| 6 | Company id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/Company​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_COMPANY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Company code | company_code | No | String |  |  |  |
| 2 | Company name | company_name | No | String |  |  |  |
| 3 | Company type | company_type | No | String |  |  | Bank, Securities |
| 4 | Company status | company_status | No | String |  |  | Active, Blocked |
| 5 | Description | descr | No | String |  |  |  |
| 6 | Page index | page_index | No | `Number` |  |  |  |
| 7 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Company code | company_code | String |  |  |
| 2 | Company name | company_name | String |  |  |
| 3 | Company type | company_type | String |  |  |
| 4 | Company status | company_status | String |  |  |
| 5 | Description | descr | String |  |  |
| 6 | Company id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/Company​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_COMPANY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Company code | company_code | `Yes` | String |  |  |  |
| 2 | Company name | company_name | `Yes` | String |  |  |  |
| 3 | Company type | company_type | `Yes` | String |  |  | Bank, Securities |
| 4 | Company status | company_status | `Yes` | String |  |  | Active, Blocked |
| 5 | Gatway username | gatway_user_name | No | String |  |  | used to login to HOST |
| 6 | Gatway password | gatway_password | No | String |  |  | used to login to HOST |
| 7 | Description | descr | No | String |  |  |  |
| 8 | Message type field | message_type_field | No | String |  |  |  |
| 9 | Processing code field | processing_code_field | No | String |  |  |  |
| 10 | Message standard | message_standard | No | String |  |  |  |
| 11 | Message format | message_format | No | String |  |  | json/xml/array/string |
| 12 | Message transport protocol | message_transport_protocol | No | String |  |  |  |
| 13 | Company URI  for sending | uri | No | String |  |  |  |
| 14 | Sending connectivity username | user_name | No | String |  |  |  |
| 15 | Sending connectivity password | password | No | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Company code | company_code | String |  |  |
| 2 | Company name | company_name | String |  |  |
| 3 | Company type | company_type | String |  |  |
| 4 | Company status | company_status | String |  |  |
| 5 | Gatway username | gatway_user_name | String |  |  |
| 6 | Gatway password | gatway_password | String |  |  |
| 7 | Description | descr | String |  |  |
| 8 | Message type field | message_type_field | String |  |  |
| 9 | Processing code field | processing_code_field | String |  |  |
| 10 | Message standard | message_standard | String |  |  |
| 11 | Message format | message_format | String |  |  |
| 12 | Message transport protocol | message_transport_protocol | String |  |  |
| 13 | Company URI  for sending | uri | String |  |  |
| 14 | Sending connectivity username | user_name | String |  |  |
| 15 | Sending connectivity password | password | String |  |  |
| 16 | Company id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/Company​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_COMPANY`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Company id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Company code | company_code | String |  |  |
| 2 | Company name | company_name | String |  |  |
| 3 | Company type | company_type | String |  |  |
| 4 | Company status | company_status | String |  |  |
| 5 | Gatway username | gatway_user_name | String |  |  |
| 6 | Gatway password | gatway_password | String |  |  |
| 7 | Description | descr | String |  |  |
| 8 | Message type field | message_type_field | String |  |  |
| 9 | Processing code field | processing_code_field | String |  |  |
| 10 | Message standard | message_standard | String |  |  |
| 11 | Message format | message_format | String |  |  |
| 12 | Message transport protocol | message_transport_protocol | String |  |  |
| 13 | Company URI  for sending | uri | String |  |  |
| 14 | Sending connectivity username | user_name | String |  |  |
| 15 | Sending connectivity password | password | String |  |  |
| 16 | Company id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** ​`​/api​/Company​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_COMPANY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Company name | company_name | `Yes` | String |  |  |  |
| 2 | Company type | company_type | `Yes` | String |  |  | Bank, Securities |
| 3 | Company status | company_status | `Yes` | String |  |  | Active, Blocked |
| 4 | Gatway username | gatway_user_name | No | String |  |  | used to login to HOST |
| 5 | Gatway password | gatway_password | No | String |  |  | used to login to HOST |
| 6 | Description | descr | No | String |  |  |  |
| 7 | Message type field | message_type_field | No | String |  |  |  |
| 8 | Processing code field | processing_code_field | No | String |  |  |  |
| 9 | Message standard | message_standard | No | String |  |  |  |
| 10 | Message format | message_format | No | String |  |  | json/xml/array/string |
| 11 | Message transport protocol | message_transport_protocol | No | String |  |  |  |
| 12 | Company URI  for sending | uri | No | String |  |  |  |
| 13 | Sending connectivity username | user_name | No | String |  |  |  |
| 14 | Sending connectivity password | password | No | String |  |  |  |
| 15 | Company id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Company code | company_code | String |  |  |
| 2 | Company name | company_name | String |  |  |
| 3 | Company type | company_type | String |  |  |
| 4 | Company status | company_status | String |  |  |
| 5 | Gatway username | gatway_user_name | String |  |  |
| 6 | Gatway password | gatway_password | String |  |  |
| 7 | Description | descr | String |  |  |
| 8 | Message type field | message_type_field | String |  |  |
| 9 | Processing code field | processing_code_field | String |  |  |
| 10 | Message standard | message_standard | String |  |  |
| 11 | Message format | message_format | String |  |  |
| 12 | Message transport protocol | message_transport_protocol | String |  |  |
| 13 | Company URI  for sending | uri | String |  |  |
| 14 | Sending connectivity username | user_name | String |  |  |
| 15 | Sending connectivity password | password | String |  |  |
| 16 | Company id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `/api​/Company​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_COMPANY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Company id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Company id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |