# 1. Refresh
- [Refresh của "Branch Open-Branch Close"](Specification/Administration/14 Branch Open-Branch Close)

# 2. Open bank
## 2.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api/Bank/OpenBank`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `OPNBANK`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Business date | business_date | `Date` |  |  |  |

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |

# 3. Close bank
## 3.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api/Bank/CloseBank`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `CLSBANK`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |


### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Business date | business_date | `Date` |  |  |  |

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |

# 4. Close branch
- [Close branch của "Branch Open-Branch Close"](Specification/Administration/14 Branch Open-Branch Close)

# 5. Check bank status
## 5.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api/Bank/BankStatus`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `BANK_STATUS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |


### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Status | status | String |  |  | Y: có online, N: không online |

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |