# 1. Add
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/Calendar​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_CALENDAR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Date | sqn_date | `Yes` | `Date` |  |  |  |
| 2 | Current date | is_current_date | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 3 | Holiday | is_holiday | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 4 | End of week (in fact) | is_end_of_week | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 5 | End of month (in fact) | is_end_of_month | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 6 | End of quarter (in fact) | is_end_of_quater | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 7 | End of half-year (in fact) | is_end_of_half_year | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 8 | End of year (in fact) | is_end_of_year | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 9 | Begin of week (in fact) | is_begin_of_week | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 10 | Begin of month (in fact) | is_begin_of_month | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 11 | Begin of quarter (in fact) | is_begin_of_quater | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 12 | Begin of half-year (in fact) | is_begin_of_half_year | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 13 | Begin of year (in fact) | is_begin_of_year | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 14 | End of week (fiscal) | is_fiscal_end_of_week | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 15 | End of month (fiscal) | is_fiscal_end_of_month | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 16 | End of quarter (fiscal) | is_fiscal_end_of_quater | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 17 | End of half-year (fiscal) | is_fiscal_end_of_half_year | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 18 | End of year (fiscal) | is_fiscal_end_of_year | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 19 | Begin of week (fiscal) | is_fiscal_begin_of_week | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 20 | Begin of month (fiscal) | is_fiscal_begin_of_month | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 21 | Begin of quarter (fiscal) | is_fiscal_begin_of_quater | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 22 | Begin of half-year (fiscal) | is_fiscal_begin_of_half_year | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 23 | Begin of year (fiscal) | is_fiscal_begin_of_year | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 24 | Description | descs | No | String |  |  |  |
| 25 | Currency code | currency_code | `Yes` | String | 3 | USD |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Date | sqn_date | `Date` |  |  |
| 2 | Current date | is_current_date | `Number` | 1 |  |
| 3 | Holiday | is_holiday | `Number` | 1 |  |
| 4 | End of week (in fact) | is_end_of_week | `Number` | 1 |  |
| 5 | End of month (in fact) | is_end_of_month | `Number` | 1 |  |
| 6 | End of quarter (in fact) | is_end_of_quater | `Number` | 1 |  |
| 7 | End of half-year (in fact) | is_end_of_half_year | `Number` | 1 |  |
| 8 | End of year (in fact) | is_end_of_year | `Number` | 1 |  |
| 9 | Begin of week (in fact) | is_begin_of_week | `Number` | 1 |  |
| 10 | Begin of month (in fact) | is_begin_of_month | `Number` | 1 |  |
| 11 | Begin of quarter (in fact) | is_begin_of_quater | `Number` | 1 |  |
| 12 | Begin of half-year (in fact) | is_begin_of_half_year | `Number` | 1 |  |
| 13 | Begin of year (in fact) | is_begin_of_year | `Number` | 1 |  |
| 14 | End of week (fiscal) | is_fiscal_end_of_week | `Number` | 1 |  |
| 15 | End of month (fiscal) | is_fiscal_end_of_month | `Number` | 1 |  |
| 16 | End of quarter (fiscal) | is_fiscal_end_of_quater | `Number` | 1 |  |
| 17 | End of half-year (fiscal) | is_fiscal_end_of_half_year | `Number` | 1 |  |
| 18 | End of year (fiscal) | is_fiscal_end_of_year | `Number` | 1 |  |
| 19 | Begin of week (fiscal) | is_fiscal_begin_of_week | `Number` | 1 |  |
| 20 | Begin of month (fiscal) | is_fiscal_begin_of_month | `Number` | 1 |  |
| 21 | Begin of quarter (fiscal) | is_fiscal_begin_of_quater | `Number` | 1 |  |
| 22 | Begin of half-year (fiscal) | is_fiscal_begin_of_half_year | `Number` | 1 |  |
| 23 | Begin of year (fiscal) | is_fiscal_begin_of_year | `Number` | 1 |  |
| 24 | Description | descs | String |  |  |
| 25 | Currency code | currency_code | String | 3 |  |
| 26 | Calendar​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 2. Modify
## 2.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** ​`​/api​/Calendar​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_CALENDAR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Date | sqn_date | `Yes` | `Date` |  |  | không được sửa |
| 2 | Holiday | is_holiday | `Yes` | `Number` | 1 | 0 | 0: No, 1: Yes |
| 3 | Description | descs | No | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Date | sqn_date | `Date` |  |  |
| 2 | Current date | is_current_date | `Number` | 1 |  |
| 3 | Holiday | is_holiday | `Number` | 1 |  |
| 4 | End of week (in fact) | is_end_of_week | `Number` | 1 |  |
| 5 | End of month (in fact) | is_end_of_month | `Number` | 1 |  |
| 6 | End of quarter (in fact) | is_end_of_quater | `Number` | 1 |  |
| 7 | End of half-year (in fact) | is_end_of_half_year | `Number` | 1 |  |
| 8 | End of year (in fact) | is_end_of_year | `Number` | 1 |  |
| 9 | Begin of week (in fact) | is_begin_of_week | `Number` | 1 |  |
| 10 | Begin of month (in fact) | is_begin_of_month | `Number` | 1 |  |
| 11 | Begin of quarter (in fact) | is_begin_of_quater | `Number` | 1 |  |
| 12 | Begin of half-year (in fact) | is_begin_of_half_year | `Number` | 1 |  |
| 13 | Begin of year (in fact) | is_begin_of_year | `Number` | 1 |  |
| 14 | End of week (fiscal) | is_fiscal_end_of_week | `Number` | 1 |  |
| 15 | End of month (fiscal) | is_fiscal_end_of_month | `Number` | 1 |  |
| 16 | End of quarter (fiscal) | is_fiscal_end_of_quater | `Number` | 1 |  |
| 17 | End of half-year (fiscal) | is_fiscal_end_of_half_year | `Number` | 1 |  |
| 18 | End of year (fiscal) | is_fiscal_end_of_year | `Number` | 1 |  |
| 19 | Begin of week (fiscal) | is_fiscal_begin_of_week | `Number` | 1 |  |
| 20 | Begin of month (fiscal) | is_fiscal_begin_of_month | `Number` | 1 |  |
| 21 | Begin of quarter (fiscal) | is_fiscal_begin_of_quater | `Number` | 1 |  |
| 22 | Begin of half-year (fiscal) | is_fiscal_begin_of_half_year | `Number` | 1 |  |
| 23 | Begin of year (fiscal) | is_fiscal_begin_of_year | `Number` | 1 |  |
| 24 | Description | descs | String |  |  |
| 25 | Currency code | currency_code | String | 3 |  |
| 26 | Calendar​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 3. Get list calendars by year
## 3.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_GET_LIST_CALENDARS`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Year | year | No | `Number` |  |  |  |
| 2 | Year | month | No | `Number` |  |  |  |
| 3 | Page index | page_index | No | `Number` |  |  |  |
| 4 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Date | sqn_date | `Date` |  |  |
| 2 | Current date | is_current_date | `Number` | 1 |  |
| 3 | Holiday | is_holiday | `Number` | 1 |  |
| 4 | End of week (in fact) | is_end_of_week | `Number` | 1 |  |
| 5 | End of month (in fact) | is_end_of_month | `Number` | 1 |  |
| 6 | End of quarter (in fact) | is_end_of_quater | `Number` | 1 |  |
| 7 | End of half-year (in fact) | is_end_of_half_year | `Number` | 1 |  |
| 8 | End of year (in fact) | is_end_of_year | `Number` | 1 |  |
| 9 | Begin of week (in fact) | is_begin_of_week | `Number` | 1 |  |
| 10 | Begin of month (in fact) | is_begin_of_month | `Number` | 1 |  |
| 11 | Begin of quarter (in fact) | is_begin_of_quater | `Number` | 1 |  |
| 12 | Begin of half-year (in fact) | is_begin_of_half_year | `Number` | 1 |  |
| 13 | Begin of year (in fact) | is_begin_of_year | `Number` | 1 |  |
| 14 | End of week (fiscal) | is_fiscal_end_of_week | `Number` | 1 |  |
| 15 | End of month (fiscal) | is_fiscal_end_of_month | `Number` | 1 |  |
| 16 | End of quarter (fiscal) | is_fiscal_end_of_quater | `Number` | 1 |  |
| 17 | End of half-year (fiscal) | is_fiscal_end_of_half_year | `Number` | 1 |  |
| 18 | End of year (fiscal) | is_fiscal_end_of_year | `Number` | 1 |  |
| 19 | Begin of week (fiscal) | is_fiscal_begin_of_week | `Number` | 1 |  |
| 20 | Begin of month (fiscal) | is_fiscal_begin_of_month | `Number` | 1 |  |
| 21 | Begin of quarter (fiscal) | is_fiscal_begin_of_quater | `Number` | 1 |  |
| 22 | Begin of half-year (fiscal) | is_fiscal_begin_of_half_year | `Number` | 1 |  |
| 23 | Begin of year (fiscal) | is_fiscal_begin_of_year | `Number` | 1 |  |
| 24 | Description | descs | String |  |  |
| 25 | Currency code | currency_code | String | 3 |  |
| 26 | Calendar​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Add new year
## 4.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADD_NEW_YEAR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |


**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Date | sqn_date | `Date` |  |  |
| 2 | Current date | is_current_date | `Number` | 1 |  |
| 3 | Holiday | is_holiday | `Number` | 1 |  |
| 4 | End of week (in fact) | is_end_of_week | `Number` | 1 |  |
| 5 | End of month (in fact) | is_end_of_month | `Number` | 1 |  |
| 6 | End of quarter (in fact) | is_end_of_quater | `Number` | 1 |  |
| 7 | End of half-year (in fact) | is_end_of_half_year | `Number` | 1 |  |
| 8 | End of year (in fact) | is_end_of_year | `Number` | 1 |  |
| 9 | Begin of week (in fact) | is_begin_of_week | `Number` | 1 |  |
| 10 | Begin of month (in fact) | is_begin_of_month | `Number` | 1 |  |
| 11 | Begin of quarter (in fact) | is_begin_of_quater | `Number` | 1 |  |
| 12 | Begin of half-year (in fact) | is_begin_of_half_year | `Number` | 1 |  |
| 13 | Begin of year (in fact) | is_begin_of_year | `Number` | 1 |  |
| 14 | End of week (fiscal) | is_fiscal_end_of_week | `Number` | 1 |  |
| 15 | End of month (fiscal) | is_fiscal_end_of_month | `Number` | 1 |  |
| 16 | End of quarter (fiscal) | is_fiscal_end_of_quater | `Number` | 1 |  |
| 17 | End of half-year (fiscal) | is_fiscal_end_of_half_year | `Number` | 1 |  |
| 18 | End of year (fiscal) | is_fiscal_end_of_year | `Number` | 1 |  |
| 19 | Begin of week (fiscal) | is_fiscal_begin_of_week | `Number` | 1 |  |
| 20 | Begin of month (fiscal) | is_fiscal_begin_of_month | `Number` | 1 |  |
| 21 | Begin of quarter (fiscal) | is_fiscal_begin_of_quater | `Number` | 1 |  |
| 22 | Begin of half-year (fiscal) | is_fiscal_begin_of_half_year | `Number` | 1 |  |
| 23 | Begin of year (fiscal) | is_fiscal_begin_of_year | `Number` | 1 |  |
| 24 | Description | descs | String |  |  |
| 25 | Currency code | currency_code | String | 3 |  |
| 26 | Calendar​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Get distinst danh sách year trong calendar
- **Workflow id:** `ADM_GET_LIST_YEARS`

- **Request message**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Page index | page_index | No | `Number` |  |  |  |
| 2 | Page size | page_size | No | `Number` |  |  |  |

- **Response message**

| No | Field name | Parameter | Type | Length | Description |
 -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Name | code_name | String |  | Giá trị mặc định là "YEAR" |
| 2 | Caption | caption | Number |  | Giá trị là số của năm, ví dụ 2020 |