# 1. Login
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/Authenticate/GetToken`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `UMG_LOGIN`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Username | username | `yes` | String |  |  |  |
| 2 | Password | password | `yes` | String |  |  |  |

**Example:**
```json
{
    "username": "admin",
    "password": "123456"
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  |  |
| 3 | User name | user_name | String |  |  |
| 4 | Login name | login_name | String |  |  |
| 5 | Token | token | String |  |  |
| 6 | Working date | working_date | `Date time` |  |  |
| 7 | Branch id | branch_id | `Number` |  |  |
| 8 | Branch code | branch_code | String |  |  |
| 9 | Branch name | branch_name | String |  |  |
| 10 | Department id | department_id | `Number` |  |  |
| 11 | Department code | department_code | String |  |  |
| 12 | Position | position | JSON Object |  |  |
|  | Cashier | cashier | `Number` |  | 1: check, 0: uncheck |
|  | Officer | officer | `Number` |  | 1: check, 0: uncheck |
|  | Chief cashier | chief_cashier | `Number` |  | 1: check, 0: uncheck |
|  | Operation staff | operation_staff | `Number` |  | 1: check, 0: uncheck |
|  | Dealer | dealer | `Number` |  | 1: check, 0: uncheck |
|  | Inter-branch user | inter_branch_user | `Number` |  | 1: check, 0: uncheck |
|  | Branch manager/authorized | branch_manager | `Number` |  | 1: check, 0: uncheck |
| 13 | Branch status | branch_status | String | 1 |  |
| 14 | Bank status | bank_status | String | 1 |  |
| 15 | Reset password | reset_password | `Boolean` |  |  |


**Example:**
```json
{
    "id": 2,
    "user_code": "00921",
    "user_name": "KH Oper 001",
    "login_name": "ac01",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ5Mzg1MzcyIiwiZXhwIjoiMTY0OTk5MDE3MiIsIlVzZXJJZCI6IjIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiS0ggT3BlciAwMDEifQ.0U4sxebvH12Qsh9ZAcYWduF0D703OlQ1LiTSguqOfwM",
    "working_date": "2022-09-12T00:00:00",
    "branch_id": 1,
    "branch_code": "0848",
    "branch_name": "SIEM REAP",
    "department_id": 2,
    "department_code": "00002",
    "position": {
        "cashier": 0,
        "officer": 0,
        "chief_cashier": 0,
        "operation_staff": 0,
        "dealer": 0,
        "inter_branch_user": 0,
        "branch_manager": 0
    },
    "branch_status": "Y",
    "bank_status": "Y",
    "reset_password": false
}
```

## 1.2 Transaction flow
Trường hợp reset_password có giá trị: 
- true: Cần update password.
- false: KHÔNG cần update password.

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid account or password! | Tên đăng nhập và mật khẩu không hợp lệ |

# 2. Logout
## 2.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `UMG_LOGOUT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Logout | logout | String |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |

# 3. Login approve
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/Authenticate/LoginApprove`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `UMG_LOGIN_APPROVE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Username | username | `yes` | String |  |  |  |
| 2 | Password | password | `yes` | String |  |  |  |

**Example:**
```json
{
    "username": "ac02",
    "password": "123456"
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 3
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid account or password! | Tên đăng nhập và mật khẩu không hợp lệ |

# 4. Get branch id HO
## 4.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_GET_HOST_BRANCH_ID`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |

### Response message
| No | Field name | Parameter | Type | Length | Description |
|----|------------|-----------|------|--------|-------------|
| 1 | Branch id HO | branch_id_ho | `Number` |  |  |

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |

# 5. Update password
- [Update password của "User Profile"](Specification/Administration/07 User Profile)