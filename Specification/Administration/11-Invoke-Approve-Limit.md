# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/UserLimit​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_USER_LIMIT`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page index | page_index | No | `Number` |  |  |  |
| 3 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Currency code of limit | currency_code | String |  |  |
| 4 | Limit of this command | u_limit | `Number` |  |  |
| 5 | Convertible | cv_table | String |  | không sử dụng. Convertible to others currencies (Y/N) |
| 6 | Limit type | limit_type | String |  | Invoke (I) limit or Approve (A) limit |
| 7 | Margin | margin | `Number` |  | không sử dụng. Limit allowance (%) need approve |
| 8 | User limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/UserLimit​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_USER_LIMIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | No | `Number` |  |  |  |
| 2 | Commands id | command_id | No | String |  |  |  |
| 3 | Page index | page_index | No | `Number` |  |  |  |
| 4 | Page size | page_size | No | `Number` |  |  |  |
| 5 | Limit type | limit_type | `Yes` | String | 1 |  | Invoke (I) limit or Approve (A) limit |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Currency code of limit | currency_code | String |  |  |
| 4 | Limit of this command | u_limit | `Number` |  |  |
| 5 | Convertible | cv_table | String |  | không sử dụng. Convertible to others currencies (Y/N) |
| 6 | Limit type | limit_type | String |  | Invoke (I) limit or Approve (A) limit |
| 7 | Margin | margin | `Number` |  | không sử dụng. Limit allowance (%) need approve |
| 8 | User limit id | id | `Number` |  |  |
| 9 | Module | module | String |  |  |
| 10 | Tran name | tran_name | String |  |  |

**Note:**
- Gửi thêm limit_type trong điều kiện advanced search
- Trả thêm module và tran_name ở response:
  - Lấy dữ liệu trong bảng `UserCommand` tương ứng với điều kiện: command_id = CommandId
  - tran_name = CommandName
  - module = CommandName của ParentId, ví dụ: CommandId = `ACT_DPT`, ParentId = ACT => `command_name = FX accounting - deposit`,  `module = Accounting`

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/UserLimit​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_USER_LIMIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 2 | Commands id | command_id | `Yes` | String |  |  |  |
| 3 | Currency code of limit | currency_code | `Yes` | String |  |  |  |
| 4 | Limit of this command | u_limit | No | `Number` | 16 | `Null` | null: không có quyền, >=0: có quyền làm với amount tương ứng |
| 5 | Convertible | cv_table | No | String |  |  | không sử dụng. Convertible to others currencies (Y/N) |
| 6 | Limit type | limit_type | `Yes` | String | 1 |  | Invoke (I) limit or Approve (A) limit |
| 7 | Margin | margin | No | `Number` |  |  | không sử dụng. Limit allowance (%) need approve |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Currency code of limit | currency_code | String |  |  |
| 4 | Limit of this command | u_limit | `Number` |  |  |
| 5 | Convertible | cv_table | String |  | không sử dụng. Convertible to others currencies (Y/N) |
| 6 | Limit type | limit_type | String |  | Invoke (I) limit or Approve (A) limit |
| 7 | Margin | margin | `Number` |  | không sử dụng. Limit allowance (%) need approve |
| 8 | User limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/UserLimit​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_USER_LIMIT`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User limit id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Currency code of limit | currency_code | String |  |  |
| 4 | Limit of this command | u_limit | `Number` |  |  |
| 5 | Convertible | cv_table | String |  | không sử dụng. Convertible to others currencies (Y/N) |
| 6 | Limit type | limit_type | String |  | Invoke (I) limit or Approve (A) limit |
| 7 | Margin | margin | `Number` |  | không sử dụng. Limit allowance (%) need approve |
| 8 | User limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** ​`​/api​/UserLimit​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_USER_LIMIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 2 | Commands id | command_id | `Yes` | String |  |  |  |
| 3 | Currency code of limit | currency_code | `Yes` | String |  |  |  |
| 4 | Limit of this command | u_limit | No | `Number` | 16 | `Null` | null: không có quyền, >=0: có quyền làm với amount tương ứng |
| 5 | Convertible | cv_table | No | String |  |  | không sử dụng. Convertible to others currencies (Y/N) |
| 6 | Limit type | limit_type | `Yes` | String | 1 |  | Invoke (I) limit or Approve (A) limit |
| 7 | Margin | margin | No | `Number` |  |  | không sử dụng. Limit allowance (%) need approve |
| 8 | User limit id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Currency code of limit | currency_code | String |  |  |
| 4 | Limit of this command | u_limit | `Number` |  |  |
| 5 | Convertible | cv_table | String |  | không sử dụng. Convertible to others currencies (Y/N) |
| 6 | Limit type | limit_type | String |  | Invoke (I) limit or Approve (A) limit |
| 7 | Margin | margin | `Number` |  | không sử dụng. Limit allowance (%) need approve |
| 8 | User limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `​/api​/UserLimit​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_USER_LIMIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User limit id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User limit id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list user limit by role id
## 7.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_USER_LIMIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | No | `Number` |  |  |  |
| 2 | Commands id | command_id | No | String |  |  |  |
| 3 | Page index | page_index | No | `Number` |  |  |  |
| 4 | Page size | page_size | No | `Number` |  |  |  |
| 5 | Limit type | limit_type | `Yes` | String | 1 |  | Invoke (I) limit or Approve (A) limit |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Currency code of limit | currency_code | String |  |  |
| 4 | Limit of this command | u_limit | `Number` |  |  |
| 5 | Convertible | cv_table | String |  | không sử dụng. Convertible to others currencies (Y/N) |
| 6 | Limit type | limit_type | String |  | Invoke (I) limit or Approve (A) limit |
| 7 | Margin | margin | `Number` |  | không sử dụng. Limit allowance (%) need approve |
| 8 | User limit id | id | `Number` |  |  |
| 9 | Module | module | String |  |  |
| 10 | Tran name | tran_name | String |  |  |

**Note:**
- Gửi thêm limit_type trong điều kiện advanced search
- Trả thêm module và tran_name ở response:
  - Lấy dữ liệu trong bảng `UserCommand` tương ứng với điều kiện: command_id = CommandId
  - tran_name = CommandName
  - module = CommandName của ParentId, ví dụ: CommandId = `ACT_DPT`, ParentId = ACT => `command_name = FX accounting - deposit`,  `module = Accounting`

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 8. Update user limit (Add nếu chưa tồn tại) theo mảng
## 8.1 Field description
### Request message

**Workflow id:** `ADM_UPDATE_LIST_USER_LIMIT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
|  | List user right | list_user_limit |  | Array Object |  |  |  |
| 1 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 2 | Commands id | command_id | `Yes` | String |  |  |  |
| 3 | Currency code of limit | currency_code | `Yes` | String |  |  |  |
| 4 | Limit of this command | u_limit | No | `Number` | 16 | `Null` | null: không có quyền, >=0: có quyền làm với amount tương ứng |
| 5 | Limit type | limit_type | `Yes` | String | 1 |  | Invoke (I) limit or Approve (A) limit |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | List user right | list_user_limit | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Currency code | currency_code | String |  |  |
| 4 | Limit of this command | u_limit | `Number` |  |  |
| 5 | Limit type | limit_type | String |  | Invoke (I) limit or Approve (A) limit |

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
