# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api​/UserAccount​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_USER_ACCOUNT`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `/api​/UserAccount​/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  |  |
| 3 | User name | user_name | String |  |  |
| 4 | Login name | login_name | String |  |  |
| 5 | Branch name | branch_name | String |  |  |
| 6 | Department name | department_name | String |  |  |
| 7 | Status | user_account_status | String |  |  |
| 8 | Is online | is_online | String |  |  |
| 9 | Email | email | String |  |  |

**Example:**
```json
{

}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/UserAccount​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_USER_ACCOUNT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User code | user_code | No | String |  |  |  |
| 2 | User name | user_name | No | String |  |  |  |
| 3 | Login name | login_name | No | String |  |  |  |
| 4 | Branch name | branch_name | No | String |  |  |  |
| 5 | Department name | department_name | No | String |  |  |  |
| 6 | Status | user_account_status | No | String |  |  |  |
| 7 | Is online | is_online | No | String |  |  |  |
| 8 | Email | email | No | String |  |  |  |
| 9 | Page index | page_index | No | `Number` |  |  |  |
| 10 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  |  |
| 3 | User name | user_name | String |  |  |
| 4 | Login name | login_name | String |  |  |
| 5 | Branch name | branch_name | String |  |  |
| 6 | Department name | department_name | String |  |  |
| 7 | Status | user_account_status | String |  |  |
| 8 | Is online | is_online | String |  |  |
| 9 | Email | email | String |  |  |

**Example:**
```json
{

}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/UserAccount​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_USER_ACCOUNT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Email | email | No | String |  |  | is unique |
| 2 | Old user id | old_user_id | No | String |  |  |  |
| 3 | User name | user_name | `Yes` | String |  |  |  |
| 4 | Login name | login_name | `Yes` | String |  |  | is unique |
| 5 | Branch id | branch_id | `Yes` | `Number` |  |  |  |
| 6 | Department id | department_id | `Yes` | `Number` |  |  |  |
| 7 | Position | position | No | JSON Object |  |  |  |
|  | Cashier | cashier | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Officer | officer | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Chief cashier | chief_cashier | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Operation staff | operation_staff | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Dealer | dealer | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Inter-branch user | inter_branch_user | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Branch manager/authorized | branch_manager | No | `Number` |  |  | 1: check, 0: uncheck |
| 8 | Remark | remark | No | String |  |  |  |
| 9 | Status of this record | user_account_status | `Yes` | String |  | N |  |
| 10 | Main language | main_language | `Yes` | String |  | en |  |
| 11 | User phone | user_phone | No | String |  |  |  |
| 12 | Phone number | phone_number | No | JSON Object |  |  |  |
|  | Home | home | No | String |  |  |  |
|  | Office | office | No | String |  |  |  |
|  | Cell | cell | No | String |  |  |  |
|  | Facsimile | facsimile | No | String |  |  |  |
|  | Telex | telex | No | String |  |  |  |
| 13 | Time zone of user | time_zone | `Yes` | `Number` |  | 7 |  |
| 14 | Thousand separate character in amount field | thousand_separate_character | `Yes` | String |  | , |  |
| 15 | Decimal separate character in amount field | decimal_separate_character | `Yes` | String |  | . |  |
| 16 | Date format for short | date_format | `Yes` | String |  | D1 |  |
| 17 | Long date format | long_date_format | No | String |  |  |  |
| 18 | Time format | time_format | `Yes` | String |  | T1 |  |
| 19 | Expire date of this user | expire_date | No | `Date time` |  |  |  |
| 20 | Id of policy apply for this user | policy_id | `Yes` | `Number` |  |  |  |
| 21 | User can change password (Y/N) | pwdchg |  | String | 1 | Y | lấy giá trị mặc định |
| 22 | Password never expires (Y/N) | pwdexp |  | String | 1 | N | lấy giá trị mặc định |
| 23 | Require change password when login (Y/N) | pwdchgr |  | String | 1 | Y | lấy giá trị mặc định |
| 24 | Work station(s) can login | wsreg |  | String |  |  | không dùng |
| 25 | Enforce password history | enforce_password_history |  | `Number` |  | 3 | lấy giá trị theo policyid từ bảng s_usrpolicy |
| 26 | Maximum password age | maximum_password_age |  | `Number` |  | 180 | lấy giá trị theo policyid từ bảng s_usrpolicy |
| 27 | Minimum password age | minimum_password_age |  | `Number` |  | 0 | lấy giá trị theo policyid từ bảng s_usrpolicy |
| 28 | Minimum password length | minimum_password_length |  | `Number` |  | 6 | lấy giá trị theo policyid từ bảng s_usrpolicy |
| 29 | Password must meet complexity requirements | password_complexity_requirements |  | String |  | N | lấy giá trị theo policyid từ bảng s_usrpolicy |
| 30 | Can login from (hh:mm) | lginfr |  | String |  |  | không dùng |
| 31 | Can login to (hh:mm) | lginto |  | String |  |  | không dùng |
| 32 | Determines the number of minutes a locked-out account remains locked out before automatically becoming unlocked | lkoutdur |  | `Number` |  | 15 | lấy giá trị theo policyid từ bảng s_usrpolicy |
| 33 | Determines the number of failed logon attempts that causes a user account to be locked out | lkoutthrs |  | `Number` |  | 3 | lấy giá trị theo policyid từ bảng s_usrpolicy |
| 34 | Determines the number of minutes that must elapse after a failed logon attempt before the failed logon attempt counter is reset to 0 bad logon attempts | resetlkout |  | `Number` |  | 15 | lấy giá trị theo policyid từ bảng s_usrpolicy |
| 35 | User define field (Reserve), json structure | udfield1 |  | String |  |  | không dùng |
| 36 | Y/N.Y: user can process cash | iscash |  | String |  | N | không dùng |
| 37 | Company code | comcode |  | String |  |  | không dùng |
| 38 | Company type | comtype |  | String |  |  | không dùng |
| 39 | Is online | is_online | `Yes` | String | 1 | N | Lấy giá trị mặc định |

- Từ dòng 21 đến 39 lấy giá trị theo policyid từ bảng s_usrpolicy or lấy giá trị mặc định, không cho nhập dữ liệu, không cần để trong resquest body

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  | tự động sinh trong quá trình add |
| 3 | Old user id | old_user_id | String |  |  |
| 4 | User name | user_name | String |  |  |
| 5 | Login name | login_name | String |  |  |
| 6 | Branch id | branch_id | `Number` |  |  |
| 7 | Department id | department_id | `Number` |  |  |
| 8 | Position | position | JSON Object |  |  |
|  | Cashier | cashier | `Number` |  | 1: check, 0: uncheck |
|  | Officer | officer | `Number` |  | 1: check, 0: uncheck |
|  | Chief cashier | chief_cashier | `Number` |  | 1: check, 0: uncheck |
|  | Operation staff | operation_staff | `Number` |  | 1: check, 0: uncheck |
|  | Dealer | dealer | `Number` |  | 1: check, 0: uncheck |
|  | Inter-branch user | inter_branch_user | `Number` |  | 1: check, 0: uncheck |
|  | Branch manager/authorized | branch_manager | `Number` |  | 1: check, 0: uncheck |
| 9 | Remark | remark | String |  |  |
| 10 | Status of this record | user_account_status | String |  |  |
| 11 | Main language | main_language | String |  |  |
| 12 | User phone | user_phone | String |  |  |
| 13 | Phone number | phone_number | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
|  | Cell | cell | String |  |  |
|  | Facsimile | facsimile | String |  |  |
|  | Telex | telex | String |  |  |
| 14 | Time zone of user | time_zone | `Number` |  |  |
| 15 | Thousand separate character in amount field | thousand_separate_character | String |  |  |
| 16 | Decimal separate character in amount field | decimal_separate_character | String |  |  |
| 17 | Date format for short | date_format | String |  |  |
| 18 | Long date format | long_date_format | String |  |  |
| 19 | Time format | time_format | String |  |  |
| 20 | Expire date of this user | expire_date | `Date time` |  |  |
| 21 | Id of policy apply for this user | policy_id | `Number` |  |  |
| 22 | Email | email | String |  |  |
|  | Password | `reset_password` | String |  | lấy giá trị `reset_password` trong bảng `UserPasswordHistory` |
|  | Is online | is_online | String |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow
- Login name: không được trùng
- Email: không được trùng
- Giải nghĩa position khi migrate data: 
    - Cashier: C
    - Chief Cashier: I
    - Dealer: B
    - Operation staff: S
    - Inter-branch user: D
    - Branch manager/authorized: M
    - Officer: O

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api​/UserAccount​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_USER_ACCOUNT`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  | tự động sinh trong quá trình add |
| 3 | Old user id | old_user_id | String |  |  |
| 4 | User name | user_name | String |  |  |
| 5 | Login name | login_name | String |  |  |
| 6 | Branch id | branch_id | `Number` |  |  |
| 7 | Department id | department_id | `Number` |  |  |
| 8 | Position | position | JSON Object |  |  |
|  | Cashier | cashier | `Number` |  | 1: check, 0: uncheck |
|  | Officer | officer | `Number` |  | 1: check, 0: uncheck |
|  | Chief cashier | chief_cashier | `Number` |  | 1: check, 0: uncheck |
|  | Operation staff | operation_staff | `Number` |  | 1: check, 0: uncheck |
|  | Dealer | dealer | `Number` |  | 1: check, 0: uncheck |
|  | Inter-branch user | inter_branch_user | `Number` |  | 1: check, 0: uncheck |
|  | Branch manager/authorized | branch_manager | `Number` |  | 1: check, 0: uncheck |
| 9 | Remark | remark | String |  |  |
| 10 | Status of this record | user_account_status | String |  |  |
| 11 | Password | `reset_password` | String |  | lấy giá trị `reset_password` trong bảng `UserPasswordHistory` |
| 12 | Main language | main_language | String |  |  |
| 13 | User phone | user_phone | String |  |  |
| 14 | Phone number | phone_number | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
|  | Cell | cell | String |  |  |
|  | Facsimile | facsimile | String |  |  |
|  | Telex | telex | String |  |  |
| 15 | Time zone of user | time_zone | `Number` |  |  |
| 16 | Thousand separate character in amount field | thousand_separate_character | String |  |  |
| 17 | Decimal separate character in amount field | decimal_separate_character | String |  |  |
| 18 | Date format for short | date_format | String |  |  |
| 19 | Long date format | long_date_format | String |  |  |
| 20 | Time format | time_format | String |  |  |
| 21 | Expire date of this user | expire_date | `Date time` |  |  |
| 22 | Id of policy apply for this user | policy_id | `Number` |  |  |
| 23 | Email | email | String |  |  |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Department code | department_code | String |  | `trả thêm` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow
- Nút `Reset password` gọi thẳng API `Reset password`
- Điều kiện nút `Reset password`
    - Hiển thị khi field `reset_password` null
    - Không hiển thị khi field `reset_password` not null

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `/api​/UserAccount​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_USER_ACCOUNT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User id | id | `Yes` | `Number` |  |  |  |
| 2 | Old user id | old_user_id | No | String |  |  |  |
| 3 | User name | user_name | `Yes` | String |  |  |  |
| 4 | Login name | login_name | `Yes` | String |  |  | is unique |
| 5 | Branch id | branch_id | `Yes` | `Number` |  |  |  |
| 6 | Department id | department_id | `Yes` | `Number` |  |  |  |
| 7 | Position | position | No | JSON Object |  |  |  |
|  | Cashier | cashier | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Officer | officer | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Chief cashier | chief_cashier | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Operation staff | operation_staff | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Dealer | dealer | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Inter-branch user | inter_branch_user | No | `Number` |  |  | 1: check, 0: uncheck |
|  | Branch manager/authorized | branch_manager | No | `Number` |  |  | 1: check, 0: uncheck |
| 8 | Remark | remark | No | String |  |  |  |
| 9 | Status of this record | user_account_status | `Yes` | String |  | N |  |
| 10 | Main language | main_language | `Yes` | String |  | en |  |
| 11 | User phone | user_phone | No | String |  |  |  |
| 12 | Phone number | phone_number | No | JSON Object |  |  |  |
|  | Home | home | No | String |  |  |  |
|  | Office | office | No | String |  |  |  |
|  | Cell | cell | No | String |  |  |  |
|  | Facsimile | facsimile | No | String |  |  |  |
|  | Telex | telex | No | String |  |  |  |
| 13 | Time zone of user | time_zone | `Yes` | String |  | 7 |  |
| 14 | Thousand separate character in amount field | thousand_separate_character | `Yes` | String |  | , |  |
| 15 | Decimal separate character in amount field | decimal_separate_character | `Yes` | String |  | . |  |
| 16 | Date format for short | date_format | `Yes` | String |  | D1 |  |
| 17 | Long date format | long_date_format | No | String |  |  |  |
| 18 | Time format | time_format | `Yes` | String |  | T1 |  |
| 19 | Expire date of this user | expire_date | No | `Date` |  |  |  |
| 20 | Id of policy apply for this user | policy_id | `Yes` | String |  |  |  |
| 21 | Email | email | No | String |  |  | is unique |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |
| 2 | User code | user_code | String |  | tự động sinh trong quá trình add |
| 3 | Old user id | old_user_id | String |  |  |
| 4 | User name | user_name | String |  |  |
| 5 | Login name | login_name | String |  | is unique |
| 6 | Branch id | branch_id | `Number` |  |  |
| 7 | Department id | department_id | `Number` |  |  |
| 8 | Position | position | JSON Object |  |  |
|  | Cashier | cashier | `Number` |  | 1: check, 0: uncheck |
|  | Officer | officer | `Number` |  | 1: check, 0: uncheck |
|  | Chief cashier | chief_cashier | `Number` |  | 1: check, 0: uncheck |
|  | Operation staff | operation_staff | `Number` |  | 1: check, 0: uncheck |
|  | Dealer | dealer | `Number` |  | 1: check, 0: uncheck |
|  | Inter-branch user | inter_branch_user | `Number` |  | 1: check, 0: uncheck |
|  | Branch manager/authorized | branch_manager | `Number` |  | 1: check, 0: uncheck |
| 9 | Remark | remark | String |  |  |
| 10 | Status of this record | user_account_status | String |  |  |
| 11 | Main language | main_language | String |  |  |
| 12 | User phone | user_phone | String |  |  |
| 13 | Phone number | phone_number | JSON Object |  |  |
|  | Home | home | String |  |  |
|  | Office | office | String |  |  |
|  | Cell | cell | String |  |  |
|  | Facsimile | facsimile | String |  |  |
|  | Telex | telex | String |  |  |
| 14 | Time zone of user | time_zone | `Number` |  |  |
| 15 | Thousand separate character in amount field | thousand_separate_character | String |  |  |
| 16 | Decimal separate character in amount field | decimal_separate_character | String |  |  |
| 17 | Date format for short | date_format | String |  |  |
| 18 | Long date format | long_date_format | String |  |  |
| 19 | Time format | time_format | String |  |  |
| 20 | Expire date of this user | expire_date | `Date time` |  |  |
| 21 | Id of policy apply for this user | policy_id | `Number` |  |  |
| 22 | Email | email | String |  |is unique |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow
- Login name: không được trùng
- Email: không được trùng

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `/api​/UserAccount​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_USER_ACCOUNT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 2
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |

**Example:**
```json
{
    "id": 2
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Reset password
## 7.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `UMG_RESET_PASSWORD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |
| 2 | Reset password | reset_password | String |  | PWDRESET trong bảng s_usrpwd_his |
| 4 | User name | user_name | String |  | `trả thêm` |
| 5 | Login name | login_name | String |  | `trả thêm` |

**Example:**
```json
{
    
}
```

## 7.2 Transaction flow
- Trên UI khi click nút reset passwword sẽ thực hiện reset tức thì.
- Khi reset thành công, update reset_password trong bảng UserPasswordHistory thành null

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 8. Logout user
## 8.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `UMG_LOGOUT_USER`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User id | id | `Yes` | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Logout | logout | String |  |  |

**Example:**
```json
{
    
}
```

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 9. Approve user
## 9.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** ``

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User id | id | `Yes` | `Number` |  |  | số nguyên dương |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User id | id | `Number` |  |  |
| 2 | Status of this record | user_account_status | String |  |  |

**Example:**
```json
{
    
}
```

## 9.2 Transaction flow

## 9.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 10. Update password
## 10.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `UMG_UPDATE_PASSWORD`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Old password | old_password | `Yes` | String |  |  |  |
| 2 | New password | new_password | `Yes` | String |  |  |  |
| 3 | Confirm new password | confirm_password | `Yes` | String |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Update | update | String |  |  |

**Example:**
```json
{
    
}
```

## 10.2 Transaction flow

## 10.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |