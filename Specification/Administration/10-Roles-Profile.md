# =========== Role Profile - Invoke - Approve ===========
# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/UserRight​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_USER_RIGHT`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page index | page_index | No | `Number` |  |  |  |
| 3 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Command id detail | command_id_detail | String |  |  |
| 4 | Invoke | invoke | `Number` |  |  |
| 5 | Approve | approve | `Number` |  |  |
| 6 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/UserRight​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_USER_RIGHT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | No | `Number` |  |  |  |
| 2 | Commands id | command_id | No | String |  |  |  |
| 3 | Command id detail | command_id_detail | No | String |  |  |  |
| 4 | Invoke | invoke | No | `Number` |  |  |  |
| 5 | Approve | approve | No | `Number` |  |  |  |
| 6 | Page index | page_index | No | `Number` |  |  |  |
| 7 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Command id detail | command_id_detail | String |  |  |
| 4 | Invoke | invoke | `Number` |  |  |
| 5 | Approve | approve | `Number` |  |  |
| 6 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/UserRight​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_USER_RIGHT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 2 | Commands id | command_id | `Yes` | String |  |  | from cha |
| 3 | Command id detail | command_id_detail | `Yes` | String |  | 0 | 0: là phân quyền cho form cha không có form con |
| 4 | Invoke | invoke | `Yes` | `Number` |  | 0 | 1: có quyền, 0: không có quyền |
| 5 | Approve | approve | `Yes` | `Number` |  | 0 | 1: có quyền, 0: không có quyền |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Command id detail | command_id_detail | String |  |  |
| 4 | Invoke | invoke | `Number` |  |  |
| 5 | Approve | approve | `Number` |  |  |
| 6 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api​/UserRight​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_USER_RIGHT`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User right id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Command id detail | command_id_detail | String |  |  |
| 4 | Invoke | invoke | `Number` |  |  |
| 5 | Approve | approve | `Number` |  |  |
| 6 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** ​`​/api​/UserRight​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_USER_RIGHT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 2 | Commands id | command_id | `Yes` | String |  |  | from cha |
| 3 | Command id detail | command_id_detail | `Yes` | String |  | 0 | 0: là phân quyền cho form cha không có form con |
| 4 | Invoke | invoke | `Yes` | `Number` |  | 0 | 1: có quyền, 0: không có quyền |
| 5 | Approve | approve | `Yes` | `Number` |  | 0 | 1: có quyền, 0: không có quyền |
| 6 | User right id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Command id detail | command_id_detail | String |  |  |
| 4 | Invoke | invoke | `Number` |  |  |
| 5 | Approve | approve | `Number` |  |  |
| 6 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `​/api​/UserRight​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_USER_RIGHT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | User right id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list commands 
## 7.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_USER_RIGHT`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page index | page_index | No | `Number` |  |  |  |
| 3 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Command id detail | command_id_detail | String |  |  |
| 4 | Invoke | invoke | `Number` |  |  |
| 5 | Approve | approve | `Number` |  |  |
| 6 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 7.2 Transaction flow

## 7.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 8. Get list user right by commands id
## 8.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_USER_RIGHT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | No | `Number` |  |  |  |
| 2 | Commands id | command_id | No | String |  |  |  |
| 3 | Command id detail | command_id_detail | No | String |  |  |  |
| 4 | Invoke | invoke | No | `Number` |  |  |  |
| 5 | Approve | approve | No | `Number` |  |  |  |
| 6 | Page index | page_index | No | `Number` |  |  |  |
| 7 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Command id detail | command_id_detail | String |  |  |
| 4 | Invoke | invoke | `Number` |  |  |
| 5 | Approve | approve | `Number` |  |  |
| 6 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 8.2 Transaction flow

## 8.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 9. Get list user right by commands id detail
## 9.1 Field description
### Request message
**HTTP Method:** 

**URL:** 

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_USER_RIGHT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | No | `Number` |  |  |  |
| 2 | Commands id | command_id | No | String |  |  |  |
| 3 | Command id detail | command_id_detail | No | String |  |  |  |
| 4 | Invoke | invoke | No | `Number` |  |  |  |
| 5 | Approve | approve | No | `Number` |  |  |  |
| 6 | Page index | page_index | No | `Number` |  |  |  |
| 7 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Command id detail | command_id_detail | String |  |  |
| 4 | Invoke | invoke | `Number` |  |  |
| 5 | Approve | approve | `Number` |  |  |
| 6 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 9.2 Transaction flow

## 9.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 10. Update user right (Add) theo mảng
## 10.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_LIST_USER_RIGHT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
|  | List user right | list_user_right  |  | Array Object |  |  |  |
| 1 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 2 | Commands id | command_id | `Yes` | String |  |  | from cha |
| 3 | Command id detail | command_id_detail | `Yes` | String |  | 0 | 0: là phân quyền cho form cha không có form con |
| 4 | Invoke | invoke | `Yes` | `Number` |  | 0 | 1: có quyền, 0: không có quyền |
| 5 | Approve | approve | `Yes` | `Number` |  | 0 | 1: có quyền, 0: không có quyền |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | List user right | list_user_right | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Commands id | command_id | String |  |  |
| 3 | Command id detail | command_id_detail | String |  |  |
| 4 | Invoke | invoke | `Number` |  |  |
| 5 | Approve | approve | `Number` |  |  |
| 6 | User right id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 10.2 Transaction flow

## 10.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# =========== Role Profile - Add - Remove ===========
# 1. Copy role
## 1.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api/UserRole/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_USER_ROLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role name | role_name | `Yes` | String |  |  |  |
| 2 | Role status | user_role_status | `Yes` | String |  | N | lấy dữ liệu mặc định trong quá trình add |
| 3 | Role id copy | role_template_id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role name | role_name | String |  |  |
| 2 | Role status | user_role_status | String |  | lấy dữ liệu mặc định (N) trong quá trình add |
| 3 | Role id | id | `Number` |  | số nguyên dương |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 2. Delete role
## 2.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `​/api​/UserRole​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_USER_ROLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 3. Get list roles
## 3.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/UserRole​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_USER_ROLE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page index | page_index | No | `Number` |  |  |  |
| 3 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role name | role_name | String |  |  |
| 2 | Role status | user_role_status | String |  |  |
| 3 | Role id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/UserRole​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_USER_ROLE`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role name | role_name | String |  |  |
| 2 | Role status | user_role_status | String |  |  |
| 3 | Role id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# =========== Role Profile - User - Assignment ===========
# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/RoleOfUser​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_ROLE_OF_USER`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page index | page_index | No | `Number` |  |  |  |
| 3 | Page size | page_size | No | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Role name | role_name | String |  |  |
| 3 | User id | usr_id | `Number` |  |  |
| 4 | User code | usr_code | String |  |  |
| 5 | User name | usr_name | String |  |  |
| 6 | Role of user​ id | id | `Number` |  |  |
| 7 | Login name | login_name | String |  | `Trả thêm` |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/RoleOfUser​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_ROLE_OF_USER`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | No | `Number` |  |  |  |
| 2 | User id | usr_id | No | `Number` |  |  |  |
| 3 | Page index | page_index | No | `Number` |  |  |  |
| 4 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | Role name | role_name | String |  |  |
| 3 | User id | usr_id | `Number` |  |  |
| 4 | User code | usr_code | String |  |  |
| 5 | User name | usr_name | String |  |  |
| 6 | Role of user​ id | id | `Number` |  |  |
| 7 | Login name | login_name | String |  | `Trả thêm` |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Assign user for role (Add)
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/RoleOfUser​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_ROLE_OF_USER`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 2 | User id | usr_id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | User id | usr_id | `Number` |  |  |
| 3 | Role of user​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api​/RoleOfUser​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_ROLE_OF_USER`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role of user​ id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | User id | usr_id | `Number` |  |  |
| 3 | Role of user​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** ​`/api​/RoleOfUser​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_ROLE_OF_USER`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role of user​ id | id | `Yes` | `Number` |  |  |  |
| 2 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 3 | User id | usr_id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | User id | usr_id | `Number` |  |  |
| 3 | Role of user​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `/api​/RoleOfUser​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_ROLE_OF_USER`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Role of user id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | User id | usr_id | `Number` |  |  |
| 3 | Role of user​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list users by role id
- [Search advanced của "Role Profile - User - Assignment"](Specification/Administration/10 Roles Profile)

# 8. Get list roles by user id
- [Search advanced của "Role Profile - User - Assignment"](Specification/Administration/10 Roles Profile)

# 9. Assign user for role (Add) theo mảng
## 9.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_LIST_ROLE_OF_USER`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
|  | List role of user | list_role_of_user  |  | Array Object |  |  |  |
| 1 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 2 | User id | usr_id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | List role of user | list_role_of_user | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | User id | usr_id | `Number` |  |  |
| 3 | Role of user​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 9.2 Transaction flow

## 9.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 10. Delete theo mảng
## 10.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** ``

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_LIST_ROLE_OF_USER`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
|  | List role of user | list_role_of_user  |  | Array Object |  |  |  |
| 1 | Role id | role_id | `Yes` | `Number` |  |  |  |
| 2 | User id | usr_id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | List role of user | list_role_of_user | Array Object |  |  |
| 1 | Role id | role_id | `Number` |  |  |
| 2 | User id | usr_id | `Number` |  |  |
| 3 | Role of user​ id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 10.2 Transaction flow

## 10.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |