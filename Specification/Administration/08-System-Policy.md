# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `/api​/UserPolicy​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_SIMPLE_SEARCH_USER_POLICY`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Policy id | id | `Number` |  |  |
| 2 | Description of policy | descr | String |  |  |
| 3 | Effective from | effective_from | `Date time` |  |  |
| 4 | Effective to | effective_to | `Date time` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** ​`/api​/UserPolicy​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_ADVANCED_SEARCH_USER_POLICY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Policy id | id | No | `Number` |  |  |  |
| 2 | Description of policy | descr | No | String |  |  |  |
| 3 | Effective from | effective_from | No | `Date time` |  |  |  |
| 5 | Effective to | effective_to | No | `Date time` |  |  |  |
| 7 | Page index | page_index | No | `Number` |  |  |  |
| 8 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Policy id | id | `Number` |  |  |
| 2 | Description of policy | descr | String |  |  |
| 3 | Effective from | effective_from | `Date time` |  |  |
| 4 | Effective to | effective_to | `Date time` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/UserPolicy​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_INSERT_USER_POLICY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Description of policy | descr | `Yes` | String |  |  |  |
| 2 | Effective from | effective_from | `Yes` | `Date time` |  |  |  |
| 3 | Effective to | effective_to | No | `Date time` |  |  |  |
| 4 | Enforce password history | enforce_password_history | No | `Number` |  | 3 |  |
| 5 | Maximum password age (0=unlimit) | maximum_password_age | No | `Number` |  | 180 |  |
| 6 | Minimum password length | minimum_password_length | No | `Number` |  | 0 |  |
| 7 | Password must meet complexity requirements | password_complexity_requirements | `Yes` | String |  | Y |  |
| 8 | At least symbol character (\`~!@#$%^&*()-_=+[]{}\|;:'\",<.>/?) | password_have_special_symbol | `Yes` | String |  | Y |  |
| 9 | At least one upper case letter | password_have_upper_case | `Yes` | String |  | Y |  |
| 10 | At least one lower case letter | password_have_lower_case | `Yes` | String |  | Y |  |
| 11 | At least one number | password_have_number | `Yes` | String |  | Y |  |
| 12 | Can login from | can_login_from | No | String |  |  |  |
| 13 | Can login to | can_login_to | No | String |  |  |  |
| 14 | The number of failed logon attempts (0=unlimit) | lockout_tthrs | No | `Number` |  | 3 |  |
| 15 | Determines the number of minutes a locked-out account remains locked out before automatically becoming unlocked | lkoutdur | No | `Number` |  | 15 |  |
| 16 | Determines the number of minutes that must elapse after a failed logon attempt before the failed logon attempt counter is reset to 0 bad logon attempts | resetlkout | No | `Number` |  | 15 |  |
| 17 | Minimum password age | pwdagemin | No | `Number` |  | 0 |  |
| 18 |  | pwdcplxsn | No | String |  | Y |  |
| 19 |  | pwdcplxru | No | String |  | Y |  |

- Từ dòng 15 đến 19 lấy giá trị mặc định khi xử lý add record, không cho nhập dữ liệu, không cần để trong resquest body

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Policy id |  | `Number` |  |  |  |
| 2 | Description of policy | descr | String |  |  |  |
| 3 | Effective from | effective_from | `Date time` |  |  |  |
| 4 | Effective to | effective_to | `Date time` |  |  |  |
| 5 | Enforce password history | enforce_password_history | `Number` |  |  |  |
| 6 | Maximum password age (0=unlimit) | maximum_password_age | `Number` |  |  |  |
| 7 | Minimum password length | minimum_password_length | `Number` |  |  |  |
| 8 | Password must meet complexity requirements | password_complexity_requirements | String |  |  |  |
| 9 | At least symbol character (`~!@#$%^&*()-_=+[]{}\|;:'\",<.>/?) | password_have_special_symbol | String |  |  |  |
| 10 | At least one upper case letter | password_have_upper_case | String |  |  |  |
| 11 | At least one lower case letter | password_have_lower_case | String |  |  |  |
| 12 | At least one number | password_have_number | String |  |  |  |
| 13 | Can login from | can_login_from | String |  |  |  |
| 14 | Can login to | can_login_to | String |  |  |  |
| 15 | The number of failed logon attempts (0=unlimit) | lockout_tthrs | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View by id
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** ​`/api​/UserPolicy​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_VIEW_USER_POLICY`

**Parameters:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Policy id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Policy id | id | `Number` |  |  |
| 2 | Description of policy | descr | String |  |  |
| 3 | Effective from | effective_from | `Date time` |  |  |
| 4 | Effective to | effective_to | `Date time` |  |  |
| 5 | Enforce password history | enforce_password_history | `Number` |  |  |
| 6 | Maximum password age (0=unlimit) | maximum_password_age | `Number` |  |  |
| 7 | Minimum password length | minimum_password_length | `Number` |  |  |
| 8 | Password must meet complexity requirements | password_complexity_requirements | String |  |  |
| 9 | At least symbol character (\`~!@#$%^&*()-_=+[]{}\|;:'\",<.>/?) | password_have_special_symbol | String |  |  |
| 10 | At least one upper case letter | password_have_upper_case | String |  |  |
| 11 | At least one lower case letter | password_have_lower_case | String |  |  |
| 12 | At least one number | password_have_number | String |  |  |
| 13 | Can login from | can_login_from | String |  |  |
| 14 | Can login to | can_login_to | String |  |  |
| 15 | The number of failed logon attempts (0=unlimit) | lockout_tthrs | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `/api​/UserPolicy​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_UPDATE_USER_POLICY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Policy id | id | `Yes` | `Number` |  |  |  |
| 2 | Description of policy | descr | `Yes` | String |  |  |  |
| 3 | Effective from | effective_from | `Yes` | `Date time` |  |  |  |
| 4 | Effective to | effective_to | No | `Date time` |  |  |  |
| 5 | Enforce password history | enforce_password_history | No | `Number` |  | 3 |  |
| 6 | Maximum password age (0=unlimit) | maximum_password_age | No | `Number` |  | 180 |  |
| 7 | Minimum password length | minimum_password_length | No | `Number` |  | 0 |  |
| 8 | Password must meet complexity requirements | password_complexity_requirements | `Yes` | String |  | Y |  |
| 9 | At least symbol character (\`~!@#$%^&*()-_=+[]{}\|;:'\",<.>/?) | password_have_special_symbol | `Yes` | String |  | Y |  |
| 10 | At least one upper case letter | password_have_upper_case | `Yes` | String |  | Y |  |
| 11 | At least one lower case letter | password_have_lower_case | `Yes` | String |  | Y |  |
| 12 | At least one number | password_have_number | `Yes` | String |  | Y |  |
| 13 | Can login from | can_login_from | No | String |  |  |  |
| 14 | Can login to | can_login_to | No | String |  |  |  |
| 15 | The number of failed logon attempts (0=unlimit) | lockout_tthrs | No | `Number` |  | 3 |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Policy id |  | `Number` |  |  |  |
| 2 | Description of policy | descr | String |  |  |  |
| 3 | Effective from | effective_from | `Date time` |  |  |  |
| 4 | Effective to | effective_to | `Date time` |  |  |  |
| 5 | Enforce password history | enforce_password_history | `Number` |  |  |  |
| 6 | Maximum password age (0=unlimit) | maximum_password_age | `Number` |  |  |  |
| 7 | Minimum password length | minimum_password_length | `Number` |  |  |  |
| 8 | Password must meet complexity requirements | password_complexity_requirements | String |  |  |  |
| 9 | At least symbol character (\`~!@#$%^&*()-_=+[]{}\|;:'\",<.>/?) | password_have_special_symbol | String |  |  |  |
| 10 | At least one upper case letter | password_have_upper_case | String |  |  |  |
| 11 | At least one lower case letter | password_have_lower_case | String |  |  |  |
| 12 | At least one number | password_have_number | String |  |  |  |
| 13 | Can login from | can_login_from | String |  |  |  |
| 14 | Can login to | can_login_to | String |  |  |  |
| 15 | The number of failed logon attempts (0=unlimit) | lockout_tthrs | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** ​`/api​/UserPolicy​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `ADM_DELETE_USER_POLICY`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Policy id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Policy id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |