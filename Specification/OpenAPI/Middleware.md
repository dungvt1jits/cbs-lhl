# 1. Thực hiện Payment Cheque
**Workflow id:** `DPT_PAYMENT_CHEQUE`

## 1.1 Request message
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Posting Entries | posting_entries | `Yes` | Array Object |  |  |  |
|  | Account number | account_number | `Yes` | String | 25 |  | có thể nhập `Deposit account number` or `GL account number` |
|  | Prefix | prefix | No | String |   |  | hai kí tự prefix của cheque đã issue cho deposit account number |
|  | Cheque number | cheque_number | No | String |   |  | cheque number (không có prefix) đã issue cho deposit account number |
|  | Currency | currency_code | `Yes` | String | 3 |  |  |
|  | Amount | amount | `Yes` | `Number` |  |  |  |
|  | Credit or Debit | credit_or_debit | `Yes` | String | 1 |  | D: Debit, C: Credit |
|  | Narrative | narrative | No | String | 250 |  | Desc của giao dịch |
|  | Group id | group_id | `Yes` | `Number` |  |  | số nguyên, dùng để xác định cặp posting, `thêm mới` |
| 2 | Source branch code | source_branch_code | No | String | 4 |  |  |
| 3 | Source id | source_id | No | String | 50 |  |  |
| 4 | Transaction reference mid | transaction_reference_mid | No | String | 50 |  |  |
| 5 | Value date | value_date | No | `Date time` |  |  |  |

## 1.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Source id | source_id | String | 50 |  |
| 2 | Channel Id | channel_id | String | 50 | chưa biết lấy giá trị ở đâu, trả về key trước, trả giá trị sau |
| 3 | Transaction reference mid | transaction_reference_mid | String | 50 |  |
| 4 | Transaction date | transaction_date | `Date time` |  |  |
| 5 | Reference id | reference_id | String | 50 |  |
- Response message ở step 2 có thể trả giống request message và thêm 3 fields `channel_id`, `transaction_date`, `reference_id`

## 1.3 Transaction flow
- Vẫn trả về posting ở step posting như workflow bình thường, sau đó cấu hình để trả trong data giống Digital.
- Cho phép khác branch đảm bảo posting IBT đúng.
- Chỉ cho phép thực hiện cùng currency.
- Tổng amount debit = amount credit trong Posting Entries, Posting Entries Fee
- `cheque_number` và `prefix` đã được issue cho deposit account nếu `account_number` là deposit account number, có trạng thái hợp lệ.
- `cheque_number` khi request message không bao gồm cả prefix, chỉ number. Nếu tồn tại `cheque_number` nhiều hơn 1, sắp xếp prefix theo alphabet, lấy `cheque_number` có prefix đầu tiên.
- Cập nhật thông tin `cheque_number` sau khi giao dịch thành công tương tự với giao dịch DPT_CDT (trường hợp cheque thuộc Debit)
- Verify và thông báo lỗi:
  - Account number không tồn tại
  - Khác currency
  - Amount debit và credit không bằng nhau
  - Những trường nào để trống mà require
  - Đối với cheque thuộc Debit
    - `cheque_number` và `prefix` không tồn tại
    - `cheque_number` và `prefix` không được issue cho deposit account nếu `account_number` là deposit account number.
    - `cheque_number` và `prefix` có trạng thái không hợp lệ (<> Unpaid)
  - Tài khoản debit là deposit có status là Normal
  - Tài khoản credit là deposit có status là New/ Normal
  - Posting side của GL không hợp lệ
  - Amount <= 0
- Các cặp posting theo `group_id` giống nhau phải đảm bảo các điều kiện posting
- Cho phép thực hiện reverse với OpenAPI đã cung cấp cho Digital
  - Kiểm tra dữ liệu của Deposit sau khi reverse
  - Kiểm tra dữ liệu của GL sau khi reverse
  - Kiểm tra dữ liệu của Cheque sau khi reverse


# 2. 