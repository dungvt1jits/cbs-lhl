# 1. Lấy dữ liệu list exchange rate cho Digital banking

**Workflow id:** `FX_GET_LIST_EXCHANGE_RATE`

## 1.1 Request message
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Credit currency code | credit_currency_code | `Yes`| String | 3 |  |  |

## 1.2 Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | List exchange rate | list_exchange_rate | JSON Object |  |  |
| 1 | Debit currency code | debit_currency_code | String | 3 |  |
| 2 | Credit currency code | credit_currency_code | String | 3 |  |
| 3 | Buy rate debit currency code | buy_rate_debit_currency | `Number` |  | số có 9 số thập phân, `CB` |
| 4 | Sell rate credit currency code | sell_rate_credit_currency | `Number` |  | số có 9 số thập phân, `CA` |
| 5 | Exchange date | exchange_date | `Date time` |  | lấy giá trị `ValueDate` trong bảng `ForeignExchangeRate` |

## 1.3 Transaction flow
Tham khảo từ workflow `FX_RULEFUNC_GET_FX_BCYRATE`.
- FX_RULEFUNC_GET_FX_BCYRATE: gửi đi rate_type tương ứng `CB` cho `debit` currency và `CA` cho `credit` currency
- branch_id_fx lấy giá trị HO (head office).
- exchange_date lấy giá trị mới nhất.
- debit_currency_code là các currency còn lại trong có trong bảng fx rate

