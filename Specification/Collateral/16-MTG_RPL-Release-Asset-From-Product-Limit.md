# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `MTG_RPL`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Collateral account | collateral_account | `Yes` | String | 25 |  |  | CMRGAC |
| 2 | Collateral account currency | collateral_account_currency | No | String | 3 |  |  | CCCR |
| 3 | Collateral booking value | collateral_booking_value | `Yes` | `Number` |  | 0 |  | CMGNBAMT |
| 4 | Collateral secure amount in use | collateral_secure_amount_in_use | No | `Number` |  | 0 |  | CAMT3 |
| 5 | Product limit code | product_limit_code | `Yes` | String |  |  |  | PLCD |
| 6 | Product limit currency | product_limit_currency | No | String | 3 |  |  | RCCR |
| 7 | Product limit amount | product_limit_amount | No | `Number` |  | 0 |  | TXAMT |
| 8 | Required secure amount | required_secure_amount | No | `Number` |  | 0 |  | CAMT4 |
| 9 | Total secured amount | total_secured_amount | No | `Number` |  | 0 |  | CAMT5 |
| 10 | Release amount from collateral | release_amount_from_collateral | `Yes` | `Number` |  | > 0 |  | CAMT |
| 11 | Release amount from product limit | release_amount_from_product_limit | `Yes` | `Number` |  | > 0 |  | PCSAMT |
| 12 | Exchange rate of collateral currency | exchange_rate_of_collateral_currency | No | `Number` |  | 0 |  | CRATE |
| 13 | Exchange rate of product limit currency | exchange_rate_of_product_limit_currency | No | `Number` |  | 0 |  | ORGRATE |
| 14 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 15 | Customer name | customer_name | `Yes` | String | 250 |  |  | CCTMA |
| 16 | Customer address | customer_address | No | String | 250 |  |  | CIDPLACE |
| 17 | Customer description | customer_description | No | String | 250 |  |  | MDESC |
| 18 | Description | description | No | String | 250 |  |  | DESCS |
| 19 | Cross rate | cross_rate | No | `Number` |  | 0 | trường ẩn | CCRRATE |
| 20 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | CVLDT |
| 21 | Base amount | base_amount | No | `Number` |  | 0 | trường ẩn | CBAMT |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Collateral account | collateral_account | String |  |  |
| 3 | Collateral account currency | collateral_account_currency | String |  |  |
| 4 | Collateral booking value | collateral_booking_value | `Number` |  |  |
| 5 | Collateral secure amount in use | collateral_secure_amount_in_use | `Number` |  |  |
| 6 | Product limit code | product_limit_code | String |  |  |
| 7 | Product limit currency | product_limit_currency | String |  |  |
| 8 | Product limit amount | product_limit_amount | `Number` |  |  |
| 9 | Required secure amount | required_secure_amount | `Number` |  |  |
| 10 | Total secured amount | total_secured_amount | `Number` |  |  |
| 11 | Release amount from collateral | release_amount_from_collateral | `Number` |  |  |
| 12 | Release amount from product limit | release_amount_from_product_limit | `Number` |  |  |
| 13 | Exchange rate of collateral currency | exchange_rate_of_collateral_currency | `Number` |  |  |
| 14 | Exchange rate of product limit currency | exchange_rate_of_product_limit_currency | `Number` |  |  |
| 15 | Customer code | customer_code | String |  |  |
| 16 | Customer name | customer_name | String |  |  |
| 17 | Customer address | customer_address | String |  |  |
| 18 | Customer description | customer_description | String |  |  |
| 19 | Description | description | String |  |  |
| 20 | Cross rate | cross_rate | `Number` |  |  |
| 21 | Value date | value_date | `Date time` |  |  |
| 22 | Base amount | base_amount | `Number` |  |  |
| 23 | Transaction status | status | String |  |  |
| 24 | Transaction date |  | `Date time` |  |  |
| 25 | User id |  | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- Collateral account exists with status is Normal
- Product limit code exists was secured before.
- 0.00 < Release amount in collateral currency (`release_amount_from_collateral`) =< Total secured amount
  - In which:
    - Total secured amount = `total_secured_amount` nhập từ request message = công thức workflow `MTG_PL_GET_INFO_CAMT5`

**Flow of events:**
- Release value that was secured before.
- Release amount in collateral currency = Release amount * Exchange rate.
- Exchange rate = Book rate (Product limit currency) / Book rate (Collateral currency).
- Released Collateral amount (new) = Released Collateral amount (old) + Release amount in Collateral currency.
- Current secure amount (new) = Current secure amount (old) - Release amount in Collateral currency.

**Database:**


**Voucher:**
- None

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Collateral account | collateral_account | String |  |  |
| 3 | Collateral account currency | collateral_account_currency | String |  |  |
| 4 | Collateral booking value | collateral_booking_value | `Number` |  |  |
| 5 | Collateral secure amount in use | collateral_secure_amount_in_use | `Number` |  |  |
| 6 | Product limit code | product_limit_code | String |  |  |
| 7 | Product limit currency | product_limit_currency | String |  |  |
| 8 | Product limit amount | product_limit_amount | `Number` |  |  |
| 9 | Required secure amount | required_secure_amount | `Number` |  |  |
| 10 | Total secured amount | total_secured_amount | `Number` |  |  |
| 11 | Release amount from collateral | release_amount_from_collateral | `Number` |  |  |
| 12 | Release amount from product limit | release_amount_from_product_limit | `Number` |  |  |
| 13 | Exchange rate of collateral currency | exchange_rate_of_collateral_currency | `Number` |  |  |
| 14 | Exchange rate of product limit currency | exchange_rate_of_product_limit_currency | `Number` |  |  |
| 15 | Customer code | customer_code | String |  |  |
| 16 | Customer name | customer_name | String |  |  |
| 17 | Customer address | customer_address | String |  |  |
| 18 | Customer description | customer_description | String |  |  |
| 19 | Description | description | String |  |  |
| 20 | Cross rate | cross_rate | `Number` |  |  |
| 21 | Value date | value_date | `Date time` |  |  |
| 22 | Base amount | base_amount | `Number` |  |  |
| 23 | Transaction status | status | String |  |  |
| 24 | Transaction date |  | `Date time` |  |  |
| 25 | User id |  | `Number` |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_MORTGAGE | GET_INFO_MTGACC | [GET_INFO_MTGACC](Specification/Common/14 Mortgage Rulefuncs) |
| 2 | GET_SIG_CMRGAC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_SCRAMT | MTG_RPL_GET_INFO_SCRAMT | [MTG_RPL_GET_INFO_SCRAMT](Specification/Common/14 Mortgage Rulefuncs) lấy giá trị `secured_amount` trả về gán vào `required_secure_amount` |
| 4 | GET_INFO_CAMT_PCSAMT `CAMT, CRATE, PCSAMT, ORGRATE` | MTG_RPL_GET_INFO_CAMT_PCSAMT | [MTG_RPL_GET_INFO_CAMT_PCSAMT](Specification/Common/14 Mortgage Rulefuncs) map tương ứng `collateral_amount_default` gán cho `release_amount_from_collateral`, `collateral_amount_in_use` gán cho `exchange_rate_of_collateral_currency`, `product_limit_amount_default` gán cho `release_amount_from_product_limit`, `product_limit_amount_in_use` gán cho `exchange_rate_of_product_limit_currency` |
| 5 | GET_INFO_PL | MTG_GET_INFO_PL | [MTG_GET_INFO_PL](Specification/Common/14 Mortgage Rulefuncs) |
| 6 | GET_INFO_CAMT5 | MTG_PL_GET_INFO_CAMT5 | [MTG_PL_GET_INFO_CAMT5](Specification/Common/14 Mortgage Rulefuncs) |
| 7 | LKP_DATA_PLCD | MTG_RPL_LOOKUP_CRDPL | [MTG_RPL_LOOKUP_CRDPL](Specification/Common/14 Mortgage Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>