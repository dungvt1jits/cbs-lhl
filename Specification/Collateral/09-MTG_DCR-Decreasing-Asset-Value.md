# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `MTG_DCR`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | CMRGAC |
| 2 | Account holder name | account_holder_name | `Yes` | String | 250 |  |  | CACNM |
| 3 | Asset value | asset_value | `Yes` | `Number` |  | 0 | số có hai số thập phân | CMGNBAMT |
| 4 | Decreasing value | decreasing_value | `Yes` | `Number` |  | > 0 | số có hai số thập phân | CAMT3 |
| 5 | Asset booking value | asset_booking_value | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT |
| 6 | Decreasing booking value | decreasing_booking_value | `Yes` | `Number` |  | > 0 | số có hai số thập phân | CAMT2 |
| 7 | Secured amount | secured_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT4 |
| 8 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 9 | Customer name | customer_name | `Yes` | String | 250 |  |  | CRNAME |
| 10 | Customer address | customer_address | No | String | 250 |  |  | CCTMA |
| 11 | Customer description | customer_description | No | String | 250 |  |  | MDESC |
| 12 | Description | description | No | String | 250 |  |  | DESCS |
| 13 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | CVLDT |
| 14 | Currency | currency | No | String | 3 |  | trường ẩn | CCCR |
| 15 | Check limit | check_limit | `Yes` | `Number` |  | 0 | trường ẩn | CAMT5 |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Account holder name | account_holder_name | String |  |  |
| 4 | Asset value | asset_value | `Number` |  | số có hai số thập phân |
| 5 | Decreasing value | decreasing_value | `Number` |  | số có hai số thập phân |
| 6 | Asset booking value | asset_booking_value | `Number` |  | số có hai số thập phân |
| 7 | Decreasing booking value | decreasing_booking_value | `Number` |  | số có hai số thập phân |
| 8 | Secured amount | secured_amount | `Number` |  | số có hai số thập phân |
| 9 | Customer code | customer_code | String |  |  |
| 10 | Customer name | customer_name | String |  |  |
| 11 | Customer address | customer_address | String |  |  |
| 12 | Customer description | customer_description | String |  |  |
| 13 | Description | description | String |  |  |
| 14 | Value date | value_date | `Date time` |  |  |
| 15 | Currency | currency | String |  |  |
| 16 | Check limit | check_limit | `Number` |  |  |
| 17 | Transaction status |  | String |  |  |
| 18 | Transaction date |  | `Date time` |  |  |
| 19 | User id |  | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Account number": exists with status "Normal".
- "Asset value" > 0.00 or "Book value" > 0.00
- "Decreasing value" > 0.00
- "Decreasing booking value" > 0.00
- "Secured amount" < or = {"Asset value" - "Decreasing value"}
- "Secured amount" < or = {"Asset booking value" - "Decreasing booking value"}

**Flow of events:**
- Transaction complete: 
    - Book value will decrease. If adjust decreasing book value.
    - Asset value will decrease. If adjust decreasing asset value.

**Database:**

**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references |  | String |  |  |  |
| 2 | Account number |  | String |  |  |  |
| 3 | Account holder name |  | String |  |  |  |
| 4 | Asset value |  | `Number` |  |  | số có hai số thập phân |
| 5 | Decreasing value |  | `Number` |  |  | số có hai số thập phân |
| 6 | Asset booking value |  | `Number` |  |  | số có hai số thập phân |
| 7 | Decreasing booking value |  | `Number` |  |  | số có hai số thập phân |
| 8 | Secured amount |  | `Number` |  |  | số có hai số thập phân |
| 9 | Customer code |  | String |  |  |  |
| 10 | Customer name |  | String |  |  |  |
| 11 | Customer address |  | String |  |  |  |
| 12 | Customer description |  | String |  |  |  |
| 13 | Description |  | String |  |  |  |
| 14 | Transaction date |  | `Date time` |  |  |  |
| 15 | User id |  | String |  |  |  |
| 16 | Transaction status |  | String |  |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_CMRGAC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_MAVB | GET_INFO_MTGACC | [GET_INFO_MTGACC](Specification/Common/14 Mortgage Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>