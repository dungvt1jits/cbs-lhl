# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `MTG_RTN`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | CMRGAC |
| 2 | Asset booking value | asset_booking_value | `Yes` | `Number` |  | 0 | số có hai số thập phân | CMGNBAMT |
| 3 | Secured amount | secured_amount | No | `Number` |  | 0 | số có hai số thập phân | CAMT3 |
| 4 | Return amount | return_amount | No | `Number` |  | > 0 | số có hai số thập phân | CAMT |
| 5 | Return amount in asset currency | return_amount_in_asset_currency | `Yes` | `Number` |  | > 0 | số có hai số thập phân | CAMT2 |
| 6 | Customer code | customer_code | `Yes` | String | 15 |  | số có hai số thập phân | CCTMCD |
| 7 | Customer name | customer_name | `Yes` | String | 250 |  |  | CCTMA |
| 8 | Customer address | customer_address | No | String | 250 |  |  | CIDPLACE |
| 9 | Customer description | customer_description | No | String | 250 |  |  | MDESC |
| 10 | Description | description | `Yes` | String | 250 |  |  | DESCS |
| 11 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | CVLDT |
| 12 | Currency code | currency_code | No | String | 3 |  | trường ẩn | CCCR |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Asset booking value | asset_booking_value | `Number` |  | số có hai số thập phân |
| 4 | Secured amount | secured_amount | `Number` |  | số có hai số thập phân |
| 5 | Return amount | return_amount | `Number` |  | số có hai số thập phân |
| 6 | Return amount in asset currency | return_amount_in_asset_currency | `Number` |  | số có hai số thập phân |
| 7 | Customer code | customer_code | String |  | số có hai số thập phân |
| 8 | Customer name | customer_name | String |  |  |
| 9 | Customer address | customer_address | String |  |  |
| 10 | Customer description | customer_description | String |  |  |
| 11 | Description | description | String |  |  |
| 12 | Value date | value_date | `Date time` |  |  |
| 13 | Currency code | currency_code | String |  |  |
| 14 | Transaction status | status | String |  |  |
| 15 | Transaction date |  | `Date time` |  |  |
| 16 | User id |  | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Account number": exists with status "Normal".
- "Return amount" must be larger than zero.
- "Return amount" must be smaller than or equal {book value - secured amount}.
  - In which:
    - Return amount (`return_amount`) < or = (`asset_booking_value` - `secured_amount`) nhập từ request message
      - asset_booking_value = `BookValue` trong bảng `MortgageAccount`
      - secured_amount = công thức workflow  `MTG_RTN_GET_INFO_SCRAMT`
- "Customer code" exists
- "Mortgage account" belongs to "Customer code"

**Flow of events:**
- Transaction complete: Book value of asset will decrease.

**Database:**


**Voucher:**
- `A2`, `A1`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_MAVB | GET_INFO_MTGACC | [GET_INFO_MTGACC](Specification/Common/14 Mortgage Rulefuncs) |
| 2 | GET_SIG_CMRGAC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_SCRAMT | MTG_RTN_GET_INFO_SCRAMT | [MTG_RTN_GET_INFO_SCRAMT](Specification/Common/14 Mortgage Rulefuncs) |
| 4 | GET_INFO_CAMT | `SELECT '@CAMT' FROM DUAL` | Làm trên JWEB |
| 5 | GET_INFO_CAMT2 | `SELECT '@CAMT2' FROM DUAL` | Làm trên JWEB |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>