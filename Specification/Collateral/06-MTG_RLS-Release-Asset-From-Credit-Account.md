# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `MTG_RLS`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | CMRGAC |
| 2 | Asset booking value | asset_booking_value | No | `Number` |  | 0 | số có hai số thập phân | CMGNBAMT |
| 3 | Collateral account currency | collateral_account_currency | No | String | 3 |  |  | CCCR |
| 4 | Credit account | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 5 | Credit account currency | credit_account_currency | No | String | 3 |  |  | RCCR |
| 6 | Secured amount | secured_amount | No | `Number` |  | 0 | số có hai số thập phân | CAMT3 = MTG_RLS_GET_INFO_SCRAMT |
| 7 | Exchange rate | exchange_rate | No | `Number` |  | 0 | số có 9 số thập phân | CCRRATE |
| 8 | Release amount in collateral currency | release_amount_in_collateral_currency | No | `Number` |  | > 0 | số có hai số thập phân | CAMT |
| 9 | Release amount in credit currency | release_amount_in_credit_currency | `Yes` | `Number` |  | > 0 | số có hai số thập phân | PCSAMT |
| 10 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 11 | Customer name | customer_name | `Yes` | String | 250 |  |  | CCTMA |
| 12 | Customer address | customer_address | No | String | 250 |  |  | CIDPLACE |
| 13 | Customer description | customer_description | No | String | 250 |  |  | MDESC |
| 14 | Description | description | No | String | 250 |  |  | DESCS |
| 15 | Base amount | base_amount | No | `Number` |  | 0 | số có hai số thập phân | CBAMT |
| 16 | Credit rate | credit_rate | No | `Number` |  | 0 | trường ẩn | CRATE |
| 17 | Debit rate | debit_rate | No | `Number` |  | 0 | trường ẩn | ORGRATE |
| 18 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | CVLDT |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Asset booking value | asset_booking_value | `Number` |  | số có hai số thập phân |
| 4 | Collateral account currency | collateral_account_currency | String | 3 |  |
| 5 | Credit account | credit_account | String |  |  |
| 6 | Credit account currency | credit_account_currency | String | 3 |  |
| 7 | Secured amount | secured_amount | `Number` |  | số có hai số thập phân |
| 8 | Exchange rate | exchange_rate | `Number` |  | số có 9 số thập phân |
| 9 | Release amount in collateral currency | release_amount_in_collateral_currency | `Number` |  | số có hai số thập phân |
| 10 | Release amount in credit currency | release_amount_in_credit_currency | `Number` |  | số có hai số thập phân |
| 11 | Customer code | customer_code | String |  |  |
| 12 | Customer name | customer_name | String |  |  |
| 13 | Customer address | customer_address | String |  |  |
| 14 | Customer description | customer_description | String |  |  |
| 15 | Description | description | String |  |  |
| 16 | Base amount | base_amount | `Number` |  | số có hai số thập phân |
| 17 | Credit rate | credit_rate | `Number` |  |
| 18 | Debit rate | debit_rate | `Number` |  |
| 19 | Value date | value_date | `Date time` |  |
| 20 | Transaction status | status | String |  |  |
| 21 | Transaction date |  | `Date time` |  |  |
| 22 | User id |  | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Account number": exists with status "Normal".
- "Credit account": exists with status "Normal".
- 0.00 < Release amount in credit currency <= Secured amount
  - In which:
    - Secured amount = `secured_amount`  nhập từ request message = công thức workflow `MTG_RLS_GET_INFO_SCRAMT`

**Flow of events:**
- System will auto convert amount to release if currency between collateral and loan is different.
    - When releasing amount in collateral currency, system will exchange foreign amount to releasing amount in loan currency.
    - When releasing amount in credit currency, system will exchange foreign amount to releasing amount in collateral currency.
    - Foreign exchange will calculate based on "Book rate".
- Don't allow releasing amount to be larger than secured amount.
- Transaction complete: 
    - Secured amount will decrease.

**Database:**


**Voucher:**
- None

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Asset booking value | asset_booking_value | `Number` |  | số có hai số thập phân |
| 4 | Collateral account currency | collateral_account_currency | String | 3 |  |
| 5 | Credit account | credit_account | String |  |  |
| 6 | Credit account currency | credit_account_currency | String | 3 |  |
| 7 | Secured amount | secured_amount | `Number` |  | số có hai số thập phân |
| 8 | Exchange rate | exchange_rate | `Number` |  | số có 9 số thập phân |
| 9 | Release amount in collateral currency | release_amount_in_collateral_currency | `Number` |  | số có hai số thập phân |
| 10 | Release amount in credit currency | release_amount_in_credit_currency | `Number` |  | số có hai số thập phân |
| 11 | Customer code | customer_code | String |  |  |
| 12 | Customer name | customer_name | String |  |  |
| 13 | Customer address | customer_address | String |  |  |
| 14 | Customer description | customer_description | String |  |  |
| 15 | Description | description | String |  |  |
| 16 | Base amount | base_amount | `Number` |  | số có hai số thập phân |
| 17 | Credit rate | credit_rate | `Number` |  |
| 18 | Debit rate | debit_rate | `Number` |  |
| 19 | Value date | value_date | `Date time` |  |
| 20 | Transaction status | status | String |  |  |
| 21 | Transaction date |  | `Date time` |  |  |
| 22 | User id |  | `Number` |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_MAVB | GET_INFO_MTGACC | [GET_INFO_MTGACC](Specification/Common/14 Mortgage Rulefuncs) |
| 2 | GET_SIG_CMRGAC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_SCRAMT | MTG_RLS_GET_INFO_SCRAMT | [MTG_RLS_GET_INFO_SCRAMT](Specification/Common/14 Mortgage Rulefuncs) | CAMT3
| 4 | GET_INFO_CCRRATE | FX_RULEFUNC_GET_INFO_CCRRATE | [FX_RULEFUNC_GET_INFO_CCRRATE](Specification/Common/20 FX Rulefuncs) |
| 5 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 6 | GET_INFO_CRNAME | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>