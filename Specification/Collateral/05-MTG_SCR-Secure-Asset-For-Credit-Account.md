# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `MTG_SCR`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | CMRGAC |
| 2 | Collateral account currency | collateral_account_currency | No | String | 3 |  |  | CCCR |
| 3 | Asset booking value | asset_booking_value | `Yes` | `Number` |  | 0 | số có hai số thập phân | CMGNBAMT = BookValue |
| 4 | Secured amount | collateral_secure_amount_in_use | No | `Number` |  | 0 | số có hai số thập phân | CAMT3 = (MortgageAmount - ReleasedCollateralAmount) |
| 5 | Credit account | credit_account | `Yes` | String | 25 |  |  | PCACC |
| 6 | Credit account currency | credit_account_currency | No | String | 3 |  |  | RCCR |
| 7 | Credit limit | credit_limit | `Yes` | `Number` |  | 0 | số có hai số thập phân | TXAMT |
| 8 | Secured amount | secured_amount | No | `Number` |  | 0 | số có hai số thập phân | CAMT4 |
| 9 | Total secured amount | total_secured_amount | `Yes` | `Number` |  | 0 | số có hai số thập phân | CAMT5 |
| 10 | Amount secure from this asset | amount_secure_from_this_asset | No | `Number` |  | > 0 | số có hai số thập phân | CAMT |
| 11 | Amount secured for credit account | amount_secured_for_credit_account | No | `Number` |  | > 0 | số có hai số thập phân | PCSAMT |
| 12 | Exchange rate | exchange_rate | No | `Number` |  | 0 | số có 9 số thập phân | CCRRATE |
| 13 | Customer code | customer_code | `Yes` | String | 15 |  |  | CCTMCD |
| 14 | Customer name | customer_name | `Yes` | String | 250 |  |  | CCTMA |
| 15 | Customer address | customer_address | No | String | 250 |  |  | CIDPLACE |
| 16 | Customer description | customer_description | No | String | 250 |  |  | MDESC |
| 17 | Description | description | No | String | 250 |  |  | DESCS |
| 18 | Base amount | base_amount | No | `Number` |  | 0 | số có hai số thập phân | CBAMT |
| 19 | Credit rate | credit_rate | No | `Number` |  | 0 | trường ẩn | CRATE |
| 20 | Debit rate | debit_rate | No | `Number` |  | 0 | trường ẩn | ORGRATE |
| 21 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | CVLDT |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Collateral account currency | collateral_account_currency | String | 3 |  |
| 4 | Asset booking value | asset_booking_value | `Number` |  | số có hai số thập phân |
| 5 | Secured amount | collateral_secure_amount_in_use | `Number` |  | số có hai số thập phân |
| 6 | Credit account | credit_account | String |  |  |
| 7 | Credit account currency | credit_account_currency | String | 3 |  |
| 8 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 9 | Secured amount | secured_amount | `Number` |  | số có hai số thập phân |
| 10 | Total secured amount | total_secured_amount | `Number` |  | số có hai số thập phân |
| 11 | Amount secure from this asset | amount_secure_from_this_asset | `Number` |  | số có hai số thập phân |
| 12 | Amount secured for credit account | amount_secured_for_credit_account | `Number` |  | số có hai số thập phân |
| 13 | Exchange rate | exchange_rate | `Number` |  | số có 9 số thập phân |
| 14 | Customer code | customer_code | String |  |  |
| 15 | Customer name | customer_name | String |  |  |
| 16 | Customer address | customer_address | String |  |  |
| 17 | Customer description | customer_description | String |  |  |
| 18 | Description | description | String |  |  |
| 19 | Base amount | base_amount | `Number` |  | số có hai số thập phân |
| 20 | Credit rate | credit_rate | `Number` |  |  |
| 21 | Debit rate | debit_rate | `Number` |  |  |
| 22 | Value date | value_date | `Date time` |  |  |
| 23 | Transaction status | status | String |  |  |
| 24 | Transaction date |  | `Date time` |  |  |
| 25 | User id |  | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Account number": exists with status "Normal".
- "Credit account": exists with status "Pending to approve".
- 0.00 < Amount secure from this asset < (Asset booking value - Secured amount).
  - In which:
    - Asset booking value = Asset booking value (`asset_booking_value`) nhập từ request message = `BookValue` trong bảng `MortgageAccount`
    - Secured amount = Secured amount (`collateral_secure_amount_in_use`) từ request message = `(MortgageAmount - ReleasedCollateralAmount)` trong bảng `MortgageAccount`

**Flow of events:**
- Allow currency of collateral account and credit account can be different.
- System will auto convert amount to secured if currency between collateral and loan is different.
    - When secured amount in collateral currency, system will exchange foreign amount to secured amount in loan currency.
    - When secured amount in credit currency, system will exchange foreign amount to secured amount in collateral currency.
    - Foreign exchange will calculate based on "Book rate".
- Allow secured amount to be larger than require minimum.
- Transaction complete: 
    - Secured amount will increase.

**Database:**



## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**

**Voucher:**
- None

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Collateral account currency | collateral_account_currency | String | 3 |  |
| 4 | Asset booking value | asset_booking_value | `Number` |  | số có hai số thập phân |
| 5 | Secured amount | collateral_secure_amount_in_use | `Number` |  | số có hai số thập phân |
| 6 | Credit account | credit_account | String |  |  |
| 7 | Credit account currency | credit_account_currency | String | 3 |  |
| 8 | Credit limit | credit_limit | `Number` |  | số có hai số thập phân |
| 9 | Secured amount | secured_amount | `Number` |  | số có hai số thập phân |
| 10 | Total secured amount | total_secured_amount | `Number` |  | số có hai số thập phân |
| 11 | Amount secure from this asset | amount_secure_from_this_asset | `Number` |  | số có hai số thập phân |
| 12 | Amount secured for credit account | amount_secured_for_credit_account | `Number` |  | số có hai số thập phân |
| 13 | Exchange rate | exchange_rate | `Number` |  | số có 9 số thập phân |
| 14 | Customer code | customer_code | String |  |  |
| 15 | Customer name | customer_name | String |  |  |
| 16 | Customer address | customer_address | String |  |  |
| 17 | Customer description | customer_description | String |  |  |
| 18 | Description | description | String |  |  |
| 19 | Base amount | base_amount | `Number` |  | số có hai số thập phân |
| 20 | Credit rate | credit_rate | `Number` |  |  |
| 21 | Debit rate | debit_rate | `Number` |  |  |
| 22 | Value date | value_date | `Date time` |  |  |
| 23 | Transaction status | status | String |  |  |
| 24 | Transaction date |  | `Date time` |  |  |
| 25 | User id |  | `Number` |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_INFO_MAVB | GET_INFO_MTGACC | [GET_INFO_MTGACC](Specification/Common/14 Mortgage Rulefuncs) | CAMT3 = (MortgageAmount - ReleasedCollateralAmount)
| 2 | GET_SIG_CMRGAC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_CAMT5 | MTG_SCR_GET_INFO_CAMT5 | [MTG_SCR_GET_INFO_CAMT5](Specification/Common/14 Mortgage Rulefuncs) |
| 4 | GET_INFO_SCRAMT | MTG_RLS_GET_INFO_SCRAMT | [MTG_RLS_GET_INFO_SCRAMT](Specification/Common/14 Mortgage Rulefuncs) |
| 5 | GET_INFO_CCRRATE | FX_RULEFUNC_GET_INFO_CCRRATE | [FX_RULEFUNC_GET_INFO_CCRRATE](Specification/Common/20 FX Rulefuncs) |
| 6 | GET_SIG_PCACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 7 | GET_INFO_CRLIMIT | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |
| 8 | GET_INFO_CRNAME | CRD_GET_INFO_CUSTOMER | [CRD_GET_INFO_CUSTOMER](Specification/Common/13 Credit Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>