# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `MTG_HDT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | `Yes` | String | 25 |  |  | CMRGAC |
| 2 | From date | from_date | `Yes` | `Date time` |  | Working date |  | FROMDT |
| 3 | To date | to_date | `Yes` | `Date time` |  | Working date |  | TODT |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | From date | from_date | `Date time` |  |  |
| 4 | To date | to_date | `Date time` |  |  |
| 5 | Transaction status | status | String |  |  |
| 6 | Transaction list | transaction_list | JSON Object |  |  |
|  | Transaction code | transaction_code | String |  |  |
|  | Transaction number | transaction_number | `Date time` |  |  |
|  | Transaction date | transaction_date | String |  |  |
|  | Created by | created_by | String |  | trả về code của cột UserId |
|  | DORC | dorc | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Description | description | String |  |  |
| 7 | Transaction date |  | `Date time` |  |  |
| 8 | User id |  | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
- "Account number": exists 
- Detail period of history. 
- "To date" >= "From date".

**Flow of events:**
- Allow see history from date to date.
- Get back one table data with information in detail below: "Transaction code", "Transaction number", "Transaction date", "Created by", "DORC", "Amount", "Description".

**Database:**


**Voucher:**
- None

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |
| 2 | Transaction status |  | String |  |  |
| 3 | User approve |  | String |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | From date | from_date | `Date time` |  |  |
| 4 | To date | to_date | `Date time` |  |  |
| 5 | Transaction status | status | String |  |  |
| 6 | Transaction list | transaction_list | JSON Object |  |  |
|  | Transaction code | transaction_code | String |  |  |
|  | Transaction number | transaction_number | `Date time` |  |  |
|  | Transaction date | transaction_date | String |  |  |
|  | Created by | created_by | String |  | trả về code của cột UserId |
|  | DORC | dorc | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Description | description | String |  |  |
| 7 | Transaction date |  | `Date time` |  |  |
| 8 | User id |  | String |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_CMRGAC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>