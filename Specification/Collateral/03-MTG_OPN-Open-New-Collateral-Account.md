# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `MTG_OPN`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Account number | account_number | No | String | 25 |  |  | pdacc |
| 2 | Account name | account_name | `Yes` | String | 250 |  |  | cacnm |
| 3 | Customer type | customer_type | `Yes` | String | 1 |  |  | cctmt |
| 4 | Customer code | customer_code | `Yes` | String | 15 |  |  | cctmcd |
| 5 | Catalogue name | catalog_name | `Yes` | String | 100 |  |  | ccatnm |
| 6 | Catalogue code | catalog_code | `Yes` | String | 25 |  |  | cncat |
| 7 | Collateral asset type | collateral_asset_type | `Yes` | String | 1 |  |  | crtype |
| 8 | Collateral asset class | collateral_asset_class | `Yes` | String | 2 |  |  | cpur |
| 9 | Security paper type | security_paper_type | `Yes` | String | 1 |  |  | cpur2 |
| 10 | Currency code | currency_code | `Yes` | String | 3 |  |  | cacccr |
| 11 | Collateral rate | collateral_rate | `Yes` | `Number` |  | > 0 | số có hai số thập phân | crate |
| 12 | Risk allocation rate | risk_allocation_rate | No | `Number` |  | 0 | số có hai số thập phân | cbkexr1 |
| 13 | Collateral asset value | collateral_asset_value | `Yes` | `Number` |  | > 0 | số có hai số thập phân | camt |
| 14 | Market value | market_value | No | `Number` |  | 0 | số có hai số thập phân | camt3 |
| 15 | Book value | book_value | `Yes` | `Number` |  | > 0 | số có hai số thập phân | camt2 |
| 16 | CC contract | cc_contract | No | String |  |  |  | ccerser |
| 17 | CC amount | cc_amount | No | `Number` |  | 0 | số có hai số thập phân | pcsamt |
| 18 | Seq number | seq_number | No | `Number` |  | 0 | cho phép `null` | cnseq |
| 19 | Reference number | reference_number | `Yes` | String |  |  |  | cserno |
| 20 | Location | location | No | String | 250 |  |  | location |
| 21 | Legal address | legal_address | No | JSON Object |  |  | Được nhập khi Location = Non local, KHÔNG được nhập khi Location = Local | address |
| 22 | Legal local address | legal_local_address | No | JSON Object |  |  |  | laddr |
| 23 | Expiry date | expiry_date | No | `Date time` |  | Working date |  | expdt |
| 24 | Policy amount | policy_amount | No | `Number` |  | 0 |  | plcyamt |
| 25 | Company issues policy | company_issues_policy | No | String | 100 |  |  | ciplcy |
| 26 | Policy number | policy_number | No | String | 100 |  |  | plcyno |
| 27 | Evaluate by | evaluate_by | `Yes` | String |  |  |  | evaby |
| 28 | Evaluate method | evaluate_method | No | String | 3 |  |  | evame |
| 29 | Evaluate date | evaluate_date | `Yes` | `Date time` |  | Working date |  | evadt |
| 30 | New evaluate date | new_evaluate_date | No | `Date time` |  | Working date |  | evandt |
| 31 | Insurance | insurance | No | String |  |  |  | insur |
| 32 | Description | description | No | String | 250 |  |  | descs |
| 33 | Book currency code | book_currency_code | `Yes` | String | 3 |  | trường ẩn, lấy giá trị `Currency code` | pcsccr |
| 34 | Value date | value_date | `Yes` | `Date time` |  | Working date | trường ẩn | cvldt |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Account name | account_name | String |  |  |
| 4 | Customer type | customer_type | String |  |  |
| 5 | Customer code | customer_code | String |  |  |
| 6 | Catalogue name | catalog_name | String |  |  |
| 7 | Catalogue code | catalog_code | String |  |  |
| 8 | Collateral asset type | collateral_asset_type | String |  |  |
| 9 | Collateral asset class | collateral_asset_class | String |  |  |
| 10 | Security paper type | security_paper_type | String |  |  |
| 11 | Currency code | currency_code | String | 3 |  |
| 12 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 13 | Risk allocation rate | risk_allocation_rate | `Number` |  | số có hai số thập phân |
| 14 | Collateral asset value | collateral_asset_value | `Number` |  | số có hai số thập phân |
| 15 | Market value | market_value | `Number` |  | số có hai số thập phân |
| 16 | Book value | book_value | `Number` |  | số có hai số thập phân |
| 17 | CC contract | cc_contract | String |  |  |
| 18 | CC amount | cc_amount | `Number` |  | số có hai số thập phân |
| 19 | Seq number | seq_number | `Number` |  |  |
| 20 | Reference number | reference_number | String |  |  |
| 21 | Location | location | String |  |  |
| 22 | Legal address | legal_address | JSON Object |  |  |
| 23 | Legal local address | legal_local_address | JSON Object |  |  |
| 24 | Expiry date | expiry_date | `Date time` |  |  |
| 25 | Policy amount | policy_amount | `Number` |  |  |
| 26 | Company issues policy | company_issues_policy | String |  |  |
| 27 | Policy number | policy_number | String |  |  |
| 28 | Evaluate by | evaluate_by | String |  |  |
| 29 | Evaluate method | evaluate_method | String |  |  |
| 30 | Evaluate date | evaluate_date | `Date time` |  |  |
| 31 | New evaluate date | new_evaluate_date | `Date time` |  |  |
| 32 | Insurance | insurance | String |  |  |
| 33 | Description | description | String |  |  |
| 34 | Book currency code | book_currency_code | String |  |  |
| 35 | Value date | value_date | `Date time` |  |  |
| 36 | Transaction status | status | String |  |  |
|  | Transaction date |  | `Date time` |  |  |
|  | User id |  | `Number` |  |  |

## 1.2 Transaction flow
**Conditions:**
-	Exists customer code and status `Normal`.
-	"Catalogue code" is defined (classification) and status `Normal`.
-	Book value > 0.00
-   Giá trị `Book value` load theo giá trị `Collateral asset value` nhập vào.

**Flow of events:**
-	Account code will auto create.
-	Allow creation account code by number sequence define.
-	Some definition information will copy from definition "Catalogue code".
-	Don't allow deleting account if it is using.
-	Allow to modify information contract after opening.
-	When complete transaction: status is "pending to approve".

**Database:**


**Voucher:**
- None

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |
| 2 | Username |  | `Yes` | String |  |  |  |
| 3 | Password |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction reference |  | String |  |  |  |
| 2 | Transaction status |  | String |  |  |  |
| 3 | User approve |  | String |  |  |  |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".

**Database:**


## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date {} is incorrect | Sai ngày reverse |
|  | Transaction {} is not allowed to be deleted | Giao dịch không cho phép reverse |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Transaction references | reference_id | String |  |  |
| 2 | Account number | account_number | String |  |  |
| 3 | Account name | account_name | String |  |  |
| 4 | Customer type | customer_type | String |  |  |
| 5 | Customer code | customer_code | String |  |  |
| 6 | Catalogue name | catalog_name | String |  |  |
| 7 | Catalogue code | catalog_code | String |  |  |
| 8 | Collateral asset type | collateral_asset_type | String |  |  |
| 9 | Collateral asset class | collateral_asset_class | String |  |  |
| 10 | Security paper type | security_paper_type | String |  |  |
| 11 | Currency code | currency_code | String | 3 |  |
| 12 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 13 | Risk allocation rate | risk_allocation_rate | `Number` |  | số có hai số thập phân |
| 14 | Collateral asset value | collateral_asset_value | `Number` |  | số có hai số thập phân |
| 15 | Market value | market_value | `Number` |  | số có hai số thập phân |
| 16 | Book value | book_value | `Number` |  | số có hai số thập phân |
| 17 | CC contract | cc_contract | String |  |  |
| 18 | CC amount | cc_amount | `Number` |  | số có hai số thập phân |
| 19 | Seq number | seq_number | String |  |  |
| 20 | Reference number | reference_number | String |  |  |
| 21 | Location | location | String |  |  |
| 22 | Legal address | legal_address | JSON Object |  |  |
| 23 | Legal local address | legal_local_address | JSON Object |  |  |
| 24 | Expiry date | expiry_date | `Date time` |  |  |
| 25 | Policy amount | policy_amount | `Number` |  |  |
| 26 | Company issues policy | company_issues_policy | String |  |  |
| 27 | Policy number | policy_number | String |  |  |
| 28 | Evaluate by | evaluate_by | String |  |  |
| 29 | Evaluate method | evaluate_method | String |  |  |
| 30 | Evaluate date | evaluate_date | `Date time` |  |  |
| 31 | New evaluate date | new_evaluate_date | `Date time` |  |  |
| 32 | Insurance | insurance | String |  |  |
| 33 | Description | description | String |  |  |
| 34 | Book currency code | book_currency_code | String |  |  |
| 35 | Value date | value_date | `Date time` |  |  |
| 36 | Transaction status | status | String |  |  |
|  | Transaction date |  | `Date time` |  |  |
|  | User id |  | `Number` |  |  |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description |
| -- | ------------------- | ------------------- | ----------- |
| 1 | GET_SIG_PDACC | GET_MEDIA_SIG_ACNO | [GET_MEDIA_SIG_ACNO](Specification/Common/12 Customer Rulefuncs) |
| 2 | GET_INFO_CACNM | CTM_GET_INFO_FULLNAME | [CTM_GET_INFO_FULLNAME](Specification/Common/12 Customer Rulefuncs) |
| 3 | GET_INFO_CCATNM | GET_INFO_MTGCAT | [GET_INFO_MTGCAT](Specification/Common/14 Mortgage Rulefuncs) |
| 4 | GET_INFO_PCSCCR | GET_INFO_MTGCAT | [GET_INFO_MTGCAT](Specification/Common/14 Mortgage Rulefuncs) |
| 5 | LKP_DATA_CCTMCD | CTM_LOOKUP_CTM_BY_CTMTYPE | [CTM_LOOKUP_CTM_BY_CTMTYPE](Specification/Common/12 Customer Rulefuncs) |
| 6 | LKP_DATA_CNCAT | MTG_LOOKUP_MORTGAGE_CATALOG | [MTG_LOOKUP_MORTGAGE_CATALOG](Specification/Common/01 Rulefunc) |

# 5. Signature
- [GET_MEDIA_SIG_ACNO](Specification/Common/02 Signature)<br>