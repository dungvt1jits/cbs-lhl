# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/MortgageAccount​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_SIMPLE_SEARCH_MORTGAGE_ACCOUNT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `/api​/MortgageAccount​/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String | 16 |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Customer code | customer_code | String | 8 |  |
| 5 | Catalogue code | catalog_code | String | 8 |  |
| 6 | Collateral type | collateral_asset_type | String |  |  |
| 7 | Classification | collateral_asset_classification | String |  |  |
| 8 | Status | collateral_account_status | String |  |  |
| 9 | Old a/c no | reference_number | String |  |  |
| 10 | Account id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/MortgageAccount​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_ADVANCED_SEARCH_MORTGAGE_ACCOUNT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account number | account_number | No | String | 16 |  |  |
| 2 | Account name | account_name | No | String |  |  |  |
| 3 | Currency | currency_code | No | String | 3 |  |  |
| 4 | Customer code | customer_code | No | String | 8 |  |  |
| 5 | Catalogue code | catalog_code | No | String | 8 |  |  |
| 6 | Collateral type | collateral_asset_type | No | String |  |  |  |
| 7 | Classification | collateral_asset_classification | No | String |  |  |  |
| 8 | Status | collateral_account_status | No | String |  |  |  |
| 9 | Old a/c no | reference_number | No | String |  |  |  |
| 10 | Page index | page_index | No | `Number` |  |  |  |
| 11 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Account number | account_number | String | 16 |  |
| 2 | Account name | account_name | String |  |  |
| 3 | Currency | currency_code | String | 3 |  |
| 4 | Customer code | customer_code | String | 8 |  |
| 5 | Catalogue code | catalog_code | String | 8 |  |
| 6 | Collateral type | collateral_asset_type | String |  |  |
| 7 | Classification | collateral_asset_classification | String |  |  |
| 8 | Status | collateral_account_status | String |  |  |
| 9 | Old a/c no | reference_number | String |  |  |
| 10 | Account id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
- [MTG_OPN: Open New Collateral Account](Specification/Collateral/03 MTG_OPN Open New Collateral Account)<br>

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/MortgageAccount​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_VIEW_MORTGAGE_ACCOUNT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 1
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account id | id | `Number` |  |  |
| 2 | Account number def | account_number_def | String |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | CC contract | cc_contract | String |  |  |
| 6 | Currency code | currency_code | String | 3 |  |
| 7 | Book currency code | book_currency_code | String | 3 |  |
| 8 | Customer type | customer_type | String |  |  |
| 9 | Customer id | customer_id | `Number` |  |  |
| 10 | Branch id | branch_id | `Number` |  |  |
| 11 | Collateral account status | collateral_account_status | String |  |  |
| 12 | Catalogue code | catalog_code | String | 8 |  |
| 13 | Collateral asset type | collateral_asset_type | String |  |  |
| 14 | Collateral asset classification | collateral_asset_classification | String |  |  |
| 15 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 16 | Risk allocation rate | risk_allocation_rate | `Number` |  | số có hai số thập phân |
| 17 | Book scope | book_scope | String |  |  |
| 18 | Depreciation option | depreciation_option | String |  |  |
| 19 | Was register at collateral center | was_register_at_collateral_center | String |  |  |
| 20 | Security paper type | security_paper_type | String |  |  |
| 21 | Security paper number | security_paper_number | String |  |  |
| 22 | Other address | other_address | JSON Object |  |  |
|  | Name of title | name_of_title | String |  |  |
|  | House no | house_no | String |  |  |
|  | Plot no | plot_no | String |  |  |
|  | Holding no | holding_no | String |  |  |
|  | Ward no | ward_no | String |  |  |
|  | Block no | block_no | String |  |  |
|  | Area (acre) | area | String |  |  |
|  | Street | street | String |  |  |
|  | Township | town_ship | String |  |  |
|  | Division, city | division_city | String |  |  |
| 23 | Location | location | String |  |  |
| 24 | Legal local address | legal_local_address | JSON Object |  |  |
|  | Address in Cambodia | address_in_combodia | String |  |  |
|  | Province | province | String |  |  |
|  | Province name | province_name | String |  | `trả thêm` |
|  | Village | village | String |  |  |
|  | Village name | village_name | String |  | `trả thêm` |
|  | Sub district | sub_district | String |  |  |
|  | Sub district name | sub_district_name | String |  | `trả thêm` |
|  | District | district | String |  |  |
|  | District name | district_name | String |  | `trả thêm` |
|  | Address | address | String |  |  |
|  | Zipcode | zip_code | String |  |  |
| 25 | Local address | local_address | JSON Object |  |  |
|  | Address in Cambodia | address_in_combodia | String |  |  |
|  | Province | province | String |  |  |
|  | Province name | province_name | String |  | `trả thêm` |
|  | Village | village | String |  |  |
|  | Village name | village_name | String |  | `trả thêm` |
|  | Sub district | sub_district | String |  |  |
|  | Sub district name | sub_district_name | String |  | `trả thêm` |
|  | District | district | String |  |  |
|  | District name | district_name | String |  | `trả thêm` |
|  | Address | address | String |  |  |
|  | Zipcode | zip_code | String |  |  |
| 26 | Evaluate by | evaluate_by | String |  |  |
| 27 | Evaluate method | evaluate_method | String |  |  |
| 28 | Evaluate date | evaluate_date | `Date time` |  |  |
| 29 | New evaluate date | new_evaluate_date | `Date time` |  |  |
| 30 | Insurance | insurance | String |  |  |
| 31 | Open date | open_date | `Date time` |  |  |
| 32 | Close date | close_date | `Date time` |  |  |
| 33 | Last transaction date | last_transaction_date | `Date time` |  |  |
| 34 | Created by | created_by | `Number` |  |  |
| 35 | Approved by | approved_by | `Number` |  |  |
| 36 | Account manager staff id | account_manager_staff_id | `Number` |  |  |
| 37 | Other paper data | other_paper_data | JSON Object |  |  |
|  | Paper type | paper_type | String |  |  |
|  | Paper no | paper_number | String |  |  |
| 38 | Collateral asset value | collateral_asset_value | `Number` |  | số có hai số thập phân |
| 39 | Market value | market_value | `Number` |  | số có hai số thập phân |
| 40 | Book value | book_value | `Number` |  | số có hai số thập phân |
| 41 | Current secure amount | current_secure_amount | `Number` |  | số có hai số thập phân, số có hai số thập phân AMT - CLRAMT trong bảng d_mtgbal |
| 42 | CC amount | cc_amount | `Number` |  | số có hai số thập phân |
| 43 | Released collateral amount | released_collateral_amount | `Number` |  | số có hai số thập phân |
| 44 | Keeping amount | keeping_amount | `Number` |  | số có hai số thập phân |
| 45 | Keeping release amount | keeping_release_amount | `Number` |  | số có hai số thập phân |
| 46 | Other counter party collateral amount | other_counter_party_collateral_amount | `Number` |  | số có hai số thập phân |
| 47 | Other counter party collateral released | other_counter_party_collateral_released | `Number` |  | số có hai số thập phân |
| 48 | Sum insurance amount | sum_insurance_amount | `Number` |  | số có hai số thập phân |
| 49 | Premium amount | premium_amount | `Number` |  | số có hai số thập phân |
| 50 | Original amount/price | original_amount | `Number` |  | số có hai số thập phân |
| 51 | Accumulate of depreciation amount | accumulate_of_depreciation_amount | `Number` |  | số có hai số thập phân |
| 52 | Net book value after depreciation | net_book_value_after_depreciation | `Number` |  | số có hai số thập phân |
| 53 | Week debit | week_debit | `Number` |  | số có hai số thập phân |
| 54 | Week credit | week_credit | `Number` |  | số có hai số thập phân |
| 55 | Month debit | month_debit | `Number` |  | số có hai số thập phân |
| 56 | Month credit | month_credit | `Number` |  | số có hai số thập phân |
| 57 | Quarter debit | quarter_debit | `Number` |  | số có hai số thập phân |
| 58 | Quarter credit | quarter_credit | `Number` |  | số có hai số thập phân |
| 59 | Semi-annual debit | semi_annual_debit | `Number` |  | số có hai số thập phân |
| 60 | Semi-annual credit | semi_annual_credit | `Number` |  | số có hai số thập phân |
| 61 | Year debit | year_debit | `Number` |  | số có hai số thập phân |
| 62 | Year credit | year_credit | `Number` |  | số có hai số thập phân |
| 63 | Remark | remark | String |  |  |
| 64 | Reference id | reference_number | String |  |  |
| 65 | Owner | owner | String |  |  |
| 66 | User define 1 | user_define1 | String |  |  |
| 67 | User define 2 | user_define2 | String |  |  |
| 68 | User define 3 | user_define3 | String |  |  |
| 69 | User define 4 | user_define4 | String |  |  |
| 70 | User define 5 | user_define5 | String |  |  |
| 71 | Policy number | policy_number | String |  |  |
| 72 | Expiry date | expiry_date | `Date time` |  |  |
| 73 | Policy amount | policy_amount | `Number` |  | số có hai số thập phân |
| 74 | Company issues policy | company_issues_policy | String |  |  |
|  | Customer code | customer_code | String |  | `trả thêm` |
|  | Customer name | customer_name | String |  | `trả thêm` |
|  | Branch code | branch_code | String |  | `trả thêm` |
|  | Created by code | created_by_code | String |  | `trả thêm` |
|  | Created by name | created_by_name | String |  | `trả thêm` |
|  | Approved by code | approved_by_code | String |  | `trả thêm` |
|  | Approved by name | approved_by_name | String |  | `trả thêm` |
|  | Account manager staff code | account_manager_staff_code | String |  | `trả thêm` |
|  | Account manager staff name | account_manager_staff_name | String |  | `trả thêm` |
|  | Catalog name | catalog_name | String |  | `trả thêm` |

**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `​/api​/MortgageAccount​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_UPDATE_MORTGAGE_ACCOUNT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account name | account_name | `Yes` | String |  |  |  |
| 2 | CC contract | cc_contract | No | String |  |  |  |
| 3 | Collateral rate | collateral_rate | No | `Number` |  |  | số có hai số thập phân |
| 4 | Risk allocation rate | risk_allocation_rate | `Yes` | `Number` |  |  | số có hai số thập phân |
| 5 | Was register at collateral center | was_register_at_collateral_center | No | String |  |  |  |
| 6 | Security paper type | security_paper_type | `Yes` | String |  |  |  |
| 7 | Security paper number | security_paper_number | `Yes` | String |  |  |  |
| 8 | Other address | other_address | No | JSON Object |  |  |  |
|  | Name of title | name_of_title | No | String |  |  |  |
|  | House no | house_no | No | String |  |  |  |
|  | Plot no | plot_no | No | String |  |  |  |
|  | Holding no | holding_no | No | String |  |  |  |
|  | Ward no | ward_no | No | String |  |  |  |
|  | Block no | block_no | No | String |  |  |  |
|  | Area (acre) | area | No | String |  |  |  |
|  | Street | street | No | String |  |  |  |
|  | Township | town_ship | No | String |  |  |  |
|  | Division, city | division_city | No | String |  |  |  |
| 9 | Location | location | No | String |  |  |  |
| 10 | Legal local address | legal_local_address | No | JSON Object |  |  |  |
|  | Province | province | No | String |  |  |  |
|  | District | district | No | String |  |  |  |
|  | Sub district | sub_district | No | String |  |  |  |
|  | Village | village | No | String |  |  |  |
|  | Address in Cambodia | address_in_combodia | No | String |  |  |  |
|  | Address | address | No | String |  |  |  |
|  | Zipcode | zip_code | No | String |  |  |  |
| 11 | Local address | local_address | No | JSON Object |  |  |  |
|  | Province | province | No | String |  |  |  |
|  | District | district | No | String |  |  |  |
|  | Sub district | sub_district | No | String |  |  |  |
|  | Village | village | No | String |  |  |  |
|  | Address in Cambodia | address_in_combodia | No | String |  |  |  |
|  | Address | address | No | String |  |  |  |
|  | Zipcode | zip_code | No | String |  |  |  |
| 12 | Evaluate by | evaluate_by | No | String |  |  |  |
| 13 | Evaluate method | evaluate_method | No | String |  |  |  |
| 14 | Evaluate date | evaluate_date | No | `Date time` |  |  |  |
| 15 | New evaluate date | new_evaluate_date | No | `Date time` |  |  |  |
| 16 | Insurance | insurance | No | String |  |  |  |
| 17 | Other paper data | other_paper_data | No | JSON Object |  |  |  |
|  | Paper type | paper_type | No | String |  |  |  |
|  | Paper no | paper_number | No | String |  |  |  |
| 18 | Market value | market_value | No | `Number` |  |  | số có hai số thập phân |
| 19 | CC amount | cc_amount | No | `Number` |  |  | số có hai số thập phân |
| 20 | Other counter party collateral amount | other_counter_party_collateral_amount | `Yes` | `Number` |  |  | số có hai số thập phân |
| 21 | Other counter party collateral released | other_counter_party_collateral_released | `Yes` | `Number` |  |  | số có hai số thập phân |
| 22 | Sum insurance amount | sum_insurance_amount | `Yes` | `Number` |  |  | số có hai số thập phân |
| 23 | Premium amount | premium_amount | `Yes` | `Number` |  |  | số có hai số thập phân |
| 24 | Original amount/price | original_amount | `Yes` | `Number` |  |  | số có hai số thập phân |
| 25 | Remark | remark | No | String |  |  |  |
| 26 | Reference id | reference_number | No | String |  |  |  |
| 27 | Owner | owner | No | String |  |  |  |
| 28 | User define 2 | user_define1 | No | String |  |  |  |
| 29 | User define 3 | user_define2 | No | String |  |  |  |
| 30 | User define 1 | user_define3 | No | String |  |  |  |
| 31 | User define 4 | user_define4 | No | String |  |  |  |
| 32 | User define 5 | user_define5 | No | String |  |  |  |
| 33 | Policy number | policy_number | No | String |  |  |  |
| 34 | Expiry date | expiry_date | No | `Date time` |  |  |  |
| 35 | Policy amount | policy_amount | No | `Number` |  | 0 | số có hai số thập phân |
| 36 | Company issues policy | company_issues_policy | No | String |  |  |  |
| 37 | Account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account id | id | `Number` |  |  |
| 2 | Account number def | account_number_def | String |  |  |
| 3 | Account number | account_number | String |  |  |
| 4 | Account name | account_name | String |  |  |
| 5 | CC contract | cc_contract | String |  |  |
| 6 | Currency code | currency_code | String | 3 |  |
| 7 | Book currency code | book_currency_code | String | 3 |  |
| 8 | Customer type | customer_type | String |  |  |
| 9 | Customer code | customer_code | String | 8 |  |
| 10 | Branch code | branch_code | String | 4 |  |
| 11 | Collateral account status | collateral_account_status | String |  |  |
| 12 | Catalogue code | catalog_code | String | 8 |  |
| 13 | Collateral asset type | collateral_asset_type | String |  |  |
| 14 | Collateral asset classification | collateral_asset_classification | String |  |  |
| 15 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 16 | Risk allocation rate | risk_allocation_rate | `Number` |  | số có hai số thập phân |
| 17 | Book scope | book_scope | String |  |  |
| 18 | Depreciation option | depreciation_option | String |  |  |
| 19 | Was register at collateral center | was_register_at_collateral_center | String |  |  |
| 20 | Security paper type | security_paper_type | String |  |  |
| 21 | Security paper number | security_paper_number | String |  |  |
| 22 | Other address | other_address | JSON Object |  |  |
|  | Name of title | name_of_title | String |  |  |
|  | House no | house_no | String |  |  |
|  | Plot no | plot_no | String |  |  |
|  | Holding no | holding_no | String |  |  |
|  | Ward no | ward_no | String |  |  |
|  | Block no | block_no | String |  |  |
|  | Area (acre) | area | String |  |  |
|  | Street | street | String |  |  |
|  | Township | town_ship | String |  |  |
|  | Division, city | division_city | String |  |  |
| 23 | Location | location | String |  |  |
| 24 | Legal local address | legal_local_address | JSON Object |  |  |
|  | Province | province | String |  |  |
|  | District | district | String |  |  |
|  | Sub district | sub_district | String |  |  |
|  | Village | village | String |  |  |
|  | Address in Cambodia | address_in_combodia | String |  |  |
|  | Address | address | String |  |  |
|  | Zipcode | zip_code | String |  |  |
| 25 | Local address | local_address | JSON Object |  |  |
|  | Province | province | String |  |  |
|  | District | district | String |  |  |
|  | Sub district | sub_district | String |  |  |
|  | Village | village | String |  |  |
|  | Address in Cambodia | address_in_combodia | String |  |  |
|  | Address | address | String |  |  |
|  | Zipcode | zip_code | String |  |  |
| 26 | Evaluate by | evaluate_by | String |  |  |
| 27 | Evaluate method | evaluate_method | String |  |  |
| 28 | Evaluate date | evaluate_date | `Date time` |  |  |
| 29 | New evaluate date | new_evaluate_date | `Date time` |  |  |
| 30 | Insurance | insurance | String |  |  |
| 31 | Open date | open_date | `Date time` |  |  |
| 32 | Close date | close_date | `Date time` |  |  |
| 33 | Last transaction date | last_transaction_date | `Date time` |  |  |
| 34 | Created by | created_by | String |  |  |
| 35 | Approved by | approved_by | String |  |  |
| 36 | Account manager staff id | account_manager_staff_id | `Number` |  |  |
| 37 | Other paper data | other_paper_data | JSON Object |  |  |
|  | Paper type | paper_type | String |  |  |
|  | Paper no | paper_number | String |  |  |
| 38 | Collateral asset value | collateral_asset_value | `Number` |  | số có hai số thập phân |
| 39 | Market value | market_value | `Number` |  | số có hai số thập phân |
| 40 | Book value | book_value | `Number` |  | số có hai số thập phân |
| 41 | Current secure amount | current_secure_amount | `Number` |  | số có hai số thập phân AMT - CLRAMT trong bảng d_mtgbal |
| 42 | CC amount | cc_amount | `Number` |  | số có hai số thập phân |
| 43 | Released collateral amount | released_collateral_amount | `Number` |  | số có hai số thập phân |
| 44 | Keeping amount | keeping_amount | `Number` |  | số có hai số thập phân |
| 45 | Keeping release amount | keeping_release_amount | `Number` |  | số có hai số thập phân |
| 46 | Other counter party collateral amount | other_counter_party_collateral_amount | `Number` |  | số có hai số thập phân |
| 47 | Other counter party collateral released | other_counter_party_collateral_released | `Number` |  | số có hai số thập phân |
| 48 | Sum insurance amount | sum_insurance_amount | `Number` |  | số có hai số thập phân |
| 49 | Premium amount | premium_amount | `Number` |  | số có hai số thập phân |
| 50 | Original amount/price | original_amount | `Number` |  | số có hai số thập phân |
| 51 | Accumulate of depreciation amount | accumulate_of_depreciation_amount | `Number` |  | số có hai số thập phân |
| 52 | Net book value after depreciation | net_book_value_after_depreciation | `Number` |  | số có hai số thập phân |
| 53 | Week debit | week_debit | `Number` |  | số có hai số thập phân |
| 54 | Week credit | week_credit | `Number` |  | số có hai số thập phân |
| 55 | Month debit | month_debit | `Number` |  | số có hai số thập phân |
| 56 | Month credit | month_credit | `Number` |  | số có hai số thập phân |
| 57 | Quarter debit | quarter_debit | `Number` |  | số có hai số thập phân |
| 58 | Quarter credit | quarter_credit | `Number` |  | số có hai số thập phân |
| 59 | Semi-annual debit | semi_annual_debit | `Number` |  | số có hai số thập phân |
| 60 | Semi-annual credit | semi_annual_credit | `Number` |  | số có hai số thập phân |
| 61 | Year debit | year_debit | `Number` |  | số có hai số thập phân |
| 62 | Year credit | year_credit | `Number` |  | số có hai số thập phân |
| 63 | Remark | remark | String |  |  |
| 64 | Reference id | reference_number | String |  |  |
| 65 | Owner | owner | String |  |  |
| 66 | User define 1 | user_define1 | String |  |  |
| 67 | User define 2 | user_define2 | String |  |  |
| 68 | User define 3 | user_define3 | String |  |  |
| 69 | User define 4 | user_define4 | String |  |  |
| 70 | User define 5 | user_define5 | String |  |  |
| 71 | Policy number | policy_number | String |  |  |
| 72 | Expiry date | expiry_date | `Date time` |  |  |
| 73 | Policy amount | policy_amount | `Number` |  | số có hai số thập phân |
| 74 | Company issues policy | company_issues_policy | String |  |  |
| 75 | User define field | user_define_field_1 | String |  | không dùng, xem xét xóa đi |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `​/api​/MortgageAccount​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_DELETE_MORTGAGE_ACCOUNT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Account id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Account id | id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list signature by customer code
- [Get information của "Customer Media Files](Specification/Customer/04 Customer Media Files)