# 1. Search common
## 1.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/MortgageCatalog​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_SIMPLE_SEARCH_MORTGAGE_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Search text | search_text | No | String |  |  |  |
| 2 | Page size | page_size | No | `Number` |  | 0 |  |
| 3 | Page index | page_index | No | `Number` |  | 0 |  |

**Example:** `/api​/MortgageCatalog​/Search?SearchText&PageIndex=0&PageSize=3`

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String | 8 |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Currency code | currency_code | String | 3 |  |
| 4 | Collateral asset type | collateral_asset_type | String |  |  |
| 5 | Classification | classification | String |  |  |
| 6 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 7 | Status | catalog_status | String |  |  |
| 8 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{

}
```

## 1.2 Transaction flow

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 2. Search advanced
## 2.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `/api​/MortgageCatalog​/Search`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_ADVANCED_SEARCH_MORTGAGE_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | No | String | 8 |  |  |
| 2 | Catalogue name | catalog_name | No | String |  |  |  |
| 3 | Currency code | currency_code | No | String | 3 |  |  |
| 4 | Collateral asset type | collateral_asset_type | No | String |  |  |  |
| 5 | Classification | classification | No | String |  |  |  |
| 6 | Collateral rate from | collateral_rate_from | No | `Number` |  |  | số có hai số thập phân |
| 7 | Collateral rate to | collateral_rate_to | No | `Number` |  |  | số có hai số thập phân |
| 8 | Status | catalog_status | No | String |  |  |  |
| 9 | Page index | page_index | No | `Number` |  |  |  |
| 10 | Page size | page_size | No | `Number` |  |  |  |

**Example:**
```json
{
    
}

```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Total count | total_count | `Number` |  |  |
|  | Total pages | total_pages | `Number` |  |  |
|  | Has previous page | has_previous_page | `Boolean` |  |  |
|  | Has next page | has_next_page | `Boolean` |  |  |
|  | Page index | page_index | `Number` |  |  |
|  | Page size | page_size | `Number` |  |  |
|  | Search results | items | Array Object |  |  |
| 1 | Catalogue code | catalog_code | String | 8 |  |
| 2 | Catalogue name | catalog_name | String |  |  |
| 3 | Currency code | currency_code | String | 3 |  |
| 4 | Collateral asset type | collateral_asset_type | String |  |  |
| 5 | Classification | classification | String |  |  |
| 6 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 7 | Status | catalog_status | String |  |  |
| 8 | Catalogue id | id | `Number` |  |  |

**Example:**
```json
{

}
```

## 2.2 Transaction flow

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | No data found | Không tìm thấy dữ liệu |

# 3. Add
## 3.1 Field description
### Request message
**HTTP Method:** `POST`

**URL:** `​/api​/MortgageCatalog​/Create`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_INSERT_MORTGAGE_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue code | catalog_code | `Yes` | String | 8 |  |  |
| 2 | Catalogue name | catalog_name | `Yes` | String |  |  |  |
| 3 | Currency code | currency_code | `Yes` | String |  |  |  |
| 4 | Collateral asset type | collateral_asset_type | `Yes` | String |  |  |  |
| 5 | Collateral asset classification | classification | No | String |  |  |  |
| 6 | Collateral rate | collateral_rate | No | `Number` |  |  |  |
| 7 | Risk allocation rate | risk_rate | No | `Number` |  |  |  |
| 8 | Book scope | book_scope | `Yes` | String |  |  |  |
| 9 | Depreciation option | depreciation_option | `Yes` | String |  |  |  |
| 10 | Catalogue status | catalog_status | `Yes` | String |  |  |  |
| 11 | Group id | group_id | No | `Number` |  | `Null` |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  | is unique |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Catalogue name | catalog_name | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Collateral asset type | collateral_asset_type | String |  |  |
| 6 | Collateral asset classification | classification | String |  |  |
| 7 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 8 | Risk allocation rate | risk_rate | `Number` |  |  |
| 9 | Book scope | book_scope | String |  |  |
| 10 | Depreciation option | depreciation_option | String |  |  |
| 11 | Status | catalog_status | String |  |  |
| 12 | Created by | created_by | `Number` |  |  |
| 13 | Approved by | approved_by | `Number` |  |  |
| 14 | Group id | group_id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |

# 4. View
## 4.1 Field description
### Request message
**HTTP Method:** `GET`

**URL:** `​/api​/MortgageCatalog​/View​/{id}`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_VIEW_MORTGAGE_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  | không được sửa |
| 2 | Catalogue code | catalog_code | String | 8 | không được sửa |
| 3 | Catalogue name | catalog_name | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Collateral asset type | collateral_asset_type | String |  |  |
| 6 | Collateral asset classification | classification | String |  |  |
| 7 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 8 | Risk allocation rate | risk_rate | `Number` |  |  |
| 9 | Book scope | book_scope | String |  |  |
| 10 | Depreciation option | depreciation_option | String |  |  |
| 11 | Status | catalog_status | String |  |  |
| 12 | Created by | created_by | `Number` |  |  |
| 13 | Approved by | approved_by | `Number` |  |  |
| 14 | Group id | group_id | `Number` |  |  |
|  | Created by code | created_by_code | String |  | `trả thêm` |
|  | Created by name | created_by_name | String |  | `trả thêm` |
|  | Approved by code | approved_by_code | String |  | `trả thêm` |
|  | Approved by name | approved_by_name | String |  | `trả thêm` |


**Example:**
```json
{
    
}
```

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 5. Modify
## 5.1 Field description
### Request message
**HTTP Method:** `PUT`

**URL:** `​/api​/MortgageCatalog​/Update`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_UPDATE_MORTGAGE_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |
| 2 | Catalogue name | catalog_name | `Yes` | String |  |  |  |
| 3 | Currency code | currency_code | `Yes` | String |  |  |  |
| 4 | Collateral asset type | collateral_asset_type | `Yes` | String |  |  |  |
| 5 | Collateral asset classification | classification | No | String |  |  |  |
| 6 | Collateral rate | collateral_rate | No | `Number` |  |  |  |
| 7 | Risk allocation rate | risk_rate | No | `Number` |  |  |  |
| 8 | Book scope | book_scope | `Yes` | String |  |  |  |
| 9 | Depreciation option | depreciation_option | `Yes` | String |  |  |  |
| 10 | Catalogue status | catalog_status | `Yes` | String |  |  |  |
| 11 | Group id | group_id | No | `Number` |  | `Null` |  |

**Example:**
```json
{
    
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  | is unique |
| 2 | Catalogue code | catalog_code | String | 8 |  |
| 3 | Catalogue name | catalog_name | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Collateral asset type | collateral_asset_type | String |  |  |
| 6 | Collateral asset classification | classification | String |  |  |
| 7 | Collateral rate | collateral_rate | `Number` |  | số có hai số thập phân |
| 8 | Risk allocation rate | risk_rate | `Number` |  |  |
| 9 | Book scope | book_scope | String |  |  |
| 10 | Depreciation option | depreciation_option | String |  |  |
| 11 | Status | catalog_status | String |  |  |
| 12 | Created by | created_by | `Number` |  |  |
| 13 | Approved by | approved_by | `Number` |  |  |
| 14 | Group id | group_id | `Number` |  |  |

**Example:**
```json
{
    
}
```

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} is unique | Thông tin là duy nhất |
|  | {} does not exist | Không tồn tại |

# 6. Delete
## 6.1 Field description
### Request message
**HTTP Method:** `DELETE`

**URL:** `​/api​/MortgageCatalog​/Delete`

**Header:** `Authorization: Bearer {token}`

**Workflow id:** `MTG_DELETE_MORTGAGE_CATALOG`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Catalogue id | id | `Yes` | `Number` |  |  |  |

**Example:**
```json
{
    "id": 3
}
```

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Catalogue id | id | `Number` |  | is unique |

**Example:**
```json
{
    "id": 3
}
```

## 6.2 Transaction flow

## 6.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} is used | Đang được sử dụng |

# 7. Get list accounting group for credit
- "module": "MTG"
- [Accounting group của "Rulefunc"](Specification/Common/01 Rulefunc)