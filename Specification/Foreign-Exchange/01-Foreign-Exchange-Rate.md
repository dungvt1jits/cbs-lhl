# 1. Get foreign exchange rate
## 1.1 Field description
### Request message

**Workflow id:** `FX_FXRATE_GETFXRATE`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Branch id fx | branch_id_fx | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Branch id fx | branch_id_fx | `Number` |  |  |
| 2 | Branch code | branch_code | String |  |  |
| 3 | Last update date | last_update_date | `Date time` |  |  |
| 4 | FX rate data | fx_rate_data | Array Object |  |  |
|  | Currency code | currency_code | String | 3 |  |
|  | Short id | short_id | String |  | currency short code |
|  | Reference rate | book_rate | `Number` |  | số có 8 số thập phân |
|  | Buying T/T rate | transfer_bid_rate | `Number` |  | số có 8 số thập phân |
|  | Selling T/T rate | transfer_ask_rate | `Number` |  | số có 8 số thập phân |
|  | Small bill rate | cash_bid_small_rate | `Number` |  | số có 8 số thập phân |
|  | Medium bill rate | cash_bid_medium_rate | `Number` |  | số có 8 số thập phân |
|  | Buying big note rate | cash_bid_big_rate | `Number` |  | số có 8 số thập phân |
|  | Selling note rate | cash_ask_rate | `Number` |  | số có 8 số thập phân |

## 1.2 Transaction flow
- last_update_date: trả về giá trị VALUEDT mới nhất tương ứng với branch_id_fx
- Exchange rate type map trên UI: 
    - book_rate: Book rate (BK)
    - cash_bid_big_rate: Buying big note rate (CB)
    - cash_bid_small_rate: Cash bid small rate (CS)
    - cash_bid_medium_rate: Cash bid medium (CM)
    - cash_ask_rate: Cash ask rate (CA)
    - transfer_bid_rate: Transfer bid rate (TB)
    - transfer_ask_rate: Transfer ask rate (TA)
	- sight_bill_rate: Sight bill rate (SB)

Reference rate is Book rate
Buying T/T rate is Transfer bid rate
Selling T/T rate is Transfer ask rate

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |

# 2. Modify
## 2.1 Field description
### Request message

**Workflow id:** `FX_APPFXRATE_MDF`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
|  | FX rate data modify | data_modify | `Yes` | Array Object |  |  |  |
| 1 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 2 | Exchange rate type | exchange_rate_type | `Yes` | String |  |  |  |
| 3 | Base currency rate | base_currency_rate | `Yes` | `Number` |  |  |  |
| 4 | Branch id fx | branch_id_fx | `Yes` | `Number` |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | FX rate data modify | data_modify | Array Object |  |  |
| 1 | Transaction reference id | tx_reference_id | String |  |  |
| 2 | Transaction date | transaction_date | `Date time` |  |  |
| 3 | Approved status | approved_status | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Short id | short_id | String |  | currency short code |
| 6 | Exchange rate type | exchange_rate_type | String |  |  |
| 7 | Base currency rate | base_currency_rate | `Number` |  |  |
| 8 | Branch id fx | branch_id | `Number` |  |  |
| 9 | User name | user_name | String |  | user thực hiện |

## 2.2 Transaction flow
- Exchange rate type: 
    - BK: Reference rate
    - TB: Buying T/T rate
    - TA: Selling T/T rate
    - ~~CS~~ SB: Small bill rate
    - CM: Medium bill rate
    - CB: Buying big note rate
    - CA: Selling note rate
	- ~~SB: Sight bill rate~~

## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |

# 3. View modify
## 3.1 Field description
### Request message

**Workflow id:** `FX_APPFXRATE_VIEW`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | FX rate data modify | data_modify | Array Object |  |  |
| 1 | Transaction reference id | tx_reference_id | String |  |  |
| 2 | Transaction date | transaction_date | `Date time` |  |  |
| 3 | Approved status | approved_status | String |  |  |
| 4 | Currency code | currency_code | String | 3 |  |
| 5 | Short id | short_id | String |  | currency short code |
| 6 | Exchange rate type | exchange_rate_type | String |  |  |
| 7 | Base currency rate | base_currency_rate | `Number` |  |  |
| 8 | Branch id fx | branch_id | `Number` |  |  |
| 9 | User name | user_name | String |  |  |

## 3.2 Transaction flow

## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |

# 4. Approve modify
## 4.1 Field description
### Request message

**Workflow id:** `FX_APPFXRATE_APR`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
|  | FX rate data modify | data_modify | `Yes` | Array Object |  |  |  |
| 1 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 2 | Exchange rate type | exchange_rate_type | `Yes` | String |  |  |  |
| 3 | Branch id fx | branch_id_fx | `Yes` | `Number` |  |  | số nguyên |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | FX rate data modify | data_modify | Array Object |  |  |
| 1 | Currency code | currency_code | String | 3 |  |
| 2 | Exchange rate type | exchange_rate_type | String |  |  |
| 3 | Branch id fx | branch_id_fx | `Number` |  | số nguyên |

## 4.2 Transaction flow

## 4.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |

# 5. Reject modify
## 5.1 Field description
### Request message

**Workflow id:** `FX_APPFXRATE_REJECT`

**Body:**
| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
|  | FX rate data modify | data_modify | `Yes` | Array Object |  |  |  |
| 1 | Currency code | currency_code | `Yes` | String | 3 |  |  |
| 2 | Exchange rate type | exchange_rate_type | `Yes` | String |  |  |  |
| 3 | Branch id fx | branch_id_fx | `Yes` | `Number` |  |  | số nguyên |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | FX rate data modify | data_modify | Array Object |  |  |
| 1 | Currency code | currency_code | String | 3 |  |
| 2 | Exchange rate type | exchange_rate_type | String |  |  |
| 3 | Branch id fx | branch_id_fx | `Number` |  | số nguyên |

## 5.2 Transaction flow

## 5.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  |  |  |

# 6. Get branch id HO
- [Get branch id HO phần "Authenticate"](Specification/Administration/00 Authenticate)

# 7. Trả về thông tin BK rate (GET_CROSS_RATE) khi nhận "Currency" 01 và "Currency" 02
- **Workflow id:** `FX_GET_CROSS_RATE`

- **Query in O9:** 
```sql
SELECT ROUND(O9UTIL.GET_CROSS_RATE ('@CCCR','@CCCR2','BK', 'BK',NULL,NULL,NULL),9) FROM DUAL

- **Request message:**

| No | Field name | Parameter | Is require | Type | Length | Default value | Description |
| -- | ---------- | --------- | ---------- | ---- | ------ | ------------- | ----------- |
| 1 | Currency 01 | currency_code01 | No | String |  |  | `CCCR` |
| 2 | Currency 02 | currency_code02 | No | String |  |  | `CCCR2` |

- **Response message:**

| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Cross rate | cross_rate | `Number` |  | số có 9 số thập phân |