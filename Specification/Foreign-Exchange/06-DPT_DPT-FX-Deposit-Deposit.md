# 1. Accept
## 1.1 Field description
### Request message

**Workflow id:** `DPT_DPT`

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
|  | Debit |  |  |  |  |  |  |  |
| 1 | Accounting type | debit_accounting_type | `Yes` | String | 3 |  |  | DACTTYPE |
| 2 | Account number | debit_account_number | `Yes` | String | 25 |  |  | PGOACC |
| 3 | Currency | debit_currency_code | `Yes` | String | 3 |  |  | PCSCCR |
| 4 | Type | debit_type | `Yes` | String | 5 |  |  | CDTYPE |
|  | Credit |  |  |  |  |  |  |  |
| 5 | Accounting type | credit_accounting_type | `Yes` | String | 3 |  |  | CACTTYPE |
| 6 | Account number | credit_account_number | `Yes` | String | 25 |  |  | PGACC |
| 7 | Currency | credit_currency_code | `Yes` | String | 3 |  |  | PGCCR |
| 8 | Type | credit_type | `Yes` | String | 5 |  |  | CCTYPE |
|  | Rate |  |  |  |  |  |  |  |
| 9 | Debit rate | debit_rate | `Yes` | `Number` |  |  | số có 9 số thập phân | PGEXR |
| 10 | Debit amount | debit_amount | `Yes` | `Number` |  |  | số có hai số thập phân | PGAMT |
| 11 | Cross rate | cross_rate | `Yes` | `Number` |  |  | số có 9 số thập phân | CCRRATE |
| 12 | Reverse rate | reverse_rate | `Yes` | `Number` |  |  | số có 9 số thập phân | SWRATE |
| 13 | Original rate | original_cross_rate | `Yes` | `Number` |  |  | số có 9 số thập phân, trường ẩn | ORGRATE |
| 14 | Credit rate | credit_rate | `Yes` | `Number` |  |  | số có hai số thập phân | CTXEXR |
| 15 | Credit amount | credit_amount | `Yes` | `Number` |  |  | số có 9 số thập phân | PCSCVT |
| 16 | Fee amount | fee_amount | `Yes` | `Number` |  |  | số có hai số thập phân | FEEAMT |
| 17 | Receive amount | receive_amount | `Yes` | `Number` |  |  | số có hai số thập phân | PRCAMT |
| 18 | Amount convert to BCY | base_amount | `Yes` | `Number` |  |  | số có hai số thập phân, trường ẩn | CBAMT |
|  | Customer information |  |  |  |  |  |  |  |
| 19 | Customer type | customer_type | No | String | 1 |  |  | CTMTYPE |
| 20 | Customer code | custommer_code | No | String | 15 |  |  | CUSTOMERCD |
| 21 | Full name | full_name | No | String | 250 |  |  | CACNM |
| 22 | Paper type | paper_type | No | String | 1 |  |  | REPIDTYPE |
| 23 | Paper number | paper_number | No | String | 100 |  |  | CCTMCDP |
| 24 | Telephone | phone_mobile | No | String | 15 |  |  | CTMTEL |
| 25 | Address | address | No | String | 250 |  |  | CCTMA |
| 26 | Nationality | nationality | No | String | 2 |  |  | NATION |
| 27 | Description | description | No | String | 250 |  |  | DESCS |
| 28 | Fee data | fee_data | No | Array Object |  |  |  |  |
|  | IFC code | ifc_code | No | `Number` |  |  |  | IFCCD |
|  | Value | ifc_value | No | `Number` |  |  | số có 5 số thập phân | IFCVAL |
|  | Fee | ifc_amount | No | `Number` |  |  | số có hai số thập phân | IFCAMT |
|  | Floor | floor_value | No | `Number` |  |  | số có hai số thập phân | FLRVAL |
|  | Ceiling | ceiling_value | No | `Number` |  |  | số có hai số thập phân | CEIVAL |
|  | Currency | currency_fee_code | No | String | 3 |  |  | CCRCD |
|  | Currency account code | currency_account_code | No | String | 3 |  | trường ẩn | CCRID |
|  | Payable rate | payrate | No | `Number` |  | 0 | trường ẩn | PAYRATE |
|  | Payable source | pay_source | No | String | 3 | CSH or DPT | trường ẩn | PAYSRC |
|  | Round amount | round_amount | No | `Number` |  | 0 | trường ẩn | RAMT |
|  | Round rate | round_rate | No | `Number` |  | 0 | trường ẩn | RRATE |
|  | Share amount | share_amount | No | `Number` |  | 0 | trường ẩn | SAMT |
|  | Share fee apply | share_fee | No | `Number` |  | 0 | trường ẩn | SFAPPL |
|  | Share rate | share_rate | No | `Number` |  | 0 | trường ẩn | SRATE |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
|  | Debit |  |  |  |  |
| 1 | Accounting type | debit_accounting_type | String |  |  |
| 2 | Account number | debit_account_number | String |  |  |
| 3 | Currency | debit_currency_code | String |  |  |
| 4 | Type | debit_type | String |  |  |
|  | Credit |  |  |  |  |
| 5 | Accounting type | credit_accounting_type | String |  |  |
| 6 | Account number | credit_account_number | String |  |  |
| 7 | Currency | credit_currency_code | String |  |  |
| 8 | Type | credit_type | String |  |  |
|  | Rate |  |  |  |  |
| 9 | Debit rate | debit_rate | `Number` |  |  |
| 10 | Debit amount | debit_amount | `Number` |  |  |
| 11 | Cross rate | cross_rate | `Number` |  |  |
| 12 | Reverse rate | reverse_rate | `Number` |  |  |
| 13 | Original rate | original_cross_rate | `Number` |  |  |
| 14 | Credit rate | credit_rate | `Number` |  |  |
| 15 | Credit amount | credit_amount | `Number` |  |  |
| 16 | Fee amount | fee_amount | `Number` |  |  |
| 17 | Receive amount | receive_amount | `Number` |  |  |
| 18 | Amount convert to BCY | base_amount | `Number` |  |  |
|  | Customer information |  |  |  |  |
| 19 | Customer type | customer_type | String |  |  |
| 20 | Customer code | custommer_code | String |  |  |
| 21 | Full name | full_name | String |  |  |
| 22 | Paper type | paper_type | String |  |  |
| 23 | Paper number | paper_number | String |  |  |
| 24 | Telephone | phone_mobile | String |  |  |
| 25 | Address | address | String |  |  |
| 26 | Nationality | nationality | String |  |  |
| 27 | Description | description | String |  |  |
| 28 | Fee data | fee_data | Array Object |  |  |
|  | IFC code | ifc_code | `Number` |  |  |
|  | Value | ifc_value | `Number` |  |  |
|  | Fee | ifc_amount | `Number` |  |  |
|  | Floor | floor_value | `Number` |  |  |
|  | Ceiling | ceiling_value | `Number` |  |  |
|  | Currency | currency_fee_code | String |  |  |
|  | Currency account code | currency_account_code | String |  |  |
|  | Payable rate | payrate | `Number` |  |  |
|  | Payable source | pay_source | String |  |  |
|  | Round amount | round_amount | `Number` |  |  |
|  | Round rate | round_rate | `Number` |  |  |
|  | Share amount | share_amount | `Number` |  |  |
|  | Share fee apply | share_fee | `Number` |  |  |
|  | Share rate | share_rate | `Number` |  |  |
| 29 | Transaction references | reference_id | String |  |  |
| 30 | Transaction status |  | String |  |  |
| 31 | Transaction date |  | `Date time` |  |  |
| 32 | User id |  | `Number` |  |  |
|  | Step code: `ACT_EXECUTE_POSTING` |  |  |  |  |
| 33 | Posting data | entry_journals | JSON Object |  |  |
|  | Group | accounting_entry_group | `Number` |  | số nguyên dương |
|  | Index in group | accounting_entry_index | `Number` |  | số nguyên dương |
|  | Posting side | debit_or_credit | String |  | D or C |
|  | Account number | bank_account_number | String |  |  |
|  | Amount | amount | `Number` |  | số có hai số thập phân |
|  | Currency | currency_code | String |  |  |

## 1.2 Transaction flow
**Conditions:**
-	Currency of Debit side and Credit side must be different.
-	Amount debit larger than zero.
-	Amount credit larger than zero.
-	If Customer exchange from deposit account then make sure "Available balance" smaller or equal debit amount, deposit account exists with status in `[Normal (N), New (W)]`, deposit type in `[Current (C), Saving (S))]`.
-	If Bank exchange amount by cash then teller do transaction must have enough cash at counter.
-	If transaction relative with GL account (debit side / credit side). It must be existing in system.
-	IFC List: 
    +	IFC Type is “Custom fee”.
    +	IFC currency is currency of Credit side.
    +	Fee amount is a percentage with amount of Credit side or a flat amount.

**Flow of events:**
-	Exchange rate allows exchanging 9 decimal digits.
-	Default:
	+ Debit rate get from table "foreign exchange" = Cash buy (CB)
Notes: If Debit by "Cash" with tiny amount will get Cash small (CS)
	+ Credit rate get from table "foreign exchange" = Cash sell (CA)
	+ Cross rate = Debit rate / Credit rate
-	If Cross rate < 1, system is more flexible about show exchange rate on voucher. It can be:
Reverse rate = 1 / Cross rate.
-	If user do any action that impact to change Cross rate. System will suggest approve.
-	Transaction complete, system will be posting auto. GL FX clearing must be declared in system, refer to “Foreign exchange account definition”.
-	Example declare FX account for exchange between USD and KHR
	+ Branch: 0002.
	+ USD exchange with KHR: 000202386002010000000
	+ KHR exchange with USD: 000201296801000000000
-	Show total fee amount in the transaction, FX receipt voucher (if have).
-	Fee deducted from Credit side by deposit account, cash or accounting.

**Posting**
| No | Debit or Credit | System account name | Amount field | Currency |
| -- | --------------- | ------------------- | ------------ | -------- |
| 1 | D | DEPOSIT (Debit currency) | Debit amount | Debit currency |
| 1 | C | Debit currency_FX_Credit currency | Debit amount | Debit currency |
| 2 | D | Credit currency_FX_Debit currency | Credit amount | Credit currency |
| 2 | C | DEPOSIT (Credit currency) | Credit amount | Credit currency |
| Case collect fee (fee amount > 0) | | | | |
| 3 | D | DEPOSIT (Credit currency) | Fee amount | Credit currency |
| 3 | C | IFCC (income) | Fee amount | Credit currency |

**Voucher:**
- `A1`, `A18`

## 1.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | Không tồn tại |
|  | {} status is invalid | Trạng thái không hợp lệ |

# 2. Reverse
## 2.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- | 
| 1 | Tran reference |  | `Yes` | String | | | |
| 2 | Username |  | `Yes` | String | | | |
| 3 | Password |  | `Yes` | String | | | |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |
| 1 | Tran reference |  | String | | | |
| 2 | Transaction status |  | String | | | |
| 3 | User approve |  | String | | | |

## 2.2 Transaction flow
**Conditions:**
- Transaction reference: exists with status "Completed".

**Flow of events:**
- System will change:
    - Transaction status to "Reversed".
    
## 2.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | Invalid user | User không hợp lệ |
|  | Username/ Password is incorrect | Không đúng Username/ Password |
|  | Reverse date is incorrect | Sai ngày reverse |
|  | Transaction is not allowed to be deleted | Giao dịch không cho phép reverse |
|  | Invalid account status | Trạng thái của tài khoản không hợp lệ |

# 3. View
## 3.1 Field description
### Request message

**Workflow id:** ``

| No | Field name | Parameter (NEW) | Is require | Type | Length | Default value | Description | Parameter (OLD) |
| -- | ---------- | --------------- | ---------- | ---- | ------ | ------------- | ----------- | --------------- |
| 1 | Transaction reference |  | `Yes` | String |  |  |  |

### Response message
| No | Field name | Parameter | Type | Length | Description |
| -- | ---------- | --------- | ---- | ------ | ----------- |

## 3.2 Transaction flow
**Conditions:**
- Transaction reference: exists.

**Flow of events:**
- System will show transaction information again.

**Database:**


## 3.3 List of error code
| Error code | Error description | Error explanation |
| ---------- | ----------------- | ----------------- |
|  | {} is required | Không được để trống |
|  | {} format is incorrect | Không đúng định dạng |
|  | {} does not exist | không tồn tại |

# 4. Rule function
| No | Rule function (OLD) | Rule function (NEW) | Description | Request | Response |
| -- | ------------------- | ------------------- | ----------- | ------- | -------- |
| 1 | GET_INFO_CCRRATE | FX_GET_FX_RATE | [FX_GET_FX_RATE](Specification/Common/20 FX Rulefuncs) | `debit_currency_code` = `debit_currency_code`, `credit_currency_code` = `credit_currency_code`, `debit_rate_type` = `CB`, `credit_rate_type` = `CA`, `branch_id_fx` = `branch_id` của branch đang làm giao dịch |  |
| 2 | GET_INFO_PGEXR | FX_RULEFUNC_GET_FX_BCYRATE | [FX_RULEFUNC_GET_FX_BCYRATE](Specification/Common/20 FX Rulefuncs) | `currency_code` = `debit_currency_code`, `rate_type` = `CB`, `branch_id_fx` = `branch_id` của branch đang làm giao dịch | `bcy_rate` = `debit_rate` |
| 3 | GET_INFO_CTXEXR | FX_RULEFUNC_GET_FX_BCYRATE | [FX_RULEFUNC_GET_FX_BCYRATE](Specification/Common/20 FX Rulefuncs) | `currency_code` = `credit_currency_code`, `rate_type` = `CA`, `branch_id_fx` = `branch_id` của branch đang làm giao dịch | `bcy_rate` = `credit_rate` |
| 4 | GET_INFO_PCSCCR_ACT | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) | `bank_account_number` = `debit_account_number` | `currency_code` = `debit_currency_code` |
| 5 | GET_INFO_PCSCCR_DPT | DPT_GET_INFO_DPTACC_CASA | [DPT_GET_INFO_DPTACC_CASA](Specification/Common/15 Deposit Rulefuncs) | `account_number` = `debit_account_number` | `currency_code` = `debit_currency_code` |
| 6 | GET_INFO_PGACC_DPT | DPT_GET_INFO_DPTACC_CASA | [DPT_GET_INFO_DPTACC_CASA](Specification/Common/15 Deposit Rulefuncs) | `account_number` = `credit_account_number` | `currency_code` = `credit_currency_code` |
| 7 | GET_INFO_PGACC_ACT | ACT_ACCHRT_GET_INFO_ACNO | [ACT_ACCHRT_GET_INFO_ACNO](Specification/Common/11 Accounting Rulefuncs) | `bank_account_number` = `credit_account_number` | `currency_code` = `credit_currency_code` |
| 8 | GET_INFO_CTM | CTM_GET_INFO_FULLNAME | [CTM_GET_INFO_FULLNAME](Specification/Common/12 Customer Rulefuncs) | `customer_code` = `custommer_code`, `customer_type` = `customer_type` | `fullname` = `full_name`, `paper_number` = `paper_number`, `address` = `address`, `nation` = `nationality`, `paper_type` = `paper_type`, `phone_mobile` = `phone_mobile` |
| 9 | LKP_DATA_ACNO | ACT_ACCHRT_LOOKUP | [ACT_ACCHRT_LOOKUP](Specification/Common/11 Accounting Rulefuncs) | `account_level_from` = 7, `account_level_to` = 7 |  |
| 10 | LKP_DATA_PGOACC_DPT | DPT_LOOKUP_DEPOSIT_CASA | [DPT_LOOKUP_DEPOSIT_CASA](Specification/Common/15 Deposit Rulefuncs) |  |  |
| 11 | LKP_DATA_CTM | CTM_LOOKUP_CTM_BY_CTMTYPE | [CTM_LOOKUP_CTM_BY_CTMTYPE](Specification/Common/12 Customer Rulefuncs) |  |  |
| 12 | GET_INFO_IFCCD | TRAN_FEE_ACT_FEE_GET_INFO_IFCCD | [TRAN_FEE_ACT_FEE_GET_INFO_IFCCD](Specification/Common/21 IFC Rulefuncs) | `ifc_code` = `ifc_code`, `currency_code` = `credit_currency_code` |  |
| 13 | LKP_DATA_IFCCD | IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY | [IFC_LOOKUP_IFCTYPE_CO_BY_CURRENCY](Specification/Common/21 IFC Rulefuncs) | `currency_code` = `credit_currency_code` |  |