# Mô tả vấn đề

Trước thời điểm `2022-Aug-13`, trong Neptune server chỉ có 1 khái niệm token mà thôi. Token này tương đương token Dynamic trong phần mô tả dưới đây.

Hiện tại để truy cập vào Neptune Server, client phải thực hiện `get-token` bằng cặp username/password. Chẳng hạn, `CMS` service phải lưu thông tin này trong hệ thống tham số của mình và dùng tham số này gọi api get-token để sinh token mới mỗi khi cần truy cập API. Điều này có một số hạn chế:

- Thông tin đăng nhập được lưu trữ ngoài hệ thống, có khả năng rò rỉ

- Tốc độ truy cập API bị giảm vì phải liên lục gọi api get-token

- Gây thừa số lượng JWT phải quản lý phía Neptune server

# Giải pháp

Để khắc phục các hạn chế trên, token trong Neptune server được chia thành 2 loại:

- `Static`: được tạo qua api `api/auth/get-static-token`. Chức năng này cho phép chủ động tạo token bằng username, password, và thời gian hết hạn của token.

```json
{
    "user_code": "admin",
    "expiration_in_minutes": 10000
}
```

`Static token` thường được dùng cho các dịch vụ nội bộ trong hệ thống Neptune chẳng hạn CMS service hay Event service, nơi mà việc cấp tài khoản truy cập được tin cậy hoàn toàn.

- `Dynamic`: được tạo qua api `api/auth/get-token` (như quy trình hiện tại). Tất cả các token trước giờ đang được quản lý đều thuộc nhóm token `Dynamic`

```json
{
    "user_code": "neptune",
    "password": "123456"
}
```

`Dynamic token` được cấp cho các bên thứ ba để truy cập vào hệ thống Neptune mà không cần phải cung cấp user name và password. Chẳng hạn, có thể cấp Dynamic token cho internet banking để truy cập vào Neptune và thực hiện giao dịch liên quan đến tài khoản khách hàng trên Corebanking. Khi cần thu hồi lại token chỉ cần vào chức năng quản lý và hủy bỏ token mà không cần phải thay đổi password của user để thu hồi lại quyền trên token.

# DDL

- Tạo bảng mới `JWT_STATIC`, có cấu trúc giống với cấu trúc bảng JWT, dùng để lưu trữ token tĩnh được tạo ra

```sql
CREATE TABLE `JWT_STATIC` (
  `ID` varchar(1000) NOT NULL,
  `token` varchar(1000) NOT NULL,
  `valid_from` bigint NOT NULL,
  `valid_to` bigint NOT NULL,
  `user_id` varchar(1000) NOT NULL,
  `user_code` varchar(1000) NOT NULL,
  `user_name` varchar(1000) NOT NULL,
  `organization_id` varchar(1000) NOT NULL,
  `organization_code` varchar(1000) NOT NULL,
  `organization_name` varchar(1000) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_JWT_STATIC_token` (`token`)
) ENGINE=InnoDB;
```

# API tạo token tĩnh

- `/api/auth/get-static-token`: đây là api dùng để lấy token tĩnh, tương tự api `/api/auth/get-token` nhưng api này phải cần `dynamic token` trong header mới có thể thực hiện được. Có nghĩa là phải dùng `username` & `password` để lấy `dynamic` token trước rồi sau đó dùng `dynamic token` này để xác thực việc tạo `static token`.

- Để tạo ra static token, đầu vào cần thông tin:
  - `username`: user name đang có hiệu lực trong hệ thống
  - `expiration-in-minutes`: số phút cấp cho token có hiệu lực

> _Chú ý_: định kỳ hệ thống sẽ quét và xó bỏ tất cả các token bao gồm dynamic và static hết hiệu lực. Tuy nhiên các token này cũng có thể được hủy bỏ trước thời hạn bằng các chức năng do người dùng thực hiện.