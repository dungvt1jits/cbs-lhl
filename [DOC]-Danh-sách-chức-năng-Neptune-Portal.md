
# Administration

Cung cấp các chức năng quản trị

## Home/Administration/Organization

Quản lý các tổ chức

## Home/Administration/Role

Quản lý Role trên Neptune Server

## Home/Administration/User

Quản lý User trên Neptune Server

# Configuration

Cung cấp chức năng cấu hình Neptune Server

## Process

Cấu hình tiến trình thực hiện Backend và Frontend của CMS service

### Home/Configuration/Process/Backend

Quản lý tiến trình backend

### Home/Configuration/Process/Frontend

Quản lý tiến trình Frontend

## Home/Configuration/API Management

Cấu hình API mà CMS có sử dụng

## Home/Configuration/Application Manangement

Quản lý App trong Neptune system

## Home/Configuration/Application Role

...

## Home/Configuration/Menu Manangement

Quản lý menu hệ thống

# Home/Monitoring

## Home/Monitoring/Dashboard

`Dash board` theo dõi giao dịch thực hiện trên Neptune Server

## Home/Monitoring/Logs

Theo dõi Log trên Neptune server: API, Grpc

## Home/Monitoring/Running instances

Theo dõi các instance đang chạy trên Neptune server

## Home/Monitoring/Service logs

Theo dõi service logs

## Home/Monitoring/Workflow Execution Monitoring

Theo dõi thực thi workflow

# System

Các chức năng hệ thống

## Home/System/Configuration

Cấu hình tham số hệ thống

## Home/System/Download

Download file cấu hình `DataFeeding.json`. File này được dùng để tạo môi trường lúc đầu.

## Home/System/Reload cache

Reload cache trên Neptune server. Mỗi khi có thay đổi các danh mục cache thì chạy lại chức năng này.

## Home/System/Session Management

Quản lý session/JWT trên Neptune server

# Khác

## Home/Import Licence

Thay đổi Licence trên hệ thống Neptune Server.

## Home/Service Management

Quản lý Service trên Neptune Server

## Home/Workflow Management

Quản lý Workflow trên Neptune server

