# Mô tả

Để thực hiện chức năng Load Balancing cho Neptune, thử Scale up service CMS chạy với 3 instances với service Nginx làm phần mêm chia tải. Đảm bảo Nginx phân phối đồng đều đến các instances.

## Cấu hình container nginx

```
nginx:
    image: ${DOCKER_REGISTRY-}nginx
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf 
    ports:
      - 80:80 
```

Chú thích:

1. Cấu hình nginx chạy với config trong file `./nginx.conf`
2. Export port `80` ra bên ngoài để lắng nghe port `80` bên trong nginx.

## Cấu hình file nginx.conf

```
events {
    worker_connections 768;
}

http {
  upstream cms {                # Cấu hình upstream 3 instance theo container name
  server cms.api;
  server cms1.api;
  server cms2.api;
}

  server {
    underscores_in_headers on;    # Nginx mặc định sẽ mất các header underscrose. Thêm dòng này để cho phép underscrose.
    location / {
    proxy_pass http://cms;       # Cấu hình proxy trỏ đến upstream cms đã khai báo. Mặc định sẽ lắng nghe port 80.
    proxy_http_version 1.1;      
    proxy_set_header   Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header   Host $host;
    proxy_cache_bypass $http_upgrade;
    proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header   X-Forwarded-Proto $scheme;
    proxy_pass_request_headers      on;
    }
  }
}
```

- CMS cần cấu hình thêm các proxy header để nginx có thể trỏ đúng request đến các instance vì tính chất của HTTP Request và Websocket. Các service khác có thể không cần.

## Cấu hình lại bảng Setting của CMS

![image](uploads/443875ead7e24601c929e2462ad35181/image.png)

1. `CMSSetting.Hostport` để port export ra bên ngoài của nginx
2. `CMSSetting.TemplateHostDev` sửa lại thành domain của nginx ( mặc định port 80)

## Unit test

![E6AF60D4-AFA3-40B6-982E-BF35A336157F](uploads/59930ffd3f52270f304b4df567760067/E6AF60D4-AFA3-40B6-982E-BF35A336157F.MP4)

![image](uploads/b491cf4853e10ddd1441cf913d0cdfeb/image.png)
1. Truy cập CMS qua Nginx (port 80)
2. Login vào ứng dụng Neptune
3. Chia tải cho cả 3 service (mặc định của nginx đang là `round-robin`) 
4. Kết nối websocket, lưu vào redis
5. Gọi api thành công.
