
# B_CRD_NPL_GEN

```sql
B_CRD_NPL_GEN (
    @TransactionNumber VARCHAR(36),
    @WorkingDate DATE,
    @UserCode NVARCHAR(5),
    @UserName NVARCHAR(250),
    @BatchDate DATETIME,
    @StepName NVARCHAR(200),
    @ReferenceId NVARCHAR(200) = NULL,
    @ReferenceCode NVARCHAR(200) = NULL,
    @TranId NVARCHAR(200) = NULL,
    @Description NVARCHAR(200) = NULL,
    @BusinessCode NVARCHAR(200) = NULL,
    @ChannelID NVARCHAR(200) = NULL
    )
```


- Gọi trong batch, thực hiện tính toán chuyển nhóm nợ cho các tài khoản Loan theo quy định được cấu hình trong bảng NPLRULE
- Ghi dữ liệu vào bảng MaterTrans: `CreditAccountTrans`, `ClassificationTrans`, `ProvisioningTrans`
- Sinh bút toán GLEntries theo Mastertrans

Trong SP này, có dùng đến các SP sau:
- `B_CRD_NPL_GEN_GenerateGLEntriesFromClassificationTrans`
- `B_CRD_NPL_GEN_GenerateGLEntriesFromCreditAccountTrans`
- `B_CRD_NPL_GEN_GenerateGLEntriesFromProvisioningTrans`


# GenerateGLEntriesFromCreditAccountTransByTransactionNumber

```sql
GenerateGLEntriesFromCreditAccountTransByTransactionNumber (@TransactionNumber VARCHAR)
```

- Tạo bút toán GLEntries cho một transaction nào đó, theo số `@TransactionNumber`
- SP này được gọi trong các giao dịch FO cần hạch toán tức thời căn cứ theo dữ liệu được ghi trong MasterTrans bởi logic của giao dịch FO