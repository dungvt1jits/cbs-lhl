# Mô tả

Ngoài các sự kiện về thực thi workflow mà Neptune đã phân phối cho các service, vẫn còn các tình huống về sự kiện nội tại của Neptune server cần được phân phối đển service để có ứng xử lý phù hợp. Ví dụ:
- Khi Neptune server xóa bỏ dữ liệu thực thi của workflow loại [READONLY] thì các dữ liệu liên quan phía service cũng cần được xóa bỏ theo
- Hoặc trong tiến trình Archiving hoặc Purging từ trong Nepune server thì các service cũng cần xử lý tương tự, vì nếu giữ riêng các dữ liệu này cũng không có ý nghĩa gì.

# Giải pháp
## Xem lại hiện trạng
Neptune server sẽ định nghĩa thêm các loại sự kiện mới tương ứng với quy trình nội tại của Neptune server. Mỗi khi phát sinh các sự kiện này, nó sẽ phân phối message đến tất cả các service có trạng thái Active.

Khác với các sự kiện liên quan đến thực thi của Workflow (service nào có đăng ký thì mới nhận được), các sự kiện này khá quan trọng nên nó sẽ gửi đến tất cả các service và các service có xử lý hay không thì sẽ tự quyết định.

Về dữ liệu của sự kiện:
- Dữ liệu phân phối theo sự kiện Neptune có cấu trúc như sau:

```c#
public class NeptuneEventDataScheme
{
   public WorkflowExecutionInforResponse execution_data { get; set; } = new();
}
```

- Cấu trúc này sẽ được bổ sung thêm nhóm dữ liệu có kiểu dữ liệu mới cho từng loại sự kiện để việc định kiểu rõ ràng hơn. Trong tương lai nếu khi phát sinh loại sự kiện mới thì kiểu dữ liệu của các sự kiện mới này cũng sẽ được bổ sung dần vào gói `NeptuneClient`.

## Các sự kiện mới

### Sự kiện `Archiving`

-  `Archiving` là tiến trình backup dữ liệu từ database online sang database dự phòng (cho mục đích lưu dự phòng, báo cáo, datawarehouse...)


### Sự kiện `Purging`
- Tương tự, `Purging` là tiến trình hủy bỏ dữ liệu trên database online và database archiving tùy theo thời điểm đến hạn thực hiện.

### Dữ liệu xử lý

- Đơn vị dữ liệu `Archiving` hoặc `Purging` xử lý chính là từng Workflow mà đại diện là `ExecutionID`.

- Do dữ liệu thực thi của `workflow` đã tồn tại ở từng `service` (nếu có tham gia xử lý trước đó) nên việc gửi toàn bộ thông tin workflow execution cho service là không cần thiết.

- Hơn nữa khi xử lý `Archiving` hay `Purging`, `Neptune server` xử lý theo từng lô dữ liệu thực thi, nên dữ liệu về sự kiện này nên được gửi theo từng lô để tối ưu băng thông mạng.

- Chẳng hạn, khi `Archiving` 1000 `workflow`s, `Neptune server` sẽ gửi một message với nội dung chứa 1000 `ExecutionID` cho các `service` liên quan. Khi service nhận được message, service sẽ có được một `List<string>` các `ExecutionID` và sẽ thực hiện các xử lý tương ứng.

- Vì tính chất quan trọng của sự kiện, nên các message được gửi lên `queue-event` và sẽ không bao giờ timeout để đảm bảo service luôn nhận được sự kiện.

- Cấu trúc mới
```c#
public class WorkflowStepExecutionPair
{
    public string execution_id { get; set; }
    public string execution_step_id { get; set; }
}

public class PurgingEventDataScheme
{
    public List<WorkflowStepExecutionPair> workflow_step_execution_list { get; set; }
}

public class ArchivingEventDataScheme
{
    public List<WorkflowStepExecutionPair> workflow_step_execution_list { get; set; }

}

public class NeptuneEventDataScheme
{
    public WorkflowExecutionInforResponse execution_data { get; set; } = new();
    public ArchivingEventDataScheme archiving_data { get; set; } = null; // added on 2022-Aug-13
    public PurgingEventDataScheme purging_data { get; set; } = null; // added on 2022-Aug-13
}

```

Các sự kiện về workflow execution hiện tại:

```c#
public enum NeptuneWorkflowEventTypeEnum
{
    EventWorkflowRegistered,
    EventWorkflowCompleted,
    EventWorkflowError,
    EventWorkflowTimeout,
    EventWorkflowCompensated,
    EventWorkflowReversed,

    EventWorkflowStepStart,
    EventWorkflowStepCompleted,
    EventWorkflowStepError,
    EventWorkflowStepTimeout,
    EventWorkflowStepCompensated,

    EventWorkflowsArchived, // added on 2022-Aug-13
    EventWorkflowsPurged, // added on 2022-Aug-13
}
```

- `EventWorkflowsArchived`: là sự kiện các workflows đã được archived xong.

- `EventWorkflowsPurged`: là sự kiện các workflows đã được Purged xong.

### Service xử lý các sự kiện

- Các service khi nhận được sự kiện, không nhất thiết phải xử lý ngay mà có thể ghi nhận và xử lý lúc hệ thống có dư nguồn lực (CPU, Memory...)

- Do phân phối theo lô, nên có thể một số service sẽ nhận được dữ liệu Execution mà DB của mình không có. Lúc này chỉ việc bỏ qua không xử lý là được.