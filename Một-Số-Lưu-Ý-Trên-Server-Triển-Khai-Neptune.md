Một số điểm lưu ý sáng nay:
- Server:
  + Server 192.168.1.230 là server CHÍNH để dev (cả DESIGN FORM và làm chức năng) và chỉ hỗ trợ backup dữ liệu trên server này
  + Server 180.77.15.166 là server phụ

- Debug:
  + Server 230: chỉ được debug "KO ĐĂNG KÝ" trên server này để đảm bảo server luôn hoạt động đầy đủ chức năng
  + Server 166: debug tự do, có thể tắt service để debug
  + Ưu tiên sử dụng Controller và Swagger để debug, nhằm tăng hiệu suất công việc và giảm thiểu thời gian chết

- Data:
  + Dữ liệu hệ thống và dữ liệu 'ít thay đổi' là QUAN TRỌNG và có nhiều ảnh hưởng đến các chức năng khác, phải chuẩn hóa migration dữ liệu này
  + Dữ liệu người dùng là cần thiết, cần tạo migration để tạo ít nhất 3 dữ liệu mẫu CHUẨN
  + Ưu tiên tập chung chuẩn hóa format và dữ liệu ACNO, và ưu tiên COMMIT phiên bản này trước để deploy server