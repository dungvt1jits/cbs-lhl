# Mô tả

Ta có giao dịch chuyển khoản giữa 2 tài khoản `CASA` khác chi nhánh:
- `102-00001`: thuộc chi nhánh 102
- `202-00002`: thuộc chi nhánh 202

Theo quy trình mới, bút toán chuyển khoản sẽ có dạng sau tại service `DPT`:
| Dr/Cr | Account | Amount | Branch |
|-------|---------|--------|--------|
| DR    | DEPOSIT | 1100   | 102    |
| CR    | DEPOSIT | 1000   | 102    |
| CR    | IFC     | 100    | 202    |

Nếu xét theo chi nhánh, bút toán sẽ không cân.

# Quy trình mới trên bút toán n-n

Xem lại cấu trúc bảng MasterTrans

![image](uploads/de0d0625854f5671744ef01283851262/image.png)

Sau khi tạo dữ liệu cho dòng Nợ trong bảng `DepositAccountTrans` với `CrossBranchCode` = `202`. Tại bước sinh GLEntries, căn cứ vào bảng `xxxGLConfig`:

- Tạo dòng bút toán chính

| Dr/Cr | Account | Amount | Branch |
|-------|---------|--------|--------|
| DR    | DEPOSIT | 1100   | 102    |
| CR    | DEPOSIT | 1000   | 102    |
| CR    | IFC     | 100    | 202    |

> Mỗi MasterTrans chỉ sinh bút toán theo cấu hình của riêng mình. Ví dụ trường hợp chuyển khoản giữa 2 tài khoản DPT thì sẽ có 2 dòng MasterTrans tương ứng với 2 account DPT khác nhau.

# Đồng bộ số liệu GLEntries tại service chức năng lên service ACT

Tại bước đồng bộ số liệu GLEntries từ các service chức năng lên ACT.GLEntries. Các bút toán sẽ xảy ra tình trạng không cân theo chi nhánh. Lúc này cần phải thực hiện quy trình `IBTClearing` để cân lại theo chi nhánh.

Các trường hợp:

## Branch to branch (1-n)

Giả sử ta có bút toán như sau:

|Dòng | Dr/Cr | Account | Amount | Branch |
|-----|-------|---------|--------|--------|
|1    | DR    | DEPOSIT | 1100   | 102    |
|2    | CR    | DEPOSIT | 1000   | 202    |
|3    | CR    | IFC     | 100    | 202    |

- Xác định bút toán cần phải thực hiện IBT nhờ vào mã chi nhánh bên Nợ và bên Có khác nhau (102 vs 202)
- Tiến hành tạo entry IBT từ chi nhánh đến HO và ngược lại. Xét từng dòng, tạo entry mới tương ứng:
  - Dr/Cr: đảo giá trị
  - Account: dùng tài khoản cấu hình cho GL IBT, với chi nhánh cùng chi nhánh dòng đang xét và Sub Account là mã chi nhánh đối ứng
  - Amount: cùng giá trị
  - Branch: cùng giá trị

Sau khi thực hiện clearing, ta có các dòng bút toán sau:

| Dòng | Dr/Cr | Account | Amount | Branch |
|------|-------|---------|--------|--------|
| 1    | D     | CASH    | 100    | 101    |
| *    | C     | IBT-HO  | 100    | 101    |
|      |       |         |        |        |
| 2    | C     | DEPOSIT | 100    | 202    |
| *    | D     | IBT-HO  | 100    | 202    |
|      |       |         |        |        |
| *    | D     | IBT-101 | 100    | HO     |
| *    | C     | IBT-202 | 100    | HO     |


## Branch to branch (n-n)

| Dòng | Dr/Cr | Account | Amount | Branch |
|------|-------|---------|--------|--------|
| 1    | D     | CASH    | 100    | 101    |
| 2    | D     | DEPOSIT | 200    | 202    |
| 3    | C     | CREDIT  | 280    | 303    |
| 4    | C     | IFC     | 20     | 404    |


Ứng với mỗi dòng bút toán luôn sinh ra tài khoản IBT đối ứng:
- Dr Branch - HO
- HO - Cr Branch
- HO-IBT(Dr Branch) - HO-IBT(Cr Branch)

| Dòng | Dr/Cr | Account | Amount | Branch |
|------|-------|---------|--------|--------|
| 1    | D     | CASH    | 100    | 101    |
| *    | C     | IBT-HO  | 100    | 101    |
|      |       |         |        |        |
| 2    | D     | DEPOSIT | 200    | 202    |
| *    | C     | IBT-HO  | 200    | 202    |
|      |       |         |        |        |
| 3    | C     | CREDIT  | 280    | 303    |
| *    | D     | IBT-HO  | 280    | 303    |
|      |       |         |        |        |
| 4    | C     | IFC     | 20     | 404    |
| *    | D     | IBT-HO  | 20     | 404    |
|      |       |         |        |        |
| *    | D     | IBT-101 | 100    | HO     |
| *    | D     | IBT-202 | 200    | HO     |
| *    | C     | IBT-303 | 280    | HO     |
| *    | C     | IBT-404 | 20     | HO     |

## Branch-to-HO hoặc HO-to-Branch

| Dòng | Dr/Cr | Account | Amount | Branch |
|------|-------|---------|--------|--------|
| 1    | D     | CASH    | 100    | HO     |
| 2    | D     | DEPOSIT | 200    | HO     |
| 3    | C     | CREDIT  | 280    | 303    |
| 4    | C     | IFC     | 20     | 404    |

Xử lý tự động

| Dòng | Dr/Cr | Account | Amount | Branch |
|------|-------|---------|--------|--------|
| 1    | D     | CASH    | 100    | HO     |
| 2    | D     | DEPOSIT | 200    | HO     |
| *    | C     | IBT-303 | 280    | HO     |
| *    | C     | IBT-404 | 20     | HO     |
|      |       |         |        |        |
|      |       |         |        |        |
| 3    | C     | CREDIT  | 280    | 303    |
| *    | D     | IBT-HO  | 280    | 303    |
|      |       |         |        |        |
| 4    | C     | IFC     | 20     | 404    |
| *    | D     | IBT-HO  | 20     | 404    |

**Tổng quát công thức**:
- Luôn sinh chân đối ứng cho từng entries theo nguyên tắc đảo Dr/Cr thành Cr/Dr
- Nếu mã chi nhánh cả 2 bên không là HO thì sinh thêm một bút toán IBT với số lượng mỗi entry của từng bên tương ứng với từng dòng
- Nhóm các entry IBT lại theo tài khoản từng bên
