# Mô tả

Để đáp ứng nhu cầu in report cho user, Neptune triển khai chức năng Report kết hợp Stimulsoft để thực hiện chức năng làm việc với report.

## Giải pháp

Tạo app Neptune Report để người dùng có thể làm việc với các report, mỗi tài khoản sẽ được phân quyền để thấy được các report khác nhau.

Triển khai service Report phục vụ việc hiển thị Report bằng thư viện Stimulsoft, áp dụng LinQ để sử dụng nhiều loại database.

## Hướng dẫn cấu hình

### Cấu hình TemplateReport

```c#
public partial class TemplateReport : BaseEntity
{
    public TemplateReport() { }

    public string Code { get; set; }

    public string Status { get; set; }

    public string FileContent { get; set; }

    public string App { get; set; }

    public string Description { get; set; }

    public string Version { get; set; }

}
```

Chú thích:

- Code: mã định danh của report
- Status: trạng thái của report (A: Active, I:Inactive)
- FileContent: nội dung của file report (dạng base64)
- App: application code của report
- Description: mô tả về report
- Version: phiên bản của report

### Cấu hình ReportConfig

```c#
public partial class ReportConfig : BaseEntity
{
  
    public ReportConfig() { }

    public string Code { get; set; }   

    public string CodeTemplate { get; set; }

    public string Status { get; set; }

    public string DataSource { get; set; }

    public string Description { get; set; }

    public string Version { get; set; }

    public string FullClassName { get; set; }

    public string MethodName { get; set; }

    public bool IsAsync { get; set; }
}
```

Chú thích:

- Code: mã định danh của config
- CodeTemplate: mã định danh của template tương ứng
- Status: trạng thái của report config (A: Active, I:Inactive)
- DataSource: tên data source mà dev muốn gán vào
- Description: mô tả về report config
- Version: phiên bản của report config
- FullClassName: full name space và class name chứa method cần thực thi lúc load report
- MethodName: tên function của method cần thực thi lúc load report
- IsAsync: method có cần thực hiện bất đồng bộ hay không

### Cấu hình Viewer Setting

```c#
public partial class ViewerSetting : BaseEntity
{
    public ViewerSetting() { }
   
    public string CodeTemplate { get; set; }
  
    public Boolean ShowPrintButton { get; set; } = true;
    
    public Boolean ShowOpenButton { get; set; } = false;
   
    public Boolean ShowSaveButton { get; set; } = true;
    
    public Boolean ShowSendEmailButton { get; set; } = false;
   
    public Boolean ShowDesignButton { get; set; } = false;
    
    public Boolean ShowResourcesButton { get; set; } = true;
   
    public Boolean UseCompression { get; set; } = false;
 
    public int CacheTimeout { get; set; } = 20;
    
    public int RequestTimeout { get; set; } = 60;
   
    public string CacheMode { get; set; } = "None";
}
```

Chú thích:

- CodeTemplate: mã định danh template report
- ShowPrintButton: hiển thị nút Print
- ShowOpenButton: hiển thị nút Open
- ShowSaveButton: hiển thị nút Save
- ShowSendEmailButton: hiển thị nút Email
- ShowDesignButton: hiển thị nút Design
- ShowResourceButton: hiển thị nút Resource
- UseCompresession: Enables compression of the viewer requests into the GZip stream. That allows to decrease the amount of internet traffic but slows down the viewer slightly. The default value of the property is `false`.
- CacheTimeout: Sets the time in minutes that the server will store the report since the last action of the viewer. The default value is `20` minutes.
- RequestTimeout: Sets the response timeout from the server in seconds, after which an error will be generated. The default value is `60` seconds. For big reports, it is recommended to increase this value.
- CacheMode: Sets the report caching mode. It can take one of the following values of the `StiServerCacheMode` enumeration:
- `None` – caching is disabled. The report will be reloaded each time using the GetReport event;
- `ObjectCache` – the cache is used as the storage, the report is stored as an object (default value);
- `ObjectSession` – the session is used as the storage, the report is stored as an object;
- `StringCache` – the server cache is used as the storage, the report is serialized to a packed string;
- `StringSession` – the session is used as storage, the report is serialized into a packed string.

### Cấu hình Mail Option

```c#
public partial class MailOption : BaseEntity
{
    public MailOption() { }
   
    public string Code { get; set; }
    
    public string Sender { get; set; }
    
    public string Password { get; set; }

    public string Host { get; set; }

    public int Port { get; set; }

    public Boolean EnableTLS { get; set; }
}
```

Chú thích:

- Code: mã định danh config cho email
- Sender: email của tài khoản gửi
- Password: password của tài khoản gửi, có thể dùng mật khẩu ứng dụng của gmail
- Host: địa chỉ mail server
- Port: port của mail server (465/587)
- EnableTLS: bật/tắt chế độ TLS.

### Cấu hình form

1. Tạo 1 form thuộc app Neptune Report (có app code ncbsReport)

![image](uploads/083936a85f5bc749744dced779c251b2/image.png)

2. Form này gần chứa component Iframe Report, ở tab config được cấu hình File report là Code của TemplateReport muốn hiển thị.

![image](uploads/5989df0b790ca74b57c558f96bd2bc89/image.png)

3. Tạo menu cho form này và phân quyền cho app Neptune Report.

![image](uploads/3289cee0c3ae643040127fdf20cd6287/image.png)

![image](uploads/51f21d768ed7ab00341ba2f855fa4107/image.png)

4. Kiểm tra lại ParaServer, _REPORT_URL_ đã mang đúng url `http:ip:port/View/Reports/` của service Report chưa, đây là url mà iframe sẽ hiển thị report

![image](uploads/3638dbead17a82de6729032cbdf54c39/image.png)

5. Vào app Neptune Report và xem thử Report đã tạo.

![image](uploads/29899b7e302dde5b3daad9e931f08d74/image.png)

## Hướng dẫn design file report (.mrt)

1. Tạo giao diện cho file bằng Stimulsoft Designer (app Desktop)

![image](uploads/44f2a7c1de6c8efdb10ca25b55402919/image.png)

Chú thích:

1. Định nghĩa data source cho report, ví dụ data source RPT_MTG_ACC là danh sách List of Mortgage Account, định nghĩa các columns muốn hiển thị cho data source này. Nếu khó khăn trong quá trình định nghĩa thì hãy copy Data Source từ các report có sẵn và sửa tên, sửa key lại cho phù hợp.
2. Danh sách các variable/parameter cho report dùng để filter, điều chỉnh cho phép người dùng nhập.
3. Danh sách các parameter mặc định của hệ thống mà Stimulsoft hỗ trợ cho report
4. Cần set DataSource name vào lưới hiển thị thông tin trên report, ở đây là RPT_MTG_ACC (vùng khoanh đỏ).
5. Cấu hình chức năng Sort trên header bằng cách nhấn vào item trên header, ở tab Prooperties, chọn Interaction

![image](uploads/b2b4efde332e40ce568cd955a1d9dd79/image.png)

3. Cửa sổ hiện ra, chọn phần Sort, chọn Sort theo cột tương ứng của data source

![image](uploads/3ce7b8c1cdccae70170f6f32b4a34cd7/image.png)

4. Preview để xem report đã design đúng và hiển thị parameter đầy đủ chưa

![image](uploads/852a908df33a3d54518ebf9f1a3e895d/image.png)

## Hướng dẫn lập trình

- Khai báo class và function mà trước đó đã cấu hình trong ReportConfig (FullClassName và MethodName) có cấu trúc như sau:

![image](uploads/9ae0a08fb3f868189d46903f3cebaa51/image.png)

1. Xóa dữ liệu DataSource của Dictionary trong file mrt (nếu có) để làm mới dữ liệu.
2. Đẩy dữ liệu vào report, thông qua tên của `DataSource` đã khai báo, tương ứng DataSource đã mapping trong file _.mrt_
3. Cần gọi dòng `.Dictionary.Synchronize` thì report mới đẩy data vào và view lên được.
4. Để hiển thị loading cho report thì dùng lệnh `.Render`.

**- Sử dụng các parameter đã khai báo trong report để thực hiện chức năng filter**

```c#
 var model = new
        {
            CurrencyCode = _report.GetValue("CurrencyCode"),
            CatalogCode = _report.GetValue("CatalogCode"),
            CollateralAccountStatus = _report.GetValue("Status"),
        };
```

**- Dùng biến này trong câu LinQ để query dữ liệu theo tham số**

```c#
var query = await (from mtgacc in _mortgageAccountRepository.Table.DefaultIfEmpty()
                           from mtgcat in _mortgageCatalogRepository.Table.Where(c => c.Id == mtgacc.CatalogId).DefaultIfEmpty()
                           from ctmsingle in _customerSingleRepository.Table.Where(s => s.Id == mtgacc.CustomerId).DefaultIfEmpty()
                           from ctmlinkage in _customerLinkageRepository.Table.Where(l => l.Id == mtgacc.CustomerId).DefaultIfEmpty()
                           from ctmgroup in _customerGroupRepository.Table.Where(g => g.Id == mtgacc.CustomerId).DefaultIfEmpty()
                           from matype in _codeListRepository.Table.Where(list => list.CodeId == mtgacc.CollateralAssetType && list.CodeName == "MATYPE" && list.CodeGroup == "MTG")
                           from mtgsts in _codeListRepository.Table.Where(list => list.CodeId == mtgacc.CollateralAccountStatus && list.CodeName == "MTGSTS" && list.CodeGroup == "MTG")
                           from maclass in _codeListRepository.Table.Where(list => list.CodeId == mtgacc.CollateralAssetClassification && list.CodeName == "MACLASS" && list.CodeGroup == "MTG")
                           where
                                (!string.IsNullOrEmpty(mtgacc.CurrencyCode) && !string.IsNullOrEmpty(model.CurrencyCode) ? mtgacc.CurrencyCode.Contains(model.CurrencyCode) : true)
                                && (!string.IsNullOrEmpty(mtgcat.CatalogCode) && !string.IsNullOrEmpty(model.CatalogCode) ? mtgcat.CatalogCode.Contains(model.CatalogCode) : true)
                                && (!string.IsNullOrEmpty(mtgacc.CollateralAccountStatus) && !string.IsNullOrEmpty(collateralAccountStatus) ? mtgacc.CollateralAccountStatus.Contains(collateralAccountStatus) : true)
                           select new
                           {
                               AccountNumber = mtgacc.AccountNumber,
                               AccountName = mtgacc.AccountName ?? string.Empty,
                               CurrencyCode = mtgacc.CurrencyCode,
                               CustomerCode = ((mtgacc.CustomerType == "C") ? ctmsingle.CustomerCode : (mtgacc.CustomerType == "L") ? ctmlinkage.LinkageCode : (mtgacc.CustomerType == "G") ? ctmgroup.GroupCode : string.Empty) ?? string.Empty,
                               CatalogCode = mtgcat.CatalogCode ?? string.Empty,
                               CollateralAssetClassification = maclass.Caption,
                               CollateralAccountStatus = mtgsts.Caption,
                           }).ToListAsync();
```

### Lưu ý: Data Source mà report nhận vào phải là dữ liệu mảng. Nên khi dùng hàm `.RegData` cần phải truyền vào dữ liệu là `List`,`Array` hoặc các kiểu `Queryable`. Nếu chỉ hiển thị dữ liệu đơn (giống như voucher) thì cứ thêm vào mảng có 1 phần tử.

> Script workflow được để trong folder Scripts của Service

# Unit test cho các chức năng report

## Chức năng Authentication & Authorization

1. View report với url chứa token không có quyền
2. Thông báo với mã lỗi 401 sẽ được trả ra ![image](uploads/765e320161843074111f1d55b79a2627/image.png)

## Tham số cho report. Bao gồm cả việc tạo list dữ liệu cho các trường tra cứu

1. View report List Customer trên app Neptune Report, đã có hiển thị parameters

![image](uploads/8e7834bcfa55144a5ab4117b97d2b320/image.png)

2. Thực hiện được chức năng tra cứu

![image](uploads/31934502a0011a3e6afeca302de358cc/image.png)

## Chức năng gửi mail trong report

1. Tham số cho việc gửi mail sẽ được lưu trong bảng `MailOption`, người dùng cấu hình `code` vào trường `MailConfigCode` của report cần gửi mail trong bảng `ReportConfig`.

![image](uploads/fa259b0ff6536ea73ff242793130c5bb/image.png)

![image](uploads/89515657b47328a823d709184fa201bb/image.png)

2. Sửa config trong bảng `ViewerSetting` của report muốn hiển thị chức năng send mail

![image](uploads/88f50469e66c4a95091748d736e7fec1/image.png)

3. View report, nhấn vào nút Send Mail trên toolbar, chọn kiểu file muốn gửi

![image](uploads/f2848728815ca051bb431d5b0c654f6b/image.png)

4. Cấu hình các tham số muốn gửi, tiếp theo nhập vào Email Options thông tin mail nhận

![image](uploads/c296fd7fe95beb175e4cd05d9060c725/image.png)

5. Nhấn OK và chờ kết quả.

![image](uploads/eb82759d7122f620a0ef01045c8de32a/image.png)

![image](uploads/d915c70b01ca704d53bb339d31c150bb/image.png)

## Chức năng sort dữ liệu

![2022-11-24_16-33-02](uploads/fd925c8b0e99d2c66f55eac74f4ce585/2022-11-24_16-33-02.mp4)

## Khi view report lần đầu, nếu trình duyệt sẽ ngăn chặn kết nối, hãy đóng form mở lại lần 2 sẽ được.