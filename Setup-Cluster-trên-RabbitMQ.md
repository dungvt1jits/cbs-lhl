
# Cài đặt RabbitMQ trên Docker

Giả sử tạo 3 instances RabbitMQ như sau:
```yml
version: "3"

services:
    neptune-rabbitmq-node-1:
        container_name: neptune-rabbitmq-node-1
        hostname: neptune-rabbitmq-node-1
        image: rabbitmq:management
        environment:
            RABBITMQ_DEFAULT_USER: user
            RABBITMQ_DEFAULT_PASS: password
            RABBITMQ_ERLANG_COOKIE: 'secret-value'
        ports:
            - 5672:5672
            - 15672:15672
    
    neptune-rabbitmq-node-2:
        container_name: neptune-rabbitmq-node-2
        hostname: neptune-rabbitmq-node-2
        image: rabbitmq:management
        environment:
            RABBITMQ_DEFAULT_USER: user
            RABBITMQ_DEFAULT_PASS: password
            RABBITMQ_ERLANG_COOKIE: 'secret-value'
        ports:
            - 5673:5672
            - 15673:15672

    neptune-rabbitmq-node-3:
        container_name: neptune-rabbitmq-node-3
        hostname: neptune-rabbitmq-node-3
        image: rabbitmq:management
        environment:
            RABBITMQ_DEFAULT_USER: user
            RABBITMQ_DEFAULT_PASS: password
            RABBITMQ_ERLANG_COOKIE: 'secret-value'
        ports:
            - 5674:5672
            - 15674:15672
networks:
    default:
        external:
            name: neptune-network
```

> Với biến `RABBITMQ_ERLANG_COOKIE` các phiên bản sau này có thể không dùng nữa.

![image](uploads/83b6743041a3ea5b856736a9a11b3125/image.png)

Kiểm tra trạng thái cluster trên các node:

- Node 1
![image](uploads/68a7bb966ac932e70605b47bf483ceef/image.png)

- Node 2
![image](uploads/9aab47157300e7f445be2d6441e3302a/image.png)

- Node 3
![image](uploads/8eed58cc0d5a41218322fbe40ae7ad33/image.png)

# Setup Cluster cho RabbitMQ

Ta sẽ tiến hành liên kết `Node 2`, `Node 3` vào `Node 1`

```bat
docker exec -it neptune-rabbitmq-node-2 rabbitmqctl stop_app
docker exec -it neptune-rabbitmq-node-2 rabbitmqctl reset
docker exec -it neptune-rabbitmq-node-2 rabbitmqctl join_cluster rabbit@neptune-rabbitmq-node-1
docker exec -it neptune-rabbitmq-node-2 rabbitmqctl start_app

docker exec -it neptune-rabbitmq-node-3 rabbitmqctl stop_app
docker exec -it neptune-rabbitmq-node-3 rabbitmqctl reset
docker exec -it neptune-rabbitmq-node-3 rabbitmqctl join_cluster rabbit@neptune-rabbitmq-node-1
docker exec -it neptune-rabbitmq-node-3 rabbitmqctl start_app
```

Kiểm tra lại trạng thái cluster trên các node:

- Node 1
![image](uploads/fe7cc8be12d3092bbfc28423eb59c756/image.png)

- Node 2
![image](uploads/a7b163e7ea719cff925247a835300a63/image.png)

- Node 3
![image](uploads/7305b2ceaa6765643ab20f241a8c3efa/image.png)

Ta thấy cả 3 node giờ đây đã được liên kết với nhau

Tiến hành kiểm tra trên màn hình Quản lý Administrator

![image](uploads/bd97e093ad67daf9a7c0aaa9b5ac84f4/image.png)

Ta thấy rằng cả 3 node luôn được đồng bộ dữ liệu với nhau.

# Tránh hiện tượng Single Point of Failure

Ta chỉ cần dựng một bộ Balancer rồi trỏ vào 3 nodes này. Sau đó các ứng dụng sẽ truy cập vào Balancer này.



