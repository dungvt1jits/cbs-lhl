| Project    | Class                      | Migration Time              | Description           |
|------------|----------------------------|-----------------------------|-----------------------|
| Framework  | SchemaMigration            | 2020/01/01 10:00:00:0000000 | Core data base schema |
| Framework  | TransactionSchemaMigration | 2020/01/01 10:00:01:0000000 | Transaction data base schema |
| Accounting | AccountingSchemaMigration  | 2020/01/01 12:30:00:0000000 | Accounting data base schema |
| Accounting | LocalizationMigration      | 2020/01/01 12:30:00:0000004 | Localization installed |
| Admin      | AdminSchemaMigration       | 2020/01/01 12:31:00:0000000 | Admin data base schema |
| Admin      | LocalizationMigration      | 2020/01/01 14:31:30:0000000 | Localization installed |
| Cash       | CashSchemaMigration        | 2020/01/01 12:32:00:0000000 | Cash data base schema  |
| Cash       | LocalizationMigration      | 2020/01/01 14:32:00:0000004 | Localization installed |
| Credit     | CreditSchemaMigration      | 2020/01/01 12:33:00:0000000 | Core data base schema  |
| Credit     | LocalizationMigration      | 2020/01/01 14:33:00:0000004 | Localization installed |
| Credit     | CreditSettingMigration     | 2020/01/01 14:33:30:0000004 | Credit setting install |
| Customer   | CustomerSchemaMigration    | 2020/01/01 12:34:00:0000000 | Core data base schema  |
| Customer   | LocalizationMigration      | 2020/01/01 14:34:30:0000004 | Localization installed |
| Deposit    | DepositSchemaMigration     | 2020/01/01 12:35:00:0000000 | Core data base schema  |
| Deposit    | LocalizationMigration      | 2020/01/01 14:35:00:0000004 | Localization installed |
| FixedAsset | FixedAssetSchemaMigration  | 2020/01/01 12:36:00:0000000 | Core data base schema  |
| FixedAsset | LocalizationMigration      | 2020/01/01 14:36:30:0000004 | Localization installed |
| FX         | FXSchemaMigration          | 2020/01/01 12:37:00:0000000 | FX data base schema |
| FX         | LocalizationMigration      | 2020/01/01 14:37:00:0000004 | Localization installed |
| IFC        | IFCSchemaMigration         | 2020/01/01 12:38:00:0000000 | IFC data base schema |
| IFC        | LocalizationMigration      | 2020/01/01 14:38:00:0000004 | Localization installed |
| Mortgage   | MortgageSchemaMigration    | 2020/01/01 12:39:00:0000000 | Core data base schema |
| Mortgage   | LocalizationMigration      | 2020/01/01 14:39:00:0000004 | Localization installed |
| Payment    | PaymentSchemaMigration     | 2020/01/01 12:40:00:0000000 | Core data base schema |
| Payment    | LocalizationMigration      | 2020/01/01 14:40:00:0000004 | Localization installed |

## Setup cho Neptune

Chạy script trong DB 

Chỉnh lại các thông số cho RabbitMQ

- Table ENVIRONMENT_VARIABLE
  - MESSAGE_BROKER_HOSTNAME = rabbitmq (tên đặt trong docker)
  - MESSAGE_BROKER_USERNAME = guest
  - MESSAGE_BROKER_PASSWORD = guest
  - MESSAGE_BROKER_PORT = 5672
  - GRPC_URL = https://neptuneserver:109/ (tên container của neptuneserver, và port grpc)

## Service ID
