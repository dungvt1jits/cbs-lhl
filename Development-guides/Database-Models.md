## Class Migration

```c#
using FluentMigrator;
using Jits.Neptune.Data.Extensions;
using Jits.Neptune.Data.Migrations;
using Jits.Neptune.Web.Customer.Domain;

namespace Jits.Neptune.Web.Customer.Migrations {
	/// <summary>
	/// Customer schema migrations
	/// </summary>
	[NeptuneMigration("2020/01/01 12:30:00:0000000", "Customer data base schema", MigrationProcessType.Installation)]
	public class CustomerSchemaMigration : AutoReversingMigration {
		/// <summary>
		/// Up to date migrate
		/// </summary>
		public override void Up() {
			Create.TableFor<SingleCustomer>();
		}
	}
}
```

- `Create.TableFor<TableName>` để tạo table.

### Khai báo tham chiếu khoá ngoại

```c#
Create.ForeignKey().FromTable(nameof(MainTable)).ForeignColumn(nameof(ForiegnKeyColumnName)).ToTable(nameof(ForeignTable)).PrimaryColumn(nameof(IdColumnForeignTable)).OnDelete(Rule.None);
```


## Builder
Trường hợp không khai báo builder, framework tự động tạo Table với các Field với kiểu dữ liệu tương ứng, và độ rộng maximum theo từng kiểu dữ liệu.
Để framework tạo dữ liệu với thông tin chính xác hơn, cần tạo các class builder để khai báo chi tiết. 

Thông tin cột:
- NotNullable(): cột không cho phép Null.
- Unique(): cột dữ liệu được đánh index, và không cho phép trùng.

## Auto Mapper

Để thuận tiện trong việc cập nhật giá trị qua lại giữa Entity và Model, framework sử dụng AutoMapper. Tạo class kế thừa từ BaseMapperConfiguration. Và khai báo các cặp mapping Entity vs Model.

```c#
public partial class FrameworkMapperConfiguration : BaseMapperConfiguration
{
	/// <summary>
	/// Constructor
	/// </summary>
	public FrameworkMapperConfiguration()
	{
		CreateModelMap<GroupOfAccount, GroupOfAccountCreateModel>();
		CreateModelMap<GroupOfAccount, GroupOfAccountUpdateModel>(map => map.ForMember(p => p.GroupId, options => options.Ignore())
			.ForMember(p => p.AccountName, options => options.Ignore()));
	}
}
```

## Setting
Lưu trữ thay thế cho bảng S_PARAM. Trong O9, khi build up param, có package o9paramgroup để lấy giá trị. 

### Tạo dữ liệu
Tạo qua bước migration

```c#
using FluentMigrator;
using Jits.Neptune.Core.Domain.Configuration;
using Jits.Neptune.Data;
using Jits.Neptune.Data.Migrations;

namespace Jits.Neptune.Web.Framework.Migrations {
	/// <summary>
	/// 
	/// </summary>
	[NeptuneMigration("2020/01/01 10:00:02:0000000", "Setting install", UpdateMigrationType.Data, MigrationProcessType.Installation)]
	public class LocalizationMigration : Migration {
		private readonly INeptuneDataProvider _dataProvider;

		/// <summary>
		/// Localization migrations
		/// </summary>
		/// <param name="dataProvider"></param>
		public LocalizationMigration(INeptuneDataProvider dataProvider) {
			_dataProvider = dataProvider;
		}

		/// <summary>
		/// Up migration
		/// </summary>
		public override void Up() {
			_dataProvider.InsertEntity(new Setting { Name = "WebApiSettings.DeveloperMode", Value = "true" });
			_dataProvider.InsertEntity(new Setting { Name = "WebApiSettings.TokenLifetimeDays", Value = "7" });
			_dataProvider.InsertEntity(new Setting { Name = "WebApiSettings.SecretKey", Value = "igTLDRYrrjV88ZAXvF+nSw" });
		}

		/// <summary>
		/// Down migration
		/// </summary>
		public override void Down() {

		}
	}
}
```

Tạo class kế thừa của ISettings

```c#
using Jits.Neptune.Core.Configuration;

namespace Jits.Neptune.Web.Framework {
	/// <summary>
	/// Settings for Web Api
	/// </summary>
	public class WebApiSettings : ISettings {
		/// <summary>
		/// Developer mode
		/// </summary>
		/// <value></value>
		public bool DeveloperMode { get; set; }

		/// <summary>
		/// Secret key
		/// </summary>
		/// <value></value>
		public string SecretKey { get; set; }

		/// <summary>
		/// Token life time in days
		/// </summary>
		/// <value></value>
		public int TokenLifetimeDays { get; set; }
	}
}
```