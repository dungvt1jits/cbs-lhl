
Các Request đều phải có Bearer Token trong Header. Token lấy từ request POST `/api/auth/get-token` của Neptune.

## Get Neptune Token

- `POST`: `/api/auth/get-token`
- Body
```json
{
  "user_code": "neptune",
  "password": "123456"
}
```

Trong các giao dịch thực hiện xuống Core, đều có truyền tham số Token, lấy từ giao dịch Login của Core.

## Login Core 

### Request

- `POST`: `/api/workflow/execute`
- Body
```json
{
  "workflowid": "UMG_LOGIN",
  "lang": "en",
  "fields": {
    "username": "admin",
    "password": "123456"
  }
}
```

### Response từ Workflow Execution Info 

```json
{
    "execution": {
        "execution_id": "cfb0f2e5c8e54981b22effb8a09d933a",
        "input": {
            "workflowid": "UMG_LOGIN",
            "lang": "en",
            "token": "",
            "fields": {
                "username": "admin",
                "password": "123456"
            }
        },
        "workflow_id": "UMG_LOGIN",
        "status": "InProgress",
        "created_on": 1645627674201,
        "finish_on": 0,
        "is_timeout": "N",
        "is_processing": "Y",
        "is_success": "Unknown"
    },
    "execution_steps": [
        {
            "step_execution_id": "d6b879b588fe43cbb927eb31a5167b6a",
            "execution_id": "cfb0f2e5c8e54981b22effb8a09d933a",
            "workflow_id": "UMG_LOGIN",
            "step_group": 1,
            "step_order": 1,
            "step_code": "UMG_LOGIN",
            "request_id": "",
            "p1_start": 1645627674568,
            "p1_finish": 1645627674659,
            "p1_status": "Completed",
            "p1_error": "",
            "p1_content": {
                "request": {
                    "request_header": {
                        "service_id": "ADM",
                        "server_ip": "172.18.0.4",
                        "utc_send_time": 1645627674572,
                        "step_timeout": 60000,
                        "execution_id": "cfb0f2e5c8e54981b22effb8a09d933a",
                        "step_execution_id": "d6b879b588fe43cbb927eb31a5167b6a",
                        "from_queue_name": "neptune-queue:Saigon:orchestration",
                        "to_queue_name": "service_queue:queue-adm",
                        "is_compensated": "N",
                        "step_mode": "TWOWAY",
                        "step_code": "UMG_LOGIN",
                        "user_id": "97a446a21953404c8fe0ef2c9d29a283",
                        "organization_id": "dd4ff5b544e94586a81d7519a5ab2934"
                    },
                    "request_body": {
                        "workflow_input": {
                            "workflowid": "UMG_LOGIN",
                            "lang": "en",
                            "token": "",
                            "fields": {
                                "username": "admin",
                                "password": "123456"
                            }
                        },
                        "data": {
                            "Username": "admin",
                            "Password": "123456"
                        }
                    }
                },
                "response": {
                    "status": 0,
                    "data": {}
                }
            },
            "p2_request_id": "4bc987a5c64543ac9ab7253fa44ce776",
            "p2_start": 1645627675811,
            "p2_finish": 1645627675822,
            "p2_status": "Error",
            "p2_error": "Object reference not set to an instance of an object.",
            "p2_content": {
                "request": {
                    "request_header": {
                        "service_id": "ADM",
                        "server_ip": "172.18.0.4",
                        "utc_send_time": 1645627674572,
                        "step_timeout": 60000,
                        "execution_id": "cfb0f2e5c8e54981b22effb8a09d933a",
                        "step_execution_id": "d6b879b588fe43cbb927eb31a5167b6a",
                        "from_queue_name": "neptune-queue:Saigon:orchestration",
                        "to_queue_name": "service_queue:queue-adm",
                        "is_compensated": "N",
                        "step_mode": "TWOWAY",
                        "step_code": "UMG_LOGIN",
                        "user_id": "97a446a21953404c8fe0ef2c9d29a283",
                        "organization_id": "dd4ff5b544e94586a81d7519a5ab2934"
                    },
                    "request_body": {
                        "workflow_input": {
                            "workflowid": "UMG_LOGIN",
                            "lang": "en",
                            "token": "",
                            "fields": {
                                "username": "admin",
                                "password": "123456"
                            }
                        },
                        "data": {
                            "Username": "admin",
                            "Password": "123456"
                        }
                    }
                },
                "response": {
                    "status": 0,
                    "data": {
                        "login": {
                            "id": 1,
                            "username": "admin",
                            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ1NjI3Njc1IiwiZXhwIjoiMTY0NjIzMjQ3NSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.D9nvOFNS3QKiG4lJiE-4ysqio4tSrvXzqYRr32UjYog"
                        }
                    }
                }
            },
            "is_success": "NotEvaluated",
            "is_timeout": "N"
        }
    ]
}
```


## View

### Request
- `POST`: `/api/workflow/execute`
- Body
```json
{
  "workflowid": "ADM_VIEW_BRANCH",
  "lang": "en",
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQxMzcwNDU1IiwiZXhwIjoiMTY0MTk3NTI1NSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.psoKTT13Aj6YOgSgZUAqdB9TCn3PxGx1Q87sUqEVPgE",
  "fields": {
    "id": 1
  }
}
```

### Response - Trường hợp có dữ liệu trả về 

```json
{
    "execution": {
        "execution_id": "3f3db966455b44a78b3b6e61f176a8fc",
        "input": {
            "workflowid": "ADM_VIEW_BRANCH",
            "lang": "en",
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE",
            "fields": {
                "id": 1
            }
        },
        "workflow_id": "ADM_VIEW_BRANCH",
        "status": "Completed",
        "created_on": 1644479778060,
        "finish_on": 1644479778490,
        "is_timeout": "N",
        "is_processing": "N",
        "is_success": "Y"
    },
    "execution_steps": [
        {
            "step_execution_id": "7b080e6b4fe8400184e69b6697e6a45d",
            "execution_id": "3f3db966455b44a78b3b6e61f176a8fc",
            "workflow_id": "ADM_VIEW_BRANCH",
            "step_group": 1,
            "step_order": 1,
            "step_code": "SQL_CAN_USER_INVOKE_COMMAND",
            "request_id": "",
            "p1_start": 1644479778161,
            "p1_finish": 1644479778176,
            "p1_status": "Completed",
            "p1_error": "",
            "p1_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644479778161,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "3f3db966455b44a78b3b6e61f176a8fc",
                        "STEP_EXECUTION_ID": "7b080e6b4fe8400184e69b6697e6a45d",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_CAN_USER_INVOKE_COMMAND",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_VIEW_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "id": 1,
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {}
                }
            },
            "p2_request_id": "96070fccd42d4eca8313dfa6de09b40c",
            "p2_start": 1644479778351,
            "p2_finish": 1644479778359,
            "p2_status": "Completed",
            "p2_error": "",
            "p2_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644479778161,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "3f3db966455b44a78b3b6e61f176a8fc",
                        "STEP_EXECUTION_ID": "7b080e6b4fe8400184e69b6697e6a45d",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_CAN_USER_INVOKE_COMMAND",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_VIEW_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "id": 1,
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {
                        "CanInvoke": true
                    }
                }
            },
            "is_success": "Y",
            "is_timeout": "N"
        },
        {
            "step_execution_id": "ced8f290659b4b529a257710144f42a1",
            "execution_id": "3f3db966455b44a78b3b6e61f176a8fc",
            "workflow_id": "ADM_VIEW_BRANCH",
            "step_group": 2,
            "step_order": 2,
            "step_code": "SQL_VIEW_BRANCH",
            "request_id": "",
            "p1_start": 1644479778393,
            "p1_finish": 1644479778399,
            "p1_status": "Completed",
            "p1_error": "",
            "p1_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644479778393,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "3f3db966455b44a78b3b6e61f176a8fc",
                        "STEP_EXECUTION_ID": "ced8f290659b4b529a257710144f42a1",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_VIEW_BRANCH",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_VIEW_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "id": 1,
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "Id": 1,
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {}
                }
            },
            "p2_request_id": "26d272c57f3b4a8f818972687f7f2ca9",
            "p2_start": 1644479778466,
            "p2_finish": 1644479778474,
            "p2_status": "Completed",
            "p2_error": "",
            "p2_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644479778393,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "3f3db966455b44a78b3b6e61f176a8fc",
                        "STEP_EXECUTION_ID": "ced8f290659b4b529a257710144f42a1",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_VIEW_BRANCH",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_VIEW_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "id": 1,
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "Id": 1,
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {
                        "branch": {
                            "BranchCode": "0001",
                            "OldBranchId": "12",
                            "BranchName": "Sai gon",
                            "BranchAddress": "22 Le",
                            "BranchPhone": "0039302029",
                            "PhoneNumber": "0038850p",
                            "TaxCode": "0048489",
                            "BaseCurrencyCode": "USD",
                            "LocalCurrencyCode": "USD",
                            "ReferenceCode": "003",
                            "Country": "VN",
                            "MainLanguage": "VN",
                            "TimeZoneOfBranch": 7,
                            "ThousandSeparateCharacter": ",",
                            "DecimalSeparateCharacter": ".",
                            "DateFormatForShort": "dd/MM/yyyy",
                            "LongDateFormat": "dd/MM/yyyy",
                            "TimeFormat": "hh",
                            "Online": "Y",
                            "Id": 1
                        }
                    }
                }
            },
            "is_success": "Y",
            "is_timeout": "N"
        }
    ]
}
```

### Response - Trường hợp không có dữ liệu 

```json
{
    "execution": {
        "execution_id": "1d872314091a429da791150f9006c3cb",
        "input": {
            "workflowid": "ADM_VIEW_BRANCH",
            "lang": "en",
            "token": "",
            "LangCode": 0,
            "fields": {
                "id": 12,
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
            }
        },
        "workflow_id": "ADM_VIEW_BRANCH",
        "status": "Completed",
        "created_on": 1644479834707,
        "finish_on": 1644479835118,
        "is_timeout": "N",
        "is_processing": "N",
        "is_success": "Y"
    },
    "execution_steps": [
        {
            "step_execution_id": "e192c949c56d48f7b81a9febb1a88474",
            "execution_id": "1d872314091a429da791150f9006c3cb",
            "workflow_id": "ADM_VIEW_BRANCH",
            "step_group": 1,
            "step_order": 1,
            "step_code": "SQL_CAN_USER_INVOKE_COMMAND",
            "request_id": "",
            "p1_start": 1644479834911,
            "p1_finish": 1644479834920,
            "p1_status": "Completed",
            "p1_error": "",
            "p1_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644479834912,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "1d872314091a429da791150f9006c3cb",
                        "STEP_EXECUTION_ID": "e192c949c56d48f7b81a9febb1a88474",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_CAN_USER_INVOKE_COMMAND",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_VIEW_BRANCH",
                            "lang": "en",
                            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE",
                            "fields": {
                                "id": 12
                            }
                        },
                        "DATA": {
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {}
                }
            },
            "p2_request_id": "076f0768d19a4c38829595bde6f0e83d",
            "p2_start": 1644479834989,
            "p2_finish": 1644479835002,
            "p2_status": "Completed",
            "p2_error": "",
            "p2_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644479834912,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "1d872314091a429da791150f9006c3cb",
                        "STEP_EXECUTION_ID": "e192c949c56d48f7b81a9febb1a88474",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_CAN_USER_INVOKE_COMMAND",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_VIEW_BRANCH",
                            "lang": "en",
                            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE",
                            "fields": {
                                "id": 12
                            }
                        },
                        "DATA": {
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {
                        "CanInvoke": true
                    }
                }
            },
            "is_success": "Y",
            "is_timeout": "N"
        },
        {
            "step_execution_id": "494620a0ffd94f7e88bd77ea92b330e3",
            "execution_id": "1d872314091a429da791150f9006c3cb",
            "workflow_id": "ADM_VIEW_BRANCH",
            "step_group": 2,
            "step_order": 2,
            "step_code": "SQL_VIEW_BRANCH",
            "request_id": "",
            "p1_start": 1644479835038,
            "p1_finish": 1644479835047,
            "p1_status": "Completed",
            "p1_error": "",
            "p1_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644479835039,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "1d872314091a429da791150f9006c3cb",
                        "STEP_EXECUTION_ID": "494620a0ffd94f7e88bd77ea92b330e3",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_VIEW_BRANCH",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_VIEW_BRANCH",
                            "lang": "en",
                            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE",
                            "fields": {
                                "id": 12
                            }
                        },
                        "DATA": {
                            "Id": 12
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {}
                }
            },
            "p2_request_id": "fbdf8ebef0c6446d8242866f0bf1d628",
            "p2_start": 1644479835095,
            "p2_finish": 1644479835104,
            "p2_status": "Completed",
            "p2_error": "",
            "p2_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644479835039,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "1d872314091a429da791150f9006c3cb",
                        "STEP_EXECUTION_ID": "494620a0ffd94f7e88bd77ea92b330e3",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_VIEW_BRANCH",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_VIEW_BRANCH",
                            "lang": "en",
                            "token": "",
                            "fields": {
                                "id": 12
                            }
                        },
                        "DATA": {
                            "Id": 12
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {
                        "branch": null
                    }
                }
            },
            "is_success": "Y",
            "is_timeout": "N"
        }
    ]
}
```

## Insert / Update

### Request

- `POST`: `/api/workflow/execute`
- Body

```json
{
  "workflowid": "ADM_INSERT_BRANCH",
  "lang": "en",
  "token": "{{authToken}}",
  "fields": {
    "old_branch_id": "12",
    "branch_name": "Sai gon",
    "country": "VN",
    "branch_address": "22 Le",
    "main_language": "VN",
    "reference_code": "003",
    "base_currency_code": "USD",
    "local_currency_code": "USD",
    "branch_phone": "0039302029",
    "phone_number": "0038850p",
    "tax_code": "0048489",
    "time_zone_of_branch": 7,
    "thousand_separate_character": ",",
    "decimal_separate_character": ".",
    "date_format_for_short": "dd/MM/yyyy",
    "long_date_format": "dd/MM/yyyy",
    "time_format": "hh",
    "online": "Y"
  }
}
```

### Response

```json
{
    "time_in_miliseconds": 43,
    "status": "COMPLETED",
    "description": "Invoke [https://localhost:105/api/workflow/get-execution-info/38d374f38970433b96f693aaddd38195] to get status of the workflow.",
    "execution_id": "38d374f38970433b96f693aaddd38195",
    "data": null
}
```


### Response success

```json
{
    "execution": {
        "execution_id": "38d374f38970433b96f693aaddd38195",
        "input": {
            "workflowid": "ADM_INSERT_BRANCH",
            "lang": "en",
            "token": "",
            "LangCode": 0,
            "fields": {
                "old_branch_id": "12",
                "branch_name": "Sai gon",
                "country": "VN",
                "branch_address": "22 Le",
                "main_language": "VN",
                "reference_code": "003",
                "base_currency_code": "USD",
                "local_currency_code": "USD",
                "branch_phone": "0039302029",
                "phone_number": "0038850p",
                "tax_code": "0048489",
                "time_zone_of_branch": 7,
                "thousand_separate_character": ",",
                "decimal_separate_character": ".",
                "date_format_for_short": "dd/MM/yyyy",
                "long_date_format": "dd/MM/yyyy",
                "time_format": "hh",
                "online": "Y",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
            }
        },
        "workflow_id": "ADM_INSERT_BRANCH",
        "status": "Completed",
        "created_on": 1644401007880,
        "finish_on": 1644401008451,
        "is_timeout": "N",
        "is_processing": "N",
        "is_success": "Y"
    },
    "execution_steps": [
        {
            "step_execution_id": "d68ae94d3e4f4d578c1ad80696cc9064",
            "execution_id": "38d374f38970433b96f693aaddd38195",
            "workflow_id": "ADM_INSERT_BRANCH",
            "step_group": 1,
            "step_order": 1,
            "step_code": "SQL_CAN_USER_INVOKE_COMMAND",
            "request_id": "",
            "p1_start": 1644401008043,
            "p1_finish": 1644401008061,
            "p1_status": "Completed",
            "p1_error": "",
            "p1_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644401008043,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "38d374f38970433b96f693aaddd38195",
                        "STEP_EXECUTION_ID": "d68ae94d3e4f4d578c1ad80696cc9064",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_CAN_USER_INVOKE_COMMAND",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_INSERT_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "old_branch_id": "12",
                                "branch_name": "Sai gon",
                                "country": "VN",
                                "branch_address": "22 Le",
                                "main_language": "VN",
                                "reference_code": "003",
                                "base_currency_code": "USD",
                                "local_currency_code": "USD",
                                "branch_phone": "0039302029",
                                "phone_number": "0038850p",
                                "tax_code": "0048489",
                                "time_zone_of_branch": 7,
                                "thousand_separate_character": ",",
                                "decimal_separate_character": ".",
                                "date_format_for_short": "dd/MM/yyyy",
                                "long_date_format": "dd/MM/yyyy",
                                "time_format": "hh",
                                "online": "Y",
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {}
                }
            },
            "p2_request_id": "07af2864027c451887be62cbcb6f8f10",
            "p2_start": 1644401008211,
            "p2_finish": 1644401008220,
            "p2_status": "Completed",
            "p2_error": "",
            "p2_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644401008043,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "38d374f38970433b96f693aaddd38195",
                        "STEP_EXECUTION_ID": "d68ae94d3e4f4d578c1ad80696cc9064",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_CAN_USER_INVOKE_COMMAND",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_INSERT_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "old_branch_id": "12",
                                "branch_name": "Sai gon",
                                "country": "VN",
                                "branch_address": "22 Le",
                                "main_language": "VN",
                                "reference_code": "003",
                                "base_currency_code": "USD",
                                "local_currency_code": "USD",
                                "branch_phone": "0039302029",
                                "phone_number": "0038850p",
                                "tax_code": "0048489",
                                "time_zone_of_branch": 7,
                                "thousand_separate_character": ",",
                                "decimal_separate_character": ".",
                                "date_format_for_short": "dd/MM/yyyy",
                                "long_date_format": "dd/MM/yyyy",
                                "time_format": "hh",
                                "online": "Y",
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {
                        "CanInvoke": true
                    }
                }
            },
            "is_success": "Y",
            "is_timeout": "N"
        },
        {
            "step_execution_id": "d52513d92b0d40be9d892a071faf71e3",
            "execution_id": "38d374f38970433b96f693aaddd38195",
            "workflow_id": "ADM_INSERT_BRANCH",
            "step_group": 2,
            "step_order": 2,
            "step_code": "SQL_INSERT_BRANCH",
            "request_id": "",
            "p1_start": 1644401008257,
            "p1_finish": 1644401008273,
            "p1_status": "Completed",
            "p1_error": "",
            "p1_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644401008257,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "38d374f38970433b96f693aaddd38195",
                        "STEP_EXECUTION_ID": "d52513d92b0d40be9d892a071faf71e3",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_INSERT_BRANCH",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_INSERT_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "old_branch_id": "12",
                                "branch_name": "Sai gon",
                                "country": "VN",
                                "branch_address": "22 Le",
                                "main_language": "VN",
                                "reference_code": "003",
                                "base_currency_code": "USD",
                                "local_currency_code": "USD",
                                "branch_phone": "0039302029",
                                "phone_number": "0038850p",
                                "tax_code": "0048489",
                                "time_zone_of_branch": 7,
                                "thousand_separate_character": ",",
                                "decimal_separate_character": ".",
                                "date_format_for_short": "dd/MM/yyyy",
                                "long_date_format": "dd/MM/yyyy",
                                "time_format": "hh",
                                "online": "Y",
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "OldBranchId": "12",
                            "BranchName": "Sai gon",
                            "BranchAddress": "22 Le",
                            "BranchPhone": "0039302029",
                            "PhoneNumber": "0038850p",
                            "TaxCode": "0048489",
                            "BaseCurrencyCode": "USD",
                            "LocalCurrencyCode": "USD",
                            "ReferenceCode": "003",
                            "Country": "VN",
                            "MainLanguage": "VN",
                            "TimeZoneOfBranch": 7,
                            "ThousandSeparateCharacter": ",",
                            "DecimalSeparateCharacter": ".",
                            "DateFormatForShort": "dd/MM/yyyy",
                            "LongDateFormat": "dd/MM/yyyy",
                            "TimeFormat": "hh",
                            "Online": "Y",
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {}
                }
            },
            "p2_request_id": "477717076ca343e182ecfa8610ac68e4",
            "p2_start": 1644401008427,
            "p2_finish": 1644401008437,
            "p2_status": "Completed",
            "p2_error": "",
            "p2_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644401008257,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "38d374f38970433b96f693aaddd38195",
                        "STEP_EXECUTION_ID": "d52513d92b0d40be9d892a071faf71e3",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_INSERT_BRANCH",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_INSERT_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "old_branch_id": "12",
                                "branch_name": "Sai gon",
                                "country": "VN",
                                "branch_address": "22 Le",
                                "main_language": "VN",
                                "reference_code": "003",
                                "base_currency_code": "USD",
                                "local_currency_code": "USD",
                                "branch_phone": "0039302029",
                                "phone_number": "0038850p",
                                "tax_code": "0048489",
                                "time_zone_of_branch": 7,
                                "thousand_separate_character": ",",
                                "decimal_separate_character": ".",
                                "date_format_for_short": "dd/MM/yyyy",
                                "long_date_format": "dd/MM/yyyy",
                                "time_format": "hh",
                                "online": "Y",
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "OldBranchId": "12",
                            "BranchName": "Sai gon",
                            "BranchAddress": "22 Le",
                            "BranchPhone": "0039302029",
                            "PhoneNumber": "0038850p",
                            "TaxCode": "0048489",
                            "BaseCurrencyCode": "USD",
                            "LocalCurrencyCode": "USD",
                            "ReferenceCode": "003",
                            "Country": "VN",
                            "MainLanguage": "VN",
                            "TimeZoneOfBranch": 7,
                            "ThousandSeparateCharacter": ",",
                            "DecimalSeparateCharacter": ".",
                            "DateFormatForShort": "dd/MM/yyyy",
                            "LongDateFormat": "dd/MM/yyyy",
                            "TimeFormat": "hh",
                            "Online": "Y",
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {
                        "id": 1
                    }
                }
            },
            "is_success": "Y",
            "is_timeout": "N"
        }
    ]
}
```

### Response error

```json
{
    "execution": {
        "execution_id": "36218d278a16487cb7ff5db9091c3a67",
        "input": {
            "workflowid": "ADM_INSERT_BRANCH",
            "lang": "en",
            "token": "",
            "LangCode": 0,
            "fields": {
                "old_branch_id": "12",
                "branch_name": "",
                "country": "",
                "branch_address": "22 Le",
                "main_language": "VN",
                "reference_code": "003",
                "base_currency_code": "USD",
                "local_currency_code": "USD",
                "branch_phone": "0039302029",
                "phone_number": "0038850p",
                "tax_code": "0048489",
                "time_zone_of_branch": 7,
                "thousand_separate_character": ",",
                "decimal_separate_character": ".",
                "date_format_for_short": "dd/MM/yyyy",
                "long_date_format": "dd/MM/yyyy",
                "time_format": "hh",
                "online": "Y",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
            }
        },
        "workflow_id": "ADM_INSERT_BRANCH",
        "status": "FinishedWithError",
        "created_on": 1644403539485,
        "finish_on": 1644403540421,
        "is_timeout": "N",
        "is_processing": "N",
        "is_success": "N"
    },
    "execution_steps": [
        {
            "step_execution_id": "4a141f2361614ac1bdba4fe75d976239",
            "execution_id": "36218d278a16487cb7ff5db9091c3a67",
            "workflow_id": "ADM_INSERT_BRANCH",
            "step_group": 1,
            "step_order": 1,
            "step_code": "SQL_CAN_USER_INVOKE_COMMAND",
            "request_id": "",
            "p1_start": 1644403539644,
            "p1_finish": 1644403539657,
            "p1_status": "Completed",
            "p1_error": "",
            "p1_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644403539645,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "36218d278a16487cb7ff5db9091c3a67",
                        "STEP_EXECUTION_ID": "4a141f2361614ac1bdba4fe75d976239",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_CAN_USER_INVOKE_COMMAND",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_INSERT_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "old_branch_id": "12",
                                "branch_name": "",
                                "country": "",
                                "branch_address": "22 Le",
                                "main_language": "VN",
                                "reference_code": "003",
                                "base_currency_code": "USD",
                                "local_currency_code": "USD",
                                "branch_phone": "0039302029",
                                "phone_number": "0038850p",
                                "tax_code": "0048489",
                                "time_zone_of_branch": 7,
                                "thousand_separate_character": ",",
                                "decimal_separate_character": ".",
                                "date_format_for_short": "dd/MM/yyyy",
                                "long_date_format": "dd/MM/yyyy",
                                "time_format": "hh",
                                "online": "Y",
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {}
                }
            },
            "p2_request_id": "11103033d8904f4ea3f2622196980f50",
            "p2_start": 1644403540205,
            "p2_finish": 1644403540213,
            "p2_status": "Completed",
            "p2_error": "",
            "p2_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644403539645,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "36218d278a16487cb7ff5db9091c3a67",
                        "STEP_EXECUTION_ID": "4a141f2361614ac1bdba4fe75d976239",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_CAN_USER_INVOKE_COMMAND",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_INSERT_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "old_branch_id": "12",
                                "branch_name": "",
                                "country": "",
                                "branch_address": "22 Le",
                                "main_language": "VN",
                                "reference_code": "003",
                                "base_currency_code": "USD",
                                "local_currency_code": "USD",
                                "branch_phone": "0039302029",
                                "phone_number": "0038850p",
                                "tax_code": "0048489",
                                "time_zone_of_branch": 7,
                                "thousand_separate_character": ",",
                                "decimal_separate_character": ".",
                                "date_format_for_short": "dd/MM/yyyy",
                                "long_date_format": "dd/MM/yyyy",
                                "time_format": "hh",
                                "online": "Y",
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {
                        "CanInvoke": true
                    }
                }
            },
            "is_success": "N",
            "is_timeout": "N"
        },
        {
            "step_execution_id": "f8d5ffbaf0ba42528c16a9234cb3b1e2",
            "execution_id": "36218d278a16487cb7ff5db9091c3a67",
            "workflow_id": "ADM_INSERT_BRANCH",
            "step_group": 2,
            "step_order": 2,
            "step_code": "SQL_INSERT_BRANCH",
            "request_id": "",
            "p1_start": 1644403540249,
            "p1_finish": 1644403540255,
            "p1_status": "Completed",
            "p1_error": "",
            "p1_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644403540249,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "36218d278a16487cb7ff5db9091c3a67",
                        "STEP_EXECUTION_ID": "f8d5ffbaf0ba42528c16a9234cb3b1e2",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_INSERT_BRANCH",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_INSERT_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "old_branch_id": "12",
                                "branch_name": "",
                                "country": "",
                                "branch_address": "22 Le",
                                "main_language": "VN",
                                "reference_code": "003",
                                "base_currency_code": "USD",
                                "local_currency_code": "USD",
                                "branch_phone": "0039302029",
                                "phone_number": "0038850p",
                                "tax_code": "0048489",
                                "time_zone_of_branch": 7,
                                "thousand_separate_character": ",",
                                "decimal_separate_character": ".",
                                "date_format_for_short": "dd/MM/yyyy",
                                "long_date_format": "dd/MM/yyyy",
                                "time_format": "hh",
                                "online": "Y",
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "OldBranchId": "12",
                            "BranchName": "",
                            "BranchAddress": "22 Le",
                            "BranchPhone": "0039302029",
                            "PhoneNumber": "0038850p",
                            "TaxCode": "0048489",
                            "BaseCurrencyCode": "USD",
                            "LocalCurrencyCode": "USD",
                            "ReferenceCode": "003",
                            "Country": "",
                            "MainLanguage": "VN",
                            "TimeZoneOfBranch": 7,
                            "ThousandSeparateCharacter": ",",
                            "DecimalSeparateCharacter": ".",
                            "DateFormatForShort": "dd/MM/yyyy",
                            "LongDateFormat": "dd/MM/yyyy",
                            "TimeFormat": "hh",
                            "Online": "Y",
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 0,
                    "OUTPUT": {}
                }
            },
            "p2_request_id": "d1334341f6434834a1527b48b444bea8",
            "p2_start": 1644403540399,
            "p2_finish": 1644403540406,
            "p2_status": "Failed",
            "p2_error": "The step has status Failed due to the condition of success.",
            "p2_content": {
                "Request": {
                    "RequestHeader": {
                        "SERVICE_ID": "ADM",
                        "SERVER_IP": "172.24.0.6",
                        "UTC_SEND_TIME": 1644403540249,
                        "STEP_TIMEOUT": 60000,
                        "EXECUTION_ID": "36218d278a16487cb7ff5db9091c3a67",
                        "STEP_EXECUTION_ID": "f8d5ffbaf0ba42528c16a9234cb3b1e2",
                        "FROM_QUEUE_NAME": "neptune-queue:Saigon:orchestration",
                        "TO_QUEUE_NAME": "service_queue:queue-adm",
                        "IS_COMPENSATED": "N",
                        "STEP_MODE": "TWOWAY",
                        "STEP_CODE": "SQL_INSERT_BRANCH",
                        "USER_ID": "4dcb83420d2242e6ac3cec93b872252e",
                        "ORGANIZATION_ID": "ba814c60e1384aacb5dd259b8882b32a"
                    },
                    "RequestBody": {
                        "WorkflowInput": {
                            "workflowid": "ADM_INSERT_BRANCH",
                            "lang": "en",
                            "token": "",
                            "LangCode": 0,
                            "fields": {
                                "old_branch_id": "12",
                                "branch_name": "",
                                "country": "",
                                "branch_address": "22 Le",
                                "main_language": "VN",
                                "reference_code": "003",
                                "base_currency_code": "USD",
                                "local_currency_code": "USD",
                                "branch_phone": "0039302029",
                                "phone_number": "0038850p",
                                "tax_code": "0048489",
                                "time_zone_of_branch": 7,
                                "thousand_separate_character": ",",
                                "decimal_separate_character": ".",
                                "date_format_for_short": "dd/MM/yyyy",
                                "long_date_format": "dd/MM/yyyy",
                                "time_format": "hh",
                                "online": "Y",
                                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                            }
                        },
                        "DATA": {
                            "OldBranchId": "12",
                            "BranchName": "",
                            "BranchAddress": "22 Le",
                            "BranchPhone": "0039302029",
                            "PhoneNumber": "0038850p",
                            "TaxCode": "0048489",
                            "BaseCurrencyCode": "USD",
                            "LocalCurrencyCode": "USD",
                            "ReferenceCode": "003",
                            "Country": "",
                            "MainLanguage": "VN",
                            "TimeZoneOfBranch": 7,
                            "ThousandSeparateCharacter": ",",
                            "DecimalSeparateCharacter": ".",
                            "DateFormatForShort": "dd/MM/yyyy",
                            "LongDateFormat": "dd/MM/yyyy",
                            "TimeFormat": "hh",
                            "Online": "Y",
                            "Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOiIxNjQ0Mzg4ODA5IiwiZXhwIjoiMTY0NDk5MzYwOSIsIlVzZXJJZCI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4ifQ.rA98ZY-wmclHvdYzpgUQVW53vjGhtfA1TSBVmPLg9oE"
                        }
                    }
                },
                "Response": {
                    "STATUS": 1,
                    "OUTPUT": {
                        "branch_name": "BranchName is required",
                        "country": "Country is required"
                    }
                }
            },
            "is_success": "N",
            "is_timeout": "N"
        }
    ]
}
```
