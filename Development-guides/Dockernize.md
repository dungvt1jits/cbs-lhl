## Chuẩn bị 

Máy cài sẵn docker, docker-compose

Trong VS Code, cài thêm extension Docker.

### Mysql

```
docker pull mysql
```

### Phpmyadmin

```
docker pull phpmyadmin
```

### Portainer

```
docker pull portainer/portainer-ce
```

## Build docker

Chuyển tới thư mục có file docker-compose.yml. Chạy lệnh:lệnhh

```
docker-compose -f ./docker-compose.yml -f ./docker-compose.override.yml up -d --build
```

Để tắt 

```
docker-compose -f ./docker-compose.yml -f ./docker-compose.override.yml down
```

Xem trạng thái các container đang chạy 
```
docker ps
```
hoặc xem bao gồm tất cả container 

```
docker ps -a
```

Xem logs của container 
```
docker logs <container_id>/<container_name>
```

Dọn rác, xoá các container đang không chạy

```
docker container prune
```

## Chạy Service lần đầu

Trong lần chạy đầu tiên, đảm bảo trong thư mục gốc của service có sẵn thư mục App_Data, và không được có sẵn file `appsettings.json`.
Chạy docker của service. 
Chạy api install POST `/install`, để tạo kết nối và khởi tạo database.

```json
{
    "ServerName": "neptunedb_mysql",
    "DatabaseName": "o9accounting",
    "Port": "3306",
    "Username": "o9accounting",
    "Password": "o9accounting",
    "DataProvider": "mysql"
}
```

Kết quả trả về `{'status': 'done'}` là thành công. 
Mở xem files của docker, trong thư mục `/app/App_Data`, copy nội dung của file appsettings.json, và tạo file appsettings.json tương ứng trong thư mục `App_Data` của service. Từ đây, mọi chỉnh sửa trên file appsettings.json sẽ được cập nhật lên file của docker. 
Điều chỉnh 1 số cấu hình cho file appsettings, thêm thông tin đường dẫn cho file certificate, cho các protocols `http2`
```json
    "Certificate": {
        "Path": "NT.pfx",
        "Password": "123456"
    }
```

- `NeptuneGrpcURL`: localhost -> neptuneserver
- `YourServiceID`: id của service 
- `YourGrpcURL`: protocol và port tương ứng với cấu hình của Grpc ở trên, với tên container của service

Ví dụ: 

```json
    "Kestrel": {
      "Endpoints": {
        "Grpc": {
          "Protocols": "Http2",
          "Url": "https://*:5000",
          "Certificate": {
            "Path": "NT.pfx",
            "Password": "123456"
          }
        },
        "webApi": {
          "Protocols": "Http1",
          "Url": "http://*:80"
        }
      }
    },
    "NeptuneConfiguration": {
      "NeptuneGrpcURL": "https://neptuneserver:109/",
      "YourServiceID": "ACT",
      "YourGrpcURL": "https://accounting.api:5000"
    }
```