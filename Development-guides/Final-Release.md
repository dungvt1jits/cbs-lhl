```mermaid
gantt
    title Final Release
        dateFormat DD-MM-YYYY
        section Team Test
        Search Specs :done, s_doc, 25-11-2021, 1w
        CRUD Specs :active, crud_doc, after s_doc, 2w
        FO Specs : fo_doc, after crud_doc, 3w
        section Team Dev
        section Team UI
        integrate with API: integrate_api, 01-12-2021, 4w
```

Dựng môi trường test độc lập. 
Chạy deploy docker riêng cho từng service
Danh sách quy hoạch các docker / service -> port mapping.
Các service tương tác lẫn nhau được -> trực tiếp hoặc gián tiếp thông qua Workflow.
Cấu hình đường dẫn tới service cần lấy thông tin.
Chuyển ngôn ngữ theo URL 
Quản lý workflow

## Setup Server Test

Cài đặt Neptune Database
- Cần tạo database cho Neptune Server trước. 
- Chạy script khởi tạo Table trong thư mục ShareFiles
- Chỉnh lại các thông số của Neptune cho tương ứng trong ENVIRONMENT_VARIABLE
  - GRPC_URL: https://neptuneserver:109/
  - MESSAGE_BROKER_HOSTNAME: rabbitmq
  - MESSAGE_BROKER_PASSWORD: guest
  - MESSAGE_BROKER_USERNAME: guest
- Tạo các database lần lượt cho các service: 
  - o9admin
  - o9accounting
  - o9cash
  - o9credit
  - o9customer
  - o9deposit
  - o9ifc
  - o9fac
  - o9fx
  - o9payment
- Đảm bảo các thư mục của service có thư mục App_Data, và file mồi appsettings.json. 
- Start lần lượt các service, và chạy api install để cài đặt database. 

Nội dung mẫu file appsettings mồi
```json
{
    "ConnectionStrings": {
      "ConnectionString": "",
      "DataProvider": "mysql",
      "SQLCommandTimeout": null
    },
    "CacheConfig": {
      "DefaultCacheTime": 60,
      "ShortTermCacheTime": 3,
      "BundledFilesCacheTime": 120
    },
    "Kestrel": {
      "Endpoints": {
        "Grpc": {
          "Protocols": "Http2",
          "Url": "https://*:5000",
          "Certificate": {
            "Path": "NT.pfx",
            "Password": "123456"
          }
        },
        "webApi": {
          "Protocols": "Http1",
          "Url": "http://*:80"
        }
      }
    },
    "NeptuneConfiguration": {
      "NeptuneGrpcURL": "https://neptuneserver:109/",
      "YourServiceID": "ACT",
      "YourGrpcURL": "https://accounting.api:5000"
    }
  }
```