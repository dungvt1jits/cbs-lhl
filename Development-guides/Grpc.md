## Neptune Queue Client 

### Prepare service definitions

### AppSettings

Gắn thêm các config khai báo service ID, đường dẫn của service và của Neptune server:

```json
    "NeptuneConfiguration": {
      "NeptuneGrpcURL": "https://neptuneserver:109/",
      "YourServiceID": "<service ID>",
      "YourGrpcURL": "https://mortgage.api:5000"
    }
```

## Design Workflow

Ví dụ từ Store gốc của giao dịch như sau 
```sql
PROCEDURE       "ADM_INSERT_BRANCH" (pmsgtransaction IN OUT VARCHAR2, puserid IN NUMBER, psessionid IN VARCHAR2,pOut_CLOB    IN OUT CLOB)
AS
BEGIN
   o9sys.o9core.checkauthorization (pmsgtransaction, puserid, psessionid);
   O9SYS.O9ADM.SQL_INSERT_BRANCH(pmsgtransaction);
   EXCEPTION
   WHEN OTHERS THEN
      o9sys.o9util.processerror (SQLCODE, SQLERRM, pmsgtransaction);
      RAISE;
END;
```

Sẽ chuyển đổi tương ứng thành 1 Workflow, với 2 bước thực hiện: 
- B1: Check Authorization 
- B2: Call ADM Service: Insert Branch

### Workflow Definition

Khai báo Workflow trong bảng WF_DEF. 

```
INSERT INTO `WF_DEF` (`WFID`, `Code`, `Name`, `Status`, `Description`, `MS_TIMEOUT`) VALUES ('ADM_INSERT_BRANCH', 'ADM_INSERT_BRANCH', 'Insert Branch profile', 'A', 'Insert Branch profile', '60000');
```

### Workflow Step Defintion

Khai báo 2 bước thực hiện chi tiết vào bảng WF_STEP_DEF:
- SERVICE_ID: nhập đúng ServiceID cần gọi tương ứng. 
- SENDING_TEMPLATE: nội dung cấu hình mapping giữa các field từ json trích ra.
- REQUEST_PROTOCOL: protocol http hoặc https theo cấu hình Http của Service.
- REQUEST_SERVER_IP: ip hoặc tên container của Service. 
- REQUEST_SERVER_PORT: port của Service.
- REQUEST_URI: đường dẫn api cần gọi. 
- SEND_BY_BROKER: `N` nếu gọi theo http, `Y` nếu gọi theo đường queue client (qua rabbitMQ).

```sql
INSERT INTO `WF_STEP_DEF` (`WFID`, `STEP_ORDER`, `STEP_GROUP`, `STEP_CODE`, `STATUS`, `DESCRIPTION`, `SERVICE_ID`, `SENDING_TEMPLATE`, `SUCCESS_CONDITION`, `REQUEST_PROTOCOL`, `REQUEST_SERVER_IP`, `REQUEST_SERVER_PORT`, `REQUEST_URI`, `SEND_BY_BROKER`, `STEP_TIMEOUT`, `STEP_MODE`) VALUES
('ADM_INSERT_BRANCH', 2, 1, 'SQL_INSERT_BRANCH', 'Active', 'Step call service Insert Branch', 'ADM', '{\"branch_code\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.BRANCH\"]},\"old_branch_id\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.OLD_BRANCH\"]},\"branch_name\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.BRANCH_NAME\"]},\"branch_address\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.BRANCH_ADDRESS\"]},\"branch_phone\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.BRANCH_PHONE\"]},\"phone_number\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.PHONE_NUMBER\"]},\"tax_code\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.TAX_CODE\"]},\"base_currency_code\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.BASE_CURRENCY\"]},\"local_currency_code\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.LOCAL_CURRENCY\"]},\"reference_code\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.REFERENCE_CODE\"]},\"country\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.COUNTRY\"]},\"branch_code\": {\"func\": \"MapFromInput\",\"type\": \"string\",\"paras\": [\"$.execution.input.fields.BRANCH\"]},}', '{\"P1_STATUS\":\"Completed\"}', 'http', 'admin.api', '80', 'api/Branch/Create', 'N', '60000', 'TWOWAY');
```

Insert Branch cần các giá trị sau:
```
{
  "branch_code": "1234",
  "old_branch_id": "string",
  "branch_name": "string",
  "branch_address": "string",
  "branch_phone": "string",
  "phone_number": "string",
  "tax_code": "string",
  "base_currency_code": "USD",
  "local_currency_code": "USD",
  "reference_code": "string",
  "country": "VN",
  "main_language": "en",
  "time_zone_of_branch": 1,
  "thousand_separate_character": ",",
  "decimal_separate_character": ".",
  "date_format_for_short": "dd/MM/yyyy",
  "long_date_format": "dd/MM/yyyy",
  "time_format": "hh",
  "online": "Y",
  "transaction_code": "100BCDDD",
  "transaction_date": "2021-12-21T13:26:22.995Z",
  "reference_id": "string",
  "branch_id": 0,
  "user_id": 0,
  "status": "string",
  "is_reverse": true,
  "amount1": 0,
  "description": "string"
}
```

### Code xử lý ở Service 

Để nhận message gửi qua Broker, add thêm file Startup để nhận và điều hướng.

```c#
using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Jits.Neptune.Core;
using Jits.Neptune.Core.Infrastructure;
using Jits.Neptune.Web.Framework.Middleware;
using Jits.Neptune.Web.Admin.Models;
using Jits.Neptune.Web.Admin.Factories;
using Jits.Neptune.Web.Admin.Services;
using Jits.Neptune.Web.Admin.Validators;
using Jits.Neptune.Web.Framework.Models.Neptune;

using JITS.Neptune.NeptuneClient;

namespace Jits.Neptune.Web.Admin.Infrastructure {
	/// <summary>
	/// Represents object for the configuring routing on application startup
	/// </summary>
	public class AdminStartup : INeptuneStartup {
		/// <summary>
		/// Add and configure any of the middleware
		/// </summary>
		/// <param name="services">Collection of service descriptors</param>
		/// <param name="configuration">Configuration of the application</param>
		public void ConfigureServices(IServiceCollection services, IConfiguration configuration) {

		}

		/// <summary>
		/// Configure using of added middleware
		/// </summary>
		/// <param name="application"></param>
		public void Configure(IApplicationBuilder application) {
            var neptuneClient = new QueueClient();
			neptuneClient.WorkflowDelivering += NeptuneClientQueue_WorkflowDelivering;
			neptuneClient.AutoConfigure();
			Singleton<QueueClient>.Instance = neptuneClient;
		}

		private void NeptuneClientQueue_WorkflowDelivering(WorkflowScheme workflow) {
            if (workflow != null)
            {
				try {
					System.Console.WriteLine("khailm receive");
					if (workflow.Request.RequestHeader.STEP_CODE.Equals("SQL_INSERT_BRANCH")) {
						var brachService = new BranchQueueService();
						workflow = brachService.InsertBranch(workflow);
					} else if (workflow.Request.RequestHeader.STEP_CODE.Equals("SQL_VIEW_BRANCH")) {
						var brachService = new BranchQueueService();
						workflow = brachService.ViewBranch(workflow);
					} else {
						Console.WriteLine("Message from NeptuneClient: " + JsonSerializer.Serialize(workflow));
					}
				} catch (NeptuneException ex) {
					workflow.Response.OUTPUT = ex.ToDictionary();
					workflow.Response.STATUS = JITS.Neptune.NeptuneClient.WorkflowScheme.RESPONSE.EnumReponseStatus.ERROR;
				} catch (Exception ex) {
					System.Console.WriteLine("exception " + ex.Message);
					workflow.Response.STATUS = JITS.Neptune.NeptuneClient.WorkflowScheme.RESPONSE.EnumReponseStatus.ERROR;
				}

				Singleton<QueueClient>.Instance.ReplyWorkflow(workflow);
            }
        }

		/// <summary>
		/// Gets order of this startup configuration implementation
		/// </summary>
		public int Order => 2;
    }
}
```

Code QueueService

```c#
	public class BranchQueueService {
		public WorkflowScheme InsertBranch(WorkflowScheme workflow) {
			var model = workflow.ToModel<BranchCreateModel>();
			var validator = EngineContext.Current.Resolve<BranchCreateValidator>();
			var validationResult = validator.Validate(model);
			
			if (!validationResult.IsValid) {
				workflow.Response.OUTPUT = validationResult.Errors.ToDictionary();

				workflow.Response.STATUS = JITS.Neptune.NeptuneClient.WorkflowScheme.RESPONSE.EnumReponseStatus.ERROR;
			} else {
				var branch = EngineContext.Current.Resolve<IBranchModelFactory>().PrepareBranchCreateModel(model);
				Task.Run(() => { EngineContext.Current.Resolve<IBranchService>().Create(branch); });

				workflow.Response.STATUS = JITS.Neptune.NeptuneClient.WorkflowScheme.RESPONSE.EnumReponseStatus.SUCCESS;
			}

			return workflow;
		}

        public WorkflowScheme ViewBranch(WorkflowScheme workflow) {
			var model = workflow.ToModel<ModelWithId>();
						
			var branch = EngineContext.Current.Resolve<IBranchService>().GetById(model.Id).Result;
			workflow.Response.OUTPUT = branch.ToDictionary("Branch");
			workflow.Response.STATUS = JITS.Neptune.NeptuneClient.WorkflowScheme.RESPONSE.EnumReponseStatus.SUCCESS;

			return workflow;
		}
	}
```

### Troubleshooting

Khi chạy ExecuteWorkflow bị lỗi Service unavailable: kiểm tra Accept_Time của service, thử trong 2 cách sau:
- Chạy api SafePing của Neptune: `/Service/SafePing/<ServiceID>`
- Chỉnh giá trị lớn hơn thời gian hiện tại, reload lại caching.


## Project Server

### Package
Sử dụng package Grpc.AspNetCore, chỉnh thêm trong file `csproj`:
```
<PackageReference Include="Grpc.AspNetCore" Version="2.40.0" />
```

### Proto file
Tạo thư mục Protos, tạo file proto (vd: admin.proto). Điều chỉnh lại `csharp_namespace` tương ứng với namespace của project. Phần `service` khai báo các hàm (tương tự với tạo interface). Tiếp theo sẽ là định nghĩa cấu trúc của các đối tượng tham gia.

```
syntax = "proto3";

option csharp_namespace = "Jits.Neptune.Web.Admin.Protos";

service AdminProtoService {
    rpc GetUser (GetUserRequest) returns (GetUserResponse);
}

message GetUserRequest {
    string userName = 1;
}

message GetUserResponse {
    int32 id = 1;
    string userName = 2;
}
```

### Link file proto 
Trong file `.csproj`, add thêm liên kết tới file `proto` mới tạo. 

```
<Protobuf Include="Protos/admin.proto" GrpcServices="Server" />
```
Sau đó, build lại project, code mới tự sinh sẽ có trong thư mục obj.

### File Service

Với các service implement các hàm đã khai báo trong file proto, chỉnh lại kế thừa từ ServiceBase

Namespace using
```c#
using Grpc.Core;
using Jits.Neptune.Web.Admin.Protos;
```

```c#
public partial class UserAccountService : AdminProtoService.AdminProtoServiceBase, IUserAccountService {
```

```c#
        /// <summary>
        /// Example Grpc service
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public override async Task<GetUserResponse> GetUser(GetUserRequest request, ServerCallContext context) {
            // sample call repository
            var user = await _userAccountRepository.GetById(1);

            var response = new GetUserResponse {
                Id = 20,
                UserName = "khail"
            };
            return response;
        }
```

### Startup 

Gắn thêm vào file `Startup` các đoạn để nạp Grpc và code liên quan. 

```c#
    public void ConfigureServices(IServiceCollection services, IConfiguration configuration) {
        services.AddGrpc();
    }

    /// <summary>
    /// Configure using of added middleware
    /// </summary>
    /// <param name="application"></param>
    public void Configure(IApplicationBuilder application) {
        application.UseEndpoints(endpoints => {
            endpoints.MapGrpcService<UserAccountService>();
        });
    }
```

## Project client

### Package
Sử dụng package Grpc.AspNetCore, chỉnh thêm trong file `csproj`:
```
<PackageReference Include="Grpc.AspNetCore" Version="2.40.0" />
```

Tạo liên kết tới file `proto` gốc để build file proto.

```
        <Protobuf Include="../Jits.Neptune.Web.Admin/Protos/admin.proto" GrpcServices="Client">
            <Link>Protos/admin.proto</Link>
        </Protobuf>
```

### Proto Service
Tạo thư mục `GrpcServices`, để chứa các class Service chuyên dành cho call Grpc. Tạo file service:

```c#
using System;
using System.Threading.Tasks;
using Jits.Neptune.Web.Admin.Protos;

namespace Jits.Neptune.Web.Accounting.GrpcServices {
    /// <summary>
    /// Call Admin Grpc
    /// </summary>
    public class AdminGrpcService {
        private readonly AdminProtoService.AdminProtoServiceClient _adminProtoService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="adminProtoService"></param>
        public AdminGrpcService(AdminProtoService.AdminProtoServiceClient adminProtoService) {
            _adminProtoService = adminProtoService ?? throw new ArgumentNullException(nameof(adminProtoService));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<GetUserResponse> GetUser(string userName) {
            var userRequest = new GetUserRequest { UserName = userName };

            return await _adminProtoService.GetUserAsync(userRequest);
        }
    }
}
```

### Startup

Gắn thêm vào file `Startup` các đoạn để nạp Grpc và code liên quan. Lưu ý: đoạn lấy cấu hình `new Uri(configuration["GrpcSettings:AdminUrl"])` hiện đang lấy từ config của docker, tương lai sẽ chuyển theo cách lấy từ Neptune Workflow. 

```c#
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Jits.Neptune.Core.Infrastructure;
using Jits.Neptune.Web.Admin.Protos;
using Jits.Neptune.Web.Accounting.GrpcServices;

namespace Jits.Neptune.Web.Accounting.Infrastructure {
	/// <summary>
	/// Represents object for the configuring routing on application startup
	/// </summary>
	public class AccountingStartup : INeptuneStartup {

		/// <summary>
		/// Add and configure any of the middleware
		/// </summary>
		/// <param name="services">Collection of service descriptors</param>
		/// <param name="configuration">Configuration of the application</param>
		public void ConfigureServices(IServiceCollection services, IConfiguration configuration) {
            services.AddGrpcClient<AdminProtoService.AdminProtoServiceClient>(o => o.Address = new Uri(configuration["GrpcSettings:AdminUrl"]));
            services.AddScoped<AdminGrpcService>();
		}

		/// <summary>
		/// Configure using of added middleware
		/// </summary>
		/// <param name="application"></param>
		public void Configure(IApplicationBuilder application) {
		}

		/// <summary>
		/// Gets order of this startup configuration implementation
		/// </summary>
		public int Order => 2;
    }
}
```