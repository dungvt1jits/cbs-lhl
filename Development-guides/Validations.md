## Prepare error messages 
Tạo sẵn trong bộ cài đặt Migration.
Hoặc tạo thông qua chạy API.

```c#
using FluentMigrator;
using Jits.Neptune.Core.Domain.Configuration;
using Jits.Neptune.Core.Domain.Localization;
using Jits.Neptune.Data;
using Jits.Neptune.Data.Migrations;

namespace Jits.Neptune.Web.Framework.Migrations {
	/// <summary>
	/// 
	/// </summary>
	[NeptuneMigration("2020/01/01 10:00:02:0000000", "Localization installed", UpdateMigrationType.Data, MigrationProcessType.Installation)]
	public class LocalizationMigration : Migration {
		private readonly INeptuneDataProvider _dataProvider;

		/// <summary>
		/// Localization migrations
		/// </summary>
		/// <param name="dataProvider"></param>
		public LocalizationMigration(INeptuneDataProvider dataProvider) {
			_dataProvider = dataProvider;
		}

		/// <summary>
		/// Up migration
		/// </summary>
		public override void Up() {
			var resources = new List<LocaleStringResource> {
				new LocaleStringResource { Language = "en", ResourceName = "Common.LocaleStringResource.Language.Value.Required", ResourceValue = "Locale string resource language is required" },
				new LocaleStringResource { Language = "en", ResourceName = "Common.LocaleStringResource.Name.Value.Unique", ResourceValue = "Locale string resource name is unique" }
			}
			_dataProvider.BulkInsertEntities(resources.ToArray()).Wait();
		}

		/// <summary>
		/// Down migration
		/// </summary>
		public override void Down() {

		}
	}
}
```

## Validators
Trong thư mục Validators, tạo class Validator mới, thừa kế từ class BaseNeptuneValidator.


```c#
using FluentValidation;
using Jits.Neptune.Web.Framework.Models.Localization;
using Jits.Neptune.Web.Framework.Services.Localization;

namespace Jits.Neptune.Web.Framework.Validators.Localization {
	/// <summary>
	/// 
	/// </summary>
	public partial class LocaleStringResourceValidator : BaseNeptuneValidator<LocaleStringResourceModel> {
		/// <summary>
		/// Validator for locale string resource
		/// </summary>
		public LocaleStringResourceValidator(ILocalizationService localizationService) {
			RuleFor(model => model.Language).NotEmpty().WithMessageAwait(localizationService.GetResource("Common.LocaleStringResource.Language.Value.Required"));
		}
	}
}
```

- `NotNull()`: kiểm tra giá trị không được rỗng. 
- `NotEmpty()`: kiểm tra giá trị không được rỗng, hoặc khoảng trắng, hoặc giá trị 0 với kiểu số.
- `Null()`: kiểm tra giá trị phải bằng rỗng.
- `Empty()`: kiểm tra giá trị phải bằng rỗng, hoặc bằng 0 với giá trị số. 
- `NotEqual(value)`: kiểm tra giá trị phải bằng với value.
- `Equal(value)`: kiểm tra giá trị phải bằng với value.
- `LessThan(value)`: kiểm tra gía trị phải nhỏ hơn value.
- `LessThanOrEqualTo(value)`: kiểm tra giá trị phải nhỏ hơn hoặc bằng value.
- `GreaterThan(value)`: kiểm tra giá trị phải lớn hơn value.
- `GreaterThanOrEqualTo(value)`: kiểm tra giá trị phải lớn hơn hoặc bằng value.
- `MaximumLength(length)`: kiểm tra độ dài tối đa của giá trị 
- `MinLength(length)`: kiểm tra độ dài tối thiểu của giá trị 
- `Must(predicate)`: kiểm tra giá trị phải thoả biểu thức truyền vào
- `InclusiveBetween(from, to)`: kiểm tra giá trị phải trong khoảng from <= value <= to
- `ExclusiveBetween(from, to)`: kiểm tra giá trị phải trong khoảng from < value < to