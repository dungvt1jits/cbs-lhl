# Giới thiệu

- Theo cơ chế hoạt động `Archiving & Purging`, Neptune cho phép sử dụng Database `Archive` có loại khác với database `Online`. Chẳng hạn như DB `Online` dùng `MySQL`, còn db `Archiving` thì dùng `PostgresQL`.

- Các tham số kết nối đến DB `Archiving` được đặt trong hệ thống biến môi trường `ENVIRONMENT_VARIABLE` của db `Online`.

# Cấu hình
Các tham số liên quan có tiền tố là `DB_ARCHIVE_`
  - `DB_ARCHIVE_SERVER_NAME`: tên hoặc IP của server cài đặt database

  - `DB_ARCHIVE_NAME`: tên database

  - `DB_ARCHIVE_SCHEMA`: tên schema chứa đối tượng dữ liệu kết nối đến

  - `DB_ARCHIVE_USERNAME`: tên user truy cập

  - `DB_ARCHIVE_PASSWORD`: password tương ứng với user truy cập. `Định dạng là base64`.

  - `DB_ARCHIVE_RDBMS_TYPE`: loại CSLD, có 4 giá trị:
    - `Oracle`
    - `MySQL`
    - `SQLServer`
    - `PostgresQL`



