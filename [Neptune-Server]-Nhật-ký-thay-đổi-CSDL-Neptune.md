# `2022-Jul-30` Issue https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/issues/1929
- WF_EXEC

```sql
CREATE TABLE `WF_EXEC_DONE` (
    `EXECUTION_ID` VARCHAR(100) NOT NULL,
    `USER_ID` VARCHAR(100) NOT NULL,
    `ORGANIZATION_ID` VARCHAR(100) NOT NULL,
    `INPUT` TEXT NOT NULL,
    `WFID` VARCHAR(100) NOT NULL,
    `LANG` VARCHAR(2) NOT NULL,
    `CREATED_ON` BIGINT NOT NULL,
    `STATUS` VARCHAR(100) NOT NULL,
    `FINISH_ON` BIGINT NOT NULL,
    `IS_SUCCESS` VARCHAR(100) NOT NULL,
    `IS_TIMEOUT` VARCHAR(100) NOT NULL,
    `IS_PROCESSING` VARCHAR(100) NOT NULL,
    `STOP_ERROR` TEXT,
    `WORKFLOW_TYPE` LONGTEXT NOT NULL,
    `REVERSED_EXECUTION_ID` LONGTEXT NOT NULL,
    `REVERSED_BY_EXECUTION_ID` LONGTEXT NOT NULL,
    PRIMARY KEY (`EXECUTION_ID`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

```
- WF_GROUP
```sql
CREATE TABLE `WF_GROUP_EXEC_DONE` (
    `EXECUTION_ID` VARCHAR(100) NOT NULL,
    `STEP_GROUP` INT NOT NULL,
    `P1_STATUS` VARCHAR(100) NOT NULL,
    `P2_STATUS` VARCHAR(100) NOT NULL,
    `P1_START` BIGINT NOT NULL,
    `P1_FINISH` BIGINT NOT NULL,
    `P1_ERROR` TEXT,
    PRIMARY KEY (`EXECUTION_ID` , `STEP_GROUP`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

```
- WF_STEP_EXEC

```sql
CREATE TABLE `WF_STEP_EXEC_DONE` (
    `STEP_EXECUTION_ID` VARCHAR(100) NOT NULL,
    `EXECUTION_ID` VARCHAR(100) NOT NULL,
    `WORKFLOW_ID` VARCHAR(100) DEFAULT NULL,
    `STEP_GROUP` INT NOT NULL,
    `STEP_ORDER` INT NOT NULL,
    `STEP_CODE` VARCHAR(100) DEFAULT NULL,
    `P1_REQUEST_ID` VARCHAR(100) DEFAULT NULL,
    `P1_START` BIGINT NOT NULL,
    `P1_FINISH` BIGINT NOT NULL,
    `P1_STATUS` VARCHAR(100) NOT NULL,
    `P1_ERROR` TEXT NOT NULL,
    `P1_CONTENT` TEXT NOT NULL,
    `P2_REQUEST_ID` VARCHAR(100) DEFAULT NULL,
    `P2_START` BIGINT NOT NULL,
    `P2_FINISH` BIGINT NOT NULL,
    `P2_STATUS` VARCHAR(100) NOT NULL,
    `P2_RESPONSE_STATUS` VARCHAR(100) DEFAULT NULL,
    `P2_ERROR` TEXT,
    `P2_CONTENT` TEXT,
    `IS_SUCCESS` VARCHAR(100) NOT NULL,
    `IS_TIMEOUT` VARCHAR(100) NOT NULL,
    `P3_START` BIGINT NOT NULL,
    `P3_FINISH` BIGINT NOT NULL,
    `P3_STATUS` VARCHAR(100) NOT NULL,
    `P3_CONTENT` TEXT,
    `P3_ERROR` TEXT,
    `SENDING_CONDITION` TEXT,
    PRIMARY KEY (`STEP_EXECUTION_ID`),
    KEY `IX_WF_STEP_EXEC_DONE_EXECUTION_ID` (`EXECUTION_ID`),
    KEY `IX_WF_STEP_EXEC_DONE_P1_FINISH` (`P1_FINISH`),
    KEY `IX_WF_STEP_EXEC_DONE_P1_START` (`P1_START`),
    KEY `IX_WF_STEP_EXEC_DONE_P2_FINISH` (`P2_FINISH`),
    KEY `IX_WF_STEP_EXEC_DONE_P2_START` (`P2_START`),
    KEY `IX_WF_STEP_EXEC_DONE_STEP_CODE` (`STEP_CODE`),
    KEY `IX_WF_STEP_EXEC_DONE_WORKFLOW_ID` (`WORKFLOW_ID`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;
```


# `2022-Aug-09` Issue https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/issues/1961

```sql
ALTER TABLE `neptune`.`WF_DEF` 
ADD COLUMN `KeepExecSeconds` BIGINT NULL DEFAULT 0 AFTER `EventWorkflowStepCompensated`;

ALTER TABLE `neptune`.`WF_EXEC` 
ADD COLUMN `ARCHIVING_TIME` BIGINT NULL DEFAULT 0 AFTER `IS_DISPUTED`,
ADD COLUMN `PURGING_TIME` BIGINT NULL DEFAULT 0 AFTER `ARCHIVING_TIME`;

ALTER TABLE `neptune`.`WF_EXEC_DONE` 
ADD COLUMN `ARCHIVING_TIME` BIGINT NULL DEFAULT 0 AFTER `IS_DISPUTED`,
ADD COLUMN `PURGING_TIME` BIGINT NULL DEFAULT 0 AFTER `ARCHIVING_TIME`;
```
# `2022-Aug-11` Issue https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/issues/1990

```sql
INSERT INTO `neptune`.`ENVIRONMENT_VARIABLE` (`VARIABLE_NAME`, `E1`) VALUES ('DB_PURGE_COMMAND_TIMEOUT_SECONDS', '600');
INSERT INTO `neptune`.`ENVIRONMENT_VARIABLE` (`VARIABLE_NAME`, `E1`) VALUES ('DB_GENERAL_COMMAND_TIMEOUT_SECONDS', '60');
INSERT INTO `neptune`.`ENVIRONMENT_VARIABLE` (`VARIABLE_NAME`, `E1`) VALUES ('DB_PURGE_MAX_WORKFLOWS_PER_PROCESSING', '1000');

```
