# Yêu cầu chức năng

- Xây dựng chức năng ghi Log cho các service sử dụng
- Có giao diện để theo dõi được thông tin log theo từng service
- Tính toán được thời gian dựa trên mối quan hệ giữa thời gian ghi log giữa các dòng log

## Vi dụ

| Date      | Time    | Workflow Execution ID            | Step Workflow Execution ID       | Step code                              | Service code | Subject       | Log text          | Details | Relative duration (ms) |
|-----------|---------|----------------------------------|----------------------------------|----------------------------------------|--------------|---------------|-------------------|---------|------------------------|
| 24-Aug-22 | 3:10:30 | 0019d28117024537834d03f4133146c7 | df62844bd21942c1862f3940441f0f49 | SQL_CAN_USER_INVOKE_COMMAND            | CMS          | BO Processing | Start BO analysis | {}      | 0.00                   |
| 24-Aug-22 | 3:10:31 | 0019d28117024537834d03f4133146c7 | 189cef7afb5449348ea0dd0c753bd370 | SQL_CAN_USER_INVOKE_COMMAND            | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:31 | 0019d28117024537834d03f4133146c7 | 1b46fa6a88604566900ca7f0a409c680 | SQL_CAN_USER_INVOKE_COMMAND            | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:32 | 0019d28117024537834d03f4133146c7 | f705d4d68945423f99763068e84980a4 | IFC_GETIFCFEE_INFOR                    | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:32 | 0019d28117024537834d03f4133146c7 | 8ec7235751e34b6998ce5fa59d182a13 | CRD_SPLO                               | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:33 | 0019d28117024537834d03f4133146c7 | 8b5ec41262a040e99de4642104e89972 | SQL_CAN_USER_INVOKE_COMMAND            | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:33 | 0019d28117024537834d03f4133146c7 | 8dff3cabac4b43ebaa78ecc90d9bba5f | CRD_SPLO                               | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:34 | 0019d28117024537834d03f4133146c7 | d9f0c786741449d19c5a1121c0b387da | SQL_CAN_USER_INVOKE_COMMAND            | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:35 | 0019d28117024537834d03f4133146c7 | a4e34ff310cc48c19629438bbf625a7a | CRD_SPLO                               | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:35 | 0019d28117024537834d03f4133146c7 | 567328f6af9845dbb377559e54cb68f9 | SQL_CAN_USER_INVOKE_COMMAND            | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:36 | 0019d28117024537834d03f4133146c7 | f318033675f14bcfbac5dd195d7bc971 | CRD_LIST_CREDIT_ACCOUNT_BY_CUSTOMER_ID | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:36 | 0019d28117024537834d03f4133146c7 | 015ae6ea5e67483fa2860307dc536a26 | CRD_SPLO                               | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |
| 24-Aug-22 | 3:10:37 | 0019d28117024537834d03f4133146c7 | 4f6021b9d25247a48180d22b0ccd3429 | CRD_SPLO                               | CMS          | BO Processing | End BO analysis   | {}      | 583.33                 |

# Giải pháp

## CMS

- `CMS` tạo bảng có cấu trúc như message sau:

```json
  {
    "log_utc": 1661339203729,
    "log_type": "WorkflowStepExecution",
    "execution_id": "637969360008562898",
    "step_execution_id": "637969360008562898-3",
    "step_code": "MSG_ECHO_2",
    "service_id": "MS1",
    "subject": "CreditBalance()",
    "log_text": "Account status = C",
    "json_details": "{}"
  }
```

- Viết chức năng insert, search

- Xây dựng giao diện trên app [Portal], menu ![image](uploads/341f25fe05662b561ff73e0087db7f82/image.png), chức năng `Service Logs`.

- Giao diện tính toán thêm cột cuối theo milliseconds, là số milliseconds giữa thời gian 2 dòng log

- Giao diện bố trí thành các tab độc lập lọc theo log_type trong danh sách trên và một tab All không lọc (tham khảo giao diện chức năng `Workflow Execution Monitoring`)

- Sắp xếp theo thứ tự `z-a` cột thời gian log `log_utc` 

## NeptuneClient

NeptuneClient cung cấp API để các service gọi thuận tiện

CMS là nơi nhận sự kiện và tiến hành ghi vào CSDL của mình

### Các hàm dùng để tạo đối tượng `CentralizedLog`

```cs
public CentralizedLog CreateCentralizedLog(string pSubject, string pLogText, CentralizedLogType pCentralizedLogType = CentralizedLogType.WorkflowStepExecution)

public CentralizedLog CreateCentralizedLog(string pSubject, string pLogText, object pDetails, CentralizedLogType pCentralizedLogType = CentralizedLogType.WorkflowStepExecution)

public CentralizedLog CreateCentralizedLog(string pSubject, string pLogText, object pDetails, CentralizedLogType pCentralizedLogType = CentralizedLogType.WorkflowStepExecution)

```

Trong đó, tham số `pCentralizedLogType = CentralizedLogType.WorkflowStepExecution` dùng để phân loại log:

- WorkflowExecution
- WorkflowStepExecution
- System
- TroubleShooting
- Performance
- Exception
- Error
- Info
- Warning
- Other

### Cách tạo và gửi Log

- Tạo đối tượng `CentralizedLog` thông qua `WorkflowScheme` tại sự kiện `WorkflowDelivering`

- Dùng instance của đối tượng `GrpcClient` và gọi hàm `SendCentralizedLog` để gửi 1 dòng log và gọi hàm `CreateCentralizedLogs` để gửi nhiều dòng logs.

Ví dụ:

```cs
private void NeptuneClientQueue_WorkflowDelivering(WorkflowScheme workflow)
{
        // centralized log
        var theCentralizedLog = workflow.CreateCentralizedLog("CreditBalance()", "Account status = C");
        GrpcClient NeptuneGrpcClient = new();
        NeptuneGrpcClient.SendCentralizedLog(theCentralizedLog);       
}
```

Message được gửi đến queue của service CMS có cấu trúc như sau:

```json
{
  "EventType": 13,
  "EventTypeName": "EventServiceToService",
  "EventData": {
    "data": {
      "from_service_code": "MS1",
      "to_service_code": "CMS",
      "event_type": "JITS.Neptune.NeptuneClient.Log.CentralizedLog",
      "text_data": "[{\"log_utc\":16615008018738984,\"log_type\":\"WorkflowStepExecution\",\"execution_id\":\"637970976002050485\",\"step_execution_id\":\"637970976002050485-1\",\"step_code\":\"MSG_ECHO\",\"service_id\":\"MS1\",\"subject\":\"CreditBalance()\",\"log_text\":\"Account status = C\",\"json_details\":\"{}\"}]"
    }
  },
  "raised_on_utc": 1661500801920
}
```

và `CMS` sau khi bóc dữ liệu theo điều kiện `event_type` = `JITS.Neptune.NeptuneClient.Log.CentralizedLog`, chuyển đổi phần `text_data` sang danh sách các log entry, rồi kế tiếp ghi vào bảng dữ liệu trên DB của `CMS`

> CMS có thể chuyển `text_data` về kiểu dữ liệu với giá trị của trường `event_type` (`JITS.Neptune.NeptuneClient.Log.CentralizedLog`) để thao tác dễ hơn.

> Chúng ta để ý rằng các thông tin liên quan đến `Workfow` và `Service` được lấy tự động từ đối tượng `WorkfowScheme` mà không cần phải khai báo.

```json
[
  {
    "log_utc": 16615008018738984,
    "log_type": "WorkflowStepExecution",
    "execution_id": "637970976002050485",
    "step_execution_id": "637970976002050485-1",
    "step_code": "MSG_ECHO",
    "service_id": "MS1",
    "subject": "CreditBalance()",
    "log_text": "Account status = C",
    "json_details": "{}"
  }
]
```

> Chú ý: `"log_utc": 16615008018738984,` được ghi nhận theo giá trị `tick` chứ không phải giá trị `milliseconds`. [Tham khảo thêm về tick](https://docs.microsoft.com/en-us/dotnet/api/system.datetime.ticks?view=net-6.0#remarks)

# Xem log từ Portal app

Vào menu 

![image](uploads/f288671f78aac69ddb4e5fbd1ec3d03f/image.png)

Chọn `Service Logs`,

![image](uploads/e847e97bdcbc8386df17c25d37e17676/image.png)

Nhập điều kiện tìm kiếm, thông thường là tìm kiếm theo `subject`, bấm `Search` và xem log trên lưới kết quả.


# Phần mềm

NeptuneClient [1.1.5.3+](https://hcm.jits.com.vn:8020/packages/jits.neptune.neptuneclient/1.1.5.3)