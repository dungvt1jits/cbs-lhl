
# Mô tả

Đối với nghiệp vụ mua bán ngoại tệ khác chi nhánh, hệ thống hỗ trợ với quy tắc sau;
- Bên Nợ và bên Có: trong một bút toán chỉ có duy nhất một loại tiền
- Luôn xử lý bút toán FX tại chi nhánh có loại tiền ngoại tệ trước, sau đó mới thực hiện IBT

# Bài toán FX-IBT (n-n)

| Dòng | D/C | Branch | Account | Currency | Amount | Equivalent |
|------|-----|--------|---------|----------|--------|------------|
| 1    | D   | 101    | CASH    | USD      | 1      | 2100       |
| 2    | D   | 202    | DEPOSIT | USD      | 34     | 71432      |
| 3    | C   | 303    | GL1     | EUR      | 2      | 4,619      |
| 4    | C   | 404    | GL2     | EUR      | 29     | 68,913     |

> Tổng Equivalent Nợ và Có bằng nhau: `73532`

- Sinh bút toán FX tự động

| Dòng | D/C | Branch | Account             | Currency | Amount | Equivalent |
|------|-----|--------|---------------------|----------|--------|------------|
| 1    | D   | 101    | CASH                | USD      | 1      | 2100       |
| 1.1  | C   | 101    | `FX-POSITION`-USD   | USD      | 1      | 2100       |
| 1.2  | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 2100   | 2100       |
|      |     |        |                     |          |        |            |
| 2    | D   | 202    | DEPOSIT             | USD      | 34     | 71432      |
| 2.1  | C   | 202    | `FX-POSITION`-USD   | USD      | 34     | 71432      |
| 2.2  | D   | 202    | `FX-EQUIVALENT`-USD | MMK      | 71432  | 71432      |
|      |     |        |                     |          |        |            |
| 3    | C   | 303    | GL1                 | EUR      | 2      | 4,619      |
| 3.1  | D   | 303    | `FX-POSITION`-EUR   | EUR      | 2      | 4,619      |
| 3.2  | C   | 303    | `FX-EQUIVALENT`-EUR | MMK      | 4,619  | 4,619      |
|      |     |        |                     |          |        |            |
| 4    | C   | 404    | GL2                 | EUR      | 29     | 68,913     |
| 4.1  | D   | 404    | `FX-POSITION`-EUR   | EUR      | 29     | 68,913     |
| 4.2  | C   | 404    | `FX-EQUIVALENT`-EUR | MMK      | 68,913 | 68,913     |

> Tổng Nợ và Tổng Có theo loại tiền luôn bằng nhau (cả FCY và BCY)

- Sắp xếp lại theo loại tiền cho dễ nhìn:

| Dòng | D/C | Branch | Account             | Currency | Amount | Equivalent |
|------|-----|--------|---------------------|----------|--------|------------|
| 1    | D   | 101    | CASH                | USD      | 1      | 2100       |
| 2    | D   | 202    | DEPOSIT             | USD      | 34     | 71432      |
| 1.1  | C   | 101    | `FX-POSITION`-USD   | USD      | 1      | 2100       |
| 2.1  | C   | 202    | `FX-POSITION`-USD   | USD      | 34     | 71432      |
|      |     |        |                     |          |        |            |
| 3.1  | D   | 303    | `FX-POSITION`-EUR   | EUR      | 2      | 4,619      |
| 4.1  | D   | 404    | `FX-POSITION`-EUR   | EUR      | 29     | 68,913     |
| 3    | C   | 303    | GL1                 | EUR      | 2      | 4,619      |
| 4    | C   | 404    | GL2                 | EUR      | 29     | 68,913     |
|      |     |        |                     |          |        |            |
| 1.2  | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 2100   | 2100       |
| 2.2  | D   | 202    | `FX-EQUIVALENT`-USD | MMK      | 71432  | 71432      |
| 3.2  | C   | 303    | `FX-EQUIVALENT`-EUR | MMK      | 4,619  | 4,619      |
| 4.2  | C   | 404    | `FX-EQUIVALENT`-EUR | MMK      | 68,913 | 68,913     |

- Xử lý bút toán IBT: chỉ IBT cho các dòng Equivalent mà thôi (theo nguyên tắc chỉ IBT cho tài khoản Base Currency)

| Dòng      | D/C | Branch | Account             | Currency | Amount | Equivalent |
|-----------|-----|--------|---------------------|----------|--------|------------|
| 1         | D   | 101    | CASH                | USD      | 1      | 2100       |
| 1.1       | C   | 101    | `FX-POSITION`-USD   | USD      | 1      | 2100       |
| 1.2       | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 2100   | 2100       |
| 1.2.IBT   | C   | 101    | IBT-HO              | MMK      | 2100   | 2100       |
| 1.2.IBT.* | D   | HO     | IBT-101             | MMK      | 2100   | 2100       |
|           |     |        |                     |          |        |            |
| 2         | D   | 202    | DEPOSIT             | USD      | 34     | 71432      |
| 2.1       | C   | 202    | `FX-POSITION`-USD   | USD      | 34     | 71432      |
| 2.2       | D   | 202    | `FX-EQUIVALENT`-USD | MMK      | 71432  | 71432      |
| 2.2.IBT   | C   | 202    | IBT-HO              | MMK      | 71432  | 71432      |
| 2.2.IBT.* | D   | HO     | IBT-202             | MMK      | 71432  | 71432      |
|           |     |        |                     |          |        |            |
| 3         | C   | 303    | GL1                 | EUR      | 2      | 4,619      |
| 3.1       | D   | 303    | `FX-POSITION`-EUR   | EUR      | 2      | 4,619      |
| 3.2       | C   | 303    | `FX-EQUIVALENT`-EUR | MMK      | 4,619  | 4,619      |
| 3.2.IBT   | D   | 303    | IBT-HO              | MMK      | 4,619  | 4,619      |
| 3.2.IBT.* | C   | HO     | IBT-303             | MMK      | 4,619  | 4,619      |
|           |     |        |                     |          |        |            |
| 4         | C   | 404    | GL2                 | EUR      | 29     | 68,913     |
| 4.1       | D   | 404    | `FX-POSITION`-EUR   | EUR      | 29     | 68,913     |
| 4.2       | C   | 404    | `FX-EQUIVALENT`-EUR | MMK      | 68,913 | 68,913     |
| 4.2.IBT   | D   | 404    | IBT-HO              | MMK      | 68,913 | 68,913     |
| 4.2.IBT.* | C   | HO     | IBT-404             | MMK      | 68,913 | 68,913     |

- Sắp xếp lại theo chi nhánh, loại tiền và kiểm tra ràng buộc kế toán:

| Dòng      | D/C | Branch | Account             | Currency | Amount | Equivalent |
|-----------|-----|--------|---------------------|----------|--------|------------|
| 1         | D   | 101    | CASH                | USD      | 1      | 2100       |
| 1.1       | C   | 101    | `FX-POSITION`-USD   | USD      | 1      | 2100       |
|           |     |        |                     |          |        |            |
| 1.2       | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 2100   | 2100       |
| 1.2.IBT   | C   | 101    | IBT-HO              | MMK      | 2100   | 2100       |
|           |     |        |                     |          |        |            |
| 2         | D   | 202    | DEPOSIT             | USD      | 34     | 71432      |
| 2.1       | C   | 202    | `FX-POSITION`-USD   | USD      | 34     | 71432      |
|           |     |        |                     |          |        |            |
| 2.2       | D   | 202    | `FX-EQUIVALENT`-USD | MMK      | 71432  | 71432      |
| 2.2.IBT   | C   | 202    | IBT-HO              | MMK      | 71432  | 71432      |
|           |     |        |                     |          |        |            |
| 3         | C   | 303    | GL1                 | EUR      | 2      | 4,619      |
| 3.1       | D   | 303    | `FX-POSITION`-EUR   | EUR      | 2      | 4,619      |
|           |     |        |                     |          |        |            |
| 3.2       | C   | 303    | `FX-EQUIVALENT`-EUR | MMK      | 4,619  | 4,619      |
| 3.2.IBT   | D   | 303    | IBT-HO              | MMK      | 4,619  | 4,619      |
|           |     |        |                     |          |        |            |
| 4         | C   | 404    | GL2                 | EUR      | 29     | 68,913     |
| 4.1       | D   | 404    | `FX-POSITION`-EUR   | EUR      | 29     | 68,913     |
|           |     |        |                     |          |        |            |
| 4.2       | C   | 404    | `FX-EQUIVALENT`-EUR | MMK      | 68,913 | 68,913     |
| 4.2.IBT   | D   | 404    | IBT-HO              | MMK      | 68,913 | 68,913     |
|           |     |        |                     |          |        |            |
| 1.2.IBT.* | D   | HO     | IBT-101             | MMK      | 2100   | 2100       |
| 2.2.IBT.* | D   | HO     | IBT-202             | MMK      | 71432  | 71432      |
| 3.2.IBT.* | C   | HO     | IBT-303             | MMK      | 4,619  | 4,619      |
| 4.2.IBT.* | C   | HO     | IBT-404             | MMK      | 68,913 | 68,913     |

Tóm tắt bút toán:
- Chi nhánh 101 mua 1 USD (tương đương 2100 MMK)
- Chi nhánh 202 mua 34 USD (tương đương 71432 MMK )
- Chi nhánh 303 bán 2 EUR (tương đương 4,619 MMK)
- Chi nhánh 202 mua 29 EUR (tương đương 68,913 MMK)
- Hội sở ghi nhận giao dịch liên chi nhánh bằng MMK tương ứng với (2100 + 71432 = 4,619 + 68,913 = 73532)
