# Nguyên tắc chung

- Bảng master trans sẽ có tên dạng `xxxTrans`. Trong đó xxx là tên master object. Ví dụ `CreditAccount`Trans, `DepositAccount`Trans, `CreditIFCBalance`Trans...

- Bảng cấu hình tài khoản GL tương ứng cấu hình trong bảng có tên dạng `xxxGLConfig`. Chẳng hạn `CreditAccount`GLConfig, `DepositAccount`GLConfig, `CreditIFCBalance`GLConfig, `DepositIFCBalance`GLConfig...

- Bảng ghi nhận các GL entries được sinh ra lưu trong bảng `GLEntries` từng service. Bảng này sẽ được tổng hợp lại theo [ngày, tài khoản GL, Nợ/Có, SUM(Amount)] rồi gửi sang Accounting

# Deposit

## Gốc

- Master trans phát sinh ghi vào bảng `DepositAccountTrans`

- Cấu hình GL tương ứng trong bảng `DepositAccountGLConfig`

## Lãi

- Master trans phát sinh ghi vào bảng `DepositIFCBalanceTrans`

- Cấu hình GL tương ứng trong bảng `DepositIFCBalanceGLConfig`

# Credit

## Classification

- Master trans phát sinh ghi vào bảng `ClassificationTrans`

- Cấu hình GL tương ứng trong bảng `ClassificationGLConfig`

## Provisioning

- Dữ liệu phát sinh được ghi trong bảng `ProvisioningTrans`

- Dữ liệu cấu hình được ghi trong bảng `ProvisioningGLConfig`

- Đối với nghiệp vụ dự phòng khoản vay, khi có sự thay đổi về giá trị dự phòng (đối với ShweBank, Số tiền dự phòng = `OS + Interest + Pennalty - SecuredAmount`).

## Nghiệp vụ liên quan đến gốc tài khoản

- Master trans phát sinh ghi vào bảng `CreditAccountTrans`

- Cấu hình GL tương ứng trong bảng `CreditAccountGLConfig`

## Nghiệp vụ liên quan đến lãi

- Master trans phát sinh ghi vào bảng `CreditIFCBalanceTrans`

- Cấu hình GL tương ứng trong bảng `CreditIFCBalanceGLConfig`