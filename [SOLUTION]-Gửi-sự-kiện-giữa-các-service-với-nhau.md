# Mô tả

Trong Neptune, có 2 hình thức tương tác giữa các service với nhau
- Theo hình thức đồng bộ: dùng `gRPC`
- Theo hình thức bất đồng bộ dùng `NeptuneEvent`


# Mô hình gửi sự kiện trong Neptune

## Bài toán đồng bộ dữ liệu giữa các service

- Chức năng cập nhật thông tin khách hàng:

  - Người dùng mở chức năng hiệu chỉnh thông tin khách hàng

  - Thay đổi vài thông tin 

  - Bấm nút Save

  - Dữ liệu từ Browser được gửi đến service `CMS`

  - `CMS` tạo và gửi Workflow cho NeptuneServer

  - NeptuneServer gọi service `CTM` để cập nhật thông tin khách hàng bị thay đổi

  - Service `CTM` thực hiện thay đổi trên DB của mình và phản hồi trạng thái thành công đến NeptuneServer

  - NeptuneServer gửi sự kiện WorkflowCompleted cho `CMS`

  - `CMS` thông báo với người dùng yêu cầu cập nhật thông tin khách hàng đã được xử lý thành công.

Tuy nhiên, khi dữ liệu thông tin không chỉ lưu ở DB CTM mà còn được lưu ở những DB khác như DPT, CRD, MTG... và sau khi CTM thay đổi xong thì dữ liệu liên quan tại các DB khác trở nên không còn đúng nữa.

Do đó, khi thay đổi thành công dữ liệu trên `CTM`, `CTM` cần thêm một bước nữa là thông báo với các service liên quan về trạng thái dữ liệu của mình thay đổi. Để tránh việc cập nhật lại dữ liệu từ các service khác làm ảnh hưởng đến thời gian thực thi của CTM, việc thông báo này cần phải được thực hiện bất đồng bộ. Có nghĩa là CTM chỉ gửi thông báo về sự thay đổi dữ liệu, chứ không cần quan tâm đến kết quả thực hiện việc này từ các service liên quan.

![image](uploads/9fe6814179cecce0d0b8a8a7c90e6d09/image.png)

NeptuneServer hỗ trợ hình thức gửi sự kiện bất đồng bộ giữa các service.

## Nguyên tắc gửi sự kiện

- Để gửi sự kiện giữa các service, địa chỉ nơi nhận được sử dụng chính là `ServiceCode`.

- Về mặt sử dụng các service làm việc với library `NeptuneClient`. Đến lượt mình, `NeptuneClient` sẽ làm việc với `NeptuneServer` để thực thi việc gửi này. Sở dĩ phải nhờ NeptuneServer thực hiện vì danh sách các service cũng như cấu hình hạ tầng queue đều do NeptuneServer đang quản lý trên Cache nên việc thực thi sẽ cho năng suất tốt hơn thực thi tại NeptuneClient.

- Khi cần phân phối một sự kiện đến nhiều service cùng lúc thì các service nên gọi phương thức theo từng nhóm các service code một lần. Chẳng hạn trong ví dụ trên, CTM cần gửi sự kiện đến 3 service DPT, MTG, CRD thì chỉ gọi 1 lần phương thức tạo sự kiện.

- Nội dung sự kiện được cấu trúc theo dạng `text`. Việc này tạo ra cơ chế mở việc tự phát triển nội dung sự kiện giữa các service với nhau. `NeptuneServer` chỉ hỗ trợ hạ tầng gửi một `text message` từ một `service` đến một hoặc nhiều `service` nhận. Đến lượt mình, các `service` nhận sẽ tự phân tích `message` và xử lý tương ứng.

## Nguyên tắc nhận sự kiện

- Service nhận sự kiện dưới dạng một `NeptuneEvent`, trong đó có chứa nhiều thông tin chung về sự kiện. Tuy nhiên, service chỉ cần quan tâm đến nội dung message được gửi đến mình thôi thì chỉ cần đến thẳng thuộc tính `text_data` để lấy về xử lý.

- Bản chất sự kiện mang tính chất thông báo, do đó các service tiêu thụ không cần phải phản hồi kết quả thực hiện của mình là thành công hay thất bại.

## Quản lý việc phân phối sự kiện giữa các service

- Để theo dõi được các `service` đã gửi các event đến các service nào, Neptune server có ghi nhận lại nội dung gửi trong dữ liệu sự kiện của mình. Cơ chế này giúp cho việc chẩn đoán hệ thống dễ dàng hơn. Chẳng hạn cần kiểm tra xem một Event có được gửi đến một service nào đó không.

---

# Sử dụng code

> Cần cập nhật phiên bản NeptuneClient mới hoặc bằng [1.1.5.1](https://hcm.jits.com.vn:8020/packages/jits.neptune.neptuneclient/1.1.5.1)

## Bên tạo sự kiện

- Gửi sự kiện đến 1 service: tham số `ToService` là một chuỗi chứa một `service code`

  + Tạo đối tượng `GrpcClient`

  + Gọi hàm 

```c#
public void RaiseServiceToServiceEvent(string pTextData, string pFromServiceCode, string pToServiceCode);
public void RaiseServiceToServiceEvent(object pObjectData, string pFromServiceCode, string pToServiceCode);
```

Ví dụ


```c#
[HttpPost]
[Route("send-servicetoservice-event")]
public WorkflowResponse<object> SendServiceToServiceEvent()
{
    var gRPCClient = new GrpcClient();

    string strFrom_service_code = "MS1"; // assign FROM service code
    string strTo_service_code = "MS1";  // assign TO service code

    var message = new List<string> { "a", "b", "c" }; // create a custom message

    // Serialized your message object to a string, then raise by this string
    string strSerializedMessage = JsonSerializer.Serialize(message);
    gRPCClient.RaiseServiceToServiceEvent(strSerializedMessage, strFrom_service_code, strTo_service_code);

    // or raised events from your object type directly

    var messageObject = new List<string> { "a", "b", "c" }; // create a custom message
    gRPCClient.RaiseServiceToServiceEvent(messageObject, strFrom_service_code, strTo_service_code);

    return null;
}
```

- Gửi đến nhiều service: tham số `ToService` là một List các `service code`
  + Tạo đối tượng `GrpcClient`

  + Gọi phương thức

```c#
public void RaiseServiceToServiceEvents(string pTextData, string pFromServiceCode, List<string> pToServiceCodeList);
public void RaiseServiceToServiceEvents(object pObjectData, string pFromServiceCode, List<string> pToServiceCodeList);
```

Ví dụ

```c#
[HttpPost]
[Route("send-many-servicetoservice-events")]
public WorkflowResponse<object> SendManyServiceToServiceEvents()
{
    var gRPCClient = new GrpcClient();

    string strFrom_service_code = "MS1"; // assign FROM service code
    string strTo_service_code = "MS1";  // assign TO service code

    var message = new { key = "some-key", value = "sapmle-value" }; // create a custom message

    string strSerializedMessage = JsonSerializer.Serialize(message);

    List<string> ListToServiceCodes = new() { strTo_service_code, strTo_service_code, strTo_service_code };

    gRPCClient.RaiseServiceToServiceEvents(strSerializedMessage, strFrom_service_code, ListToServiceCodes);

    // or raised events from your object type directly
    var messageObject = new List<string> { "a", "b", "c" }; // create a custom message
    gRPCClient.RaiseServiceToServiceEvents(messageObject, strFrom_service_code, ListToServiceCodes);

    return null;
}
```

## Bên tiêu thụ sự kiện (Bắt sự kiện phía service)

- Khai báo 1 instance đối tượng `QueueClient`

- Móc sự kiện vào hàm xử lý

```c#
QueueController.NeptuneQueueClient = new QueueClient();
QueueController.NeptuneQueueClient.ServiceToServiceEvent += NeptuneQueueClient_ServiceToServiceEvent;
```

- Xử lý sự kiện trong hàm

```c#
private void NeptuneQueueClient_ServiceToServiceEvent(NeptuneEvent<ServiceToServiceEventData> pServiceToServiceEvent)
{
    string strLog = "NeptuneQueueClient_ServiceToServiceEvent: \n";
    strLog += "text_data=" + pServiceToServiceEvent.EventData.data.text_data;
    Console.WriteLine(strLog);

    // or deserialize directly
    var list = pServiceToServiceEvent.EventData.data.Deserialize<List<string>>();
    Console.WriteLine(list);
}
```

> `.EventData.data.text_data` chứa chuỗi dữ liệu được gửi đi trước đó (gRPCClient.RaiseServiceToServiceEvent(**strSerializedMessage**, strFrom_service_code, strTo_service_code);)