To @khailm , @nhidy 

Tài liệu này chứa các đầu việc liên quan đến dự án cần phải hoàn thành (Checklist để nắm rõ trạng thái công việc)

Yêu cầu Khai và Nhi cập nhật thời gian thực, liên kết với các Isssue nếu có để theo dõi chi tiết.

# Tài liệu

## Technical document

- [x] Technical solution: tài liệu giải pháp kỹ thuật của Core Banking Neptune 1.3
- [ ] Trouble shooting
- [ ] Migration upload (upload từng phần hoặc toàn phần)
- [ ] Installation (tài liệu cài đặt hệ thống Corebanking Neptune)
- [ ] System Operation (Portal & Admin apps)

## Specification Documents
- [x] Spec các phân hệ theo phạm vi KTB [docs](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/Business%20Requirement%20Specification)
- [ ] 

## Test evidence

- [ ] Tài liệu Unit Test các chức năng có trạng thái đã test xong, nếu dùng phiên bản KTB thì cung cấp tài liệu tương ứng

# GAP document

(tài liệu GAP làm với khách hàng, các mục đã được khách hàng xác nhận)

- [ ] DPT product (10 products)
- [ ] Payment
- [ ] Accounting
- [ ] Loan
- [ ] Mortgage
- [ ] Fixed Asset
- [ ] Cash
- [ ] Admin
- [ ] Security

# Các tùy chỉnh hệ thống theo GAP (mô tả và giải pháp tương ứng, link issue kèm theo)

## Accounting
- [ ] CoA: hỗ trợ đa kế toán đồ
- [ ] Multi CoA

## Customer

## Deposit

## Loan

(Qua tết Âm lịch mới làm BPA bên ShweBank)

## Payment

## Mortgage

# Quy ước chung toàn hệ thống

## Quy ước về hiển thị lỗi với người dùng

## Quy ước về trao đổi lỗi qua Open API

