# Giới thiệu

- Để kết nối đến Database `Online`, Neptune dùng các biến môi trường của hệ điều hành chứ không cấu hình từ file hay trong CSDL


# Cấu hình

- `NEPTUNE_DB_ONLINE_SERVER_NAME`: tên server hoặc IP của máy chủ cài đặt database `online`

- `NEPTUNE_DB_ONLINE_NAME`: Tên Database truy cập đến

- `NEPTUNE_DB_ONLINE_SCHEMA`: Schema truy cập đến

- `NEPTUNE_DB_ONLINE_RDBMS_TYPE`: loại database, có 4 giá trị (`Oracle`, `MySQL`, `SQLServer`, `PostgresQL`)

- `NEPTUNE_DB_ONLINE_USERNAME`: tên user truy cập

- `NEPTUNE_DB_ONLINE_PASSWORD`: password tương ứng với user truy cập. Chú ý `định dạng base64`.

Do đó nếu chạy từ môi trường không phải container, bạn cần thiết lập các biến này.

# Ví dụ

Chẳng hạn trên môi trường Windows OS, tạo file bat có nội dung như sau:

```bat
setx NEPTUNE_DB_ONLINE_SERVER_NAME localhost
setx NEPTUNE_DB_ONLINE_NAME NeptuneDB
setx NEPTUNE_DB_ONLINE_SCHEMA NeptuneDB
setx NEPTUNE_DB_ONLINE_RDBMS_TYPE MySQL
setx NEPTUNE_DB_ONLINE_USERNAME neptune_user
setx NEPTUNE_DB_ONLINE_PASSWORD QWJjQDEyMzQ1Ng==
```
> Thay các giá trị phù hợp

Còn trên môi trường container, bạn cần truyền giá trị này thông qua syntax của từng công cụ (docker run hay docker-compose)
```yml
  neptuneserver:
    image: ${DOCKER_REGISTRY-}neptuneserver
    build:
      context: .
      dockerfile: neptuneserver/Dockerfile_deploy
    container_name: neptuneserver
    environment:
      - NEPTUNE_DB_ONLINE_SERVER_NAME=neptunedb_mysql
      - NEPTUNE_DB_ONLINE_NAME=neptune
      - NEPTUNE_DB_ONLINE_SCHEMA=neptune
      - NEPTUNE_DB_ONLINE_RDBMS_TYPE=MySQL
      - NEPTUNE_DB_ONLINE_USERNAME=neptune
      - NEPTUNE_DB_ONLINE_PASSWORD=bmVwdHVuZQ==
```
