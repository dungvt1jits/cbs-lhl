# Giao dịch batch

- Sử dụng StoredProcedure trên từng database của service để đạt được hiệu suất tính toán
- Cũng theo quy tắc tính toán và ghi nhận vào bảng `xxxTrans` trước.
- Tạo bút toán GL tương ứng từ bảng `xxxTrans` này
- Tổng hợp bút toán (group by) và đồng bộ sang service ACT

# Danh sách Batch Steps (bản ShweBank)

- [Danh sách Batch Steps](https://hcm.jits.com.vn:8062/rndhcm/cbs-shwe/-/wikis/%5BBatch%5D%20Danh%20s%C3%A1ch%20s%C3%A1ch%20giao%20d%E1%BB%8Bch%20batch)
