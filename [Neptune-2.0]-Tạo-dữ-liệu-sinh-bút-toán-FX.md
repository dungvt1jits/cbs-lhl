# Mô tả

Giả định Base currency là `MMK`

Bút toán `FX` sẽ không được tạo tại service ACT nữa, mà sẽ được chuyển xuống phân hệ chức năng thực hiện.

Ví dụ: ta có giao dịch mua bán ngoại tệ giữa 2 tài khoản Cash khác loại tiền:
- `102-1111101`: loại tiền `USD`
- `102-1111102`: loại tiền `MMK`

Theo quy trình cũ, bút toán chuyển khoản sẽ có dạng sau:
| Dr/Cr | Account | Amount | Currency |
|-------|---------|--------|----------|
| DR    | CASH    | 100    |  USD     |
| CR    | CASH    | 2100   |  MMK     |

Sau đó, hệ thống phát hiện chéo tiền tệ nên sẽ sinh bút toán tự động tại phân hệ ACT:
| Dr/Cr   | Account       | Amount    | Currency |
|---------|---------------|-----------|----------|
| DR      | CASH          | 100       |  USD     |
| `CR`    | `FX-MMK`      | `100`     |  `USD`   |
| `DR`    | `FX-USD`      | `2100`    |  `MMK`   |
| CR      | CASH          | 2100      |  MMK     |

# Quy trình mới

Xem lại cấu trúc bảng MasterTrans


 
