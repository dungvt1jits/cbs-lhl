# Cài đặt `Functional Application Server`

- OS: `Windows Server 2022`
- IIS: cài tính năng này trong Windows
- Cài đặt Dotnet SDK theo phiên bản (hiện tại đang dùng bản Dotnet SDK 6.)
- Cài đặt Dotnet `Hosting Bundle phiên bản 7.0.3` (để IIS có thể kết nối với DotNet Runtime khi start ứng dụng Dotnet core)
- Khởi động lại hệ thống
- Cài đặt RabbitMQ
- Cấu hình app `Neptune Server`
- Cấu hình các `service còn lại`
- Tạo các Site tương ứng với các microservice bằng giao diện hoặc command-line `appcmd`

# Cài đặt `Report Application Server`

Cài đặt tương tự `Functional Application Server`

# Cài đặt Database server

- Cài đặt Server Database `Microsoft SQL Server 2022`, phiên bản `Enterprise`
- Tạo các users `login` cần thiết tương ứng với các microservice trong Neptune Core banking

Lưu ý:

- Thư mục cài đặt
- Quy ước cách đặt tên cho từng database (service) file name

# Check list

Liệt kê các thao tác kiểm tra hệ thống sau khi cài đặt để có cơ sở xác nhận đã cài đặt đúng.
