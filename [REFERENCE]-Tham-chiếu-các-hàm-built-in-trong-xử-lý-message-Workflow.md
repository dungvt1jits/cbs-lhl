# Mô tả

- Các hàm built-in trong Neptune system là các hàm được xây dựng sẵn và chỉ được hỗ trợ bởi Neptune system.

- Các hàm này được dùng trong việc cấu hình message gửi đến các service:
   - `Sending_Template`, `Sending_Condition` khi cấu hình các workflow step
   - `Điều kiện thỏa mãn entry posting`, `giá trị của entry` khi cấu hình bút toán.

# Quy ước

- Không phân biệt hoa/thường

- Tên hàm có thể dùng các ký tự mang ý nghĩa ngăn cách như khoảng trắng ` `, '-', `_` để tường minh hơn khi đọc. Các ký tự này sẽ bị loại bỏ khi xử lý hàm. Chẳng hạn, các hàm sau đều có giá trị như sau:
   - `MapFromInput`
   - `Map-From-Input`
   - `mapfrominput`
   - `MAP FROM INPUT`

# Cú pháp

- Dùng cấu trúc json

```json
{
	"func": "MapFromInput",
	"type": "string",
	"paras": [
	  "$.execution.input.fields.ACCOUNT_NO"
	]
}
```

Trong đó:

- `func`: là tên hàm. Tham khảo mục sau để biết danh sách hàm được hỗ trợ
- `type`: kiểu dữ liệu trả vể của hàm
- `paras`: mảng tham số truyền cho hàm

> Chú ý: kể từ ngày `20-Aug-2022` attribute `type` có thể bỏ qua không cần chỉ định, kiểu dữ liệu của hàm được xác định rõ ràng căn cứ trên chức năng của hàm. Hoặc đối với hàm có kiểu dữ liệu là đối tượng, chẳng hạn hàm `MapFromInput`, kiểu giá trị thực tế của hàm được xác định runtime tùy theo kiểu dữ liệu thực tế của tham số.
  > Chẳng hạn: 
  > ```json
  > {
  >	"func": "MapFromInput",
  >	"paras": [
  >	  "$.execution.input.fields.ACCOUNT_NO"
  >	]
  > }
  > ```
  > Hàm MapFromInput sẽ tự hiểu là kiểu string nếu mục `$.execution.input.fields.ACCOUNT_NO` chứa giá trị chuỗi.


Các hàm built-in này hỗ trợ JPath theo chuẩn của Newtonsoft. Tham khảo [Querying JSON with complex JSON](https://www.newtonsoft.com/json/help/html/QueryJsonSelectTokenJsonPath.htm)

# Kiểu dữ liệu trả về của các hàm

| Tên Hàm               | Alias         | Kiểu trả về | Mô tả                                                                                   |
|-----------------------|---------------|-------------|-----------------------------------------------------------------------------------------|
| And                   | &&            | bool        | Đánh giá tất cả các phần tử trong paras đều là true                                     |
| IsBooleanEqual        | === hoặc b==b | bool        | Kiểm tra giá trị 2 phần tử boolean có bằng nhau không                                   |
| IsEmpty               |               | bool        | Kiểm tra 1 chuỗi có rỗng không                                                          |
| IsEquals              | ==            | bool        | Kiểm tra 2 số có bằng nhau không                                                        |
| IsGreaterThan         | >             | bool        | Kiểm tra tham số 1 có lớn hơn tham số 2 không                                           |
| IsGreaterThanOrEquals | >=            | bool        | Kiểm tra tham số 1 có lớn hơn hay bằng tham số 2 không                                  |
| IsLessThan            | <             | bool        | Kiểm tra tham số 1 có nhỏ hơn tham số 2 không                                           |
| IsLessThanOrEquals    | <=            | bool        | Kiểm tra tham số 1 có nhỏ hơn hay bằng tham số 2 không                                  |
| IsNotEmpty            |               | bool        | Kiểm tra một chuỗi có rỗng không                                                        |
| IsNotEquals           | !=            | bool        | Kiểm tra 2 số có khác nhau không                                                        |
| IsNotNull             |               | bool        | Kiểm tra 1 giá trị có khác null không                                                   |
| IsNotNullNotEmpty     |               | bool        | Kiểm tra 1 giá trị có phải khác null và khác chuỗi rỗng không                           |
| IsNull                |               | bool        | Kiểm tra 1 giá trị có null không                                                        |
| IsNullOrEmpty         |               | bool        | KIểm tra 1 giá trị có null hoặc empty không                                             |
| IsStringEqual         | s==s          | bool        | KIểm tra 2 giá trị chuỗi có bằng nhau không                                             |
| Divide                | /             | number      | Thực hiện phép chia 2 số. Nếu có tham số thứ 3 thì dùng tham số này để làm tròn kết quả |
| Length                | len           | number      | Tính chiều dài chuỗi                                                                    |
| Log                   |               | number      | Tính log cơ số e                                                                        |
| Log10                 |               | number      | Tính log cơ số 10                                                                       |
| Log2                  |               | number      | Tính log cơ số 2                                                                        |
| Multiply              | *             | number      | Thực hiện phép nhân giữa các tham số                                                    |
| Pi                    |               | number      | Giá trị hằng số PI                                                                      |
| Pow                   | ^             | number      | (a, b) ==> a ^ b                                                                        |
| Round                 |               | number      | Làm tròn 1 giá trị số                                                                   |
| Sqrt                  |               | number      | Căn bậc 2                                                                               |
| Sum                   |               | number      | Tính tổng tất cả các tham số                                                            |
| gRPC                  |               | object      | Gọi Grpc                                                                                |
| MapFromInput          | map           | object      | trích giá trị 1 trường nào đó từ đầu vào theo JPATH                                     |
| SimpleEvaluate        |               | object      | Tính toán biểu thức đơn giản                                                            |
| Concat                |               | string      | Ghép giá trị các tham số lại vói nhau                                                   |
| Join                  |               | string      | Ghép giá trị các tham số từ vị trí thứ hai lại với nhau, ngăn cách bởi tham số đầu tiên |
| LeftChars             |               | string      | Cắt ký tự bên trái                                                                      |
| Replace               |               | string      | Thay thế ký tự                                                                          |
| RightChars            |               | string      | Cắt ký tự bên phải                                                                     |
| SubString             |               | string      | Cắt ký tự ở giữa                                                                        |
| ToLower               |               | string      | Chuyển thành chữ thường                                                                 |
| ToUpper               |               | string      | Chuyển thành chữ hoa                                                                    |

# Tham chiếu qua ví dụ

## Nội dung message đầu vào

```json
{
  "workflowid": "TEST_001",
  "lang": "en",
  "token": "",
  "fields": {
    "ACCOUNT_NO": "123456789",
    "ACCOUNT_NAME": "Donald Trump",
    "AMOUNT": 2022,
    "AMOUNT1": 2000,
    "AMOUNT2": 2500,
    "CASA_ACCOUNT": null,
    "CASA_AMOUNT": 0,
    "CUST_ADDRESS": "",
    "DESCS": "Test transaction TEST_001",
    "IFCLIST": [],
    "IS_VIP": true,
    "IS_HUMAN": false
  }
}
```

## Cấu hình Template

```json
{
  "ACCOUNT_NO_MapFromInput": {
    "func": "MapFromInput",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO"
    ]
  },
  "ACCOUNT_NO_Invalid_func": {
    "func": "Invalid_func",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO"
    ]
  },
  "ACCOUNT_NO_LeftChars_3": {
    "func": "LeftChars",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO",
      3
    ]
  },
  "ACCOUNT_NO_RightChars_2": {
    "func": "RightChars",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO",
      2
    ]
  },
  "ACNO_replace_3_BY_*": {
    "func": "replace",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO",
      "3",
      "*"
    ]
  },
  "ACNO_concat_ACCOUNT_NAME": {
    "func": "concat",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO",
      "$.execution.input.fields.ACCOUNT_NAME"
    ]
  },
  "ACCOUNT_NO_join_ACCOUNT_NAME": {
    "func": "join",
    "type": "string",
    "paras": [
      "---",
      "$.execution.input.fields.ACCOUNT_NO",
      "$.execution.input.fields.ACCOUNT_NAME"
    ]
  },
  "CASA_ACCOUNT_IsNull": {
    "func": "IsNull",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.CASA_ACCOUNT"
    ]
  },
  "CASA_ACCOUNT_IsNotNull": {
    "func": "IsNotNull",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.CASA_ACCOUNT"
    ]
  },
  "ACCOUNT_NO_IsNull": {
    "func": "IsNull",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO"
    ]
  },
  "ACCOUNT_NO_IsNotNull": {
    "func": "IsNotNull",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO"
    ]
  },
  "CUST_ADDRESS_IsEmpty": {
    "func": "IsEmpty",
    "type": "string",
    "paras": [
      "$.execution.input.fields.CUST_ADDRESS"
    ]
  },
  "CUST_ADDRESS_IsNotEmpty": {
    "func": "IsNotEmpty",
    "type": "string",
    "paras": [
      "$.execution.input.fields.CUST_ADDRESS"
    ]
  },
  "ACCOUNT_NAME_IsNotEmpty": {
    "func": "IsNotEmpty",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NAME"
    ]
  },
  "CASA_ACCOUNT_IsNullOrEmpty": {
    "func": "IsNullOrEmpty",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.CASA_ACCOUNT"
    ]
  },
  "CUST_ADDRESS_IsNullOrEmpty": {
    "func": "IsNullOrEmpty",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.CUST_ADDRESS"
    ]
  },
  "CUST_ADDRESS_IsNotNullNotEmpty": {
    "func": "IsNotNullNotEmpty",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.CUST_ADDRESS"
    ]
  },
  "ACCOUNT_NAME_IsNotNullNotEmpty": {
    "func": "IsNotNullNotEmpty",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NAME"
    ]
  },
  "ACNO_SUBSTRING_4_5": {
    "func": "substring",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NAME",
      4,
      5
    ]
  },
  "ACCOUNT_NAME_ToUpper": {
    "func": "ToUpper",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NAME"
    ]
  },
  "ACCOUNT_NAME_ToLower": {
    "func": "ToLower",
    "type": "string",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NAME"
    ]
  },
  "ACCOUNT_NAME_Length": {
    "func": "Length",
    "type": "number",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NAME"
    ]
  },
  "ACCOUNT_NO_IsStringEqual_ACCOUNT_NAME": {
    "func": "IsStringEqual",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO",
      "$.execution.input.fields.ACCOUNT_NAME"
    ]
  },
  "ACCOUNT_NO_IsStringEqual_ACCOUNT_NO": {
    "func": "IsStringEqual",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.ACCOUNT_NO",
      "$.execution.input.fields.ACCOUNT_NO"
    ]
  },
  "AMOUNT": {
    "func": "MapFromInput",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT"
    ]
  },
  "AMOUNT1_equals_AMOUNT2": {
    "func": "IsEqual",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      "$.execution.input.fields.AMOUNT2"
    ]
  },
  "AMOUNT1_==_AMOUNT2": {
    "func": "==",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      "$.execution.input.fields.AMOUNT2"
    ]
  },
  "AMOUNT1_equal_2000": {
    "func": "IsEqual",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1_==_2000": {
    "func": "==",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1_IsNotEqual_2000": {
    "func": "IsNotEqual",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1_!=_2000": {
    "func": "!=",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1_<>_2000": {
    "func": "<>",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1_IsLessThan_2000": {
    "func": "IsLessThan",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1_IsLessThanOrEqual_2000": {
    "func": "IsLessThanOrEqual",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1_<=_2000": {
    "func": "<=",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1_IsGreaterThanOrEqual_2000": {
    "func": ">=",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1_>=_2000": {
    "func": ">=",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      2000
    ]
  },
  "AMOUNT1+AMOUNT2": {
    "func": "Sum",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      "$.execution.input.fields.AMOUNT2"
    ]
  },
  "AMOUNT1-AMOUNT2": {
    "func": "Sum",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      "-$.execution.input.fields.AMOUNT2"
    ]
  },
  "AMOUNT1*AMOUNT2": {
    "func": "Multiply",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      "$.execution.input.fields.AMOUNT2"
    ]
  },
  "AMOUNT1*-AMOUNT2": {
    "func": "Multiply",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      "-$.execution.input.fields.AMOUNT2"
    ]
  },
  "AMOUNT+(AMOUNT1*AMOUNT2)": {
    "func": "Sum",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT",
      {
        "func": "Multiply",
        "type": "number",
        "paras": [
          "$.execution.input.fields.AMOUNT1",
          "$.execution.input.fields.AMOUNT2"
        ]
      }
    ]
  },
  "AMOUNT+(AMOUNT1*2022.5)-AMOUNT2": {
    "func": "Sum",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT",
      {
        "func": "Multiply",
        "type": "number",
        "paras": [
          "$.execution.input.fields.AMOUNT1",
          2022.5
        ]
      },
      "-$.execution.input.fields.AMOUNT2"
    ]
  },
  "FEE": {
    "func": "MapFromInput",
    "type": "array",
    "paras": [
      "$.execution.input.fields.IFCLIST"
    ]
  },
  "Complex_Concat": {
    "func": "Concat",
    "type": "string",
    "paras": [
      "Tx:",
      "$.execution.input.workflowid",
      ". Account ",
      "$.execution.input.fields.ACCOUNT_NO",
      " - ",
      "$.execution.input.fields.ACCOUNT_NAME",
      ". CASA account: ",
      "$.execution.input.fields.CASA_ACCOUNT",
      ".",
      "$.execution.input.fields.DESCS"
    ]
  },
  "IS_VIP_IsBoolEqual_IS_HUMAN": {
    "func": "IsBoolEqual",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.IS_VIP",
      "$.execution.input.fields.IS_HUMAN"
    ]
  },
  "IS_VIP_IsBoolEqual_IS_VIP": {
    "func": "IsBoolEqual",
    "type": "boolean",
    "paras": [
      "$.execution.input.fields.IS_VIP",
      "$.execution.input.fields.IS_VIP"
    ]
  },
  "AND1": {
    "func": "&&",
    "type": "boolean",
    "paras": [
      {
        "func": "IsBoolEqual",
        "type": "boolean",
        "paras": [
          "$.execution.input.fields.IS_VIP",
          "$.execution.input.fields.IS_VIP"
        ]
      },
      {
        "func": "IsBoolEqual",
        "type": "boolean",
        "paras": [
          "$.execution.input.fields.IS_VIP",
          "$.execution.input.fields.IS_VIP"
        ]
      }
    ]
  },
  "AND2": {
    "func": "&&",
    "type": "boolean",
    "paras": [
      {
        "func": "IsBoolEqual",
        "type": "boolean",
        "paras": [
          "$.execution.input.fields.IS_VIP",
          "$.execution.input.fields.IS_VIP"
        ]
      },
      {
        "func": "IsBoolEqual",
        "type": "boolean",
        "paras": [
          "$.execution.input.fields.IS_VIP",
          "$.execution.input.fields.IS_HUMAN"
        ]
      }
    ]
  },
  "AND3": {
    "func": "&&",
    "type": "boolean",
    "paras": [
      {
        "func": "IsBoolEqual",
        "type": "boolean",
        "paras": [
          "$.execution.input.fields.IS_VIP",
          "$.execution.input.fields.IS_VIP"
        ]
      },
      false
    ]
  },
  "OR1": {
    "func": "||",
    "type": "boolean",
    "paras": [
      {
        "func": "IsBoolEqual",
        "type": "boolean",
        "paras": [
          "$.execution.input.fields.IS_VIP",
          "$.execution.input.fields.IS_VIP"
        ]
      },
      {
        "func": "IsBoolEqual",
        "type": "boolean",
        "paras": [
          "$.execution.input.fields.IS_VIP",
          "$.execution.input.fields.IS_HUMAN"
        ]
      }
    ]
  },
  "OR2": {
    "func": "||",
    "type": "boolean",
    "paras": [
      true,
      {
        "func": "IsBoolEqual",
        "type": "boolean",
        "paras": [
          "$.execution.input.fields.IS_VIP",
          "$.execution.input.fields.IS_HUMAN"
        ]
      }
    ]
  },
  "OR3": {
    "func": "||",
    "type": "boolean",
    "paras": [
      false,
      {
        "func": "IsBoolEqual",
        "type": "boolean",
        "paras": [
          "$.execution.input.fields.IS_VIP",
          "$.execution.input.fields.IS_HUMAN"
        ]
      }
    ]
  },
  "divide": {
    "func": "divide",
    "type": "number",
    "paras": [
      "$.execution.input.fields.AMOUNT1",
      3
    ]
  },
  "round": {
    "func": "round",
    "type": "number",
    "paras": [
      {
        "func": "divide",
        "type": "number",
        "paras": [
          "$.execution.input.fields.AMOUNT1",
          3
        ]
      },
      0
    ]
  },
  "pow": {
    "func": "pow",
    "type": "number",
    "paras": [
      11,
      8
    ]
  },
  "sqrt": {
    "func": "sqrt",
    "type": "number",
    "paras": [
      144
    ]
  },
  "log": {
    "func": "log",
    "type": "number",
    "paras": [
      144
    ]
  },
  "log10": {
    "func": "log10",
    "type": "number",
    "paras": [
      144
    ]
  },
  "log2": {
    "func": "log2",
    "type": "number",
    "paras": [
      144
    ]
  }
}
```
## Đầu ra sau khi được phân giải các hàm
```json
{
  "ACCOUNT_NO_MapFromInput": "123456789",
  "ACCOUNT_NO_Invalid_func": null,
  "ACCOUNT_NO_LeftChars_3": "123",
  "ACCOUNT_NO_RightChars_2": "89",
  "ACNO_replace_3_BY_*": "12*456789",
  "ACNO_concat_ACCOUNT_NAME": "123456789Donald Trump",
  "ACCOUNT_NO_join_ACCOUNT_NAME": "123456789---Donald Trump",
  "CASA_ACCOUNT_IsNull": true,
  "CASA_ACCOUNT_IsNotNull": false,
  "ACCOUNT_NO_IsNull": false,
  "ACCOUNT_NO_IsNotNull": true,
  "CUST_ADDRESS_IsEmpty": true,
  "CUST_ADDRESS_IsNotEmpty": false,
  "ACCOUNT_NAME_IsNotEmpty": true,
  "CASA_ACCOUNT_IsNullOrEmpty": true,
  "CUST_ADDRESS_IsNullOrEmpty": true,
  "CUST_ADDRESS_IsNotNullNotEmpty": false,
  "ACCOUNT_NAME_IsNotNullNotEmpty": true,
  "ACNO_SUBSTRING_4_5": "ald T",
  "ACCOUNT_NAME_ToUpper": "DONALD TRUMP",
  "ACCOUNT_NAME_ToLower": "donald trump",
  "ACCOUNT_NAME_Length": 12,
  "ACCOUNT_NO_IsStringEqual_ACCOUNT_NAME": false,
  "ACCOUNT_NO_IsStringEqual_ACCOUNT_NO": true,
  "AMOUNT": 2022,
  "AMOUNT1_equals_AMOUNT2": false,
  "AMOUNT1_==_AMOUNT2": false,
  "AMOUNT1_equal_2000": true,
  "AMOUNT1_==_2000": true,
  "AMOUNT1_IsNotEqual_2000": false,
  "AMOUNT1_!=_2000": false,
  "AMOUNT1_<>_2000": false,
  "AMOUNT1_IsLessThan_2000": false,
  "AMOUNT1_IsLessThanOrEqual_2000": true,
  "AMOUNT1_<=_2000": true,
  "AMOUNT1_IsGreaterThanOrEqual_2000": true,
  "AMOUNT1_>=_2000": true,
  "AMOUNT1+AMOUNT2": 4500,
  "AMOUNT1-AMOUNT2": -500,
  "AMOUNT1*AMOUNT2": 5000000,
  "AMOUNT1*-AMOUNT2": null,
  "AMOUNT+(AMOUNT1*AMOUNT2)": 5002022,
  "AMOUNT+(AMOUNT1*2022.5)-AMOUNT2": 4044522,
  "FEE": [],
  "Complex_Concat": "Tx:TEST_001. Account 123456789 - Donald Trump. CASA account: .Test transaction TEST_001",
  "IS_VIP_IsBoolEqual_IS_HUMAN": false,
  "IS_VIP_IsBoolEqual_IS_VIP": true,
  "AND1": true,
  "AND2": false,
  "AND3": false,
  "OR1": true,
  "OR2": true,
  "OR3": false,
  "divide": 666.66666667,
  "round": 667,
  "pow": 214358881,
  "sqrt": 12,
  "log": 4.969813299576001,
  "log10": 2.1583624920952498,
  "log2": 7.169925001442312
}
```
