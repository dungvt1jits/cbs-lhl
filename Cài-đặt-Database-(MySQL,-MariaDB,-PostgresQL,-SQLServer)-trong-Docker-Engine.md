
> **Điều kiện tiên quyết**
> > Bạn phải cài đặt thành công Docker Engine trước khi bắt đầu

Tạo file `docker-compose.yml` có nội dung sau tùy theo từng loại DB bạn muốn dùng:

## Cài DB `SQLServer` chạy trong môi trường Docker Engine

```yml
version: "3"

services:
    neptune-sqlserver:
        container_name: neptune-sqlserver-server
        hostname: neptune-sqlserver-server
        image: mcr.microsoft.com/mssql/server
        environment:
            ACCEPT_EULA: Y
            SA_PASSWORD: Neptune@123456
        ports:
            - 1433:1433
networks:
    default:
        external:
            name: neptune-network
```

## Cài DB `PostgresQL` chạy trong môi trường Docker Engine

```yml
version: "3"

services:
    neptune-postgres:
        container_name: neptune-postgres-server
        hostname: neptune-postgres-server
        image: postgres
        environment:
            POSTGRES_USER: neptune
            POSTGRES_PASSWORD: Neptune@123456
            POSTGRES_DB: NeptuneDB
            
        ports:
            - 8082:8080
            - 5432:5432
networks:
    default:
        external:
            name: neptune-network
```

## Cài DB `MySQL` chạy trong môi trường Docker Engine

```yml
version: "3"

services:
    neptune-mysql:
        container_name: neptune-mysql-server
        hostname: neptune-mysql-server
        image: mysql
        environment:
            MYSQL_USER: neptune_user
            MYSQL_PASSWORD: Abc@123456
            MYSQL_DATABASE: NeptuneDB
            MYSQL_ROOT_PASSWORD: MySQL@123456
        ports:
            - 3306:3306
            - 33060:33060

networks:
    default:
        external:
            name: neptune-network
```

## Cài DB `MariaDB` chạy trong môi trường Docker Engine

```yml
version: "3"

services:
    neptune-mariadb:
        container_name: neptune-mariadb-server
        hostname: neptune-mariadb-server
        image: mariadb
        environment:
            MARIADB_USER: neptune_user
            MARIADB_PASSWORD: Abc@123456
            MARIADB_DATABASE: NeptuneMariaDB
            MARIADB_ROOT_PASSWORD: MariaDB@123456
        ports:
            - 3307:3306
            - 33070:33060

networks:
    default:
        external:
            name: neptune-network
```

# Chạy docker-compose

Sau khi đã có file `docker-compose.yml` ở bước trên, bạn di chuyển vào thư mục chứa file này, mở `Command line` tool và chạy lệnh sau:

> **docker-compose up -d**