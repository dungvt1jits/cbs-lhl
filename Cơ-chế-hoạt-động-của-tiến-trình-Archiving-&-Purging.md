# Cơ chế hoạt động

Hệ thống sử dụng 2 Database để hoạt động:
- Online
- Archive

![image](uploads/10acd63738f5761ec878c2ef18b65cfc/image.png)

Các db này có thể khác loại hoàn toàn (Oracle/SQLServer/MySQL/PostgresQL)

- `Online` db: là db chứa cấu hình cũng như dữ liệu xử lý workflow.

- `Archiving` db: là db chứa dữ liệu backup từ db Online.

# Tiến trình Archiving & Purging

![image](uploads/7ad568ce3d776692319935d1a4617b70/image.png)

## Archiving

Khi Neptune Server hoạt động, toàn bộ dữ liệu về workflow được lưu trữ trong db `Online`. Theo thời gian những dữ liệu này sẽ dần tích lũy và làm cho kích thước file dữ liệu càng lớn. Nếu không có cơ chế giảm khối lượng dữ liệu này thì:
  - Hiệu suất hoạt động sẽ giảm
  - Server không đủ ổ cứng để lưu trữ

Hệ thống có tiến trình chuyển dữ liệu xử lý workflow từ db Online sang db Archving. Dữ liệu trong db Archiving này được dùng cho mục đích backup mà thôi. 

## Purging

Sau một thời gian backup, đến lượt db Archive này cũng sẽ tăng trưởng về khối lượng dữ liệu và cũng cần phải xử lý để giảm bớt khối lượng này. Tiến trình bỏ bớt dữ liệu trong db Archive này gọi là tiến trình `Purging`.

Tiến trình Purging thực chất là tiến trình xóa bỏ dữ liệu xử lý workflow theo thời gian lịch sử nào đó. Được cấu hình trong tham số của NeptuneServer.

# Các tham số liên quan

## Tiến trình Archiving

| Tham số | Ví dụ |
|-----------------------------------------|----------------------|
| DB_ARCHIVE_BACKWARDS_IN_SECONDS         | 604800               |
| DB_ARCHIVE_COMMAND_TIMEOUT_SECONDS      | 600                  |
| DB_ARCHIVE_INTERVAL_IN_SECONDS          | 86400                |
| DB_ARCHIVE_MAX_WORKFLOWS_PER_PROCESSING | 100                  |
| DB_ARCHIVE_NAME                         | neptune_archive      |
| DB_ARCHIVE_PASSWORD                     | bmVwdHVuZV9hcmNoaXZl |
| DB_ARCHIVE_RDBMS_TYPE                   | MySQL                |
| DB_ARCHIVE_SCHEMA                       | neptune_archive      |
| DB_ARCHIVE_SERVER_NAME                  | neptunedb_mysql      |
| DB_ARCHIVE_SERVER_PORT                  | 3306                 |
| DB_ARCHIVE_START_UTC_TIME_IN_HHMMSS     | 010000               |
| DB_ARCHIVE_USERNAME                     | neptune_archive      |


## Purging
| Tham số | Ví dụ |
|---------------------------------------|----------|
| DB_PURGE_APILOG_BACKWARDS_IN_SECONDS  | 3600     |
| DB_PURGE_APILOG_INTERVAL_IN_SECONDS   | 60       |
| DB_PURGE_BACKWARDS_IN_SECONDS         | 15552000 |
| DB_PURGE_COMMAND_TIMEOUT_SECONDS      | 60       |
| DB_PURGE_GRPCLOG_BACKWARDS_IN_SECONDS | 3600     |
| DB_PURGE_GRPCLOG_INTERVAL_IN_SECONDS  | 60       |
| DB_PURGE_INSTANCE_INTERVAL_IN_SECONDS | 60       |
| DB_PURGE_INTERVAL_IN_SECONDS          | 86400    |
| DB_PURGE_JWT_INTERVAL_IN_SECONDS      | 600      |
| DB_PURGE_MAX_WORKFLOWS_PER_PROCESSING | 1000     |
| DB_PURGE_START_UTC_TIME_IN_HHMMSS     | 020000   |



