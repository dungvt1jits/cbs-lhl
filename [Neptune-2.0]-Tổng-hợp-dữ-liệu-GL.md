# Các mode tạo GLEntries

Có 3 mode cập nhật GL:
- Mode `Synchronization`: Sau khi thực hiện xong xử lý tại các service chức năng, các GLEntries tại service sẽ được đưa vào trong message, đặt trong `context` của message. Đến lượt phân hệ ACT sẽ đọc các entries này và ghi vào Statement của `ACT`. Sau đó cập nhật số dư

- Mode `Asynchronization`: Xử lý giống mode `Synchronization` nhưng cấu hình thuộc tính Step GL xử lý bất đồng bộ bằng cách thiết lập thuộc tính `StepDefinition.StepMode` = 'Oneway`

![image](uploads/70386253ae815db56bc35f024bbe87fb/image.png)

- Mode `EOD`: Mode này, dữ liệu GL không được sinh ra tại các service mà để đến cuối ngày, trong quá trình chạy batch sẽ tạo lại GLEntries từ bảng `xxxTrans` và đồng bộ sang GL. Mode này sẽ làm giảm thời gian thực hiện giao dịch do chỉ cần tạo bảng `xxxTrans` là xong.

# Tổng hợp dữ liệu GL từ service chức năng sang service `ACT`

- Dữ liệu `GL entries` tại các service được tạo trong bảng `GLEntries` theo từng bút toán giao dịch (theo trường `TransactionNumber`). Các entries này chỉ là một phần của một bút toán hoàn chỉnh. 

- Chẳng hạn trong giao dịch trả gốc cho tài khoản vay: chân Nợ có trong service CRD, và chân Có thì lại tồn tại trong service DPT. Khi đồng bộ sang phân hệ GL mới tạo thành giá trị hoàn chỉnh.

- Mỗi transaction bao gồm nhiều entry Nợ/Có theo quan hệ n-n, bao gồm cả entry IBT, FX tồn tại ở nhiều service khác nhau, liên kết qua số `TransactionNumber`

- Khi chuyển sang phân hệ ACT sẽ được nhóm theo các thông tin sau (`group by`):
  - Tài khoản GL
  - Tính chất Nợ/Có
  - ValueDate
  - Số tiền: tổng amount của các entries trong nhóm

- Số giao dịch đồng bộ sang GL