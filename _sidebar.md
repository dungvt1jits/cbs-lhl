<details><summary>Weekly report</summary>
- [MOM_10082021](Weekly report/MOM_10082021)<br>
- [MOM_10152021](Weekly report/MOM_10152021)<br>
- [MOM_10222021](Weekly report/MOM_10222021)<br>
- [MOM_10292021](Weekly report/MOM_10292021)<br>
- [MOM_11052021](Weekly report/MOM_11052021)<br>
- [MOM_11122021](Weekly-report/MOM_11122021)<br>
- [MOM_11192021](Weekly-report/MOM_11192021)<br>
- [MOM_11262021](Weekly-report/MOM_11262021)<br>
- [MOM_12032021](Weekly-report/MOM_12032021)<br>
- [MOM_12102021](Weekly-report/MOM_12102021)<br>
- [MOM_12172021](Weekly-report/MOM_12172021)<br>
- [MOM_12242021](Weekly-report/MOM_12242021)<br>
- [MOM_12312021](Weekly-report/MOM_12312021)<br>
- [MOM_01072022](Weekly-report/MOM_01072022)<br>
- [MOM_01142022](Weekly-report/MOM_01142022)<br>
- [MOM_01212022](Weekly-report/MOM_01212022)<br>
- [MOM_01282022](Weekly-report/MOM_01282022)<br>
- [MOM_02182022](Weekly-report/MOM_02182022)<br>
- [MOM_02252022](Weekly-report/MOM_02252022)<br>
- [MOM_03042022](Weekly-report/MOM_03042022)<br>
- [MOM_03112022](Weekly-report/MOM_03112022)<br>
- [MOM_03182022](Weekly-report/MOM_03182022)<br>
- [MOM_03252022](Weekly-report/MOM_03252022)<br>
- [MOM_04012022](Weekly-report/MOM_04012022)<br>
- [MOM_04082022](Weekly-report/MOM_04082022)<br>
- [MOM_04152022](Weekly-report/MOM_04152022)<br>
- [MOM_04222022](Weekly-report/MOM_04222022)<br>
- [MOM_04292022](Weekly-report/MOM_04292022)<br>
- [MOM_05062022](Weekly-report/MOM_05062022)<br>
- [MOM_05132022](Weekly-report/MOM_05132022)<br>
- [MOM_05202022](Weekly-report/MOM_05202022)<br>
- [MOM_05272022](Weekly-report/MOM_05272022)<br>
- [MOM_06032022](Weekly-report/MOM_06032022)<br>
- [MOM_06102022](Weekly-report/MOM_06102022)<br>
- [MOM_06172022](Weekly-report/MOM_06172022)<br>
- [MOM_06242022](Weekly-report/MOM_06242022)<br>
- [MOM_07012022](Weekly-report/MOM_07012022)<br>
- [MOM_07082022](Weekly-report/MOM_07082022)<br>
- [MOM_07152022](Weekly-report/MOM_07152022)<br>
- [MOM_07222022](Weekly-report/MOM_07222022)<br>
- [MOM_07292022](Weekly-report/MOM_07292022)<br>
- [MOM_08052022](Weekly-report/MOM_08052022)<br>
- [MOM_08122022](Weekly-report/MOM_08122022)<br>
- [MOM_08192022](Weekly-report/MOM_08192022)<br>
- [MOM_08262022](Weekly-report/MOM_08262022)<br>
- [MOM_08312022](Weekly-report/MOM_08312022)<br>
- [MOM_09092022](Weekly-report/MOM_09092022)<br>
- [MOM_09172022](Weekly-report/MOM_09172022)<br>
- [MOM_09232022](Weekly-report/MOM_09232022)<br>
- [MOM_09302022](Weekly-report/MOM_09302022)<br>
- [MOM_10072022](Weekly-report/MOM_10072022)<br>
- [MOM_10142022](Weekly-report/MOM_10142022)<br>
- [MOM_10212022](Weekly-report/MOM_10212022)<br>
- [MOM_10282022](Weekly-report/MOM_10282022)<br>
- [MOM_11042022](Weekly-report/MOM_11042022)<br>
- [MOM_11112022](Weekly-report/MOM_11112022)<br>
- [MOM_11182022](Weekly-report/MOM_11182022)<br>
- [MOM_11252022](Weekly-report/MOM_11252022)<br>
- [MOM_12022022](Weekly-report/MOM_12022022)<br>
</details>

<details><summary>Daily report</summary>
- [MOM_10262021_chieu](Daily meeting/MOM_10262021_chieu)<br>
- [MOM_10292021](Daily meeting/MOM_10292021)<br>
- [MOM_03112021_sang](Daily meeting/MOM_03112021_sang)<br>
- [MOM_11182021](Daily meeting/MOM_11182021)<br>
- [MOM_01132022](Daily meeting/MOM_01132022)<br>
- [MOM_04122022](Daily meeting/MOM_04122022)<br>
- [MOM_06232022](Daily meeting/MOM_06232022)<br>
- [MOM_11292022](Daily meeting/MOM_11292022)<br>
</details>

<details><summary>Development guides</summary>
- [Database Models](Development guides/Database Models)<br>
- [Validations](Development guides/Validations)<br>
- [Migration info](Development guides/Migration Info)<br>
- [Docker info](Development guides/Dockernize)<br>
- [Grpc info](Development guides/Grpc)<br>
- [Api Spec](Development guides/API_Spec)<br>
</details>


[Neptune documents](Neptune documents)

[Neptune 2.0]([Neptune 2.0] Tài liệu)

[Business Requirement Specification](Business Requirement Specification)

[ShweBank Digital Transformation Project](ShweBank Digital Transformation Project)

<details><summary>Customer Module</summary>
- [01. Customer Profile](Specification/Customer/01 Customer Profile)<br>
- [02. Customer Group](Specification/Customer/02 Customer Group)<br>
- [03. Customer Linkage](Specification/Customer/03 Customer Linkage)<br>
- [04. Customer Media Files](Specification/Customer/04 Customer Media Files)<br>
- [05. Approve Customer Modification](Specification/Customer/05 Approve Customer Modification)<br>
<details><summary>Transaction list</summary>
- [06. CTM_APR: Approve Customer](Specification/Customer/06 CTM_APR Approve Customer)<br>
- [07. CTM_CAC: Merge Customer](Specification/Customer/07 CTM_CAC Merge Customer)<br>
- [08. CTM_CAS: Change Customer Status](Specification/Customer/08 CTM_CAS Change Customer Status)<br>
- [09. CTM_SEC: Adjust Customer Sector, Category](Specification/Customer/09 CTM_SEC Adjust Customer Sector, Category)<br>
</details>
- [10. Settings](Specification/Customer/10 Settings)<br>
</details>

<details><summary>IFC Module</summary>
- [01. IFC Item Definition](Specification/IFC/01 IFC Item Definition)<br>
- [02. Tariff Configuration](Specification/IFC/02 Tariff Configuration)<br>
- [03. IFC Auto Fee](Specification/IFC/03 IFC Auto Fee)<br>
- [04. Settings](Specification/IFC/04 Settings)<br>
</details>

<details><summary>Deposit Module</summary>
- [01. Catalogue Definition](Specification/Deposit/01 Catalogue Definition)<br>
- [02. Account Information](Specification/Deposit/02 Account Information)<br>
- [03. Approve Account Modification](Specification/Deposit/03 Approve Account Modification)<br>
<details><summary>Deposit transaction list</summary>
- [04. DPT_OPN: Open New Deposit Account](Specification/Deposit/04 DPT_OPN Open New Deposit Account)<br>
- [05. DPT_OPC: Open And Deposit LFA By Cash](Specification/Deposit/05 DPT_OPC Open And Deposit LFA By Cash)<br>
- [06. DPT_OPT: Open And Deposit LFA By Transfer](Specification/Deposit/06 DPT_OPT Open And Deposit LFA By Transfer)<br>
- [07. DPT_OPM: Open And Deposit LFA By Miscellaneous](Specification/Deposit/07 DPT_OPM Open And Deposit LFA By Miscellaneous)<br>
- [08. DPT_APR: Approve Deposit Account](Specification/Deposit/08 DPT_APR Approve Deposit Account)<br>
- [09. DPT_AOPC: Approve Open And Deposit LFA By Cash](Specification/Deposit/09 DPT_AOPC Approve Open And Deposit LFA By Cash)<br>
- [10. DPT_AOPT: Approve Open And Deposit LFA By Transfer](Specification/Deposit/10 DPT_AOPT Approve Open And Deposit LFA By Transfer)<br>
- [11. DPT_AOPM: Approve Open And Deposit LFA By Miscellaneous](Specification/Deposit/11 DPT_AOPM Approve Open And Deposit LFA By Miscellaneous)<br>
- [12. DPT_CDP: Cash Deposit](Specification/Deposit/12 DPT_CDP Cash Deposit)<br>
- [13. DPT_MDP: Miscellaneous Deposit](Specification/Deposit/13 DPT_MDP Miscellaneous Deposit)<br>
- [14. DPT_TRF: Transfer From Deposit Account To Deposit Account](Specification/Deposit/14 DPT_TRF Transfer From Deposit Account To Deposit Account)<br>
- [15. DPT_TRT: Transfer From Account Of Time Deposit](Specification/Deposit/15 DPT_TRT Transfer From Account Of Time Deposit)<br>
- [16. DPT_CWR: Cash Withdrawal](Specification/Deposit/16 DPT_CWR Cash Withdrawal)<br>
- [17. DPT_MWR: Miscellaneous Withdrawal](Specification/Deposit/17 DPT_MWR Miscellaneous Withdrawal)<br>
- [18. DPT_CWT: Cash Withdrawal Time Deposit](Specification/Deposit/18 DPT_CWT Cash Withdrawal Time Deposit)<br>
- [19. DPT_MWT: Miscellaneous Withdrawal Time Deposit](Specification/Deposit/19 DPT_MWT Miscellaneous Withdrawal Time Deposit)<br>
- [20. DPT_BLK: Block Account](Specification/Deposit/20 DPT_BLK Block Account)<br>
- [21. DPT_RLS: Release Block Account](Specification/Deposit/21 DPT_RLS Release Block Account)<br>
- [22. DPT_CAS: Change Account Status](Specification/Deposit/22 DPT_CAS Change Account Status)<br>
- [23. DPT_CIP: Interest Payment By Cash](Specification/Deposit/23 DPT_CIP Interest Payment By Cash)<br>
- [24. DPT_DIP: Interest Payment To Deposit Account](Specification/Deposit/24 DPT_DIP Interest Payment To Deposit Account)<br>
- [25. DPT_MIP: Miscellaneous Interest Payment](Specification/Deposit/25 DPT_MIP Miscellaneous Interest Payment)<br>
- [26. DPT_IFC: Adjust Deposit Interest](Specification/Deposit/26 DPT_IFC Adjust Deposit Interest)<br>
- [27. DPT_EMK: Hold Balance](Specification/Deposit/27 DPT_EMK Hold Balance)<br>
- [28. DPT_ERL: Release Hold Balance](Specification/Deposit/28 DPT_ERL Release Hold Balance)<br>
- [29. DPT_FOC: Fee Collection By Cash For DD](Specification/Deposit/29 DPT_FOC Fee Collection By Cash For DD)<br>
- [30. DPT_FEE: Fee Collection By Transfer](Specification/Deposit/30 DPT_FEE Fee Collection By Transfer)<br>
- [31. DPT_CLS: Close Deposit Account](Specification/Deposit/31 DPT_CLS Close Deposit Account)<br>
- [32. DPT_DLS: Close Deposit Account By Deposit](Specification/Deposit/32 DPT_DLS Close Deposit Account By Deposit)<br>
- [33. DPT_MLS: Close Deposit Account By Miscellaneous](Specification/Deposit/33 DPT_MLS Close Deposit Account By Miscellaneous)<br>
- [34. DPT_HIS: Transaction History Inquiry](Specification/Deposit/34 DPT_HIS Transaction History Inquiry)<br>
- [35. DPT_LAS: Update Last Transaction Date](Specification/Deposit/35 DPT_LAS Update Last Transaction Date)<br>
- [74. DPT_DMN: Deposit Money](Specification/Deposit/74 DPT_DMN Deposit Money)<br>
- [75. DPT_WDM: Withdrawal Money](Specification/Deposit/75 DPT_WDM Withdrawal Money)<br>
- [79. DPT_RDP: Re-Open Deposit Account](Specification/Deposit/79 DPT_RDP Re-Open Deposit Account)<br>
- [80. DPT_RADR: Re-Active Dormant Account](Specification/Deposit/80 DPT_RADR Re-Active Dormant Account)<br>
</details>
- [36. Stock Inventory](Specification/Deposit/36 Stock Inventory)<br>
- [37. Clearing Check](Specification/Deposit/37 Clearing Check)<br>
<details><summary>Stock transaction list</summary>
- [38. DPT_SRG: Stock Registration](Specification/Deposit/38 DPT_SRG Stock Registration)<br>
- [39. DPT_SAB: Stock Assigned To Branch](Specification/Deposit/39 DPT_SAB Stock Assigned To Branch)<br>
- [40. DPT_SAT: Stock Assigned To Teller](Specification/Deposit/40 DPT_SAT Stock Assigned To Teller)<br>
- [41. DPT_CCR: Stock Confirm Received](Specification/Deposit/41 DPT_CCR Stock Confirm Received)<br>
- [42. DPT_CRT: Stock Returned](Specification/Deposit/42 DPT_CRT Stock Returned)<br>
- [43. DPT_SRA: Reject Assigned Stock](Specification/Deposit/43 DPT_SRA Reject Assigned Stock)<br>
- [44. DPT_CIS: Cheque Book Issued](Specification/Deposit/44 DPT_CIS Cheque Book Issued)<br>
- [45. DPT_SBI: Deposit Saving Book Issue](Specification/Deposit/45 DPT_SBI Deposit Saving Book Issue)<br>
- [45. DPT_FBI: Fixed Deposit Book Issue](Specification/Deposit/45 DPT_FBI Fixed Deposit Book Issue)<br>
- [46. DPT_CER: Fixed Deposit Receipt Issued](Specification/Deposit/46 DPT_CER Fixed Deposit Receipt Issued)<br>
- [47. DPT_CDT: Deposit By Cheque](Specification/Deposit/47 DPT_CDT Deposit By Cheque)<br>
- [48. DPT_CWC: Cash Withdrawal By Cheque](Specification/Deposit/48 DPT_CWC Cash Withdrawal By Cheque)<br>
- [49. DPT_CWM: Miscellaneous Debit By Cheque](Specification/Deposit/49 DPT_CWM Miscellaneous Debit By Cheque)<br>
- [50. DPT_CCD: Outward Cheque Sent For Clearing](Specification/Deposit/50 DPT_CCD Outward Cheque Sent For Clearing)<br>
- [51. DPT_OCC: Outward Cheque Cleared](Specification/Deposit/51 DPT_OCC Outward Cheque Cleared)<br>
- [52. DPT_OCR: Outward Cheque Return](Specification/Deposit/52 DPT_OCR Outward Cheque Return)<br>
- [53. DPT_CEI: Issued Hold Balance For Cheque](Specification/Deposit/53 DPT_CEI Issued Hold Balance For Cheque)<br>
- [54. DPT_REC: Release Hold Balance For Cheque](Specification/Deposit/54 DPT_REC Release Hold Balance For Cheque)<br>
- [55. DPT_CSA: Change Status Of Cheque To Cancel](Specification/Deposit/55 DPT_CSA Change Status Of Cheque To Cancel)<br>
- [56. DPT_CSS: Change Status Of Cheque To Stop](Specification/Deposit/56 DPT_CSS Change Status Of Cheque To Stop)<br>
- [57. DPT_CSU: Change Status Of Cheque To Unpaid](Specification/Deposit/57 DPT_CSU Change Status Of Cheque To Unpaid)<br>
- [58. DPT_CSC: Change Status Of Passbook Or Receipt To Closed](Specification/Deposit/58 DPT_CSC Change Status Of Passbook Or Receipt To Closed)<br>
- [59. DPT_CSD: Change Status Of Passbook Or Receipt To Damage](Specification/Deposit/59 DPT_CSD Change Status Of Passbook Or Receipt To Damage)<br>
- [60. DPT_CSL: Change Status Of Passbook Or Receipt To Lost](Specification/Deposit/60 DPT_CSL Change Status Of Passbook Or Receipt To Lost)<br>
- [61. DPT_CSN: Change Status Of Passbook Or Receipt To Normal](Specification/Deposit/61 DPT_CSN Change Status Of Passbook Or Receipt To Normal)<br>
- [62. DPT_CSO: Change Status Of Passbook Or Receipt To Stop Payment](Specification/Deposit/62 DPT_CSO Change Status Of Passbook Or Receipt To Stop Payment)<br>
- [63. DPT_SLS: Cheque Leaves Status Inquiry](Specification/Deposit/63 DPT_SLS Cheque Leaves Status Inquiry)<br>
- [64. DPT_CIQ: Cheque Inquiry](Specification/Deposit/64 DPT_CIQ Cheque Inquiry)<br>
- [76. DPT_CCI: Cashier Cheque Issued](Specification/Deposit/76 DPT_CCI Cashier Cheque Issued)<br>
- [77. DPT_CCW: Cashier Cheque Withdrawal](Specification/Deposit/77 DPT_CCW Cashier Cheque Withdrawal)<br>
- [78. DPT_RCC: Cashier Cheque Return](Specification/Deposit/78 DPT_RCC Cashier Cheque Return)<br>
- [76. DPT_POI: Payment Order Issued](Specification/Deposit/76 DPT_POI Payment Order Issued)<br>
- [77. DPT_POW: Payment Order Withdrawal](Specification/Deposit/77 DPT_POW Payment Order Withdrawal)<br>
- [78. DPT_RPO: Payment Order Return](Specification/Deposit/78 DPT_RPO Payment Order Return)<br>
- [81. DPT_CTS: Change Status Of Stock](Specification/Deposit/81 DPT_CTS Change Status Of Stock)<br>
</details>

<details><summary>Batch transaction list</summary>
- [65. Change Status Dormant Calculation And Collection Dormant Fee](Specification/Deposit/65 Change Status Dormant Calculation And Collection Dormant Fee)<br>
- [66. Calculation And Collection Maintenance Fee](Specification/Deposit/66 Calculation And Collection Maintenance Fee)<br>
- [67. Change Status Maturity](Specification/Deposit/67 Change Status Maturity)<br>
- [68. Calculate Interest Accrual And Withholding Tax](Specification/Deposit/68 Calculate Interest Accrual And Withholding Tax)<br>
- [69. Transfer Interest Due](Specification/Deposit/69 Transfer Interest Due)<br>
- [70. Rollover](Specification/Deposit/70 Rollover)<br>
- [71. Transfer Interest To DD Account](Specification/Deposit/71 Transfer Interest To DD Account)<br>
- [72. Transfer Principal And Interest To DD Account](Specification/Deposit/72 Transfer Principal And Interest To DD Account)<br>
- [73. DPT_TIO: Transfer Interest To Overdue](Specification/Deposit/73 DPT_TIO Transfer Interest To Overdue)<br>
</details>
- [82. Settings](Specification/Deposit/82 Settings)<br>
</details>

<details><summary>Credit Module</summary>
- [01. Catalogue Definition](Specification/Credit/01 Catalogue Definition)<br>
- [02. Account Information](Specification/Credit/02 Account Information)<br>
- [03. Group Limit](Specification/Credit/03 Group Limit)<br>
- [04. Product Limit](Specification/Credit/04 Product Limit)<br>
- [05. Sub Product Limit](Specification/Credit/05 Sub Product Limit)<br>
- [06. Approve Account Modification](Specification/Credit/06 Approve Account Modification)<br>
<details><summary>Transaction list</summary>
- [07. CRD_PLO: Open Product Limit](Specification/Credit/07 CRD_PLO Open Product Limit)<br>
- [08. CRD_PLA: Approve Product Limit](Specification/Credit/08 CRD_PLA Approve Product Limit)<br>
- [09. CRD_PLAD: Adjust Product Limit](Specification/Credit/09 CRD_PLAD Adjust Product Limit)<br>
- [10. CRD_SPLO: Open Sub-Product Limit](Specification/Credit/10 CRD_SPLO Open Sub-Product Limit)<br>
- [11. CRD_SPLA: Approve Sub Product Limit](Specification/Credit/11 CRD_SPLA Approve Sub Product Limit)<br>
- [12. CRD_SPAD: Adjust Sub Product Limit](Specification/Credit/12 CRD_SPAD Adjust Sub Product Limit)<br>
- [13. CRD_OPN: Open New Credit Account](Specification/Credit/13 CRD_OPN Open New Credit Account)<br>
- [14. CRD_REJ: Reject Credit Account](Specification/Credit/14 CRD_REJ Reject Credit Account)<br>
- [15. CRD_APR: Approve Credit Account](Specification/Credit/15 CRD_APR Approve Credit Account)<br>
- [16. CRD_CDR: Cash Disbursement](Specification/Credit/16 CRD_CDR Cash Disbursement)<br>
- [17. CRD_MDR: Miscellaneous Disbursement](Specification/Credit/17 CRD_MDR Miscellaneous Disbursement)<br>
- [18. CRD_TDR: Disbursement By Transfer](Specification/Credit/18 CRD_TDR Disbursement By Transfer)<br>
- [19. CRD_BLK: Block Credit Account](Specification/Credit/19 CRD_BLK Block Credit Account)<br>
- [20. CRD_CAS: Change Account Status](Specification/Credit/20 CRD_CAS Change Account Status)<br>
- [21. CRD_MIPM: Miscellaneous Interest And Principal Collection](Specification/Credit/21 CRD_MIPM Miscellaneous Interest And Principal Collection)<br>
- [22. CRD_CIPM: Principal And Interest Collection By Cash](Specification/Credit/22 CRD_CIPM Principal And Interest Collection By Cash)<br>
- [23. CRD_IPCT: Principal And Interest Collection By Transfer](Specification/Credit/23 CRD_IPCT Principal And Interest Collection By Transfer)<br>
- [24. CRD_FOC: Fee Collection By Cash](Specification/Credit/24 CRD_FOC Fee Collection By Cash)<br>
- [25. CRD_FCD: Fee Collection By Deposit](Specification/Credit/25 CRD_FCD Fee Collection By Deposit)<br>
- [26. CRD_FCG: Miscellaneous Fee Collection](Specification/Credit/26 CRD_FCG Miscellaneous Fee Collection)<br>
- [27. CRD_RFC: Fee Refund By Cash](Specification/Credit/27 CRD_RFC Fee Refund By Cash)<br>
- [28. CRD_RFD: Fee Refund By Transfer](Specification/Credit/28 CRD_RFD Fee Refund By Transfer)<br>
- [29. CRD_FRG: Miscellaneous Fee Refund](Specification/Credit/29 CRD_FRG Miscellaneous Fee Refund)<br>
- [30. CRD_CLA: Credit Limit Adjustment](Specification/Credit/30 CRD_CLA Credit Limit Adjustment)<br>
- [31. CRD_EXT: Extend Credit Account](Specification/Credit/31 CRD_EXT Extend Credit Account)<br>
- [32. CRD_IFC: Adjust Credit Interest](Specification/Credit/32 CRD_IFC Adjust Credit Interest)<br>
- [33. CRD_RSCH: Re-Generate Credit Schedule For Equally](Specification/Credit/33 CRD_RSCH Re-Generate Credit Schedule For Equally)<br>
- [34. CRD_GSCH: Grace Principal Period For Compound](Specification/Credit/34 CRD_GSCH Grace Principal Period For Compound)<br>
- [35. CRD_IHIS: Interest Of Compound Credit Inquiry](Specification/Credit/35 CRD_IHIS Interest Of Compound Credit Inquiry)<br>
- [36. CRD_NPL: NPL Processing](Specification/Credit/36 CRD_NPL NPL Processing)<br>
- [37. CRD_WOF: Change Credit To Write-Off](Specification/Credit/37 CRD_WOF Change Credit To Write-Off)<br>
- [38. CRD_PIW: Interest And Principal Collection For Write-Off](Specification/Credit/38 CRD_PIW Interest And Principal Collection For Write-Off)<br>
- [39. CRD_SCR: Set Customer Ranking](Specification/Credit/39 CRD_SCR Set Customer Ranking)<br>
- [40. CRD_CLS: Close Account](Specification/Credit/40 CRD_CLS Close Account)<br>
- [41. CRD_SPLC: Close Sub Product Limit](Specification/Credit/41 CRD_SPLC Close Sub Product Limit)<br>
- [42. CRD_PLC: Close Product Limit](Specification/Credit/42 CRD_PLC Close Product Limit)<br>
- [43. CRD_HIS: History Inquiry](Specification/Credit/43 CRD_HIS History Inquiry)<br>
- [44. CRD_AIPV: Adjust Interest Provision](Specification/Credit/44 CRD_AIPV Adjust Interest Provision)<br>
</details>
<details><summary>Batch transaction list</summary>
- [45. B_CRD_CIC: Interest Calculation](Specification/Credit/45 B_CRD_CIC Interest Calculation)<br>
- [46. B_CRD_ANI: Accrued Interest](Specification/Credit/46 B_CRD_ANI Accrued Interest)<br>
- [47. B_CRD_CTD: Transfer Interest And Principal To Due](Specification/Credit/47 B_CRD_CTD Transfer Interest And Principal To Due)<br>
- [48. B_CRD_RID: Collect Interest From Deposit](Specification/Credit/48 B_CRD_RID Collect Interest From Deposit)<br>
- [49. B_CRD_RPD: Collect Principal From Deposit](Specification/Credit/49 B_CRD_RPD Collect Principal From Deposit)<br>
- [50. B_CRD_NPL: Change Classification Status](Specification/Credit/50 B_CRD_NPL Change Classification Status)<br>
- [51. B_CRD_API: Allocate Prepaid Interest](Specification/Credit/51 B_CRD_API Allocate Prepaid Interest)<br>
- [52. B_CRD_AIPB: Adjust Interest Provision Batch](Specification/Credit/52 B_CRD_AIPB Adjust Interest Provision Batch)<br>
- [53. B_CRD_TID: Transfer Interest To Due](Specification/Credit/53 B_CRD_TID Transfer Interest To Due)<br>
</details>
- [54. Settings](Specification/Credit/54 Settings)<br>
</details>

<details><summary>Overdraft Module</summary>
<details><summary>Overdraft catalogue definition</summary>
- [Deposit Catalogue Definition](Specification/Deposit/01 Catalogue Definition)<br>
- [Credit Catalogue Definition](Specification/Credit/01 Catalogue Definition)<br>
</details>
<details><summary>Overdraft account information</summary>
- [Deposit Account Information](Specification/Deposit/02 Account Information)<br>
- [Credit Account Information](Specification/Credit/02 Account Information)<br>
</details>
<details><summary>Overdraft transaction list</summary>
- [01. DPT_ODO: Create Overdraft Contract](Specification/Overdraft/01 DPT_ODO Create Overdraft Contract)<br>
- [02. DPT_COD: Close OD Facility By Cash](Specification/Overdraft/02 DPT_COD Close OD Facility By Cash)<br>
- [03. DPT_COG: Close OD Facility By GL](Specification/Overdraft/03 DPT_COG Close OD Facility By GL)<br>
- [04. B_CRD_DOD: Disbursement To OD Account](Specification/Overdraft/04 B_CRD_DOD Disbursement To OD Account)<br>
- [05. B_CRD_POD: Principal Collection For OD Account](Specification/Overdraft/05 B_CRD_POD Principal Collection For OD Account)<br>
- [06. B_CRD_CIC: Interest Calculation](Specification/Overdraft/06 B_CRD_CIC Interest Calculation)<br>
- [07. B_CRD_ANI: Accrued Interest](Specification/Overdraft/07 B_CRD_ANI Accrued Interest)<br>
- [08. B_CRD_TOD: Transfer Interest To Due](Specification/Overdraft/08 B_CRD_TOD Transfer Interest To Due)<br>
- [09. B_CRD_IOD: OD Interest Collection Month End](Specification/Overdraft/09 B_CRD_IOD OD Interest Collection Month End)<br>
- [10. B_CRD_DODM: Disbursement For OD Account Month End](Specification/Overdraft/10 B_CRD_DODM Disbursement For OD Account Month End)<br>
</details>
</details>

<details><summary>Collateral Module</summary>
- [01. Catalogue Definition](Specification/Collateral/01 Catalogue Definition)<br>
- [02. Account Information](Specification/Collateral/02 Account Information)<br>
<details><summary>Transaction list</summary>
- [03. MTG_OPN: Open New Collateral Account](Specification/Collateral/03 MTG_OPN Open New Collateral Account)<br>
- [04. MTG_APR: Approve Collateral Account](Specification/Collateral/04 MTG_APR Approve Collateral Account)<br>
- [05. MTG_SCR: Secure Asset For Credit Account](Specification/Collateral/05 MTG_SCR Secure Asset For Credit Account)<br>
- [06. MTG_RLS: Release Asset From Credit Account](Specification/Collateral/06 MTG_RLS Release Asset From Credit Account)<br>
- [07. MTG_BLK: Block Account](Specification/Collateral/07 MTG_BLK Block Account)<br>
- [08. MTG_BRL: Release Block Account](Specification/Collateral/08 MTG_BRL Release Block Account)<br>
- [09. MTG_DCR: Decreasing Asset Value](Specification/Collateral/09 MTG_DCR Decreasing Asset Value)<br>
- [10. MTG_INR: Increasing Asset Value](Specification/Collateral/10 MTG_INR Increasing Asset Value)<br>
- [11. MTG_RTN: Return Collateral Asset To Customer](Specification/Collateral/11 MTG_RTN Return Collateral Asset To Customer)<br>
- [12. MTG_CLS: Close Account](Specification/Collateral/12 MTG_CLS Close Account)<br>
- [13. MTG_REA: Re-Active Collateral Account](Specification/Collateral/13 MTG_REA Re-Active Collateral Account)<br>
- [14. MTG_HDT: History Inquiry Detail](Specification/Collateral/14 MTG_HDT History Inquiry Detail)<br>
- [15. MTG_SPL: Secure Asset For Product Limit](Specification/Collateral/15 MTG_SPL Secure Asset For Product Limit)<br>
- [16. MTG_RPL: Release Asset From Product Limit](Specification/Collateral/16 MTG_RPL Release Asset From Product Limit)<br>
</details>
- [17. Settings](Specification/Collateral/17 Settings)<br>
</details>

<details><summary>Payment Module</summary>
- [01. Catalogue Definition](Specification/Payment/01 Catalogue Definition)<br>
- [02. Payment Queue](Specification/Payment/02 Payment Queue)<br>
- [03. Account Linkage](Specification/Payment/03 Account Linkage)<br>
- [04. Fund Transfer Via Fast](Specification/Payment/04 Fund Transfer Via Fast)<br>
- [05. Payment Queue At Center](Specification/Payment/05 Payment Queue At Center)<br>
- [06. Correspondent Bank](Specification/Payment/06 Correspondent Bank)<br>
<details><summary>Transaction list</summary>
- [07. PMT_OIT: Outward Internal Bank](Specification/Payment/07 PMT_OIT Outward Internal Bank)<br>
- [08. PMT_IIT: Inward Internal Bank](Specification/Payment/08 PMT_IIT Inward Internal Bank)<br>
- [09. PMT_ODT: Outward Domestic Bank](Specification/Payment/09 PMT_ODT Outward Domestic Bank)<br>
- [10. PMT_OITT: Outward International Bank](Specification/Payment/10 PMT_OITT Outward International Bank)<br>
- [11. PMT_ODTF: Outward Fund Transfer Via Fast](Specification/Payment/11 PMT_ODTF Outward Fund Transfer Via Fast)<br>
- [12. PMT_KITT: Create Inward Domestic Or Swift Message](Specification/Payment/12 PMT_KITT Create Inward Domestic Or Swift Message)<br>
- [13. PMT_IDT: Inward Domestic Bank](Specification/Payment/13 PMT_IDT Inward Domestic Bank)<br>
- [14. PMT_IITT: Inward Payment Bank](Specification/Payment/14 PMT_IITT Inward International Bank)<br>
</details>
- [15. Settings](Specification/Payment/15 Settings)<br>
</details>

<details><summary>Cash Module</summary>
- [01. Cash Flow](Specification/Cash/01 Cash Flow)<br>
- [02. Cash Denomination](Specification/Cash/02 Cash Denomination)<br>
- [03. CSH_MOV: Internal Cash Movement](Specification/Cash/03 CSH_MOV Internal Cash Movement)<br>
- [04. CSH_DNM: Cash Denomination](Specification/Cash/04 CSH_DNM Cash Denomination)<br>
- [05. Transfer Cash On End Of Date](Specification/Cash/05 Transfer Cash On End Of Date)<br>
- [06. Limit Cash Position](Specification/Cash/06 Limit Cash Position)<br>
- [07. Settings](Specification/Cash/07 Settings)<br>
</details>

<details><summary>Fixed Asset Module</summary>
- [01. Catalogue Definition](Specification/Fixed Asset/01 Catalogue Definition)<br>
- [02. Account Information](Specification/Fixed Asset/02 Account Information)<br>
<details><summary>Transaction list</summary>
- [03. FAC_OPN: Open New Fixed Asset](Specification/Fixed Asset/03 FAC_OPN Open New Fixed Asset)<br>
- [04. FAC_PBC: Purchase Fixed Asset By Cash](Specification/Fixed Asset/04 FAC_PBC Purchase Fixed Asset By Cash)<br>
- [05. FAC_PBD: Purchase Fixed Asset By Deposit](Specification/Fixed Asset/05 FAC_PBD Purchase Fixed Asset By Deposit)<br>
- [06. FAC_PBG: Purchase Fixed Asset By Accounting](Specification/Fixed Asset/06 FAC_PBG Purchase Fixed Asset By Accounting)<br>
- [07. FAC_SBC: Selling Fixed Asset By Cash](Specification/Fixed Asset/07 FAC_SBC Selling Fixed Asset By Cash)<br>
- [08. FAC_SBD: Selling Fixed Asset By Deposit](Specification/Fixed Asset/08 FAC_SBD Selling Fixed Asset By Deposit)<br>
- [09. FAC_SBG: Selling Fixed Asset By Accounting](Specification/Fixed Asset/09 FAC_SBG Selling Fixed Asset By Accounting)<br>
- [10. FAC_TFB: Transfer Fixed Asset To Another Branch](Specification/Fixed Asset/10 FAC_TFB Transfer Fixed Asset To Another Branch)<br>
- [11. FAC_DEP: Fixed Asset Accumulate](Specification/Fixed Asset/11 FAC_DEP Fixed Asset Accumulate)<br>
- [12. FAC_CLS: Close Fixed Asset Account](Specification/Fixed Asset/12 FAC_CLS Close Fixed Asset Account)<br>
</details>
- [13. Settings](Specification/Fixed Asset/13 Settings)<br>
</details>

<details><summary>Accounting Module</summary>
- [01. Bank Account Definition](Specification/Accounting/01 Bank Account Definition)<br>
- [02. Account Map Table](Specification/Accounting/02 Account Map Table)<br>
- [03. Group Definition](Specification/Accounting/03 Group Definition)<br>
- [04. Module Account List](Specification/Accounting/04 Module Account List)<br>
- [05. Account Of Group Definition](Specification/Accounting/05 Account Of Group Definition)<br>
- [06. Extension Account Of Group Definition](Specification/Accounting/06 Extension Account Of Group Definition)<br>
- [07. Common Account Definition](Specification/Accounting/07 Common Account Definition)<br>
- [08. Clearing Account Definition](Specification/Accounting/08 Clearing Account Definition)<br>
- [09. Foreign Exchange Account Definition](Specification/Accounting/09 Foreign Exchange Account Definition)<br>
<details><summary>Transaction list</summary>
- [10. ACT_MAN: Internal Transaction](Specification/Accounting/10 ACT_MAN Internal Transaction)<br>
- [11. ACT_FEE: Fee collection by cash](Specification/Accounting/11 ACT_FEE Fee collection by cash)<br>
- [12. ACT_GFEE: Miscellaneous Fee collection](Specification/Accounting/12 ACT_GFEE Miscellaneous Fee collection)<br>
</details>
- [13. Settings](Specification/Accounting/13 Settings)<br>
</details>

<details><summary>Foreign Exchange Module</summary>
- [01. Foreign Exchange Rate](Specification/Foreign Exchange/01 Foreign Exchange Rate)<br>
<details><summary>Transaction list</summary>
- [02. CSH_CSH: FX Cash-Cash](Specification/Foreign Exchange/02 CSH_CSH FX Cash-Cash)<br>
- [03. CSH_DPT: FX Cash-Deposit](Specification/Foreign Exchange/03 CSH_DPT FX Cash-Deposit)<br>
- [04. CSH_ACT: FX Cash-GL](Specification/Foreign Exchange/04 CSH_ACT FX Cash-GL)<br>
- [05. DPT_CSH: FX Deposit-Cash](Specification/Foreign Exchange/05 DPT_CSH FX Deposit-Cash)<br>
- [06. DPT_DPT: FX Deposit-Deposit](Specification/Foreign Exchange/06 DPT_DPT FX Deposit-Deposit)<br>
- [07. DPT_ACT: FX Deposit-GL](Specification/Foreign Exchange/07 DPT_ACT FX Deposit-GL)<br>
- [08. ACT_CSH: FX GL-Cash](Specification/Foreign Exchange/08 ACT_CSH FX GL-Cash)<br>
- [09. ACT_DPT: FX GL-Deposit](Specification/Foreign Exchange/09 ACT_DPT FX GL-Deposit)<br>
- [10. ACT_ACT: FX GL-GL](Specification/Foreign Exchange/10 ACT_ACT FX GL-GL)<br>
</details>
- [11. Settings](Specification/Foreign Exchange/11 Settings)<br>
</details>

<details><summary>Administration Module</summary>
- [00. Authenticate](Specification/Administration/00 Authenticate)<br>
- [01. Import Data Files](Specification/Administration/01 Import Data Files)<br>
- [02. Process Imported Files](Specification/Administration/02 Process Imported Files)<br>
- [03. Country](Specification/Administration/03 Country)<br>
- [04. Currency](Specification/Administration/04 Currency)<br>
- [05. Branch Profile](Specification/Administration/05 Branch Profile)<br>
- [06. Department Profile](Specification/Administration/06 Department Profile)<br>
- [07. User Profile](Specification/Administration/07 User Profile)<br>
- [08. System Policy](Specification/Administration/08 System Policy)<br>
- [09. Bank Open-Bank Close](Specification/Administration/09 Bank Open-Bank Close)<br>
- [10. Roles Profile](Specification/Administration/10 Roles Profile)<br>
- [11. Invoke Approve Limit](Specification/Administration/11 Invoke Approve Limit)<br>
- [12. Company](Specification/Administration/12 Company)<br>
- [13. Calendar](Specification/Administration/13 Calendar)<br>
- [14. Branch Open-Branch Close](Specification/Administration/14 Branch Open-Branch Close)<br>
- [15. Cash Limit Position](Specification/Administration/15 Cash Limit Position)<br>
- [16. Code Definition List](Specification/Administration/16 Code Definition List)<br>
- [17. ADM_FXD: Delete FX Rate By Branch](Specification/Administration/17 ADM_FXD Delete FX Rate By Branch)<br>
- [18. Settings](Specification/Administration/18 Settings)<br>
</details>

<details><summary>Job Module</summary>
- [01. Job Process](Specification/Job/01 Job Process)<br>
</details>

<details><summary>Common</summary>
- [01. Rulefunc](Specification/Common/01 Rulefunc)<br>
- [02. Signature](Specification/Common/02 Signature)<br>
- [03. Voucher](Specification/Common/03 Voucher)<br>
- [04. Transaction Journal](Specification/Common/04 Transaction Journal)<br>
- [05. Transaction Flow Credit](Specification/Common/05 Transaction Flow Credit)<br>
- [06. Transaction Flow Deposit](Specification/Common/06 Transaction Flow Deposit)<br>
- [07. Transaction Flow Cash](Specification/Common/07 Transaction Flow Cash)<br>
- [08. Transaction Flow GL](Specification/Common/08 Transaction Flow GL)<br>
- [09. Transaction Flow Collateral](Specification/Common/09 Transaction Flow Collateral)<br>
- [10. Transaction Flow Fixed Asset](Specification/Common/10 Transaction Flow Fixed Asset)<br>
- [11. Accounting Rulefuncs](Specification/Common/11 Accounting Rulefuncs)<br>
- [12. Customer Rulefuncs](Specification/Common/12 Customer Rulefuncs)<br>
- [13. Credit Rulefuncs](Specification/Common/13 Credit Rulefuncs)<br>
- [14. Mortgage Rulefuncs](Specification/Common/14 Mortgage Rulefuncs)<br>
- [15. Deposit Rulefuncs](Specification/Common/15 Deposit Rulefuncs)<br>
- [16. Payment Rulefuncs](Specification/Common/16 Payment Rulefuncs)<br>
- [17. Admin Rulefuncs](Specification/Common/17 Admin Rulefuncs)<br>
- [18. Cash Rulefuncs](Specification/Common/18 Cash Rulefuncs)<br>
- [19. Fixed Asset Rulefuncs](Specification/Common/19 Fixed Asset Rulefuncs)<br>
- [20. FX Rulefuncs](Specification/Common/20 FX Rulefuncs)<br>
- [21. IFC Rulefuncs](Specification/Common/21 IFC Rulefuncs)<br>

</details>

---

- [Navigation](_sidebar)