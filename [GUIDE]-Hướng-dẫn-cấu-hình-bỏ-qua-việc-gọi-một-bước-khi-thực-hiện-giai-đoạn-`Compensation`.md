## Giới thiệu

`Compensation` là giai đoạn quay lui một workflow khi một bước nào đó trong workflow bị thất bại.
Chẳng hạn, workflow thực hiện n bước, từ bước 1 đến bước n. Tại bước thứ i (1 <= i <= n), nếu trạng thái xử lý của service trả về là `status` = `ERROR` thì tiến trình `Compensation` sẽ được kích hoạt.

## Tiến trình

Workflow sẽ tuần tự gửi message đến các service với nội dung message như nội dung mà service đã trả về trước đó.

Giả sử bước thứ i bị lỗi, workflow sẽ gửi từ bước `1` đến bước `i-1`.

Trong nội dung message gửi cho service, cờ `request.request_header.is_compensated` sẽ có giá trị `Y`. Các service có thể xét điều kiện này hoặc gọi hàm `request.request_header.IsCompensated()` để biết trạng thái message là trong tiến trình `Compensation`.

Có 2 trường hợp xảy ra:

1. Nếu service thực hiện message `Compensation` thành công thì trạng thái trả về sẽ là `response.status` = `SUCCESS`, khi đó Neptune Server sẽ ghi nhận việc quay lui của service thành công.
2. Ngược lại, nếu service xử lý thất bại thì trạng thái trả về sẽ là `response.status` = `ERROR`. Khi đó Neptune server sẽ ghi nhận việc quay lui thất bại. Bước thực hiện sẽ ghi nhận trạng thái `WF_STEP_EXECUTION.IS_DISPUTED` = `Y`. Việc này sẽ kéo theo cả workflow sẽ được ghi nhận `WF_EXEC.IS_DISPUTED` là `Y`.

## Xử lý các workflow có trạng thái `DISPUTED`

Trạng thái `DISPUTED` được hiểu là workflow có trạng thái tranh chấp nội bộ giữa các bước bên trong. Trạng thái này phản ánh các bước của workflow gây ra tình trạng không nhất quán dữ liệu giữa các service sau khi thực hiện workflow lần đầu tiên. Khi workflow ở trạng thái này, cần hiểu là việc xử lý bằng cơ chế tự động `Compensation` trước đó đã thất bại. Cần có một quy trình khác để khắc phục tình trạng này.

## Những bước nào cần thực hiện `Compensation` trong workflow

Việc quay lui chỉ nên thực hiện đối với các bước có làm thay đổi trạng thái dữ liệu mà thôi, các bước chỉ mang tính chất truy vấn dữ liệu sẽ không cần thiết phải thực hiện lại trong tiến trình quay lui này.

Trong định nghĩa workflow, chúng ta cần cấu hình trường `WF_STEP_DEF.ON_COMPENSATING`. Trường này có 2 giá trị:

- Compensate
- DoNotCompensate

Trong đó:

`Compensate`: quy định trong tiến trình `Compensation`, bước này sẽ được thực hiện

`DoNotCompensate`: quy định trong tiến trình `Compensation`, bước này sẽ **không** được thực hiện

> Việc cấu hình các bước trong Workflow một cách hợp lý sẽ giúp cho hiệu suất thực hiện cao và tránh sai sót dữ liệu.



