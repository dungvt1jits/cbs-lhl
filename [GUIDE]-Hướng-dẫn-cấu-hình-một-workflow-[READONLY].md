# Mô tả

- Workflow [ReadOnly] là workflow khi thực thi không làm thay đổi trạng thái CSDL[^1]. Các workflow này chỉ đọc dữ liệu mà thôi. Ví dụ như các workflow RuleFunc loại `GETINFO` hoặc `LOOKUP` chỉ lấy dữ liệu phục vụ cho thao tác giao diện từ người dùng.

- Các workflow khi thực thi sẽ sinh ra dữ liệu thực thi. Việc lưu trữ dữ liệu thực thi của các workflow [READONLY] sẽ làm tốn `storage` của Database và không cần thiết cho việc khai thác về sau. Do đó sau khi sử dụng xong chúng ta cần xóa bỏ chúng đi. Việc xóa bỏ có thể thực hiện tự động bằng cách cấu hình workflow.

# Cách thực hiện

- Trong thông tin định nghĩa của workflow - bảng `WF_DEF` - có trường `KeepExecSeconds`. Trường này kiểu số nguyên.

- Thiết lập giá trị:
  - Giá trị mặc định là `0`
  - Nếu có giá trị `= 0`, thời điểm workflow bị hủy dữ liệu thực thi sẽ được tính bằng:
    > `Thời điểm gửi workflow` + `Khoảng thời gian Timeout của workflow` + `biến DB_PURGE_BACKWARDS_IN_SECONDS`
  - Nếu có giá trị `> 0`, thời điểm workflow bị hủy dữ liệu thực thi sẽ được tính bằng:
    > `Thời điểm gửi workflow` + `Khoảng thời gian Timeout của workflow` + `giá trị KeepExecSeconds` 

# Tiến trình thực hiện

- Định kỳ (quy định bởi `biến`[^2] `DB_PURGE_INTERVAL_IN_SECONDS`), hệ thống sẽ tiến hành hủy bỏ các workflow có thời gian purge nhỏ hơn thời điểm thực hiện tiến trình này.

- Giả sử đối với các workflow có tính chất [READONLY], ta cấu hình giá trị `KeepExecSeconds` là 300 (tức 300 giây, ~ `5` phút). Vậy từ thời điểm hoàn tất workflow đến thời điểm `[5 phút + DB_PURGE_INTERVAL_IN_SECONDS]`, dữ liệu thực thi của các workflow này sẽ bị xóa bỏ khỏi hệ thống. Các workflow có cấu hình `KeepExecSeconds = 0` sẽ hoạt động theo cơ chế `Archiving & Purging`[^3].

---
---
[^1]: Cơ sở dữ liệu
[^2]: Biến trong Neptune là hệ thống tham số cấu hình, có thể tùy chỉnh qua giao diện trong `app Portal`
[^3]: ` là cơ chế backup dữ liệu và xóa bỏ dữ liệu thực thi workflow ra khỏi hệ thống theo tham số cấu hình