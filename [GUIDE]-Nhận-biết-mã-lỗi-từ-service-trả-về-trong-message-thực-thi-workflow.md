# Giới thiệu

Khi gọi một workflow, các bước của Workflow được thực hiện theo thứ tự [`StepGroup` - `StepOrder`]

Nếu một bước được xử lý với trạng thái không thành công, thì giá trị trả về của `response.status` là `ERROR`

Ngoài ra, cần phân biệt lỗi cụ thể là gì để có ứng xử phù hợp ở nơi tiêu thụ giá trị trạng thái `ERROR` này.

# Xem mã lỗi từ service trả về

Trong phần `respose` của message sẽ được bổ sung thêm một giá trị `error_message`.

Các `functional service` khi trả về trạng thái lỗi cho một step, cần cung cấp thêm thông tin về lỗi này.

Thông tin này sẽ được cung cấp/hiển thị cho người dùng cuối dưới dạng văn bản tường minh.

Phía client, khi nhận một message có step có trạng thái `ERROR` thì đi đến đường dẫn `...response.error_message` để lấy nội dung lỗi hiển thị cho người dùng cuối.

Ví dụ:

````c#
public WorkflowScheme Error(WorkflowScheme workflow, ValidationResult validationResult)
{
	workflow.response.data = validationResult.Errors.ToDictionary();
	workflow.response.error_message = .... // thông tin văn bản lỗi
	workflow.response.status = JITS.NeptuneClient.Scheme.Workflow.WorkflowScheme.RESPONSE.EnumReponseStatus.ERROR;
	return workflow;
}
````