# Bút toán mua bán ngoại tệ

Bút toán mua bán ngoại tệ là bút toán có loại tiền bên `Nợ` khác với loại tiền bên `Có`.

> Trong phần xử lý của core Neptune chỉ chấp nhận bút toán có duy nhất một loại tiền ở một bên.

Quy ước:
- `BCY`: tiền chuẩn, Base Currency (MMK)
- `FCY`: tiền ngoại tệ, Foreign Currency

Cột `Equivalent` luôn phải đảm bảo cân giữa bên Nợ và bên Có

# Trường hợp `FCY-to-FCY`

## `FCY-to-FCY` (1-1)

Bài toán:

| Dòng | D/C | Branch | Account | Currency | Amount | Equivalent |
|------|-----|--------|---------|----------|--------|------------|
| 1    | D   | 101    | CASH    | USD      | 1      | 210000     |
| 2    | C   | 101    | DEPOSIT | THB      | 34     | 210000     |

Sinh bút toán tự động:

| Dòng | D/C | Branch | Account           | Currency | Amount | Equivalent |
|------|-----|--------|-------------------|----------|--------|------------|
| 1    | D   | 101    | CASH              | USD      | 1      | 210000     |
| *    | C   | 101    | `FX-POSITION`-THB   | USD      | 1      | 210000     |
| *    | D   | 101    | `FX-EQUIVALENT`-THB | MMK      | 210000 | 210000     |
|      |     |        |                   |          |        |            |
| 2    | C   | 101    | DEPOSIT           | THB      | 34     | 210000     |
| *    | D   | 101    | `FX-POSITION`-USD   | THB      | 34     | 210000     |
| *    | C   | 101    | `FX-EQUIVALENT`-USD | MMK      | 210000 | 210000     |

Sắp xếp lại bút toán theo tiền tệ, ta có:

| Dòng | D/C | Branch | Account           | Currency | Amount | Equivalent |
|------|-----|--------|-------------------|----------|--------|------------|
| 1    | D   | 101    | CASH              | USD      | 1      | 210000     |
| *    | C   | 101    | `FX-POSITION`-THB   | USD      | 1      | 210000     |
|      |     |        |                   |          |        |            |
| 2    | C   | 101    | DEPOSIT           | THB      | 34     | 210000     |
| *    | D   | 101    | `FX-POSITION`-USD   | THB      | 34     | 210000     |
|      |     |        |                   |          |        |            |
| *    | D   | 101    | `FX-EQUIVALENT`-THB | MMK      | 210000 | 210000     |
| *    | C   | 101    | `FX-EQUIVALENT`-USD | MMK      | 210000 | 210000     |

Nhận xét:
- Bút toán cân theo từng cặp tiền tệ

## `FCY-to-FCY` (n-n)

Bài toán:

| Dòng | D/C | Branch | Account | Currency | Amount | Equivalent |
|------|-----|--------|---------|----------|--------|------------|
| 1    | D   | 101    | CASH    | USD      | 1      | 2100       |
| 2    | D   | 101    | DEPOSIT | USD      | 34     | 71432      |
| 3    | C   | 101    | GL1     | EUR      | 2      | 4619       |
| 4    | C   | 101    | GL2     | EUR      | 29     | 68913      |

Sinh bút toán tự động:

| Dòng | D/C | Branch | Account           | Currency | Amount | Equivalent |
|------|-----|--------|-------------------|----------|--------|------------|
| 1    | D   | 101    | CASH              | USD      | 1      | 2100       |
| *    | C   | 101    | `FX-POSITION`-USD   | USD      | 1      | 2100       |
| *    | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 210000 | 2100       |
|      |     |        |                   |          |        |            |
| 2    | D   | 101    | DEPOSIT           | USD      | 34     | 71432      |
| *    | C   | 101    | `FX-POSITION`-USD   | USD      | 34     | 71432      |
| *    | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 71432  | 71432      |
|      |     |        |                   |          |        |            |
| 3    | C   | 101    | GL1               | EUR      | 3      | 4,619      |
| *    | D   | 101    | `FX-POSITION`-EUR   | EUR      | 3      | 4,619      |
| *    | C   | 101    | `FX-EQUIVALENT`-EUR | MMK      | 4,619  | 4,619      |
|      |     |        |                   |          |        |            |
| 4    | C   | 101    | GL2               | EUR      | 178    | 68,913     |
| *    | D   | 101    | `FX-POSITION`-EUR   | EUR      | 178    | 68,913     |
| *    | C   | 101    | `FX-EQUIVALENT`-EUR | MMK      | 68,913 | 68,913     |

Sắp xếp lại:

| Dòng | D/C | Branch | Account           | Currency | Amount | Equivalent |
|------|-----|--------|-------------------|----------|--------|------------|
| 1    | D   | 101    | CASH              | USD      | 1      | 2100       |
| *    | C   | 101    | `FX-POSITION`-USD   | USD      | 1      | 2100       |
|      |     |        |                   |          |        |            |
| 2    | D   | 101    | DEPOSIT           | USD      | 34     | 71432      |
| *    | C   | 101    | `FX-POSITION`-USD   | USD      | 34     | 71432      |
|      |     |        |                   |          |        |            |
| 3    | C   | 101    | GL1               | EUR      | 3      | 4,619      |
| *    | D   | 101    | `FX-POSITION`-EUR   | EUR      | 3      | 4,619      |
|      |     |        |                   |          |        |            |
| 4    | C   | 101    | GL2               | EUR      | 178    | 68,913     |
| *    | D   | 101    | `FX-POSITION`-EUR   | EUR      | 178    | 68,913     |
|      |     |        |                   |          |        |            |
| *    | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 210000 | 2100       |
| *    | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 71432  | 71432      |
| *    | C   | 101    | `FX-EQUIVALENT`-EUR | MMK      | 4,619  | 4,619      |
| *    | C   | 101    | `FX-EQUIVALENT`-EUR | MMK      | 68,913 | 68,913     |

# Trường hợp FCY-to-BCY (n-n)

Bài toán:

| Dòng | D/C | Branch | Account | Currency | Amount | Equivalent |
|------|-----|--------|---------|----------|--------|------------|
| 1    | D   | 101    | CASH    | USD      | 100    | 210000     |
| 2    | D   | 101    | DEPOSIT | USD      | 50     | 105000     |
| 3    | C   | 101    | CASH    | MMK      | 200000 | 200000     |
| 4    | C   | 101    | GL      | MMK      | 115000 | 115000     |

Hệ thống sinh các dòng FX Clearing

| Dòng | D/C | Branch | Account             | Currency | Amount | Equivalent |
|------|-----|--------|---------------------|----------|--------|------------|
| 1    | D   | 101    | CASH                | USD      | 100    | 210000     |
| *    | C   | 101    | `FX-POSITION`USD    | USD      | 100    |            |
| *    | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 210000 |            |
|      |     |        |                     |          |        |            |
| 2    | D   | 101    | DEPOSIT             | USD      | 50     | 105000     |
| *    | C   | 101    | `FX-POSITION`USD    | USD      | 50     |            |
| *    | D   | 101    | `FX-EQUIVALENT`USD  | MMK      | 105000 |            |
|      |     |        |                     |          |        |            |
| 3    | C   | 101    | CASH                | MMK      | 200000 | 200000     |
| 4    | C   | 101    | GL                  | MMK      | 115000 | 115000     |

Kiểm tra theo từng bút toán: cùng chi nhánh, cùng Currency:

| Dòng | D/C | Branch | Account             | Currency | Amount | Equivalent |
|------|-----|--------|---------------------|----------|--------|------------|
| 1    | D   | 101    | CASH                | USD      | 100    | 210000     |
| 2    | D   | 101    | DEPOSIT             | USD      | 50     | 105000     |
| *    | C   | 101    | `FX-POSITION`USD    | USD      | 100    |            |
| *    | C   | 101    | `FX-POSITION`USD    | USD      | 50     |            |
|      |     |        |                     |          |        |            |
| *    | D   | 101    | `FX-EQUIVALENT`-USD | MMK      | 210000 |            |
| *    | D   | 101    | `FX-EQUIVALENT`USD  | MMK      | 105000 |            |
| 3    | C   | 101    | CASH                | MMK      | 200000 | 200000     |
| 4    | C   | 101    | GL                  | MMK      | 115000 |            |

Tổng USD: 
- Tổng Nợ bằng tổng Có; 100 + 50 = 150

Tổng MMK:
- Tổng Nợ bằng tổng Có; `210000 + 105000` = `200000 + 115000` = `315000`