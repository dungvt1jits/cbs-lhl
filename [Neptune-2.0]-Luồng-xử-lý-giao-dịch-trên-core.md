# Mô tả luồng giao dịch chính

Giao dịch từ `Neptune Workflow` được gửi đến các service:
1. `Admin`

Sinh mã số giao dịch - `TransactionNumber`

Ngoài ra còn có các trường chung và được lưu vào bảng `Transaction` của tất cả các service liên quan

Cập nhật bảng [Transaction] dùng chung: https://hcm.jits.com.vn:8062/rndhcm/cbs-shwe/-/issues/559

Để thuận tiện, sau khi `Admin` xử lý xong Journal, cần đưa thông tin này vào context của message giao dịch để các service liên quan cập nhật lại `Transaction` của mình:
- TransactionNumber
- ValueDate
	
2. Chức năng liên quan

- MasterObject (bảng `xxx`): bảng lưu dữ liệu chính của đối tượng nghiệp vụ (CREDIT, DEPOSIT, IFCBal, MORTGAGE...)
- MasterTrans (bảng `xxxTrans`): lưu thay đổi có liên quan đến kế toán (có thông tin về số tiền)
- `TransRuleConfig`: bảng cấu hình cách sinh bút toán theo nghiệp vụ
- `GLEntries`: bảng bút toán GL được tạo từ bảng `xxxTrans`. Bảng này được tạo ra để tổng hợp khi chuyển sang ACT. Lưu dữ liệu trong thời gian 30 ngày để đối soát.

# Nguyên tắc chung trong việc cập nhật dữ liệu

- Admin luôn thực hiện trước tiên để kiểm tra quyền và tạo số giao dịch cho các service chức năng. Dữ liệu chung được đặt trong `context` của message.
- Các service chức năng cập nhật dữ liệu theo quy tắc:
  - Theo từng MasterObject trong service, lặp lại:
    - Cập nhật dữ liệu cho `MasterObject`
    - Cập nhật dữ liệu cho bảng `TransactionDetails` (Audit)
    - Sinh dữ liệu cho bảng `xxxTrans` để tạo bút toán về sau
    - Đối với Mode sinh GL trong giao dịch, gọi luôn chức năng tạo bút toán từ bảng `xxxTrans`. Dữ liệu bút toán sẽ được sinh ra đặt tại bảng `GLEntries`

- Các service ghi nhận dữ liệu vào bảng `Transaction` (dữ liệu thông tin về giao dịch, bảng này service nào cũng có để có thể truy vấn thông tin giao dịch độc lập không phụ thuộc vào admin nữa)

![image](uploads/7b8fa50044da83b669e5f428f9b90f75/image.png)

- Các thay đổi lên `Master Object` được ghi nhận vào bảng:
  - `MasterTrans`: dữ liệu thay đổi có liên quan đến GL
  - `TransactionDetails`: dữ liệu thay đổi cho mục đích Audit

- Các thay đổi khác ghi nhận như xử lý hiện tại

## Bảng trans chung cho các service

- `Transaction`, `GLEntries`:
  - o9deposit
  - o9credit
  - o9cash
  - o9fx
  - o9mortgage
  - o9fac
  - o9payment




