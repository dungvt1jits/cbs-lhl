# Mô tả

Hệ thống Neptune hỗ trợ việc hủy bỏ một giao dịch (bản chất là một workflow) đã hoàn thành trước đó dưới hình thức `reverse` một workflow. Để thực hiện việc này, Neptune cung cấp một API để thực hiện:

> /api/workflow/reverse

với `Request Body`:

```
{
  "execution_id": "{{execution_id}}",
  "token": "..."
}
```

trong đó:
  + `execution_id`: là số execution của giao dịch cần hủy
  + `token`: là token còn hiệu lực tại thời điểm thực hiện hủy. Nếu truyền giá trị null hoặc rỗng ("") thì Neptune server sẽ dùng token đã thực hiện giao dịch trước đó để sử dụng. Tuy nhiên, chức năng hủy nên truyền vào vì quy định thông thường token chỉ có hiệu lực khoảng 30 phút mà thôi.

# Điều kiện thực hiện

* Giao dịch bị hủy phải có thuộc tính cho phép Hủy (WF_DEFINITION.AllowReversal = `Yes`)
* Giao dịch hủy trước đó phải có trạng thái `Completed`
* Giao dịch phải tồn tại trong hệ thống (chưa bị `Archiving` hoặc `Purging`)
* Giao dịch chưa từng bị hủy trước đó (đã hủy thành công 1 lần)

# Nội dung xử lý

- Hệ thống khởi tạo một giao dịch hoàn toàn mới với nội dung giống nội dung của giao dịch muốn hủy trước đó.
- Khi chuyển message cho các service, trường `request.request_header.reversal_execution_id` của WorkflowScheme chứa ExecutionID của message cần hủy. Lúc này Service cần kiểm tra giá trị này khác null hay không để biết được giao dịch hoặc đơn giản gọi phương thức `request.request_header.IsReversal()` để biết giao dịch đang xử lý có phải là giao dịch `reversal` hay không. Ví dụ:

```
if (workflow.request.request_header.IsReversal())
{
   Console.WriteLine("-- IS_REVERSED: YES");
}
else
{
   Console.WriteLine("-- IS_REVERSED: NO");
}
```

- Khi thực hiện giao dịch `reversal`, vẫn có trường hợp `Compensation` như giao dịch bình thường. Vì vậy, các service cũng cần implement đầy đủ trường hợp Compensation cho giao dịch reversal.

- Trong trường hợp không thể thực hiện được quy trình Compensation, các bước sẽ bị đánh dấu là `disputed`. Lúc này cần phải xử lý thủ công từng trường hợp.

## Các mã lỗi thường gặp

- Hủy bỏ Workflow không cho phép hủy

```
{
    "time_in_miliseconds": 7,
    "status": "ERROR",
    "description": "Workflow (ID=[TEST_001]) is not allowed to reverse.",
    "execution_id": null,
    "data": null
}
```

- Hủy bỏ Workflow với ExecutionID không tồn tại

```
{
    "time_in_miliseconds": 5,
    "status": "ERROR",
    "description": "Execution ID [f2d0ef7ec64d4d99a86c1bc3d068b0e0x] not found.",
    "execution_id": null,
    "data": null
}
```

- Hủy bỏ Workflow với ExecutionID đã bị hủy trước đó

```
{
    "time_in_miliseconds": 7,
    "status": "ERROR",
    "description": "Execution ID [8a5d11782d364f2d9c540395f20959a5] has been reversed.",
    "execution_id": null,
    "data": null
}
```

# Dữ liệu thay đổi cần chú ý

- Giao dịch hủy, trường `REVERSED_EXECUTION_ID` chứa ExecutionID của giao dịch bị hủy

| # EXECUTION_ID                   | WORKFLOW_TYPE | REVERSED_EXECUTION_ID            | REVERSED_BY_EXECUTION_ID         |
|----------------------------------|---------------|----------------------------------|----------------------------------|
| 3878402d6a4845ec8b27d4f85af9d7fb | Reversal      | 6a83c7d89d5842ad920372f75ab911db |                                  |

- Giao dịch bị hủy, trường `REVERSED_BY_EXECUTION_ID` chứa ExecutionID của giao dịch hủy mình

| # EXECUTION_ID                   | WORKFLOW_TYPE | REVERSED_EXECUTION_ID            | REVERSED_BY_EXECUTION_ID         |
|----------------------------------|---------------|----------------------------------|----------------------------------|
| 6a83c7d89d5842ad920372f75ab911db | Normal        |                                  | 3878402d6a4845ec8b27d4f85af9d7fb |
