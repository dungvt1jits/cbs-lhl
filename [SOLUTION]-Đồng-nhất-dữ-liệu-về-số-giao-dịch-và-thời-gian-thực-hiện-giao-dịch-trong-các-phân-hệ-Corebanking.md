# Mô tả

Hiện tại service admin đang dùng số execution_id từ Neptune server làm số tham chiếu giao dịch và thời gian thực hiện giao dịch được dùng theo thời gian đăng ký giao dịch với Neptune server.

Thêm nữa các phân hệ chức năng cũng tự lấy thời gian thực hiện của mình làm thời gian ghi nhận dữ liệu nghiệp vụ. Chẳng hạn thông tin về thời gian statement của DPT, ACT, và Admin là không giống nhau.

Theo nghiệp vụ core banking, các dữ liệu giữa master account và GL, Journal đều phải _nhất quán_ .

Do đó cần thống nhất một nơi duy nhất tạo ra dữ liệu này và các bên liên quan sử dụng dữ liệu này.

# Giải pháp

Tất cả các giao dịch có liên quan làm thay đổi trạng thái dữ liệu database đều phải gọi qua service admin để kiểm tra quyền. Ta sẽ sử dụng bước này để sinh dữ liệu về giao dịch theo nghiệp vụ:
- số giao dịch (theo logic)
- ngày giờ giao dịch (theo logic)
- các thông tin khác (chẳng hạn thông tin về hạn mức giao dịch hoặc thông tin khác phát sinh sau này)

Các thông tin này sẽ được lưu trong header của message trao đổi giữa các service với nhau và đặt trong trường `request.request_header.tx_context`.

```c#
public Dictionary<string, object> tx_context { get; set; } = new();
```

`tx_context` là kiểu dữ liệu `Dictionary<string, object>` dùng để chứa các dữ liệu do `admin` service sinh ra. Các service khi xử lý công việc của mình có thể lấy trực tiếp biến này với các key quy ước từ phân hệ admin để xử lý (`admin` cần tạo spec rõ ràng để các service xử lý dữ liệu này một cách đúng đắn)

Ví dụ, admin sẽ đặt dữ liệu vào message như sau

```cs
private void NeptuneClientQueue_WorkflowDelivering(WorkflowScheme workflow)
{
        if (workflow.request.request_header.step_code == "SQL_CAN_USER_INVOKE_COMMAND")
        {
            workflow.request.request_header.tx_context.Add("tx_id", DateTime.Now.Ticks);
            workflow.request.request_header.tx_context.Add("tx_dt", DateTime.Now.ToString());
            workflow.request.request_header.tx_context.Add("user_limit", new Random().NextDouble());
        }
}
```

# Thay đổi DB và Code

- DDL liên quan https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/issues/2027

- Cần cập nhật phiên bản NeptuneClient [1.1.5.2](https://hcm.jits.com.vn:8020/packages/jits.neptune.neptuneclient/1.1.5.2)
