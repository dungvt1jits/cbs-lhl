## Vấn đề

Các service chức năng trong hệ thống Neptune, khi đang hoạt động, định kỳ sẽ gửi tín hiệu xác nhận trạng thái khả dụng cho Neptune server.

Neptune server căn cứ trên trạng thái này để biết một service tình trạng có tốt không.

Trong trường hợp cần bảo trì, nâng cấp, làm gián đoạn sự liên tục có thể dẫn đến sự sai sót dữ liệu nếu giao dịch có liên quan đến nhiều service. Lúc này sẽ phát sinh giao dịch có trạng thái `Disputed`.

Để hạn chế phải sửa dữ liệu thủ công khi có sai sót. Ta cần đóng/mở service một cách hợp lý

## Quy trình thực hiện

1. Trước khi tắt service

Trước khi tắt service, ta cần chuyển trạng thái hoạt động của service sang trạng thái `Inactive`. Với trạng thái này, các workflow có liên quan đến service này sẽ bị Neptune server từ chối.

Ví dụ, service `MS1` sau khi bị chuyển trạng thái thì workflow có dùng đến service `MS1`, khi đưa vào hệ thống, `Neptune server` sẽ trả về lỗi như sau:

```json
{
    "time_in_miliseconds": 143,
    "status": "ERROR",
    "description": "The service [MS1] is not active.",
    "execution_id": null,
    "data": null
}
```

Và như vậy service sẽ <u>an toàn</u> để tiến hành nâng cấp, bảo trì.

2. Sau khi nâng cấp, khởi động lại service

Tương tự như bước trên, sau khi bảo trì service, ta khởi đông service hoàn chỉnh, rồi thực hiện chuyển trạng thái service từ `Inactive` thành `Active`

3. Thực hiện qua App Portal

Vào app Portal, menu `Service management`, chọn service và chuyển trạng thái trường Status (`Active`, `Inactive`) hoặc từ menu ngữ cảnh theo task https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/issues/1992