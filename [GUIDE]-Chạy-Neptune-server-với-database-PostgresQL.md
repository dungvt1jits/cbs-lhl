# Cài đặt database

Xem thêm [Cài đặt Database PostgresQL](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/C%C3%A0i-%C4%91%E1%BA%B7t-Database-(MySQL,-MariaDB,-PostgresQL,-SQLServer)-trong-Docker-Engine#c%C3%A0i-db-postgresql-ch%E1%BA%A1y-trong-m%C3%B4i-tr%C6%B0%E1%BB%9Dng-docker-engine)

# Cấu hình Database

## Tạo Database NeptuneDB

Dùng tool Postgres Client (chẳng hạn pgAdmin4) để tạo database, đặt tên là `NeptuneDB`

### Tạo Schema NeptuneDB

Tạo một Schema, chẳng hạn `NeptuneDB`

### Dữ liệu cấu hình

#### [Cấu hình `CONFIGURATION`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/C%E1%BA%A5u-h%C3%ACnh-m%E1%BA%B7c-%C4%91%E1%BB%8Bnh#b%E1%BA%A3ng-configuration)

#### [Cấu hình `ENVIRONMENT_VARIABLE`](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/C%E1%BA%A5u-h%C3%ACnh-m%E1%BA%B7c-%C4%91%E1%BB%8Bnh#b%E1%BA%A3ng-environment_variable)

# Thiết lập biến môi trường OS để chạy NeptuneServer

Xem [[Guide] Hướng dẫn cấu hình kết nối Database Online
Giới thiệu](https://hcm.jits.com.vn:8062/rndhcm/cbs-neptune/-/wikis/%5BGuide%5D-H%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-c%E1%BA%A5u-h%C3%ACnh-k%E1%BA%BFt-n%E1%BB%91i-Database-Online)