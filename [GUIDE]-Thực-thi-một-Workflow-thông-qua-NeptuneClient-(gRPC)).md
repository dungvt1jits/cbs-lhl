## Yêu cầu

Thực thi một Workflow thông qua `NeptuneClient`

## Cách thực hiện

- Khởi tạo một đối tượng `GrpcClient`
- Gọi phương thức ExecuteWorkflow với 2 tham số
```
public WorkflowResponse<object> ExecuteWorkflow(string pToken, string pMessageContent)
```
  - `pToken`: là token được cấp từ Neptune Server
  - `pMessageContent`: thể hiện cho đối tượng Workflow body

## Yêu cầu
NeptuneClient phiên bản `1.1.4.1`